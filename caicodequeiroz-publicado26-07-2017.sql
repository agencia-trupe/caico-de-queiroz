-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: mysql01.caicodequeiroz.hospedagemdesites.ws
-- Generation Time: 26-Jul-2017 às 12:35
-- Versão do servidor: 5.6.35-81.0-log
-- PHP Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `caicodequeiroz`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `about`
--

CREATE TABLE `about` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tumblr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `snapchat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pinterest` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `periscope` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vimeo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `googleplus` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wordpress` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `about`
--

INSERT INTO `about` (`id`, `texto`, `imagem`, `instagram`, `twitter`, `facebook`, `tumblr`, `linkedin`, `snapchat`, `pinterest`, `youtube`, `periscope`, `vimeo`, `googleplus`, `wordpress`, `created_at`, `updated_at`) VALUES
(2, '<p>A Ca&iacute;co de Queiroz Agentes Associados atua hoje em uma plataforma plural. Al&eacute;m do mercado art&iacute;stico, que conta com apresentadoras, atores, maquiadores, estilistas, cabeleireiros, designers, gourmets, fot&oacute;grafos, jornalistas, roteiristas, palestrantes e pensadores. Temos uma vertente muito forte na &aacute;rea de moda, com influenciadores em v&aacute;rios segmentos, al&eacute;m da &aacute;rea de licenciamento.&nbsp;</p>\r\n\r\n<p>O empres&aacute;rio Ca&iacute;co de Queiroz atua no mercado desde 1984 e abriu seu escrit&oacute;rio em 2004, com sede em Moema, S&atilde;o Paulo.</p>\r\n\r\n<p>Com um olhar diferenciado para o mercado e sobre o gerenciamento e planejamento, da vida profissional de seus clientes, a empresa busca alinhar, de maneira criteriosa, tanto a carreira de influenciadores, como produtos e conte&uacute;dos. Atuamos de acordo com as necessidades do mercado atual, cada vez mais din&acirc;mico e competitivo, em todos os segmentos.&nbsp;</p>\r\n\r\n<p>Nas nossas redes de plataformas e c&eacute;lulas de atua&ccedil;&atilde;o contamos com Agenciamento Art&iacute;stico, Conte&uacute;dos, Licenciamentos de Produtos e Publica&ccedil;&otilde;es Liter&aacute;rias. Para isso, contamos com uma equipe de profissionais capazes de localizar e buscar quaisquer oportunidades de neg&oacute;cios nos mais diferenciados or&ccedil;amentos e solicita&ccedil;&otilde;es, absorvendo tanto o mercado nacional quanto internacional.&nbsp;</p>\r\n\r\n<p>Antenados com as tend&ecirc;ncias do mercado, percebemos que existe uma vanguarda art&iacute;stica ao mesmo tempo rica em talento, mas carente de bons servi&ccedil;os. Por isso inovamos e criamos departamentos que possam atender as necessidades do mercado, al&eacute;m de oferecer aos nossos profissionais agenciados a oportunidade de expandir suas &aacute;reas de atua&ccedil;&atilde;o.</p>\r\n\r\n<p>Convido voc&ecirc; a &nbsp;navegar por nosso site e conhe&ccedil;a as possibilidades que oferecemos.&nbsp;</p>\r\n\r\n<p>Bem-vindo a Ca&iacute;co de Queiroz Agentes Associados!</p>\r\n', 'foto-perfil-about.jpg', 'https://www.instagram.com/caicodequeiroz_empresariamento/', 'www.twitter.com/caicodequeiroz', 'https://www.facebook.com/Ca%C3%ADco-de-Queiroz-Agentes-Associados-183213735031558/?fref=ts', 'http://caicodequeiroz.tumblr.com/', 'https://www.linkedin.com/in/caicodequeiroz', 'caicodequeiroz', 'https://www.pinterest.com/caicodequeiroz/', 'https://www.youtube.com/user/1caicodequeiroz', 'https://www.periscope.tv/caicodequeiroz', 'https://vimeo.com/user45261002', 'https://plus.google.com/114735752105681672346', 'https://caicodequeiroz.wordpress.com/', '0000-00-00 00:00:00', '2017-05-11 17:28:42');

-- --------------------------------------------------------

--
-- Estrutura da tabela `acao_social`
--

CREATE TABLE `acao_social` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `acao_social`
--

INSERT INTO `acao_social` (`id`, `texto`, `created_at`, `updated_at`) VALUES
(1, '<p>A&ccedil;&atilde;o social. Qual a verdadeira express&atilde;o desta palavra?</p>\r\n\r\n<p>Hoje todos lutamos por um mundo melhor, por um mundo onde a busca por dias melhores se torna di&aacute;ria e necess&aacute;ria. Qual &eacute; o&nbsp; mundo que queremos deixar para a humanidade?</p>\r\n\r\n<p>Na comemora&ccedil;&atilde;o do nosso anivers&aacute;rio de 10 anos (2015) , &nbsp;iniciei de forma mais ativa o apoio as a&ccedil;&otilde;es sociais para que possamos deixar os pr&oacute;ximos anos melhores e, juntamente com toda minha equipe e staff de personalidades, abra&ccedil;amos duas causas: o Movimento Think And love&nbsp; e a crech&ecirc; Tiaozinho , comunidade de crian&ccedil;as carentes que h&aacute; cinco anos alegramos na P&aacute;scoa e Natal.</p>\r\n\r\n<p>Em 20016 apoiamos, atrav&eacute;s de um trabalho ativo junto ao nosso elenco, as seguintes a&ccedil;&otilde;es;</p>\r\n\r\n<p><strong>&ldquo;Brazil Foudation&rdquo;</strong> com Pedro Andrade, &nbsp;funda&ccedil;&atilde;o que mobiliza recursos para ideias e a&ccedil;&otilde;es que transformam o Brasil. Trabalham com l&iacute;deres e organiza&ccedil;&otilde;es sociais e uma rede global de apoiadores para promover igualdade, justi&ccedil;a social e oportunidade).</p>\r\n\r\n<p><strong>&ldquo;Sou + Edica&ccedil;&atilde;o / Doe Educa&ccedil;&atilde;</strong>o&rdquo; - <strong>Instituto Ayrton Senna</strong> com Andr&eacute; Vasco, Celso Zucatelli e Daniella Cicarelli, iniciativa que traz hist&oacute;rias reais de estudantes que foram beneficiados pela organiza&ccedil;&atilde;o e ressalta que toda crian&ccedil;a merece a chance de ser vencedora).</p>\r\n\r\n<p><strong>&ldquo;Escreva Comigo- Instituto Ayrton Senna&rdquo;</strong> com Ant&ocirc;nio Fagundes, campanha realizada para atrair mais colaboradores para a causa da educa&ccedil;&atilde;o de qualidade.</p>\r\n\r\n<p>&ldquo;<strong>Eu Cinto a Vida&rdquo;</strong> em parceria com a <strong>ONG Tr&acirc;nsito Amigo</strong>, com Amanda Ramalho, Carol Scaff, Luitha, Marilia Moreno, Pen&eacute;lope Nova, Pedro Bosnich e Tatiana Dumenti a fim de conscientizar e estimular o uso do cinto de seguran&ccedil;a nos bancos traseiros.</p>\r\n\r\n<p><strong>&quot;Desmatamento Zero&quot; - Greenpeace Brasil,&nbsp;</strong>em parceria com a&nbsp;<strong>Ag&ecirc;ncia Lema</strong>, com Chiara Gadaleta, Cynthia Benini e Erick Krominski, para conscientiza&ccedil;&atilde;o dos males causados pelas queimadas no Brasil.</p>\r\n\r\n<p><strong>&quot;Tamb&eacute;m &eacute; Viol&ecirc;ncia&quot; - Artemis&nbsp;</strong>pela luta contra todas as formas de viol&ecirc;ncia dom&eacute;stica contra a mulher.</p>\r\n\r\n<p>Sempre estamos avaliando diversas a&ccedil;&otilde;es que, de fato, v&atilde;o transformar a vida de algumas pessoas, porque acreditamos que se defendermos uma causa, viveremos em um mundo melhor.</p>\r\n\r\n<p>Venha conosco fazer um mundo melhor!</p>\r\n', '0000-00-00 00:00:00', '2017-07-25 18:29:37');

-- --------------------------------------------------------

--
-- Estrutura da tabela `acao_social_imagens`
--

CREATE TABLE `acao_social_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `acao_social_id` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `acao_social_imagens`
--

INSERT INTO `acao_social_imagens` (`id`, `acao_social_id`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(916, 1, '20161013120015lilian-violencia.jpg', 0, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(917, 1, '20151015182558acao-associal-.jpg', 1, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(918, 1, '20160511104146fa3.jpg', 2, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(919, 1, '20160527124810marilia-morena-mascara.jpg', 3, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(920, 1, '20151019184418mov-0002-14-calendario-af3.jpg', 4, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(921, 1, '20160510151718artton-sena-01.jpg', 5, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(922, 1, '20151019184433mov-0002-14-calendario-af8.jpg', 6, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(923, 1, '20160510151718senna-valendo-2.jpg', 7, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(924, 1, '20160527140904amanda-ramalho-mascara2.jpg', 8, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(925, 1, '20151019184420mov-0002-14-calendario-af.jpg', 9, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(926, 1, '20151123170152marcabasica-brazilfoundation-cor-980x646.jpg', 10, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(927, 1, '20161013120029mel-violencia-site.jpg', 11, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(928, 1, '20160531181558carol-scaff-mascara.jpg', 12, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(929, 1, '20151201101515logo-tiaozinho.png', 13, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(930, 1, '20151019184432mov-0002-14-calendario-af7.jpg', 14, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(931, 1, '20161013120049erick-greenpeace.jpg', 15, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(932, 1, '20151019184439mov-0002-14-calendario-af12.jpg', 16, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(933, 1, '20160530154726ligia-site.jpg', 17, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(934, 1, '20160530153813andre-site.jpg', 18, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(935, 1, '20160527140846penelope-nova-mascara2.jpg', 19, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(936, 1, '20160510152224sena-valendo-3.jpg', 20, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(937, 1, '20151019184648mov-0002-14-calendario-af5.jpg', 21, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(938, 1, '20160613112154luize-cinto.jpg', 22, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(939, 1, '20151019184438mov-0002-14-calendario-af10.jpg', 23, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(940, 1, '20160527124809marcelo-sommer-mascara.jpg', 24, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(941, 1, '20160527124808alex-cardoso-mascara.jpg', 25, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(942, 1, '20151019184440mov-0002-14-calendario-af11.jpg', 26, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(943, 1, '20151019184412mov-0002-14-calendario-af6.jpg', 27, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(944, 1, '20160530153912zuca-site.jpg', 28, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(945, 1, '20151019184411mov-0002-14-calendario-af4.jpg', 29, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(946, 1, '20160514201255blog-eu-cinto-a-vida.jpg', 30, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(947, 1, '20151019184612mov-0002-14-calendario-af2.jpg', 31, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(948, 1, '20160530153842dani-site.jpg', 32, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(949, 1, '20160531181558dudu-mascara.jpg', 33, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(950, 1, '20160527124809luitha-mascara.jpg', 34, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(951, 1, '20160527124811tatiana-dumenti-mascara.jpg', 35, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(952, 1, '20160527124811pedro-bonisch-mascara.jpg', 36, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(953, 1, '20161013120042cinthya-greenpeace.jpg', 37, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(954, 1, '20151019184438mov-0002-14-calendario-af9.jpg', 38, '2017-07-25 18:29:37', '2017-07-25 18:29:37'),
(955, 1, '20170725152233graac.jpg', 39, '2017-07-25 18:29:37', '2017-07-25 18:29:37');

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `titulo`, `frase`, `imagem`, `link`, `ordem`, `created_at`, `updated_at`) VALUES
(16, 'Formadores De Opinião ', 'Profissionais em destaques em seus Segmentos ', 'formadores-opiniao.jpg', 'www.caicodequeiroz.com.br', 0, '2017-01-16 15:00:37', '2017-01-27 23:56:01'),
(19, 'Lúcia Bronstein ', 'Atriz -  Em Cartaz com a Peça ', 'lucia.jpg', 'http://www.caicodequeiroz.com.br/portfolio/entrada/lucia-bronstein', 0, '2017-01-16 19:45:59', '2017-01-28 00:00:29'),
(23, 'Divisão De Agenciamento ', 'Direcionamento e gerencia de carreiras ', 'agencia-mento-valendo.jpg', 'http://www.caicodequeiroz.com.br/about', 0, '2017-01-27 23:42:06', '2017-01-27 23:42:06'),
(24, 'Divisão De Licenciamento ', 'Produto Duda Molinos by Vult ', 'licenciamento-duda-folder.jpg', 'http://www.caicodequeiroz.com.br/licenciamento', 0, '2017-01-27 23:50:44', '2017-01-27 23:50:44'),
(27, 'Claudio Tognolli', 'Jornalismos Investigativo -  Divisão De Jornalismo ', 'buner-rodi.jpg', 'http://www.caicodequeiroz.com.br/portfolio/entrada/claudio-tognolli', 0, '2017-02-14 20:42:05', '2017-02-14 20:44:06'),
(28, 'Bethy Lagardère ', 'Divisão De Influenciadores ', 'bethy-rodmi-02.jpg', 'http://www.caicodequeiroz.com.br/portfolio/entrada/bethy-lagardere', 0, '2017-02-17 21:39:10', '2017-02-17 21:39:10'),
(29, 'Adriana Couto ', 'Apresentadora /Jornalista ', 'adriana-couto-romi-1.jpg', 'http://www.caicodequeiroz.com.br/portfolio/entrada/adriana-couto', 0, '2017-02-17 21:41:09', '2017-02-17 21:41:09'),
(30, 'Daniella Cicarelli', 'Apresentadora ', 'danmiella-rondomi-folder.jpg', '', 0, '2017-02-17 22:00:26', '2017-02-17 22:00:26'),
(31, 'O Rico E Lazãro ', 'Cassio Scapin  ( Beroso)  Saulo Meneghetti  ( Oziel ) ', 'folder-hommu.jpg', '', 0, '2017-03-12 23:08:31', '2017-03-12 23:08:31'),
(32, 'André Lima ', 'Estilista ', 'andre-lima-roming.jpg', '', 0, '2017-03-12 23:26:18', '2017-03-12 23:26:18'),
(33, 'Fabiana Gomes ', 'Porta voz  M.A.C  Cosmetics no Brasil ', 'fabaina-rodonn.jpg', 'http://www.caicodequeiroz.com.br/portfolio/entrada/fabiana-gomes', 0, '2017-03-21 15:57:14', '2017-03-21 15:57:14'),
(34, 'Walcyr Carrasco ', 'Escritor, Autor e Dramaturgo ', 'walcyr-rondomi.jpg', 'http://www.caicodequeiroz.com.br/portfolio/entrada/walcyr-carrasco', 0, '2017-03-21 18:09:00', '2017-03-21 18:09:00'),
(35, 'Costanza Pascolato ', 'Consultora De Estilo ', 'costanza-pas.jpg', 'http://www.caicodequeiroz.com.br/portfolio/entrada/costanza-pascolato-1-2', 0, '2017-03-28 19:42:07', '2017-03-28 19:42:07'),
(37, 'André Dias ', 'Novela Novo Mundo -  Rede Globo', 'bunner-roding.jpg', 'http://www.caicodequeiroz.com.br/portfolio/entrada/andre-dias', 0, '2017-05-18 23:41:05', '2017-05-18 23:41:05'),
(38, 'Adriana Araújo ', 'Ancora Jornal Da Record ', '20170613152055rodolmi-testeira.jpg', 'http://www.caicodequeiroz.com.br/portfolio/entrada/adriana-araujo', 0, '2017-06-13 18:20:55', '2017-06-13 18:20:55'),
(39, 'Luciana Mello ', 'Divisão de Bandas e Músicos ', '20170704221752luciana-mello-hondomi.jpg', 'http://caicodequeiroz.com.br/portfolio/entrada/luciana-mello', 0, '2017-07-05 01:17:52', '2017-07-05 01:17:52'),
(40, 'Lobão ', 'Divisão de Bandas e Músicos ', '20170704222446lobao-rondomi-valendo.jpg', 'http://caicodequeiroz.com.br/portfolio/entrada/lobao', 0, '2017-07-05 01:24:46', '2017-07-05 01:24:46'),
(41, 'Mel Lisboa ', 'Divisão De Atores ', '20170724205910rodolmi-m-el.jpg', 'http://www.caicodequeiroz.com.br/portfolio/detalhe/mel-lisboa', 0, '2017-07-24 23:59:11', '2017-07-24 23:59:11'),
(42, 'Divisão De Publicações ', 'Projetos de livros', '20170724211543publicacoes.jpg', 'http://www.caicodequeiroz.com.br/publicacoes', 0, '2017-07-25 00:15:43', '2017-07-25 00:15:43');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `telefone` text COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vimeo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `flickr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pinterest` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `telefone`, `endereco`, `google_maps`, `facebook`, `twitter`, `instagram`, `vimeo`, `youtube`, `linkedin`, `flickr`, `pinterest`, `created_at`, `updated_at`) VALUES
(7, '<p><strong>Caico de Queiroz Agentes Associados</strong></p>\r\n\r\n<p>contato@caicodequeiroz.com.br&nbsp;</p>\r\n\r\n<p>55 +&nbsp;11&nbsp;5054-2606<br />\r\n55 +&nbsp;11&nbsp;5051-9634<br />\r\n55 +&nbsp;11&nbsp;2649-9939</p>\r\n\r\n<p>Whatsapp 55 +&nbsp;11 99322-5005&nbsp;</p>\r\n\r\n<p><br />\r\n<strong>Atendimento</strong></p>\r\n\r\n<p>Lara Raimondo<br />\r\nlara@caicodequeiroz .com.br</p>\r\n\r\n<p>55 + 11 99322-5005</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Cac&aacute; Fonseca</p>\r\n\r\n<p>caca@caicodequeiroz.com.br</p>\r\n\r\n<p>55 + 11 99624-1150</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Paula Terras</strong></p>\r\n\r\n<p>paula@caicodequeiroz.com.br</p>\r\n\r\n<p>55 + 11 94564-3735</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Assistente</strong></p>\r\n\r\n<p>Priscila Buda</p>\r\n\r\n<p>priscila@caicodequeiroz.com.br</p>\r\n\r\n<p>55 + 11 95924-9569<br />\r\n<br />\r\n<strong>Financeiro</strong><br />\r\nSandra Regina Soares<br />\r\nsandra@caicodequeiroz.com.br<br />\r\n55 +&nbsp;11 96274-8560&nbsp;<br />\r\n&nbsp;</p>\r\n\r\n<p><strong>Imprensa</strong></p>\r\n\r\n<p>contato@caicodequeiroz.com.br</p>\r\n\r\n<p>55 + 11 99624-1150</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Licenciamento</strong><br />\r\nlicenciamento@caicodequeroiz.com.br</p>\r\n\r\n<p>55 + 11 96274-8560<br />\r\n<br />\r\n<br />\r\n&nbsp;</p>\r\n', '<p>contato@caicodequeiroz.com.br&nbsp;<br />\r\nAv. Rouxinol, 1041, cj 206 - Moema<br />\r\n04516-902 - S&atilde;o Paulo/SP - Brasil</p>\r\n', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.935356429954!2d-46.66607729999997!3d-23.60665119999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5a0602543253%3A0x77f12ff7e4d42fc8!2sAv.+Rouxinol%2C+1041+-+Indian%C3%B3polis%2C+S%C3%A3o+Paulo+-+SP%2C+04516-001!5e0!3m2!1spt-BR!2sbr!4v1438694580298" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '2017-07-11 18:35:06');

-- --------------------------------------------------------

--
-- Estrutura da tabela `imprensa`
--

CREATE TABLE `imprensa` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `imprensa`
--

INSERT INTO `imprensa` (`id`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
(2, '<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Auxiliamos todo o nosso staff junto a imprensa, exclusivamente para assuntos relacionados a sua carreira.&nbsp;</li>\r\n	<li>Alguns de nossos artistas possuem assessoria de imprensa exclusiva, voc&ecirc; poder&aacute; acess&aacute;-los atrav&eacute;s dos contatos.</li>\r\n</ul>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;&nbsp;</p>\r\n\r\n<p>Obrigado!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><u>Assessoria de Imprensa :</u></strong></p>\r\n\r\n<p><strong>Dudu Betholini</strong><br />\r\nAg&ecirc;ncia Lema&nbsp;-&nbsp;(11) 3871-0022<br />\r\nCl&aacute;udio Rodrigues</p>\r\n\r\n<p><strong>Gloria Coelho</strong><br />\r\nMSG Comunica&ccedil;&atilde;o - (11)&nbsp;3060-8455<br />\r\nPriscila - priscila@soniagoncalves.com.br</p>\r\n\r\n<p><strong>Luitha Miraglia</strong><br />\r\nVira Comunica&ccedil;ao &nbsp;(11) 98076-1234&nbsp;</p>\r\n\r\n<p>Guilherme Pichonelli - guilherme@viracomunica&ccedil;ao.com.br&nbsp;</p>\r\n\r\n<p><strong>Marcelo Rosenbaum</strong><br />\r\nMarqueterie ag&ecirc;ncia de Comunica&ccedil;&atilde;o - (11) 3083-7399<br />\r\nBia - bia@marqueterie.com.br</p>\r\n\r\n<p><strong>Sergio Menezes</strong><br />\r\nMercadocom - (21) 98600-3032<br />\r\nRibamar - rfilho@mercadocom.com.br &nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'img-imprensa.jpg', '0000-00-00 00:00:00', '2016-05-14 23:00:34');

-- --------------------------------------------------------

--
-- Estrutura da tabela `juridico`
--

CREATE TABLE `juridico` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `juridico`
--

INSERT INTO `juridico` (`id`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
(2, '<p>SOUZA BASTOS Advocacia e Consultoria &eacute; uma sociedade de advogados, com habilidades multidisciplinares, que tem como miss&atilde;o atender aos seus clientes com dedica&ccedil;&atilde;o, pessoalidade, confiabilidade e excel&ecirc;ncia.</p>\r\n\r\n<p>Com base em uma filosofia moderna, SOUZA BASTOS Advocacia e Consultoria busca prestar um assessoramento jur&iacute;dico eficiente e completo, compreendendo a consultoria jur&iacute;dica preventiva e a pr&aacute;tica forense, almejando a permanente busca de solu&ccedil;&otilde;es, a fim de alcan&ccedil;ar a satisfa&ccedil;&atilde;o plena dos seus clientes, sempre &agrave; luz da &eacute;tica profissional.</p>\r\n\r\n<p>O atendimento personalizado, a presteza de suas profissionais e a agilidade na presta&ccedil;&atilde;o dos servi&ccedil;os jur&iacute;dicos, sempre pautados pela &eacute;tica e profissionalismo, renderam a este escrit&oacute;rio a confian&ccedil;a, o respeito e a credibilidade dos seus clientes.</p>\r\n\r\n<p>SOUZA BASTOS Advocacia e Consultoria tem atua&ccedil;&atilde;o no &acirc;mbito contencioso e consultivo nos mais diversos campos do Direito.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>S&oacute;cio:</strong> Ant&ocirc;nio Jos&eacute; Souza Bastos<br />\r\n<strong>Endere&ccedil;o:</strong> Av. Tancredo Neves, 620, 33&ordm; andar, Ed. Mundo Plaza, Caminho das Arvores, CEP: 41.820-020, Salvador, Brasil<br />\r\n<strong>Telefone:</strong> 55 71 2202-6155<br />\r\n<strong>E-mail:</strong> antonio@souzabastosadvocacia.com.br<br />\r\n<strong>Site:&nbsp;</strong><a href="http://www.souzabastosadvocacia.com.br" target="_blank">www.souzabastosadvocacia.com.br</a></p>\r\n', '20150817123116Escritório-Externo-01.jpg', '0000-00-00 00:00:00', '2015-08-24 18:03:01');

-- --------------------------------------------------------

--
-- Estrutura da tabela `licenciamento`
--

CREATE TABLE `licenciamento` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `licenciamento`
--

INSERT INTO `licenciamento` (`id`, `texto`, `created_at`, `updated_at`) VALUES
(1, '<p>&nbsp;</p>\r\n\r\n<p>A nossa divis&atilde;o de licenciamento surgiu para atender a uma demanda de mercado. Em busca de diferenciais competitivos para suas marcas, grandes empresas desejavam a assinatura de grandes personalidades em suas linhas de produtos.&nbsp; Nomes do nosso <em>casting</em> come&ccedil;aram a ser consultados para encabe&ccedil;ar tais projetos.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Desde ent&atilde;o, o departamento foi estruturado com profissionais experientes no segmento, visando n&atilde;o s&oacute; atender com excel&ecirc;ncia a esta demanda, como tamb&eacute;m, a prospec&ccedil;&atilde;o de novos neg&oacute;cios.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Partindo deste cen&aacute;rio, trouxemos novas marcas e personalidades, incrementamos e diversificamos os perfis, atendendo de forma mais ampla aos anseios das empresas e, consequentemente, dos consumidores.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>O trabalho exige uma avalia&ccedil;&atilde;o detalhada de cada nome/marca do nosso <em>casting</em>, ent&atilde;o formatamos o planejamento estrat&eacute;gico e apresentamos aos principais players de cada categoria de produto. As prospec&ccedil;&otilde;es s&atilde;o consultivas, direcionadas a empresas alinhadas as nossas expectativas e que tenham espa&ccedil;o no portf&oacute;lio para acolher uma linha licenciada.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Atuamos ativamente na manuten&ccedil;&atilde;o do contrato, com especial aten&ccedil;&atilde;o ao desenvolvimento e aprova&ccedil;&atilde;o dos produtos, promo&ccedil;&otilde;es e ativa&ccedil;&otilde;es, recebimento dos relat&oacute;rios de royalties e avalia&ccedil;&atilde;o do desempenho de vendas.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Nosso objetivo &eacute; trazer ao mercado produtos exclusivos e de qualidade. Projetos que proporcionem aos nossos clientes o diferencial competitivo que procuram e as nossas personalidades e marcas, a sinergia de mercado que planejamos.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '0000-00-00 00:00:00', '2015-12-02 17:29:48');

-- --------------------------------------------------------

--
-- Estrutura da tabela `licenciamento_imagens`
--

CREATE TABLE `licenciamento_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `licenciamento_id` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `licenciamento_imagens`
--

INSERT INTO `licenciamento_imagens` (`id`, `licenciamento_id`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(550, 1, '2015110612545411148803-10202866554903648-1036967815970466697-n-1.jpg', 0, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(551, 1, '20151106125203logo-preto.jpg', 1, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(552, 1, '20151201113413catalogo-produtosindd.jpg', 2, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(553, 1, '20151201113415kit-masculino.jpg', 3, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(554, 1, '20151201113415kit-originalle.jpg', 4, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(555, 1, '20151201113340phisalia-20150915.jpg', 5, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(556, 1, '20151201113759sapatilhas-anacapri-amor.jpg', 6, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(557, 1, '201511061254225.jpg', 7, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(558, 1, '20151202150745kw-22.jpg', 8, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(559, 1, '20151202150851duda-molinos-helca.jpg', 9, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(560, 1, '2015121010552820151202151855campanha-01-neo-glamour.jpg', 10, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(561, 1, '20151202151746duda-molinos-fim-linha-maquiagem55382.jpg', 11, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(562, 1, '20151202151138divulgacao.jpg', 12, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(563, 1, '20151202151609linha-piratas-do-caribe-duda-molinos-02.jpg', 13, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(564, 1, '20151202151211divulgacao-1.jpg', 14, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(565, 1, '2015121011040920151202151855campanha-01-neo-glamour.jpg', 15, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(566, 1, '20151202152017folder-duda-molinos-final.jpg', 16, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(567, 1, '20170330125557cos1.JPG', 17, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(568, 1, '20170330125417cos9.JPG', 18, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(569, 1, '20151202152838vct5uc1tvhsoxxztx1r-brwmf9z1iiq6f7jxho3bqw4.jpeg', 19, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(570, 1, '201512021528383dv7on7elehw5kgref9jvevswklc2pwjmvlitrwvdao.jpeg', 20, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(571, 1, '20151202152838z4xq39hz5s3spbd6p2wcgxkvt7jvolsrx5kyf2igogi.jpeg', 21, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(572, 1, '20151202152839yet0pqnwpnrcluef-rkjp-9gfiv-gbncf0ygni8ezme.jpeg', 22, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(573, 1, '20160913175247dudu-bertholini-003.png', 23, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(574, 1, '20160721132154dudu-bertholini-001.png', 24, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(575, 1, '20160913175246pedro-nikelab-1200x801.jpg', 25, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(576, 1, '201609131752451514584-739631206121268-712873413422627785-n.jpg', 26, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(577, 1, '20160913175245nikexplairmaxsp.jpg', 27, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(578, 1, '20160913175245pedro02.jpg', 28, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(579, 1, '20160913175254pedrolorenco-lineup-72.jpg', 29, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(580, 1, '20170330124819costanza-pascolato.jpg', 30, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(581, 1, '20170330124210unknown-5.jpeg', 31, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(582, 1, '20161220142403whatsapp-image-2016-12-04-at-095523.jpeg', 32, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(583, 1, '20161220185408cp-azulejo-xicara-pires-08.jpg', 33, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(584, 1, '20161220185407cp-azulejo-prato-marcador-05.jpg', 34, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(585, 1, '20161220185418cp-azulejo-18-pcs-07.jpg', 35, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(586, 1, '20161220190211cp-orquideas-xicara-pires.jpg', 36, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(587, 1, '20161220185427cp-orquideas-prato-marcador-01.jpg', 37, '2017-03-30 15:56:36', '2017-03-30 15:56:36'),
(588, 1, '20161220185412cp-orquideas-18-pcs-03.jpg', 38, '2017-03-30 15:56:36', '2017-03-30 15:56:36');

-- --------------------------------------------------------

--
-- Estrutura da tabela `marca`
--

CREATE TABLE `marca` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `marca`
--

INSERT INTO `marca` (`id`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 'folder-logo.jpg', '0000-00-00 00:00:00', '2017-01-16 15:14:50');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_07_23_163409_create_about_table', 2),
('2015_07_23_163527_create_imprensa_table', 2),
('2015_07_23_163541_create_juridico_table', 2),
('2015_07_23_163624_create_creditos_table', 2),
('2015_07_27_135822_create_publicacoes_table', 3),
('2015_07_27_135840_create_publicacoes_imagens_table', 3),
('2015_07_27_140439_create_acao_social_table', 3),
('2015_07_27_140513_create_acao_social_imagens_table', 3),
('2015_07_27_140539_create_parcerias_table', 3),
('2015_07_27_140547_create_parcerias_imagens_table', 3),
('2015_07_27_140603_create_licenciamento_table', 3),
('2015_07_27_140610_create_licenciamento_imagens_table', 3),
('2015_07_27_160743_create_contato_table', 4),
('2015_07_29_121349_create_categorias_table', 5),
('2015_07_29_121404_create_subcategorias_table', 5),
('2015_07_30_095211_create_personalidades_table', 6),
('2015_07_30_141817_create_paralaxe_table', 7),
('2015_07_31_142616_create_portfolio_fotos', 8),
('2015_07_31_142633_create_portfolio_publicidade', 8),
('2015_07_31_142648_create_portfolio_capas', 8),
('2015_07_31_150213_create_portfolio_desfiles', 8),
('2015_08_02_221135_create_portfolio_videos_table', 9),
('2015_08_02_221156_create_portfolio_acao_social_table', 9),
('2015_08_02_221217_create_portfolio_redes_sociais_table', 9),
('2015_08_02_221516_create_portfolio_palestras_table', 9),
('2015_08_02_221539_create_portfolio_consultoria_moda_table', 9),
('2015_08_02_221602_create_portfolio_publicacoes_table', 9),
('2015_08_02_221620_create_portfolio_extras_table', 9),
('2015_08_04_102816_alter_categorias_table', 10);

-- --------------------------------------------------------

--
-- Estrutura da tabela `parcerias`
--

CREATE TABLE `parcerias` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `parcerias`
--

INSERT INTO `parcerias` (`id`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
(1, '<p><strong>Caico de Queiroz</strong> conta um um time <strong>exclusivo</strong> de <strong>artistas, influenciadores, formadores de opini&atilde;o,&nbsp;</strong>desenvolvemos trabalhos e parcerias em todas as plataformas de neg&oacute;cios, al&eacute;m de sermos uma <strong>c&eacute;lula conectada </strong>com todo o mercado de agenciamento nacional e internacional, desta forma somos &nbsp;<strong>+ &nbsp;plural</strong>&nbsp; e podemos ajudar &nbsp;voc&ecirc; a viabilizar o seu projeto, &nbsp;seja ele em <strong>Artes, Musicas, Esportes, Jornalismo, Gastronomia Media&ccedil;&otilde;es,</strong> <strong>Palestrantes, M&iacute;dias off line e on line, </strong>&nbsp;podemos buscar no mercado o profissional certo que venha atender o seu projeto.&nbsp;</p>\r\n\r\n<p>Iremos cuidar para voc&ecirc; de todo o processo de contrata&ccedil;&atilde;o, respeitando o <strong>briefing </strong>e o <strong>budget</strong>, atuando desde a elabora&ccedil;&atilde;o da proposta para contrata&ccedil;&atilde;o, cuidando das quest&otilde;es jur&iacute;dicas e&nbsp; acompanhando a execu&ccedil;&atilde;o da presta&ccedil;&atilde;o de servi&ccedil;os e&nbsp; do cumprimento dos contratos e obriga&ccedil;&otilde;es firmados, sempre visando garantir a seguran&ccedil;a e o sucesso das contrata&ccedil;&otilde;es.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p>Voc&ecirc;s ganham na agilidade, seguran&ccedil;a e n&atilde;o t&ecirc;m nenhum custo adicional para isso.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Convido voc&ecirc; a continuar navegando no nosso site.&nbsp;</p>\r\n', 'img-parcerias.jpg', '0000-00-00 00:00:00', '2015-12-01 16:31:30');

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_acao_social`
--

CREATE TABLE `portfolio_acao_social` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_personalidades_id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portfolio_acao_social`
--

INSERT INTO `portfolio_acao_social` (`id`, `portfolio_personalidades_id`, `titulo`, `imagem`, `text`, `ordem`, `created_at`, `updated_at`) VALUES
(892, 51, 'Think And Love', '20150820160731Acao Social_Pedro Andrade_Think love_1.png', '"Um país que não investe em educação não evolui"', 0, '2016-03-07 19:48:12', '2016-03-07 19:48:12'),
(893, 51, 'Brazil Foundatior ', '2015120112573420151123170152marcabasica-brazilfoundation-cor-980x646.jpg', '"Nenhuma organização arrecada tanto dinheiro fora do Brasil para organizações filantrópicas brasileiras quanto a Brazil Foundation, já trabalho com eles há pelo menos quatro anos e tenho o maior orgulho de fazer parte desta organização. Admiro o fato deles terem continuado esse trabalho na época em que o Brasil estava num momento promissor e também agora que o Brasil está passando por uma fase menos próspera, eles continuam na mesma batalha e seguem priorizando quem realmente precisa de ajuda neste país. Tenho orgulho de fazer parte disso"', 1, '2016-03-07 19:48:12', '2016-03-07 19:48:12'),
(894, 51, 'AmFAr ', '20151209221547pedro-amfar.jpeg', 'uma fundação de pesquisas para a prevenção contra o HIV, o tratamento, educação e procura da cura contra a AIDS. Desde 1985', 2, '2016-03-07 19:48:12', '2016-03-07 19:48:12'),
(918, 24, 'Doe Letras', '20150817132225Acao social_Astrid Fontenelle_1.PNG', '" Ler é importante para apredizado "', 0, '2016-03-17 01:34:46', '2016-03-17 01:34:46'),
(919, 24, 'Think and Love', '20150817132243Acao social_Astrid Fontenelle_think love.png', '"O meu mundo melhor é um mundo sem preconceito nem violência"', 1, '2016-03-17 01:34:46', '2016-03-17 01:34:46'),
(920, 24, 'Parto com Respeito ', '20151027181420astrid-epoca-campanha.jpg', 'Conheça seus direitos e faça a melhor escolha para você e seu bebê. #partocomrespeito', 2, '2016-03-17 01:34:46', '2016-03-17 01:34:46'),
(921, 24, 'Eles por Elas ', '20151027182142astrid-fontenelle.jpg', 'He for She ', 3, '2016-03-17 01:34:46', '2016-03-17 01:34:46'),
(922, 24, 'Olhar do Bem ', '20151027183507pop.jpg', 'Neste Outubro Rosa, junte-se a nós em um movimento contra a mortalidade por câncer de mama. O #olhardobem chegou para mudar o olhar das pessoas para a importância da conscientização e do diagnóstico precoce. Nos ajude a salvar vidas!', 4, '2016-03-17 01:34:46', '2016-03-17 01:34:46'),
(1085, 141, 'Instituto Socioambiental', '20160518144615instituto-socioambiental.JPG', 'O Instituto Socioambiental (ISA) é uma organização da sociedade civil brasileira, sem fins lucrativos, fundada em 1994, para propor soluções de forma integrada a questões sociais e ambientais com foco central na defesa de bens e direitos sociais, coletivos e difusos relativos ao meio ambiente, ao patrimônio cultural, aos direitos humanos e dos povos.', 0, '2016-06-23 14:55:25', '2016-06-23 14:55:25'),
(1116, 32, 'Think And Love', '20150819152616Acao social_Carla Lamarca_ think love.png', '"No meu mundo melhor não existe desrespeito, intolerância e desigualdade em nenhum lugar"', 0, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(1259, 152, 'Campanha #EucintoaVida', '20160805112057penelope-site.jpg', 'Sabe o que seria uma boa você fazer no carro, a partir de hoje? Mesmo quando sentar atrás, esteja à frente: USE O CINTO. (A gente acha que não pega nada, mas a real é que 75% das mortes em acidentes de carro podem ser evitadas simplesmente assim ', 0, '2016-12-23 03:26:00', '2016-12-23 03:26:00'),
(1260, 152, 'Feira de Adoção da A.A.A.C - Pedigree', '20160805112537sem-titulo-2.jpg', 'CAMPINAAAAAAAS! VEM BUSCAR?!? ', 1, '2016-12-23 03:26:00', '2016-12-23 03:26:00'),
(1267, 138, 'Instituto Ayrton Senna - Doe Educação', '20160505155926celso-insta-aaa.jpg', 'Eu apoio o Instituto Ayrton Senna, apoie você também #DoeEducação. Vamos todos levantar a bandeira da Educação e repetir este gesto que Ayrton Senna eternizou como símbolo de vitória e superação. Afinal, toda criança merece a chance de ser vencedora! Participe www.InstitutoAyrtonSenna.org.br/doe', 0, '2017-01-19 19:36:49', '2017-01-19 19:36:49'),
(1298, 146, 'Instituto Ayrton Senna - Doe Educação', '2016051215120020160505155926celso-insta-aaa.jpg', 'Eu apoio o Instituto Ayrton Senna, apoie você também #DoeEducação. Vamos todos levantar a bandeira da Educação e repetir este gesto que Ayrton Senna eternizou como símbolo de vitória e superação. Afinal, toda criança merece a chance de ser vencedora! Participe www.InstitutoAyrtonSenna.org.br/doe', 0, '2017-01-27 18:53:38', '2017-01-27 18:53:38'),
(1306, 173, 'Transparência Internacional, na luta contra a corrupção', '2016101820534214670767-1123223277731192-3557580059639217188-n.jpg', 'Especialista da rede mundial da entidade Transparência Internacional, na luta contra a corrupção', 0, '2017-01-27 20:43:07', '2017-01-27 20:43:07'),
(1307, 140, 'Instituto Socioambiental', '20160518144505instituto-socioambiental.JPG', 'O Instituto Socioambiental (ISA) é uma organização da sociedade civil brasileira, sem fins lucrativos, fundada em 1994, para propor soluções de forma integrada a questões sociais e ambientais com foco central na defesa de bens e direitos sociais, coletivos e difusos relativos ao meio ambiente, ao patrimônio cultural, aos direitos humanos e dos povos.', 0, '2017-01-27 21:07:56', '2017-01-27 21:07:56'),
(1313, 39, 'Think and Love', '20151023111342mov-0002-14-calendario-af2.jpg', '"O meu mundo melhor tem educação de qualidade para todos"', 0, '2017-01-27 21:27:16', '2017-01-27 21:27:16'),
(1323, 84, 'Think And Love', '20150821131417acao social_Silvia Poppovic_think love.png', '" Meu mundo melhor é um mundo com arte e cultura " (Silvia Poppovic)', 0, '2017-01-27 22:34:16', '2017-01-27 22:34:16'),
(1324, 44, 'Think And Love', '20150820152451acao social_Silvia Poppovic_think love.png', '"Meu mundo melhor é um mundo com arte e cultura"', 0, '2017-01-27 22:38:07', '2017-01-27 22:38:07'),
(1325, 44, 'ICCESP', '20151203142150sem-titulo-3.jpg', '"Certamente um dos maiores  e melhores centros de excelência na América Latina no tratamento do Câncer, voltado para toda a população carente e dedicado ao ensino e pesquisa de ponta em oncologia. Me orgulha fazer parte desse time!"', 1, '2017-01-27 22:38:07', '2017-01-27 22:38:07'),
(1326, 44, 'Artesol', '20151203142238logotipo-artesol-ong-parceria-bebeboom-compras-coletivas-doacao2.jpg', '"Artesanato Solidário, criado por Dona Ruth Cardoso, resgata e valoriza os saberes locais do artesanato de origem e promove a divulgação e  remuneração justa pelo trabalho do artesão tornando viável a sobrevivência de milhares de pessoas em suas comunidades.', 2, '2017-01-27 22:38:07', '2017-01-27 22:38:07'),
(1348, 103, '"Eu Cinto a Vida" - ONG Amigos do Trânsito', '20160517123120image12.PNG', '"Eu Cinto a Vida"  por que no banco traseiro também devemos usar o cinto de segurança. Assim, muitas muitas mortes serão evitadas.', 0, '2017-02-16 19:44:16', '2017-02-16 19:44:16'),
(1354, 38, 'Instituto Ayrton Senna', '20160505163154sem-titulo-1.jpg', 'Eu apoio o Instituto Ayrton Senna, apoie você também #DoeEducação. Vamos todos levantar a bandeira da Educação e repetir este gesto que Ayrton Senna eternizou como símbolo de vitória e superação. Afinal, toda criança merece a chance de ser vencedora! Participe www.InstitutoAyrtonSenna.org.br/doe', 0, '2017-02-21 13:41:12', '2017-02-21 13:41:12'),
(1355, 76, 'Instituto Ayrton Senna', '20160505163253sem-titulo-1.jpg', 'Eu apoio o Instituto Ayrton Senna, apoie você também #DoeEducação. Vamos todos levantar a bandeira da Educação e repetir este gesto que Ayrton Senna eternizou como símbolo de vitória e superação. Afinal, toda criança merece a chance de ser vencedora! Participe www.InstitutoAyrtonSenna.org.br/doe', 0, '2017-02-21 13:49:30', '2017-02-21 13:49:30'),
(1373, 156, 'Calendario Anjinho Feliz', '20161114172408vaelndo.jpg', 'A fundação da Ação Social Edmundo e Olga, mantenedora da Creche Comunitária Anjinho Feliz, foi uma atitude de fé, coragem, desprendimento e perseverança.', 0, '2017-03-10 21:35:31', '2017-03-10 21:35:31'),
(1397, 151, 'Mire No Alvo Certo ', '20170210195007unknown-6.jpeg', 'Valorize os Artistas E encare os Problemas do Brasil  ', 0, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(1437, 104, 'Eles Por Elas', '20151007175550maria-ribeiro-barbara-gancia.jpg', 'Ação Social para GNT', 0, '2017-04-26 17:36:35', '2017-04-26 17:36:35'),
(1446, 42, '"Eu Cinto a Vida" - ONG Amigos do Trânsito', '201605181053202.jpg', 'Voce sabia que o cinto de seguranca é obrigatorio no banco de trás( também) e pode reduzir em ate 75% o numero de mortes por acidente de transito no Brasil? Preserve a sua vida e de quem voce ama! ', 0, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(1451, 149, '#SOMOSTODOSCROMOSSOMO21 ', '20160524160657somos-todos-cromossomo-21.jpg', 'Tem por objetivo buscar igualdade para as pessoas com síndrome de down, inclusão que ultrapasse rótulos, diagnósticos e crenças limitantes. Virou filme e chega aos cinemas de todo Brasil no segundo semestre de 2016. Saulo Meneghetti é um dos padrinhos do projeto e faz parte do elenco do longa metragem. ', 0, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(1452, 149, 'ONG VIVA BEM - VIVA BEM A IDADE QUE TEM', '20160524160329viva-bem.jpg', 'Fundada em 1999 a ONG localizada em Ribeirão Pires - SP acolhe e atende idosos carentes, que tenham família ou não. A missão é promover bem estar social, físico, mental e emocional dos idosos carentes, visando qualidade de vida, respeito mutuo considerando todas as necessidades do ser humano, inclusive os de aspectos afetivo. Carinho e atenção são os pontos de partida para uma qualidade de vida diferenciada dos idosos', 1, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(1453, 149, 'PROJETO VIBRAR PARKINSON ', '20160524160120projeto-vibrar-com-parkinson.jpg', 'Divulgar e conscientizar a população a respeito da doença de Parkinson e informar a respeito dos tratamentos. O Parkinson é uma doença caracterizada pela perda progressiva da capacidade e habilidade motora e contrário do que a maioria das pessoas pensa, Parkinson também acomete pessoas jovens. ', 2, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(1454, 149, 'CAJEC - CASA JOSÉ EDUARDO CAVICHIO', '20160524160202cajec.jpg', 'Apoio a criança com câncer. Instituição filantrópica sem fins lucrativos, que desde 1996 assiste crianças e adolescentes com câncer de todo o Brasil e América Latina para realizar tratamento em hospitais na cidade de São Paulo. Oferece moradia transporte, seis refeições diárias, auxilio na compra de medicamentos, órteses e próteses. Proporciona tratamento fisioterapêutico e psicológico.  ', 3, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(1456, 40, 'Associação Brasileira de Amigos e Familiares de Portadores de Hipertensão Pulmonar (Abraf).', '2015111812134012243531-10205420504052257-2194683293075075992-n.jpg', '“Perca o fôlego pelos pacientes com hipertensão pulmonar” ', 0, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(1457, 40, 'Amfar', '20151126153856img-2402.JPG', ' American Foundation for Aids Research', 1, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(1458, 34, 'Think and Love', '20151023110913mov-0002-14-calendario-af.jpg', '"O meu mundo melhor é o que tem saúde para todo mundo"', 0, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(1459, 135, 'Think And Love', '20160119143333acao.jpg', '"O meu mundo melhor é o que tem saúde para todo mundo"', 0, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(1462, 150, 'Eu Amo Meus Peitos - Sociedade Brasileira de Mastologia ', '20160530122832pedro-bosnich.jpg', '"Campanha para alertar e pedir para que façam o auto exame. Homens também podem sofrer desta doença. Fique alerta!"', 0, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(1464, 102, 'Campanha Michael Kors contra a fome', '20151126145442lilian-pacce-camapanha-michael-kors.jpg', '" Hoje eu quero dar minha pequena contribuição para a linda campanha de Michael Kors contra a fome #watchhungerstop #michaelkors No site watchhungerstop.com você pode criar sua própria camiseta e marcar um amigo! Esse pequeno ato vai proporcionar 50 refeições para crianças carentes." Lilian Pacce', 0, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(1466, 191, 'Vice-Presidente da Associação Beneficente Elo Solidário.', '201703291249531097970-10201869350480333-832956360-n.jpg', 'Idealizada pelo ator Oswaldo Lot, a associação foi fundada em 2011 na cidade de Bauru (SP) e hoje tem ramificações em outras cidades brasileiras. O projeto nasceu com o objetivo de multiplicar as oportunidades para as pessoas, permitindo o acesso de todos quantos queiram e necessitem à educação, esporte, lazer e cultura. O projeto integra empresas privadas, organizações, classe artística e profissionais voluntários para capacitação e inclusão de jovens de comunidades carentes.', 0, '2017-05-22 19:17:48', '2017-05-22 19:17:48'),
(1470, 35, 'Instituto Ayrton Senna - Doe Educação', '20160510133137cica-doe-educaao.jpg', 'Há 30 anos, ao vencer a corrida de Detroit Ayrton Senna levantou a bandeira do Brasil, um símbolo de vitória e superação. Vamos todos fazer o mesmo apoiando a bandeira da Educação, afinal toda criança merece a chance de ser vencedora! DoeEducação entre no site www.institutoayrtonsenna.org.br/doe Eu e nossas crianças agradecemos!', 0, '2017-05-22 19:43:30', '2017-05-22 19:43:30'),
(1471, 35, 'Daniella Cicarelli', '20151026103931daniela-cicarelli-acao-social.jpg', 'Unidos pela prevenção', 1, '2017-05-22 19:43:30', '2017-05-22 19:43:30'),
(1475, 30, '"Eu Cinto a Vida" - ONG Trânsito Amigo', '20160517122457bbb.jpg', '"Vocês sabiam  que 75% das mortes de trânsito podem ser evitadas com o uso do cinto de segurança?', 0, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(1477, 176, 'Se Eu Disse Não É Estupro ', '20170320222823765112-255f4805ae2b4a83afbd1a040f35c133mv2.jpg', 'Campanha contra a violência contra a mulher - A revista Marie Claire convocou mulheres famosas e anônimas a publicarem suas fotos, nuas ou seminuas, nas redes sociais para mostrar que, quando uma mulher diz “não”, é “não”. ', 0, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(1478, 47, 'Eu Cinto a Vida - ONG Trânsito Amigo', '20160517115111tati.jpg', ' “Eu Cinto a Vida”, a fim de conscientizar e estimular o uso do cinto de segurança nos bancos traseiros.', 0, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(1485, 157, 'Greenpeace Brasil - Desmatamento Zero', '20161013110405erick-greenpeace.jpg', 'Levantamentos indicam que 2016 será um ano recorde de queimadas, assine a petição pelo desmatamento zero e faça parte do movimento para salvar as florestas. Quanto mais queimadas, pior nosso ar. www.desmatamentozero.org.br  #DesmatamentoZero #GreenpeaceBrasil', 0, '2017-06-26 13:48:53', '2017-06-26 13:48:53'),
(1487, 158, 'Greenpeace Brasil - Desmatamento Zero', '20161013110452erick-greenpeace.jpg', 'Levantamentos indicam que 2016 será um ano recorde de queimadas, assine a petição pelo desmatamento zero e faça parte do movimento para salvar as florestas. Quanto mais queimadas, pior nosso ar. www.desmatamentozero.org.br  #DesmatamentoZero #GreenpeaceBrasil', 0, '2017-06-28 18:48:51', '2017-06-28 18:48:51'),
(1490, 33, 'Ecoera', '20150819153132acao social_Chiara Gadaleta_Ecoera.jpg', '"Para nos do Ecoera a moda pode mudar vidas e ser um agente transformador"', 0, '2017-06-30 13:37:53', '2017-06-30 13:37:53'),
(1491, 33, 'Think and Love', '20151023112645mov-0002-14-calendario-af6.jpg', '"O meu mundo melhor é um mundo conectado com as questões sociais e ambientais"', 1, '2017-06-30 13:37:53', '2017-06-30 13:37:53'),
(1494, 55, 'Greenpeace Desmatamento Zero', '201611181811217760a37b-8c7a-4b9d-b8fe-868719031ea3.jpg', 'Nessa época do ano a Amazônia sofre com as queimadas. O fogo - na maioria das vezes provocado pelo homem - piora a qualidade de vida de quem vive alí e afeta também o clima do mundo inteiro. Há um ano o Greenpeace entregou no Congresso o PL pelo #DesmatamentoZero, que agora está aberto à consulta pública. É hora de agir para que as nossas florestas sejam protegidas para sempre. Então Desmantamento Zero, please!', 0, '2017-07-07 13:10:58', '2017-07-07 13:10:58'),
(1495, 55, 'Campanha Michael Kors contra a fome', '20151126150002lilian-pacce-camapanha-michael-kors.jpg', '" Hoje eu quero dar minha pequena contribuição para a linda campanha de Michael Kors contra a fome #watchhungerstop #michaelkors No site watchhungerstop.com você pode criar sua própria camiseta e marcar um amigo! Esse pequeno ato vai proporcionar 50 refeições para crianças carentes." Lilian Pacce', 1, '2017-07-07 13:10:58', '2017-07-07 13:10:58'),
(1496, 55, 'Violência Doméstica - #tambéméviolência (Artemis)', '20161013112306lilian-violencia.jpg', 'Há diferentes tipos de violência que calam e aprisionam milhões de mulheres diariamente no Brasil: violência psicológica, moral, patrimonial, física e sexual. Ainda não existem maneiras efetivas de denunciar agressores por violências silenciosas, que nem sempre deixam marcas visíveis.', 2, '2017-07-07 13:10:58', '2017-07-07 13:10:58'),
(1497, 92, 'Campanha Michal Kors', '20151028132032lilian-pacce-acao-social.jpg', '" Hoje eu quero dar minha pequena contribuição para a linda campanha de Michael Kors contra a fome #watchhungerstop #michaelkors No site watchhungerstop.com você pode criar sua própria camiseta e marcar um amigo! Esse pequeno ato vai proporcionar 50 refeições para crianças carentes." Lilian Pacce', 0, '2017-07-07 13:12:46', '2017-07-07 13:12:46'),
(1498, 92, 'Violência Doméstica - #tambéméviolência (Artemis)', '20161013112401lilian-violencia.jpg', 'Há diferentes tipos de violência que calam e aprisionam milhões de mulheres diariamente no Brasil: violência psicológica, moral, patrimonial, física e sexual. Ainda não existem maneiras efetivas de denunciar agressores por violências silenciosas, que nem sempre deixam marcas visíveis.', 1, '2017-07-07 13:12:46', '2017-07-07 13:12:46'),
(1499, 145, 'Violência Doméstica - #tambéméviolência (Artemis)', '20161013112505lilian-violencia.jpg', ' Há diferentes tipos de violência que calam e aprisionam milhões de mulheres diariamente no Brasil: violência psicológica, moral, patrimonial, física e sexual. Ainda não existem maneiras efetivas de denunciar agressores por violências silenciosas, que nem sempre deixam marcas visíveis.', 0, '2017-07-07 13:14:24', '2017-07-07 13:14:24'),
(1500, 49, 'Think And Love', '20150820155842acao social_Andre Vasco_think love.png', '" O meu mundo melhor começa com educação como algo prioritário em qualquer nação"', 0, '2017-07-07 13:48:52', '2017-07-07 13:48:52'),
(1501, 49, 'André Vasco - Teleton 2015', '20151027155301maxresdefault.jpg', 'Somos todos Teleton', 1, '2017-07-07 13:48:52', '2017-07-07 13:48:52'),
(1502, 49, ' Instituto Ayrton Senna - Doe Educação', '20160505160103fullsizerender.jpg', 'Eu apoio o Instituto Ayrton Senna, apoie você também #DoeEducação. Vamos todos levantar a bandeira da Educação e repetir este gesto que Ayrton Senna eternizou como símbolo de vitória e superação. Afinal, toda criança merece a chance de ser vencedora! Participe www.InstitutoAyrtonSenna.org.br/doe', 2, '2017-07-07 13:48:52', '2017-07-07 13:48:52'),
(1513, 29, 'Defensores da Amamentação', '20160329115641img-20160329-wa0002.jpg', '"Amamentei meus dois filhos, Bernardo e Clarice, e tenho muito orgulho disso. Sei a importância que a amamentação tem no desenvolvimento da criança e no fortalecimento do elo mãe-filho. Por isso, sou uma defensora da amamentação"', 0, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(1514, 29, 'Violência Doméstica - #tambéméviolência (Artemis)', '20161013112115mel-violencia.jpg', 'Há diferentes tipos de violência que calam e aprisionam milhões de mulheres diariamente no Brasil: violência psicológica, moral, patrimonial, física e sexual. Ainda não existem maneiras efetivas de denunciar agressores por violências silenciosas, que nem sempre deixam marcas visíveis.', 1, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(1515, 53, ' "VIVA O AMOR INCONDICIONAL"', '2016022419174510410288-966083326813766-2430153566700050930-n.jpg', '"Vamos combater qualquer tipo de preconceito, Homofobia,Transfobia,Lesbofobia .', 0, '2017-07-25 18:08:40', '2017-07-25 18:08:40');

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_capas`
--

CREATE TABLE `portfolio_capas` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_personalidades_id` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portfolio_capas`
--

INSERT INTO `portfolio_capas` (`id`, `portfolio_personalidades_id`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(3484, 71, '20160401115324303910.jpg', 0, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3485, 71, '20160314114442capa-camila-pitanga-revista-claudia.jpg', 1, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3486, 71, '20160203165758capa-revista-claudia-grazi-fev-16.jpg', 2, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3487, 71, '20151202173347schon19-s.jpg', 3, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3488, 71, '20151202175556untitled-1.jpg', 4, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3489, 71, '20151113173413img-20151113-wa0017.jpg', 5, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3490, 71, '20151204140829alinne-moraes-marie-claire.jpg', 6, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3491, 71, '20151027184015capa-bzart2.jpg', 7, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3492, 71, '20151207020223tumblr-nibknm7f0e1r40bzeo1-1280.jpg', 8, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3493, 71, '20151130133333ritalee3.jpg', 9, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3494, 71, '20151027184007capa-revista-marie-claire-opcao-2.jpg', 10, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3495, 71, '20151027184121585804-carolina-dieckmann-eacute-a-capa-da-950x0-1.jpg', 11, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3496, 71, '20151202174101capa-voguenoiva013capa.jpg', 12, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3497, 71, '20150821102836Capa de revista_Duda molinos_2.jpg', 13, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3498, 71, '20151202175110caparg75taisaraujocapa.jpg', 14, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3499, 71, '20151202173800631-abril-2014.jpg', 15, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3500, 71, '20151202175109caparg79fernandamottacapa.jpg', 16, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3501, 71, '20151207025556harpers-bazaar-janeiro-2014-dani-braga-interna.jpg', 17, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3502, 71, '201511301333191340787-5121-atm14.jpg', 18, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3503, 71, '20151202175051cover-8d05c28c-4e84-4d7f-a97f-7a86ac275ea3.jpg', 19, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3504, 71, '20151202173809page-1.jpg', 20, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3505, 71, '20151204134807capa21.jpg', 21, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3506, 71, '20151027183955capa-revista-marie-claire-grazi.jpg', 22, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3507, 71, '20151202173356577388-429096573814991-512105683-n-1.jpg', 23, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3508, 71, '20151207153324fernanda-young.jpg', 24, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3509, 71, '20151203112850tumblr-mqrjup6q1e1r40bzeo1-1280.jpg', 25, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3510, 71, '20151202175050duda-molinos-revista-iguatemi-ed-53-01.jpg', 26, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3511, 71, '20151203112855tumblr-mv4sdoimm71r40bzeo1-500.png', 27, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3512, 71, '20151204134807244768post-foto.jpg', 28, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3513, 71, '20151203112848tumblr-mknpuu4lam1r40bzeo1-500.jpg', 29, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3514, 71, '20151203112841revista-vogue-noivas.jpg', 30, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3515, 71, '20151203112842revista-marie-claire-jul-2015-grazi-01.jpg', 31, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3516, 71, '20151203112854revista-vogue-juliana-paes-01.jpg', 32, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3517, 71, '20151203112857revista-estilo-camila-pitanga-01.JPG', 33, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3518, 71, '20151204134801tumblr-ng2mw3wk8l1r40bzeo1-500.jpg', 34, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3519, 71, '20151207021252capa-hapers-bazzar.jpg', 35, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(3669, 32, '20150819152646Capa de revista_Carla Lamarca_3.png', 0, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(3670, 32, '20150819152640Capa de revista_Carla Lamraca_4.png', 1, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(3671, 32, '20150819152639Capa de revista_Carla Lamarca_1.png', 2, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(3672, 32, '20150819152649Capa de revista_Carla Lamarca_2.png', 3, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(3673, 32, '2015102700410819-carla-lamarca.jpg', 4, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(3674, 32, '20151027191946capa-81-carla-lamarca.jpg', 5, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(3801, 131, '20151217154230capa-lofficiel-brasil-dec-jan.jpg', 0, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(3802, 131, '20151217162019sem-titulo-2.jpg', 1, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(3803, 131, '20151217151454imagem-01.jpg', 2, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(3804, 131, '20151217162023sem-titulo-9.jpg', 3, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(3805, 131, '20151217154230renata-kuerten-fontana-trentini-and-goulart-by-andre-schiliro.jpg', 4, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(3806, 131, '2015121715145110446567-10152952162373443-2347718293037227844-n.jpg', 5, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(3807, 131, '20151217162021sem-titulo-4.jpg', 6, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(3808, 131, '20151217162021sem-titulo-3.jpg', 7, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(3809, 131, '20151217162027sem-titulo-14.jpg', 8, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(3810, 131, '20151217162022sem-titulo-6.jpg', 9, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(3811, 131, '20151217162022sem-titulo-5.jpg', 10, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(3812, 131, '20151217162026sem-titulo-11.jpg', 11, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(3813, 131, '20151217162028sem-titulo-13.jpg', 12, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(3814, 131, '20151217162027sem-titulo-12.jpg', 13, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(3815, 131, '20151217162028sem-titulo-10.jpg', 14, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(3816, 131, '20151217162028sem-titulo-8.jpg', 15, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(3866, 152, '20160621145720penelope-nova-capa-inked-f-005.jpg', 0, '2016-12-23 03:25:59', '2016-12-23 03:25:59'),
(3867, 152, '201606211457201-capa-sa.jpg', 1, '2016-12-23 03:25:59', '2016-12-23 03:25:59'),
(3868, 152, '20161206191831whatsapp-image-2016-12-04-at-095153.jpeg', 2, '2016-12-23 03:25:59', '2016-12-23 03:25:59'),
(3900, 39, '20150820144735Capa de revista_Luize Altenhofen_1.jpg', 0, '2017-01-27 21:27:16', '2017-01-27 21:27:16'),
(3901, 39, '20150820144736Capa de revista+Luize Altenhofen_4.jpg', 1, '2017-01-27 21:27:16', '2017-01-27 21:27:16'),
(3902, 39, '20150820144736Capa de revista_Luize Altenhofen_3.jpg', 2, '2017-01-27 21:27:16', '2017-01-27 21:27:16'),
(3903, 39, '20150820144734Capa de revista_Luize Altenhofen_2.jpg', 3, '2017-01-27 21:27:16', '2017-01-27 21:27:16'),
(3904, 39, '20151202163004capa.jpg', 4, '2017-01-27 21:27:16', '2017-01-27 21:27:16'),
(3915, 87, '20160602132013capa-revista-isto-e.jpg', 0, '2017-01-27 21:55:06', '2017-01-27 21:55:06'),
(3916, 78, '20151029103531capa-mag-nova-sem-preco-low-654x860.jpg', 0, '2017-01-27 21:57:58', '2017-01-27 21:57:58'),
(3917, 78, '20160105152838sem-titulo-1.jpg', 1, '2017-01-27 21:57:58', '2017-01-27 21:57:58'),
(3928, 31, '20150921134612Capa_ed80-baixa.jpg', 0, '2017-01-27 22:52:05', '2017-01-27 22:52:05'),
(3938, 65, '20160224153307guga-tratada.jpg', 0, '2017-01-27 23:11:20', '2017-01-27 23:11:20'),
(3939, 155, '20160725111802images.jpeg', 0, '2017-01-30 13:12:34', '2017-01-30 13:12:34'),
(3990, 19, '20151210130519img-20151210-wa0012.jpg', 0, '2017-03-14 20:49:42', '2017-03-14 20:49:42'),
(3991, 19, '20161221125328whatsapp-image-2016-12-04-at-095700.jpeg', 1, '2017-03-14 20:49:42', '2017-03-14 20:49:42'),
(4069, 42, '20150820150255Capa de revista_Marilia Moreno_3.jpg', 0, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(4070, 42, '20150820150407Capa-de-revista_Marilia-Moreno_2.jpg', 1, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(4071, 42, '20150820150255Capa de revista_Marilia Moreno_1.jpg', 2, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(4076, 149, '201608041214032016-08-04-photo-00000008.jpg', 0, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(4077, 40, '20151104162207marcelle-bittar-para-rev-made-por-gustavo-zylberstajn-226.jpg', 0, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(4078, 40, '20150921172413Cópia de IMG_0189.jpg', 1, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(4079, 40, '2015090912505911954545_10205027208220107_574907204409803296_n.jpg', 2, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(4080, 40, '20150820145134Capa de revista_Marcelle Bittar_3.jpg', 3, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(4081, 40, '20150820145138Capa de revista_Marcelle Bittar_4.jpg', 4, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(4082, 40, '20150820145128Capa de revista_Marcelle Bittar_1.jpg', 5, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(4083, 40, '20150820145128Capa de revista_Marcelle Bittar_2.jpg', 6, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(4086, 112, '20170517153138capa-rsvp.jpg', 0, '2017-05-17 18:32:22', '2017-05-17 18:32:22'),
(4087, 112, '20170517153131capa-profashional.jpg', 1, '2017-05-17 18:32:22', '2017-05-17 18:32:22'),
(4088, 34, '20151123213750ffwmag-costanza-pascolato-carol-trentini-mari-w.jpg', 0, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(4089, 34, '20170215094632gps-costanza.jpg', 1, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(4090, 34, '20151123213750costanza0001-794x1024.jpg', 2, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(4091, 34, '20151123215338confidencial-costanza-pascolato-05.jpg', 3, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(4092, 135, '20160119143144capas1.jpg', 0, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(4093, 135, '20170215094729gps-costanza.jpg', 1, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(4094, 135, '20160119143149capas-2.jpg', 2, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(4095, 135, '20160119143152capas3.jpg', 3, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(4103, 102, '20170411164421img-8410.PNG', 0, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(4106, 186, '20170214194725captura-de-tela-2017-02-14-as-155340.png', 0, '2017-05-22 18:19:01', '2017-05-22 18:19:01'),
(4112, 35, '20151124173138capa-de-revista-daniella-cicarelli-3.jpg', 0, '2017-05-22 19:43:30', '2017-05-22 19:43:30'),
(4113, 35, '20151124173139capa-de-revista-daniella-cicarelli-2.jpg', 1, '2017-05-22 19:43:30', '2017-05-22 19:43:30'),
(4114, 35, '20151124173141capa-de-revista-daniella-cicarelli-1.jpg', 2, '2017-05-22 19:43:30', '2017-05-22 19:43:30'),
(4133, 30, '20150819151656capa de revista_Carol Scaff_1.jpg', 0, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(4134, 30, '20150819151704capa de revista_Carol Scaff_2.jpg', 1, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(4141, 176, '20170321120117isabella-santoni-glamour.jpg', 0, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(4142, 176, '20170321120047unknown-15.jpeg', 1, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(4143, 47, '20150820154517Capa de revista_Tatiana Dumenti.jpg', 0, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(4144, 47, '20151210142939tatiana.jpg', 1, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(4145, 170, '20161004183704vogue-rg-capa-seu-jorge-2006-claro.jpg', 0, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(4146, 170, '20161004183702tpm-35-agosto-2004.jpg', 1, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(4147, 170, '20161004183659capa-raca-2006-vanessa-da-mata.jpg', 2, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(4148, 170, '20161004183700docu0012-trip.jpg', 3, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(4149, 170, '20161004183700img-6252kiko-ferrite.jpg', 4, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(4150, 170, '20161004183702docu0032-boaf.jpg', 5, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(4151, 170, '20161004183702img-6380kiko-ferrite.jpg', 6, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(4152, 170, '20161004183700img-6256kiko-ferrite.jpg', 7, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(4153, 170, '20161004183703tpm-36-setembro-2004.jpg', 8, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(4154, 170, '20161004183704vogue-kids-brasil-verao-2008-2009.jpg', 9, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(4155, 56, '20160602132151capa-revista-isto-e.jpg', 0, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(4156, 56, '20160913165659revista-tam-nas-nuvens-pedro-lourenco-10860-mlb20035169696-012014-f.jpg', 1, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(4157, 56, '20160913165701capa-download2.jpg', 2, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(4168, 45, '20150929140733tpm100-capinhas-155.jpg', 0, '2017-06-26 17:06:04', '2017-06-26 17:06:04'),
(4169, 45, '20150929135413Capa_corpo_a_Corpo.jpg', 1, '2017-06-26 17:06:04', '2017-06-26 17:06:04'),
(4170, 45, '201509291354139aqrcfsizobebos.jpg', 2, '2017-06-26 17:06:04', '2017-06-26 17:06:04'),
(4171, 45, '20150929133935Cópia de Digitalizar0009.jpg', 3, '2017-06-26 17:06:04', '2017-06-26 17:06:04'),
(4185, 33, '20151111155040img-3329.jpg', 0, '2017-06-30 13:37:53', '2017-06-30 13:37:53'),
(4217, 55, '20170411163902img-8410.PNG', 0, '2017-07-07 13:10:58', '2017-07-07 13:10:58'),
(4218, 70, '20170707104025capa-monica-iozzi-julho-2017.jpeg', 0, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(4219, 70, '20160321085951cosmo-marco-carolcastrocapa.jpg', 1, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(4220, 70, '20150821102051Capa de revista_Vanessa Rozan_2.jpg', 2, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(4221, 70, '20150821102051Capa de revista_Vanessa Rozan_1.jpg', 3, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(4222, 70, '20151027124705capa-ed86-v8.jpg', 4, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(4223, 26, '20170707104128whatsapp-image-2017-06-28-at-93347-pm.jpeg', 0, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(4224, 26, '20150819144136Capa de revista_Camila alves_1.jpg', 1, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(4225, 26, '20150819144136Capa de revista_Camila Alves_4.jpg', 2, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(4226, 26, '20150819144136Capa de revista_Camila Alves_3.jpg', 3, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(4227, 26, '20151116125938camila-alves.jpg', 4, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(4228, 26, '20151027002020slxqw7iga9nqqx7q.jpg', 5, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(4229, 26, '20150819144136Capa de revista_Camila Alves_2.jpg', 6, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(4230, 26, '20151027002024unknown-1.jpeg', 7, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(4231, 208, '20170704235739caparenataceribelli.jpg', 0, '2017-07-10 17:11:12', '2017-07-10 17:11:12'),
(4232, 208, '20170704235739maxima.jpeg', 1, '2017-07-10 17:11:12', '2017-07-10 17:11:12'),
(4233, 208, '20170704235739viver-maio.jpg', 2, '2017-07-10 17:11:12', '2017-07-10 17:11:12'),
(4234, 208, '20170704235739capa-renata.JPG', 3, '2017-07-10 17:11:12', '2017-07-10 17:11:12'),
(4235, 77, '20151111155126img-3329.jpg', 0, '2017-07-12 20:16:07', '2017-07-12 20:16:07'),
(4246, 89, '20151027124812colocar.jpeg', 0, '2017-07-20 13:35:13', '2017-07-20 13:35:13'),
(4250, 52, '20151027124725colocar.jpeg', 0, '2017-07-20 14:01:07', '2017-07-20 14:01:07'),
(4251, 52, '20151015150424capajpg', 1, '2017-07-20 14:01:07', '2017-07-20 14:01:07'),
(4252, 52, '2015101515044802jpg', 2, '2017-07-20 14:01:07', '2017-07-20 14:01:07'),
(4253, 58, '20151201171206poder.jpg', 0, '2017-07-20 14:20:45', '2017-07-20 14:20:45'),
(4254, 58, '20151201171206kaza.jpg', 1, '2017-07-20 14:20:45', '2017-07-20 14:20:45'),
(4255, 58, '20151201171205bravo.jpg', 2, '2017-07-20 14:20:45', '2017-07-20 14:20:45'),
(4256, 58, '20151201171206gastro.jpg', 3, '2017-07-20 14:20:45', '2017-07-20 14:20:45'),
(4257, 58, '20151201171206spepoca.jpg', 4, '2017-07-20 14:20:45', '2017-07-20 14:20:45'),
(4260, 207, '20170630175803img-1014510-capa-edicao-80-lobao-1.jpg', 0, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(4266, 29, '20151026152322mel-lisboa-rev-inked-03.jpg', 0, '2017-07-24 23:49:28', '2017-07-24 23:49:28');

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_categorias`
--

CREATE TABLE `portfolio_categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `classe_menu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portfolio_categorias`
--

INSERT INTO `portfolio_categorias` (`id`, `titulo`, `slug`, `classe_menu`, `ordem`, `created_at`, `updated_at`) VALUES
(1, 'Apresentadores', 'apresentadores', 'link-azul-claro', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Atores', 'atores', 'link-azul', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Design | Arquitetura', 'design-e-arquitetura', 'link-abacate', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Moda', 'moda', 'link-pink', 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Jornalistas', 'jornalistas', 'link-laranja', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Influenciadores | Criadores', 'influenciadores-criadores', 'link-bege', 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Make Up | Hair', 'make-up-e-hair', 'link-vermelho', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Palestrantes | Workshops', 'palestrantes-e-workshops', 'link-rosa', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Fotógrafos', 'fotografos', '', 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Gastronomia', 'gastronomia', 'link-ouro', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Produção Executiva', 'producao-executiva', '', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Escritores', 'escritores', 'link-amarelo', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Bandas | Músicos', 'bandas-musicas', 'link-verde', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_consultoria_moda`
--

CREATE TABLE `portfolio_consultoria_moda` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_personalidades_id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portfolio_consultoria_moda`
--

INSERT INTO `portfolio_consultoria_moda` (`id`, `portfolio_personalidades_id`, `texto`, `created_at`, `updated_at`) VALUES
(39, 112, '<p>Dirigiu as revistas Moda (na Folha de S.Paulo), KEY e Plastic Dreams, e entre 2011 e 2014 &uacute;ltimos foi publisher e Diretora de Reda&ccedil;&atilde;o dos t&iacute;tulos da revista francesa L&#39;Officiel no Brasil (L&#39;Officiel feminina, L&#39;Officiel Hommes e L&#39;Officiel Voyage), foi colunista da Vogue, editora de moda da Revista da Folha, dirigiu o SPFW Journal, foi editora de moda do portal FFW (onde comentava ao vivo os desfiles do S&atilde;o Paulo Fashion Week), al&eacute;m de j&aacute; ter contribu&iacute;do para as mais importantes publica&ccedil;&otilde;es do pa&iacute;s.&nbsp;</p>\r\n\r\n<p>J&aacute; prestou consultorias para empresas como Botic&aacute;rio (maquiagem), Vult / Duda Molinos (maquiagem), Natura, Brastemp e shopping Iguatemi.&nbsp;</p>\r\n\r\n<p>H&aacute; 15 anos &eacute; consultora da Melissa, marca da Grendene, participando desde a elabora&ccedil;&atilde;o dos temas de cada cole&ccedil;&atilde;o at&eacute; a materializa&ccedil;&atilde;o deles em projetos especiais, tais como a concep&ccedil;&atilde;o de eventos, livros, exposi&ccedil;&otilde;es e a dire&ccedil;&atilde;o dos desfiles da marca, como o dos temas Nation (no SPFW) e Wanna Be Carioca, realizado no Rio de Janeiro (verao 2016).</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-05-17 18:32:22', '2017-05-17 18:32:22'),
(41, 147, '<p>Consultoria que identifica e adequa as necessidades pessoais e profissionais da cliente respeitando a sua personalidade e estilo de vida, trabalhando os seguintes pontos:</p>\r\n\r\n<ul>\r\n	<li>As cores, texturas e tecidos que mais valorizam seu tipo f&iacute;sico.</li>\r\n	<li>\r\n	<p>O visual adequado para cada ocasi&atilde;o.</p>\r\n	</li>\r\n	<li>\r\n	<p>Como ter um guarda roupa inteligente e coordenado.</p>\r\n	</li>\r\n	<li>\r\n	<p>Como comprar de forma mais direcionada.</p>\r\n	</li>\r\n	<li>\r\n	<p>Como escolher melhor os acess&oacute;rios.</p>\r\n	</li>\r\n	<li>\r\n	<p>Dicas de melhor corte de cabelo e maquiagem.</p>\r\n\r\n	<p>No final do processo, a cliente recebe um dossi&ecirc; personalizado, documentando todos os pontos trabalhados, com refer&ecirc;ncias, fotos dos looks montados e recomenda&ccedil;&otilde;es.</p>\r\n	</li>\r\n</ul>\r\n', '2017-07-25 13:43:56', '2017-07-25 13:43:56');

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_creditos`
--

CREATE TABLE `portfolio_creditos` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_personalidades_id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portfolio_creditos`
--

INSERT INTO `portfolio_creditos` (`id`, `portfolio_personalidades_id`, `texto`, `created_at`, `updated_at`) VALUES
(694, 57, '<p><strong><em><u>Agradecimentos:</u></em></strong></p>\r\n\r\n<ul>\r\n	<li>SPFW</li>\r\n</ul>\r\n', '2015-12-04 17:54:18', '2015-12-04 17:54:18'),
(1056, 51, '<p><u><strong>Agradecimentos:</strong></u></p>\r\n\r\n<ul>\r\n	<li>Canal Globonews</li>\r\n	<li>Canal GNT</li>\r\n	<li>Canal Fusion&nbsp;</li>\r\n	<li>TV Globo</li>\r\n	<li>In Press Porter Novelli</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '2016-03-07 19:48:12', '2016-03-07 19:48:12'),
(1079, 24, '<p><em><strong><u>Agradecimentos&nbsp;</u></strong></em></p>\r\n\r\n<ul>\r\n	<li><strong>&nbsp;Canal GNT</strong></li>\r\n	<li><strong>Canal MTV</strong></li>\r\n	<li><strong>Kapo</strong></li>\r\n	<li><strong>Coca Cola</strong></li>\r\n	<li><strong>Laborat&oacute;rio GSK</strong></li>\r\n	<li><strong>In Press Porter Novelli</strong></li>\r\n	<li><strong>Gui Paganini</strong></li>\r\n	<li><strong>Eliana Rodrigues&nbsp;</strong></li>\r\n</ul>\r\n', '2016-03-17 01:34:46', '2016-03-17 01:34:46'),
(1111, 97, '<p><u><em><strong>Agradecimentos</strong></em></u>:</p>\r\n\r\n<ul>\r\n	<li>R&aacute;dio Jovem Pan</li>\r\n	<li>Canal J&aacute; Pensou Nisso</li>\r\n	<li>Marki Lehto</li>\r\n</ul>\r\n', '2016-03-17 19:59:33', '2016-03-17 19:59:33'),
(1189, 66, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Canal Jovem Pan</li>\r\n	<li>Canal Marcio Mussarella</li>\r\n	<li>Canal Mini TV</li>\r\n	<li>Canal Todo Seu</li>\r\n	<li>Canal Turma do Bem</li>\r\n	<li>TV Globo</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '2016-04-18 18:35:56', '2016-04-18 18:35:56'),
(1204, 61, '<p><strong><em><u>Agradecimentos:</u></em></strong></p>\r\n\r\n<ul>\r\n	<li>Canal Comer O qu&ecirc;?&nbsp;</li>\r\n</ul>\r\n', '2016-04-26 21:00:21', '2016-04-26 21:00:21'),
(1214, 71, '<p><strong><em><u>Agradecimentos:</u></em></strong></p>\r\n\r\n<ul>\r\n	<li>Canal Pantene Brasil&nbsp;</li>\r\n	<li>Canal Vivara</li>\r\n	<li>Canal Avon</li>\r\n	<li>Canal Riachuello</li>\r\n	<li>Canal Caixa Economica Federal&nbsp;</li>\r\n	<li>Canal Loreal Brasil&nbsp;</li>\r\n	<li>Takla Produ&ccedil;&otilde;es&nbsp;Art&iacute;sticas</li>\r\n	<li>O2 Filmes&nbsp;</li>\r\n</ul>\r\n', '2016-05-04 18:54:30', '2016-05-04 18:54:30'),
(1252, 139, '', '2016-05-24 18:09:58', '2016-05-24 18:09:58'),
(1330, 32, '<p><u><strong>Agradecimentos</strong></u>:</p>\r\n\r\n<ul>\r\n	<li>Canal Andr&eacute; Peniche</li>\r\n	<li>Canal Ig</li>\r\n	<li>Canal MTV</li>\r\n	<li>Canal Vapt Filmes</li>\r\n	<li>Monster Movie</li>\r\n	<li>TV Globo</li>\r\n</ul>\r\n', '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(1375, 91, '<p><u><em><strong>Agradecimentos</strong></em></u>:</p>\r\n\r\n<ul>\r\n	<li>Canal TNT</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '2016-09-20 19:38:47', '2016-09-20 19:38:47'),
(1397, 82, '<p><u><em><strong>Agradecimentos</strong></em></u>:</p>\r\n\r\n<ul>\r\n	<li>&nbsp;Canal GNT</li>\r\n	<li>Canal TV Folha</li>\r\n	<li>Canal Sem Filtro</li>\r\n	<li>R&aacute;dio BandNews FM</li>\r\n	<li>In Press Porter Novelli</li>\r\n</ul>\r\n', '2016-10-13 13:29:50', '2016-10-13 13:29:50'),
(1427, 124, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>TV Record</li>\r\n</ul>\r\n', '2016-11-16 17:43:37', '2016-11-16 17:43:37'),
(1467, 152, '<p>F&aacute;bio Tieri</p>\r\n', '2016-12-23 03:26:00', '2016-12-23 03:26:00'),
(1495, 90, '<p><em><u><strong>Agradecimentos:</strong></u></em></p>\r\n\r\n<ul>\r\n	<li>Canal Glamour Brasil</li>\r\n	<li>Roberto Leone</li>\r\n</ul>\r\n', '2017-01-27 18:57:06', '2017-01-27 18:57:06'),
(1502, 63, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Canal TNT</li>\r\n</ul>\r\n', '2017-01-27 19:29:19', '2017-01-27 19:29:19'),
(1506, 173, '<ul>\r\n	<li>El Pais&nbsp;</li>\r\n	<li>HoffPost&nbsp;</li>\r\n	<li>Companhia Das Letras&nbsp;</li>\r\n	<li>Objetiva&nbsp;</li>\r\n	<li>Estad&atilde;o&nbsp;</li>\r\n</ul>\r\n', '2017-01-27 20:43:07', '2017-01-27 20:43:07'),
(1507, 99, '<p>Agradecimento</p>\r\n\r\n<p>Canal Folha Comida&nbsp;</p>\r\n', '2017-01-27 21:06:29', '2017-01-27 21:06:29'),
(1513, 100, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Canal MTV Brasil&nbsp;</li>\r\n	<li>Lojas Renner&nbsp;</li>\r\n	<li>&nbsp;Cleiby Trevisan</li>\r\n</ul>\r\n', '2017-01-27 21:22:59', '2017-01-27 21:22:59'),
(1514, 39, '<p><strong><em><u>Agradecimentos:</u></em></strong></p>\r\n\r\n<ul>\r\n	<li>Fotografa - Regina Torchia&nbsp;</li>\r\n</ul>\r\n', '2017-01-27 21:27:16', '2017-01-27 21:27:16'),
(1519, 137, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Fotos: Roberto Leone</li>\r\n</ul>\r\n', '2017-01-27 21:35:23', '2017-01-27 21:35:23'),
(1522, 87, '<p><strong><em><u>Agradecimentos:</u></em></strong></p>\r\n\r\n<ul>\r\n	<li>Canal MAC</li>\r\n	<li>Canal Nike</li>\r\n</ul>\r\n', '2017-01-27 21:55:06', '2017-01-27 21:55:06'),
(1523, 78, '<p><strong><em><u>Agradecimentos:</u></em></strong></p>\r\n\r\n<ul>\r\n	<li>Canal Cavalera</li>\r\n	<li>Revista Mag</li>\r\n	<li>Enjoy</li>\r\n</ul>\r\n', '2017-01-27 21:57:58', '2017-01-27 21:57:58'),
(1525, 68, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>TV Globo</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-01-27 22:14:14', '2017-01-27 22:14:14'),
(1529, 84, '<p><u><em><strong>Agradecimentos</strong></em></u>:</p>\r\n\r\n<ul>\r\n	<li>R&aacute;dio Jovem Pan</li>\r\n	<li>Canal J&aacute; Pensou Nisso</li>\r\n	<li>Marki Lehto</li>\r\n</ul>\r\n', '2017-01-27 22:34:17', '2017-01-27 22:34:17'),
(1530, 44, '<p><u><strong>Agradecimentos:</strong></u></p>\r\n\r\n<ul>\r\n	<li>Canal J&aacute; Pensou Nisso</li>\r\n	<li>Marki Lehto</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-01-27 22:38:07', '2017-01-27 22:38:07'),
(1531, 31, '<p>&nbsp;<strong><em><u>Agradecimentos</u></em></strong></p>\r\n\r\n<ul>\r\n	<li>Canal&nbsp; GNT</li>\r\n	<li>Rede Glogo de Televis&atilde;o</li>\r\n	<li>Canal TV Nube</li>\r\n	<li>In Press Porter Novelli</li>\r\n	<li>Eliana Rodrigues</li>\r\n</ul>\r\n', '2017-01-27 22:52:05', '2017-01-27 22:52:05'),
(1532, 136, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Fotos: Roberto Leone</li>\r\n</ul>\r\n', '2017-01-27 22:55:14', '2017-01-27 22:55:14'),
(1541, 65, '<p><u><strong>Agradecimentos:</strong></u></p>\r\n\r\n<ul>\r\n	<li>Canal Globonews</li>\r\n	<li>Canal TV Estad&atilde;o</li>\r\n	<li>Gustavo Malheiros</li>\r\n</ul>\r\n', '2017-01-27 23:11:20', '2017-01-27 23:11:20'),
(1542, 95, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Canal Maybelline</li>\r\n	<li>Canal SBT</li>\r\n</ul>\r\n', '2017-01-27 23:13:36', '2017-01-27 23:13:36'),
(1543, 64, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>TV Globo</li>\r\n</ul>\r\n', '2017-01-27 23:21:56', '2017-01-27 23:21:56'),
(1561, 154, '<p>Tv GAzeta</p>\r\n\r\n<p>Todo Seu&nbsp;</p>\r\n\r\n<p>A Semana&nbsp;</p>\r\n', '2017-02-09 18:24:01', '2017-02-09 18:24:01'),
(1567, 23, '<p><strong><em><u>Agradecimentos:</u></em></strong></p>\r\n\r\n<ul>\r\n	<li>Canal Amanda Ramalho</li>\r\n	<li>Canal R&aacute;dio Jovem Pan</li>\r\n	<li>Programa P&acirc;nico na Band&nbsp;- Rede Bandeirantes de Televis&atilde;o</li>\r\n</ul>\r\n', '2017-02-16 19:43:48', '2017-02-16 19:43:48'),
(1568, 103, '<p><strong><u>Agradecimentos:</u></strong></p>\r\n\r\n<ul>\r\n	<li>Canal Amanda Ramalho</li>\r\n	<li>Canal Programa P&acirc;nico na&nbsp;Band&nbsp;&nbsp;- Rede Bandeirantes de Televis&atilde;o</li>\r\n	<li>Programa P&acirc;nico&nbsp; - &nbsp;Radio Jovem Pan&nbsp;</li>\r\n</ul>\r\n', '2017-02-16 19:44:16', '2017-02-16 19:44:16'),
(1569, 129, '<p><u><em><strong>Agradecimentos</strong></em></u>:</p>\r\n\r\n<ul>\r\n	<li>Canal Multishow</li>\r\n	<li>Canal Martha Kiss</li>\r\n</ul>\r\n', '2017-02-20 03:13:01', '2017-02-20 03:13:01'),
(1572, 38, '<p><u><strong>Agradecimentos</strong></u></p>\r\n\r\n<ul>\r\n	<li>TV Bandeirantes</li>\r\n	<li>Radio Jovem Pan&nbsp;</li>\r\n	<li>Carlo Locatelli</li>\r\n</ul>\r\n', '2017-02-21 13:41:12', '2017-02-21 13:41:12'),
(1573, 76, '<p><u><strong>Agradecimentos:</strong></u></p>\r\n\r\n<ul><br />\r\n	<li>Radio Jovem Pan&nbsp;</li>\r\n	<li>Carlo Locatelli&nbsp;&nbsp;</li>\r\n</ul>\r\n', '2017-02-21 13:49:30', '2017-02-21 13:49:30'),
(1603, 104, '<p><strong><em><u>Agradecimentos:</u></em></strong></p>\r\n\r\n<ul>\r\n	<li>Canal &nbsp;GNT</li>\r\n	<li>In Press Porter Novelli</li>\r\n</ul>\r\n', '2017-04-26 17:36:35', '2017-04-26 17:36:35'),
(1611, 132, '<p><em><u><strong>Agradecimentos:</strong></u></em></p>\r\n\r\n<ul>\r\n	<li>Canal Fox Play Brasil</li>\r\n	<li>Canal Sib&eacute;ria</li>\r\n	<li>TV Globo</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-05-02 20:46:33', '2017-05-02 20:46:33'),
(1615, 42, '<p><strong><em><u>Agradecimentos:</u></em></strong></p>\r\n\r\n<ul>\r\n	<li>Canal Sony</li>\r\n	<li>Revista Shape</li>\r\n	<li>Revista Vanidades</li>\r\n	<li>Bare Minerals</li>\r\n	<li>Mercedes Benz</li>\r\n	<li>Prada</li>\r\n</ul>\r\n', '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(1616, 54, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Donata Meirelles</li>\r\n	<li>Canal GNT</li>\r\n</ul>\r\n', '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(1617, 126, '<p><strong><em><u>Agradecimentos:</u></em></strong></p>\r\n\r\n<ul>\r\n	<li>Programa Morning Show -&nbsp;R&aacute;dio Jovem Pan</li>\r\n</ul>\r\n', '2017-05-12 17:15:26', '2017-05-12 17:15:26'),
(1621, 40, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>SPFW</li>\r\n	<li>Revista Glamurama</li>\r\n	<li>Revista Mag</li>\r\n	<li>Revista Style</li>\r\n	<li>Revista Vogue</li>\r\n	<li>Revista Studio W</li>\r\n	<li>Arezzo</li>\r\n	<li>Joias Guerreiro</li>\r\n	<li>Henrique Gendre&nbsp;</li>\r\n</ul>\r\n', '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(1623, 112, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Canal TNT</li>\r\n	<li>Revista L&acute;Oficciel</li>\r\n	<li>Grendene - Melissa</li>\r\n</ul>\r\n', '2017-05-17 18:32:22', '2017-05-17 18:32:22'),
(1625, 75, '<p><u><strong>Agradecimentos:</strong></u></p>\r\n\r\n<ul>\r\n	<li>R&aacute;dio BandNews FM</li>\r\n</ul>\r\n', '2017-05-17 18:47:46', '2017-05-17 18:47:46'),
(1626, 79, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Canal GNT</li>\r\n	<li>In Press Porter Novelli</li>\r\n</ul>\r\n', '2017-05-17 18:49:24', '2017-05-17 18:49:24'),
(1627, 60, '<p><u><em><strong>Agradecimentos</strong></em></u>:</p>\r\n\r\n<ul>\r\n	<li>Canal GNT</li>\r\n	<li>In Press Porter Novelli</li>\r\n</ul>\r\n', '2017-05-17 18:51:50', '2017-05-17 18:51:50'),
(1628, 111, '<p><strong><em><u>Agradecimentos:</u></em></strong></p>\r\n\r\n<ul>\r\n	<li>Canal GNT</li>\r\n	<li>In Press Porter Novelli</li>\r\n</ul>\r\n', '2017-05-17 18:52:48', '2017-05-17 18:52:48'),
(1629, 125, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>R&aacute;dio Jovem Pan</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-05-17 18:55:10', '2017-05-17 18:55:10'),
(1631, 34, '<p><u><strong>Agradecimentos:</strong></u></p>\r\n\r\n<ul>\r\n	<li>Canal Glamour Brasil</li>\r\n	<li>Revista Vogue&nbsp;</li>\r\n	<li>Revista Mag</li>\r\n	<li>Revista Glamour&nbsp;</li>\r\n	<li>Revista Expressions</li>\r\n	<li>Havaianas</li>\r\n	<li>Chilli Beans</li>\r\n	<li>Consuelo Blocker&nbsp;</li>\r\n	<li>Roberto Leone</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(1632, 135, '<p><strong><u>Agradecimentos:</u></strong></p>\r\n\r\n<ul>\r\n	<li>Canal Glamour Brasil</li>\r\n	<li>Revista Vogue&nbsp;</li>\r\n	<li>Revista Mag</li>\r\n	<li>Revista Glamour&nbsp;</li>\r\n	<li>Revista Expressions<!--  --></li>\r\n	<li>Havaianas</li>\r\n	<li>Chilli Beans</li>\r\n	<li>Consuelo Blocker&nbsp;</li>\r\n	<li>Roberto Leone</li>\r\n</ul>\r\n', '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(1633, 148, '<p>Roberto Leone</p>\r\n', '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(1638, 102, '<p><u><strong>Agradecimentos:</strong></u></p>\r\n\r\n<ul>\r\n	<li><strong>Canal GNT&nbsp;</strong></li>\r\n	<li><strong>In &nbsp;Press Porter Novelli</strong></li>\r\n	<li><strong>Site Lilian Pacce&nbsp;</strong></li>\r\n</ul>\r\n', '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(1639, 186, '<p>Agradecimentos - Casa Harcourt</p>\r\n', '2017-05-22 18:19:01', '2017-05-22 18:19:01'),
(1641, 35, '<p><u><strong>Agradecimentos:</strong></u></p>\r\n\r\n<ul>\r\n	<li>TV Globo</li>\r\n	<li>Revista Trip</li>\r\n	<li>Revista Vogue RG</li>\r\n	<li>Revista GQ</li>\r\n	<li>Red Bull</li>\r\n	<li>Honda</li>\r\n	<li>Jairo Goldflus</li>\r\n	<li>Miro</li>\r\n	<li>Tiago Molinos</li>\r\n	<li>Gustavo Zylberstain.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-05-22 19:43:30', '2017-05-22 19:43:30'),
(1644, 101, '<p><strong><em><u>Agradecimentos</u></em></strong></p>\r\n\r\n<ul>\r\n	<li>Oi</li>\r\n</ul>\r\n', '2017-05-26 23:05:38', '2017-05-26 23:05:38'),
(1645, 128, '<p><u><strong>Agradecimentos</strong></u></p>\r\n\r\n<ul>\r\n	<li>Adri Felden</li>\r\n</ul>\r\n', '2017-05-29 17:29:27', '2017-05-29 17:29:27'),
(1646, 134, '<p><u><strong>Agradecimentos:</strong></u></p>\r\n\r\n<p>Adri Felden</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-05-29 17:29:44', '2017-05-29 17:29:44'),
(1649, 30, '<p><u><em><strong>Agradecimentos</strong></em></u>:</p>\r\n\r\n<ul>\r\n	<li>&nbsp;Canal Rede TV</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(1652, 47, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>TV Globo</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(1653, 56, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Canal MAC</li>\r\n	<li>Canal Nike</li>\r\n	<li>TVGlobo</li>\r\n	<li>Revista Isto &Eacute; Platinum</li>\r\n	<li>La Perla&nbsp;</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(1658, 45, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Revista Cabelos e Cosm&eacute;ticos</li>\r\n	<li>Revista Corpo a Corpo</li>\r\n	<li>Revista TPM</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-06-26 17:06:04', '2017-06-26 17:06:04'),
(1663, 33, '<p><u><strong>Agradecimentos</strong></u>:</p>\r\n\r\n<ul>\r\n	<li>Canal GNT</li>\r\n	<li>Canal Ecoera</li>\r\n	<li>Canal Meet Joe TV</li>\r\n	<li>Canal SBT</li>\r\n	<li>&nbsp;Ecoera&nbsp;</li>\r\n	<li>In Press Porter Novelli</li>\r\n	<li>Revista 29 horas</li>\r\n</ul>\r\n', '2017-06-30 13:37:53', '2017-06-30 13:37:53'),
(1666, 185, '', '2017-06-30 20:50:49', '2017-06-30 20:50:49'),
(1668, 55, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li><em><strong>Canal GNT</strong></em></li>\r\n	<li><em><strong>In Press Porter Novelli</strong></em></li>\r\n</ul>\r\n', '2017-07-07 13:10:58', '2017-07-07 13:10:58'),
(1669, 92, '<p><u><em><strong>Agradecimentos</strong></em></u>:</p>\r\n\r\n<ul>\r\n	<li>Canal GNT</li>\r\n	<li>In Press Porter Novelli</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-07-07 13:12:46', '2017-07-07 13:12:46'),
(1670, 145, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li><em><strong>Canal GNT</strong></em></li>\r\n	<li><em><strong>In Press Porter Novelli</strong></em></li>\r\n</ul>\r\n', '2017-07-07 13:14:24', '2017-07-07 13:14:24'),
(1671, 70, '<p><strong><em><u>Agradecimentos:</u></em></strong></p>\r\n\r\n<ul>\r\n	<li>Canal Enjoei</li>\r\n	<li>Canal SBT</li>\r\n	<li>Canal Maria Garcia</li>\r\n	<li>Canal Maybelline</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(1672, 26, '<p><u><strong>Agradecimentos</strong></u>:</p>\r\n\r\n<ul>\r\n	<li>TV Globo</li>\r\n	<li>Canal Today Food</li>\r\n	<li>Canal OK! TV USA</li>\r\n	<li>Tiago Molinos</li>\r\n	<li>&nbsp;Colin Angus</li>\r\n</ul>\r\n', '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(1673, 49, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Canal Discovery Channel&nbsp;</li>\r\n	<li>Rede Bandeirantes de Televis&atilde;o</li>\r\n	<li>&nbsp;Canal SBT</li>\r\n	<li>&nbsp;Regina Torchia</li>\r\n</ul>\r\n', '2017-07-07 13:48:52', '2017-07-07 13:48:52'),
(1674, 77, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Canal Ecoera</li>\r\n	<li>Canal GNT</li>\r\n	<li>Canal Meet Joe TV</li>\r\n	<li>Revista 29 horas</li>\r\n	<li>In Press Porter Novelli</li>\r\n</ul>\r\n', '2017-07-12 20:16:07', '2017-07-12 20:16:07'),
(1675, 106, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Canal Ecoera</li>\r\n</ul>\r\n', '2017-07-12 20:18:31', '2017-07-12 20:18:31'),
(1677, 21, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Produ&ccedil;&atilde;o &quot;O Jardim&quot;</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-07-18 19:58:12', '2017-07-18 19:58:12'),
(1680, 89, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Canal GNT</li>\r\n	<li>Revista Casa Claudia</li>\r\n	<li>Designboom</li>\r\n	<li>Archdaily</li>\r\n	<li>The Creators Project</li>\r\n	<li>Freude Von Freuden</li>\r\n	<li>&nbsp;Andr&eacute; Klotz</li>\r\n</ul>\r\n', '2017-07-20 13:35:13', '2017-07-20 13:35:13'),
(1682, 52, '<p><u><em><strong>Agradecimentos</strong></em></u>:</p>\r\n\r\n<ul>\r\n	<li>Canal GNT</li>\r\n	<li>Revista Casa Claudia</li>\r\n	<li>Designboom</li>\r\n	<li>Archdaily</li>\r\n	<li>The Creators Project</li>\r\n	<li>Freude Von Freuden</li>\r\n	<li>&nbsp;Andr&eacute; Klotz</li>\r\n</ul>\r\n', '2017-07-20 14:01:07', '2017-07-20 14:01:07'),
(1683, 58, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Canal&nbsp;GNT</li>\r\n	<li>Canal KLM</li>\r\n	<li>Canal Mapa da Cacha&ccedil;a</li>\r\n	<li>Canal Mocot&oacute; Restaurante</li>\r\n	<li>Canal Sadia</li>\r\n	<li>TV Globo</li>\r\n</ul>\r\n', '2017-07-20 14:20:45', '2017-07-20 14:20:45'),
(1684, 109, '<p><strong><em><u>Agradecimentos</u></em></strong></p>\r\n\r\n<ul>\r\n	<li>Canal Mapa da Cacha&ccedil;a</li>\r\n	<li>Canal Mocot&oacute; Restaurante</li>\r\n	<li>Canal Revista &Eacute;poca</li>\r\n	<li>Canal KLM</li>\r\n	<li>Canal Sadia</li>\r\n</ul>\r\n', '2017-07-20 14:21:17', '2017-07-20 14:21:17'),
(1685, 67, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Canal Globonews</li>\r\n</ul>\r\n', '2017-07-24 18:47:03', '2017-07-24 18:47:03'),
(1691, 29, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Revista Inked</li>\r\n	<li>Revista GQ&nbsp;</li>\r\n	<li>Produ&ccedil;&atilde;o &quot;Luz Negra&quot;</li>\r\n	<li>Produ&ccedil;&atilde;o &quot;Otelo&quot;</li>\r\n	<li>Produ&ccedil;&atilde;o &quot; Rita Lee Mora ao Lado&quot;</li>\r\n	<li>Bob Sousa</li>\r\n	<li>Foto: Miro</li>\r\n</ul>\r\n', '2017-07-24 23:49:29', '2017-07-24 23:49:29'),
(1692, 53, '<p><u><em><strong>Agradecimentos:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Canal TNT</li>\r\n	<li>Canal Ecoera</li>\r\n	<li>O Botic&aacute;rio</li>\r\n</ul>\r\n', '2017-07-25 18:08:40', '2017-07-25 18:08:40');

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_desfiles`
--

CREATE TABLE `portfolio_desfiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_personalidades_id` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portfolio_desfiles`
--

INSERT INTO `portfolio_desfiles` (`id`, `portfolio_personalidades_id`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(571, 51, '20151020170736pedro-andrade-e-miles-1.jpg', 0, '2016-03-07 19:48:12', '2016-03-07 19:48:12'),
(678, 73, '20160106190006desfile-cavalera-spfw-verao2014-045.jpg', 0, '2016-07-29 23:01:36', '2016-07-29 23:01:36'),
(679, 73, '20160106190012desfile-cavalera-spfw-verao2014-0511.jpg', 1, '2016-07-29 23:01:36', '2016-07-29 23:01:36'),
(680, 73, '20160106190010desfile-cavalera-spfw-verao2014-0371.jpg', 2, '2016-07-29 23:01:36', '2016-07-29 23:01:36'),
(681, 73, '20160106190010desfile-cavalera-spfw-verao2014-0061.jpg', 3, '2016-07-29 23:01:36', '2016-07-29 23:01:36'),
(682, 73, '20160106190011desfile-cavalera-spfw-verao2014-0031.jpg', 4, '2016-07-29 23:01:36', '2016-07-29 23:01:36'),
(683, 73, '20160729195929doestilista3.jpg', 5, '2016-07-29 23:01:36', '2016-07-29 23:01:36'),
(684, 73, '20160729195943196144-desfile-doestilista-saopauloinverno2011rtw-115-654x982.jpg', 6, '2016-07-29 23:01:36', '2016-07-29 23:01:36'),
(685, 73, '2016072919595712621-dest-i09-002.jpg', 7, '2016-07-29 23:01:36', '2016-07-29 23:01:36'),
(686, 73, '2016072920000612675-dest-i09-017.jpg', 8, '2016-07-29 23:01:36', '2016-07-29 23:01:36'),
(687, 73, '20160729200027166144-desfiles-doestilista-saopauloinverno2007rtw-117.jpg', 9, '2016-07-29 23:01:36', '2016-07-29 23:01:36'),
(688, 73, '20160729200037166123-desfiles-doestilista-saopauloinverno2007rtw-100.jpg', 10, '2016-07-29 23:01:36', '2016-07-29 23:01:36'),
(689, 73, '20160729200053100779-desfile-doestilista-verao2009rtw-121.jpg', 11, '2016-07-29 23:01:36', '2016-07-29 23:01:36'),
(690, 73, '20160729200105100758-desfile-doestilista-verao2009rtw-100.jpg', 12, '2016-07-29 23:01:36', '2016-07-29 23:01:36'),
(691, 73, '2016072920012528990-dest-i10-014.jpg', 13, '2016-07-29 23:01:36', '2016-07-29 23:01:36'),
(692, 73, '2016072920012528995-dest-i10-019.jpg', 14, '2016-07-29 23:01:36', '2016-07-29 23:01:36'),
(889, 54, '2015120216334112108073-925375134164561-5056994702355925846-n.jpg', 0, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(890, 54, '20151130190712gloria-coelho-inverno-2015.jpg', 1, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(891, 54, '201510281433306.jpg', 2, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(892, 54, '201510281433264.jpg', 3, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(893, 54, '20151028143318aaaaaa.jpg', 4, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(894, 54, '201510281433337.jpg', 5, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(895, 54, '201510281433379.jpg', 6, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(900, 149, '20160530235907img-3411.jpg', 0, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(904, 40, '20151020164203image001.jpg', 0, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(905, 40, '20151116125510marcele-spfw2013.jpg', 1, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(907, 112, '20151026172607erika-palomino.jpg', 0, '2017-05-17 18:32:22', '2017-05-17 18:32:22'),
(908, 150, '20160531221232201605312209132016054297-pedro-bosnich-na-novela-em-familia-318x479.jpg', 0, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(926, 35, '201510271602536efxa12h24ml7ssjz81ra3yn2.jpg', 0, '2017-05-22 19:43:30', '2017-05-22 19:43:30'),
(940, 30, '201510091222121 capa desfiles.jpg', 0, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(941, 30, '20151009122211DSC_0190.jpg', 1, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(942, 30, '201510091222122.jpg', 2, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(954, 176, '20170612122510fabi-gomes-desfile-reinaldo.jpg', 0, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(955, 176, '20170321120017unknown-17.jpeg', 1, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(956, 176, '20170321120324unknown-8.jpeg', 2, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(957, 176, '20170321120017unknown-6.jpeg', 3, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(958, 176, '20170321120018unknown-12.jpeg', 4, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(959, 176, '20170321120018unknown-10.jpeg', 5, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(960, 176, '20170321120018unknown-13.jpeg', 6, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(961, 47, '20151130190821spfw-inveno-2015.jpg', 0, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(962, 56, '20160602193934desfile-001-la-perla.jpg', 0, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(963, 56, '20160602194437desfile-004-la-perla.jpg', 1, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(964, 56, '20160602193938desfile-002-la-perla.jpg', 2, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(981, 70, '20151118174507uma1-1024x545.jpg', 0, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(982, 70, '20151118174507beleza-spfw.JPG', 1, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(983, 70, '20151118174507191015-backstage-uma-3-590x393.jpg', 2, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(984, 70, '201511181745222-belezanatural.jpg', 3, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(985, 26, '20151027000842400px-camila-alves-in-kaufmanfranco-2.jpg', 0, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(986, 26, '201510270008422hqa142incs8gh9bs00c0yxoy.jpg', 1, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(987, 26, '20151027000843012810-camila-alves-86255489.jpg', 2, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(988, 26, '201510270008422253071393.jpg', 3, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(989, 53, '20151006185209neon-desfile-verao-2014-spfw-fotos-Marcelo-Soubhia.jpg', 0, '2017-07-25 18:08:40', '2017-07-25 18:08:40'),
(990, 53, '20151026183043pv-275-lunender-135212-baixa.jpg', 1, '2017-07-25 18:08:40', '2017-07-25 18:08:40'),
(991, 53, '20151026183049158115-970x600-1.jpeg', 2, '2017-07-25 18:08:40', '2017-07-25 18:08:40');

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_espetaculos`
--

CREATE TABLE `portfolio_espetaculos` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_personalidades_id` int(10) UNSIGNED NOT NULL,
  `titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `autor` text COLLATE utf8_unicode_ci NOT NULL,
  `editora` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portfolio_espetaculos`
--

INSERT INTO `portfolio_espetaculos` (`id`, `portfolio_personalidades_id`, `titulo`, `autor`, `editora`, `imagem`, `texto`, `ordem`, `created_at`, `updated_at`) VALUES
(418, 71, '“Vanya e Sonia e Masha e Spike”,', 'Jorge Takla', '2015', '20151210163953vania-sonia.jpg', 'A montagem foi considerada a comédia de maior sucesso na Broadway em 2014 e conquistou o Tony Awards de Melhor Peça em 2013, quando feita pelo Lincoln Center, em Nova York. Foi justamente nesta ocasião que o diretor Jorge Takla começou a batalhar pela produção do espetáculo no Brasil. Texto  de Christopher Durang, tradução de Bianca Tadini ', 0, '2016-05-04 18:54:30', '2016-05-04 18:54:30'),
(419, 71, 'Jesus Cristo Superstar', 'Jorge Takla', '2014', '20151202183243jesus-cristo-superstar-2.jpg', '"Jesus Cristo Superstar", dirigido por Jorge Takla, foi aplaudido pela crítica e público desde sua estreia em março, até o termino em 1° de junho no Teatro do Complexo Ohtake Cultural, em São Paulo.  visagismo, assinado pelo beauty artist Duda Molinos e equipe do seu espaço de beleza', 1, '2016-05-04 18:54:30', '2016-05-04 18:54:30'),
(420, 71, 'O Rei e Eu” de Rodgers & Hammerstein', 'Jorge Takla ', '2012', '20151202184728o-rei-e-eu.jpg', '“O Rei e Eu” de Rodgers & Hammerstein invade o Teatro Alfa em São Paulo inundando os palcos com o encantamento e a magia da conhecida estória de Anna Leonowens, uma inglesa que é contratada para ser professora dos inúmeros filhos de Mongkut (Rama IV), Rei de Sião (Tailândia) no século 19. Direção Jorge Takla ', 2, '2016-05-04 18:54:30', '2016-05-04 18:54:30'),
(421, 71, 'Evita ', 'Jorge Takla ', '2010', '20151202182831ovadiasaadiacombr-evita.jpg', 'Visagismo', 3, '2016-05-04 18:54:30', '2016-05-04 18:54:30'),
(422, 71, 'Miss Saigon ', 'Jorge Takla ', '2010', '20151202185149misssaigon.jpg', 'Visagismo', 4, '2016-05-04 18:54:30', '2016-05-04 18:54:30'),
(423, 71, 'Mademoiselle Chanel', 'Jorge Takla ', '2005/2006', '20151202183910t-chanel-r.jpg', ' Marília Pêra,  A atriz deu vida à figura contraditória de Coco Chanel, encarnando sua postura, seus trejeitos. O texto de Maria Adelaide Amaral é um primor: engraçado, emotivo, inteligente, envolvente,O texto aborda também seu relacionamento com artistas plásticos, bailarinos, pessoas das artes em geral e, claro, seus amores. Direção Jorge Takla. ', 5, '2016-05-04 18:54:30', '2016-05-04 18:54:30'),
(1888, 129, 'As Palavras e As Coisas ', 'Pedro Bricio', '2017', '20170206111415lucia-b-peca-as-palavras-e-as-coisas.jpg', 'O texto coloca em cena a história de três amigos, envolvidos numa trama de amor, morte e literatura. A peça fala sobre a passagem do tempo, os conflitos entre memória e realidade, entre o corpo e o mundo.', 0, '2017-02-20 03:13:01', '2017-02-20 03:13:01'),
(1889, 129, 'Trabalhos de Amores Quase Perdidos', 'Pedro Brício', '2017', '20170206114302trabalhos-de-amores-quase-perdidos.jpg', 'A peça é um jogo de amor e acaso, narração e atuação. Numa espécie de metateatro, onde personagem e narrador se misturam, o texto fala da crise dos 30, da juventude e do amor na juventude. Do encontro com o outro – o prazer, o choque e o inesperado deste encontro. Revela o banal e o trágico do cotidiano através dos caprichos do acaso tecendo o destino.', 1, '2017-02-20 03:13:01', '2017-02-20 03:13:01'),
(1890, 129, 'Rózà', 'Martha Kiss Perrone, Joana Levi', '2014', '2015120916063812122736-692987374179309-7443400266630102073-n.jpg', 'Rózà é um espetáculo multimídia construído a partir das cartas de Rosa Luxemburgo escritas, em sua maioria, nas prisões alemãs do início do século 20. Nascida na Polônia, judia, militante cosmopolita, dirigente revolucionária, mulher apaixonada, oradora famosa e antimilitarista, Rosa foi brutalmente assassinada em 1919, pouco dias após deixar a prisão e assumir a linha de frente da revolução alemã.', 2, '2017-02-20 03:13:01', '2017-02-20 03:13:01'),
(1891, 129, 'Histórias de Amor Duram Apenas 90 Minutos ', 'Paulo Halm', '2010', '20170206120033historias-de-amor-duram-apenas-90-minutos.jpg', 'A história gira em torno de Zeca (Caio Blat), prestes a chegar aos 30 anos e meio perdido na vida. Vive há cinco anos com Júlia (Maria Ribeiro), por ele definida como uma "típica personagem de filme francês: linda, inteligente e fria". Zeca sonha em concluir um livro, mas empacou por volta da página 50. Sustentado pelo dinheiro deixado por sua mãe, já falecida, ele nada tem a fazer a não ser vagar pela cidade. Só que, como diz um velho ditado, mente vazia é oficina do diabo. É o que ocorre quando ele vê, por acaso, Júlia e uma colega em um camarim de loja, apenas usando roupas íntimas. Zeca rapidamente conclui que ela o está traindo, com outra mulher.  ', 3, '2017-02-20 03:13:01', '2017-02-20 03:13:01'),
(1979, 178, '"Vicente Celestino - A Voz Orgulho do Brasil"', 'Direção de Jacqueline Laurence', '2010', '20170210190901timthumbphp.gif', 'Protagonista do Musical', 0, '2017-03-08 02:55:15', '2017-03-08 02:55:15'),
(1990, 156, 'Meu Amigo Charlie Brown ', 'Alonso Barros ', '2016 ', '20161114191405charles.jpg', '', 0, '2017-03-10 21:35:31', '2017-03-10 21:35:31'),
(1991, 156, 'Pé na Cova', 'Miguel Falabella', '2013', '20161004140806pe-na-cova.jpg', 'Globo', 1, '2017-03-10 21:35:31', '2017-03-10 21:35:31'),
(1992, 156, '4 Faces do Amor', 'Tadeu Aguiar', '2016', '201610041410004-faces.jpg', 'Eduardo Bakr', 2, '2017-03-10 21:35:31', '2017-03-10 21:35:31'),
(1993, 156, 'Avenida Q', 'Charles Möeller e Cláudio Botelho', '2009', '20161004140126avenida-q.jpg', ' Robert Lopez e Jeff Marx', 3, '2017-03-10 21:35:31', '2017-03-10 21:35:31'),
(1994, 156, 'Xanadu', 'Miguel Falabella', '2012', '20161004140255xanadu.jpg', '', 4, '2017-03-10 21:35:31', '2017-03-10 21:35:31'),
(1995, 156, 'Miss Saigon', 'Fred Hanson', '2007', '20161004140433miss-saigon.jpg', 'Alain Boublil/Claude-Michel Schönberg', 5, '2017-03-10 21:35:31', '2017-03-10 21:35:31'),
(1996, 156, 'As Bruxas de Eastwick', 'Charles Möeller e Cláudio Botelho', '2011', '20161004140607bruxas.jpg', 'textos e letras de John Dempsey e música de Dana P. Rowe', 6, '2017-03-10 21:35:31', '2017-03-10 21:35:31'),
(1997, 156, 'Company', 'Charles Möeller e Cláudio Botelho', '2000', '20161004140717company.jpg', 'Stephen Sondheim e George Furth', 7, '2017-03-10 21:35:31', '2017-03-10 21:35:31'),
(1998, 156, '220V', 'Paulo Gustavo', '', '20161004141031220v.jpg', 'Multishow', 8, '2017-03-10 21:35:31', '2017-03-10 21:35:31'),
(2091, 19, 'Novela "O Rico e Lázaro"', 'Direção de Edgard Miranda', '2017', '20170314174843cassio-scapin-l-o-rico-e-lazaro.jpg', 'Texto de Paula Richard', 0, '2017-03-14 20:49:42', '2017-03-14 20:49:42'),
(2092, 19, 'Espetáculo "Histeria"', 'Direção de Jô Soares', '2016', '20160510160337c2f658410aacca7491df7c0329c21c67.jpg', 'Texto de Terry Johnson', 1, '2017-03-14 20:49:42', '2017-03-14 20:49:42'),
(2093, 19, 'Eu Não Dava Praquilo ', 'Elias Andreatto', '2013', '20151014141732photocollage.jpg', '', 2, '2017-03-14 20:49:42', '2017-03-14 20:49:42'),
(2094, 19, 'A Viúva Alegre - de Franz Lehár', 'Jorge Takla', '2012', '20151021134831unknown-8.jpeg', '', 3, '2017-03-14 20:49:42', '2017-03-14 20:49:42'),
(2095, 19, 'O Libertino ', 'Jô Soares', '2011', '20151026174013cassio.jpg', '', 4, '2017-03-14 20:49:42', '2017-03-14 20:49:42'),
(2142, 151, '"Visitando o Senhor Green"', 'Direção de Cassio Scapin', '2017', '20170223105733a172432c-dead-4a81-ad8c-e08f27ad5d87.jpg', 'Texto de Jeff Baron', 0, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(2143, 151, '"O Rei e a Cora Enfeitiçada"', 'Direção de Cynthia Falabella e Débora Fababella ', '2015 e 2017', '2016061511314610155251-10152594217176975-1588842544824062942-n.jpg', 'Texto de Rogério Falabella', 1, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(2144, 151, '"Carta Rasgada"', 'Direção de Gerardo Franco e Beto Narsci', '2016', '2016061511424920160530113601carta-rasgada-1.jpg', 'Texto de Alex Giostri', 2, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(2145, 151, '"Diálogos de Salomé com São João Batista"', 'Direção de Sérgio Ferrara', '2016', '20161003180252dialogos-de-salome-com-joao-batista-vivian-fernandez-2.jpg', 'Texto de Sérgio Ferrara a partir de Oscar Wilde', 3, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(2146, 151, '"Antes de Tudo"', 'Texto e Direção de Dan Rosseto', '2015', '2016061511380212191476-10153142818241975-5233562897310932693-n.jpg', '', 4, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(2147, 151, 'Navio Fantasma - O Holandês Voador', 'Kleber Montanheiro', '2015', '20160615111627flyer-eletronico.jpg', '', 5, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(2148, 151, '"Êxtase"', 'Direção de Ivan Feijó', '2015', '2016061511363311703128-492969454201031-3486038748020383466-n.jpg', 'Texto de Walcyr Carrasco', 6, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(2149, 151, '"Sabiá"', 'Texto e Direção de Paulo Faria', '2014', '2016061511334610300699-10201791480529080-7268515741763769823-n.jpg', '', 7, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(2150, 151, '"Propriedades Condenadas"', 'Direção de Marco Antonio Pâmio', '2014', '20170224194411eflyer-propriedades-condenadas-11.jpg', 'Tennesse Willlians', 8, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(2151, 151, '"Atrás do Pano"', 'Direção de Marcelo Romagnoli', '2012', '2016061511095453455-477313428967401-1553755090-o.jpg', 'Texto de Luiza Jorge', 9, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(2152, 151, '"Jamais Seremos Tão Jovens"', 'Direção de Kiko Jaez', '2012', '20170224195902jamais-seremos-tao-jovens.jpeg', 'Texto de Mário Cesar Costa', 10, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(2153, 151, '"Pobre Super Homem"', 'Direção de Sergio Ferrara', '2000', '20160615105237389450-2276807597458-630033075-n.jpg', 'Texto de Brad Freser', 11, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(2181, 159, 'Série "Pacto de Sangue"', 'Adrian Caetano e Tomás Portella', '2017', '20170120224428andre-ramiro-3-a-lei.jpg', 'Canal Space', 0, '2017-04-13 20:45:55', '2017-04-13 20:45:55'),
(2182, 159, 'Novela "Terra Prometida"', 'Alexandre Avancini', '2016', '20161004134201terra-prometida.jpg', 'Personagem Jesana', 1, '2017-04-13 20:45:55', '2017-04-13 20:45:55'),
(2183, 159, 'Longa Metragem "O Último Virgem" ', 'Rilson Baco e Felipe Bretas', '2016', '2016122016533115492364-1355766467776273-1964435164720139206-n.jpg', 'Personagem Luizão', 2, '2017-04-13 20:45:55', '2017-04-13 20:45:55'),
(2184, 159, 'Curta Metragem "Nocaute"', 'Fernando Negrovsk', '2016', '20170219192537download.jpg', '', 3, '2017-04-13 20:45:55', '2017-04-13 20:45:55'),
(2185, 159, 'Curta Metragem "Cinzas e Café"', 'Daniel Gravelli', '2016', '20170219192314cinzas-e-cafe.jpg', '', 4, '2017-04-13 20:45:55', '2017-04-13 20:45:55'),
(2186, 159, 'Série "Conselho Tutelar"', 'Rudi Lagemann', '2015', '20170219192043conselho-tutelar-record.jpg', 'Personagem Caveira', 5, '2017-04-13 20:45:55', '2017-04-13 20:45:55'),
(2187, 159, 'Curta Metragem "Luz Clara"', 'Alexandre Mello', '2015', '20161208111936maxresdefault.jpg', '', 6, '2017-04-13 20:45:55', '2017-04-13 20:45:55'),
(2188, 159, 'Longa Metragem "Trash - A Esperança vem do Lixo"', 'Stephen Daldry', '2014', '20161004133943trash.jpg', '', 7, '2017-04-13 20:45:55', '2017-04-13 20:45:55'),
(2189, 159, 'Série "Plano Alto"', 'Marcílio Moraes', '2014', '20161004134137plano-alto.png', 'Personagem Alfredo', 8, '2017-04-13 20:45:55', '2017-04-13 20:45:55'),
(2190, 159, 'Novela "Pecado Mortal"', 'Alexandre Avancini', '2013', '20161004134116pecado-mortal.png', 'Personagem Jeferson Carneiro', 9, '2017-04-13 20:45:55', '2017-04-13 20:45:55'),
(2191, 159, 'Novela "Vidas em Jogo"', 'Alexandre Avancini', '2011', '20161004134037vidas-em-jogo.jpg', 'Personagem Betão', 10, '2017-04-13 20:45:55', '2017-04-13 20:45:55'),
(2192, 159, 'Longa Metragem "Tropa de Elite 2"', 'José Padilha', '2010', '20161004133841tropa-de-elite-2.jpg', 'Personagem Mathias', 11, '2017-04-13 20:45:56', '2017-04-13 20:45:56'),
(2193, 159, 'Novela "Lei e o Crime"', 'Alexandre Avancini', '2009', '20161004134011lei-e-crime.jpg', 'Personagem Tião Meleca', 12, '2017-04-13 20:45:56', '2017-04-13 20:45:56'),
(2194, 159, 'Longa Metragem "Última Parada 174"', 'Bruno Barreto', '2008', '20161004133910174.jpg', 'Personagem Souza', 13, '2017-04-13 20:45:56', '2017-04-13 20:45:56'),
(2195, 159, 'Longa Metragem "Tropa de Elite"', 'José Padilha', '2007', '20161004133816tropa-de-elite-1.jpg', 'Personagem Mathias', 14, '2017-04-13 20:45:56', '2017-04-13 20:45:56'),
(2198, 167, 'Novela "A Lei do Amor" - TV Globo', 'Denise Saraceni', '2016', '20161004153519a-lei-do-amor.jpg', 'Personagem Padre Paulo', 0, '2017-04-18 21:39:14', '2017-04-18 21:39:14'),
(2199, 167, 'Musical "Wicked"', 'Joe Mantello', '2016', '20161004152305wicked.jpg', 'Stephen Schwartz', 1, '2017-04-18 21:39:14', '2017-04-18 21:39:14'),
(2200, 167, '"Mudança de Hábito - O Musical"', 'Jerry Zaks', '2015', '20161004152714mudanca-de-habito.jpg', 'texto de Cheri e Bill Steinkellner com músicas de Alan Menken e letras de Glenn Slater', 2, '2017-04-18 21:39:14', '2017-04-18 21:39:14'),
(2201, 167, 'Novela "Babilônia" - TV Globo', 'Dennis Carvalho', '2015', '20161004153455babilonia.jpg', 'Personagem Tadeu', 3, '2017-04-18 21:39:14', '2017-04-18 21:39:14'),
(2202, 167, 'Musical "O Rei Leão"', 'Julie Taymor', '2013/14', '20161004152757rei-leao.jpg', 'Personagem Mufasa', 4, '2017-04-18 21:39:14', '2017-04-18 21:39:14'),
(2203, 167, 'Novela "Sangue Bom" - TV Globo', 'Dennis Carvalho', '2013', '20161004153424sangue-bom.jpg', 'Personagem Arthur Bicalho', 5, '2017-04-18 21:39:14', '2017-04-18 21:39:14'),
(2204, 167, 'Musical "Hair"', 'Charles Möeller e Cláudio Botelho', '2011/12', '20161004152935hair.jpg', 'James Rado and Gerome Ragni, música por Galt MacDermot', 6, '2017-04-18 21:39:14', '2017-04-18 21:39:14'),
(2205, 167, 'Novela "Lado a Lado" - TV Globo', 'Dennis Carvalho', '2011', '20161004153321lado-a-lado.png', 'Personagem Chico', 7, '2017-04-18 21:39:14', '2017-04-18 21:39:14'),
(2253, 132, 'LOVE STORY', 'Tadeu Aguiar', '2016', '20160721134821love-story-elenco.jpg', '', 0, '2017-05-02 20:46:33', '2017-05-02 20:46:33'),
(2254, 132, 'OU TUDO OU NADA"', 'Tadeu Aguiar ', '2015', '2015120915035012079468-1254175604599432-263086709851219396-n.jpg', 'Versão brasileira : Arthur Xexéo', 1, '2017-05-02 20:46:33', '2017-05-02 20:46:33'),
(2255, 132, 'Namíbia, Não', 'Lazaro Ramos ', '2015', '2015120915073612066021-10205660082284465-360346433260230023-n.jpg', 'Aldri Anunciação ', 2, '2017-05-02 20:46:33', '2017-05-02 20:46:33'),
(2269, 149, 'Novela "O Rico e Lázaro"', 'Direção de Edgard Miranda', '2017', '20170314175251saulo-meneghetti-l-o-rico-e-lazaro.jpg', 'Texto de Paula Richard', 0, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(2270, 149, 'Novela "Escrava Mãe"', 'Direção de Ivan Zettel', '2016 - 2017', '2017040322333113258964-1785623531650181-1570651772-n.jpg', 'Texto de Gustavo Reiz', 1, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(2289, 153, 'Love Story', 'Tadeu Aguiar', '', '20160727171351love.jpg', 'Stephen Clark', 0, '2017-05-17 18:39:39', '2017-05-17 18:39:39'),
(2290, 153, 'Rita Lee Mora Ao Lado', 'Debora Dubois', '', '20160729162632fabio.jpg', '', 1, '2017-05-17 18:39:39', '2017-05-17 18:39:39'),
(2291, 153, 'Priscilla  A Rainha do Deserto', '', '', '20160727172904priscillabr.jpg', '', 2, '2017-05-17 18:39:39', '2017-05-17 18:39:39'),
(2294, 150, 'Coisas Estranhas Acontecem Nessa Casa', 'Marcio Macena, com supervisão artística de Marisa Orth', '2016', '20160826120145coisas-estranhas-flyer.jpg', '', 0, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(2295, 150, '“Carta Rasgada”', 'Gerardo Franco e Beto Nasci ', '2016', '20160530113601carta-rasgada-1.jpg', '', 1, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(2296, 150, 'Caso Sério - Uma Comédia Emotiva ', 'Claudio Simões', '2014', '20160530113631e8b634-a9f1b8c234df739beac52a746e709fc1.png', '', 2, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(2297, 150, 'O Chapeleiro Maluco', 'Jarbas Homem de Mello e Rogério Matias', '2012', '20160530113733o-chapeleiro-maluco.png', '', 3, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(2298, 150, 'O Fantasma da Máscara', 'Rosi Campos', '2011', '20160530113803e8b634-939fce6f05e011f4e5eb29c0e22b9e5f.jpg', '', 4, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(2299, 150, 'Tango, Bolero e Cha Cha Cha', 'Bibi Ferreira ', '2011', '2016053011383420111017092401-cartaz-tango.jpg', '', 5, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(2300, 150, 'HQB - Teatro Virando Gibi !', 'Beto Marden e Flávio de Souza', '2009', '20160530113852hqb.jpg', '', 6, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(2301, 191, 'Minisserie Maysa Quando Fala o Coraçao ', 'Jayme Monjardim ', '2013', '20170329124926face9.jpg', 'Manoel Carlos ', 0, '2017-05-22 19:17:48', '2017-05-22 19:17:48'),
(2322, 192, 'XXXX', 'XXX', 'XXX', '20170404153804que-aconteceu-com-vo-quim.jpeg', 'XXX', 0, '2017-05-25 21:06:42', '2017-05-25 21:06:42'),
(2323, 192, 'zzz', 'zzz', 'zzz', '20170404153957selvagens.jpeg', 'zzz', 1, '2017-05-25 21:06:42', '2017-05-25 21:06:42'),
(2378, 160, '"Intocáveis"', 'Direção de Iacov Hillel', '2015/2016', '20160926134843intocaveis.jpg', 'Texto de Eric Toledano e Olivier Nakashe', 0, '2017-05-30 18:49:42', '2017-05-30 18:49:42'),
(2379, 160, '"Love & Money"', 'Direção de André Pink', '2014', '20160926135739love-and-money-2.jpg', 'Texto de Dennis Kellyn', 1, '2017-05-30 18:49:42', '2017-05-30 18:49:42'),
(2380, 160, '"O Homem Feio"', 'Direção de André Pink', '2014', '20160926135813o-homem-feio.jpg', 'Texto de Marius Von Mayenburg', 2, '2017-05-30 18:49:42', '2017-05-30 18:49:42'),
(2381, 160, '"Meu Pedacinho de Chão" - TV Globo', 'Direção de Luiz Fernando Carvalho ', '2014', '20161006163512meu-pedacinho-de-chao.jpg', 'Personagem Conchetta', 3, '2017-05-30 18:49:42', '2017-05-30 18:49:42'),
(2382, 160, '"Suburbia" - TV Globo', 'Direção de Luiz Fernando Carvalho', '2012', '20161006163541suburbia-1.jpg', 'Personagem Sylvia', 4, '2017-05-30 18:49:42', '2017-05-30 18:49:42'),
(2383, 160, '"A Pequena Sereia"', 'Direção de Paulo Ribeiro', '2012', '20170303090825a-pequena-sereia1.jpg', 'Adaptação teatral de Vladimir Capella a partir de texto de Hans Christian Andersen ', 5, '2017-05-30 18:49:42', '2017-05-30 18:49:42'),
(2384, 160, '"O Maravilhoso Mundo de Dissocia"', 'Direção de André Pink', '2011', '20170302231733estreiaead2-0.JPG', 'Texto de Anthony Neilson', 6, '2017-05-30 18:49:42', '2017-05-30 18:49:42'),
(2385, 160, '"Afinal o que querem as mulheres?" - TV Globo', 'Direção de Luiz Fernando Carvalho', '2010', '20170302232201260px-afinal-o-que-querem-as-mulheres.jpg', 'Texto de João Paulo Cuenca com coautoria de Cecília Giannetti e Michel Melamed', 7, '2017-05-30 18:49:42', '2017-05-30 18:49:42'),
(2386, 172, 'Novela "Haja Coração" - TV Globo', 'Teresa Lampreia', '2016', '20161006171034haja-coracao.jpg', 'Personagem Giba Salles', 0, '2017-05-31 19:26:16', '2017-05-31 19:26:16'),
(2387, 172, 'Espetáculo "Rabbit"', 'Eric Lenate', '2014', '20161006171350rabbit.jpg', 'Texto de Nina Raine', 1, '2017-05-31 19:26:16', '2017-05-31 19:26:16'),
(2388, 172, 'Espetáculo "Oresteia - O Canto do Bode"', 'Marco Antônio Rodrigues', '2013', '20161006172424oresteia.jpg', 'Ésquilo/ adaptação de Reinaldo Maia', 2, '2017-05-31 19:26:16', '2017-05-31 19:26:16'),
(2391, 161, 'Casa Apodrecida', 'Leonardo Bertholini', '2016', '2017012023381615101618-1704078416576199-7796005403388542976-n.jpg', 'Teatro - Versão teatral livremente inspirada no romance realista “O Primo Basílio”, escrito por Eça de Queirós', 0, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(2392, 161, 'A Vida Dela', 'Delson Antunes', '2016', '20161004142805a-vida-dela.jpg', 'Teatro - Texto de Priscila Gontijo', 1, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(2393, 161, 'O Homem Elefante', 'CIbele Forjaz e Wagner Antônio', '2015', '20161004142345homem-elefante.jpg', 'Teatro - Texto de Bernard Pomerance', 2, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(2394, 161, 'Amor Veríssimo', 'Arthur Fontes', '2015', '20161004142919amor-verissimo.jpg', 'GNT - Série produzida pela Conspiração, baseada em crônicas sobre relacionamentos amorosos escritas por Luís Fernando Veríssimo, repletas de bom humor e adaptadas pela primeira vez para a televisão', 3, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(2395, 161, 'O Acidente', 'Daniel Carvalho Faria', '2015', '201701202333563-oacidente-fotodanielmoragas.jpg', 'Teatro - Texto de Bosco Brasil', 4, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(2396, 161, 'Vermelho Amargo', 'Diogo Liberano', '2014', '20161004142500vermelho-amargo.jpg', 'Teatro - Texto de Bartolomeu Campos de Queirós', 5, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(2397, 161, 'Céu Sobre Chuva ou Botequim', 'Antonio Pedro Borges', '2013', '20161004142623botequim.jpg', 'Teatro - Texto de Gianfrancesco Guarnieri', 6, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(2398, 161, 'Momo e o Senhor do Tempo', 'Cristina Moura', '2013', '2017012023071113.jpg', 'Teatro - Texto de Michael Ende', 7, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(2399, 161, 'Farnese de Saudade', 'Celina Sodré', '2012', '2017012023305302.jpg', 'Teatro - Concepção, texto e atuação do ator', 8, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(2400, 161, 'O Menino Que Vendia Palavras', 'Cristina Moura', '2012', '20170120231043menino-virtual-709x513.jpeg', 'Teatro - Texto de Ignácio de Loyola Brandão ', 9, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(2401, 161, 'Dois Jogos: Sete Jogadores', 'Celina Sodré', '2011', '2017012023303611.jpg', 'Teatro - Inspirado em fragmentos de diversos autores, entre eles Heiner Müller, Franz Kafka, Ingmar Bergman, Van Gogh e Clarice Lispector', 10, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(2436, 182, 'Gostosas, Lindas e Sexies ', ' Ernani Nunes', '2017', '20170209181538ljkjh.jpg', 'No Rio de Janeiro vivem quatro grandes e inseparáveis amigas: Beatriz, Tânia, Ivone e Marilu. Elas vestem manequim plus size e enfrentam todas as aventuras e desencontros amorosos e profissionais que quatro jovens mulheres podem enfrentar na capital carioca, (quase) sempre de bom humor.', 0, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(2437, 182, 'Mais Forte Que o Mundo ', 'Afonso Poyart', '2016 ', '20170207174116050851.jpg', 'Baseado na história do expoente brasileiro de lutas de vale tudo, o filme mostrará a vida de superação do lutador. Lutando para construir sua vida em Manaus, passa por inúmeras dificuldades e trabalhos duros; tenta jogar futebol, mas é ironizado pelos colegas por chutar as canelas de todos. Um professor de jiu-jitsu que assistia a um jogo percebe o potencial do garoto e começa a treiná-lo em artes marciais. Aldo então desponta como lutador de jiu-jitsu e boxe tailandês, e começa a chamar atenção dos empresários. Na arena de lutas de MMA, o lutador mostra que seu potencial é bem maior do que todos pensavam, e inicia uma carreira meteórica que o levará do Amazonas para o mundo.', 1, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(2438, 182, 'Engolir a Escuridão ', 'Roberto Alvim', '2016 ', '2017020916380112983386-1384875094872113-5807568342020412781-o.jpg', 'Partindo de "Suíte Número 1", texto do dramaturgo francês Philippe Minyana, Alvim criou esta montagem que apresenta a dificuldade do ser humano em lidar com o luto e com a perda. A peça conta a história de um escritor que entra em colapso após ter sua casa invadida e vandalizada por um bando de crianças. A trilha sonora foi composta por Vladimir Safatle, filósofo e colunista da Folha.', 2, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(2439, 182, 'Osho - Cracks Of Soul', 'Janssen Hugo Lage', '2015', '20170209165543cartaz-osho-1.jpg', 'O espetáculo é uma performance teatral, um mergulho no misterioso universo vazio existencial do homem contemporâneo. Uma profunda reflexão sobre crenças, ações, sentimentos, delírios, enganos, erros, acertos, dúvidas, certezas e incertezas, que nos levam aos confins da alma. Um ensaio sobre o abandono, o retorno, a iluminação e a jornada, livremente inspirado na herança literária e espiritual deixada por Rajneesh (Osho) – seus pensamentos descritos em textos, ensaios, discursos e palestras.', 3, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(2440, 182, 'Super Nada', 'Rubens Rewald', '2013', '20170209182441super-nada-cartaz.jpg', 'O filme conta a história de Guto, que entre pequenos trabalhos, como intérprete de pegadinha e malabarista, sonha em ser um grande ator. Entre um trabalho e outro é chamado para fazer um teste em um programa de TV protagonizado por seu ídolo Zeca (protagonizado por Jair Rodrigues), um decadente comediante.', 4, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(2441, 182, 'Piramo e Tisbe', 'Vladimir Capella', '2011', '20170209172224piramo4.jpg', 'Beleza, poesia, paixão e desejo são alguns dos atributos encontrados no romance entre Píramo e Tisbe. Na mitologia greco-romana, a história do casal se assemelha a de Romeu e Julieta, dois amantes separados por um cruel destino.', 5, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(2442, 182, 'Natal Luz ', 'Equipe Gramado', '2011', '20170209173152nl.jpg', 'Sofia, a menininha que visitava a Fantástica Fábrica de Natal, cresceu, amadureceu e se transformou! Mas, como todas as pessoas quando chegam a fase adulta, se permitiu sonhar de novo!  Ela voltou a acreditar na magia do Natal, e toda a emoção que essa época nos proporciona. Reencontrou seus amigos da Fantástica Fábrica de Natal e foi mostrar a eles o que vivenciou em suas viagens. Como são os natais pelo mundo, suas curiosidades, suas festas. Um cenário altamente tecnológico nos proporciona mudar de ambiente e sair por aí, descobrindo como vários países comemoram seus natais.', 6, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(2443, 182, 'Sitio do Pica Pau Amarelo', 'Roberto Talma', '2009', '20170209173544cartaz.jpg', ' Muito fiel a obra de Lobato, o animado e ágil musical contou com uma excelente adaptação de Flávio de Souza e uma segura direção de Roberto Talma.', 7, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(2444, 182, 'O Colecionador de Crepúsculos', 'Vladimir Capella', '2009', '20170209171809colecionador-2-cartaz.gif', 'O fio condutor da peça é a história relatada no conto sobre um caipira que convida uma senhora rica e muito culta, a Morte, para batizar seu filho. Os outros enredos são intercalados na história central e o público passa a se deleitar com os costumes, crenças, mitos, tradições, características e as diversas falas do povo brasileiro. O próprio Cascudo vira personagem da peça e aparece ouvindo, registrando, fumando seu charuto, além de apreciar o crepúsculo, uma de suas agradáveis manias, daí o titulo da peça.', 8, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(2450, 197, 'A Espera Dela', 'Paulo Marcello', '2015', '20170518155002ana-carolina-godoy-espetaculo-a-espera-dela-foto-joao-caldas.jpg', 'O espetáculo “A Espera Dela”, que marca a estreia do Grupo Instante, ganha uma segunda temporada na Sala Carlos Miranda, da Funarte SP. Com direção de Paulo Marcello, um dos atores veteranos da Cia. Razões Inversas, a montagem reúne fragmentos de histórias de mulheres que experimentam a descoberta do próprio corpo, comportamentos autodestrutivos e situações de violência. Elas também desenvolvem uma compreensão do que é o feminino.', 0, '2017-06-22 17:15:36', '2017-06-22 17:15:36'),
(2451, 197, 'Eterna Magia', 'Direção de Ulysses Cruz', '2007', '20170622141342eterna-magia-3.JPG', 'Escrita por Elizabeth Jhin', 1, '2017-06-22 17:15:36', '2017-06-22 17:15:36'),
(2452, 197, 'LEONCE E LENA', 'Texto: Karl Georg Buchner', '2006', '20170616160742foto-leonce-e-lena-2.jpg', 'Comédia que trata dos desencontros de um casamento arranjado, o espetáculo utiliza a forma de um gracioso minueto para contar a história de amor de um príncipe, Leonce, e de uma princesa, Lena, do reino Popô e do reino Pipi, que, entediados, encontram-se casualmente fora de seus reinos, apaixonam-se sem saber a condição de nobreza alheia e casam-se. Com direção de Gabriel Villela.', 2, '2017-06-22 17:15:36', '2017-06-22 17:15:36'),
(2453, 201, 'Avenida Q - Musical ', 'André Gress', '2009', '20170523145548unknown-2.jpeg', 'Vencedor Melhor Ator Coadjuvante  APTR', 0, '2017-06-23 15:02:22', '2017-06-23 15:02:22'),
(2454, 201, 'Bilac Vê Estrelas ', 'De Joao Fonseca', '2015', '20170523150510andre-dias-olavo-bilac-ve-estrelas.png', ' Bilac Vê Estrelas é uma comédia musical com um roteiro bem articulado para o palco, obra de Heloisa Seixas e Julia Romeu', 1, '2017-06-23 15:02:22', '2017-06-23 15:02:22'),
(2455, 201, 'Tudo Ou Nada ', 'Tadeu Aguiar ', '2016', '20170523150842andre-dias.jpg', 'Enquanto alguns decidem chorar as perdas, haverá sempre quem esteja lá para “vender lenços”, mesmo que sejam para secar lágrimas de alegria e risadas. Essa é a proposta de “Ou Tudo ou Nada“,', 2, '2017-06-23 15:02:22', '2017-06-23 15:02:22'),
(2456, 201, '4 Faces Do amor ', 'Tadeu Aguiar ', '2016', '20170523151346b4-faces-do-amor-amanda-acosta-andre-dias-foto-roberto-ikeda-5617.jpg', '4 Faces do Amor poderia ser definida como uma mera comédia musical romântica, sobre as venturas e desventuras', 3, '2017-06-23 15:02:22', '2017-06-23 15:02:22'),
(2457, 201, '33 Variaçoes ', 'Wolf Maya ', '2016', '20170523151913img-8455.jpg', 'Duas histórias paralelas, em épocas diferentes, intercalam-se no drama 33 Variações,', 4, '2017-06-23 15:02:22', '2017-06-23 15:02:22'),
(2479, 158, 'Espetáculo "Absurdos Sérios"', 'Direção de Luciano Ferrari', '2017', '20170626102312absurdos-serios-erick-krominski.jpg', 'Comédia trágica nonsense, absurda e divertida. Oito personagens que dividem a misteriosa antessala do consultório do Dr. Curatudo, uma personalidade fantástica e midiática que promete a cura para todos os males humanos.', 0, '2017-06-28 18:48:51', '2017-06-28 18:48:51'),
(2480, 158, 'Programa "Shark Tank Brasil" - Repórter Especial', 'Canal Sony Brasil', '2017', '20170628154159shark-tank-brasil-erick-krominski.jpg', 'Versão brasileira do reality show norte-americano com investidores interessados em dar apoio financeiro a grandes ideias de empreendimento. Mas para garantir o investimento necessário, os empreendedores terão que convencer estes verdadeiros “tubarões” dos negócios.', 1, '2017-06-28 18:48:51', '2017-06-28 18:48:51'),
(2483, 185, 'Musical “Cartola – O Mundo é um Moinho” ', 'Direção e encenação de Roberto Lage', '2017', '201706301709109d03c1b5-9ceb-4952-9a11-7c3e6f46036c.jpg', 'Compositor de “Alvorada” e “As Rosas não Falam”, Cartola é um ícone da música brasileira. Considerado por diversos músicos e críticos como o maior sambista da história da música brasileira, agora é homenageado por suas próprias composições. Durante duas horas e meia, seus principais sucessos são revisitados pelos personagens, servindo de trilha para contar sua própria história. A dramaturgia é de Artur Xexéo, em pesquisa de Nilcemar Nogueira, neta do Poeta das rosas.', 0, '2017-06-30 20:50:49', '2017-06-30 20:50:49'),
(2493, 70, 'Programa Esquadrão da Moda', 'SBT', '2017', '20170630103240vanessa-rozan-tv.jpg', '', 0, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(2495, 194, 'Novela "Novo Mundo"', 'Direção de Vinicius Coimbra', '2017', '20170517104954whatsapp-image-2017-05-17-at-100324.jpeg', 'Personagem: Guarda', 0, '2017-07-11 17:57:13', '2017-07-11 17:57:13'),
(2504, 211, 'Musical “Carrossel” ', 'Com direção de Zé Henrique de Paula e Fernanda Maya', '2017', '20170714150622peca-carrossel.jpg', '', 0, '2017-07-17 00:35:26', '2017-07-17 00:35:26'),
(2505, 211, 'Musical “Alegria Alegria”', 'Roteiro e Direção de Moacyr Góes', '2017', '20170714150527alegria-alegria-teatro.jpg', '', 1, '2017-07-17 00:35:26', '2017-07-17 00:35:26'),
(2506, 211, 'Musical "Mamonas"', 'Direção do José Possi Neto', '2016', '20170714152810musical-mamonas.jpg', '', 2, '2017-07-17 00:35:26', '2017-07-17 00:35:26'),
(2507, 195, '"Na Luz do Samba"!', '', '2016', '20170716232234500x500.jpg', '', 0, '2017-07-17 02:22:52', '2017-07-17 02:22:52'),
(2508, 195, '"6º Solo"', '', '2011', '201707162312416-solo.jpg', '', 1, '2017-07-17 02:22:52', '2017-07-17 02:22:52'),
(2509, 195, '"Nega"', '', '2007', '201707162316261718-2-8.jpg', '', 2, '2017-07-17 02:22:52', '2017-07-17 02:22:52'),
(2510, 195, '"L.M."', '', '2004', '20170716231755a9fe893b56834df2bb002fc20ee8de28.jpg', '', 3, '2017-07-17 02:22:52', '2017-07-17 02:22:52'),
(2511, 195, '"Olha Pra Mim"', '', '2002', '20170716232107olha-pra-mim-w320.jpg', '', 4, '2017-07-17 02:22:52', '2017-07-17 02:22:52'),
(2512, 195, '"Assim que se faz"', '', '2000', '20170716231534luciana-mello-assim-que-se-faz.jpg', '', 5, '2017-07-17 02:22:52', '2017-07-17 02:22:52'),
(2513, 195, '"Luciana Rodrigues"', '', '1995', '20170716231443luciana-rodrigues-500x500.jpg', '', 6, '2017-07-17 02:22:52', '2017-07-17 02:22:52'),
(2516, 21, 'Amadores ', 'Leonardo Moreira', '2016', '2016053011522213125014-10206286358859483-4669138888287598587-n.jpg', '', 0, '2017-07-18 19:58:11', '2017-07-18 19:58:11'),
(2517, 21, 'O Jardim ', 'Leonardo Moreira', '2011', '20151020083500image.jpeg', '', 1, '2017-07-18 19:58:12', '2017-07-18 19:58:12'),
(2519, 207, 'O Rigor e a Misericórdia', '', '2016', '20170720182103o-rigor-e-a-misericordia.jpg', '', 0, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(2520, 207, 'Lobão Elétrico: Lino, Sexy & Brutal (Ao Vivo em SP) ', '', '2012', '20170720182457lobao-eletrico.jpg', '', 1, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(2521, 207, 'Acústico MTV', '', '2007', '20170720182736mtv.jpg', '', 2, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(2522, 207, 'Canções Dentro da Noite Escura ', '', '2005', '20170720182812niote-escura.jpg', '', 3, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(2523, 207, '2001: Uma Odisséia No Universo Paralelo ', '', '2001', '201707201829082001.png', '', 4, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(2524, 207, 'A Vida é Doce ', '', '1999', '20170720183057lobao.jpg', '', 5, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(2525, 207, 'Noite', '', '1998', '20170720183131noite.jpg', '', 6, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(2526, 207, 'Nostalgia da Modernidade ', '', '1995', '20170720183658mi0002775502.jpg', '', 7, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(2527, 207, 'O Inferno É Fogo ', '', '1991', '20170720183743o-inferno-e-fogo.jpg', '', 8, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(2528, 207, 'Vivo', '', '1990', '20170720183821vivo.jpg', '', 9, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(2529, 207, 'Lobão e os Presidentes - Sob o Sol de Parador ', '', '1989', '20170720184017alb-105150-big2.jpg', '', 10, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(2530, 207, 'Cuidado!  ', '', '1988', '20170720184049cuidado.jpg', '', 11, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(2531, 207, 'Vida Bandida ', '', '1987', '20170720184222lobaovidabandida.jpg', '', 12, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(2532, 207, 'O Rock Errou ', '', '1986', '20170720184259o-rock-errou.jpg', '', 13, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(2533, 207, 'Ronaldo Foi pra Guerra ', '', '1984', '20170720184438ronaldo-foi-a-guerra.jpg', '', 14, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(2534, 207, 'Cena de Cinema ', '', '1982', '20170720184528cena-de-cinema.jpg', '', 15, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(2580, 29, 'Pacto de Sangue', 'Adrian Caetano e Tomás Portella', '2017', '20161206194433whatsapp-image-2016-12-04-at-095619.jpeg', 'Série - Canal Space ', 0, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(2581, 29, 'Pescadora de Ilusão', 'Direção de Ge Petean', '2017', '20170426190646pescadora-de-ilusa-o-por-deborah-schcolnic1.png', 'Espetáculo infantil inspirado no livro “A Mulher Que Matou os Peixes” de Clarice Lispector', 1, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(2582, 29, 'Roque Santeiro, o Musical', 'Débora Dubois', '2017', '20170131144058mocinha.jpg', 'A atriz interpreta a personagem "Mocinha" no espetáculo que também conta com músicas compostas por Zeca Baleiro', 2, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(2583, 29, 'Magal e os Formigas ', 'Newton Cannito', '2016', '20161206193040whatsapp-image-2016-12-04-at-095551.jpeg', 'Magal e os Formigas é um filme de comédia-dramática brasileiro, que traz a história de um aposentado ranzinza que tem visões com o cantor Sidney Magal.', 3, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(2584, 29, 'Peer Gynt ', 'Gabriel Vilela ', '2016 ', '2017013114555814494784-1274956145857306-8329805941203257534-n.jpg', 'Peer Gynt é um jovem que adora contar histórias. Os textos são todos poéticos, de difícil execução inspirados no folclore norueguês.', 4, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(2585, 29, 'Cães Famintos ', 'Beto Oliveira ', '2016', '20170131144724caes-famintos-t207218.jpg', '', 5, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(2586, 29, 'Rita Lee Mora ao lado  -   O Musical', 'Débora  Dubois /  Marcio Macena ', '2015', '20151021212959rita-em-baixa-.jpg', 'Henrique Bartsch ', 6, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(2587, 29, 'Otelo ', 'Debora Dubois', '2015', '2015102121303812039617-509352409231242-302064239529931866-n.jpg', 'William Shakespeare', 7, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(2588, 29, 'Luz Negra', 'Espetáculo musical da Cia. Pessoal do Faroeste', '2015', '2015121013351512359891-1082872701732319-1146080751165436876-n.jpg', '', 8, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(2591, 213, 'Longa-Metragem: "Petit Mal"', 'Diretores: Lucas Camargo de Barros e Nicolas Thomé Zetune', '2016', '20170725172957e2191b5e-24b1-4955-9f2b-9eb403813122.jpg', ' É um filme sobre o amor em São Paulo e um afeto tão potente que transcende corpos e tempos. Personagem Protagonista -  Janaina ', 0, '2017-07-25 21:24:58', '2017-07-25 21:24:58');

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_extras`
--

CREATE TABLE `portfolio_extras` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_personalidades_id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portfolio_extras`
--

INSERT INTO `portfolio_extras` (`id`, `portfolio_personalidades_id`, `texto`, `created_at`, `updated_at`) VALUES
(569, 51, '<p><em><u><strong>Voc&ecirc; pode rever por aqui :</strong></u></em></p>\r\n\r\n<ul>\r\n	<li><em><u><strong>Manhattan Connection</strong></u></em></li>\r\n</ul>\r\n\r\n<p><a href="http://g1.globo.com/globo-news/manhattan-connection/videos/v/manhattan-pedro-andrade-confere-famosa-loja-de-decoracao-de-nova-york/4563278/"><em><u><strong>http://g1.globo.com/globo-news/manhattan-connection/videos/v/manhattan-pedro-andrade-confere-famosa-loja-de-decoracao-de-nova-york/4563278/</strong></u></em></a></p>\r\n\r\n<ul>\r\n	<li><u><strong>Programa do J&ocirc; - TV Globo</strong></u></li>\r\n</ul>\r\n\r\n<p><a href="http://globoplay.globo.com/v/3321453/"><u><strong>http://globoplay.globo.com/v/3321453/</strong></u></a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><u><strong>O Melhor Guia de NY</strong></u></li>\r\n</ul>\r\n\r\n<p><a href="http://globoplay.globo.com/v/3116129/">http://globoplay.globo.com/v/3116129/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n', '2016-03-07 19:48:12', '2016-03-07 19:48:12'),
(582, 24, '<p><u><em><strong>Voc&ecirc; pode rever por aqui todos os Epis&oacute;dios</strong></em></u>:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong>Chegadas e Partidas</strong></li>\r\n</ul>\r\n\r\n<p><a href="http://globosatplay.globo.com/gnt/chegadas-e-partidas/"><strong>http://globosatplay.globo.com/gnt/chegadas-e-partidas/</strong></a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong>Saia Justa</strong></li>\r\n</ul>\r\n\r\n<p><a href="http://globosatplay.globo.com/gnt/v/4571649/"><strong>http://globosatplay.globo.com/gnt/v/4571649/</strong></a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2016-03-17 01:34:46', '2016-03-17 01:34:46'),
(637, 66, '<p><u><em><strong>Voc&ecirc; pode rever por aqui as entrevistas</strong></em></u>:</p>\r\n\r\n<ul>\r\n	<li><strong>Programa do J&ocirc; Soares - Rede Globo</strong></li>\r\n</ul>\r\n\r\n<p>http://globoplay.globo.com/v/1384997/</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2016-04-18 18:35:56', '2016-04-18 18:35:56'),
(692, 139, '<p><strong>Aqui voc&ecirc; pode assistir entrevistas e dicas do&nbsp;Vic Meirelles: </strong></p>\r\n\r\n<ul>\r\n	<li>Dicas de decora&ccedil;&atilde;o natalina&nbsp;-&nbsp;Programa Mais&nbsp;Voc&ecirc;:&nbsp;<a href="http://globoplay.globo.com/v/4689112/">http://globoplay.globo.com/v/4689112/</a></li>\r\n</ul>\r\n\r\n<ul>\r\n	<li>Entrevista Que loucura &eacute; essa&nbsp;- TV Glamurama:&nbsp;<a href="https://www.youtube.com/watch?v=ZgL0jfiHNaU">https://www.youtube.com/watch?v=ZgL0jfiHNaU</a></li>\r\n</ul>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Plantas Artificiais Ganham Espa&ccedil;o na Decora&ccedil;&atilde;o e Conquistam pela Perfei&ccedil;&atilde;o:&nbsp;<a href="http://g1.globo.com/sao-paulo/bom-dia-sp/videos/t/quadro-verde/v/plantas-artificiais-ganham-espaco-na-decoracao-e-conquistam-pela-perfeicao/4925824/">http://g1.globo.com/sao-paulo/bom-dia-sp/videos/t/quadro-verde/v/plantas-artificiais-ganham-espaco-na-decoracao-e-conquistam-pela-perfeicao/4925824/</a></p>\r\n	</li>\r\n</ul>\r\n', '2016-05-24 18:09:57', '2016-05-24 18:09:57'),
(762, 144, '<ul>\r\n	<li><strong>Aqui voc&ecirc; assiste as materias com Danilo Vieira</strong>:</li>\r\n</ul>\r\n\r\n<p>Jornal Nacional</p>\r\n\r\n<p><a href="http://g1.globo.com/jornal-nacional/noticia/2016/04/taxistas-param-o-transito-do-rio-em-protesto-contra-o-aplicativo-uber.html" target="_blank">http://g1.globo.com/jornal-nacional/noticia/2016/04/taxistas-param-o-transito-do-rio-em-protesto-contra-o-aplicativo-uber.html</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Jornal da Globo</p>\r\n\r\n<p><a href="http://g1.globo.com/jornal-da-globo/edicoes/2016/01/07.html" target="_blank">http://g1.globo.com/jornal-da-globo/edicoes/2016/01/07.html</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Jornal Hoje</p>\r\n\r\n<p><a href="http://g1.globo.com/jornal-hoje/noticia/2015/09/carioca-de-17-anos-e-escolhida-para-representar-meninas-brasileiras-na-onu.html" target="_blank">http://g1.globo.com/jornal-hoje/noticia/2015/09/carioca-de-17-anos-e-escolhida-para-representar-meninas-brasileiras-na-onu.html</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Bom Dia Brasil</p>\r\n\r\n<p><a href="http://g1.globo.com/bom-dia-brasil/noticia/2015/09/confira-o-que-mudou-no-jeito-de-ouvir-musica-desde-o-primeiro-rock-rio.html" target="_blank">http://g1.globo.com/bom-dia-brasil/noticia/2015/09/confira-o-que-mudou-no-jeito-de-ouvir-musica-desde-o-primeiro-rock-rio.html</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>RJTV 2&ordm; Edi&ccedil;&atilde;o</p>\r\n\r\n<p><a href="http://g1.globo.com/rio-de-janeiro/noticia/2016/04/rjtv-filma-reacoes-e-conversas-com-estatua-de-drummond-assista.html" target="_blank">http://g1.globo.com/rio-de-janeiro/noticia/2016/04/rjtv-filma-reacoes-e-conversas-com-estatua-de-drummond-assista.html</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Fant&aacute;stico</p>\r\n\r\n<p><a href="http://g1.globo.com/fantastico/noticia/2015/09/fantastico-conta-historia-de-familias-unidas-pelo-amor-sob-o-mesmo-teto.html" target="_blank">http://g1.globo.com/fantastico/noticia/2015/09/fantastico-conta-historia-de-familias-unidas-pelo-amor-sob-o-mesmo-teto.html</a></p>\r\n', '2016-07-29 16:37:10', '2016-07-29 16:37:10'),
(816, 82, '<p><strong><em><u>Voc&ecirc; pode rever por aqui todos os Epis&oacute;dios&nbsp;</u></em></strong>:</p>\r\n\r\n<ul>\r\n	<li><strong><em>Saia Justa</em></strong></li>\r\n</ul>\r\n\r\n<p><a href="http://gnt.globo.com/programas/saia-justa/videos/3500724.htm">http://gnt.globo.com/programas/saia-justa/videos/3500724.htm</a></p>\r\n\r\n<p><a href="http://gnt.globo.com/programas/saia-justa/videos/5176550.htm">http://gnt.globo.com/programas/saia-justa/videos/5176550.htm</a></p>\r\n\r\n<p><strong><em><u>Voc&ecirc;&nbsp; pode ouvir por aqui todos os Epis&oacute;dios:</u></em></strong></p>\r\n\r\n<ul>\r\n	<li>Programa Alta Frequencia -&nbsp; R&aacute;dio BandNews FM</li>\r\n</ul>\r\n\r\n<p><a href="http://bandnewsfm.band.uol.com.br/Noticia.aspx?COD=757949&amp;Tipo=227">http://bandnewsfm.band.uol.com.br/Noticia.aspx?COD=757949&amp;Tipo=227</a></p>\r\n\r\n<ul>\r\n	<li><strong>Barbara Gancia visita&nbsp;</strong><strong>o &#39;bairro dos cornos&#39;&nbsp;</strong><strong>no&nbsp;</strong><strong>&#39;Saia por A&iacute;&#39;</strong></li>\r\n</ul>\r\n\r\n<p><a href="http://gnt.globo.com/programas/saia-justa/videos/5355250.htm" target="_blank">http://gnt.globo.com/programas/saia-justa/videos/5355250.htm</a></p>\r\n', '2016-10-13 13:29:50', '2016-10-13 13:29:50'),
(867, 175, '<p>Assista aqui a entrevista de Sergio Zobaran no Programa do J&ocirc;:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u>http://globoplay.globo.com/v/1270635</u></p>\r\n\r\n<p>&nbsp;</p>\r\n', '2016-12-07 19:28:50', '2016-12-07 19:28:50'),
(921, 90, '<ul>\r\n	<li><u><strong>Coluna Vogue Brasil</strong></u>:</li>\r\n</ul>\r\n\r\n<p><a href="http://vogue.globo.com/moda/moda-news/noticia/2012/11/costanza-pascolato-fala-sobre-estilo-e-beleza-garance-dore-video.html">http://vogue.globo.com/moda/moda-news/noticia/2012/11/costanza-pascolato-fala-sobre-estilo-e-beleza-garance-dore-video.html</a></p>\r\n\r\n<p><a href="http://caicodequeiroz.wordpress.com/2014/07/08/costanza-pascolato-para-vogue/">http://caicodequeiroz.wordpress.com/2014/07/08/costanza-pascolato-para-vogue/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong><u>Participa&ccedil;&atilde;o no Programa de Pedro Bial</u></strong></li>\r\n</ul>\r\n\r\n<p><a href="http://gnt.globo.com/programas/programa-com-bial/videos/5516598.htm">http://gnt.globo.com/programas/programa-com-bial/videos/5516598.htm</a></p>\r\n\r\n<p><a href="http://globosatplay.globo.com/gnt/v/5520010/">http://globosatplay.globo.com/gnt/v/5520010/</a></p>\r\n', '2017-01-27 18:57:06', '2017-01-27 18:57:06'),
(927, 173, '<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong>El Pais:&nbsp;</strong><strong><strong><strong><a href="http://brasil.elpais.com/brasil/2015/12/11/deportes/1449844174_656899.html">http://brasil.elpais.com/brasil/2015/12/11/deportes/1449844174_656899.html</a></strong></strong></strong></li>\r\n	<li><strong>HoffPos</strong>&nbsp;t<a href="http://www.brasilpost.com.br/2016/02/25/entrevista-corrupcao-fifa_n_9161824.html">http://www.brasilpost.com.br/2016/02/25/entrevista-corrupcao-fifa_n_9161824.html</a></li>\r\n	<li>Espn&nbsp;</li>\r\n	<li>REDETV</li>\r\n</ul>\r\n', '2017-01-27 20:43:07', '2017-01-27 20:43:07'),
(929, 140, '<ul>\r\n	<li><strong>Aqui voc&ecirc; pode ler e acompanhar a coluna do Le&atilde;o Serva na Folha:</strong></li>\r\n	<li><a href="http://www1.folha.uol.com.br/colunas/leaoserva/">http://www1.folha.uol.com.br/colunas/leaoserva/</a></li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong>Assista aqui algumas entrevistas com o Le&atilde;o Serva</strong>:</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Programa do JO: <a href="https://globoplay.globo.com/v/4222970/">https://globoplay.globo.com/v/4222970/</a></li>\r\n	<li>\r\n	<p>Morning Show: <a href="https://youtu.be/53IdUGwo4bg">https://youtu.be/53IdUGwo4bg</a></p>\r\n	</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-01-27 21:07:56', '2017-01-27 21:07:56'),
(935, 100, '<p><u><em><strong>Voc&ecirc; pode rever todos os Epis&oacute;dios por aqui:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li><u><em><strong>Canal MTV</strong></em></u></li>\r\n</ul>\r\n\r\n<p><a href="http://www.mtv.com.br/programas/movel/episodios/projota-no-mov3l/video/projota-rimando/"><u><em><strong>http://www.mtv.com.br/programas/movel/episodios/projota-no-mov3l/video/projota-rimando/</strong></em></u></a></p>\r\n\r\n<ul>\r\n	<li><strong><u><em>Parada Coca Cola&nbsp;</em></u></strong></li>\r\n</ul>\r\n\r\n<p><a href="http://www.mtv.com.br/programas/destaques/paradacocacola/videos/parada-coca-cola/">http://www.mtv.com.br/programas/destaques/paradacocacola/videos/parada-coca-cola/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-01-27 21:22:59', '2017-01-27 21:22:59'),
(940, 31, '<p><strong><em><u>Voc&ecirc; pode rever por aqui todos os Epis&oacute;dios:</u></em></strong></p>\r\n\r\n<ul>\r\n	<li><em><strong>Saia Justa</strong></em></li>\r\n</ul>\r\n\r\n<p><a href="http://globosatplay.globo.com/gnt/v/4476071/">http://globosatplay.globo.com/gnt/v/4476071/</a></p>\r\n\r\n<p><a href="http://gnt.globo.com/programas/saia-justa/videos/3500724.htm"><em><strong>http://gnt.globo.com/programas/saia-justa/videos/3500724.htm</strong></em></a></p>\r\n\r\n<p><a href="http://gnt.globo.com/programas/saia-justa/videos/5176550.htm">http://gnt.globo.com/programas/saia-justa/videos/5176550.htm</a></p>\r\n\r\n<ul>\r\n	<li><strong>Entrevista Programa do J&ocirc;</strong></li>\r\n</ul>\r\n\r\n<p><a href="http://globotv.globo.com/rede-globo/programa-do-jo/v/jo-entrevista-barbara-gancia/2463109/"><strong>http://globotv.globo.com/rede-globo/programa-do-jo/v/jo-entrevista-barbara-gancia/2463109/</strong></a></p>\r\n\r\n<ul>\r\n	<li><strong>Barbara Gancia visita&nbsp;</strong><strong>o &#39;bairro dos cornos&#39;&nbsp;</strong><strong>no&nbsp;</strong><strong>&#39;Saia por A&iacute;&#39;</strong></li>\r\n</ul>\r\n\r\n<p><a href="http://gnt.globo.com/programas/saia-justa/videos/5355250.htm" target="_blank">http://gnt.globo.com/programas/saia-justa/videos/5355250.htm</a></p>\r\n', '2017-01-27 22:52:05', '2017-01-27 22:52:05'),
(946, 65, '<p><em><u><strong>Voc&ecirc; pode ler por aqui todas a mat&eacute;rias:</strong></u></em></p>\r\n\r\n<ul>\r\n	<li><em><u><strong>O Estado de SP</strong></u></em></li>\r\n</ul>\r\n\r\n<p><a href="http://internacional.estadao.com.br/blogs/gustavo-chacra/sera-que-o-cubs-sera-campeao-apos-107-anos-como-previu-de-volta-para-o-futuro/=">http://internacional.estadao.com.br/blogs/gustavo-chacra/sera-que-o-cubs-sera-campeao-apos-107-anos-como-previu-de-volta-para-o-futuro/=</a></p>\r\n\r\n<p><u><em><strong>Aqui voc&ecirc; pode rever todos os Epis&oacute;dios</strong></em></u>:</p>\r\n\r\n<ul>\r\n	<li><strong><u><em>Em Pauta - Canal Globonews</em></u></strong></li>\r\n</ul>\r\n\r\n<p>&nbsp;<a href="http://g1.globo.com/globo-news/globo-news-em-pauta/videos/v/guga-chacra-da-dicas-de-viagem-para-londres/4433972/">http://g1.globo.com/globo-news/globo-news-em-pauta/videos/v/guga-chacra-da-dicas-de-viagem-para-londres/4433972/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-01-27 23:11:20', '2017-01-27 23:11:20'),
(947, 64, '<p><strong><em><u>Voc&ecirc; pode rever todos os Epis&oacute;dios Aqui:</u></em></strong></p>\r\n\r\n<ul>\r\n	<li><strong><em><u>Programa Mais Voc&ecirc; - Ana Maria Braga - TV Globo</u></em></strong></li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="http://gshow.globo.com/programas/mais-voce/videos/t/programas/v/felipe-suhre-conversa-com-o-casal-que-decidiu-parar-de-fumar-ao-mesmo-tempo/4566338/">http://gshow.globo.com/programas/mais-voce/videos/t/programas/v/felipe-suhre-conversa-com-o-casal-que-decidiu-parar-de-fumar-ao-mesmo-tempo/4566338/</a></p>\r\n', '2017-01-27 23:21:56', '2017-01-27 23:21:56'),
(998, 181, '<p><strong><u>Conhe&ccedil;a mais do Trabalho de Thiago Amaral na Imprensa:</u></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Revista Serafina</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="http://fotografia.folha.uol.com.br/galerias/20822-tres-thiagos#foto-338588">http://fotografia.folha.uol.com.br/galerias/20822-tres-thiagos#foto-338588</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Programa &quot;Inspira&ccedil;&otilde;es&quot;, Canal Arte 1&nbsp;</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="http://arte1.band.uol.com.br/thiago-amaral/">http://arte1.band.uol.com.br/thiago-amaral/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Programa &quot;Na Mira&quot;, Canal Arte 1</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="http://arte1.band.uol.com.br/na-nossa-mira-10/">http://arte1.band.uol.com.br/na-nossa-mira-10/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Cosmic Dance no Programa &quot;Estrelas&quot;, Rede Globo</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="https://globoplay.globo.com/v/5428956/">https://globoplay.globo.com/v/5428956/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Thiago e Cosmic Dance no Programa Bem Estar, Rede Globo </strong></u></p>\r\n\r\n<p><a href="http://g1.globo.com/bemestar/videos/t/edicoes/v/conheca-a-danca-indiana-que-exige-um-intenso-trabalho-corporal/3764826/">http://g1.globo.com/bemestar/videos/t/edicoes/v/conheca-a-danca-indiana-que-exige-um-intenso-trabalho-corporal/3764826/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Entrevista da Folha&nbsp;</strong></u></p>\r\n\r\n<p>http://www1.folha.uol.com.br/ilustrada/2015/09/1686054-arra-permite-testar-nocoes-estranhas-de-tempo-e-de-espaco.shtml</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(1020, 129, '<p><u><em><strong>Aqui voc&ecirc; pode rever todos o epis&oacute;dios da s&eacute;rie&nbsp;&quot;Do amor&quot;:</strong></em></u><br />\r\n<br />\r\n<strong>Canal &nbsp;Multishow</strong></p>\r\n\r\n<ul>\r\n	<li><a href="http://multishow.globo.com/programas/do-amor/videos/3078872.htm">http://multishow.globo.com/programas/do-amor/videos/3078872.htm</a>&nbsp;&nbsp;</li>\r\n	<li>\r\n	<p><a href="http://multishow.globo.com/programas/do-amor/videos/2969360.htm">http://multishow.globo.com/programas/do-amor/videos/2969360.htm</a></p>\r\n	</li>\r\n	<li>\r\n	<p><a href="http://multishow.globo.com/programas/do-amor/videos/2895124.htm">http://multishow.globo.com/programas/do-amor/videos/2895124.htm</a></p>\r\n	</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-02-20 03:13:01', '2017-02-20 03:13:01'),
(1022, 76, '<p><u><em><strong>Aqui poder&aacute; ouvir todos os epis&oacute;dios:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li><u><em><strong><a href="http://jovempanfm.bol.uol.com.br/podcasts/missao-impossivel/">http://jovempanfm.bol.uol.com.br/podcasts/missao-impossivel/</a></strong></em></u></li>\r\n</ul>\r\n', '2017-02-21 13:49:30', '2017-02-21 13:49:30'),
(1025, 178, '<p>Entrevista Programa J&ocirc; Soares</p>\r\n\r\n<p><a href="http://globoplay.globo.com/v/1268144/">http://globoplay.globo.com/v/1268144/</a></p>\r\n', '2017-03-08 02:55:15', '2017-03-08 02:55:15'),
(1028, 156, '<p><em><strong>Entrevista no Programa do J&ocirc; Soares&nbsp;</strong></em></p>\r\n\r\n<p><a href="https://globoplay.globo.com/v/3694831/">https://globoplay.globo.com/v/3694831/</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em><strong>Musical no Program do J&ocirc; Soares</strong></em></p>\r\n\r\n<p><a href="https://globoplay.globo.com/v/3694825/">https://globoplay.globo.com/v/3694825/</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em><strong>P&eacute; na Cova _ Personagem Adenoide </strong></em></p>\r\n\r\n<p><a href="https://globoplay.globo.com/v/2366344/">https://globoplay.globo.com/v/2366344/</a>&nbsp;</p>\r\n', '2017-03-10 21:35:31', '2017-03-10 21:35:31'),
(1061, 159, '<p><strong>Entrevista no Programa do J&ocirc; Soares&nbsp;</strong></p>\r\n\r\n<p><a href="https://globoplay.globo.com/v/4170355/">https://globoplay.globo.com/v/4170355/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-04-13 20:45:55', '2017-04-13 20:45:55'),
(1062, 167, '<p>C&eacute;sar Mello no Encontro com F&aacute;tima Bernardes</p>\r\n\r\n<p><a href="http://globoplay.globo.com/v/2530551/">http://globoplay.globo.com/v/2530551/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Mat&eacute;ria pra Uol</p>\r\n\r\n<p><a href="http://blogdoarcanjo.blogosfera.uol.com.br/2016/05/15/sucesso-nos-musicais-cesar-mello-quase-morreu-quando-era-bebe/">http://blogdoarcanjo.blogosfera.uol.com.br/2016/05/15/sucesso-nos-musicais-cesar-mello-quase-morreu-quando-era-bebe/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Mat&eacute;ria pro GShow</p>\r\n\r\n<p><a href="http://gshow.globo.com/novelas/lado-a-lado/por-tras-das-cameras/noticia/2012/10/cesar-mello-o-capoeira-chico-tem-corpo-esculpido-gracas-a-danca.html">http://gshow.globo.com/novelas/lado-a-lado/por-tras-das-cameras/noticia/2012/10/cesar-mello-o-capoeira-chico-tem-corpo-esculpido-gracas-a-danca.html</a></p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-04-18 21:39:14', '2017-04-18 21:39:14'),
(1071, 132, '<p><u><em><strong>Aqui voc&ecirc; pode rever o making of sobre a pe&ccedil;a, &quot;Politicamente Incorreto&quot;:</strong></em></u></p>\r\n\r\n<p><strong>Fox Play Brasil</strong></p>\r\n\r\n<ul>\r\n	<li>http://www.foxplaybrasil.com.br/watch/328268867630</li>\r\n</ul>\r\n', '2017-05-02 20:46:33', '2017-05-02 20:46:33'),
(1075, 54, '<p><em><u><strong>Confira aqui a entrevista de Gloria Coelho no Programa GNT Fashion</strong></u></em>:</p>\r\n\r\n<ul>\r\n	<li>Programa GNT Fashion</li>\r\n</ul>\r\n\r\n<p><a href="http://gnt.globo.com/programas/gnt-fashion/videos/2225528.htm">http://gnt.globo.com/programas/gnt-fashion/videos/2225528.htm</a></p>\r\n', '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(1080, 153, '<p><u><strong>Clique aqui e assista a atua&ccedil;&atilde;o de Fabio Ventura</strong></u>:</p>\r\n\r\n<p><a href="http://www.fm94.rj.gov.br/index.php/controladorprograma/visualizar/6858/74">http://www.fm94.rj.gov.br/index.php/controladorprograma/visualizar/6858/74</a></p>\r\n\r\n<p><a href="http://g1.globo.com/globo-news/estudio-i/videos/v/estreia-nesta-sexta-10-no-rio-a-versao-brasileira-do-musical-love-story/5085302/">http://g1.globo.com/globo-news/estudio-i/videos/v/estreia-nesta-sexta-10-no-rio-a-versao-brasileira-do-musical-love-story/5085302/</a></p>\r\n\r\n<p><a href="https://globoplay.globo.com/v/5084798/">https://globoplay.globo.com/v/5084798/</a></p>\r\n\r\n<p><a href="https://globoplay.globo.com/v/5119921/">https://globoplay.globo.com/v/5119921/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-05-17 18:39:39', '2017-05-17 18:39:39'),
(1082, 75, '<p><u><em><strong>Voc&ecirc; pode ouvir por aqui os programas</strong></em></u>:</p>\r\n\r\n<ul>\r\n	<li>Programa Alta Frequencia -&nbsp; R&aacute;dio BandNews FM</li>\r\n</ul>\r\n\r\n<p><a href="http://bandnewsfm.band.uol.com.br/Noticia.aspx?COD=757949&amp;Tipo=227">http://bandnewsfm.band.uol.com.br/Noticia.aspx?COD=757949&amp;Tipo=227</a></p>\r\n', '2017-05-17 18:47:46', '2017-05-17 18:47:46'),
(1083, 79, '<p><u><em><strong>Aqui voc&ecirc; poder&aacute; rever todos os Epis&oacute;dios:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Food Truck A Batalha - GNT</li>\r\n</ul>\r\n\r\n<p><a href="http://globosatplay.globo.com/gnt/v/3994120/">http://globosatplay.globo.com/gnt/v/3994120/</a></p>\r\n\r\n<p><a href="http://globosatplay.globo.com/gnt/v/4054729/">http://globosatplay.globo.com/gnt/v/4054729/</a></p>\r\n\r\n<p><a href="http://globosatplay.globo.com/gnt/v/4072433/">http://globosatplay.globo.com/gnt/v/4072433/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-05-17 18:49:24', '2017-05-17 18:49:24'),
(1084, 60, '<p><u><em><strong>Aqui voc&ecirc; poder&aacute; rever todos os Epis&oacute;dios</strong>:</em></u></p>\r\n\r\n<ul>\r\n	<li>Food Truck A Batalha</li>\r\n</ul>\r\n\r\n<p><a href="http://globosatplay.globo.com/gnt/v/3994120/">http://globosatplay.globo.com/gnt/v/3994120/</a></p>\r\n\r\n<p><a href="http://globosatplay.globo.com/gnt/v/4054729/">http://globosatplay.globo.com/gnt/v/4054729/</a></p>\r\n\r\n<p><a href="http://globosatplay.globo.com/gnt/v/4072433/">http://globosatplay.globo.com/gnt/v/4072433/</a></p>\r\n', '2017-05-17 18:51:50', '2017-05-17 18:51:50'),
(1086, 34, '<ul>\r\n	<li><u><strong>Coluna Vogue Brasil</strong></u>:</li>\r\n</ul>\r\n\r\n<p><a href="http://vogue.globo.com/moda/moda-news/noticia/2012/11/costanza-pascolato-fala-sobre-estilo-e-beleza-garance-dore-video.html">http://vogue.globo.com/moda/moda-news/noticia/2012/11/costanza-pascolato-fala-sobre-estilo-e-beleza-garance-dore-video.html</a></p>\r\n\r\n<p><a href="http://caicodequeiroz.wordpress.com/2014/07/08/costanza-pascolato-para-vogue/">http://caicodequeiroz.wordpress.com/2014/07/08/costanza-pascolato-para-vogue/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong><u>Participa&ccedil;&atilde;o no Programa de Pedro Bial</u></strong></li>\r\n</ul>\r\n\r\n<p><a href="http://gnt.globo.com/programas/programa-com-bial/videos/5516598.htm">http://gnt.globo.com/programas/programa-com-bial/videos/5516598.htm</a></p>\r\n\r\n<p><a href="http://globosatplay.globo.com/gnt/v/5520010/">http://globosatplay.globo.com/gnt/v/5520010/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(1087, 135, '<p><strong><u>Coluna Vogue Brasil</u></strong>:</p>\r\n\r\n<p><a href="http://vogue.globo.com/moda/moda-news/noticia/2012/11/costanza-pascolato-fala-sobre-estilo-e-beleza-garance-dore-video.html">http://vogue.globo.com/moda/moda-news/noticia/2012/11/costanza-pascolato-fala-sobre-estilo-e-beleza-garance-dore-video.html</a></p>\r\n\r\n<p><a href="http://caicodequeiroz.wordpress.com/2014/07/08/costanza-pascolato-para-vogue/">http://caicodequeiroz.wordpress.com/2014/07/08/costanza-pascolato-para-vogue/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong><u>Participa&ccedil;&atilde;o no Programa de Pedro Bial</u></strong></li>\r\n</ul>\r\n\r\n<p><a href="http://gnt.globo.com/programas/programa-com-bial/videos/5516598.htm">http://gnt.globo.com/programas/programa-com-bial/videos/5516598.htm</a></p>\r\n\r\n<p><a href="http://globosatplay.globo.com/gnt/v/5520010/">http://globosatplay.globo.com/gnt/v/5520010/</a></p>\r\n', '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(1088, 148, '<p>Para conhecer mais do Blog de&nbsp;Consuelo Blocker&nbsp;confira aqui algumas mat&eacute;rias:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="http://www.consueloblog.com/viajar-o-que-fazer-em-caso-de-imprevistos-grandes-imprevistos/">www.consueloblog.com/viajar-o-que-fazer-em-caso-de-imprevistos-grandes-imprevistos/</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="http://www.consueloblog.com/mulheres-que-inspiram-texto-para-o-blog-da-bontempo/">www.consueloblog.com/mulheres-que-inspiram-texto-para-o-blog-da-bontempo/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="http://www.consueloblog.com/as-minhas-dicas-de-como-conquistar-um-homem-e-segura-lo/">www.consueloblog.com/as-minhas-dicas-de-como-conquistar-um-homem-e-segura-lo/</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="http://www.consueloblog.com/fazer-um-pedido-uma-forca-maiornao-e-que-funciona/">www.consueloblog.com/fazer-um-pedido-uma-forca-maiornao-e-que-funciona/</a>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="http://www.consueloblog.com/minhas-razoes-para-ser-feliz-depois-dos-enta/">www.consueloblog.com/minhas-razoes-para-ser-feliz-depois-dos-enta/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(1092, 102, '<p><em><u><strong>Voc&ecirc; pode rever por aqui todos os Epis&oacute;dios</strong></u></em>:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong>Gnt Fashion &nbsp;</strong>​​</li>\r\n</ul>\r\n\r\n<p><a href="https://www.youtube.com/user/sitelilianpacce">https://www.youtube.com/user/sitelilianpacce</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong>GNT Fashion - Alemanha</strong></li>\r\n</ul>\r\n\r\n<p><a href="http://gnt.globo.com/programas/gnt-fashion/episodios/49270.htm">http://gnt.globo.com/programas/gnt-fashion/episodios/49270.htm</a></p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(1093, 186, '<p>Reveja aqui Bethy Largard&eacute;re &nbsp;</p>\r\n\r\n<p><a href="http://globoplay.globo.com/v/2993934/">http://globoplay.globo.com/v/2993934/</a></p>\r\n', '2017-05-22 18:19:01', '2017-05-22 18:19:01'),
(1095, 35, '<p><strong><em><u>Voc&ecirc; pode rever por aqui:</u></em></strong></p>\r\n\r\n<ul>\r\n	<li><u><em><strong>Entrevista no Programa do J&ocirc;</strong></em></u></li>\r\n</ul>\r\n\r\n<p><a href="http://globoplay.globo.com/v/4528487/"><u><em><strong>http://globoplay.globo.com/v/4528487/</strong></em></u></a></p>\r\n\r\n<ul>\r\n	<li><strong><u>Desafiados - Caldeir&atilde;o do Huck</u></strong></li>\r\n</ul>\r\n\r\n<p><a href="http://globoplay.globo.com/v/4593381/"><strong><u>http://globoplay.globo.com/v/4593381/</u></strong></a></p>\r\n', '2017-05-22 19:43:30', '2017-05-22 19:43:30'),
(1096, 128, '<p><u><strong>Aqui voc&ecirc; confere algumas mat&eacute;rias:</strong></u></p>\r\n\r\n<ul>\r\n	<li><strong><u>Canal Globo News</u></strong></li>\r\n</ul>\r\n\r\n<p><a href="http://g1.globo.com/globo-news/globo-news-em-pauta/videos/t/todos-os-videos/v/grupo-de-mulheres-encontra-alternativa-para-driblar-o-desemprego/4919852/">http://g1.globo.com/globo-news/globo-news-em-pauta/videos/t/todos-os-videos/v/grupo-de-mulheres-encontra-alternativa-para-driblar-o-desemprego/4919852/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="http://g1.globo.com/globo-news/globo-news-em-pauta/videos/v/mara-luquet-tira-duvidas-sobre-o-tesouro-direto/4340546/">http://g1.globo.com/globo-news/globo-news-em-pauta/videos/v/mara-luquet-tira-duvidas-sobre-o-tesouro-direto/4340546/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="http://g1.globo.com/globo-news/globo-news-em-pauta/videos/v/mara-luquet-tira-duvidas-sobre-investimento-e-viagens-internacionais/4517456/">http://g1.globo.com/globo-news/globo-news-em-pauta/videos/v/mara-luquet-tira-duvidas-sobre-investimento-e-viagens-internacionais/4517456/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong><u>TV Globo - Jornal da Globo</u></strong></li>\r\n</ul>\r\n\r\n<p><a href="http://globoplay.globo.com/v/3740728/">http://globoplay.globo.com/v/3740728/</a></p>\r\n\r\n<p><a href="http://globoplay.globo.com/v/2843614/">http://globoplay.globo.com/v/2843614/</a></p>\r\n\r\n<ul>\r\n	<li><u><strong>TV Globo - SPTV</strong></u></li>\r\n</ul>\r\n\r\n<p><a href="http://globoplay.globo.com/v/4210282/">http://globoplay.globo.com/v/4210282/</a></p>\r\n\r\n<ul>\r\n	<li><u><strong>Programa do J&ocirc;</strong></u></li>\r\n</ul>\r\n\r\n<p><a href="http://gshow.globo.com/programas/programa-do-jo/episodio/2016/04/22/jo-soares-entrevista-matheus-nachtergaele-e-mara-luquet.html#video-4976233">http://gshow.globo.com/programas/programa-do-jo/episodio/2016/04/22/jo-soares-entrevista-matheus-nachtergaele-e-mara-luquet.html#video-4976233</a></p>\r\n\r\n<ul>\r\n	<li><strong>Meninas do J&ocirc;</strong></li>\r\n</ul>\r\n\r\n<p><a href="http://gshow.globo.com/programas/programa-do-jo/episodio/2016/05/18/jo-soares-recebe-jornalistas-para-debate-sobre-politica.html#video-5033747">http://gshow.globo.com/programas/programa-do-jo/episodio/2016/05/18/jo-soares-recebe-jornalistas-para-debate-sobre-politica.html#video-5033747</a></p>\r\n\r\n<ul>\r\n	<li><u><strong>Pr&ecirc;mios Recebidos:</strong></u></li>\r\n</ul>\r\n\r\n<p><strong>Medalha Ruth Cardoso</strong> para as entidades e personalidades que se destacaram na luta pelos direitos femininos em S&atilde;o Paulo. O evento &eacute; promovido pelo Conselho Estadual da Condi&ccedil;&atilde;o Feminina da Secretaria da Justi&ccedil;a e da Defesa da Cidadania.</p>\r\n\r\n<p><strong>Pr&ecirc;mio Corecon Jornalista do ano 2014 &ndash; </strong>Conselho dos Economistas do Brasil e Conselho Regional de Economia de S&atilde;o Paulo</p>\r\n\r\n<p><strong>Pr&ecirc;mio Abrapp de jornalismo 2013</strong></p>\r\n\r\n<p><strong>Pr&ecirc;mio F&oacute;rum Longevidade Bradesco 2012</strong></p>\r\n\r\n<p><strong>Trof&eacute;u Mulher Imprensa</strong> &ndash; vencedora da categoria comentarista de r&aacute;dio da oitava edi&ccedil;&atilde;o.</p>\r\n\r\n<p><strong>Pr&ecirc;mio Bovespa - </strong>Primeira colocada do pr&ecirc;mio por dois anos consecutivos, 2002 e 2003</p>\r\n\r\n<p><strong>Finalista Pr&ecirc;mio Esso de jornalismo Econ&ocirc;mico em 2001</strong></p>\r\n\r\n<p><strong>Pr&ecirc;mio Apimec S&atilde;o Paulo como editora do caderno FolhaInvest da Folha de S&atilde;o Paulo em 1998 e do caderno Eu&amp; Investimentos do Valor Econ&ocirc;mico em 2000</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-05-29 17:29:27', '2017-05-29 17:29:27'),
(1097, 134, '<p><u><strong>Aqui voc&ecirc; confere algumas mat&eacute;rias e entrevista</strong></u><u><strong>:</strong></u></p>\r\n\r\n<ul>\r\n	<li><strong><u>Canal Globo News&nbsp;</u></strong></li>\r\n</ul>\r\n\r\n<p><a href="http://g1.globo.com/globo-news/globo-news-em-pauta/videos/t/todos-os-videos/v/grupo-de-mulheres-encontra-alternativa-para-driblar-o-desemprego/4919852/">http://g1.globo.com/globo-news/globo-news-em-pauta/videos/t/todos-os-videos/v/grupo-de-mulheres-encontra-alternativa-para-driblar-o-desemprego/4919852/</a></p>\r\n\r\n<p><a href="http://g1.globo.com/globo-news/globo-news-em-pauta/videos/v/mara-luquet-tira-duvidas-sobre-o-tesouro-direto/4340546/">http://g1.globo.com/globo-news/globo-news-em-pauta/videos/v/mara-luquet-tira-duvidas-sobre-o-tesouro-direto/4340546/</a></p>\r\n\r\n<p><a href="http://g1.globo.com/globo-news/globo-news-em-pauta/videos/v/mara-luquet-tira-duvidas-sobre-investimento-e-viagens-internacionais/4517456/">http://g1.globo.com/globo-news/globo-news-em-pauta/videos/v/mara-luquet-tira-duvidas-sobre-investimento-e-viagens-internacionais/4517456/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong><u>TV Globo - Jornal da Globo</u></strong></li>\r\n</ul>\r\n\r\n<p><a href="http://globoplay.globo.com/v/3740728/">http://globoplay.globo.com/v/3740728/</a></p>\r\n\r\n<p><a href="http://globoplay.globo.com/v/2843614/">http://globoplay.globo.com/v/2843614/</a></p>\r\n\r\n<ul>\r\n	<li><u><strong>TV Globo - SPTV</strong></u></li>\r\n</ul>\r\n\r\n<p><a href="http://globoplay.globo.com/v/4210282/">http://globoplay.globo.com/v/4210282/</a></p>\r\n\r\n<ul>\r\n	<li><strong><u>Programa do J&ocirc;</u></strong></li>\r\n</ul>\r\n\r\n<p><a href="http://gshow.globo.com/programas/programa-do-jo/episodio/2016/04/22/jo-soares-entrevista-matheus-nachtergaele-e-mara-luquet.html#video-4976233">http://gshow.globo.com/programas/programa-do-jo/episodio/2016/04/22/jo-soares-entrevista-matheus-nachtergaele-e-mara-luquet.html#video-4976233</a></p>\r\n\r\n<ul>\r\n	<li><strong>Meninas do J&ocirc;</strong></li>\r\n</ul>\r\n\r\n<p><a href="http://gshow.globo.com/programas/programa-do-jo/episodio/2016/04/20/meninas-do-jo-falam-sobre-situacao-politica-do-brasil.html#video-4971426">http://gshow.globo.com/programas/programa-do-jo/episodio/2016/04/20/meninas-do-jo-falam-sobre-situacao-politica-do-brasil.html#video-4971426</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><u><strong>Pr&ecirc;mios Recebidos:</strong></u></li>\r\n</ul>\r\n\r\n<p><strong>Medalha Ruth Cardoso</strong> para as entidades e personalidades que se destacaram na luta pelos direitos femininos em S&atilde;o Paulo. O evento &eacute; promovido pelo Conselho Estadual da Condi&ccedil;&atilde;o Feminina da Secretaria da Justi&ccedil;a e da Defesa da Cidadania.</p>\r\n\r\n<p><strong>Pr&ecirc;mio Corecon Jornalista do ano 2014 &ndash; </strong>Conselho dos Economistas do Brasil e Conselho Regional de Economia de S&atilde;o Paulo</p>\r\n\r\n<p><strong>Pr&ecirc;mio Abrapp de jornalismo 2013</strong></p>\r\n\r\n<p><strong>Pr&ecirc;mio F&oacute;rum Longevidade Bradesco 2012</strong></p>\r\n\r\n<p><strong>Trof&eacute;u Mulher Imprensa</strong> &ndash; vencedora da categoria comentarista de r&aacute;dio da oitava edi&ccedil;&atilde;o.</p>\r\n\r\n<p><strong>Pr&ecirc;mio Bovespa - </strong>Primeira colocada do pr&ecirc;mio por dois anos consecutivos, 2002 e 2003</p>\r\n\r\n<p><strong>Finalista Pr&ecirc;mio Esso de jornalismo Econ&ocirc;mico em 2001</strong></p>\r\n\r\n<p><strong>Pr&ecirc;mio Apimec S&atilde;o Paulo como editora do caderno FolhaInvest da Folha de S&atilde;o Paulo em 1998 e do caderno Eu&amp; Investimentos do Valor Econ&ocirc;mico em 2000</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-05-29 17:29:44', '2017-05-29 17:29:44'),
(1098, 172, '<p>Jeronimo como GIBA em Haja Cora&ccedil;&atilde;o<br />\r\n&nbsp;</p>\r\n\r\n<p><a href="https://globoplay.globo.com/v/5284551/">https://globoplay.globo.com/v/5284551/</a><br />\r\n<a href="https://globoplay.globo.com/v/5286868/">https://globoplay.globo.com/v/5286868/</a><br />\r\n<a href="https://globoplay.globo.com/v/5126646/">https://globoplay.globo.com/v/5126646/</a><br />\r\n<a href="https://globoplay.globo.com/v/5234124/">https://globoplay.globo.com/v/5234124/</a></p>\r\n\r\n<p><a href="https://globoplay.globo.com/v/5131805/">https://globoplay.globo.com/v/5131805/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-05-31 19:26:16', '2017-05-31 19:26:16'),
(1102, 182, '<p><u><strong>Voc&ecirc; pode conferir as cenas da Novela <em>Rock Story aqui:</em></strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="http://globoplay.globo.com/v/5620005/">http://globoplay.globo.com/v/5620005/</a><br />\r\n<a href="http://globoplay.globo.com/v/5535317/">http://globoplay.globo.com/v/5535317/</a><br />\r\n<a href="http://globoplay.globo.com/v/5537687/">http://globoplay.globo.com/v/5537687/</a></p>\r\n', '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(1103, 56, '<p><strong><em><u>Voc&ecirc; pode assistir aqui um pouco sobre Pedro Louren&ccedil;o:</u></em></strong></p>\r\n\r\n<ul>\r\n	<li><strong><em>Entrevista&nbsp; Programa do J&ocirc;</em></strong></li>\r\n</ul>\r\n\r\n<p><a href="http://globotv.globo.com/rede-globo/programa-do-jo/v/estilista-pedro-lourenco-leva-a-moda-brasileira-a-paris/1394339/"><strong><em><u>http://globotv.globo.com/rede-globo/programa-do-jo/v/estilista-pedro-lourenco-leva-a-moda-brasileira-a-paris/1394339/</u></em></strong></a></p>\r\n', '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(1107, 33, '<p><strong><em><u>Voc&ecirc; pode rever por aqui todos os epis&oacute;dios:</u></em></strong></p>\r\n\r\n<ul>\r\n	<li>Estilo GNT</li>\r\n</ul>\r\n\r\n<p><a href="http://gnt.globo.com/moda/materias/chiara-gadaleta-ensina-a-usar-vestidos-longos.htm">http://gnt.globo.com/moda/materias/chiara-gadaleta-ensina-a-usar-vestidos-longos.htm</a></p>\r\n', '2017-06-30 13:37:53', '2017-06-30 13:37:53'),
(1116, 55, '<p><em><u><strong>Voc&ecirc; pode rever por aqui todos os Epis&oacute;dios</strong></u></em>:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong>Gnt Fashion &nbsp;</strong>​​</li>\r\n</ul>\r\n\r\n<p><a href="https://www.youtube.com/user/sitelilianpacce">https://www.youtube.com/user/sitelilianpacce</a></p>\r\n\r\n<ul>\r\n	<li><strong>GNT Fashion - Alemanha</strong></li>\r\n</ul>\r\n\r\n<p><a href="http://gnt.globo.com/programas/gnt-fashion/episodios/49270.htm">http://gnt.globo.com/programas/gnt-fashion/episodios/49270.htm</a></p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-07-07 13:10:58', '2017-07-07 13:10:58'),
(1117, 92, '<p><em><u><strong>Voc&ecirc; pode rever por aqui todos os Epis&oacute;dios</strong></u></em>:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong>Gnt Fashion &nbsp;</strong>​​</li>\r\n</ul>\r\n\r\n<p><a href="https://www.youtube.com/user/sitelilianpacce">https://www.youtube.com/user/sitelilianpacce</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong>GNT Fashion Alemanha</strong></li>\r\n</ul>\r\n\r\n<p><a href="http://gnt.globo.com/programas/gnt-fashion/episodios/49270.htm">http://gnt.globo.com/programas/gnt-fashion/episodios/49270.htm</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-07-07 13:12:46', '2017-07-07 13:12:46'),
(1118, 145, '<p><em><u><strong>Voc&ecirc; pode rever por aqui todos os Epis&oacute;dios</strong></u></em>:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong>Gnt Fashion &nbsp;</strong>​​</li>\r\n</ul>\r\n\r\n<p><a href="https://www.youtube.com/user/sitelilianpacce">https://www.youtube.com/user/sitelilianpacce</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong>GNT Fashion Alemanha</strong></li>\r\n</ul>\r\n\r\n<p><a href="http://gnt.globo.com/programas/gnt-fashion/episodios/49270.htm">http://gnt.globo.com/programas/gnt-fashion/episodios/49270.htm</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-07-07 13:14:24', '2017-07-07 13:14:24'),
(1122, 208, '<p><u>Voc&ecirc; pode rever por aqu</u>i:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>- Fant&aacute;stico - TV Globo&nbsp;</p>\r\n\r\n<p>S&eacute;rie sobre Transg&ecirc;neros - Primeira vez que o assunto &eacute; tratado na TV aberta</p>\r\n\r\n<p><a href="http://especiais.g1.globo.com/fantastico/2017/quem-sou-eu/">http://especiais.g1.globo.com/fantastico/2017/quem-sou-eu/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>- Programa Ana Maria Braga</p>\r\n\r\n<p>Renata Ceribelli se diverte com incidente com le&atilde;o durante V&iacute;deo Show</p>\r\n\r\n<p><a href="https://globoplay.globo.com/v/2871782/">https://globoplay.globo.com/v/2871782/</a> &nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>- Programa Mar&iacute;lia Gabriela entrevista&nbsp;&nbsp;GNT</p>\r\n\r\n<p>Renata Ceribelli fala sobre o trabalho no Fant&aacute;stico</p>\r\n\r\n<p><a href="http://gnt.globo.com/programas/marilia-gabriela-entrevista/videos/1848952.htm">http://gnt.globo.com/programas/marilia-gabriela-entrevista/videos/1848952.htm</a> &nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-07-10 17:11:12', '2017-07-10 17:11:12'),
(1123, 77, '<p><u><em><strong>Voc&ecirc; pode rever todos os Epis&oacute;dio aqui:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>GNT</li>\r\n</ul>\r\n\r\n<p><a href="http://gnt.globo.com/moda/videos/1403692.htm">http://gnt.globo.com/moda/videos/1403692.htm</a></p>\r\n', '2017-07-12 20:16:07', '2017-07-12 20:16:07'),
(1129, 209, '<p><strong>Conhe&ccedil;a mais do trabalho de Tiago Scheuer atrav&eacute;s dos links abaixo:</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="http://g1.globo.com/jornal-hoje/videos/t/edicoes/v/chuva-continua-concentrada-no-norte-e-no-litoral-do-nordeste/5984520/">http://g1.globo.com/jornal-hoje/videos/t/edicoes/v/chuva-continua-concentrada-no-norte-e-no-litoral-do-nordeste/5984520/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="http://g1.globo.com/jornal-nacional/videos/t/edicoes/v/previsao-e-de-chuva-em-varias-partes-do-brasil-no-domingo-20/5884296/">http://g1.globo.com/jornal-nacional/videos/t/edicoes/v/previsao-e-de-chuva-em-varias-partes-do-brasil-no-domingo-20/5884296/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="http://g1.globo.com/hora1/videos/t/edicoes/v/confira-a-previsao-do-tempo-para-a-sexta-feira-26/5895712/">http://g1.globo.com/hora1/videos/t/edicoes/v/confira-a-previsao-do-tempo-para-a-sexta-feira-26/5895712/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-07-18 18:33:06', '2017-07-18 18:33:06'),
(1133, 89, '<p>Designboom: www.designboom.com/tag/estudio-guto-requena/</p>\r\n\r\n<p>Archdaily: http://www.archdaily.com.br/br/tag/estudio-guto-requena</p>\r\n\r\n<p>The Creators Project: http://thecreatorsproject.vice.com/article/gsearch?query=guto+requena</p>\r\n\r\n<p>Freude Von Freuden: www.freundevonfreunden.com/interviews/guto-requena/</p>\r\n', '2017-07-20 13:35:13', '2017-07-20 13:35:13'),
(1135, 52, '<ul>\r\n	<li>Designboom: www.designboom.com/tag/estudio-guto-requena/</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Archdaily: http://www.archdaily.com.br/br/tag/estudio-guto-requena</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>The Creators Project: http://thecreatorsproject.vice.com/article/gsearch?query=guto+requena</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Freude Von Freuden: www.freundevonfreunden.com/interviews/guto-requena/</li>\r\n</ul>\r\n', '2017-07-20 14:01:07', '2017-07-20 14:01:07'),
(1136, 58, '<p><u><em><strong>Aqui voc&ecirc; poder&aacute; rever os programas:</strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Encontro com F&aacute;tima Bernardes - TV Globo</li>\r\n</ul>\r\n\r\n<p><a href="http://globoplay.globo.com/v/3980578/">http://globoplay.globo.com/v/3980578/</a></p>\r\n\r\n<ul>\r\n	<li>&nbsp;Programa The Taste Brasil - Canal GNT</li>\r\n</ul>\r\n\r\n<p><a href="http://gnt.globo.com/programas/the-taste-brasil/videos/4189374.htm">http://gnt.globo.com/programas/the-taste-brasil/videos/4189374.htm</a></p>\r\n', '2017-07-20 14:20:45', '2017-07-20 14:20:45'),
(1137, 67, '<p><u><em><strong>Voc&ecirc; pode rever todos os Epis&oacute;dios aqui</strong></em></u>:</p>\r\n\r\n<ul>\r\n	<li><u><strong>&quot;Entre Aspas&quot; - Canal Globonews</strong></u></li>\r\n</ul>\r\n\r\n<p><a href="http://globosatplay.globo.com/globonews/v/3157867/">http://globosatplay.globo.com/globonews/v/3157867/</a></p>\r\n', '2017-07-24 18:47:03', '2017-07-24 18:47:03'),
(1143, 206, '<p><strong><u>Voc&ecirc; pode rever aqui alguns programas do GloboNews:</u></strong></p>\r\n\r\n<p>-&nbsp;Trump teria decidido tirar EUA do Acordo de Paris sobre o clima</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>http://g1.globo.com/globo-news/jornal-globo-news/videos/t/edicao-das-10h/v/trump-teria-decidido-tirar-eua-do-acordo-de-paris-sobre-o-clima/5907068/<br />\r\n&nbsp;</p>\r\n\r\n<p>-&nbsp;CUT &eacute; liberada para fazer atos pelo Dia do Trabalho na Avenida Paulista</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>http://g1.globo.com/globo-news/jornal-globo-news/videos/t/videos/v/cut-e-liberada-para-fazer-atos-pelo-dia-do-trabalho-na-avenida-paulista/5836766/</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>-&nbsp;Atos pol&iacute;ticos e shows marcam a festa do Dia do Trabalhador em S&atilde;o Paulo</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>http://g1.globo.com/globo-news/jornal-globo-news/videos/t/videos/v/atos-politicos-e-shows-marcam-a-festa-do-dia-do-trabalhador-em-sao-paulo/5838674/</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>-&nbsp;Posto de combust&iacute;vel de SP vai vender gasolina sem impostos</p>\r\n\r\n<p>http://g1.globo.com/globo-news/jornal-globo-news/videos/t/edicao-das-10h/v/posto-de-combustivel-de-sp-vai-vender-gasolina-sem-impostos/5920218/</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>-&nbsp;Trump cobra proposta mais dura sobre a proibi&ccedil;&atilde;o de entrada de imigrantes nos EUA</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>http://g1.globo.com/globo-news/jornal-globo-news/videos/t/edicao-das-10h/v/trump-cobra-proposta-mais-dura-sobre-a-proibicao-de-entrada-de-imigrantes-nos-eua/5917524/</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>-&nbsp;PF faz opera&ccedil;&atilde;o contra crimes eleitorais em campanha para Prefeitura de S&atilde;o Paulo</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>http://g1.globo.com/globo-news/jornal-globo-news/videos/v/pf-faz-operacao-contra-crimes-eleitorais-em-campanha-para-prefeitura-de-sao-paulo/5909570/</p>\r\n', '2017-07-24 20:18:25', '2017-07-24 20:18:25'),
(1149, 29, '<ul>\r\n	<li><u><strong>Assista aqui a entrevista da Mel Lisboa no &quot;Programa do J&ocirc;&quot;:</strong></u></li>\r\n</ul>\r\n\r\n<p><a href="https://globoplay.globo.com/v/4603035/">https://globoplay.globo.com/v/4603035/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><strong><u>Aqui voc&ecirc; pode conferir a entrevista da Mel Lisboa no programa &quot;Morning Show&quot; - &nbsp;Radio Jovem Pan:</u></strong></li>\r\n</ul>\r\n\r\n<p><a href="https://soundcloud.com/ca-co-de-queiroz/mel-lisboa3003b">https://soundcloud.com/ca-co-de-queiroz/mel-lisboa3003b</a></p>\r\n\r\n<p><a href="https://soundcloud.com/ca-co-de-queiroz/mel-lisboa3003a">https://soundcloud.com/ca-co-de-queiroz/mel-lisboa3003a</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(1150, 147, '<ul>\r\n	<li><strong>Aqui voc&ecirc; confere a consultoria de Bia Paes de Barros;</strong></li>\r\n	<li><u>Programa Um Armario Mil Looks &ndash; Look todo branco</u></li>\r\n</ul>\r\n\r\n<p><a href="http://gnt.globo.com/moda/videos/4485997.htm">http://gnt.globo.com/moda/videos/4485997.htm</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><u>Programa Um Armario Mil Looks - Como usar o t&ecirc;nis de forma sofisticada</u></li>\r\n</ul>\r\n\r\n<p><a href="http://gnt.globo.com/moda/videos/4455167.htm">http://gnt.globo.com/moda/videos/4455167.htm</a></p>\r\n', '2017-07-25 13:43:56', '2017-07-25 13:43:56'),
(1152, 143, '<p><strong>Aqui voc&ecirc; conhece a coluna da Patricia Campos Mello&nbsp;na Folha de S&atilde;o Paulo:</strong></p>\r\n\r\n<p><a href="http://www1.folha.uol.com.br/colunas/patriciacamposmello/" target="_blank">http://www1.folha.uol.com.br/colunas/patriciacamposmello/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Acesse os links e confira as mat&eacute;rias jornal&iacute;sticas com Patricia Campos Mello</strong>:</p>\r\n\r\n<p><a href="http://zh.clicrbs.com.br/rs/entretenimento/noticia/2016/04/14-flip-aposta-em-autores-que-vao-alem-da-ficcao-5786154.html">http://zh.clicrbs.com.br/rs/entretenimento/noticia/2016/04/14-flip-aposta-em-autores-que-vao-alem-da-ficcao-5786154.html</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="http://portalimprensa.com.br/noticias/ultimas_noticias/76759/a++boa+e+velha+reportagem+ainda+e+essencial+no+jornalismo+diz+patricia+campos+mello">http://portalimprensa.com.br/noticias/ultimas_noticias/76759/a++boa+e+velha+reportagem+ainda+e+essencial+no+jornalismo+diz+patricia+campos+mello</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="http://www.blogdacompanhia.com.br/tag/patricia-campos-mello/">http://www.blogdacompanhia.com.br/tag/patricia-campos-mello/</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a href="http://www1.folha.uol.com.br/ilustrada/2016/04/1761355-flip-anuncia-mesa-sobre-guerra-da-siria-e-refugiados-com-poeta-do-pais.shtml">http://www1.folha.uol.com.br/ilustrada/2016/04/1761355-flip-anuncia-mesa-sobre-guerra-da-siria-e-refugiados-com-poeta-do-pais.shtml</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n', '2017-07-25 14:18:09', '2017-07-25 14:18:09');

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_fotos`
--

CREATE TABLE `portfolio_fotos` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_personalidades_id` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portfolio_fotos`
--

INSERT INTO `portfolio_fotos` (`id`, `portfolio_personalidades_id`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(7972, 51, '20151209211751pedro-andrade-8.JPG', 0, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7973, 51, '20150928162209Pedro 2948.jpg', 1, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7974, 51, '20151209215556mg-1416.jpeg', 2, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7975, 51, '20151209211751pedro-andrade-4.JPG', 3, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7976, 51, '20151209215256mg-1189.jpeg', 4, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7977, 51, '20151209215748foto01.JPG', 5, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7978, 51, '201512092156493z8p6060.jpeg', 6, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7979, 51, '2015120921575111659552-10153512829306495-2617190607661582871-n.jpg', 7, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7980, 51, '20150909163715Pedro 3025.jpg', 8, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7981, 51, '20151209211750pedro-andrade-7.JPG', 9, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7982, 51, '20151209215816img-1528.jpeg', 10, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7983, 51, '20150909164115_MG_3650.jpg', 11, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7984, 51, '20151209215551mg-1402.jpeg', 12, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7985, 51, '20151209211750pedro-andrade-6.JPG', 13, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7986, 51, '20160204125037a5a5519.JPG', 14, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7987, 51, '20151209215552mg-1353.jpeg', 15, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7988, 51, '20151209215821pedro-andrade-06.jpeg', 16, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7989, 51, '20151209215750img-6269.jpeg', 17, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7990, 51, '20160215171316screenshot-2016-02-05-07-49-12.png', 18, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7991, 51, '20151209215747mg-1507.jpeg', 19, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7992, 51, '20160204125040a5a7180.JPG', 20, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7993, 51, '20151209215754img-6279.PNG', 21, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7994, 51, '20151209215824pedro-andrade-nova-york-perfil.jpeg', 22, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7995, 51, '20151209215821pedro-andrade-04.jpeg', 23, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7996, 51, '20151209215824img-6280.jpeg', 24, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7997, 51, '20151209215859mg-1543.jpeg', 25, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7998, 51, '20160216124601140451403.jpeg', 26, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(7999, 51, '20151209215831pedro-andrade-copy.jpeg', 27, '2016-03-07 19:48:11', '2016-03-07 19:48:11'),
(8064, 24, '20160304163433astrid-fotos.jpg', 0, '2016-03-17 01:34:46', '2016-03-17 01:34:46'),
(8065, 24, '20160304152755astrid-fontenelle-9778-1.jpg', 1, '2016-03-17 01:34:46', '2016-03-17 01:34:46'),
(8066, 24, '20160304152759astrid-fontenelle-4.jpg', 2, '2016-03-17 01:34:46', '2016-03-17 01:34:46'),
(8067, 24, '20160307133433saia-justa-verao-2016-astrid-creditos-eliana-rodrigues-6.jpg', 3, '2016-03-17 01:34:46', '2016-03-17 01:34:46'),
(8806, 142, '20160429125427img-4709edit.jpg', 0, '2016-05-04 16:19:50', '2016-05-04 16:19:50'),
(8807, 71, '20151202175900duda-molinos-revista-iguatemi-ed-53-04.jpg', 0, '2016-05-04 18:54:28', '2016-05-04 18:54:28'),
(8808, 71, '20151203112731sem-titulo-008912.jpg', 1, '2016-05-04 18:54:28', '2016-05-04 18:54:28'),
(8809, 71, '20151202175922isabeli-schon8.jpg', 2, '2016-05-04 18:54:28', '2016-05-04 18:54:28'),
(8810, 71, '20151202175829128-leticia-sabatella-1.jpg', 3, '2016-05-04 18:54:28', '2016-05-04 18:54:28'),
(8811, 71, '2015120217580309.jpg', 4, '2016-05-04 18:54:28', '2016-05-04 18:54:28'),
(8812, 71, '20151202175850digitalizar0028.jpg', 5, '2016-05-04 18:54:28', '2016-05-04 18:54:28'),
(8813, 71, '20151202180006revista-estilo-paola-01.jpg', 6, '2016-05-04 18:54:28', '2016-05-04 18:54:28'),
(8814, 71, '20151207130216editorial-duda-molinos-2.jpg', 7, '2016-05-04 18:54:28', '2016-05-04 18:54:28'),
(8815, 71, '20151202175955feature-capa-bzart26.jpg', 8, '2016-05-04 18:54:28', '2016-05-04 18:54:28'),
(8816, 71, '20151202175909batom-008158.jpg', 9, '2016-05-04 18:54:28', '2016-05-04 18:54:28'),
(8817, 71, '20151203112730vogue-pv035372.jpg', 10, '2016-05-04 18:54:28', '2016-05-04 18:54:28'),
(8818, 71, '2015121016190012047179-909596669089766-723658295467887448-n.jpg', 11, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(8819, 71, '20151202175918edu-5731.jpg', 12, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(8820, 71, '20151202180040p45-009472pb.jpg', 13, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(8821, 71, '2015120217594202.jpg', 14, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(8822, 71, '20151207130217editorial-duda-molinos-3-3.jpg', 15, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(8823, 71, '2015120712332920151204134836el324-beleza-ensaiomakemai2015-3.jpg', 16, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(8824, 71, '20151202180042capture-013419.jpg', 17, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(8825, 71, '20151203112639sem-titulo-000225.jpg', 18, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(8826, 71, '20151202175950capture-013472.jpg', 19, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(8827, 71, '20151202180018p45-002971.jpg', 20, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(8828, 71, '201512071537461453307-590352607680842-805384803-n.jpg', 21, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(8829, 71, '20151207031943ellw-editotal.jpg', 22, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(8830, 71, '20151203112727sem-titulo-009063.jpg', 23, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(8831, 71, '20151202180121p45-002993.jpg', 24, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(8832, 71, '20151202180028revista-vogue-juliana-paes-08.jpg', 25, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(8833, 71, '2015120712332920151204134832el324-beleza-ensaiomakemai2015-1.jpg', 26, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(8834, 71, '20151203112723sem-titulo-000350.jpg', 27, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(8835, 71, '20151203112733vogue-4-pv035523.jpg', 28, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(8836, 71, '201512071537461625679-667656479950454-5895229590281739367-n.jpg', 29, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(8837, 71, '20151204134922mg-72231-900x600.jpg', 30, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(8838, 71, '2015120712332920151204134834el324-beleza-ensaiomakemai2015-2.jpg', 31, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(8839, 71, '2015120715335926-03-1.JPG', 32, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(9279, 139, '20160328170500arranjovicmeirelles02.jpg', 0, '2016-05-24 18:09:57', '2016-05-24 18:09:57'),
(9280, 139, '20160328170502arranjovicmeirelles05.jpg', 1, '2016-05-24 18:09:57', '2016-05-24 18:09:57'),
(9281, 139, '20160328170503arranjovicmeirelles09.jpg', 2, '2016-05-24 18:09:57', '2016-05-24 18:09:57'),
(9282, 139, '20160328170508arranjovicmeirelles06.jpg', 3, '2016-05-24 18:09:57', '2016-05-24 18:09:57'),
(9283, 139, '20160328170505vicmeirellescasamentos06.jpg', 4, '2016-05-24 18:09:57', '2016-05-24 18:09:57'),
(9284, 139, '20160328170506vicmeirelleseventoinfantil06.jpg', 5, '2016-05-24 18:09:57', '2016-05-24 18:09:57'),
(9285, 139, '20160328170510vicmeirellescorporativo05.jpg', 6, '2016-05-24 18:09:57', '2016-05-24 18:09:57'),
(9286, 139, '20160328170510vicmeirellescasamentos14.jpg', 7, '2016-05-24 18:09:57', '2016-05-24 18:09:57'),
(9287, 139, '20160328170512vicmeirellescasamentos21.jpg', 8, '2016-05-24 18:09:57', '2016-05-24 18:09:57'),
(9288, 139, '20160328170512fasano16.jpg', 9, '2016-05-24 18:09:57', '2016-05-24 18:09:57'),
(9289, 139, '20160328170512vicmeirelleseventoinfantil04.jpg', 10, '2016-05-24 18:09:57', '2016-05-24 18:09:57'),
(9290, 139, '20160328170531vicmeirellescorporativo03.jpg', 11, '2016-05-24 18:09:57', '2016-05-24 18:09:57'),
(9291, 139, '20160328170532vicmeirellescorporativo04.jpg', 12, '2016-05-24 18:09:57', '2016-05-24 18:09:57'),
(9292, 139, '20160328170532vicmeirellescorporativo02.jpg', 13, '2016-05-24 18:09:57', '2016-05-24 18:09:57'),
(9293, 139, '20160328170531vicmeirellescorporativo01.jpg', 14, '2016-05-24 18:09:57', '2016-05-24 18:09:57'),
(10127, 32, '20151027005259carla-lamarca-joais-lama-handmade-2-940x520.jpg', 0, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(10128, 32, '20151027010013carla-lamarca-verao-2013-moda-carla-lamarca.jpg', 1, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(10129, 32, '20151027005259carla-1.jpg', 2, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(10130, 32, '20151027004308quotnao-gosto-da-minha-sobrancelha-e-quase-inexistentequot-diz-carla.jpg', 3, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(10131, 32, '20151027004307carla-lamarca2.jpg', 4, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(10132, 32, '20151027005008carla-pb.jpg', 5, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(10133, 32, '20151027010012carla-lamarca-verao-2013-moda-2.jpg', 6, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(10134, 32, '20151027004306img-4889.jpg', 7, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(10135, 32, '20151027010013carla-lamarca-verao-2013-moda-3.jpg', 8, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(10136, 32, '20151027005006carla-lamarca1-divulg.jpg', 9, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(10137, 32, '201510270043067701229carla-lamarca-ti-ti-ti-urilo-benicio-paris-hilton-500-500.jpg', 10, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(10138, 32, '20151027004307burn-epoque-carla-lamarca-01.jpg', 11, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(10139, 32, '20151027004336images-1.jpeg', 12, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(10140, 32, '20151027010001images-3.jpeg', 13, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(10141, 32, '2016062314564420151027004038nova-imagem2.png', 14, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(10142, 32, '20151027005259img-9077.JPG', 15, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(11294, 131, '2015121715161120.jpg', 0, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11295, 131, '20151217151610ccc.jpg', 1, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11296, 131, '20151217151613ffff.jpg', 2, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11297, 131, '20151217151613ac.jpg', 3, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11298, 131, '2015121715161016.jpg', 4, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11299, 131, '20151217151621sem-titulo-9.jpg', 5, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11300, 131, '20151217151617h.jpg', 6, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11301, 131, '20151217151613.jpg', 7, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11302, 131, '20151217151619nn.jpg', 8, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11303, 131, '20151217151618lll.jpg', 9, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11304, 131, '20151217151619ggg.jpg', 10, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11305, 131, '20151217151616hhh.jpg', 11, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11306, 131, '2015121715161119.jpg', 12, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11307, 131, '20151217151623sem-titulo-14.jpg', 13, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11308, 131, '20151217151622vv.jpg', 14, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11309, 131, '20151217151625renner-look1-33.jpg', 15, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11310, 131, '20151217151626y.jpg', 16, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11311, 131, '20151217151624sem-titulo-1.jpg', 17, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11312, 131, '20151217151629x.jpg', 18, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11313, 131, '20151217151619q.jpg', 19, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11314, 131, '20151217151629kk.jpg', 20, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11315, 131, '20151217151626o.jpg', 21, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11316, 131, '20151217151629renner-look3-234.jpg', 22, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11317, 131, '20151217151631copia-de-ddd.jpg', 23, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11318, 131, '20151217151631sem-titulo-13.jpg', 24, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11319, 131, '20151217151633qq.jpg', 25, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11320, 131, '20151217151629gggg.jpg', 26, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11321, 131, '20151217151632m.jpg', 27, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11322, 131, '20151217151635sem-titulo-6.jpg', 28, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11323, 131, '20151217151636sem-titulo-15.jpg', 29, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11324, 131, '20151217151636z.jpg', 30, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11325, 131, '20151217151636fffff.jpg', 31, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11326, 131, '20151217151635hh.jpg', 32, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11327, 131, '20151217160945claudia20leitte20-20foto-lupa-2045020180809.jpg', 33, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(11754, 152, '20160728213438f-t-0388.jpg', 0, '2016-12-23 03:25:59', '2016-12-23 03:25:59'),
(11755, 152, '20160728213657f-t-9546.jpg', 1, '2016-12-23 03:25:59', '2016-12-23 03:25:59'),
(11756, 152, '20160728214207f-t-9595.jpg', 2, '2016-12-23 03:25:59', '2016-12-23 03:25:59'),
(11757, 152, '20160728214937f-t-0275.jpg', 3, '2016-12-23 03:25:59', '2016-12-23 03:25:59'),
(11758, 152, '20160728214618f-t-9686.jpg', 4, '2016-12-23 03:25:59', '2016-12-23 03:25:59'),
(11761, 165, '20160922114131unknown-5.jpeg', 0, '2017-01-16 14:49:39', '2017-01-16 14:49:39'),
(11762, 165, '20160922113146tratada.jpg', 1, '2017-01-16 14:49:39', '2017-01-16 14:49:39'),
(11763, 165, '20160922114239home-4.jpg', 2, '2017-01-16 14:49:39', '2017-01-16 14:49:39'),
(11764, 165, '20160922114239home-1.jpg', 3, '2017-01-16 14:49:39', '2017-01-16 14:49:39'),
(11765, 165, '20160922114131unknown-4.jpeg', 4, '2017-01-16 14:49:39', '2017-01-16 14:49:39'),
(11855, 138, '20160216155530030-68494.jpg', 0, '2017-01-19 19:36:49', '2017-01-19 19:36:49'),
(11856, 138, '20160216155520home-5-foto-2.jpg', 1, '2017-01-19 19:36:49', '2017-01-19 19:36:49'),
(11857, 138, '20160216155533030-68536.jpg', 2, '2017-01-19 19:36:49', '2017-01-19 19:36:49'),
(11858, 138, '20160216155613celso-zucatelli1.jpg', 3, '2017-01-19 19:36:49', '2017-01-19 19:36:49'),
(11859, 138, '20160216155524010-68366.jpg', 4, '2017-01-19 19:36:49', '2017-01-19 19:36:49'),
(11995, 146, '2016051214382920160216155530030-68494.jpg', 0, '2017-01-27 18:53:38', '2017-01-27 18:53:38'),
(11996, 146, '2016051214382820160216155520home-5-foto-2.jpg', 1, '2017-01-27 18:53:38', '2017-01-27 18:53:38'),
(11997, 146, '2016051214382720160216155613celso-zucatelli1.jpg', 2, '2017-01-27 18:53:38', '2017-01-27 18:53:38'),
(11998, 146, '2016051214382820160216155524010-68366.jpg', 3, '2017-01-27 18:53:38', '2017-01-27 18:53:38'),
(11999, 146, '2016051214383020160216155533030-68536.jpg', 4, '2017-01-27 18:53:38', '2017-01-27 18:53:38'),
(12065, 63, '2016042912255212802932-587416134756256-6742088490463200686-n.jpg', 0, '2017-01-27 19:29:19', '2017-01-27 19:29:19'),
(12066, 63, '20151210140159erika-palomino.jpg', 1, '2017-01-27 19:29:19', '2017-01-27 19:29:19'),
(12067, 63, '20160429122711erika-tratada.jpg', 2, '2017-01-27 19:29:19', '2017-01-27 19:29:19'),
(12072, 173, '20161019083951jamilfilho.png', 0, '2017-01-27 20:43:07', '2017-01-27 20:43:07'),
(12073, 173, '20161019081348img-8261.JPG', 1, '2017-01-27 20:43:07', '2017-01-27 20:43:07'),
(12074, 173, '20161019083954roda-viva-jamil-chade-foto-jair-magri-60.jpg', 2, '2017-01-27 20:43:07', '2017-01-27 20:43:07'),
(12106, 39, '20151104150349luize-altenhofen-tratada.jpg', 0, '2017-01-27 21:27:16', '2017-01-27 21:27:16'),
(12107, 39, '20160407211205img-1371.JPG', 1, '2017-01-27 21:27:16', '2017-01-27 21:27:16'),
(12108, 39, '20150909120733image2.png', 2, '2017-01-27 21:27:16', '2017-01-27 21:27:16'),
(12109, 39, '20160407211203img-1368.JPG', 3, '2017-01-27 21:27:16', '2017-01-27 21:27:16'),
(12110, 39, '20151008154547Luize Altenhofen_2.jpg', 4, '2017-01-27 21:27:16', '2017-01-27 21:27:16'),
(12111, 39, '20160407211205img-1370.JPG', 5, '2017-01-27 21:27:16', '2017-01-27 21:27:16'),
(12112, 39, '20150820144643Luize Altenhofen_3.jpg', 6, '2017-01-27 21:27:16', '2017-01-27 21:27:16'),
(12113, 39, '20150820144643Luize Altenhofen_5.png', 7, '2017-01-27 21:27:16', '2017-01-27 21:27:16'),
(12114, 39, '20150909121442image3.png', 8, '2017-01-27 21:27:16', '2017-01-27 21:27:16'),
(12115, 39, '20160407211207img-1369.JPG', 9, '2017-01-27 21:27:16', '2017-01-27 21:27:16'),
(12116, 39, '20160407211205img-1372.JPG', 10, '2017-01-27 21:27:16', '2017-01-27 21:27:16'),
(12136, 137, '20160128171412sem-titulo-4.jpg', 0, '2017-01-27 21:35:23', '2017-01-27 21:35:23'),
(12225, 31, '2016030414375120150925124355barbara-tratada-01.jpg', 0, '2017-01-27 22:52:05', '2017-01-27 22:52:05'),
(12226, 31, '20160307151810tratda-saia-justa-verao-2016-barbara-gancia-creditos-eliana-rodrigues-4-copy.jpg', 1, '2017-01-27 22:52:05', '2017-01-27 22:52:05'),
(12227, 136, '20160224143941globo-6grszmlxkk3d06rk5y6-original.jpg', 0, '2017-01-27 22:55:14', '2017-01-27 22:55:14'),
(12228, 136, '20160224143941images.jpeg', 1, '2017-01-27 22:55:14', '2017-01-27 22:55:14'),
(12257, 155, '20161220135447whatsapp-image-2016-12-20-at-072646.jpeg', 0, '2017-01-30 13:12:34', '2017-01-30 13:12:34'),
(12406, 154, '20160701170148f-140274.jpg', 0, '2017-02-09 18:24:01', '2017-02-09 18:24:01'),
(12407, 154, '20160701164651ronald-02.jpg', 1, '2017-02-09 18:24:01', '2017-02-09 18:24:01'),
(12408, 154, '20160701164650ronald-03.jpg', 2, '2017-02-09 18:24:01', '2017-02-09 18:24:01'),
(12409, 154, '20160701164651ap-ronald-rios-cor.jpg', 3, '2017-02-09 18:24:01', '2017-02-09 18:24:01'),
(12410, 154, '20160701164651historias-rap-nacional-episodio-3-emicida-completo.jpg', 4, '2017-02-09 18:24:01', '2017-02-09 18:24:01'),
(12411, 154, '20160701164650unknown.jpeg', 5, '2017-02-09 18:24:01', '2017-02-09 18:24:01'),
(12412, 154, '20160701170128home-3.jpg', 6, '2017-02-09 18:24:01', '2017-02-09 18:24:01'),
(12941, 181, '20170210205339i2k5271.jpg', 0, '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(12942, 181, '20170210205252messagepart-3.jpg', 1, '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(12943, 181, '20170210205335620x452.jpg', 2, '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(12944, 181, '20170210205244bacteria.jpg', 3, '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(12945, 181, '20170210205248foto.JPG', 4, '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(12946, 181, '20170210205252captura-de-tela-2015-04-03-as-234528.png', 5, '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(12947, 181, '20170210205325thiagoamoral-fotoligiajardim.jpg', 6, '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(12948, 181, '20170210205253rodanei.jpg', 7, '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(12949, 181, '20170210205300captura-de-tela-2014-06-22-as-205933.png', 8, '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(12950, 181, '20170210205339arra-pedro-bonacina.jpg', 9, '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(12951, 181, '20170210205339arra-29-07-201511090-fotos-de-pedro-bonacina.png', 10, '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(12952, 181, '20170210205335ficcao.jpg', 11, '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(13083, 103, '20151124110504amanda-ramaho-cortada.jpg', 0, '2017-02-16 19:44:15', '2017-02-16 19:44:15'),
(13084, 103, '2015112320054414-1.jpg', 1, '2017-02-16 19:44:16', '2017-02-16 19:44:16'),
(13085, 103, '2015112320054511866228-973662589351625-3538787110641561876-n.jpg', 2, '2017-02-16 19:44:16', '2017-02-16 19:44:16'),
(13086, 103, '20151023191449aamnda-homne-rdio-.jpg', 3, '2017-02-16 19:44:16', '2017-02-16 19:44:16'),
(13288, 179, '201702131208281k2a6476.JPG', 0, '2017-02-20 03:53:13', '2017-02-20 03:53:13'),
(13289, 179, '201702131208431k2a6521.JPG', 1, '2017-02-20 03:53:13', '2017-02-20 03:53:13'),
(13290, 179, '201702131208001k2a6457.JPG', 2, '2017-02-20 03:53:13', '2017-02-20 03:53:13'),
(13291, 179, '201702131207551k2a6434.JPG', 3, '2017-02-20 03:53:13', '2017-02-20 03:53:13'),
(13292, 179, '201702131208241k2a6509.JPG', 4, '2017-02-20 03:53:13', '2017-02-20 03:53:13'),
(13293, 179, '201702131208161k2a6460.JPG', 5, '2017-02-20 03:53:13', '2017-02-20 03:53:13'),
(13294, 179, '201702131210171k2a6486.JPG', 6, '2017-02-20 03:53:13', '2017-02-20 03:53:13'),
(13295, 179, '201702131210121k2a6295.JPG', 7, '2017-02-20 03:53:13', '2017-02-20 03:53:13'),
(13296, 179, '201702131210201k2a6280.JPG', 8, '2017-02-20 03:53:13', '2017-02-20 03:53:13'),
(13298, 38, '20151027225254mg-94532.jpg', 0, '2017-02-21 13:41:12', '2017-02-21 13:41:12'),
(13299, 38, '2016011217200410906390-909363175755228-6376365254881869719-n.jpg', 1, '2017-02-21 13:41:12', '2017-02-21 13:41:12'),
(13300, 38, '20150820144308Ligia Mendes_12.jpg', 2, '2017-02-21 13:41:12', '2017-02-21 13:41:12'),
(13301, 38, '20150820144256Ligia Mendes_4.jpg', 3, '2017-02-21 13:41:12', '2017-02-21 13:41:12'),
(13302, 38, '20150820144307Ligia Mendes_10.jpg', 4, '2017-02-21 13:41:12', '2017-02-21 13:41:12'),
(13303, 38, '20150820144254Ligia Mendes_3.jpg', 5, '2017-02-21 13:41:12', '2017-02-21 13:41:12'),
(13304, 38, '20151027214609foto-de-divulgacao-2.png', 6, '2017-02-21 13:41:12', '2017-02-21 13:41:12'),
(13305, 38, '20151027214905ligia-mendes-2.jpg', 7, '2017-02-21 13:41:12', '2017-02-21 13:41:12'),
(13306, 38, '20151027214919ligia-mendes-5.jpg', 8, '2017-02-21 13:41:12', '2017-02-21 13:41:12'),
(13307, 38, '20160321163531img-4379.jpg', 9, '2017-02-21 13:41:12', '2017-02-21 13:41:12'),
(13308, 38, '20170221104053whatsapp-image-2017-02-21-at-065620.jpeg', 10, '2017-02-21 13:41:12', '2017-02-21 13:41:12'),
(13362, 178, '20170210184836img-4280.JPG', 0, '2017-03-08 02:55:15', '2017-03-08 02:55:15'),
(13363, 178, '20170210185313unknown-1.jpeg', 1, '2017-03-08 02:55:15', '2017-03-08 02:55:15'),
(13364, 178, '20170210185348vicente.jpg', 2, '2017-03-08 02:55:15', '2017-03-08 02:55:15'),
(13365, 178, '2017021019250223-mvg-alexandre.jpg', 3, '2017-03-08 02:55:15', '2017-03-08 02:55:15'),
(13366, 178, '2017021019250225-fha-celestino5.jpg', 4, '2017-03-08 02:55:15', '2017-03-08 02:55:15'),
(13367, 178, '2017021019250225-fha-celestino3.jpg', 5, '2017-03-08 02:55:15', '2017-03-08 02:55:15'),
(13368, 178, '20170210192504ale-schumaquer-01.jpg', 6, '2017-03-08 02:55:15', '2017-03-08 02:55:15'),
(13369, 178, '20170210192504home-5-2.jpg', 7, '2017-03-08 02:55:15', '2017-03-08 02:55:15'),
(13485, 156, '20160725113915img-1308.JPG', 0, '2017-03-10 21:35:31', '2017-03-10 21:35:31'),
(13554, 19, '2016053012130320151021140125img-2031-2.png', 0, '2017-03-14 20:49:42', '2017-03-14 20:49:42'),
(13555, 19, '20151021140121img-561450-cassio-scapin20131101181383337057.jpg', 1, '2017-03-14 20:49:42', '2017-03-14 20:49:42'),
(13556, 19, '20151021140121image.jpg', 2, '2017-03-14 20:49:42', '2017-03-14 20:49:42'),
(13557, 19, '201512081328082-1024x683.jpg', 3, '2017-03-14 20:49:42', '2017-03-14 20:49:42'),
(13558, 19, '20151021140133tmrj-aviuvaalegre06.jpg', 4, '2017-03-14 20:49:42', '2017-03-14 20:49:42'),
(13559, 19, '20151021140151cassio-scapin1jpg1.jpg', 5, '2017-03-14 20:49:42', '2017-03-14 20:49:42'),
(13560, 19, '20160509144411cassio-scapin-1.jpg', 6, '2017-03-14 20:49:42', '2017-03-14 20:49:42'),
(13561, 19, '20160510153040histeria-cassio-scapin-1024x676.jpg', 7, '2017-03-14 20:49:42', '2017-03-14 20:49:42'),
(13562, 19, '20160510153040img-721359-histeria20160509121462808141.jpg', 8, '2017-03-14 20:49:42', '2017-03-14 20:49:42'),
(13563, 19, '20161221111546whatsapp-image-2016-12-20-at-142523.jpeg', 9, '2017-03-14 20:49:42', '2017-03-14 20:49:42'),
(13819, 151, '20160729084829c785f9caa11ec8c45f601f9ea6fd5693.jpg', 0, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(13820, 151, '2016072908430912573794-1120862441279536-3621219933967528349-n.jpg', 1, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(13821, 151, '2016072908430912573794-1120862644612849-5773056513691227206-n.jpg', 2, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(13822, 151, '2016072908430912508925-1120862521279528-526512725972124663-n.jpg', 3, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(13823, 151, '2016072914172212509288-1120862364612877-2423046137413071805-n.jpg', 4, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(13824, 151, '2016072908430912540720-1120862691279511-3680313532350802445-n.jpg', 5, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(13825, 151, '2016072908430912565617-1120862621279518-2776698404765200987-n.jpg', 6, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(13826, 151, '20170210194509unknown-8.jpeg', 7, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(13827, 151, '2016072908443110711090-853914344641015-8645656908090304645-n.jpg', 8, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(13828, 151, '201703211520590cc8d0f4602ab0e6445119837c7d1a18.jpg', 9, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(13829, 151, '20160729084311gh-new-06.jpg', 10, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(13830, 151, '20170210194509gustvo-111.jpg', 11, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(13831, 151, '20160729084829cgf2486.jpg', 12, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(13832, 151, '20160729084829mg-0361.jpg', 13, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(13833, 151, '2016072908482915-1.JPG', 14, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(13834, 151, '20160729084829317375-2276793197098-1371312177-n.jpg', 15, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(13835, 151, '2016072908482920160502-204900-1.JPG', 16, '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(14291, 159, '20161108204244home-5.jpg', 0, '2017-04-13 20:45:54', '2017-04-13 20:45:54'),
(14292, 159, '20161108204248images-2.jpeg', 1, '2017-04-13 20:45:54', '2017-04-13 20:45:54'),
(14293, 159, '20161108204313andre-ramiro-5876.jpg', 2, '2017-04-13 20:45:54', '2017-04-13 20:45:54'),
(14294, 159, '20161108204201andre-ramiro-5798-close.jpg', 3, '2017-04-13 20:45:54', '2017-04-13 20:45:54'),
(14295, 159, '2016110820421275505-226327.jpg', 4, '2017-04-13 20:45:54', '2017-04-13 20:45:54'),
(14296, 159, '20161108204312andre-matias.jpg', 5, '2017-04-13 20:45:54', '2017-04-13 20:45:54'),
(14297, 159, '2016111711072440dc2e92-483c-48d4-92fc-f2f3a45df9a0.jpg', 6, '2017-04-13 20:45:54', '2017-04-13 20:45:54'),
(14298, 159, '20161206194832andre-ramiro-a-lei.jpg', 7, '2017-04-13 20:45:54', '2017-04-13 20:45:54'),
(14299, 159, '20161206195412luz-clara-3.png', 8, '2017-04-13 20:45:54', '2017-04-13 20:45:54'),
(14300, 159, '20161220170616whatsapp-image-2016-12-04-at-095949.jpeg', 9, '2017-04-13 20:45:54', '2017-04-13 20:45:54'),
(14301, 159, '20170124142346whatsapp-image-2016-12-20-at-142513.jpeg', 10, '2017-04-13 20:45:54', '2017-04-13 20:45:54'),
(14311, 167, '201610041654133-papeis.jpg', 0, '2017-04-18 21:39:13', '2017-04-18 21:39:13'),
(14312, 167, '20161004145115by-pritessa-1919-peq.jpg', 1, '2017-04-18 21:39:13', '2017-04-18 21:39:13'),
(14313, 167, '20161004144901by-pritessa-1287-2.jpg', 2, '2017-04-18 21:39:13', '2017-04-18 21:39:13'),
(14314, 167, '20161004165411wicked.jpg', 3, '2017-04-18 21:39:13', '2017-04-18 21:39:13'),
(14315, 167, '20161004145037by-pritessa-1681-2.jpg', 4, '2017-04-18 21:39:13', '2017-04-18 21:39:13'),
(14316, 167, '20161004165411mufasa-2.JPG', 5, '2017-04-18 21:39:13', '2017-04-18 21:39:13'),
(14317, 167, '20161004145227by-pritessa-1761.jpg', 6, '2017-04-18 21:39:13', '2017-04-18 21:39:13'),
(14318, 167, '20161004145254by-pritessa-1946.jpg', 7, '2017-04-18 21:39:13', '2017-04-18 21:39:13'),
(14319, 167, '20161004165211by-pritessa-1775-peq.jpg', 8, '2017-04-18 21:39:13', '2017-04-18 21:39:13'),
(14320, 167, '20161004165217by-pritessa-1599-2-peq.jpg', 9, '2017-04-18 21:39:13', '2017-04-18 21:39:13'),
(14321, 167, '20161004165224by-pritessa-1308-2-peq.jpg', 10, '2017-04-18 21:39:13', '2017-04-18 21:39:13'),
(14322, 167, '20161004165241cesar-mello-2.jpg', 11, '2017-04-18 21:39:13', '2017-04-18 21:39:13'),
(14323, 167, '20161004165240by-pritessa-2013-2-peq.jpg', 12, '2017-04-18 21:39:13', '2017-04-18 21:39:13'),
(14324, 167, '20161004165411mufasa-3.JPG', 13, '2017-04-18 21:39:13', '2017-04-18 21:39:13'),
(14427, 132, '2015120915483812087948-10203904085156209-2755408551358275436-o.jpg', 0, '2017-05-02 20:46:33', '2017-05-02 20:46:33'),
(14428, 132, '20160630161050unknown-3.jpeg', 1, '2017-05-02 20:46:33', '2017-05-02 20:46:33'),
(14429, 132, '20151209154735391542-504228602927473-211070265-n.jpg', 2, '2017-05-02 20:46:33', '2017-05-02 20:46:33'),
(14430, 132, '20151209154835copia-de-pi-mof-sa-rgiomenezes-1280x720-328444995875.jpg', 3, '2017-05-02 20:46:33', '2017-05-02 20:46:33'),
(14431, 132, '2015120915483410982276-1202747803075546-7688347176880136404-n.jpg', 4, '2017-05-02 20:46:33', '2017-05-02 20:46:33'),
(14432, 132, '2015120915483810409531-1041712552512406-3068973075139488353-n.jpg', 5, '2017-05-02 20:46:33', '2017-05-02 20:46:33'),
(14433, 132, '20151209154735557139-331505420276594-258241963-n.jpg', 6, '2017-05-02 20:46:33', '2017-05-02 20:46:33'),
(14434, 132, '2015120915483711041728-858005370929765-253608317790454542-n.jpg', 7, '2017-05-02 20:46:33', '2017-05-02 20:46:33'),
(14435, 132, '20151209154840sergio-menezes-6.jpg', 8, '2017-05-02 20:46:33', '2017-05-02 20:46:33'),
(14436, 132, '20151209154839sergio-menezes.jpg', 9, '2017-05-02 20:46:33', '2017-05-02 20:46:33'),
(14437, 132, '20151209154840sergio-menezes-2.jpg', 10, '2017-05-02 20:46:33', '2017-05-02 20:46:33'),
(14438, 132, '2015120915483912246980-1275806802436312-7687334328972947285-n.jpg', 11, '2017-05-02 20:46:33', '2017-05-02 20:46:33'),
(14439, 132, '20160112172051sergio-menezes-solange-couto-g.jpg', 12, '2017-05-02 20:46:33', '2017-05-02 20:46:33'),
(14443, 42, '20160623160140em-baixa-88.jpg', 0, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(14444, 42, '20150909134122SITE _Marilia Moreno _ Eletric Sommer 001.jpg', 1, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(14445, 42, '20160623155543broderick-hunter-v1-issue3-25.jpg', 2, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(14446, 42, '20150820150249Marilia Moreno_5.jpg', 3, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(14447, 42, '20150909141254Marilia Moreno_3.jpg', 4, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(14448, 42, '20160623155352em-baixa-01.jpg', 5, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(14449, 42, '20150909134125SITE _Marilia Moreno _ Eletric Sommer 002.png', 6, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(14450, 42, '201606231521121901.jpg', 7, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(14451, 42, '20151009150340Marilia Moreno_studiow_JOY 5.jpeg', 8, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(14452, 42, '20160623155445em-baixa-04.jpg', 9, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(14453, 42, '20150909141310Marilia17crop.jpg', 10, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(14454, 42, '2016011217170412522970-1104316376259906-5658011973643520323-n.jpg', 11, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(14455, 42, '201605311148133.jpg', 12, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(14456, 42, '20160623155559broderick-hunter-v1-issue3-30.jpg', 13, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(14457, 42, '20160623161051marilia-baixa-99.jpg', 14, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(14496, 149, '20160808205410a99d9828-final.jpg', 0, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(14497, 149, '20160630160947saulo-re-04.jpg', 1, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(14498, 149, '20160808205338a99d9681-final.jpg', 2, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(14499, 149, '20160808205111img-6377.JPG', 3, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(14500, 149, '20160531000025img-3416.jpg', 4, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(14501, 149, '20160613152621saulo-por-luciana-sposito-rj-4-001.jpg', 5, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(14502, 149, '20160531000025saulo-tratada-01.jpg', 6, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(14503, 149, '20160613152621saulo-por-luciana-sposito-rj-7-001.jpg', 7, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(14504, 149, '20160808205339a99d9691-final.jpg', 8, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(14505, 149, '20161004202402cszs2dpweaaxfke.jpg', 9, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(14506, 149, '20160808205430a99d9694-final.jpg', 10, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(14507, 149, '20160808205803saulo-re-09.jpg', 11, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(14508, 149, '20160808205616a99d9828-final.jpg', 12, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(14509, 149, '20160808205356a99d9940-final.jpg', 13, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(14510, 149, '20160808205753saulo-re-10.jpg', 14, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(14511, 149, '20160809165315home-saulo-meneghetti-tratada.jpg', 15, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(14513, 40, '20151126153026marcelle-bittar-para-rev-made-por-gustavo-zylberstajn-236.jpg', 0, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(14514, 40, '20160809163910olympiada-07.jpg', 1, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(14515, 40, '20151126153020marcelle-bittar-para-rev-made-por-gustavo-zylberstajn-228.jpg', 2, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(14516, 40, '20150909130741Marcelle Bittar_11.jpg', 3, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(14517, 40, '20151126153025marcelle-bittar-para-rev-made-por-gustavo-zylberstajn-230.jpg', 4, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(14518, 40, '20150820145116Marcelle Bittar_7.jpg', 5, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(14519, 40, '20160808210124olympiada-02.jpg', 6, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(14520, 40, '20150820145102Marcelle Bittar_4.jpg', 7, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(14521, 40, '20151126153010marcelle-bittar-para-rev-made-por-gustavo-zylberstajn-227.jpg', 8, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(14522, 40, '20150820145105Marcelle Bittar_2.jpg', 9, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(14523, 40, '20151126153027marcelle-bittar-para-rev-made-por-gustavo-zylberstajn-229.jpg', 10, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(14524, 40, '20150820145121Marcelle Bittar_5.jpg', 11, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(14525, 40, '20150820145122marcelle Bittar_10.jpg', 12, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(14526, 40, '20150820145138Marcelle Bittar_12.jpg', 13, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(14527, 40, '20151126153023marcelle-bittar-para-rev-made-por-gustavo-zylberstajn-232.jpg', 14, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(14528, 40, '20151126153027marcelle-bittar-para-rev-made-por-gustavo-zylberstajn-238.jpg', 15, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(14529, 40, '20160809163910olympiada-05.jpg', 16, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(14530, 40, '20161212115320whatsapp-image-2016-12-04-at-095326.jpeg', 17, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(14552, 112, '2016042912350512802932-587416134756256-6742088490463200686-n.jpg', 0, '2017-05-17 18:32:22', '2017-05-17 18:32:22'),
(14553, 112, '2016042912405220151210140159erika-palomino.jpg', 1, '2017-05-17 18:32:22', '2017-05-17 18:32:22'),
(14554, 112, '20160429123516erika-palomino-2016.jpg', 2, '2017-05-17 18:32:22', '2017-05-17 18:32:22'),
(14555, 153, '20170517153932fabio-v.jpg', 0, '2017-05-17 18:39:39', '2017-05-17 18:39:39'),
(14557, 60, '20160321161727food-truck-marcio-silva.JPG', 0, '2017-05-17 18:51:50', '2017-05-17 18:51:50'),
(14572, 34, '201511232144043.jpg', 0, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14573, 34, '20150819153738Costanza Pascolato_9.png', 1, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14574, 34, '20151123214624costanza-pascolato.jpg', 2, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14575, 34, '20151123214405untitled-5.jpg', 3, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14576, 34, '20150819153731Costanza Pascolato_5.jpg', 4, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14577, 34, '20151123213702constanza-pascolato.jpg', 5, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14578, 34, '20150819153732Costanza Pascolato_3.jpg', 6, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14579, 34, '20150819153733Costanza Pascolato_1.jpg', 7, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14580, 34, '20150921174128IMG_0249.jpg', 8, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14581, 34, '20150819153732Costanza Pascolato_6.jpg', 9, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14582, 34, '20151123214403bob-wolfenson-constanza-e-jum-nakao.jpg', 10, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14583, 34, '20160307134040costanza.jpg', 11, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14584, 34, '2015112321370220120128-mag-colee7f5es-1129320-2020fotos20-20fe1bio20bartelt.jpg', 12, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14585, 34, '2015112321370220122701-mag-colee7f5es-1055520-2020fotos20-20fe1bio20bartelt.jpg', 13, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14586, 34, '2015112322113500a7bc74.jpg', 14, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14587, 34, '20151123215223costanza-pascolato-8.jpg', 15, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14588, 34, '20151123221135costanza-shoestock-vg12-06.jpg', 16, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14589, 34, '201511232211365000x5000-costanza-pascolato-esta-no-rio-e-faz-noite-de-autografos-na-barra.jpg', 17, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14590, 34, '20151210142736costanza.jpg', 18, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14591, 34, '20170206105109whatsapp-image-2017-02-06-at-093245.jpeg', 19, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14592, 34, '20170328162027confidencial-costanza-pascolato-03.jpg', 20, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14593, 34, '20170328162029costanza-home-3.jpg', 21, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14594, 34, '20170328162032costanza-shoestock-vg12-06.jpg', 22, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14595, 34, '20170328162034costanza.jpg', 23, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14596, 34, '20170328162048constanza-pascolato-veio-rocknroll-sao-paulo-fashion-week-em-janeiro-e-fevereiro-de-2011.jpg', 24, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14597, 34, '20151123215717sem-titulo-13.jpg', 25, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14598, 34, '2017051718113809b93e9a-442b-4206-855e-50735a3cc00b.jpg', 26, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14599, 34, '201705171811468eec7dba-b064-49fe-9f0c-47bed9c74121.jpg', 27, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(14600, 135, '20160119143028foto-7.jpg', 0, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14601, 135, '20160119143000foto1.png', 1, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14602, 135, '20160119143004foto2.jpg', 2, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14603, 135, '20160119143011foto3.jpg', 3, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14604, 135, '20160119143022foto-5.jpg', 4, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14605, 135, '20160119143025foto-6.jpg', 5, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14606, 135, '20160119143031foto-8.jpg', 6, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14607, 135, '20160119143035foto-9.jpg', 7, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14608, 135, '20160119143038foto-10.jpg', 8, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14609, 135, '20160119143040foto-11.jpg', 9, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14610, 135, '20160119143045foto-12.jpg', 10, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14611, 135, '20170328162313confidencial-costanza-pascolato-03.jpg', 11, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14612, 135, '20160119143057foto-14.jpg', 12, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14613, 135, '20160119143100foto-16.jpg', 13, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14614, 135, '20160119143107foto-19.jpg', 14, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14615, 135, '20160119143114foto-20.jpg', 15, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14616, 135, '20160119143119foto-21.jpg', 16, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14617, 135, '20170327153935images.jpeg', 17, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14618, 135, '20160119143126foto15.jpg', 18, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14619, 135, '20170328162313costanza-shoestock-vg12-06.jpg', 19, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14620, 135, '20170328162314costanza.jpg', 20, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14621, 135, '20170328162314foto-01-costanza-pascolato-foto-romulo-fialdini-arquivo-vogue.jpg', 21, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14622, 135, '20170328162321home5.jpg', 22, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14623, 135, '20170328162315images.jpeg', 23, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14624, 135, '20170328162323constanza-pascolato-veio-rocknroll-sao-paulo-fashion-week-em-janeiro-e-fevereiro-de-2011.jpg', 24, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14625, 135, '20160119143130foto-22.jpg', 25, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14626, 135, '201705171813268eec7dba-b064-49fe-9f0c-47bed9c74121.jpg', 26, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14627, 135, '2017051718133309b93e9a-442b-4206-855e-50735a3cc00b.jpg', 27, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(14681, 148, '20170518211105012.jpg', 0, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(14682, 148, '20160531120550img-3700-2188551526-o-696x1024.jpg', 1, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(14683, 148, '20160531115232dsc02166-680x1024.jpg', 2, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(14684, 148, '2016053112054710932109-897739040289040-168618419-n.jpg', 3, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(14685, 148, '20160531120550img-8273-682x1024.jpg', 4, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(14686, 148, '2016053112054712819113-521200181394236-996156215-n.jpg', 5, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(14687, 148, '20160531120548fullsizerender-5-1024x1024.jpg', 6, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(14688, 148, '201605311205471515999-1053065631384188-1623737054-n.jpg', 7, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(14689, 148, '2016053112054712917990-771123119691008-479833657-n.jpg', 8, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(14690, 148, '20160531120550dsc03592-812x1024.jpg', 9, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(14691, 148, '2016053112054813166893-1617783291878576-1607917864-n.jpg', 10, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(14692, 148, '201605311208062015-11-11-162311-1-680x1024.jpg', 11, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(14693, 148, '2016053112072811875592-942383752509041-2081859445-n.jpg', 12, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(14694, 148, '2016053112094712071136-107366286290182-423683265-n.jpg', 13, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(14695, 148, '201705171810248eec7dba-b064-49fe-9f0c-47bed9c74121.jpg', 14, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(14722, 150, '20160613113009unknown.jpeg', 0, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(14723, 150, '201605312215181957946-595576563897676-1794092153657364637-o.jpg', 1, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(14724, 150, '201605312215161495482-447641892024478-552736664-n.jpg', 2, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(14725, 150, '20160613113009unknown-2.jpeg', 3, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(14726, 150, '201605312215161488845-439666109488723-34523994-n.jpg', 4, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(14727, 150, '2016053122151810437816-610760812379251-8625127100117276015-n.jpg', 5, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(14728, 150, '2016053122151810687334-624866277635371-4294874076187508484-o.jpg', 6, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(14729, 150, '20160531221516993458-426995000755834-1102248201-n.jpg', 7, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(14730, 150, '2016053122151810317733-595579337230732-2472586255497308760-o.jpg', 8, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(14731, 150, '2016053122152010258786-595578883897444-6977716694611525316-o.jpg', 9, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(14732, 150, '2016053122151911035538-713154258806572-5347498702914803870-n.jpg', 10, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(14733, 150, '2016053122152010694201-624866267635372-3093414477718313630-o.jpg', 11, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(14734, 150, '201605312232271471909-426994887422512-29776128-n.jpg', 12, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(14735, 150, '2016053122151911850700-739626662825998-8479610150297308476-o.jpg', 13, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(14736, 150, '2016053122152012592191-812229012232429-1390187049816587169-n.jpg', 14, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(14737, 150, '201605312232271488945-618839784904687-8579015498449380298-n.jpg', 15, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(14785, 102, '20170519112847unknown-1.jpeg', 0, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(14786, 102, '20170518214948img-8412.PNG', 1, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(14787, 102, '20170518214832home-6.jpg', 2, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(14788, 102, '20170518214832home-3.jpg', 3, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(14789, 102, '20170518215125lema-greenpeace-lilian-pacce-out2016-por-leandro-godoi-3-1.JPG', 4, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(14790, 102, '20170518214902060417-lp-biquini-bh-347x520-1.jpg', 5, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(14791, 102, '20170518214831biquini.jpg', 6, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(14792, 102, '20170518214835170509-biquini-whatsapp.jpg', 7, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(14793, 102, '20170518214934img-1500.JPG', 8, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(14794, 102, '20170519112848unknown-5.jpeg', 9, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(14795, 102, '20170518214945lilian.jpg', 10, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(14796, 102, '20170518215558publicacoes-lilian-pacce.jpg', 11, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(14797, 102, '20170519112848unknown-2.jpeg', 12, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(14798, 102, '20170518215559lilian.jpg', 13, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(14839, 186, '2017021419464020090622-1249369210.jpg', 0, '2017-05-22 18:19:01', '2017-05-22 18:19:01'),
(14840, 186, '20170214194641bethy-2.jpg', 1, '2017-05-22 18:19:01', '2017-05-22 18:19:01'),
(14841, 186, '2017021419464020090528-1244185792.jpg', 2, '2017-05-22 18:19:01', '2017-05-22 18:19:01'),
(14842, 186, '20170214194642131107053647.jpg', 3, '2017-05-22 18:19:01', '2017-05-22 18:19:01'),
(14843, 186, '20170214194642unknown.jpeg', 4, '2017-05-22 18:19:01', '2017-05-22 18:19:01'),
(14844, 186, '20170214194651home-2.jpg', 5, '2017-05-22 18:19:01', '2017-05-22 18:19:01'),
(14845, 191, '201703291236021889019-10203252473057533-847203635-o.jpg', 0, '2017-05-22 19:17:47', '2017-05-22 19:17:47'),
(14846, 191, '20170515103709whatsapp-image-2017-05-14-at-221540.jpeg', 1, '2017-05-22 19:17:47', '2017-05-22 19:17:47'),
(14847, 191, '20170329123630book-chris-4.jpg', 2, '2017-05-22 19:17:47', '2017-05-22 19:17:47'),
(14848, 191, '2017032912360215624379-730613223753815-6754310177219936256-n.jpg', 3, '2017-05-22 19:17:47', '2017-05-22 19:17:47'),
(14849, 191, '201703291236022-1.jpg', 4, '2017-05-22 19:17:47', '2017-05-22 19:17:47'),
(14850, 191, '2017032912360217342533-10212632938003294-5047971888688823671-n.jpg', 5, '2017-05-22 19:17:47', '2017-05-22 19:17:47'),
(14851, 191, '2017032912360717358668-10212647757573774-5577331966231467476-o.jpg', 6, '2017-05-22 19:17:47', '2017-05-22 19:17:47'),
(14852, 191, '20170329123609face10.jpg', 7, '2017-05-22 19:17:47', '2017-05-22 19:17:47'),
(14853, 191, '2017032912390512.jpg', 8, '2017-05-22 19:17:47', '2017-05-22 19:17:47'),
(14883, 35, '20160623151055cica-tratada.jpg', 0, '2017-05-22 19:43:30', '2017-05-22 19:43:30'),
(14884, 35, '20150826153336cica Branco tratada .jpg', 1, '2017-05-22 19:43:30', '2017-05-22 19:43:30'),
(14885, 35, '20150819155447Daniella Cicarelli_5.jpg', 2, '2017-05-22 19:43:30', '2017-05-22 19:43:30'),
(14886, 35, '20150921172919Cica Punta Trata.jpg', 3, '2017-05-22 19:43:30', '2017-05-22 19:43:30'),
(14887, 35, '201508261536397 2.JPG', 4, '2017-05-22 19:43:30', '2017-05-22 19:43:30'),
(14888, 35, '20151203122132daniela-cicarelli.jpg', 5, '2017-05-22 19:43:30', '2017-05-22 19:43:30'),
(14889, 35, '20160623151406daniella-cicarelli-3.jpg', 6, '2017-05-22 19:43:30', '2017-05-22 19:43:30'),
(14977, 192, '20170404153322copia-de-vitor-6.jpg', 0, '2017-05-25 21:06:42', '2017-05-25 21:06:42'),
(14978, 192, '20170404153321mg-1182.jpeg', 1, '2017-05-25 21:06:42', '2017-05-25 21:06:42'),
(14979, 192, '20170404153320mg-1132.jpeg', 2, '2017-05-25 21:06:42', '2017-05-25 21:06:42'),
(14980, 192, '20170404153325vitor-19.jpg', 3, '2017-05-25 21:06:42', '2017-05-25 21:06:42'),
(14981, 192, '20170404153328vitor-16.jpg', 4, '2017-05-25 21:06:42', '2017-05-25 21:06:42'),
(14982, 192, '20170404153327vitor-9.jpg', 5, '2017-05-25 21:06:42', '2017-05-25 21:06:42'),
(14983, 192, '20170404153321copia-de-soentre-nos-dir-joca-andreazza-2016.jpeg', 6, '2017-05-25 21:06:42', '2017-05-25 21:06:42'),
(14984, 192, '20170404153329vitor-20.jpg', 7, '2017-05-25 21:06:42', '2017-05-25 21:06:42'),
(14985, 192, '20170404153328copia-de-vitor-13.jpg', 8, '2017-05-25 21:06:42', '2017-05-25 21:06:42'),
(14986, 192, '20170404153329vitor-15.jpg', 9, '2017-05-25 21:06:42', '2017-05-25 21:06:42'),
(14987, 192, '20170404153333copia-de-vitor-7.jpg', 10, '2017-05-25 21:06:42', '2017-05-25 21:06:42'),
(14988, 192, '20170404153332vitor-18.jpg', 11, '2017-05-25 21:06:42', '2017-05-25 21:06:42'),
(14989, 192, '20170404153610copia-de-o-desmonte-dir-amarildo-felix-2016.jpg', 12, '2017-05-25 21:06:42', '2017-05-25 21:06:42'),
(14990, 192, '20170404153329copia-de-vitor-2.jpg', 13, '2017-05-25 21:06:42', '2017-05-25 21:06:42'),
(15192, 160, '20170530153617bruna-3-2.JPG', 0, '2017-05-30 18:49:42', '2017-05-30 18:49:42'),
(15193, 160, '20170530153645jc150515-174719.jpg', 1, '2017-05-30 18:49:42', '2017-05-30 18:49:42'),
(15238, 30, '20151009123200Ana Carolina Scaff (6).JPG', 0, '2017-05-30 20:05:54', '2017-05-30 20:05:54');
INSERT INTO `portfolio_fotos` (`id`, `portfolio_personalidades_id`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(15239, 30, '2015112320531112191727-900497739987541-965907799298091630-n.jpg', 1, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(15240, 30, '2015112320591111012646-934412206600217-5679769962695551589-o.jpg', 2, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(15241, 30, '20151009122719Final8269.jpg', 3, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(15242, 30, '20151009122713Ana Carolina Scaff (17).jpg', 4, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(15243, 30, '20151009123159Ana Carolina Scaff (7).JPG', 5, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(15244, 30, '2015112320591111258877-934414463266658-5657994774476053746-o.jpg', 6, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(15245, 30, '20151009123000Carol Scaff_6.jpg', 7, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(15246, 30, '20151009123201Ana Carolina Scaff (18).JPG', 8, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(15247, 30, '2015112320531011218770-10153685051984487-7299235534916040280-n.jpg', 9, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(15248, 30, '2015112320591111807422-934412289933542-1885327531877247092-o.jpg', 10, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(15249, 30, '20151009122716Ana Carolina Scaff (20).jpg', 11, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(15250, 30, '20170530163320mg-0022.JPG', 12, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(15251, 30, '201705301633260410.jpg', 13, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(15252, 30, '201705301633580548.jpg', 14, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(15253, 30, '20170530163408mg-0053.JPG', 15, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(15254, 30, '20170530163448carol-scaff-28-of-33.jpg', 16, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(15255, 30, '20170530163450mg-9846.JPG', 17, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(15256, 30, '201705301635411810.jpg', 18, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(15257, 30, '201705301635491368.jpg', 19, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(15258, 30, '201705301635590866.jpg', 20, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(15259, 30, '201705301636361066.jpg', 21, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(15260, 172, '20170403120101jeronimo-valrndo.jpg', 0, '2017-05-31 19:26:16', '2017-05-31 19:26:16'),
(15261, 172, '20161018223112mg-1061.jpg', 1, '2017-05-31 19:26:16', '2017-05-31 19:26:16'),
(15262, 172, '20161018223109foto-rosto-6.jpg', 2, '2017-05-31 19:26:16', '2017-05-31 19:26:16'),
(15263, 172, '20170403120205image6.JPG', 3, '2017-05-31 19:26:16', '2017-05-31 19:26:16'),
(15264, 172, '20161018223110mg-1354.jpg', 4, '2017-05-31 19:26:16', '2017-05-31 19:26:16'),
(15265, 172, '20161018223110mg-0916.jpg', 5, '2017-05-31 19:26:16', '2017-05-31 19:26:16'),
(15266, 172, '20161018223112foto-rosto.jpg', 6, '2017-05-31 19:26:16', '2017-05-31 19:26:16'),
(15267, 172, '20161018223112mg-0750.jpg', 7, '2017-05-31 19:26:16', '2017-05-31 19:26:16'),
(15268, 172, '20161018223107foto-corpo-2.jpg', 8, '2017-05-31 19:26:16', '2017-05-31 19:26:16'),
(15269, 172, '20161018223351image1.JPG', 9, '2017-05-31 19:26:16', '2017-05-31 19:26:16'),
(15313, 161, '2016102520534225.jpg', 0, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15314, 161, '2016102520025120b.jpg', 1, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15315, 161, '20161025200323vandre-silveira-1222.jpg', 2, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15316, 161, '20161025200252farnesedesaudade-vandresilveira-foto-de-rodrigocastro-5b.jpg', 3, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15317, 161, '20161025200254vandre-silveira-1152.jpg', 4, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15318, 161, '2016102520025201.jpg', 5, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15319, 161, '20161025200342vandre-silveira-1261.jpg', 6, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15320, 161, '20161025200252unknown-4.jpeg', 7, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15321, 161, '20161025200501img-0733.jpg', 8, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15322, 161, '2016102520534618.png', 9, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15323, 161, '2016102520534419.jpg', 10, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15324, 161, '2016102520540707.png', 11, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15325, 161, '20161025205412vandre-silveira-1610.jpg', 12, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15326, 161, '20161025211136brbara-2.jpg', 13, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15327, 161, '20161027121116img-5812.JPG', 14, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15328, 161, '20161027121131webserie-todo-tempo-do-mundo.jpg', 15, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15329, 161, '20161027121148img-5814.JPG', 16, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15330, 161, '20161220134553whatsapp-image-2016-12-20-at-071659.jpeg', 17, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15331, 161, '20161220134536whatsapp-image-2016-12-20-at-071727.jpeg', 18, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15332, 161, '20161220134537whatsapp-image-2016-12-20-at-071748.jpeg', 19, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15333, 161, '20170612101109vandre-1.jpg', 20, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15334, 161, '20170612101109vandre-5.jpg', 21, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(15357, 176, '20170522150359outras-fabi-gomes.jpg', 0, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15358, 176, '20170320222753mac3.jpg', 1, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15359, 176, '201703211233491701givg-003-889-v2.jpg', 2, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15360, 176, '201703202227531f7a7770-dtlnkca.jpg', 3, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15361, 176, '20170522163716beleza110750.jpg', 4, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15362, 176, '20170321120233unknown-9.jpeg', 5, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15363, 176, '20170320222753mac2.jpg', 6, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15364, 176, '20170522163920beleza110899.jpg', 7, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15365, 176, '201703202227531f7a7880-certa.jpg', 8, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15366, 176, '20170321120233unknown-11.jpeg', 9, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15367, 176, '201703211233451701givg-001-319-v1.jpg', 10, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15368, 176, '20170320222753mac1.jpg', 11, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15369, 176, '201703202227531f7a8057.jpg', 12, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15370, 176, '20170321120231unknown-4.jpeg', 13, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15371, 176, '20170321154955beleza-maquiagem-jazz-brilho-glamour-64054.jpg', 14, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15372, 176, '201703211233511701givg-004-1376-v2.jpg', 15, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15373, 176, '20170321120232unknown-5.jpeg', 16, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15374, 176, '201703211233501701givg-005-1765-v1.jpg', 17, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15375, 176, '201703211233501701givg-002-591-v1.jpg', 18, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15376, 176, '20170321120233unknown-7.jpeg', 19, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15377, 176, '2017032112210462928170-ancelmo-gois-capa-da-dezembro-chegara-as-bancas-no-dia-30-11-da-glamour-com-a-atriz-isabe.jpg', 20, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15378, 176, '201703211233551701givg-006-1890-v1.jpg', 21, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(15484, 182, '20170613144059img-3379-marjorie-pb.jpg', 0, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(15485, 182, '20170613144108img-3011-marjorie.jpg', 1, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(15486, 182, '20170613144403img-3587-marjorie.jpg', 2, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(15487, 182, '20170613144425img-3433-marjorie.jpg', 3, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(15488, 182, '201702081249059cd90e-6a283c43bf5c4ce0a94a8dc62da3370e.jpg', 4, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(15489, 182, '20170613144613img-3547-marjorie.jpg', 5, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(15490, 182, '20170613144451img-3388-marjorie.jpg', 6, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(15491, 182, '20170613144610img-3229-marjorie.jpg', 7, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(15492, 182, '20170613144616img-3597-marjorie.jpg', 8, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(15493, 182, '20170613144614img-3547-marjorie-pb.jpg', 9, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(15494, 182, '20170613145251img-3164-marjorie.jpg', 10, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(15495, 182, '201702081246259cd90e-435f7132d02346818f66e683306a2fa2jpg-srz-1280-1920-85-22-050-120-000-jpg-srz.jpeg', 11, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(15496, 182, '20170208124625home-4.jpg', 12, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(15529, 47, '20170620144854tati-dument-foto-1.jpeg', 0, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(15530, 47, '20170620144904tati-dument-foto-2.jpeg', 1, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(15531, 47, '20160725225615image167.jpeg', 2, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(15532, 47, '20150820154559Tatiana Dumenti_6.jpg', 3, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(15533, 47, '20160725225425image169.jpeg', 4, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(15534, 47, '20150820154544Tatiana Dumenti_2.jpg', 5, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(15535, 47, '20160725225748image157.jpeg', 6, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(15536, 47, '20160112164433tati-ffw-2.jpg', 7, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(15537, 47, '20160113215315dsc6778-2.jpeg', 8, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(15538, 47, '20160725225425image160.jpeg', 9, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(15539, 47, '20150820154544Tatiana Dumenti_3.jpg', 10, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(15540, 47, '20160113215316dsc7152-2.jpeg', 11, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(15541, 47, '20150820154602Tatiana dumenti_9.jpg', 12, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(15542, 47, '20160725225425image166.jpeg', 13, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(15543, 47, '20160113215315dsc6887-2.jpeg', 14, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(15544, 47, '20150820154603Tatiana Dumenti_7.jpg', 15, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(15545, 47, '20150820154554tatiana Dumenti_4.jpg', 16, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(15546, 47, '20160112164433tati-ffw3.jpg', 17, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(15547, 47, '20160112164434tati-ffw.jpg', 18, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(15548, 47, '20160725225748image148.jpeg', 19, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(15573, 197, '20170613163921ana-carolina-godoy-3474.jpg', 0, '2017-06-22 17:15:36', '2017-06-22 17:15:36'),
(15574, 197, '20170613163837img-0509-ana-carolina-godoy.jpg', 1, '2017-06-22 17:15:36', '2017-06-22 17:15:36'),
(15575, 197, '20170613164055ana-carolina-godoy-3412-copia.jpg', 2, '2017-06-22 17:15:36', '2017-06-22 17:15:36'),
(15576, 197, '20170505153917ana-carolina-godoy.jpg', 3, '2017-06-22 17:15:36', '2017-06-22 17:15:36'),
(15577, 197, '20170613163854ana-carolina-godoy-3019-copia.jpg', 4, '2017-06-22 17:15:36', '2017-06-22 17:15:36'),
(15578, 197, '20170613163713ana-carolina-godoy-3308-copia.jpg', 5, '2017-06-22 17:15:36', '2017-06-22 17:15:36'),
(15579, 197, '20170613164004img-0290-ana-carolina-godoy.jpg', 6, '2017-06-22 17:15:36', '2017-06-22 17:15:36'),
(15580, 197, '20170613163525carol-vidotti-09.jpg', 7, '2017-06-22 17:15:36', '2017-06-22 17:15:36'),
(15581, 197, '20170613163658ana-carolina-godoy-3165-copia-crop.jpg', 8, '2017-06-22 17:15:36', '2017-06-22 17:15:36'),
(15582, 197, '20170613163710ana-carolina-godoy-2967-copia.jpg', 9, '2017-06-22 17:15:36', '2017-06-22 17:15:36'),
(15583, 197, '20170613163954img-0415-ana-carolina-godoy.jpg', 10, '2017-06-22 17:15:36', '2017-06-22 17:15:36'),
(15584, 197, '20170613164055ana-carolina-godoy-3385copia.jpg', 11, '2017-06-22 17:15:36', '2017-06-22 17:15:36'),
(15585, 170, '20170622174118omar-foto-thais.jpg', 0, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(15586, 170, '20161004183600docu0013-hair.jpg', 1, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(15587, 170, '20161004183602rebecaemail.jpg', 2, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(15588, 170, '20161004183450leibasica-foto06-00016.jpg', 3, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(15589, 170, '2016100418360001unit-jazz-mg-5911w-bycaddah.jpg', 4, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(15590, 170, '20161004183449leibasica-foto21-00013.jpg', 5, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(15591, 170, '20161004183601omar01baoba.jpg', 6, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(15592, 170, '20161004183602olhos.jpg', 7, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(15593, 170, '20161004183602omar-03-mg-0850.jpg', 8, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(15594, 170, '20161004183603dsc7328-copy.jpg', 9, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(15606, 187, '20170623120105adriana-couto.jpeg', 0, '2017-06-23 15:01:13', '2017-06-23 15:01:13'),
(15607, 187, '20170619180345fullsizerender.jpg', 1, '2017-06-23 15:01:13', '2017-06-23 15:01:13'),
(15608, 201, '20170623120213andre-dias.jpg', 0, '2017-06-23 15:02:22', '2017-06-23 15:02:22'),
(15609, 201, '20170518191910614436e2-d240-4230-a10a-2a7fafdab8fd.jpg', 1, '2017-06-23 15:02:22', '2017-06-23 15:02:22'),
(15610, 201, '20170518191828home-andre-dias-5.jpg', 2, '2017-06-23 15:02:22', '2017-06-23 15:02:22'),
(15611, 201, '20170518191827img-1656-copy-683x1024.jpg', 3, '2017-06-23 15:02:22', '2017-06-23 15:02:22'),
(15612, 201, '20170518191920andre-dias.jpg', 4, '2017-06-23 15:02:22', '2017-06-23 15:02:22'),
(15613, 201, '20170518191828img-1738-copy-683x1024.jpg', 5, '2017-06-23 15:02:22', '2017-06-23 15:02:22'),
(15614, 201, '2017051819182610540533-882320815132218-4641064576131408523-n.jpg', 6, '2017-06-23 15:02:22', '2017-06-23 15:02:22'),
(15615, 201, '20170518194401andre-dias-olavo-bilac-ve-estrelas.png', 7, '2017-06-23 15:02:22', '2017-06-23 15:02:22'),
(15616, 201, '20170518191923andre-dias-pb-tratada.jpg', 8, '2017-06-23 15:02:22', '2017-06-23 15:02:22'),
(15617, 201, '2017051819182710177238-807846762579624-6491318942340037965-n.jpg', 9, '2017-06-23 15:02:22', '2017-06-23 15:02:22'),
(15618, 201, '20170518192014andre-dias.JPG', 10, '2017-06-23 15:02:22', '2017-06-23 15:02:22'),
(15619, 201, '20170523151935andre-dias.jpg', 11, '2017-06-23 15:02:22', '2017-06-23 15:02:22'),
(15620, 56, '20170623120311pedro-lourenco.jpg', 0, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15621, 56, '20160913170000la-perla-fw16-17-adv-campaign-photo-credit-mertmarcus-1.jpg', 1, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15622, 56, '20160913165952la-perla-fw16-17-adv-campaign-photo-credit-mertmarcus-6.jpg', 2, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15623, 56, '2015112617261910518989-664679536949769-2993544363306302249-n.jpg', 3, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15624, 56, '201511261726191512119-724617634289292-4951974921336956287-o.jpg', 4, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15625, 56, '20160913170000la-perla-fw16-17-adv-campaign-photo-credit-mertmarcus-2.jpg', 5, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15626, 56, '20160913165959la-perla-fw16-17-adv-campaign-photo-credit-mertmarcus-13.jpg', 6, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15627, 56, '2015112617261910422569-702100029874386-5084821767554028037-n.jpg', 7, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15628, 56, '2015112617262010428670-724617904289265-1129909113633126449-n.jpg', 8, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15629, 56, '20160913170001la-perla-fw16-17-adv-campaign-photo-credit-mertmarcus-11.jpg', 9, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15630, 56, '201511261726184-1.jpg', 10, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15631, 56, '2015112617262110646895-702099306541125-3657592864926794005-n.jpg', 11, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15632, 56, '20160913170947pedro02.jpg', 12, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15633, 56, '20160913170954pedrolorenco-lineup-72.jpg', 13, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15634, 56, '20151216221137pedro-lourenco-nike.jpg', 14, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15635, 56, '2015112617262110850323-739631452787910-968375324363947992-n.jpg', 15, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15636, 56, '201511261726181514584-739631206121268-712873413422627785-n.jpg', 16, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15637, 56, '20160902142705tenis-pedro1.jpg', 17, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15638, 56, '20160902142705tenis-pedro2.jpg', 18, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15639, 56, '20160902142705tenis-pedro3.jpg', 19, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15640, 56, '20160913170947nikexplairmaxsp.jpg', 20, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15641, 56, '20160913170947nikexplairmaxrio.jpg', 21, '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(15665, 190, '20170623120522walcyr-carrasco.jpg', 0, '2017-06-23 15:05:26', '2017-06-23 15:05:26'),
(15672, 196, '20170622180204img-1156-carol-vidotti.jpg', 0, '2017-06-23 15:33:38', '2017-06-23 15:33:38'),
(15673, 196, '20170622180327img-1494-carol-vidotti-pb.jpg', 1, '2017-06-23 15:33:38', '2017-06-23 15:33:38'),
(15674, 196, '20170622180340img-1520-carol-vidotti-2.jpg', 2, '2017-06-23 15:33:38', '2017-06-23 15:33:38'),
(15675, 196, '20170622180518ksc7631.jpg', 3, '2017-06-23 15:33:38', '2017-06-23 15:33:38'),
(15676, 196, '20170622180408img-1407-carol-vidotti.jpg', 4, '2017-06-23 15:33:38', '2017-06-23 15:33:38'),
(15677, 196, '20170622180520ksc7660.jpg', 5, '2017-06-23 15:33:38', '2017-06-23 15:33:38'),
(15678, 196, '20170622180517ksc7537.jpg', 6, '2017-06-23 15:33:38', '2017-06-23 15:33:38'),
(15679, 196, '20170622180653img-1581-carol-vidotti.jpg', 7, '2017-06-23 15:33:38', '2017-06-23 15:33:38'),
(15680, 196, '20170622180517img-1546-carol-vidotti.jpg', 8, '2017-06-23 15:33:38', '2017-06-23 15:33:38'),
(15681, 196, '20170622180700ksc7671.jpg', 9, '2017-06-23 15:33:38', '2017-06-23 15:33:38'),
(15682, 196, '20170622180700img-1604-carol-vidotti.jpg', 10, '2017-06-23 15:33:38', '2017-06-23 15:33:38'),
(15686, 205, '20170623122506eq7a3111.jpg', 0, '2017-06-23 15:48:55', '2017-06-23 15:48:55'),
(15687, 205, '20170623122903home-2.jpg', 1, '2017-06-23 15:48:55', '2017-06-23 15:48:55'),
(15688, 205, '20170623122506eq7a3262.jpg', 2, '2017-06-23 15:48:55', '2017-06-23 15:48:55'),
(15709, 45, '20150820153327Renata-Fan_2.jpg', 0, '2017-06-26 17:06:04', '2017-06-26 17:06:04'),
(15710, 45, '20150820153328renata-Fan_4.jpg', 1, '2017-06-26 17:06:04', '2017-06-26 17:06:04'),
(15711, 45, '20151008152221fan trtada .jpg', 2, '2017-06-26 17:06:04', '2017-06-26 17:06:04'),
(15736, 158, '20160810123330krominski.jpg', 0, '2017-06-28 18:48:51', '2017-06-28 18:48:51'),
(15786, 33, '20170630103647chiara-gadaleta.jpg', 0, '2017-06-30 13:37:53', '2017-06-30 13:37:53'),
(15787, 33, '2015121013132312249756-10207350546309279-4428111973247490502-n.jpg', 1, '2017-06-30 13:37:53', '2017-06-30 13:37:53'),
(15788, 33, '2015121013132212219478-10207292258692125-2507417962364751513-n.jpg', 2, '2017-06-30 13:37:53', '2017-06-30 13:37:53'),
(15791, 185, '20170619181325renata-vilela-crotada.jpg', 0, '2017-06-30 20:50:49', '2017-06-30 20:50:49'),
(15936, 193, '20170411163520maria-zilda.jpg', 0, '2017-07-05 23:11:29', '2017-07-05 23:11:29'),
(15990, 55, '2016042518272920151201123916image001.jpg', 0, '2017-07-07 13:10:58', '2017-07-07 13:10:58'),
(15991, 55, '2016042518272520151026174739img-20151026-wa0005.jpg', 1, '2017-07-07 13:10:58', '2017-07-07 13:10:58'),
(15992, 55, '2016042518272720151026174823img-20151026-wa0008.jpg', 2, '2017-07-07 13:10:58', '2017-07-07 13:10:58'),
(15993, 55, '2016042518272820151026174805img-20151026-wa0006.jpg', 3, '2017-07-07 13:10:58', '2017-07-07 13:10:58'),
(15994, 55, '2016042518272820151026174844img-20151026-wa0009.jpg', 4, '2017-07-07 13:10:58', '2017-07-07 13:10:58'),
(15995, 55, '2016042518272920151026174856img-20151026-wa0007.jpg', 5, '2017-07-07 13:10:58', '2017-07-07 13:10:58'),
(15996, 55, '20170707101037apresentacao-lilian-pacce-3.jpg', 6, '2017-07-07 13:10:58', '2017-07-07 13:10:58'),
(15997, 92, '20170707101237apresentacao-lilian-pacce-3.jpg', 0, '2017-07-07 13:12:46', '2017-07-07 13:12:46'),
(15998, 145, '201605110937012016042518272920151201123916image001.jpg', 0, '2017-07-07 13:14:24', '2017-07-07 13:14:24'),
(15999, 145, '201605110937142016042518272720151026174823img-20151026-wa0008.jpg', 1, '2017-07-07 13:14:24', '2017-07-07 13:14:24'),
(16000, 145, '201605110937152016042518272820151026174805img-20151026-wa0006.jpg', 2, '2017-07-07 13:14:24', '2017-07-07 13:14:24'),
(16001, 145, '201605110937152016042518272520151026174739img-20151026-wa0005.jpg', 3, '2017-07-07 13:14:24', '2017-07-07 13:14:24'),
(16002, 145, '201605110937152016042518272820151026174844img-20151026-wa0009.jpg', 4, '2017-07-07 13:14:24', '2017-07-07 13:14:24'),
(16003, 145, '201605110937162016042518272920151026174856img-20151026-wa0007.jpg', 5, '2017-07-07 13:14:24', '2017-07-07 13:14:24'),
(16004, 145, '20170707101353apresentacao-lilian-pacce-3.jpg', 6, '2017-07-07 13:14:24', '2017-07-07 13:14:24'),
(16005, 70, '20170512150729whatsapp-image-2017-05-12-at-145611.jpeg', 0, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16006, 70, '20170623120409vanessa-rozan.jpg', 1, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16007, 70, '20151118173410editorial-revista-gloss-01.jpg', 2, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16008, 70, '20151118173449editorial-revista-gloss-02.jpg', 3, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16009, 70, '20151118173454sem-titulo4-crop.jpg', 4, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16010, 70, '20151118173451editorial-revista-quem-01.jpg', 5, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16011, 70, '20151118173413editorial-revista-criativa-01.jpg', 6, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16012, 70, '20151118173412editorial-revista-criativa-02.jpg', 7, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16013, 70, '20151118173458editorial-vanessa-03.jpg', 8, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16014, 70, '20151118173458editorial-vanessa-02.jpg', 9, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16015, 70, '20151118173658ej-after-six-0254.jpg', 10, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16016, 70, '20151118173707ej-after-six-0048.jpg', 11, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16017, 70, '20151118173406editorial-maquiagem-02.jpg', 12, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16018, 70, '20151118173815editorial-maquiagem-01.jpg', 13, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16019, 70, '20151118173842gloss-4.jpeg', 14, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16020, 70, '2015120320464820151118173854img-5562-copy.jpg', 15, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16021, 70, '20151118173717ej-after-six-0137.jpg', 16, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16022, 70, '20151118173454sem-titulo5-crop.jpg', 17, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16023, 70, '20151118173841img-5640-copy.jpg', 18, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16024, 70, '20151118173857img-5324-copy.jpg', 19, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16025, 70, '20151118174115img-5447-copy.jpg', 20, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16026, 70, '20160321173531cosmopolitam-04-vanessa-rozan-boca.jpg', 21, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16027, 70, '20160321173532cosmopolitam-05-vanessa-rozan-boca.jpg', 22, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(16028, 26, '20150819143351Camila Alves_7.jpg', 0, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(16029, 26, '20150819143350Camila Alves_11.jpg', 1, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(16030, 26, '20151028185952header-image.jpg', 2, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(16031, 26, '20150819143349Camila Alves_10.jpg', 3, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(16032, 26, '20151124081250ujo-10.jpg', 4, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(16033, 26, '20150827114123Cópia de Camila alves_12.jpg', 5, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(16034, 26, '20151116125149camila-alves-foto10-0034.jpg', 6, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(16035, 26, '20151027001449756983-camila-alves-escolheu-um-vestido-preto-950x0-2.jpg', 7, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(16036, 26, '20151123203538bf6dd67afd1a4ceebc5d80dc50bd1c6c.jpg', 8, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(16037, 26, '20151023000533hering-jeas-13052011-49352.jpg', 9, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(16038, 26, '20151027002108camila-alves-independent-spirit-awards-2013.jpg', 10, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(16039, 26, '20151023000530hering-jeas-13052011-49329.jpg', 11, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(16040, 26, '20151027002108camila-alves-sea-trees-premiere-68th-annual-wj1d6pclngqx.jpg', 12, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(16041, 26, '20160216130303camila-bobs.jpg', 13, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(16042, 26, '201511232035381404669107-694863-1404669327-noticia-normal.jpg', 14, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(16043, 49, '20150820155824Andre-Vasco_1.jpg', 0, '2017-07-07 13:48:50', '2017-07-07 13:48:50'),
(16044, 49, '20150820155824Andre-Vasco_3.jpg', 1, '2017-07-07 13:48:50', '2017-07-07 13:48:50'),
(16045, 49, '20150820155825Andre-Vasco_7.jpg', 2, '2017-07-07 13:48:50', '2017-07-07 13:48:50'),
(16046, 49, '20151027155919andrevasco-foto-plus.jpg', 3, '2017-07-07 13:48:50', '2017-07-07 13:48:50'),
(16057, 183, '2017021018155187324-grazi-larissa-com-o-visual-mais-diapo-1.jpg', 0, '2017-07-07 18:50:06', '2017-07-07 18:50:06'),
(16058, 183, '2017021018155212439209-1210521902309231-7574230067131505883-n.jpg', 1, '2017-07-07 18:50:06', '2017-07-07 18:50:06'),
(16059, 183, '2017021018155212439315-1210564612304960-6783171953601543279-n.jpg', 2, '2017-07-07 18:50:06', '2017-07-07 18:50:06'),
(16060, 183, '201702101815541273863-1210531165641638-5376309182525424030-o.jpg', 3, '2017-07-07 18:50:06', '2017-07-07 18:50:06'),
(16061, 183, '20170210181557pinocchio-em-as-aventuras-de-lasanha-e-ravioli.jpg', 4, '2017-07-07 18:50:06', '2017-07-07 18:50:06'),
(16062, 183, '2017021018155212512506-1210522322309189-6930811150985846460-n.jpg', 5, '2017-07-07 18:50:06', '2017-07-07 18:50:06'),
(16063, 183, '2017021018155512493798-1210529628975125-7672433714642108325-o.jpg', 6, '2017-07-07 18:50:06', '2017-07-07 18:50:06'),
(16064, 183, '20170210181555ana-barroso2.jpg', 7, '2017-07-07 18:50:06', '2017-07-07 18:50:06'),
(16065, 183, '2017021018155713692697-1343893678972052-3569039589119281303-n.jpg', 8, '2017-07-07 18:50:06', '2017-07-07 18:50:06'),
(16066, 183, '2017021018155712622381-1210552148972873-2364397318173853818-o.jpg', 9, '2017-07-07 18:50:06', '2017-07-07 18:50:06'),
(16067, 208, '20170705000759renata-ceribelli-3-750cr2.jpg', 0, '2017-07-10 17:11:12', '2017-07-10 17:11:12'),
(16068, 208, '20170705000731renata-ceribelli.jpg', 1, '2017-07-10 17:11:12', '2017-07-10 17:11:12'),
(16069, 208, '20170705000731renata-ceribelli-divulgacao-globo.jpg', 2, '2017-07-10 17:11:12', '2017-07-10 17:11:12'),
(16079, 77, '20170630103823chiara-gadaleta.jpg', 0, '2017-07-12 20:16:07', '2017-07-12 20:16:07'),
(16122, 211, '20170712165922mg-0355.jpg', 0, '2017-07-17 00:35:25', '2017-07-17 00:35:25'),
(16123, 211, '20170712165836mg-0369.jpg', 1, '2017-07-17 00:35:25', '2017-07-17 00:35:25'),
(16124, 211, '20170712165952mg-0404.jpg', 2, '2017-07-17 00:35:25', '2017-07-17 00:35:25'),
(16125, 211, '20170712165938img-0227.jpg', 3, '2017-07-17 00:35:25', '2017-07-17 00:35:25'),
(16126, 211, '20170712170008img-0302.jpg', 4, '2017-07-17 00:35:25', '2017-07-17 00:35:25'),
(16138, 195, '201707042313241k2a9623-t.jpg', 0, '2017-07-17 02:22:52', '2017-07-17 02:22:52'),
(16139, 195, '20170704231119luciana-mello-2.jpg', 1, '2017-07-17 02:22:52', '2017-07-17 02:22:52'),
(16140, 195, '20170704231147luciana-mello-home-5.jpg', 2, '2017-07-17 02:22:52', '2017-07-17 02:22:52'),
(16141, 195, '201707042313101k2a9532-t.jpg', 3, '2017-07-17 02:22:52', '2017-07-17 02:22:52'),
(16142, 195, '2017070423113028042015085812img-9154.jpg', 4, '2017-07-17 02:22:52', '2017-07-17 02:22:52'),
(16197, 209, '20170718123919tiago-sheuer-02tratada.jpg', 0, '2017-07-18 18:33:06', '2017-07-18 18:33:06'),
(16198, 209, '20170718123920tiago-sheuer-03.jpg', 1, '2017-07-18 18:33:06', '2017-07-18 18:33:06'),
(16213, 21, '20170418163713paula-picarelli-2.jpg', 0, '2017-07-18 19:58:11', '2017-07-18 19:58:11'),
(16214, 21, '20170418163636tata9766.jpg', 1, '2017-07-18 19:58:11', '2017-07-18 19:58:11'),
(16215, 21, '20170718165339aa-5580.jpg', 2, '2017-07-18 19:58:11', '2017-07-18 19:58:11'),
(16216, 21, '2017041816353322.jpg', 3, '2017-07-18 19:58:11', '2017-07-18 19:58:11'),
(16217, 21, '201704181635313.jpg', 4, '2017-07-18 19:58:11', '2017-07-18 19:58:11'),
(16218, 21, '20170418163632tata9848.jpg', 5, '2017-07-18 19:58:11', '2017-07-18 19:58:11'),
(16219, 21, '201704181635336.jpg', 6, '2017-07-18 19:58:11', '2017-07-18 19:58:11'),
(16220, 21, '201704181635335.jpg', 7, '2017-07-18 19:58:11', '2017-07-18 19:58:11'),
(16221, 21, '20170418163711foto-2.jpg', 8, '2017-07-18 19:58:11', '2017-07-18 19:58:11'),
(16222, 21, '20170418163636tata9811-2.jpg', 9, '2017-07-18 19:58:11', '2017-07-18 19:58:11'),
(16223, 21, '20170718165342aa-7316.jpg', 10, '2017-07-18 19:58:11', '2017-07-18 19:58:11'),
(16224, 21, '20170718165357aa-7161.jpg', 11, '2017-07-18 19:58:11', '2017-07-18 19:58:11'),
(16242, 89, '20170720103041guto-3.jpg', 0, '2017-07-20 13:35:13', '2017-07-20 13:35:13'),
(16243, 89, '20170720103041guto-4.jpg', 1, '2017-07-20 13:35:13', '2017-07-20 13:35:13'),
(16244, 89, '20170720103041guto-1.jpg', 2, '2017-07-20 13:35:13', '2017-07-20 13:35:13'),
(16245, 89, '20170720103041guto-5.jpg', 3, '2017-07-20 13:35:13', '2017-07-20 13:35:13'),
(16246, 89, '20170720103041guto-2.jpg', 4, '2017-07-20 13:35:13', '2017-07-20 13:35:13'),
(16247, 89, '20170720103041guto-6.jpg', 5, '2017-07-20 13:35:13', '2017-07-20 13:35:13'),
(16250, 52, '20170720105714gutoskolselectabaixa.jpg', 0, '2017-07-20 14:01:07', '2017-07-20 14:01:07'),
(16251, 52, '20170720105721gutoskolselectabaixa26.jpg', 1, '2017-07-20 14:01:07', '2017-07-20 14:01:07'),
(16283, 207, '20170704224859home-03.jpg', 0, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(16284, 207, '20170705105350image002.png', 1, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(16285, 207, '20170705105350image003.png', 2, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(16286, 207, '20170704223612home-01.jpg', 3, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(16287, 207, '2017070422361219275041-1464806523563021-6555810764894693437-n.jpg', 4, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(16288, 207, '20170704224859lobao.jpg', 5, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(16289, 207, '2017070422360917799064-1301608273255839-6942906500447619354-n.jpg', 6, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(16290, 207, '20170704223612maxresdefault-1.jpg', 7, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(16291, 207, '201707042236102016722924.jpg', 8, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(16292, 207, '20170704224859foto-do-lobao-revista-epoca.jpg', 9, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(16293, 207, '20170705105350image004.png', 10, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(16294, 198, '20170612133542adriana-araujo-004.jpg', 0, '2017-07-21 17:57:02', '2017-07-21 17:57:02'),
(16295, 198, '20170612133538adriana-araujo-009.jpg', 1, '2017-07-21 17:57:02', '2017-07-21 17:57:02'),
(16296, 198, '20170612133558adriana-araujo-003.jpg', 2, '2017-07-21 17:57:02', '2017-07-21 17:57:02'),
(16297, 198, '20170612133538adriana-araujo-010.jpg', 3, '2017-07-21 17:57:02', '2017-07-21 17:57:02'),
(16298, 198, '20170612133554adriana-araujo-005.jpg', 4, '2017-07-21 17:57:02', '2017-07-21 17:57:02'),
(16299, 198, '20170612133338adriana-araujo-007.jpg', 5, '2017-07-21 17:57:02', '2017-07-21 17:57:02'),
(16300, 198, '20170612133402adriana-araujo-002.jpg', 6, '2017-07-21 17:57:02', '2017-07-21 17:57:02'),
(16301, 198, '20170721145618adriana-araujo-caico-2608.jpg', 7, '2017-07-21 17:57:02', '2017-07-21 17:57:02'),
(16302, 198, '20170612133423adriana-araujo-008.jpg', 8, '2017-07-21 17:57:02', '2017-07-21 17:57:02'),
(16303, 198, '2017061315040413085493.jpeg', 9, '2017-07-21 17:57:03', '2017-07-21 17:57:03'),
(16304, 212, '20170720174424mg-3849-izabella.jpg', 0, '2017-07-24 13:39:13', '2017-07-24 13:39:13'),
(16305, 212, '20170720174124mg-4153-izabella.jpg', 1, '2017-07-24 13:39:13', '2017-07-24 13:39:13'),
(16306, 212, '20170720174143mg-3966-izabella.jpg', 2, '2017-07-24 13:39:13', '2017-07-24 13:39:13'),
(16307, 212, '20170720174127mg-3802-izabella.jpg', 3, '2017-07-24 13:39:13', '2017-07-24 13:39:13'),
(16308, 212, '20170720174318mg-4059-izabella.jpg', 4, '2017-07-24 13:39:13', '2017-07-24 13:39:13'),
(16309, 212, '20170720174235mg-4307-elizeth.jpg', 5, '2017-07-24 13:39:13', '2017-07-24 13:39:13'),
(16310, 212, '20170720174414mg-4182-izabella.jpg', 6, '2017-07-24 13:39:13', '2017-07-24 13:39:13'),
(16311, 212, '20170720174430mg-3817-izabella.jpg', 7, '2017-07-24 13:39:13', '2017-07-24 13:39:13'),
(16312, 67, '20160729083805img-9231.JPG', 0, '2017-07-24 18:47:03', '2017-07-24 18:47:03'),
(16322, 206, '20170718155942aline-012.jpg', 0, '2017-07-24 20:18:22', '2017-07-24 20:18:22'),
(16374, 29, '20170724203808mel-lisboa12-3553.JPG', 0, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(16375, 29, '20170724204442img-2151-mel-lisboa.jpg', 1, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(16376, 29, '2015121013344412359891-1082872701732319-1146080751165436876-n.jpg', 2, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(16377, 29, '20170131144034mocinha.jpg', 3, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(16378, 29, '20170724204538img-2504-mel-lisboa.jpg', 4, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(16379, 29, '20170724204644img-2222-mel-lisboa.jpg', 5, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(16380, 29, '20151022232539rita-lee-moraaolado.jpg', 6, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(16381, 29, '20170724204831img-2559-mel-lisboa-copia.jpg', 7, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(16382, 29, '20170724204301img-2162-mel-lisboa.jpg', 8, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(16383, 29, '20170724204547img-2541-mel-lisboa-pb.jpg', 9, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(16384, 29, '20170131143910359129.jpg', 10, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(16385, 29, '20170706103850pescadora-de-ilusao-por-deborah-schcolnic1.png', 11, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(16386, 29, '20170724204820img-2535-mel-lisboa.jpg', 12, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(16387, 29, '20170131143938mel-lisboa.jpg', 13, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(16388, 29, '20170724204332img-2150-mel-lisboa.jpg', 14, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(16410, 210, '20170724193235img-20150715-wa0034.jpg', 0, '2017-07-25 01:54:36', '2017-07-25 01:54:36'),
(16411, 210, '20170724193239tratada-betto.jpg', 1, '2017-07-25 01:54:36', '2017-07-25 01:54:36'),
(16412, 210, '20170724193234img-20150715-wa0033-1.jpg', 2, '2017-07-25 01:54:36', '2017-07-25 01:54:36'),
(16413, 210, '20170712162120img-20170708-wa0012.jpg', 3, '2017-07-25 01:54:36', '2017-07-25 01:54:36'),
(16414, 210, '20170712162120img-20170708-wa0013.jpg', 4, '2017-07-25 01:54:36', '2017-07-25 01:54:36'),
(16415, 210, '201707241932301.jpg', 5, '2017-07-25 01:54:36', '2017-07-25 01:54:36'),
(16416, 210, '20170724193234image.jpeg', 6, '2017-07-25 01:54:36', '2017-07-25 01:54:36'),
(16417, 210, '20170724193230image-3.jpg', 7, '2017-07-25 01:54:36', '2017-07-25 01:54:36'),
(16418, 210, '20170724193234site-pb-2.jpg', 8, '2017-07-25 01:54:36', '2017-07-25 01:54:36'),
(16419, 210, '20170724224122hoem-2222.jpg', 9, '2017-07-25 01:54:36', '2017-07-25 01:54:36'),
(16420, 210, '20170724224122beto-home-2.jpg', 10, '2017-07-25 01:54:36', '2017-07-25 01:54:36'),
(16421, 210, '20170724224122beto-home-6.jpg', 11, '2017-07-25 01:54:36', '2017-07-25 01:54:36'),
(16422, 147, '20160621150628biapaesdebarros-2.jpg', 0, '2017-07-25 13:43:56', '2017-07-25 13:43:56'),
(16423, 53, '20150821090319Dudu Bertholini.jpg', 0, '2017-07-25 18:08:40', '2017-07-25 18:08:40'),
(16436, 213, '20170725175248mg-3474-janaina.jpg', 0, '2017-07-25 21:24:55', '2017-07-25 21:24:55'),
(16437, 213, '20170725175102mg-3138-janaina.jpg', 1, '2017-07-25 21:24:55', '2017-07-25 21:24:55'),
(16438, 213, '20170725175132mg-3243-janaina.jpg', 2, '2017-07-25 21:24:55', '2017-07-25 21:24:55'),
(16439, 213, '20170725175122mg-3368-janaina.jpg', 3, '2017-07-25 21:24:55', '2017-07-25 21:24:55'),
(16440, 213, '20170725175141mg-3441-janaina.jpg', 4, '2017-07-25 21:24:55', '2017-07-25 21:24:55'),
(16441, 213, '20170725172353jana-2.jpg', 5, '2017-07-25 21:24:55', '2017-07-25 21:24:55'),
(16442, 213, '20170725175244mg-3583-janaina.jpg', 6, '2017-07-25 21:24:55', '2017-07-25 21:24:55'),
(16443, 213, '20170725175058mg-3126.jpg', 7, '2017-07-25 21:24:55', '2017-07-25 21:24:55'),
(16444, 213, '20170725175138mg-3194-janaina.jpg', 8, '2017-07-25 21:24:56', '2017-07-25 21:24:56'),
(16445, 213, '20170725175301mg-3571-janaina.jpg', 9, '2017-07-25 21:24:56', '2017-07-25 21:24:56');

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_idiomas`
--

CREATE TABLE `portfolio_idiomas` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portfolio_idiomas`
--

INSERT INTO `portfolio_idiomas` (`id`, `titulo`, `imagem`, `created_at`, `updated_at`) VALUES
(5, 'Português', 'bandeira-brasil.png', '2016-03-07 18:21:02', '2016-03-15 21:41:53'),
(6, 'Inglês', 'bandeira-eua.png', '2016-03-07 20:20:35', '2016-03-15 21:41:04'),
(7, 'Italiano', 'bandeira-italia.png', '2016-03-15 21:42:05', '2016-03-15 21:42:05'),
(8, 'Hebraico', 'bandeira-israel.png', '2016-03-15 21:42:15', '2016-03-15 21:42:15'),
(9, 'Francês', 'bandeira-franca.png', '2016-03-15 21:42:24', '2016-03-15 21:42:24'),
(10, 'Espanhol', 'bandeira-espanha.png', '2016-03-15 21:42:32', '2016-03-15 21:42:32'),
(11, 'Alemão', 'bandeira-alemanha.png', '2016-03-15 21:42:42', '2016-03-15 21:42:42'),
(12, 'Suéco', '20170530111917images.png', '2017-05-30 14:19:22', '2017-05-30 14:19:22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_idiomas_personalidades`
--

CREATE TABLE `portfolio_idiomas_personalidades` (
  `id` int(10) UNSIGNED NOT NULL,
  `personalidade` int(10) UNSIGNED NOT NULL,
  `idioma` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portfolio_idiomas_personalidades`
--

INSERT INTO `portfolio_idiomas_personalidades` (`id`, `personalidade`, `idioma`, `created_at`, `updated_at`) VALUES
(15, 24, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 24, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 97, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 97, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(166, 66, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(167, 66, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(168, 66, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(169, 66, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(170, 66, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(171, 66, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(184, 61, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(185, 61, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(186, 61, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(187, 61, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(208, 142, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(290, 139, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(434, 141, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(435, 141, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(502, 32, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(503, 32, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(515, 144, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(530, 73, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(531, 73, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(595, 91, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(596, 91, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(597, 91, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(598, 91, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(633, 82, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(785, 152, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(787, 165, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(799, 138, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(800, 138, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(801, 138, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(834, 146, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(835, 146, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(836, 146, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(840, 90, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(841, 90, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(842, 90, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(855, 63, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(856, 63, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(857, 63, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(858, 63, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(860, 173, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(861, 173, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(862, 173, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(864, 99, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(865, 99, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(866, 99, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(867, 99, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(872, 140, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(873, 140, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(877, 100, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(878, 100, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(880, 87, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(881, 87, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(882, 68, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(883, 68, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(884, 68, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(888, 84, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(889, 84, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(890, 44, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(891, 44, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(893, 31, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(903, 65, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(908, 95, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(909, 95, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(910, 64, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(911, 155, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(959, 177, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(960, 177, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(961, 177, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(962, 177, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(995, 154, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(996, 154, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1055, 181, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1099, 129, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1100, 129, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1105, 179, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1106, 179, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1107, 179, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1114, 178, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1120, 156, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1133, 19, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1134, 19, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1135, 19, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1175, 151, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1241, 159, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1242, 159, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1247, 167, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1261, 104, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1271, 107, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1277, 105, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1278, 105, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1279, 108, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1280, 108, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1281, 108, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1282, 108, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1284, 132, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1295, 42, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1296, 42, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1300, 188, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1307, 126, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1314, 149, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1316, 40, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1325, 112, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1326, 112, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1327, 112, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1328, 112, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1329, 153, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1330, 75, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1331, 125, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1332, 125, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1336, 34, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1337, 34, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1338, 34, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1339, 135, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1340, 135, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1341, 135, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1381, 148, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1382, 148, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1383, 150, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1397, 186, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1398, 186, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1399, 191, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1404, 35, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1428, 192, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1429, 72, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1430, 72, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1431, 88, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1432, 88, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1464, 128, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1465, 134, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1481, 160, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1486, 30, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1487, 30, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1495, 161, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1501, 176, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1502, 176, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1503, 176, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1519, 182, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1520, 182, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1529, 47, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1532, 197, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1533, 170, 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1535, 201, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1536, 201, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1537, 201, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1538, 56, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1539, 56, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1542, 196, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1543, 205, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1557, 33, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1558, 33, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1559, 33, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1567, 185, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1568, 185, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1596, 193, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1597, 193, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1598, 193, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1605, 70, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1606, 70, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1607, 26, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1608, 49, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1611, 183, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1612, 183, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1613, 208, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1614, 77, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1615, 77, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1616, 77, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1617, 106, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1618, 106, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1619, 106, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1631, 211, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1632, 211, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1634, 195, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1635, 195, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1652, 209, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1653, 209, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1666, 89, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1668, 52, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1675, 207, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1676, 198, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1677, 198, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1678, 212, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1695, 206, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1696, 206, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1697, 206, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1709, 29, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1710, 29, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1713, 210, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1714, 147, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1718, 143, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1719, 143, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1720, 143, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1721, 53, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1727, 213, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_palestras`
--

CREATE TABLE `portfolio_palestras` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_personalidades_id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portfolio_palestras`
--

INSERT INTO `portfolio_palestras` (`id`, `portfolio_personalidades_id`, `texto`, `created_at`, `updated_at`) VALUES
(300, 57, '<p><strong><u>Moda</u></strong></p>\r\n\r\n<p><strong><u>Mercado de Moda</u></strong></p>\r\n\r\n<p><strong><u>Empreendedorismo</u></strong></p>\r\n\r\n<ul>\r\n	<li>Constru&ccedil;&atilde;o do SPFW</li>\r\n</ul>\r\n\r\n<p><strong><em><u>Comportamento</u></em></strong></p>\r\n\r\n<p><strong><u>Novas Fam&iacute;lias</u></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n', '2015-12-04 17:54:18', '2015-12-04 17:54:18'),
(373, 97, '<p><u>N&atilde;o dou conta de tudo (o stress da mulher maravilha</u>)</p>\r\n\r\n<p><u>Meu marido me sabota (competi&ccedil;&atilde;o no casamento)</u></p>\r\n\r\n<p><u>N&atilde;o aceito ficar velha (ang&uacute;stia do envelhecimento)</u></p>\r\n\r\n<p><u>A infidelidade acendeu meu casamento</u></p>\r\n\r\n<p><u>N&atilde;o tenho tempo pra mim</u></p>\r\n\r\n<p><u>Meus filhos pensam que podem tudo</u></p>\r\n\r\n<p><u>Quero casar mas n&atilde;o consigo (para Homens e Mulheres)</u></p>\r\n\r\n<p><u>Adoro uma fofoca</u></p>\r\n\r\n<p><u>Ele s&oacute; pensa naquilo. E eu n&atilde;o.</u></p>\r\n\r\n<p><u>Sou gordinha</u></p>\r\n\r\n<p><u>Depois da separa&ccedil;&atilde;o virei outra</u></p>\r\n\r\n<p><u>Como segurei meu marido</u></p>\r\n\r\n<p><u>Fui m&atilde;e depois dos 40</u></p>\r\n\r\n<p><u>Minha sogra destruiu meu casamento</u></p>\r\n\r\n<p><u>Meu filho sofre Bulling</u></p>\r\n\r\n<p><u>Minha mulher &eacute; um sargento (esposa mandona)</u></p>\r\n\r\n<p><u>Eu me mordo de ci&uacute;mes</u></p>\r\n\r\n<p>&nbsp;</p>\r\n', '2016-03-17 19:59:33', '2016-03-17 19:59:33'),
(389, 96, '<p><strong><u>Panorama da beleza no s&eacute;culo XXI</u></strong></p>\r\n\r\n<ul>\r\n	<li>Maquiagem</li>\r\n	<li>Cabelo</li>\r\n</ul>\r\n', '2016-04-25 19:37:35', '2016-04-25 19:37:35'),
(433, 73, '<p><u><strong>Temas: </strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>- Trajet&oacute;ria e Processo Criativo</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>- Cria&ccedil;&atilde;o e Desenvolvimento de Cole&ccedil;&atilde;o&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>- Criando para si e para outros</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>- Processo criativo e desenvolvimento de um desfile</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2016-07-29 23:01:36', '2016-07-29 23:01:36'),
(444, 91, '<p><strong><u>A Import&acirc;ncia do Novo</u></strong></p>\r\n\r\n<ul>\r\n	<li>&nbsp;A&nbsp;import&acirc;ncia do novo no universo da moda, do design e da contemporaneidade.</li>\r\n	<li>Como identificar o novo, as ferramentas para a pesquisa, para onde olhar e o valor da cria&ccedil;&atilde;o autoral nos dias de hoje.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '2016-09-20 19:38:47', '2016-09-20 19:38:47'),
(459, 152, '<ul>\r\n	<li>Sexo</li>\r\n	<li>Sexualidade Feminina</li>\r\n	<li>Relacionamentos</li>\r\n	<li>Alimenta&ccedil;&atilde;o saud&aacute;vel</li>\r\n	<li>A vida com atividades f&iacute;sicas</li>\r\n	<li>Universo Fitness</li>\r\n</ul>\r\n', '2016-12-23 03:26:00', '2016-12-23 03:26:00'),
(464, 90, '<p><strong><u>Bate papo informal&nbsp;</u></strong></p>\r\n\r\n<ul>\r\n	<li>moda</li>\r\n	<li>estamparia</li>\r\n	<li>empreendedorismo</li>\r\n	<li>lifestyle.</li>\r\n</ul>\r\n', '2017-01-27 18:57:06', '2017-01-27 18:57:06'),
(473, 98, '<p>&nbsp;</p>\r\n\r\n<p><strong><u>Pol&iacute;tica e perspectivas s&oacute;cio-econ&ocirc;micas</u></strong></p>\r\n\r\n<p><strong><u>Comportamento</u></strong></p>\r\n\r\n<p><strong><u>Carreira e Mulher</u></strong></p>\r\n', '2017-01-27 21:45:48', '2017-01-27 21:45:48'),
(474, 87, '<p><u><strong>Moda</strong></u></p>\r\n\r\n<p><u><strong>Tend&ecirc;ncias</strong></u></p>\r\n\r\n<p><u><strong>Tecnologia</strong></u></p>\r\n\r\n<p><u><strong>Inova&ccedil;&atilde;o</strong></u></p>\r\n\r\n<p><u><strong>Desenvolvimento de cole&ccedil;&atilde;o</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-01-27 21:55:06', '2017-01-27 21:55:06'),
(475, 93, '<p><strong><u>Bate papo informal&nbsp;</u></strong></p>\r\n\r\n<ul>\r\n	<li>moda</li>\r\n	<li>estamparia</li>\r\n	<li>empreendedorismo</li>\r\n	<li>lifestyle.</li>\r\n</ul>\r\n', '2017-01-27 21:55:39', '2017-01-27 21:55:39'),
(476, 68, '<p><strong>M&iacute;dia trainning</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>- O que &eacute; not&iacute;cia</p>\r\n\r\n<p>- O caminho da informa&ccedil;&atilde;o</p>\r\n\r\n<p>- O funcionamento de uma reda&ccedil;&atilde;o</p>\r\n\r\n<p>- A rela&ccedil;&atilde;o com o jornalista</p>\r\n\r\n<p>- A pauta</p>\r\n\r\n<p>- O comportamento em entrevistas</p>\r\n\r\n<p>- Simula&ccedil;&atilde;o e avalia&ccedil;&atilde;o de entrevistas</p>\r\n', '2017-01-27 22:14:14', '2017-01-27 22:14:14'),
(478, 95, '<p><u><strong>Maquiagem e tend&ecirc;ncias para noivas </strong></u></p>\r\n\r\n<ul>\r\n	<li>maquiagem para diferentes hor&aacute;rios (dia, tarde, noite);</li>\r\n	<li>demonstra&ccedil;&atilde;o completa de dois looks;</li>\r\n	<li>dicas de como manter a maquiagem por mais tempo;</li>\r\n	<li>passo a passo para efeitos de video em HD, foto e presencial;</li>\r\n	<li>duvidas gerais e perguntas frequentes.</li>\r\n</ul>\r\n\r\n<p><u><strong>Tendencias de Maquiagem</strong></u></p>\r\n\r\n<ul>\r\n	<li>previs&atilde;o dos estilos de maquiagem da pr&oacute;xima temporada;</li>\r\n	<li>demonstra&ccedil;&atilde;o de uma maquiagem completa e apresenta&ccedil;&atilde;o de segunda maquiagem j&aacute; finalizada;</li>\r\n	<li>passo a passo explicativo com t&eacute;cnicas e produtos que ser&atilde;o importantes;</li>\r\n	<li>apresenta&ccedil;&atilde;o das imagens dos backstages internacionais e nacionais;</li>\r\n	<li>duvidas gerais e perguntas frequentes.</li>\r\n</ul>\r\n\r\n<p><strong>Maquiagem de moda para a vida real </strong></p>\r\n\r\n<ul>\r\n	<li>previs&atilde;o dos estilos de maquiagem da pr&oacute;xima temporada;</li>\r\n	<li>&nbsp;demonstra&ccedil;&atilde;o com passo a passo para maquiagem do dia-a-dia atualizada com t&eacute;cnicas de backtage;</li>\r\n	<li>dicas para adaptar as tendencias para a vida real;</li>\r\n	<li>duvidas gerais e perguntas frequentes.</li>\r\n</ul>\r\n\r\n<p><strong>Maquiagem para o dia e noite </strong></p>\r\n\r\n<ul>\r\n	<li>demonstra&ccedil;&atilde;o bem explicada com dicas de automaquiagem para aplica&ccedil;&atilde;o no dia a dia;</li>\r\n	<li>passo a passo para transformar um look b&aacute;sico em uma maquiagem impactante;</li>\r\n	<li>dicas para adaptar as t&eacute;cnicas em formatos de olhos e rostos diferentes;</li>\r\n	<li>dicas gerais para prolongar a dura&ccedil;&atilde;o e o efeito da maquiagem do dia para a noite;</li>\r\n	<li>&nbsp;duvidas gerais e perguntas frequentes.</li>\r\n</ul>\r\n', '2017-01-27 23:13:36', '2017-01-27 23:13:36'),
(479, 155, '<p>Izabella Camargo orienta e capacita porta-vozes para falar e lidar com a imprensa desde 2007 com um m&eacute;todo pr&oacute;prio de Media Training .</p>\r\n\r\n<p>Quem quer estabelecer uma comunica&ccedil;&atilde;o eficiente com a m&iacute;dia precisa compreender o dinamismo dos ve&iacute;culos de imprensa. Esse &eacute; o caminho pra compartilhar suas ideias de forma objetiva.&nbsp;</p>\r\n\r\n<p>Conte&uacute;do:</p>\r\n\r\n<p>1 - A m&iacute;dia como ela &eacute;;</p>\r\n\r\n<p>2 -&nbsp; Produzindo a not&iacute;cia que sua empresa precisa;</p>\r\n\r\n<p>3 - Como organizar as ideias para um discurso eficiente e evitar ru&iacute;dos na mensagem.</p>\r\n\r\n<p>4 - A import&acirc;ncia da imagem pessoal e da linguagem corporal;</p>\r\n\r\n<p>5 - Na pr&aacute;tica sem traumas.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-01-30 13:12:34', '2017-01-30 13:12:34'),
(483, 177, '<p><strong>1- A metodologia da percep&ccedil;&atilde;o &nbsp;na rela&ccedil;&atilde;o vendedor-produto-cliente:</strong> abordagens marxista (trabalho), positivista (n&uacute;meros) , fenomenol&oacute;gica (percep&ccedil;&otilde;es) e qu&acirc;ntica (rela&ccedil;&atilde;o direta com consumidor).&nbsp;<br />\r\n<br />\r\n<strong>2- O mundo euc&ecirc;ntrico:</strong> cada consumidor cont&eacute;m mundos. A troca do modelo m&iacute;dia de massas pelo de massas de m&iacute;dias.<br />\r\n<br />\r\n<strong>3- A psican&aacute;lise do consumo:</strong> como os produtos se circunscrevem no imagin&aacute;rio/identidade do consumidor.&nbsp;<br />\r\n<br />\r\n<br />\r\n&nbsp;</p>\r\n', '2017-02-06 19:13:34', '2017-02-06 19:13:34'),
(488, 104, '<p><u><strong>Hist&oacute;ria&nbsp; de 30 anos como alcoolista</strong></u></p>\r\n\r\n<ul>\r\n	<li>Vida e inf&acirc;ncia com a presen&ccedil;a do &aacute;lcool</li>\r\n	<li>Interna&ccedil;&otilde;es</li>\r\n	<li>Trajet&oacute;ria profissional</li>\r\n	<li>Sua vida como uma pessoa recuperada</li>\r\n	<li>Import&acirc;ncia&nbsp;da manuten&ccedil;&atilde;o do tratamento</li>\r\n	<li>Importantcia de grupos de apoio como o AA (Alco&oacute;licos An&ocirc;nimos).&nbsp;</li>\r\n	<li>Necessidade de um di&aacute;logo entre ind&uacute;stria, governo e sociedade.</li>\r\n</ul>\r\n\r\n<p><u><strong>Empoderamento da Mulher</strong></u></p>\r\n', '2017-04-26 17:36:35', '2017-04-26 17:36:35'),
(490, 107, '<p><u><strong>A Constru&ccedil;&atilde;o da Imagem de Moda</strong></u></p>\r\n\r\n<ul>\r\n	<li>Moda e Estilo</li>\r\n	<li>Styling e o Stylist</li>\r\n	<li>Hist&oacute;ria do Styling e os profissionais relevantes</li>\r\n	<li>&nbsp;Como ingressar no mercado de trabalho e criar uma assinatura &uacute;nica</li>\r\n	<li>Trajet&oacute;ria profissional</li>\r\n</ul>\r\n', '2017-04-27 19:20:24', '2017-04-27 19:20:24'),
(492, 105, '<p><strong><u>Trajet&oacute;ria e Processo Criativo</u></strong></p>\r\n\r\n<p><strong><u>Cria&ccedil;&atilde;o e Desenvolvimento de Cole&ccedil;&atilde;o&nbsp;</u></strong></p>\r\n\r\n<p><strong><u>Criando para si e para outros</u></strong></p>\r\n\r\n<p><strong><u>Processo criativo e desenvolvimento de um desfile</u></strong></p>\r\n', '2017-04-27 19:25:53', '2017-04-27 19:25:53'),
(493, 108, '<p><u><strong>Gastronomia</strong></u></p>\r\n\r\n<ul>\r\n	<li>Palestras sobre os mais variados temas hist&oacute;ricos e pr&aacute;ticos.</li>\r\n	<li>P&uacute;blico alvo: especialistas ou apreciadores dos temas abordados (como vinhos ou charutos)</li>\r\n	<li><u>Degusta&ccedil;&atilde;o de vinhos / cervejas / charutos</u>: as degusta&ccedil;&otilde;es s&atilde;o organizadas com diferentes amostras a serem provadas e comparadas, com explica&ccedil;&otilde;es a respeito de cada uma e das diferen&ccedil;as que as caracterizam.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><u>Hist&oacute;ria da gastronomia</u>: palestras abordam diferentes momentos da hist&oacute;ria da gastronomia, com a possibilidade de uma degusta&ccedil;&atilde;o, no fim da palestra, de pratos que correspondam &agrave; &eacute;poca hist&oacute;rica abordada.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><u>Harmoniza&ccedil;&atilde;o de comida e vinhos</u> (jantares enogastron&ocirc;micos): jantares com uma sucess&atilde;o de v&aacute;rios pratos, cada um deles acompanhado de um diferente vinho, com breves explica&ccedil;&otilde;es, entre um prato e outro, de como (e porque) foi feita a escolha deste determinado prato com seu vinho.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Relacionamento</strong></u></p>\r\n\r\n<ul>\r\n	<li>Palestras e atividades que buscam introduzir momentos de confraterniza&ccedil;&atilde;o e relaxamento, numa forma de contato menos formal e mais prazerosa.</li>\r\n	<li>P&uacute;blico alvo: executivos e clientes de alto n&iacute;vel; funcion&aacute;rios de escal&atilde;o elevado; potenciais clientes e prospects de alto n&iacute;vel. Tanto no caso de funcion&aacute;rios que est&atilde;o sendo premiados, quanto no caso de clientes que est&atilde;o sendo brindados com um evento especial, &eacute; frequente que os convites sejam estendidos aos c&ocirc;njuges.</li>\r\n	<li><u>Jantares enogastron&ocirc;micos</u> &ndash; Realizados em restaurantes de alto n&iacute;vel, ou em locais apropriados (sal&atilde;o de hot&eacute;is; mans&otilde;es alugadas; clubes etc.), os almo&ccedil;os ou jantares dirigidos por Josimar Melo criam uma atmosfera descontra&iacute;da em que o p&uacute;blico aprende algo mais sobre um tema que est&aacute; na moda (a cultura da gastronomia, a combina&ccedil;&atilde;o de pratos com vinhos), mas ao mesmo tempo se sente num clima de festa. O almo&ccedil;o ou jantar conta com uma sucess&atilde;o de v&aacute;rios pratos, cada um deles acompanhado de um vinho diferente, com breves explica&ccedil;&otilde;es, entre um prato e outro, de como (e porque) foi feita a escolha deste determinado prato com seu vinho. No final, vinho do Porto, charuto e mais descontra&ccedil;&atilde;o.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><u><strong>Motiva&ccedil;&atilde;o</strong></u></p>\r\n\r\n<ul>\r\n	<li>Palestras que buscam, a partir de um universo glamuroso, atraente e muito na moda (o universo da gastronomia), mostrar que mesmo neste mundo de prazer e glamour h&aacute; os mesmos obst&aacute;culos comuns a tantos of&iacute;cios e carreiras &ndash; e dar exemplos de como eles s&atilde;o superados, para a del&iacute;cia do p&uacute;blico.</li>\r\n	<li>P&uacute;blico alvo: profissionais de todas as &aacute;reas, especialmente de empresas que t&ecirc;m interesse em estimular a criatividade e a iniciativa de seus membros.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><u>Foco: Inova&ccedil;&atilde;o</u> &ndash; Josimar Melo conta a hist&oacute;ria de grandes chefs de cozinha que, desafiando o senso comum, criam uma culin&aacute;ria revolucion&aacute;ria e encantam o mundo. O palestrante relata a trajet&oacute;ria destes chefs, e tamb&eacute;m conta, por sua experi&ecirc;ncia de jornalista e apresentador de TV, sua pr&oacute;pria experi&ecirc;ncia vivida no contato (e na cozinha) de nomes como Ferran Adri&agrave; (El Bulli), Heston Blumenthal (The Fat Duck) e Ren&eacute; Redzepi (do restaurante Noma, eleito o melhor do mundo em 2010). No final ou durante (opcional), coquetel com demonstra&ccedil;&atilde;o de algumas destas t&eacute;cnicas revolucion&aacute;rias (canap&eacute;s desconstru&iacute;dos, drinques moleculares).</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><u>Foco: Supera&ccedil;&atilde;o</u> &ndash; Josimar Melo discute e mostra como chefs de cozinha sem forma&ccedil;&atilde;o formal se transformaram em l&iacute;deres na sua categoria, tornando-se exemplo de determina&ccedil;&atilde;o e sucesso. O que &eacute; necess&aacute;rio para se destacar num mundo competitivo onde n&atilde;o basta a educa&ccedil;&atilde;o t&eacute;cnica, mas s&atilde;o fundamentais a originalidade, a iniciativa e talento art&iacute;stico. S&atilde;o relatados hist&oacute;rias e casos exemplares, com base em profissionais como o brasileiro Alex Atala, do restaurante D.O.M.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-04-27 19:34:03', '2017-04-27 19:34:03'),
(495, 86, '<p><u><strong>Moda</strong></u></p>\r\n\r\n<ul>\r\n	<li>Como &eacute; o processo de cria&ccedil;&atilde;o de uma cole&ccedil;&atilde;o</li>\r\n	<li>Onde e como encontrar inspira&ccedil;&otilde;es {livros / cinema / viagens</li>\r\n	<li>Se mostre: respeite quem voc&ecirc; &eacute; por dentro de verdade, para que as pessoas possam se inspirar em voc&ecirc;</li>\r\n	<li>&nbsp;Inspire-se nas pessoas que voc&ecirc; v&ecirc;</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-05-05 20:41:29', '2017-05-05 20:41:29'),
(496, 111, '<p><strong><u>&nbsp;<em>Como Montar um Food Truck de Sucesso </em></u></strong></p>\r\n\r\n<ul>\r\n	<li>Por que um food truck?</li>\r\n	<li>Conceito e comida: voc&ecirc; &eacute; seu food truck e o seu food truck &eacute; voc&ecirc;.</li>\r\n	<li>Sonho e realidade de montar e manter um food truck: os altos e baixos do dia-a-dia.</li>\r\n	<li>Acelerar ou brecar? A import&acirc;ncia de um planejamento para entender a viabilidade de seu neg&oacute;cio.</li>\r\n</ul>\r\n\r\n<p><strong><u>Montando e dirigindo seu food truck.</u></strong></p>\r\n\r\n<ul>\r\n	<li>Como e com quem montar seu food truck:&nbsp; fornecedores, ve&iacute;culo, montagem das cozinhas.</li>\r\n	<li>Log&iacute;stica, locais de venda e equipe.</li>\r\n	<li>Colocando seu neg&oacute;cio nas mentes, cora&ccedil;&otilde;es e ruas da cidade atrav&eacute;s das redes sociais.</li>\r\n</ul>\r\n\r\n<p><u><strong>Sobre empreendedorismo e motivacional com base do&nbsp;</strong></u><em><u><strong> Food Truck</strong></u> </em></p>\r\n\r\n<ul>\r\n	<li>Tocando seu food truck na pr&aacute;tica.</li>\r\n	<li>Voc&ecirc; tem fome de qu&ecirc;? At&eacute; aonde quero chegar?</li>\r\n	<li>Vida X Trabalho - quanto pesa o seu trabalho na sua vida pessoal&nbsp;</li>\r\n	<li>P&oacute;s venda, balan&ccedil;o e dia seguinte.</li>\r\n</ul>\r\n\r\n<p><u><strong>Aulas-show de cozinha em geral&nbsp; e cozinha de food truck</strong></u></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-05-17 18:52:48', '2017-05-17 18:52:48'),
(499, 128, '<p>Palestras&nbsp; focadas no tema &quot;Finan&ccedil;as&quot;.</p>\r\n', '2017-05-29 17:29:27', '2017-05-29 17:29:27'),
(501, 176, '<p>Temas de Palestras, Wokshops e Master Classes:</p>\r\n\r\n<p>. Tend&ecirc;ncias de Maquiagem<br />\r\n. Pele - diferentes t&eacute;cnicas e acabamentos<br />\r\n. Noivas<br />\r\n. Red Carpet<br />\r\n. Feline Eyes<br />\r\n. Creative Workshop</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(502, 92, '<ul>\r\n	<li>O Poder da Imagem</li>\r\n	<li>Tendencias de moda&nbsp;</li>\r\n	<li>Tendencias de comportamento</li>\r\n	<li>Ecotendencias</li>\r\n</ul>\r\n', '2017-07-07 13:12:46', '2017-07-07 13:12:46'),
(503, 106, '<p><strong><u>Ecoera</u></strong></p>\r\n\r\n<ul>\r\n	<li>Descarte t&ecirc;xtil</li>\r\n	<li>Reciclagem de residuos</li>\r\n	<li>P&oacute;s-consumo</li>\r\n	<li>Novas Tecnologias.</li>\r\n</ul>\r\n', '2017-07-12 20:18:31', '2017-07-12 20:18:31'),
(505, 89, '<p><u>Esculpindo Mem&oacute;rias: O Design na Era Digital</u></p>\r\n\r\n<ul>\r\n	<li>&nbsp;universo digital e aos novos modos de vida</li>\r\n	<li>as bases para se entender os rumos da arquitetura, do design e de comportamento nos tempos atuais</li>\r\n	<li>informatiza&ccedil;&atilde;o do cotidiano, intera&ccedil;&atilde;o, cibercultura e mem&oacute;ria, cultura ciborgue e novas subjetividades.&nbsp;</li>\r\n	<li>produ&ccedil;&atilde;o de projetos do Estudio Guto Requena</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>&nbsp;PARIS, FRAN&Ccedil;A - &quot;O que sobre o amor?&quot;, Na Universidade Sorbonne</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>&nbsp;BERLIM, ALEMANHA - &quot;Sobre o Amor, o Design e a Cidade&quot;, na Humboldt Universitat</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>&nbsp;BARCELOS, PORTUGAL - &quot;Vamos tomar um mergulho&quot; no IPCA (Escola Superior de Design)</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>&nbsp;BREMEN, ALEMANHA - &quot;Hacking the City&quot;, na Universidade das Artes de Bremen</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>&nbsp;COPENHAGUE, DINAMARCA - &quot;E quanto ao amor? Design, Processo e Projetos do Estudio Guto Requena &quot;, na KEA (Copenhagen School of Design and Technology)</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li>&nbsp;BARCELONA, ESPANHA - &quot;Sobre o Amor, a Experi&ecirc;ncia e a Cidade&quot; no ICOUL - Congresso Internacional sobre Paisagem Urbana</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-07-20 13:35:13', '2017-07-20 13:35:13'),
(506, 109, '<p><u><em><strong>Gest&atilde;o, cria&ccedil;&atilde;o, lideran&ccedil;a de equipes </strong></em></u></p>\r\n\r\n<ul>\r\n	<li>Trajet&oacute;ria do pequeno bar de seu pai at&eacute; se transformar num dos mais celebrados e premiados restaurantes&nbsp;em toda a hist&oacute;ria da gastronomia brasileira.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-07-20 14:21:17', '2017-07-20 14:21:17'),
(508, 143, '<p><strong><u>Palestras:</u></strong></p>\r\n\r\n<ul>\r\n	<li>Empoderamento feminino</li>\r\n	<li>Mulheres jornalistas cobrindo guerras e epidemias</li>\r\n	<li>Oriente M&eacute;dio</li>\r\n	<li>Pol&iacute;tica dos Estados Unidos</li>\r\n	<li>Pol&iacute;tica externa do Brasil</li>\r\n	<li>Pol&iacute;tica internacional</li>\r\n</ul>\r\n', '2017-07-25 14:18:09', '2017-07-25 14:18:09');

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_paralaxe`
--

CREATE TABLE `portfolio_paralaxe` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_personalidades_id` int(10) UNSIGNED NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portfolio_paralaxe`
--

INSERT INTO `portfolio_paralaxe` (`id`, `portfolio_personalidades_id`, `imagem_1`, `imagem_2`, `imagem_3`, `imagem_4`, `imagem_5`, `created_at`, `updated_at`) VALUES
(1767, 51, '20150909174139Pedro Andrade_4.png', '20151022162033pedro-foto-2.png', '20151022164604pedro-foto-3.png', '20150820160648Pedro Andrade_9.png', '20150909174810b2a60897ab6bfc0fe217c932013d139b 2.png', '2016-03-07 19:48:11', '2016-03-07 22:27:16'),
(1778, 24, '20151009131455Kapo Astrid 002.png', '20160307133825foto02.png', '20150817132349Astrid Fontenelle_2.png', '20151210140820astrid-aeromoca.png', '20151209171116astrid-fontenelle-9778-1.png', '2016-03-17 01:34:46', '2016-03-17 01:34:46'),
(1801, 23, '201603171714592925517c-0c0e-4214-8fa0-715c57f0741f.jpg', '2016031717182512670599-10205681775545278-7817980979783068866-n.jpg', '', '', '', '2016-03-17 20:18:29', '2016-03-17 20:18:29'),
(1856, 96, '20151130211011labdudamolinos-curso1.png', '20151130211011labdudamolinos-curso2.png', '', '', '', '2016-04-25 19:37:35', '2016-04-25 19:37:35'),
(1868, 71, '2015120713064420151204134922mg-72231-900x600.png', '2015120712295220151207025340beautitas-pantene-gisele-bundchen-egerie.png', '2015120712302620151204140829alinne-moraes-marie-claire.png', '20151207125706106613586.png', '20151207130136editorial-duda-molinos-3-2.png', '2016-05-04 18:54:28', '2016-05-04 18:54:28'),
(1911, 139, '20160328170340vicmeirellescorporativo03.jpg', '20160328170358vicmeirellescasamentos13.jpg', '20160328175135imagem-3-home-vic-meirelles.jpg', '20160328170429mg-4302.jpg', '20160328173759arranjovicmeirelles03.jpg', '2016-05-24 18:09:57', '2016-05-24 18:09:57'),
(2018, 32, '20151027004924carlafinalalta.jpg', '20151027003925042578037-exh00.jpg', '20160623145119carla-lamarca-home-3.jpg', '20160321155918home-5.jpg', '20151027003925burn-epoque-carla-lamarca-08.jpg', '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(2039, 73, '2016072918145905-1.jpg', '2016072918072302.jpg', '20160729182749acoes.png', '20160729183523230616-oliveira.jpg', '20160729183707doestilista3.jpg', '2016-07-29 23:01:36', '2016-07-29 23:01:36'),
(2166, 130, '20151123153912cenouraebronze04.png', '20151123154730agua-doce-quem-anuncio-dupla-450x310-outline3.png', '20151123154142imagem1.png', '20151123154100sky-6.png', '20151123155230sky-003.png', '2016-11-08 23:57:42', '2016-11-08 23:57:42'),
(2168, 131, '20151217162933foto-home-1-mascara.jpg', '20151217163201foto-home-2-mascara.jpg', '20151217155104post-3206-1242326188.jpg', '20151217155547qq.jpg', '20151217160959juliano-cazarre-andre-schiliro-2.jpg', '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(2208, 152, '20160725144912home-2.jpg', '20160725144912home-5-2.jpg', '20160728211829home-5.jpg', '20160728211117home-penelope.jpg', '', '2016-12-23 03:25:59', '2016-12-23 03:25:59'),
(2210, 165, '20160921174255home-1.jpg', '20160921173201home-2-sergio-a.jpg', '20160921173201home-03.jpg', '20160921173230home-5.jpg', '20160921173201home-4.jpg', '2017-01-16 14:49:39', '2017-01-16 14:49:39'),
(2218, 138, '20160216152855celso-zucatelli-pb.png', '20160216134057050-68609-1.png', '20160216144833home-5-tratada.png', '20160216134056celso49.png', '20160216155411zuca-contato.png', '2017-01-19 19:36:49', '2017-01-19 19:36:49'),
(2254, 100, '20150921174330Thuita 222.jpg', '20150921174329Thulita 222.jpg', '20150921174325DSC_1286edited.jpg', '20150921174325LUITHA 1 .jpg', '', '2017-01-27 21:22:59', '2017-01-27 21:22:59'),
(2255, 39, '20160407211026img-1371.JPG', '2015090912263120121015193651552.png', '20150820144635Luize Altenhofen_3.png', '20150820144636Luize Altenhofen_1.png', '20160407211127img-1372.JPG', '2017-01-27 21:27:16', '2017-01-27 21:27:16'),
(2261, 78, '20160113132746guerreiro-2.png', '201601131319401743497-1224666104225489-5800184934618979306-n.png', '20151130211116rg.png', '20151029103455regina-guerreiro.png', '2016011313193920160105152838sem-titulo-1.png', '2017-01-27 21:57:58', '2017-01-27 21:57:58'),
(2263, 68, '20160308172401boardi-home-2.jpg', '', '', '', '', '2017-01-27 22:14:14', '2017-01-27 22:14:14'),
(2267, 44, '20151105162554silvia-home-1.png', '20150820152247Silvia Poppovic.png', '201509091703482.png', '20150909173217FullSizeRender.png', '20150909173217FullSizeRender 2.png', '2017-01-27 22:38:07', '2017-01-27 22:38:07'),
(2269, 31, '20150921134532barbara-gancia-351 2.png', '20150925123228--------bgancia 2.png', '20150925123313Barbara Gancia_1.png', '20150921134532barbara-gancia-331 2.png', '2015092512315020150302-triciavieira-saiajusta-_HRO2775.png', '2017-01-27 22:52:05', '2017-01-27 22:52:05'),
(2276, 65, '20160224151819chuf-guga-chacra-02.png', '20160224153236guga-capa.png', '20160321154502home-5.jpg', '20160321155132home-6.jpg', '', '2017-01-27 23:11:20', '2017-01-27 23:11:20'),
(2277, 155, '20160719141057home-5-foto-1.jpg', '20160725111158home-valendo.jpg', '20160725111229image2-2.JPG', '20160719133153home-3.jpg', '20160719133153isabella-4.jpg', '2017-01-30 13:12:34', '2017-01-30 13:12:34'),
(2300, 154, '20160701160450ronald-02.jpg', '20160701160804home-2-valendo.jpg', '20160701170004home-3.jpg', '20160701165927f-140274.jpg', '20160808175809opcao-foto-corte-folder-02.jpg', '2017-02-09 18:24:01', '2017-02-09 18:24:01'),
(2346, 181, '20170210205101home-2.jpg', '20170210205101home-5.jpg', '20170210205136home-3.jpg', '20170210205155home-4.jpg', '20170210205136home-6.jpg', '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(2360, 103, '2016020415420912647311-1062787020439181-3091300070837155785-n.png', '20151023192712sem-titulo-1.png', '20150924135250IMG_0624.png', '20150924135250IMG_0626.png', '20150924135233IMG_0620.png', '2017-02-16 19:44:15', '2017-02-16 19:44:15'),
(2376, 129, '20160530132551lucia-bronstein-5.jpg', '20151209160736roza2-interna.png', '20151209161659lucia-bronstein.png', '20160530132607copia-de-lucia-bronstein-2.jpg', '', '2017-02-20 03:13:01', '2017-02-20 03:13:01'),
(2380, 179, '20170213121339veronica-5.jpg', '20170213120116home-2.jpg', '20170213120116home-03.jpg', '20170213120413veronica.jpg', '20170213120116hiome-4.jpg', '2017-02-20 03:53:13', '2017-02-20 03:53:13'),
(2382, 38, '20151103180753home-radio.jpg', '20151019141740ligia-mendes-teste-ii.jpg', '20151027214428liogia-2.jpg', '20150820144248Ligia Mendes_8.jpg', '20151103170006mg-9894.jpg', '2017-02-21 13:41:12', '2017-02-21 13:41:12'),
(2388, 178, '20170210192421home.jpg', '20170210184750ale-schumaquer-01.jpg', '20170210192252home-5-2.jpg', '20170210192252home-5.jpg', '20170210192252schumarquer.jpg', '2017-03-08 02:55:15', '2017-03-08 02:55:15'),
(2405, 19, '201510211410542015-780736276-eu-nao-dava-praquilo-com-cassio-scapin-foto-joao-caldas-fo-.jpg', '20151021141140img-561450-cassio-scapin20131101181383337057.jpg', '20160510153007img-2144.jpg', '20151021141054cassio-scapin1jpg1.jpg', '2015102114105411266704.jpeg', '2017-03-14 20:49:42', '2017-03-14 20:49:42'),
(2469, 159, '20161108202036home-2.jpg', '20161108202035r1adlz.jpg', '20161108202034home-3.jpg', '20161108202034home-5.jpg', '20161108202103home4.jpg', '2017-04-13 20:45:54', '2017-04-13 20:45:54'),
(2474, 167, '20161004184100cesar-mello-home-1.jpg', '20161004164919cesar-mello-home-2.jpg', '20161004164919cesar-mello-home-3.jpg', '20161004184030untitled-3.jpg', '20161004165021cesar-mello-home-4.jpg', '2017-04-18 21:39:13', '2017-04-18 21:39:13'),
(2490, 42, '20151009152718600full-marilia-moreno.png', '20160623153317marilia-home-5-01.jpg', '20150909141107Marilia5bw.png', '20150909133419SITE _ Marilia Moreno _ Eletric Sommer 003.png', '20160623154023broderick-hunter-v1-issue3-29.jpg', '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(2491, 54, '2016011216463212.png', '20160112164635chilli.png', '201601121646355.png', '201601121648379.png', '20160112164702verao-2010.png', '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(2492, 188, '20170222174108andre-lima-home.jpg', '20170222174106andre-lima-2.jpg', '20170222180138sofa.jpg', '20170222180139home-5.jpg', '', '2017-05-11 13:50:28', '2017-05-11 13:50:28'),
(2494, 126, '20160321161218edgar-03.jpg', '20160321161218edgar.jpg', '', '', '', '2017-05-12 17:15:26', '2017-05-12 17:15:26'),
(2498, 149, '20160530235845home-2.jpg', '20160804115626home-2.jpg', '20160530234107home-5.jpg', '20160808205007a99d9828-final.jpg', '20160523213532saulo-valendo.jpg', '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(2500, 40, '20151126165023sem-titulo-1.png', '20151022160653marcelle-bittar-foto-2.png', '20160808210712olympiada-08.jpg', '20151126152806marcelle-bittar-para-rev-made-por-gustavo-zylberstajn-231.png', '20151126165922sem-titulo-1.png', '2017-05-17 15:42:25', '2017-05-17 15:42:25'),
(2504, 112, '20151019182105erika-2-.png', '20151019182810grazi-erika-.png', '20151019182341l-oficila-.png', '20151019180927242685-465564-lof-hommes-04-1.png', '20151019180928capa-lofficiel-001.png', '2017-05-17 18:32:22', '2017-05-17 18:32:22'),
(2506, 60, '20160321161711food-truck-creditos-eliana-rodrigues-3.JPG', '20160321161712food-truck-creditos-eliana-rodrigues-1.JPG', '', '', '', '2017-05-17 18:51:50', '2017-05-17 18:51:50'),
(2508, 34, '20170327155231costanza-home-3.jpg', '20170327155355home5.jpg', '20161220164330whatsapp-image-2016-12-04-at-095523.jpeg', '20150827121831hommage-costanza-pascolato-l-oqh1zp1.png', '201511232143194.png', '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(2509, 135, '20161220142615whatsapp-image-2016-12-04-at-095523.jpeg', '20170327155445home5.jpg', '20170327153857costanza-home-3.jpg', '201601191429415.png', '20170327154500home-5.jpg', '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(2517, 148, '20160531192519quadro-negro.jpg', '20160531192259consul.jpg', '', '', '', '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(2520, 150, '20160613115045pedo.jpg', '20160531220635pedro-home-5-02.jpg', '20160613112821pedro-bos.jpg', '201605312208581235000-426994837422517-1193398048-n.jpg', '2016053122383813263874-884286775026652-3222692745999426366-n.jpg', '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(2524, 102, '20160425164028tratada-lilian-2.jpg', '20170518213850biquini-lilian-hiome-2.jpg', '20170518213913home-3.jpg', '20170518220002capa-valendo-home.jpg', '20160425164028img-7727-1.jpg', '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(2528, 186, '20170214194245home-2.jpg', '20170214193829home2.jpg', '20170214193829bethe-3.jpg', '20170214193829home-4.jpg', '', '2017-05-22 18:19:01', '2017-05-22 18:19:01'),
(2529, 191, '20170329123454cris-home-2.jpg', '20170329123454cris-home-02.jpg', '20170329123454cris-carminato03.jpg', '20170329123453cris-003.jpg', '20170329123453cris-005.jpg', '2017-05-22 19:17:47', '2017-05-22 19:17:47'),
(2532, 35, '20151125110652daniella-cicarelli-foto1.jpg', '20151125102530daniella-cicarelli-foto-2.jpg', '20151125102557daniella-cicarelli-foto-3.jpg', '20150819155438daniella Cicarelli_6.jpg', '20160623150941home-5.jpg', '2017-05-22 19:43:30', '2017-05-22 19:43:30'),
(2541, 192, '20170404152906vitor-home-2.jpg', '20170404152909home-2.jpg', '20170404152823home-03.jpg', '20170404152819home-4.jpg', '20170404153124copia-2-de-desmonte-139.jpg', '2017-05-25 21:06:41', '2017-05-25 21:06:41'),
(2550, 101, '20150929153150Tuti Muller 5.png', '20150929153150Tuti Muller 4.png', '2016020115002611050694-1121129394597617-8470617149845319375-n.png', '20151210161030tutti.png', '', '2017-05-26 23:05:38', '2017-05-26 23:05:38'),
(2565, 160, '20170530154933fundo-preto.jpg', '20170530154624boa-8.jpg', '', '', '', '2017-05-30 18:49:42', '2017-05-30 18:49:42'),
(2568, 30, '20170530170533home-2.jpg', '20170530162923sacaff-01.jpg', '20151009120900Carol Scaff tratada .jpg', '20170530162816sacaff-home-4.jpg', '20170530163046carol-scaff.jpg', '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(2572, 161, '20161025194552invertida-tratada.jpg', '20161025193932vandre.jpg', '20161025201331home-3.jpg', '20161025185419home-5.jpg', '20161025185420vandre-home-5.jpg', '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(2574, 176, '20170321122051valendo-site.jpg', '20170320223431fabi-valendo.jpg', '20170321121410unknown-9.jpeg', '20170320222536fabi-home-4.jpg', '20170321121338untitled-1.jpg', '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(2585, 182, '20170613143717home-01.jpg', '20170613143719home-2-marjorie.jpg', '20170613143718home-03.jpg', '20170613143718home-4.jpg', '20170613143719home-5.jpg', '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(2589, 47, '20151015185722tratada-.png', '20150826160740SANDRO_BARROS_VERAO2015_CAMPANHA_524.png', '20151015192331tataiab-home-5-.png', '20150820154545Tatiana Dumenti_5.png', '20150928182250Tatiana Dumenti -  para camp.  da coleção 740  Park Avenue do designer Sandro Barros - por Peu Campos. (117) .png', '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(2592, 197, '20170613163206home-01.jpg', '20170613163214untitled-1.jpg', '20170613163205godoy-home-3.jpg', '20170613163212godoy-5.jpg', '20170613163430home-5.jpg', '2017-06-22 17:15:36', '2017-06-22 17:15:36'),
(2593, 170, '20161004182534home-01.jpg', '20161004182833omar-02.jpg', '20161004182500home-2.jpg', '20161004183404omar-4.jpg', '20161004183308omar-5.jpg', '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(2595, 201, '20170518190758home-4-andre.jpg', '20170518190821tratada.jpg', '20170518191621home-andre-dias-5.jpg', '20170518190723personagem.jpg', '20170518194344valendo.jpg', '2017-06-23 15:02:22', '2017-06-23 15:02:22'),
(2596, 56, '20151019173608nike-.png', '20151019173220nike-2.png', '20160913165442pedro02.jpg', '20151019184008pedro-lourenco-dellano-.png', '201511261725311292332-524523770965347-1672448288-o.png', '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(2598, 190, '20170412123837pecas.jpg', '20170412123838livros-centro.jpg', '20170412123920novela-3.jpg', '', '', '2017-06-23 15:05:26', '2017-06-23 15:05:26'),
(2601, 196, '20170622174105vidotti-01.jpg', '20170622174105vidotti-ho-2.jpg', '20170622180014vidotti-03.jpg', '20170622175232hone-03-valendo.jpg', '20170622175301vidotti-5.jpg', '2017-06-23 15:33:38', '2017-06-23 15:33:38'),
(2603, 205, '20170623122337home-01.jpg', '20170623122338home-2.jpg', '20170623122338home-4.jpg', '', '', '2017-06-23 15:48:55', '2017-06-23 15:48:55'),
(2608, 45, '20150929133533Renata Fan 5.png', '20150929140805Screenshot_2013-05-16-15-18-05.png', '20150929133841Digitalizar0014.png', '20150820153323Renata-Fan_5.png', '20150820153323Renata-Fan_3.png', '2017-06-26 17:06:03', '2017-06-26 17:06:03'),
(2611, 158, '20160810123034images.jpeg', '', '', '', '', '2017-06-28 18:48:51', '2017-06-28 18:48:51'),
(2615, 33, '20150925131321chiara-gadaleta-interna.png', '20150925131321chiara.png', '20150925131320aguamarinha.png', '20150925131321chiara_gadaleta_klajmic__exercito_salvacao.png', '20150925131321chiara.png', '2017-06-30 13:37:53', '2017-06-30 13:37:53'),
(2638, 55, '20170411164201img-8412.PNG', '', '', '', '', '2017-07-07 13:10:58', '2017-07-07 13:10:58'),
(2639, 70, '20151118183304foto-home-5-1-vanessa-rozan-fundopreto.png', '20160928150226img-20160926-wa0002.jpg', '20151118183316foto-home-5-3-fundo-preto.png', '20151118173100gloss-3.png', '20170523164157vanessa-bolsa.jpg', '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(2640, 26, '20151003124655Camila Alves_8.png', '20151023001718camila-22-.png', '20151003123713Cópia de Camila Alves_4.png', '20150819143225Camila Alves_2.png', '20151123203452large.png', '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(2641, 49, '20151016182302teste-home-3.png', '20150909181110TATO BELLINI e MARCOS DUARTE - ANDRÉ VASCOS (12).png', '20151023194923sem-titulo-1.png', '20150820155804Andre Vasco_4.png', '20150909182253andre reduzida .png', '2017-07-07 13:48:50', '2017-07-07 13:48:50'),
(2643, 183, '20170210181440nana-barroso-02.jpg', '20170210183241home-55.jpg', '20170210181424home-5-foto-3.jpg', '20170210181426home-5-foto-05.jpg', '20170210181423home-4.jpg', '2017-07-07 18:50:06', '2017-07-07 18:50:06'),
(2645, 195, '20170704230431home-01.jpg', '20170704230041lu-mello-home-02.jpg', '20170704230629luciana-home-3.jpg', '20170418174614luciana-baixa.jpg', '20170704231024home-5.jpg', '2017-07-17 02:22:52', '2017-07-17 02:22:52'),
(2655, 21, '20170718165238picarelli-home-1.jpg', '20170418163457paula.jpg', '20170418163405picarelli.jpg', '20151023165932foto-2.png', '201510011111322.png', '2017-07-18 19:58:11', '2017-07-18 19:58:11'),
(2658, 189, '20170309203141belise-home-02.jpg', '20170309203142belise-home-2.jpg', '20170309203139belise-3.jpg', '20170309204203belise-5.jpg', '20170309203633belize-5.jpg', '2017-07-19 21:23:41', '2017-07-19 21:23:41'),
(2660, 52, '20151027131935foto-home-01.png', '20151027132150foto-home-2.png', '20151029120354foto-home-3.png', '20151027185236foto-home-4.png', '20151027185819foto-home-5.png', '2017-07-20 14:01:07', '2017-07-20 14:01:07'),
(2661, 58, '20151201171851klm-mocotodest.png', '20151201171925poder.png', '20151201172023moty2012-rodrigo-08.png', '20151201172007unknown.png', '20151201172244andorinhachef.png', '2017-07-20 14:20:45', '2017-07-20 14:20:45'),
(2666, 207, '20170704222738lobao-home-3.jpg', '20170704222713home-2-valendo.jpg', '20170704222713home-4.jpg', '20170704222715home-5-lobao.jpg', '20170704224827home-5.jpg', '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(2667, 198, '20170612133047hom-e-01-araujo.jpg', '20170612133047adriana-araujo.jpg', '20170612133047home-03-adriana-araujo.jpg', '20170612133047home-4-ana.jpg', '20170612133047home-5.jpg', '2017-07-21 17:57:02', '2017-07-21 17:57:02'),
(2668, 212, '20170720173920home-01-izabella.jpg', '20170720173920home-03.jpg', '20170720173921izabella-home-2.jpg', '20170720173920home-04-izabella.jpg', '20170720173921homer-4.jpg', '2017-07-24 13:39:13', '2017-07-24 13:39:13'),
(2674, 206, '20170718152622aline-01.jpg', '20170718152623tratada.jpg', '20170718152653untitled-1.jpg', '20170718152622home-4.jpg', '', '2017-07-24 20:18:21', '2017-07-24 20:18:21'),
(2680, 29, '20170724203652mel-home-1.jpg', '20170724202355home-2.jpg', '20170724202438home-02-mel.jpg', '20170724202355home-5-mel.jpg', '20170724203411home-5-mel.jpg', '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(2683, 210, '20170724220307beto-home-2.jpg', '20170724220306beto-home-6.jpg', '20170724220307home-5.jpg', '20170724220307home-4.jpg', '20170724220630hoem-2222.jpg', '2017-07-25 01:54:36', '2017-07-25 01:54:36'),
(2684, 53, '2016012817312812509171-952892368132862-8537451827847182248-n.png', '2015100619123911125540_892322204189879_1798029636346766265_o.png', '2015100619123911999548_892350687520364_1793146410742517052_o.png', '2015100619123611741092_873623956059704_2114176257634859792_o.png', '2015100619123812006346_899785296776903_5199142873140871819_n.png', '2017-07-25 18:08:39', '2017-07-25 18:08:39'),
(2686, 213, '20170725174239home.jpg', '20170725174329janaina-home-02.jpg', '20170725174840home-666-1.jpg', '20170725174239janaina.jpg', '20170725174240home-5.jpg', '2017-07-25 21:24:55', '2017-07-25 21:24:55');

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_personalidades`
--

CREATE TABLE `portfolio_personalidades` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_categorias_id` int(10) UNSIGNED NOT NULL,
  `portfolio_subcategorias_id` int(10) UNSIGNED DEFAULT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cor_nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_entrada` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumb_home` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_site` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arquivo_release` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publicar` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portfolio_personalidades`
--

INSERT INTO `portfolio_personalidades` (`id`, `portfolio_categorias_id`, `portfolio_subcategorias_id`, `nome`, `slug`, `cor_nome`, `imagem_entrada`, `thumb_home`, `link_site`, `arquivo_release`, `publicar`, `created_at`, `updated_at`) VALUES
(19, 2, 23, 'Cassio Scapin', 'cassio-scapin', 'FFF', '20151019163030img-20151019-wa0020.jpg', 'home-2.jpg', '', 'release_cassio-scapin.docx', 1, '2015-08-17 14:31:28', '2017-01-27 18:38:58'),
(21, 2, 24, 'Paula Picarelli', 'paula-picarelli', 'FFF', '20151019162118p-picarelli-home-.jpg', 'picarelli.jpg', '', 'release_paula-picarelli.doc', 1, '2015-08-17 15:33:49', '2017-01-27 21:51:51'),
(23, 1, 14, 'Amanda Ramalho', 'amanda-ramalho', 'FFF', '20151023191334aamnda-homne-rdio-.jpg', '', '', 'release_amanda-ramalho.doc', 0, '2015-08-17 16:13:24', '2017-02-16 19:43:48'),
(24, 1, 12, 'Astrid Fontenelle', 'astrid-fontenelle', 'FFF', 'fullsizerender.jpg', 'fullsizerender.jpg', '', 'release_astrid-fontenelle.docx', 0, '2015-08-17 16:20:23', '2016-12-16 17:32:52'),
(26, 1, 12, 'Camila Alves', 'camila-alves', 'FFF', '20150819143144Camila-alves_9.jpg', 'camila-rondomi.jpg', '', 'release_camila-alves.doc', 1, '2015-08-19 17:29:07', '2017-01-19 18:43:35'),
(29, 2, 24, 'Mel Lisboa', 'mel-lisboa', 'FFF', '20170724200557home-valendo-mel.jpg', '20170724200558homing-valendo-mel.jpg', '', 'release_mel-lisboa.docx', 1, '2015-08-19 18:11:55', '2017-07-24 23:05:58'),
(30, 1, 12, 'Carol Scaff', 'carol-scaff', '000', '20170530170553valendo-final.jpg', '20151009120609scaff-tratada-.jpg', 'http://carolinascaff.blogspot.com.br/', 'release_carol-scaff.doc', 1, '2015-08-19 18:16:09', '2017-05-30 20:05:54'),
(31, 1, 12, 'Barbara Gancia', 'barbara-gancia-1', 'FFF', 'barbara-home.jpg', 'untitled-3.jpg', '', 'release_barbara-gancia-1.docx', 1, '2015-08-19 18:19:56', '2017-01-27 22:52:05'),
(32, 1, 12, 'Carla Lamarca', 'carla-lamarca', 'FFF', '20151009135751carla-Lamarca-tratada-.jpg', '20151009135751carla-Lamarca-tratada-.jpg', 'https://carlalamarca.wordpress.com/			', 'release_carla-lamarca.doc', 1, '2015-08-19 18:25:20', '2016-12-16 17:32:52'),
(33, 1, 12, 'Chiara Gadaleta', 'chiara-gadaleta-1', 'FFF', 'chiara-2-home.jpg', '', 'http://www.ecoera.com.br', 'release_chiara-gadaleta.pdf', 1, '2015-08-19 18:30:47', '2017-01-27 18:50:19'),
(34, 1, 12, 'Costanza Pascolato', 'costanza-pascolato', 'FFF', '2015101410005120150928160818constanza-pascolato-veio-rocknroll-sao-paulo-fashion-week-em-janeiro-e-fevereiro-de-2011.jpg', '', '', 'release_costanza-pascolato-1.docx', 1, '2015-08-19 18:36:26', '2017-01-27 19:02:40'),
(35, 1, 12, 'Daniella Cicarelli', 'daniella-cicarelli', '000', 'daniella-cicarelli-tratada.jpg', 'cica-romio.jpg', '', 'release_daniella-cicarelli.docx', 1, '2015-08-19 18:53:26', '2017-01-27 23:00:00'),
(38, 1, 12, 'Ligia Mendes ', 'ligia-mendes-1', 'FFF', 'ligia-home.jpg', 'ligia-home.jpg', '', 'release_ligia-mendes-1.doc', 1, '2015-08-20 17:40:08', '2016-12-16 17:32:52'),
(39, 1, 12, 'Luize Altenhofen', 'luize-altenhofen', 'FFF', '20151008154251Luize-Home-.jpg', 'lui.jpg', '', 'release_luize-altenhofen.doc', 1, '2015-08-20 17:45:28', '2017-01-27 21:27:16'),
(40, 1, 12, 'Marcelle Bittar', 'marcelle-bittar', 'FFF', '20151009161857Bittar-home-.jpg', 'bittsr.jpg', '', 'release_marcelle-bittar.doc', 1, '2015-08-20 17:49:19', '2017-01-27 21:32:33'),
(42, 1, 12, 'Marilia Moreno', 'marilia-moreno', 'FFF', 'marilia-home.jpg', 'marillia-ronmdo.jpg', 'http://www.mariliamoreno.com/about.html', 'release_marilia-moreno.doc', 1, '2015-08-20 18:01:12', '2017-01-27 21:40:34'),
(44, 1, 12, 'Silvia Poppovic', 'silvia-poppovic-1', '000', '20150924164529Cópia-de-Silvia-Poppovic.jpg', 'popovick.jpg', '', 'release_silvia-poppovic-1-2-3.doc', 1, '2015-08-20 18:21:38', '2017-01-27 22:38:07'),
(45, 1, 12, 'Renata Fan', 'renata-fan', 'FFF', '20150929135115Cópia-de-Screenshot_2013-05-16-15-17-40.jpg', 'fan-rod.jpg', '', 'release_renata-fan.pdf', 1, '2015-08-20 18:30:28', '2017-01-27 22:06:00'),
(47, 1, 12, 'Tatiana Dumenti', 'tatiana-dumenti', 'FFF', '20151015185751tatiana-home-.jpg', '20151015185751tatiana-home-.jpg', 'http://www.tatianadumenti.com.br', 'release_tatiana-dumenti.doc', 1, '2015-08-20 18:44:11', '2016-12-16 17:32:52'),
(49, 1, 12, 'André Vasco', 'andre-vasco', 'FFF', 'home-anre-vasco-final-1.jpg', 'vasco-rondomi.jpg', 'http://www.andrevasco.com/', 'release_andre-vasco.docx', 1, '2015-08-20 18:56:54', '2017-05-12 18:17:24'),
(51, 1, 12, 'Pedro Andrade', 'pedro-andrade-1', 'FFF', '20151008162124Pedro-Home-.jpg', '20151008162124Pedro-Home-.jpg', '', 'release_pedro-andrade-1.doc', 0, '2015-08-20 19:05:10', '2016-12-16 17:32:52'),
(52, 3, NULL, 'Guto Requena', 'guto-requena-1', '000', '20151019163933guto-2-valendo-.jpg', 'guto-rodo.jpg', '', 'release_guto-requena.pdf', 1, '2015-08-20 19:11:47', '2017-01-27 23:02:37'),
(53, 4, 17, 'Dudu Bertholini', 'dudu-bertholini-1', 'FFF', '20151006190133Dudu-Estilita-.jpg', 'dudu-rondomi.jpg', '', 'release_dudu-bertholini-1.doc', 1, '2015-08-21 12:01:52', '2017-01-27 23:01:16'),
(54, 4, 17, 'Gloria Coelho', 'gloria-coelho', '000', '20150821090445Glória-Coelho.jpg', '20150821090445Glória-Coelho.jpg', 'http://www.gloriacoelho.com.br', 'release_gloria-coelho.docx', 1, '2015-08-21 12:04:48', '2016-12-16 17:32:52'),
(55, 4, 19, 'Lilian Pacce', 'lilian-pacce-1-2', 'FFF', 'img-1500.JPG', 'corte-lilian-pacce.jpg', 'http://www.lilianpacce.com.br', 'release_lilian-pacce-1-2.doc', 1, '2015-08-21 12:08:47', '2017-04-04 17:29:30'),
(56, 4, 17, 'Pedro Lourenço', 'pedro-lourenco', 'FFF', '20151019171315pedro-lourenco-valendo-.jpg', '20151019171315pedro-lourenco-valendo-.jpg', 'http://www.pedrolourenco.com', 'release_pedro-lourenco.doc', 1, '2015-08-21 12:16:31', '2016-12-16 17:32:52'),
(57, 8, NULL, 'Paulo Borges', 'paulo-borges', 'FFF', '20151019190118paulo-borges-.jpg', '20151019190118paulo-borges-.jpg', '', 'release_paulo-borges.doc', 1, '2015-08-21 12:19:56', '2016-12-16 17:32:52'),
(58, 10, 20, 'Rodrigo Oliveira', 'rodrigo-oliveira', '000', 'rodrigo-palestrante.jpg', 'oliveira.jpg', '', 'release_rodrigo-oliveira.doc', 0, '2015-08-21 12:23:43', '2017-07-20 14:20:45'),
(60, 10, 22, 'Márcio Silva', 'marcio-silva-1-2', 'FFF', '20151014193425marcio-silva.jpg', '', '', 'release_marcio-silva-1-2.doc', 0, '2015-08-21 12:30:26', '2017-05-17 18:51:50'),
(61, 10, 21, 'Josimar Melo', 'josimar-melo', 'FFF', '20151023110950foto-home-josimar-mello.jpg', '20151023110950foto-home-josimar-mello.jpg', 'http://josimarmelo.blog.uol.com.br', 'release_josimar-mello-2.doc', 1, '2015-08-21 12:32:44', '2016-12-16 17:32:52'),
(63, 5, NULL, 'Erika Palomino', 'erika-palomino-1', 'FFF', '20150821093711ERIKA-PALOMINO.jpg', '', '', 'release_erika-palomino-1.doc', 1, '2015-08-21 12:37:13', '2017-01-27 19:29:19'),
(64, 5, NULL, 'Felipe Suhre', 'felipe-suhre', 'FFF', '20150922115532-Felipe-Suhre-site-.jpg', 'romi-valendo-shure.jpg', '', 'release_felipe-shure.pdf', 1, '2015-08-21 12:43:31', '2017-01-27 23:21:56'),
(65, 5, NULL, 'Guga Chacra', 'guga-chacra', 'FFF', '20151014194158guga-chacra.jpg', 'guga-romi.jpg', 'http://internacional.estadao.com.br/blogs/gustavo-chacra', 'release_guga-chacra.docx', 1, '2015-08-21 12:49:07', '2017-01-27 23:11:20'),
(66, 5, NULL, 'Mona Dorf', 'mona-dorf', '000', '20151014194337mona-dorf-texte-.jpg', '20151014194337mona-dorf-texte-.jpg', 'https://monadorf.wordpress.com/o-livro/', 'release_mona-dorf.doc', 1, '2015-08-21 13:03:49', '2016-12-16 17:32:52'),
(67, 5, NULL, 'Monica Waldvogel', 'monica-waldvogel-1', '000', '20151020004252monica-w-palestratne-.jpg', '20151020004252monica-w-palestratne-.jpg', '', 'release_monica-waldvogel-1.doc', 1, '2015-08-21 13:07:28', '2016-12-16 17:32:52'),
(68, 5, NULL, 'Rodrigo Bocardi', 'rodrigo-bocardi', '000', 'unknown.jpeg', 'bocardi.jpg', '', 'release_rodrigo-bocardi.doc', 1, '2015-08-21 13:10:55', '2017-01-27 22:14:14'),
(70, 7, NULL, 'Vanessa Rozan', 'vanessa-rozan', '000', 'img-1368.JPG', 'img-1368.JPG', 'http://liceudemaquiagem.com.br', 'release_vanessa-rozan.doc', 1, '2015-08-21 13:21:20', '2016-12-16 17:32:52'),
(71, 7, NULL, 'Duda Molinos', 'duda-molinos-1', 'FFF', '20151014193552duda-molinos.jpg', '20151014193552duda-molinos.jpg', 'http://www.labdudamolinos.com.br', 'release_duda-molinos-1.docx', 0, '2015-08-21 13:22:24', '2016-12-16 17:32:52'),
(72, 3, NULL, 'Marcelo Rosenbaum', 'marcelo-rosenbaum-1', '000', '20150921135542Marcelo.-Tratada-Pb-menor--.jpg', '20150921135542Marcelo.-Tratada-Pb-menor--.jpg', 'http://www.rosenbaum.com.br', 'release_marcelo-rosembaum-1.doc', 0, '2015-08-21 13:32:51', '2017-05-26 13:49:45'),
(73, 4, 17, 'Marcelo Sommer ', 'marcelo-sommer', 'FFF', '20151014192548marcelo-sommer.jpg', '20151014192548marcelo-sommer.jpg', 'http://www.marcelosommer.com.br/', 'release_marcelo-sommer.doc', 1, '2015-08-21 13:35:20', '2016-12-16 17:32:52'),
(75, 1, 14, 'Barbara Gancia', 'barbara-gancia', 'FFF', 'img-1371.JPG', '', 'http://bandnewsfm.band.uol.com.br/Noticia.aspx?COD=757949&Tipo=227', 'release_barbara-gancia.doc', 0, '2015-08-21 15:41:32', '2017-05-17 18:47:46'),
(76, 1, 14, 'Ligia Mendes', 'ligia-mendes', '000', 'ligia-radio.jpg', '', 'http://jovempanfm.bol.uol.com.br/podcasts/missao-impossivel/', 'release_ligia-mendes.doc', 1, '2015-08-21 15:47:03', '2017-01-27 21:08:43'),
(77, 4, 19, 'Chiara Gadaleta', 'chiara-gadaleta-1-2', 'FFF', 'chiara-2-home.jpg', '', 'http://www.ecoera.com.br', 'release_chiara-gadaleta-1-2.docx', 1, '2015-08-21 15:50:05', '2017-07-12 20:16:07'),
(78, 4, 19, 'Regina Guerreiro', 'regina-guerreiro', '000', '20150821125640Regina-Guerreiro.jpg', 'guerreiro.jpg', 'http://regina.guerreiro.blog.uol.com.br', 'release_regina-guerreiro-2.pdf', 1, '2015-08-21 15:56:40', '2017-01-27 21:57:58'),
(79, 10, 20, 'Márcio Silva', 'marcio-silva-1', '000', '20151014183832marcio-home-.jpg', '20151014183832marcio-home-.jpg', '', 'release_marcio-silva-1.doc', 0, '2015-08-21 15:58:32', '2017-05-17 18:49:24'),
(82, 5, NULL, 'Barbara Gancia', 'barbara-gancia-1-2-3', 'FFF', '20151020003447img-2345.jpg', '20151020003447img-2345.jpg', 'http://blogdabarbara.folha.blog.uol.com.br/', 'release_barbara-gancia-3.docx', 1, '2015-08-21 16:08:47', '2016-12-16 17:32:52'),
(84, 5, NULL, 'Silvia Poppovic', 'silvia-poppovic', '000', '20151020005146popovick-palestratne-.jpg', '', '', 'release_silvia-poppovic.doc', 1, '2015-08-21 16:15:45', '2017-01-27 22:34:16'),
(86, 8, NULL, 'Gloria Coelho', 'gloria-coelho-1', '000', '20151014190606gloria-coelho.jpg', '', 'http://www.gloriacoelho.com.br', 'release_gloria-coelho-2.docx', 1, '2015-08-21 16:23:46', '2017-01-27 19:39:30'),
(87, 8, NULL, 'Pedro Lourenço', 'pedro-lourenco-1', '000', '20151014191658pedro-lourenco-valendo-.jpg', '', 'http://www.pedrolourenco.com', 'release_pedro-lourenco-1.doc', 1, '2015-08-21 16:27:21', '2017-01-27 21:55:06'),
(88, 8, NULL, 'Marcelo Rosenbaum ', 'marcelo-rosenbaum', '000', '20151014191213rosembau-valendo-.jpg', '', 'http://www.rosenbaum.com.br', 'release_marcelo-rosenbaum.doc', 0, '2015-08-21 16:29:30', '2017-05-26 13:50:03'),
(89, 8, NULL, 'Guto Requena', 'guto-requena', '000', '20151014190642guto-requena.jpg', '', 'http://www.gutorequena.com.br', 'release_guto-requena-2.pdf', 1, '2015-08-21 16:31:27', '2017-01-27 20:35:07'),
(90, 8, NULL, 'Costanza Pascolato', 'costanza-pascolato-1', '000', '20151014190038costanza-home-palestrante-.jpg', '', '', 'release_costanza-pascolato-3.docx', 1, '2015-08-21 16:37:16', '2017-01-27 18:57:06'),
(91, 8, NULL, 'Erika Palomino', 'erika-palomino', 'FFF', '20151014190431erika-palestrante-.jpg', '20151014190431erika-palestrante-.jpg', '', 'release_erika-palomino.doc', 1, '2015-08-21 16:41:34', '2016-12-16 17:32:52'),
(92, 8, NULL, 'Lilian Pacce', 'lilian-pacce', '000', '20151014191131lilian-palestrante-.jpg', '', 'http://www.lilianpacce.com.br', 'release_lilian-pacce.doc', 1, '2015-08-21 16:45:44', '2017-01-27 21:09:14'),
(93, 8, NULL, 'Regina Guerreiro', 'regina-guerreiro-1', 'FFF', '20151013175900Regina-Home-Palestrante-tratada.jpg', '', '', 'release_regina-guerreiro-3.pdf', 1, '2015-08-21 16:47:49', '2017-01-27 21:55:39'),
(95, 8, NULL, 'Vanessa Rozan', 'vanessa-rozan-1', 'FFF', '20151014192037vanessa-rozan.jpg', '', 'http://liceudemaquiagem.com.br', 'release_vanessa-rozan-1.doc', 1, '2015-08-21 16:53:24', '2017-01-27 23:13:36'),
(96, 8, NULL, 'Duda Molinos', 'duda-molinos', 'FFF', '20151014190305duda-molinos.jpg', '20151014190305duda-molinos.jpg', 'http://www.labdudamolinos.com.br', 'release_duda-molinos.docx', 0, '2015-08-21 16:56:21', '2016-12-16 17:32:52'),
(97, 8, NULL, 'Silvia Poppovic', 'silvia-poppovic-1-2', '000', '20151014191953popovick-palestratne-.jpg', '20151014191953popovick-palestratne-.jpg', '', 'release_silvia-poppovic-1-2.doc', 1, '2015-08-21 16:58:49', '2016-12-16 17:32:52'),
(98, 8, NULL, 'Monica Waldvogel', 'monica-waldvogel', '000', '20151014191613monica-w-palestratne-.jpg', '', '', 'release_monica-waldvogel-2.pdf', 1, '2015-08-21 17:02:55', '2017-01-27 21:45:48'),
(99, 5, NULL, 'Josimar Melo', 'josimar-melo-1', 'FFF', '20151023110923foto-home-josimar-mello.jpg', '', 'http://josimarmelo.blog.uol.com.br', 'release_josimar-mello.doc', 1, '2015-08-21 17:10:41', '2017-01-27 21:06:29'),
(100, 1, 12, 'Luitha ', 'luitha', 'FFF', 'foto-home-luitha-nova-fechada.jpg', '20150921174344ALuitha-Miraglia-3333.jpg', '', 'release_luitha.doc', 1, '2015-09-21 20:43:44', '2016-12-21 13:20:33'),
(101, 2, 24, 'Tuti Muller', 'tuti-muller', '000', '20150929153211Trada-valendo-.jpg', '20150929153211Trada-valendo-.jpg', '', 'release_tuti-muller.doc', 1, '2015-09-23 20:57:05', '2017-05-26 23:05:38'),
(102, 1, 12, 'Lilian Pacce', 'lilian-pacce-1', 'FFF', '20150923182521lil.jpg', '', 'http://www.lilianpacce.com.br', 'release_lilian-pacce-1.doc', 1, '2015-09-23 21:25:21', '2017-01-27 21:09:38'),
(103, 1, 12, 'Amanda Ramalho', 'amanda-ramalho-1', 'FFF', '20150928181625Cópia-de-IMG_0627.jpg', '', '', 'release_amanda-ramalho-1.doc', 0, '2015-09-24 16:53:09', '2017-02-16 19:44:15'),
(104, 8, NULL, 'Barbara Gancia', 'barbara-gancia-1-2', 'FFF', '20151013180031barbaraGancia.jpg', '', '', 'release_barbara-gancia-5.docx', 1, '2015-10-06 17:29:06', '2017-01-27 18:52:41'),
(105, 8, NULL, 'Marcelo Sommer', 'marcelo-sommer-1', '000', '20151014191310sommer-palestratne-01.jpg', '', '', 'release_marcelo-sommer-1.doc', 1, '2015-10-06 19:09:54', '2017-01-27 21:33:36'),
(106, 8, NULL, 'Chiara Gadaleta ', 'chiara-gadaleta', '000', '20151014185955chiara-g-palestrante-.jpg', '', 'http://www.ecoera.com.br', 'release_chiara-gadaleta.docx', 1, '2015-10-06 19:27:03', '2017-07-12 20:18:31'),
(107, 8, NULL, 'Dudu Bertholini', 'dudu-bertholini', 'FFF', '20151014190347dudu-bertholini.jpg', '', '', 'release_dudu-bertholini.doc', 1, '2015-10-06 20:02:39', '2017-01-27 19:15:01'),
(108, 8, NULL, 'Josimar Melo ', 'josimar-melo-1-2', 'FFF', '20151014190908josmar-mello.jpg', '', 'http://josimarmelo.blog.uol.com.br', 'release_josimar-melo-1-2.doc', 1, '2015-10-06 20:26:12', '2017-04-27 19:34:03'),
(109, 8, NULL, 'Rodrigo Oliveira', 'rodrigo-oliveira-1', 'FFF', '20151014191909rodrigo-palestrante-.jpg', '', '', 'release_rodrigo-oliveira-1.doc', 0, '2015-10-06 20:36:35', '2017-07-20 14:21:17'),
(111, 8, NULL, 'Marcio Silva', 'marcio-silva', 'FFF', '20151014191456marcio-palestrante-2jpg.jpg', '', '', 'release_marcio-silva.doc', 0, '2015-10-06 21:18:04', '2017-05-17 18:52:48'),
(112, 4, 19, 'Erika Palomino', 'erika-palomino-1-2', 'FFF', '20151019180120erika-palestrante-2-.jpg', '', '', 'release_erika-palomino-1-2.doc', 1, '2015-10-09 19:16:20', '2017-01-27 19:30:03'),
(124, 5, NULL, 'Luciana Liviero', 'luciana-liviero', 'FFF', '20151021125615luciana-livieiro-.jpg', '20151021125615luciana-livieiro-.jpg', 'http://luliviero.wix.com/lucianaliviero#!contato/c24vq', 'release_luciana-liviero.docx', 0, '2015-10-21 14:30:15', '2016-12-16 17:32:52'),
(125, 1, 14, 'Silvia Popovic', 'silvia-popovic', 'FFF', 'image001.jpg', 'image001.jpg', '', 'release_silvia-popovic.doc', 0, '2015-10-28 19:56:27', '2017-05-17 18:55:10'),
(126, 1, 12, 'Edgard Piccoli', 'edgard-piccoli', 'FFF', 'edgar-home.jpg', 'edgar-rondoni.jpg', '', 'release_edgard-piccoli.doc', 1, '2015-10-29 00:22:47', '2017-01-27 23:04:02'),
(128, 8, NULL, 'Mara Luquet ', 'mara-luquet', 'FFF', 'palestrantes.jpg', 'palestrantes.jpg', 'http://www.letraselucros.com', 'release_mara-luquet.docx', 1, '2015-11-13 22:17:35', '2016-12-16 17:32:52'),
(129, 2, 24, 'Lucia Bronstein', 'lucia-bronstein', '000', 'lucia-bronstein-tratada.jpg', 'lucia.jpg', '', 'release_lucia-bronstein.doc', 1, '2015-11-16 14:57:17', '2017-02-20 03:13:01'),
(130, 11, NULL, 'Produçao Executiva ', 'producao-executiva', '000', 'p-e-valendo.jpg', 'p-e-valendo.jpg', '', '', 1, '2015-11-23 16:57:58', '2016-12-16 17:32:52'),
(131, 9, NULL, 'André  Schiliró', 'andre-schiliro', '000', 'andre-sh-valendo.jpg', 'andre-sh-valendo.jpg', 'http://www.schiliro.com.br ', 'release_andre-schiriliro.docx', 0, '2015-11-23 17:58:59', '2016-12-16 17:32:52'),
(132, 2, 23, 'Sérgio Menezes', 'sergio-menezes', '000', 'sergio-menezes-home.jpg', 'sergio-menezes-home.jpg', '', 'release_sergio-menezes.doc', 1, '2015-12-09 12:19:03', '2017-03-08 11:01:06'),
(134, 5, NULL, 'Mara Luquet ', 'mara-luquet-1', '000', 'mara-home-pb.jpg', '', 'http://www.letraselucros.com', 'release_mara-luquet-1.docx', 1, '2016-01-18 18:18:26', '2017-01-27 21:27:34'),
(135, 4, 19, 'Costanza Pascolato', 'costanza-pascolato-1-2', 'FFF', 'capa.jpg', 'costnza-rondomi.jpg', '', 'release_costanza-pascolato-2.docx', 1, '2016-01-19 16:35:11', '2017-01-27 22:59:23'),
(136, 1, 12, 'Marilu Beer', 'marilu-beer', '000', 'marilu.jpg', 'marilu.jpg', '', 'release_marilu-beer.doc', 0, '2016-01-28 19:05:34', '2016-12-16 17:32:52'),
(137, 3, NULL, 'Marilu Beer', 'marilu-beer-1', '000', 'marilu.jpg', 'marilu.jpg', '', 'release_marilu-beer-1.doc', 1, '2016-01-28 19:16:41', '2017-01-27 21:35:23'),
(138, 1, 12, 'Celso Zucatelli', 'celso-zucatelli', '000', 'zuca-home-valendo.jpg', 'zuca-home-valendo.jpg', 'http://www.celsozucatelli.com.br/', 'release_celso-zucatelli.docx', 1, '2016-02-16 15:39:43', '2016-12-16 17:32:52'),
(139, 3, NULL, 'Vic Meirelles', 'vic-meirelles', 'FFF', 'vic-valendo.jpg', 'vic-valendo.jpg', 'http://vicmeirelles.com.br/', 'release_vic-meirelles.doc', 1, '2016-03-28 19:47:09', '2016-12-16 17:32:52'),
(140, 5, NULL, 'Leão Serva ', 'leao-serva', 'FFF', 'leao-serva-home-tratada.jpg', '', '', 'release_leao-serva.docx', 1, '2016-04-07 23:37:04', '2017-01-27 21:07:56'),
(141, 8, NULL, 'Leão Serva ', 'leao-serva-1', 'FFF', 'leao-serva-home-tratada-palestrante.jpg', 'leao-serva-home-tratada-palestrante.jpg', '', 'release_leao-serva-1.docx', 1, '2016-04-07 23:56:06', '2016-12-16 17:32:52'),
(142, 10, 20, 'André Falcão ', 'andre-falcao', 'FFF', 'andre-falcao-tratada-homne.jpg', 'andre-falcao-tratada-homne.jpg', '', 'release_andre-falcao.docx', 1, '2016-04-26 16:09:32', '2016-12-16 17:32:52'),
(143, 5, NULL, 'Patricia Campos Mello ', 'patricia-campos-mello', 'FFF', 'patricia-campos-mello-home.jpg', 'patricia-campos-mello-home.jpg', '', 'release_patricia-campos-mello.doc', 1, '2016-05-04 18:53:05', '2016-12-16 17:32:52'),
(144, 5, NULL, 'Danilo Vieira  ', 'danilo-vieira', '000', 'danilo-vieira-home.jpg', 'danilo-vieira-home.jpg', '', 'release_danilo-vieira.docx', 1, '2016-05-04 19:01:54', '2016-12-16 17:32:52'),
(145, 5, NULL, 'Lilian Pacce ', 'lilian-pacce-1-2-3', 'FFF', 'foto-home-moda.jpg', '', 'http://www.lilianpacce.com.br/', 'release_lilian-pacce-1-2-3.doc', 1, '2016-05-11 12:48:38', '2017-01-27 21:10:30'),
(146, 5, NULL, 'Celso Zucatelli', 'celso-zucatelli-1', '000', 'zuca-home-valendo.jpg', '', 'http://www.celsozucatelli.com.br/', 'release_celso-zucatelli-1.doc', 1, '2016-05-12 18:06:18', '2017-01-27 18:53:38'),
(147, 4, 19, 'Bia Paes de Barros', 'bia-paes-de-barros', '000', 'valendo-home.jpg', 'bia-paes.jpg', '', 'release_bia-paes-de-barros.docx', 0, '2016-05-23 19:52:07', '2017-07-25 13:43:56'),
(148, 6, NULL, 'Consuelo Blocker ', 'consuelo-blocker', 'FFF', 'foto-da-home-consuleo-blocker.jpg', 'foto-da-home-consuleo-blocker.jpg', 'http://WWW.CONSUELOBLOG.COM', 'release_consuelo-blocker.doc', 1, '2016-05-23 21:03:36', '2016-12-16 17:32:52'),
(149, 2, 23, 'Saulo Meneghetti', 'saulo-meneghetti', 'FFF', 'fullsizerender.jpg', 'untitled-2.jpg', '', 'release_saulo-meneghetti.doc', 1, '2016-05-23 23:26:06', '2017-01-27 22:33:28'),
(150, 1, 12, 'Pedro Bosnich', 'pedro-bosnich', 'FFF', 'home-pedro.jpg', 'bosnchi.jpg', '', 'release_pedro-bosnich.doc', 1, '2016-05-24 01:08:51', '2017-04-06 15:24:05'),
(151, 2, 23, 'Gustavo  Haddad ', 'gustavo-haddad', 'FFF', 'home-valendo.jpg', 'gustavo.jpg', '', 'release_gustavo-haddad.doc', 1, '2016-06-13 16:05:58', '2017-01-27 21:15:27'),
(152, 1, 12, 'Penélope  Nova ', 'penelope-nova', '000', 'penelope.jpg', 'penelope.jpg', 'http://www.acreditabonita.com.br', '', 0, '2016-06-20 19:37:40', '2016-12-23 03:25:59'),
(153, 2, 23, 'Fabio Ventura ', 'fabio-ventura', '000', 'fabio-ventura-home.jpg', 'fabio-ventura-home.jpg', '', 'release_fabio-ventura-construcao.docx', 1, '2016-06-29 14:50:01', '2016-12-16 17:32:52'),
(154, 1, 12, 'Ronald Rios ', 'ronald-rios', '000', 'ronald-home.jpg', 'ronald.jpg', '', '', 1, '2016-06-29 16:53:43', '2017-01-27 22:25:51'),
(155, 5, NULL, 'Izabella Camargo ', 'izabella-camargo', '000', 'izabella-valendo.jpg', 'unknown-5.jpeg', '', 'release_izabella-camargo.docx', 1, '2016-07-19 15:56:29', '2017-01-30 13:12:34'),
(156, 2, 24, 'Sabrina Korgut  ', 'sabrina-korgut', '000', 'home-sabrina.jpg', 'sabrina-k.jpg', '', '', 1, '2016-07-25 14:42:58', '2017-01-27 22:28:43'),
(157, 6, NULL, 'Erick Krominski ', 'erick-krominski', '000', 'erick-01.jpeg', 'erick-rodoni.jpg', '', 'release_erick-krominski.docx', 1, '2016-07-27 14:21:21', '2017-01-27 23:04:28'),
(158, 1, 12, 'Erick Krominski ', 'erick-krominski-1', 'FFF', 'erick-01.jpeg', '', '', 'release_erick-krominski-1.docx', 1, '2016-08-10 15:33:36', '2017-01-27 19:28:31'),
(159, 2, 23, 'André Ramiro ', 'andre-ramiro', '000', 'andrea-ramiro-home.jpg', 'ramiro-rondomi.jpg', '', 'release_andre-ramiro.docx', 1, '2016-09-13 15:43:05', '2017-01-19 18:00:42'),
(160, 2, 24, 'Bruna Miglioranza', 'bruna-miglioranza', '000', 'bruna-home-2.jpg', 'bruna.jpg', '', 'release_bruna-miglioranza.docx', 1, '2016-09-13 16:12:36', '2017-02-20 04:03:24'),
(161, 2, 23, 'Vandré Silveira', 'vandre-silveira', '000', 'home-vandre-valendo-2.jpg', 'wnadre.jpg', '', 'release_vandre-silveira.docx', 1, '2016-09-13 16:28:07', '2017-01-27 22:43:06'),
(165, 5, NULL, 'Sergio Aguiar ', 'sergio-aguiar', 'FFF', 'sergio-aguar.jpg', 'sergio-aguar.jpg', '', 'release_sergio-aguiar.doc', 0, '2016-09-21 16:48:07', '2017-01-16 14:49:39'),
(167, 2, 23, 'César Mello', 'cesar-mello', '000', 'cesar-mello-home-valendo.jpg', 'rodon-cesar-mello.jpg', '', 'release_cesar-mello.docx', 1, '2016-10-04 17:56:44', '2017-01-27 18:42:56'),
(170, 7, NULL, 'Omar Bergea', 'omar-bergea', 'FFF', 'omar-valendo.jpg', 'omar-valendo.jpg', '', '', 1, '2016-10-04 21:01:31', '2016-12-16 17:32:52'),
(171, 6, NULL, 'Jana Rosa ', 'jana-rosa', 'FFF', 'home-jana.jpg', 'jana-home.jpg', 'http://www.janarosa.com.br', '', 0, '2016-10-06 19:18:16', '2017-03-30 14:43:38'),
(172, 2, 23, 'Jeronimo Martins ', 'jeronimo-martins', '000', '20170531162615home-valendo.jpg', '', '', 'release_jeronimo-martins.docx', 1, '2016-10-06 20:03:42', '2017-05-31 19:26:16'),
(173, 5, NULL, 'Jamil Chade ', 'jamil-chade', '000', 'jamil-home-01.jpg', 'jamil.jpg', 'http://internacional.estadao.com.br/blogs/jamil-chade/', 'release_jamil-chade.doc', 1, '2016-10-18 22:16:29', '2017-01-27 20:43:07'),
(174, 2, 23, 'Leonardo Medeiros ', 'leonardo-medeiros', 'FFF', 'leo-dias-home.jpg', 'leo-dias-home.jpg', '', '', 0, '2016-10-20 16:08:03', '2016-12-16 17:32:52'),
(175, 8, NULL, 'Sergio Zobaran', 'sergio-zobaran', 'FFF', 'sergio-home.jpg', 'sergio-home.jpg', '', 'release_sergio-zobaran.docx', 0, '2016-12-06 21:39:11', '2016-12-16 17:32:52'),
(176, 7, NULL, 'Fabiana Gomes ', 'fabiana-gomes', '000', 'home-fabiana.jpg', 'fabi-rondo.jpg', '', 'release_fabiana-gomes.docx', 1, '2016-12-06 22:24:15', '2017-03-21 22:37:18'),
(177, 5, NULL, 'Claudio Tognolli ', 'claudio-tognolli', '000', 'claudio-tpgnolio.jpg', 'toginolli.jpg', '', 'release_claudio-tognolli.docx', 1, '2016-12-15 22:16:38', '2017-01-27 23:12:09'),
(178, 2, 23, 'Alexandre Schumacher ', 'alexandre-schumacher', '000', 'home-shumaquer.jpg', 'randomico-shumaker.jpg', '', 'release_alexandre-schumacher.docx', 1, '2016-12-20 10:51:45', '2017-02-17 22:44:38'),
(179, 2, 24, 'Veronica Valois ', 'veronica-valois', 'FFF', 'home-valendo-veronica.jpg', 'veronica-romming.jpg', '', 'release_veronica-valois.docx', 1, '2016-12-26 14:13:19', '2017-02-20 03:53:13'),
(181, 2, 23, 'Thiago Amaral ', 'thiago-amaral', '000', 'thiago-amaral.jpg', 'rodanei.jpg', '', 'release_thiago-amaral.docx', 1, '2017-02-02 18:19:19', '2017-02-13 14:07:47'),
(182, 2, 24, 'Marjorie Gerardi', 'marjorie-gerardi', '000', '20170613145411img-3348-marjorie.jpg', 'untitled-1.jpg', 'https://www.marjoriegerardi.com', 'release_marjorie-gerardi.docx', 1, '2017-02-02 18:39:28', '2017-06-19 15:29:31'),
(183, 2, 24, 'Ana Barroso ', 'ana-barroso', '000', '20170530110712home.jpg', 'ana.jpg', '', 'release_ana-barroso.doc', 1, '2017-02-04 14:20:08', '2017-05-30 14:07:40'),
(185, 2, 24, 'Renata Vilella ', 'renata-vilella', '000', 'home-renata.jpg', 'renata-vile.jpg', '', 'release_renata-vilela.docx', 1, '2017-02-07 22:02:46', '2017-05-24 19:43:32'),
(186, 6, NULL, 'Bethy Lagardère', 'bethy-lagardere', 'FFF', 'bethy-home.jpg', 'roding.jpg', '', 'release_bethy-lagardere.docx', 1, '2017-02-14 00:49:30', '2017-04-10 18:13:44'),
(187, 1, 12, 'Adriana Couto ', 'adriana-couto', 'FFF', 'home-couto-02.jpg', 'adriana-couto.jpg', '', '', 1, '2017-02-17 21:01:16', '2017-02-17 21:57:46'),
(188, 4, 17, 'André Lima ', 'andre-lima', '000', 'andre-lima.jpg', 'andre-lima-roding.jpg', '', 'release_andre-lima.docx', 1, '2017-02-22 19:42:16', '2017-04-24 03:52:58'),
(189, 2, 24, 'Belize Pombal ', 'belize-pombal', 'FFF', 'belise-home-01.jpg', 'belise-roding.jpg', '', 'release_belize-pombal.docx', 1, '2017-03-09 23:35:28', '2017-04-04 17:30:24'),
(190, 12, NULL, 'Walcyr Carrasco ', 'walcyr-carrasco', '000', 'wlacyr-valaendo.jpg', 'walacyr-rodon.jpg', 'http://walcyrcarrasco.com.br', 'release_walcyr-carrasco.docx', 1, '2017-03-19 01:09:32', '2017-03-31 15:45:04'),
(191, 2, 24, 'Cris Carniato', 'cris-carniato', '000', 'home-cris.jpg', 'cris-carminato.jpg', '', 'release_cris-carniato.docx', 1, '2017-03-28 20:22:12', '2017-03-29 18:27:48'),
(192, 2, 23, 'Vitor Placca ', 'vitor-placca', '000', 'vityor-plasca-home.jpg', 'rondomi-viotr.jpg', '', 'release_vitor-placca.docx', 1, '2017-04-04 17:55:09', '2017-04-24 21:01:58'),
(193, 2, 24, 'Maria Zilda Bethlem', 'maria-zilda-bethlem', 'FFF', 'maria-zilda-home-valendo.jpg', 'close.jpg', 'http://www2.uol.com.br/mariazilda/', 'release_maria-zilda-bethlem-1.docx', 0, '2017-04-11 19:38:08', '2017-07-05 23:11:29'),
(194, 2, 23, 'Josué  Galinari', 'josue-galinari', 'FFF', 'home-josue.jpg', 'home-josue.jpg', 'https://www.josuegalinari.com.br/', 'release_josue-galinari.docx', 1, '2017-04-13 21:38:35', '2017-07-11 17:57:13'),
(195, 13, NULL, 'Luciana Mello ', 'luciana-mello', 'FFF', 'home-lu-mello-valendo.jpg', 'rondomi.jpg', '', 'release_luciana-mello.docx', 1, '2017-04-18 19:54:19', '2017-06-30 20:42:10'),
(196, 2, 24, 'Carol Vidotti ', 'carol-vidotti', 'FFF', '20170622180800home-vidotti.jpg', '20170622180800roding.jpg', '', 'release_carol-vidotti.docx', 1, '2017-04-19 19:34:29', '2017-06-23 15:33:38'),
(197, 2, 24, 'Ana Carolina Godoy ', 'ana-carolina-godoy', 'FFF', '20170613163403godoy-home.jpg', '20170613164600godoy-rodondomi.jpg', '', 'release_ana-carolina-godoy.docx', 1, '2017-04-19 19:47:26', '2017-06-13 19:46:00'),
(198, 5, NULL, 'Adriana Araújo ', 'adriana-araujo', '000', '20170612121029home-valendo.jpg', '20170613150417adriana-rondomi.jpg', '', 'release_adriana-araujo.docx', 1, '2017-04-24 16:56:35', '2017-06-13 18:04:17'),
(199, 5, NULL, 'Gerson Camarotti', 'gerson-camarotti', 'FFF', 'home-gerson.jpg', 'gerson-rondomi.jpg', 'http://g1.globo.com/politica/blog/blog-do-camarotti/autor/gerson-camarotti/', 'release_gerson-camarotti.docx', 1, '2017-04-24 21:47:24', '2017-04-24 21:50:28'),
(201, 2, 23, 'André Dias ', 'andre-dias', 'FFF', '20170530104713home.jpg', 'andre-roding.jpg', 'https://andrediasoficial.wordpress.com/2017/05/10/nelson-xavier-descanse-em-paz/', 'release_andre-dias.docx', 1, '2017-05-18 20:21:15', '2017-05-30 13:47:13'),
(202, 2, 23, 'Pedro Bosnich', 'pedro-bosnich-1', 'FFF', 'pedro-home-ator.jpg', 'pedro-homing.jpg', '', '', 1, '2017-05-19 01:44:14', '2017-05-19 01:44:14'),
(203, 2, 23, 'Isaac Medeiros ', 'isaac-medeiros', '000', '20170623112933isaac-home.jpg', '20170623112933roldeing.jpg', '', 'release_isaac-medeiros.docx', 1, '2017-06-23 14:29:33', '2017-07-17 13:26:36'),
(204, 2, 23, 'Daniel Siwek', 'daniel-siwek', '000', '20170623113706daniel-home.jpg', '20170623113706daniel-roding.jpg', '', '', 1, '2017-06-23 14:37:06', '2017-06-23 14:37:06'),
(205, 2, 23, 'Ricardo Pepeliskof', 'ricardo-pepeliskof', 'FFF', '20170623123228roberto-pepiloskf.jpg', '20170623123229ricardo-pepe.jpg', '', '', 1, '2017-06-23 15:32:29', '2017-06-23 15:47:17'),
(206, 5, NULL, 'Aline Midlej', 'aline-midlej', '000', '20170718152828aline-m-home-copy.jpg', '2017071815282829062017-aline-midlej-por-marina-gross-089-copy.jpg', '', 'release_aline-midlej.docx', 1, '2017-06-23 16:30:56', '2017-07-18 18:50:33'),
(207, 13, NULL, 'Lobão', 'lobao', '000', '20170630173957unknown.jpeg', '20170630173958lobao-roding.jpg', 'http://lobao.com.br', 'release_lobao.docx', 1, '2017-06-30 20:39:58', '2017-07-19 18:49:57'),
(208, 5, NULL, 'Renata Ceribelli ', 'renata-ceribelli', 'FFF', '20170703144819retana-ceribelle.jpg', '20170703144820renata-ceribelli-rodomi.jpg', '', 'release_renata-ceribelli.docx', 1, '2017-07-03 17:48:20', '2017-07-04 14:15:21'),
(209, 5, NULL, 'Tiago Scheuer', 'tiago-scheuer', '000', '20170705003436thiago-shouer.jpg', '20170705003030thiago-sh-rodoning.jpg', '', 'release_tiago-scheuer.docx', 1, '2017-07-05 03:30:30', '2017-07-06 23:37:52'),
(210, 2, 23, 'Betto Marque ', 'betto-marque', '000', '20170712162239betto-marques-valendo.jpg', '20170712162240beto-rondomi.jpg', '', '', 1, '2017-07-12 19:22:40', '2017-07-24 19:27:42'),
(211, 2, 23, 'Patrick  Amstalden ', 'patrick-amstalden', 'FFF', '20170716213525patrick-w-valendo.jpg', '20170712171215patrick.jpg', '', '', 1, '2017-07-12 20:12:15', '2017-07-17 00:35:25'),
(212, 2, 24, 'Izabella Bicalho ', 'izabella-bicalho', 'FFF', '20170719145853izabella-home.jpg', '20170719145853roding-izabella.jpg', '', '', 1, '2017-07-19 17:58:53', '2017-07-19 17:58:53'),
(213, 2, 24, 'Janaina Afhonso', 'janaina-afhonso', 'FFF', '20170725163844home-janaina.jpg', '20170725162904janaina-roming.jpg', '', '', 1, '2017-07-24 19:44:39', '2017-07-25 19:38:45');

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_premios`
--

CREATE TABLE `portfolio_premios` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_personalidades_id` int(10) UNSIGNED NOT NULL,
  `titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portfolio_premios`
--

INSERT INTO `portfolio_premios` (`id`, `portfolio_personalidades_id`, `titulo`, `imagem`, `texto`, `ordem`, `created_at`, `updated_at`) VALUES
(315, 181, 'Prêmio Questão de Crítica 2013', '', 'Categoria Melhor Ator com o solo "Ficção" - Cia Hiato', 0, '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(316, 181, 'Prêmio Shell de Teatro 2013', '', 'Indicação na categoria Melhor Ator com o solo "Ficção" - Cia Hiato', 1, '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(317, 181, 'Prêmio Femsa de Teatro Infantil 2013', '', 'Indicação na categoria Melhor Ator pelo espetáculo "Experiência"', 2, '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(318, 181, 'Festival do Juri Popular 2012', '', 'Categoria Melhor Ator pelo curta metragem "Quem Matou Jorge?"  com direção de Fernando Frahia', 3, '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(319, 181, 'Prêmio Festival do Juri Popular 2012', '', 'Categoria Melhor Ator pelo curta metragem "Quem Matou Jorge?" com direção de Fernando Frahia', 4, '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(320, 181, 'Prêmio Funarte Interações Estéticas 2010', '', 'Projeto do Coletivo Urubus contemplado pela performance “entrExtremos”, realizada no Parque Nacional Serra da Capivara (PI) e em São Paulo (SP) com direção de Carol Pinzan', 5, '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(321, 181, 'Prêmio FENATA 2010', '', 'Categoria Melhor Interpretação Coletiva com o espetáculo "Cachorro Morto"', 6, '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(322, 181, 'Prêmio Mapa Cultural Paulista 2001', '', 'Categorias Melhor Ator e Ator Revelação pelo espetáculo “Uma Vendedora de Recursos” com direção de Paulo Felício e Tina Lopes', 7, '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(377, 178, 'Prêmios Shell, Qualidade Brasil e Contigo 2010', '', 'Indicação na categoria "Melhor Ator" pelo espetáculo" Vicente Celestino - A Voz Orgulho do Brasil"', 0, '2017-03-08 02:55:15', '2017-03-08 02:55:15'),
(378, 178, 'Prêmio Contigo 2006', '', 'Indicação na categoria "Ator Revelação" pelo personagem Caco da novela "Pé na Jaca" de Carlos Lombardi e direção de Ricardo Waddington - TV Globo', 1, '2017-03-08 02:55:15', '2017-03-08 02:55:15'),
(379, 178, 'Emmy 2006', '', 'Indicação do especial "Por toda a minha vida" sobre a vida de Elis Regina, com direção de Carlos Lombardi - TV Globo', 2, '2017-03-08 02:55:15', '2017-03-08 02:55:15'),
(380, 178, 'Prêmios Shell e Qualidade Brasil 2004', '', 'Indicação na categoria "Melhor Ator" pelo musical "Ópera do Malandro" de Charles Möeller e Claudio Botelho', 3, '2017-03-08 02:55:16', '2017-03-08 02:55:16'),
(381, 178, 'Prêmio Shell 1999', '', 'Indicação na categoria "Melhor Espetáculo" por "Os Cafajestes" com direção de Fernando Guerreiro', 4, '2017-03-08 02:55:16', '2017-03-08 02:55:16'),
(382, 178, 'Prêmio Shell 1997', '', 'Indicação na categoria "Melhor Ator" pelo espetáculo "Ventania" de José Vicente', 5, '2017-03-08 02:55:16', '2017-03-08 02:55:16'),
(383, 178, 'Prêmio APTESP 1995', '', 'Indicação na categoria "Melhor Ator" com o musical infantojuvenil "La Fontaine em Fábulas"', 6, '2017-03-08 02:55:16', '2017-03-08 02:55:16'),
(464, 159, 'Troféu Raça Negra da Afrobras (Federação das Religiões Afro-Brasileiras) 2010', '20161116181932trofeu-raca-negra-2010-melhor-ator-do-ano.jpg', 'Melhor Ator do Ano', 0, '2017-04-13 20:45:56', '2017-04-13 20:45:56'),
(465, 159, '3ª Edição do Prêmio Contigo de Cinema Nacional 2007 ', '20161116170513premio-de-cinema-revista-contigo-2007-eleito-melhor-ator-coadjuvante.jpg', 'Melhor Ator Coadjuvante eleito pelo voto popular e pelo juri oficial', 1, '2017-04-13 20:45:56', '2017-04-13 20:45:56'),
(466, 159, 'Troféu Raça Negra da Afrobras (Federação das Religiões Afro-Brasileiras) 2007', '20161116170800trofeu-raca-negra-melhor-ator-2010.jpg', 'Melhor Ator do Ano ', 2, '2017-04-13 20:45:56', '2017-04-13 20:45:56'),
(467, 167, 'Prêmio IX Festival de Teatro de Piedade 2005', '', 'Categoria Melhor Ator pelo Monólogo "Café com Torradas" ', 0, '2017-04-18 21:39:14', '2017-04-18 21:39:14'),
(468, 167, 'Prêmio Mapa Cultural Paulista 2004', '', 'Categoria Melhor Ator pelo espetáculo "Macário" com direção de Marcelo Airoldi', 1, '2017-04-18 21:39:14', '2017-04-18 21:39:14'),
(591, 161, 'Festival Home Theatre 2014', '', 'Prêmio de Melhor Ator pelo espetáculo "Farnese de Saudade"', 0, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(592, 161, '25º Prêmio Shell - 2013', '', 'Indicação na categoria Cenário (concebido pelo ator) pelo espetáculo "Farnese de Saudade"', 1, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(593, 161, 'Questão de Crítica 2013', '', 'Prêmio de Melhor Cenografia (concebida pelo ator) pelo espetáculo "Farnese de Saudade" ', 2, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(594, 161, '6º Festival de Cinema de Maringá - 2009', '', 'Prêmio de Melhor Ator pelo curta metragem "Bárbara"', 3, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(595, 161, 'II For Rainbow - Festival de Fortaleza de Cinema de Diversidade Sexual 2008', '', 'Prêmio de Melhor Ator  pelo curta metragem "Bárbara"', 4, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(596, 161, '8ª Edição do Festival Ibero Americano de Curta Metragens de Sergipe - 2008', '', 'Prêmio de Melhor Ator pelo Júri Popular pelo curta metragem "Bárbara"', 5, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(597, 161, 'Festival de Cinema de Juiz de Fora 2007', '', 'Prêmio Primeiro Plano de Melhor Ator pelo curta metragem "Bárbara"', 6, '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(600, 176, 'Prêmio Beauty Artist do Ano', '', 'Revista Glamour 2015', 0, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(601, 176, 'Melhor Maquiadora do Ano ', '', 'Revista Cabelo e Cia 2016 ', 1, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(617, 201, 'Prêmio APTR 2010', '20170523144545unknown.jpeg', 'Melhor ator coadjuvante de Musical pelo espetáculo "Era no Tempo do Rei" dirigido por João Fonseca', 0, '2017-06-23 15:02:22', '2017-06-23 15:02:22'),
(618, 190, 'Emmy Internacional ', '20170318221436walcyrcarrasco-1480507374.jpg', 'Verdades Secretas ', 0, '2017-06-23 15:05:26', '2017-06-23 15:05:26'),
(619, 190, '59º Premio  APCA ', '20170403174241premio-apca-2016-foto-miguel-arcanjo-prado.jpg', 'Melhor Novela - 2016 ', 1, '2017-06-23 15:05:26', '2017-06-23 15:05:26'),
(645, 183, 'Prêmio Zilka Salaberry 2016', '', 'Categoria Especial pela continuidade do trabalho da BB Companhia de Teatro com o espetáculo "Reconto dos Contos de Fadas"', 0, '2017-07-07 18:50:09', '2017-07-07 18:50:09'),
(646, 183, 'Prêmio CBTIJ 2016', '', 'Categoria Especial pela continuidade do trabalho da BB Companhia de Teatro com o espetáculo "Reconto dos Contos de Fadas"', 1, '2017-07-07 18:50:09', '2017-07-07 18:50:09'),
(647, 183, 'V Festival de Teatro de Resende  1998', '', 'Melhor Atriz pelo espetáculo "A História de Topetudo"', 2, '2017-07-07 18:50:09', '2017-07-07 18:50:09'),
(648, 183, 'Prêmio Coca-Cola de Teatro Jovem 1997', '', 'Melhor Atriz, Melhor Texto e Melhor Espetáculo do Ano por "A História de Topetudo"', 3, '2017-07-07 18:50:09', '2017-07-07 18:50:09'),
(649, 183, 'Prêmio Mambembe de Teatro 1996', '', 'Melhor Espetáculo Infantil e Indicação de Melhor Atriz e Melhor Texto pelo espetáculo "A História de Topetudo"', 4, '2017-07-07 18:50:09', '2017-07-07 18:50:09'),
(658, 52, 'Prix Versailles ', '20170720110044guto-premio.jpg', 'O Estudio Guto Requena foi premiado na categoria de Prêmios Especiais de Restaurantes Exteriores com The Dancing Pavilion, nossa Arquitetura Interativa projetada para as Olimpíadas 2016 no Rio. ', 0, '2017-07-20 14:01:07', '2017-07-20 14:01:07'),
(659, 52, 'TOP XXI Prêmio Design Brasil', '20160715183536guto-requena.jpg', 'Estúdio Guto Requena foi premiado com “Emotive Cartography” um bar interativo como o melhor design de ambiente na categoria: espaços públicos e comerciais', 1, '2017-07-20 14:01:07', '2017-07-20 14:01:07'),
(662, 207, 'Grammy 2007', '20170705173116lobao-grammy.jpg', 'O CD e DVD Acústico MTV, que lhe rendeu o Grammy de Melhor Disco de Rock. ', 0, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(688, 29, 'Kikito  2006', '', 'Melhor atriz  no longa "Sonhos e Desejos"', 0, '2017-07-24 23:49:29', '2017-07-24 23:49:29'),
(689, 29, 'Prêmio Qualidade Brasil', '', 'Melhor atriz de musical pelo espetaculo "Rita Lee Mora ao Lado"', 1, '2017-07-24 23:49:29', '2017-07-24 23:49:29'),
(690, 29, 'Prêmio Quem', '', 'Melhor atriz de teatro por "Rita Lee Mora ao Lado"', 2, '2017-07-24 23:49:29', '2017-07-24 23:49:29'),
(691, 29, 'Prêmio Qualidade Brasil', '', 'Melhor atriz de comédia no espetáculo  "Superadas"', 3, '2017-07-24 23:49:29', '2017-07-24 23:49:29'),
(692, 29, 'Prêmio Cidadão São Paulo ', '', 'Trabalho desenvolvido junto a Companhia Pessoal do Faroeste. ', 4, '2017-07-24 23:49:29', '2017-07-24 23:49:29'),
(693, 143, 'Troféu Mulher Imprensa 2016', '', 'categoria repórter de jornal', 0, '2017-07-25 14:18:09', '2017-07-25 14:18:09');

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_publicacoes`
--

CREATE TABLE `portfolio_publicacoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_personalidades_id` int(10) UNSIGNED NOT NULL,
  `titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `autor` text COLLATE utf8_unicode_ci NOT NULL,
  `editora` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portfolio_publicacoes`
--

INSERT INTO `portfolio_publicacoes` (`id`, `portfolio_personalidades_id`, `titulo`, `autor`, `editora`, `imagem`, `texto`, `ordem`, `created_at`, `updated_at`) VALUES
(2286, 51, 'O melhor guia de Nova York', 'Pedro Andrade', 'Rocco', '20150820160938Publicacoes_Pedro Andrade_1.png', 'Pedro Andrade reúne neste guia prático e sofisticado as melhores dicas de culinária, moda, cultura e show bizz da Big Apple.', 0, '2016-03-07 19:48:12', '2016-03-07 19:48:12'),
(2365, 97, 'Silvia Poppovic e você', 'Silvia Poppovic ', 'Mandarim', '20151201174603livro-silvia.jpg', 'O livro aborda temas como casamento, filhos, amor, sexo, trabalho e as pequenas dificuldades do dia-a-dia na visão da autora.', 0, '2016-03-17 19:59:33', '2016-03-17 19:59:33'),
(2424, 66, 'Autores e ideias', 'Mona Dorf', 'Editora Benvirá', '20150821100540Publicações_mona dorf.jpg', 'Mona Dorf apresenta as entrevistas feitas com os autores que participaram do Letras & Leituras, seu programa na rádio Eldorado.', 0, '2016-04-18 18:35:56', '2016-04-18 18:35:56'),
(2429, 96, 'Maquiagem', 'Duda Molinos', 'Senac', '20151123142927maquiagemduda-senac.jpg', 'Dicas de maquiagem', 0, '2016-04-25 19:37:35', '2016-04-25 19:37:35'),
(2444, 61, 'Guia Josimar 2016', 'Josimar Melo ', 'DBA', '2016011412245212289604-1654931774791496-1095599795465099297-n.jpg', 'Josimar sempre com um olhar atento no que há  de melhor na gastronomia ', 0, '2016-04-26 21:00:21', '2016-04-26 21:00:21'),
(2445, 61, 'Guia Josimar 2015', 'Josimar Melo', 'DBA', '20151123144931201510161712062015.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 1, '2016-04-26 21:00:21', '2016-04-26 21:00:21'),
(2446, 61, 'Guia Josimar 2014', 'Josimar Melo', 'DBA', '20151123144816201510161710172014.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 2, '2016-04-26 21:00:21', '2016-04-26 21:00:21'),
(2447, 61, 'Guia Josimar 2013', 'Joismar Melo', 'DBA', '20151123144905201510161711362013.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 3, '2016-04-26 21:00:21', '2016-04-26 21:00:21'),
(2448, 61, 'Guia Josimar 2012', 'Josimar Melo', 'DBA', '20151123145000201510161712342012.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 4, '2016-04-26 21:00:21', '2016-04-26 21:00:21'),
(2449, 61, 'Guia Josimar 2011', 'Josimar Melo', 'DBA', '20151123145025201510161713102011.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 5, '2016-04-26 21:00:21', '2016-04-26 21:00:21'),
(2450, 61, 'Guia Josimar 2010', 'Josimar Melo', 'DBA', '20151123145055201510161713472010.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 6, '2016-04-26 21:00:21', '2016-04-26 21:00:21'),
(2451, 61, 'Guia Josimar 2008', 'Josimar Melo', 'DBA', '20151123145120201510161714302008.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 7, '2016-04-26 21:00:21', '2016-04-26 21:00:21'),
(2452, 61, 'Guia Josimar 2007', 'Josimar Melo', 'DBA', '20151123145151201510161715142007.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 8, '2016-04-26 21:00:21', '2016-04-26 21:00:21'),
(2453, 61, 'Guia Josimar 2006', 'Josimar Melo', 'DBA', '20151123145304201510161717172006.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 9, '2016-04-26 21:00:21', '2016-04-26 21:00:21'),
(2454, 61, 'Guia Josimar 2005', 'Josimar Melo', 'DBA', '20151123145334201510161717332005.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 10, '2016-04-26 21:00:21', '2016-04-26 21:00:21'),
(2455, 61, 'Guia Josimar 2004', 'Josimar Melo', 'DBA', '20151123145402201510161717562004.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 11, '2016-04-26 21:00:21', '2016-04-26 21:00:21'),
(2456, 61, 'Guia Josimar 2003', 'Josimar Melo', 'DBA', '20151123145433201510161718202003.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 12, '2016-04-26 21:00:21', '2016-04-26 21:00:21'),
(2457, 61, 'Guia Josimar 2002', 'Josimar Melo', 'DBA', '20151123145508201510161718442002.jpg', 'Josimar analisa os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 13, '2016-04-26 21:00:21', '2016-04-26 21:00:21'),
(2458, 61, 'A Cerveja', 'Josimar Melo', 'Publifolha', '201511231455552015101617202720150821140732publicacoes-josimar-melo-acerveja-2.jpg', 'Uma obra sobre a cerveja no Brasil e no mundo. ', 14, '2016-04-26 21:00:21', '2016-04-26 21:00:21'),
(2459, 61, 'Berinjela se Escreve com J', 'Josimar Melo', 'DBA', '201511231456402015101617211420150821140759publicacoes-josimar-melo-beringela-se-escreve-com-j-3.jpg', 'Livro aborda a grafia dos cardápios nos resturantes', 15, '2016-04-26 21:00:21', '2016-04-26 21:00:21'),
(2460, 61, 'Flavour Guide', 'Josimar Melo', 'BBD', '201511231457222015101617222020150821140810publicacoes-josimar-melo-flovour-guide-4.jpg', 'Um guia que conta com a colaboração dos melhores restaurantes e chefs de cozinha do país.', 16, '2016-04-26 21:00:21', '2016-04-26 21:00:21'),
(2479, 71, 'Maquiagem', 'Duda Molinos', 'Senac', '20151123142827maquiagemduda-senac.jpg', 'Dicas de maquiagem', 0, '2016-05-04 18:54:30', '2016-05-04 18:54:30'),
(2556, 139, 'Vic Meirelles 2015', 'Vic Meirelles', 'Queen Books', '20160328164631capa-livro-para-site.jpg', 'Vic Meirelles é uma assinatura. O que quer dizer que basta existir esses dois nomes (Vic + Meirelles) embaixo de qualquer papel, projeto ou combinação de cores - que seja! - para que a coisa ganhe uma proporção estratosférica. Sim, porque com ele nada é pouco ou muito menos "mais ou menos". Vic é sinônimo de quantidade e exagero, sempre aliados ao bom gosto. ', 0, '2016-05-24 18:09:57', '2016-05-24 18:09:57'),
(2601, 73, 'Coleção Moda Brasileira ', 'Marcelo Sommer', 'Cosac Naify', '20151123151143arquivoexibir-1.jpg', 'Através de 482 imagens, o livro desvenda o mundo de Marcelo Sommer. Das primeiras peças vendidas no Mercado Mundo Mix até o momento de amadurecimento atual, a edição aborda treze anos de trajetória do estilista.', 0, '2016-07-29 23:01:36', '2016-07-29 23:01:36'),
(2632, 91, 'A Moda', 'Erika Palomino', 'Publifolha', '201511231434081376484.jpg', 'Apresenta o universo da alta-costura, do prêt-à-porter ao streetwear. O livro analisa a cadeia têxtil e o ponto de partida das tendências, além de esclarecer os principais conceitos e correntes no estudo da moda. A obra inclui um histórico sobre a moda brasileira e serve como paradidático para os cursos de Moda.', 0, '2016-09-20 19:38:47', '2016-09-20 19:38:47'),
(2633, 91, 'Babado Forte', 'Erika Palomino', 'Mandarim', '20151123143449babado-forte-moda-musica-e-noite-livro-erika-palomino-13989-mlb3618042428-012013-o.jpg', 'Autora conta neste livro a história da juventude dos anos 90, sob o eixo Rio-São Paulo, dentro do trinômio MMC, ou seja, moda, música e comportamento.', 1, '2016-09-20 19:38:47', '2016-09-20 19:38:47'),
(2864, 90, 'Meu caderno de esrampa', 'Costanza Pascolato', 'Editora Planeta', '2015112314222690915-livro-sta-constancia-01.jpg', 'As estampas favoritas produzidas pela tecelagem de sua família, a Santa Constancia, além de ilustrações para colorir, o livro vem recheado de dicas de estilo e elegância sobre o universo das roupas estampadas.', 0, '2017-01-27 18:57:06', '2017-01-27 18:57:06'),
(2865, 90, 'O essencial', '', 'Sextante', '20151123142212o-essencial-capa-costanza-pascolato-livro-moda-onde-comprar.jpg', 'Dicas sobre elegância, educação e bom gosto.', 1, '2017-01-27 18:57:06', '2017-01-27 18:57:06'),
(2866, 90, 'Como ser uma modelo de sucesso', 'Como ser uma modelo de sucesso', 'Jaboticaba', '20150821133623Publicacoes_Costanza Pascolato_1.jpg', 'Através do depoimento de bookers, fotógrafos e modelos, o livro fornece dicas para quem quer seguir a carreira de modelo.', 2, '2017-01-27 18:57:06', '2017-01-27 18:57:06'),
(2867, 90, 'Confidencial', '', 'Jaboticaba', '20151123142151confidencial-costanza-livro.jpg', 'Dicas de moda, estilo e bem viver', 3, '2017-01-27 18:57:06', '2017-01-27 18:57:06'),
(2868, 90, 'O essencial', '', 'Sextante', '20151123142141novo-livro-costanza-pacolato-o-essencial-2.jpg', 'Dicas sobre elegância, educação e bom gosto.', 4, '2017-01-27 18:57:06', '2017-01-27 18:57:06'),
(2883, 63, 'A Moda', 'Erika Palomino', 'Publifolha', '20150821094108Publicacoes_Erika Palomino_1.jpg', 'Apresenta o universo da alta-costura, do prêt-à-porter ao streetwear. O livro analisa a cadeia têxtil e o ponto de partida das tendências, além de esclarecer os principais conceitos e correntes no estudo da moda. A obra inclui um histórico sobre a moda brasileira e serve como paradidático para os cursos de Moda.', 0, '2017-01-27 19:29:19', '2017-01-27 19:29:19'),
(2884, 63, 'Babado Forte', 'Erika Palomino', 'Mandarim', '20150821094125publicacoes_Erika Palomino_2.jpg', 'Autora conta neste livro a história da juventude dos anos 90, sob o eixo Rio-São Paulo, dentro do trinômio MMC, ou seja, moda, música e comportamento.', 1, '2017-01-27 19:29:19', '2017-01-27 19:29:19'),
(2888, 173, 'Politica, Propina e Futebol ', 'Jamil Chade ', 'Objetiva ', '20161018202147livrojamilchadedivulgacao.jpg', ' Jamil Chade nos avisa logo no início de “Política, Propina e Futebol: Como o ‘Padrão Fifa’ ameaça o esporte mais popular do planeta” que este não se trata de um livro sobre futebol. De fato, há poucas menções a bola rolando, mas muitas sobre quem realmente a faz rolar, como por exemplo, a CBF.', 0, '2017-01-27 20:43:07', '2017-01-27 20:43:07'),
(2889, 173, 'A Copa Como  Ela  É', 'Jamil Chade ', 'Companhia Das Letras ', '20161018200743a-copa-como-ela-e-capa.jpg', 'Depois de acompanhar seus preparativos durante dez anos, o jornalista Jamil Chade revela os bastidores da Copa do Mundo mais cara da história. Durante dez anos, o jornalista Jamil Chade acompanhou de perto as negociações que culminaram na escolha do Brasil para sediar a Copa do Mundo de 2014, bem como todas as polêmicas que se seguiram ao anúncio. Correspondente de O Estado de S. Paulo na Suíça, Chade teve acesso privilegiado aos corredores da Fifa e às principais figuras que movimentaram o grande balcão de oportunidades que se tornou o Mundial', 1, '2017-01-27 20:43:07', '2017-01-27 20:43:07'),
(2890, 173, 'Rousseff ', 'Chade,Jamil / Indjov,Momchil', 'Virgiliae', '20161020131008roussef-quadrado.jpg', 'Quando Dilma Rousseff foi eleita presidente do Brasil, ninguém sabia os tantos porquês da comemoração que se deu na Bulgária. Por trás do motivo aparentemente simples - Dilma é filha de um búlgaro - esconde-se a as tragédias e abismos enfrentados por uma família dividida entre dois países, e de fato separada por uma mudança de sobrenome, os Russev e os Rousseff. Tendo o Pedro Rousseff fugido do seu país de origem em 1929, Dilma teve um meio-irmão a quem nunca conheceu. A vida dos dois, Dilma e Luben, seguiram rumos diametralmente opostos, mas que, por coincidências, tiveram muitos pontos em comum, como o comunismo e a opressão de regimes ditatoriais. A história dessa família dividida desenrola-se em duas conturbadas regiões do mundo, América Latina e Bálcãs, que são muito mais do que pano de fundo para a saga desta obra.', 2, '2017-01-27 20:43:07', '2017-01-27 20:43:07'),
(2891, 173, 'O Mundo não é Plano ', 'Jamil Chade ', 'Saraiva ', '20161020131808o-mundo-nao-e-plano-quadrado.jpg', 'O mundo não é plano trata das diferenças humanas, mostra que as condições existem, que interesses ainda sobrepujam vidas, apresenta dados que mostram sobras em alguns locais enquanto falta tanto em outros. Dilema humano, este livro trata de gente: dos que ainda não andam sozinhos. Mas é escrito para muitos outros, os que precisam tomar consciência e exigir que a situação fique o mais rápido possível como um registro ruim da história.', 3, '2017-01-27 20:43:07', '2017-01-27 20:43:07'),
(2894, 99, 'Josimar 2016', 'Josimar Melo', 'DBA', '2015120717035410-o-livro-lancado.jpg', 'Josimar sempre com um olhar atento no que há  de melhor na gastronomia ', 0, '2017-01-27 21:06:29', '2017-01-27 21:06:29'),
(2895, 99, 'Guia Josimar 2015', 'Josimar Melo ', 'DBA', '20151123145940201510161712062015.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 1, '2017-01-27 21:06:29', '2017-01-27 21:06:29'),
(2896, 99, 'Guia Josimar 2014', 'Josimar Melo', 'DBA', '20151123145951201510161710172014.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 2, '2017-01-27 21:06:29', '2017-01-27 21:06:29'),
(2897, 99, 'Guia Josimar 2013', 'Josimar Melo', 'DBA', '20151123150011201510161711362013.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 3, '2017-01-27 21:06:29', '2017-01-27 21:06:29'),
(2898, 99, 'Guia Josimar 2012', 'Josimar Melo ', 'DBA', '20151123150106201510161712342012.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 4, '2017-01-27 21:06:29', '2017-01-27 21:06:29'),
(2899, 99, 'Guia Josimar 2011', 'Josimar Melo ', 'DBA', '20151123150131201510161713102011.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 5, '2017-01-27 21:06:29', '2017-01-27 21:06:29'),
(2900, 99, 'Guia Josimar 2010', 'Josimar Melo ', 'DBA', '20151123150145201510161713472010.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 6, '2017-01-27 21:06:29', '2017-01-27 21:06:29'),
(2901, 99, 'Guia Josimar Melo 2009', 'Josimar Melo', 'DBA', '20150821140924publicacoes_josimar Melo_guia 2009_9.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 7, '2017-01-27 21:06:29', '2017-01-27 21:06:29'),
(2902, 99, 'Guia Josimar Melo 2008', 'Josimar Melo ', 'DBA', '20151123150223201510161714302008.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 8, '2017-01-27 21:06:29', '2017-01-27 21:06:29'),
(2903, 99, 'Guia Josimar Melo 2007', 'Josimar Melo ', 'DBA', '20151123150239201510161715142007.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 9, '2017-01-27 21:06:29', '2017-01-27 21:06:29'),
(2904, 99, 'A cerveja', 'Josimar Melo', '', '201511231503032015101617202720150821140732publicacoes-josimar-melo-acerveja-2.jpg', 'Uma obra sobre a cerveja no Brasil e no mundo. ', 10, '2017-01-27 21:06:29', '2017-01-27 21:06:29'),
(2905, 99, 'Guia Josimar Melo 2006', 'Josimar Melo', 'DBA', '20151123150323201510161717172006.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 11, '2017-01-27 21:06:29', '2017-01-27 21:06:29'),
(2906, 99, 'Guia Josimar 2005', 'Josimar Melo ', 'DBA', '20151123150342201510161717332005.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 12, '2017-01-27 21:06:29', '2017-01-27 21:06:29'),
(2907, 99, 'Berinjela se escreve com J', 'Josimar  Melo', 'DBA', '201511231504032015101617211420150821140759publicacoes-josimar-melo-beringela-se-escreve-com-j-3.jpg', 'Livro aborda a grafia dos cardápios nos restaurantes', 13, '2017-01-27 21:06:29', '2017-01-27 21:06:29'),
(2908, 99, 'Flavour', 'Josimar Melo', 'BBD', '20150821140810Publicacoes_Josimar Melo_Flovour guide_4].jpg', 'O livro é um guia que conta com a colaboração dos melhores restaurantes e chefs de cozinha do país', 14, '2017-01-27 21:06:29', '2017-01-27 21:06:29'),
(2926, 140, 'Como Viver em São Paulo Sem Carro 2014', 'Leão Serva', 'Neotrópica', '201604081754412014.jpg', 'Já na 3ª edição, este livro vem mostrando que a troca do automóvel por outras formas de locomoção é algo real, uma tendência que está acontecendo no mundo e também na cidade. Para entender a mudança, o Como Viver em São Paulo sem Carro 2014 entrevistou 15 pessoas que se adaptaram a essa nova realidade. Neste livro, elas contam porque optaram por vender o carro – e quais meios adotaram como alternativa. ', 0, '2017-01-27 21:07:56', '2017-01-27 21:07:56'),
(2927, 140, 'Como Viver em São Paulo Sem Carro 2013', 'Leão Serva', 'Neotrópica', '20160408175347edicao-ok.jpg', 'Nos últimos tempos milhares de paulistanos têm mudado seus hábitos de locomoção para fugir dos grandes congestionamentos. Adotam Metrô, ônibus, bicicleta ou caminhada. Sua vida é facilitada pelo esforço de “heróis da mobilidade”, que criam soluções para ajudar quem decidiu abandonar o carro para ser mais feliz. ', 1, '2017-01-27 21:07:56', '2017-01-27 21:07:56'),
(2928, 140, 'Como Viver em São Paulo Sem Carro 2012', 'Leão Serva ', 'Neotrópica', '20160408175254edicao-2012.jpg', '12 cidadãos de São Paulo, de diferentes atividades, idades e origens, procuram descrever como descobriram soluções para levar a vida em São Paulo, dispensando o uso do automóvel. Cada depoimento é acompanhado de um mapa ilustrado, que pretende mostrar as preferências de cada entrevistado entre as opções que a cidade oferece.', 2, '2017-01-27 21:07:56', '2017-01-27 21:07:56'),
(2944, 93, 'Ui!', 'Regina Guerreiro', 'Editora Lust', '20151123151430download.jpg', 'Biografia da autora, busca retratar uma profissional com mais de 40 anos de experiência no mercado de moda.', 0, '2017-01-27 21:55:39', '2017-01-27 21:55:39'),
(2945, 78, 'Ui!', 'Regina Gurreiro', 'Editora Lust', '20151123151353download.jpg', 'Biografia da autora, busca retratar uma profissional com mais de 40 anos de experiência no mercado de moda.', 0, '2017-01-27 21:57:58', '2017-01-27 21:57:58'),
(2946, 84, 'Silvia Poppovic e você', '', 'Mandarim', '2015112315161420150821135805publicacoes-silvia-poppovic-1.jpg', 'O livro aborda temas como casamento, filhos, amor, sexo, trabalho e as pequenas dificuldades do dia-a-dia na visão da autora.', 0, '2017-01-27 22:34:17', '2017-01-27 22:34:17'),
(2947, 44, 'Silvia Popovic e você', '', 'Siciliano', '20151201174528livro-silvia.jpg', 'O livro aborda temas como casamento, filhos, amor, sexo, trabalho e as pequenas dificuldades do dia-a-dia na visão da autora.', 0, '2017-01-27 22:38:07', '2017-01-27 22:38:07'),
(2979, 177, ' Assassinato de Reputações - Um Crime de Estado', 'Claudio Tognolli / Romeu Tuma Junior', 'Matrix ', '20170201133353download.jpg', ' Impossível entender o Brasil e seus grandes escândalos dos últimos anos sem ler esta obra. Entenda como funcionaram, na última década, órgãos de segurança institucional como a Polícia Federal e a Abin. Veja as provas do grampo telefônico no STF e como são tratados desafetos políticos e empresários incômodos ao governo, e qual o objetivo real de operações midiáticas como a Trovão, a Chacal e a Satiagraha, entre muitos outros temas polêmicos. ', 0, '2017-02-06 19:13:34', '2017-02-06 19:13:34'),
(2980, 177, 'Assassinato de Reputações II - Muito Além da Lava Jato ', 'Claudio Tognolli / Romeu Tuma Junior', 'Matrix', '20170201132651romeu-tuma-jr-ii.png', 'O segundo volume de Assassinato de reputações traz esses e outros relatos contundentes nunca revelados dos bastidores da Lava Jato, inclusive o fato de que ela poderia ter sido antecipada em dois anos, por conta de uma denúncia rica em detalhes que não foi levada em frente pela Polícia Federal, e que teria sérios reflexos às vésperas das eleições que colocaram Dilma Rousseff mais quatro anos à frente da presidência da República. A obra mostra também informações inéditas das falcatruas no BNDES, as negociatas dos filhos de Lula e muitos outros fatos escabrosos do PT e de outros políticos brasileiros, revelados por Romeu Tuma Junior.', 1, '2017-02-06 19:13:34', '2017-02-06 19:13:34'),
(2981, 177, 'Bem Vindo ao Inferno ', ' ClaudioTognolli / Malu Magalhães', 'Matrix ', '20170201132625download-1.jpg', 'Vana Lopes foi uma das vítimas do médico estuprador Roger Abdelmassih. Sua busca por justiça começou em 1993, e passou por diversos percalços e incidentes estranhos, como um boletim de ocorrência desaparecido da delegacia. A luta para localizar Abdelmassih, após ele ganhar um habeas corpus do STF e fugir do país, é um dos maiores exemplos de determinação e coragem que o Brasil já viu. Enquanto a polícia não conseguia pistas, Vana soube utilizar com maestria e criatividade as redes sociais e a mídia, para se transformar em uma catalisadora de informantes e juntar documentos – entre movimentações financeiras e viagens – que conduziram a polícia à capturado criminoso.', 2, '2017-02-06 19:13:34', '2017-02-06 19:13:34'),
(2982, 177, '50 Anos a Mil ', 'Lobão / Claudio Tognolli', 'Nova Fronteira', '20170201133751download-2.jpg', 'Polêmico,zangado,romântico,revolucionário.50 anos a mil é a autobiografia de Lobão,que conta,em um volume fartamente ilustrado,a história do menino que queria ser jogador de futebol e acabou se transformando num dos grandes nomes do Rock brasileiro.As músicas,os amigos,as confusões com a polícia - o grande lobo não poupa nada nem ninguém.', 3, '2017-02-06 19:13:34', '2017-02-06 19:13:34'),
(2983, 177, 'O Seculo do Crime', 'José Arbex Jr. / Claudio Tognolli', 'Boitempo', '20170201181330o-seculo-do-crime-jose-arbex-jr-8585934093-300x300-pu6ec7e281-1.jpg', 'Desvenda o mundo subterrâneo das drogas, da venda ilegal de armas, crianças e órgãos humanos. Livro-reportagem, procura explicar as razões sociais e políticas que deram origem às principais máfias em atividade no mundo. Polêmico, os autores constroem um painel sobre as estruturas das principais organizações criminosas multinacionais e analisam os principais episódios.', 4, '2017-02-06 19:13:34', '2017-02-06 19:13:34'),
(3046, 171, 'Como Ter Uma vida Normal Sendo Louca ', 'Jana Rosa / Camila Frender ', 'Ediouro', '20161006160546publicacoes-jana-rosa-1.jpg', '', 0, '2017-03-30 14:43:38', '2017-03-30 14:43:38'),
(3047, 171, 'Enfim , 30 ', 'Jana Rosa / Camila Frender ', 'Companhia das Letras ', '20161006160931imagemaspx.png', '', 1, '2017-03-30 14:43:38', '2017-03-30 14:43:38'),
(3069, 159, 'Crônicas de um Rimador', 'Álbum de rap de André Ramiro', 'Estudio Fábrica de Chocolate', '20161004133303cronicas-de-um-rimador.jpg', 'Produção de Damien Seth e Co-produção de DJ Pachu.', 0, '2017-04-13 20:45:55', '2017-04-13 20:45:55'),
(3072, 199, 'Memorial Do Escândalo ', 'Gerson Camarotti & Bernardo DeLa Penna ', 'Geração ', '20170424183412unknown.jpeg', '', 0, '2017-04-24 21:50:28', '2017-04-24 21:50:28'),
(3073, 199, 'Segredos Do Conclave', 'Gerson Camarotti', 'Geração ', '20170424184053unknown-1.jpeg', '', 1, '2017-04-24 21:50:28', '2017-04-24 21:50:28'),
(3074, 107, 'Dudu Bertholini Dândi Tropical', 'Dudu Bertholini', '', '20151123143313dudub.png', 'Dândi Tropical é uma imersão na vida de Dudu Bertholini, retratando facetas, crenças e influências da vida do estilista.', 0, '2017-04-27 19:20:24', '2017-04-27 19:20:24'),
(3077, 105, 'Coleção Moda Brasileira', 'Marcelo Sommer', 'Cosac Naify', '20151123151243arquivoexibir-1.jpg', 'Através de 482 imagens, o livro desvenda o mundo de Marcelo Sommer. Das primeiras peças vendidas no Mercado Mundo Mix até o momento de amadurecimento atual, a edição aborda treze anos de trajetória do estilista.', 0, '2017-04-27 19:25:53', '2017-04-27 19:25:53'),
(3078, 108, 'Guia Josimar 2016', 'Josimar Melo', 'DBA', '2016011413132412289604-1654931774791496-1095599795465099297-n.jpg', 'Josimar sempre com um olhar atento no que há  de melhor na gastronomia ', 0, '2017-04-27 19:34:03', '2017-04-27 19:34:03'),
(3079, 108, 'Guia Josimar 2015', 'Josimar Melo', 'DBA', '201510161712062015.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 1, '2017-04-27 19:34:03', '2017-04-27 19:34:03'),
(3080, 108, 'Guia Josimar 2014', 'Josimar Melo', 'DBA', '201510161710172014.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 2, '2017-04-27 19:34:03', '2017-04-27 19:34:03'),
(3081, 108, 'Guia Josimar 2013', 'Josimar Melo', 'DBA', '201510161711362013.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 3, '2017-04-27 19:34:03', '2017-04-27 19:34:03'),
(3082, 108, 'Guia Josimar 2012', 'Josimar Melo', 'DBA', '201510161712342012.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 4, '2017-04-27 19:34:03', '2017-04-27 19:34:03'),
(3083, 108, 'Guia Josimar 2011', 'Josimar Melo', 'DBA', '201510161713102011.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 5, '2017-04-27 19:34:03', '2017-04-27 19:34:03'),
(3084, 108, 'Guia Josimar 2010', 'Josimar Melo', 'DBA', '201510161713472010.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 6, '2017-04-27 19:34:03', '2017-04-27 19:34:03'),
(3085, 108, 'Guia Josimar 2008', 'Josimar Melo', 'DBA', '201510161714302008.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 7, '2017-04-27 19:34:03', '2017-04-27 19:34:03'),
(3086, 108, 'Guia Josimar 2007', 'Josimar Melo', 'DBA', '201510161715142007.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 8, '2017-04-27 19:34:03', '2017-04-27 19:34:03'),
(3087, 108, 'Guia Josimar 2006', 'Josimar Melo', 'DBA', '201510161717172006.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 9, '2017-04-27 19:34:03', '2017-04-27 19:34:03'),
(3088, 108, 'Guia Josimar 2005', 'Josimar Melo', 'DBA', '201510161717332005.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 10, '2017-04-27 19:34:03', '2017-04-27 19:34:03'),
(3089, 108, 'Guia Josimar 2004', 'Josimar Melo', 'DBA', '201510161717562004.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 11, '2017-04-27 19:34:03', '2017-04-27 19:34:03'),
(3090, 108, 'Guia Josimar 2003', 'Josimar Melo', 'DBA', '201510161718202003.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 12, '2017-04-27 19:34:03', '2017-04-27 19:34:03'),
(3091, 108, 'Guia Josimar 2002', 'Josimar Melo', 'DBA', '201510161718442002.jpg', 'Josimar analisa com os melhores estabelecimentos gastronômicos da cidade de São Paulo. Com informações completas, como faixas de preço, cartões de crédito e tíquetes aceitos em cada estabelecimento, além do perfil dos bares e restaurantes e a classificação de seus serviços.', 13, '2017-04-27 19:34:03', '2017-04-27 19:34:03'),
(3092, 108, 'A Cerveja', 'Josimar Melo', 'Publifolha', '2015101617202720150821140732publicacoes-josimar-melo-acerveja-2.jpg', 'Uma obra sobre a cerveja no Brasil e no mundo. ', 14, '2017-04-27 19:34:03', '2017-04-27 19:34:03'),
(3093, 108, 'Berinjela', 'Josimar Melo', 'DBA', '2015101617211420150821140759publicacoes-josimar-melo-beringela-se-escreve-com-j-3.jpg', 'Livro aborda a grafia dos cardápios nos restaurantes', 15, '2017-04-27 19:34:03', '2017-04-27 19:34:03'),
(3094, 108, 'Flavour', 'Josimar Melo', 'BBD', '2015101617222020150821140810publicacoes-josimar-melo-flovour-guide-4.jpg', 'O livro é um guia que conta com a colaboração dos melhores restaurantes e chefs de cozinha do país', 16, '2017-04-27 19:34:03', '2017-04-27 19:34:03'),
(3097, 54, 'Coleção Moda Brasileira', 'Glória Coelho', 'Cosac Naify', '201512011859248.jpg', 'A coleção, iniciativa inédita no mercado editorial brasileiro, traça um panorama da moda contemporânea através da trajetória de cinco dos mais importantes estilistas do país', 0, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(3098, 86, 'Coleção Moda Brasileira', 'Glória Coelho', 'Cosac Naif', '201512011900548.jpg', 'A coleção, iniciativa inédita no mercado editorial brasileiro, traça um panorama da moda contemporânea através da trajetória de cinco dos mais importantes estilistas do país', 0, '2017-05-05 20:41:29', '2017-05-05 20:41:29'),
(3104, 112, 'A Moda', 'Erika Palomino', 'Publifolha', '201511231436381376484.jpg', 'Apresenta o universo da alta-costura, do prêt-à-porter ao streetwear. O livro analisa a cadeia têxtil e o ponto de partida das tendências, além de esclarecer os principais conceitos e correntes no estudo da moda. A obra inclui um histórico sobre a moda brasileira e serve como paradidático para os cursos de Moda.', 0, '2017-05-17 18:32:22', '2017-05-17 18:32:22'),
(3105, 112, 'Babado Forte', 'Erika Palomino', 'Mandarim', '20151123143650babado-forte-moda-musica-e-noite-livro-erika-palomino-13989-mlb3618042428-012013-o.jpg', 'Autora conta neste livro a história da juventude dos anos 90, sob o eixo Rio-São Paulo, dentro do trinômio MMC, ou seja, moda, música e comportamento.', 1, '2017-05-17 18:32:22', '2017-05-17 18:32:22'),
(3106, 125, 'Silvia Poppovic e você', 'Silvia Poppovic', 'Mandarim', '20151201175119livro-silvia.jpg', 'O livro aborda temas como casamento, filhos, amor, sexo, trabalho e as pequenas dificuldades do dia-a-dia na visão da autora.', 0, '2017-05-17 18:55:10', '2017-05-17 18:55:10'),
(3107, 34, 'Meu cardeno de estampa', 'Costanza Pascolato', 'Editora Planeta', '20151201160357testeira-redes.jpg', 'As estampas favoritas produzidas pela tecelagem de sua família, a Santa Constancia, além de ilustrações para colorir, o livro vem recheado de dicas de estilo e elegância sobre o universo das roupas estampadas.', 0, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(3108, 34, 'O essencial', '', 'Sextante', '20151201160442116660001-1gg.jpg', 'Dicas sobre elegância, educação e bom gosto.', 1, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(3109, 34, 'Como ser uma modelo de sucesso', '', 'Jaboticaba', '2015120116050220150821133623publicacoes-costanza-pascolato-1.jpg', 'Através do depoimento de bookers, fotógrafos e modelos, o livro fornece dicas para quem quer seguir a carreira de modelo.', 2, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(3110, 34, 'Confidencial', '', 'Jaboticaba', '20151201155017costanza.jpg', 'Dicas de moda, estilo e bem viver', 3, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(3111, 34, 'O essencial', '', 'Sextante', '20151201155031costanza-3.jpg', 'Dicas sobre elegância, educação e bom gosto.', 4, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(3112, 135, 'Meu Cadernos de Estampas', 'Costanza Pascolato', 'Editora Planeta', '20160119143857meu-caderno-de-estampas-edito-planeta.jpg', 'As estampas favoritas produzidas pela tecelagem de sua família, a Santa Constancia, além de ilustrações para colorir, o livro vem recheado de dicas de estilo e elegância sobre o universo das roupas estampadas', 0, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(3113, 135, 'O Essencial', 'Costanza Pascolato', 'Editora Sextante', '20160119144014o-essencial-ed-sextante.jpg', 'Dicas sobre elegância, educação e bom gosto.', 1, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(3114, 135, 'Como ser uma modelo de sucesso', 'Costanza Pascolato', 'Editora Jaboticaba', '20160119144121como-ser-uma-modelo-de-sucess-o-edit-jaboticaba.jpg', 'Através do depoimento de bookers, fotógrafos e modelos, o livro fornece dicas para quem quer seguir a carreira de modelo.', 2, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(3115, 135, 'Costanza Pascolato', 'Costanza Pascolato', 'Editora Jaboticaba', '20160119144156confidencial-ed-jaboticaba.jpg', 'Dicas de moda, estilo e bem viver', 3, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(3116, 135, 'O Essencial', 'Costanza Pascolato', 'Editora Sextante', '20160119144258oessencial-antigo-ed-sextante.jpg', 'Dicas sobre elegância, educação e bom gosto', 4, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(3126, 102, 'O Biquíni Made In Brazil', 'Lilian Pacce ', ' Arte Ensaio', '20160928110954livro-lilian-pacce.jpg', ' Biquíni Made In Brazil, que fala sobre a história e todas as curiosidades da peça. Com projeto gráfico assinado por Giovanni Bianco, além de mostrar fotos incríveis da evolução do traje de banho.', 0, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(3127, 102, 'Ecobags', '', 'Senac', '2015112316255520151123150726ecobags-capa-livro.jpg', 'Um registro em fotos da exposição “Eu Não Sou de Plástico“, com sacolas não-descartáveis assinadas por grandes grifes e estilistas.', 1, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(3128, 102, 'Pelo Mundo da Moda', '', 'Senac', '20151123162629201511231507084612-livros-pelo-mundo-da-moda1.jpg', 'O livro  conta um pouco da história da moda,  revela não somente os bastidores das grandes criações no exterior como também o perfil de modelos de projeção internacional, além de apresentar uma análise do trabalho e da personalidade de importantes estilistas.', 2, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(3130, 72, 'Entre sem bater', 'Marcelo Rosembaum', 'Abril', '20151123150933reforma-casa-marcelo-rosenbaum-vira-livro-abre.jpeg', 'O livro permite ao leitor fazer uma imersão no dia a dia do designer e no processo criativo na construção do seu novo lar.', 0, '2017-05-26 13:49:46', '2017-05-26 13:49:46'),
(3131, 88, 'Entre sem bater', 'Marcelo Rosenbaum', 'Abril', '20151123151007reforma-casa-marcelo-rosenbaum-vira-livro-abre.jpeg', 'O livro permite ao leitor fazer uma imersão no dia a dia do designer e no processo criativo na construção do seu novo lar.', 0, '2017-05-26 13:50:03', '2017-05-26 13:50:03'),
(3132, 128, 'O Futuro É... Viajar, Malhar, Estudar, Namorar e Investir!', 'Mara Luquet', 'Benvira ', '2016092815130946332062.jpg', 'Neste livro, ela elabora com maestria três mensagens essenciais para quem quer bem usufruir da revolução da longevidade - Sem dúvida é melhor envelhecer com qualidade a morrer cedo. É indispensável adotar uma perspectiva de curso de vida- quanto mais cedo nos prepararmos para bem envelhecer, melhor – porém, nunca é tarde demais.', 0, '2017-05-29 17:29:27', '2017-05-29 17:29:27'),
(3133, 128, 'Muito Além do Voo', 'Mara Luquet e Ruy Marra', 'Leya Brasil ', '20151113203149download.jpg', 'Em “Muito Além do Voo”, os autores mostram como o estresse do dia a dia e o medo paralisante podem ser vencidos ao seguirmos uma alimentação saudável, praticarmos algum exercício, termos pensamentos positivos e respirarmos corretamente – e com várias referências e pesquisas científicas provando que isso é possível. Para eles, o esporte foi um ótimo momento para se vencer medos e se preparar para a adoção de um novo estilo de vida que só beneficia nosso corpo e nossa alma.', 1, '2017-05-29 17:29:27', '2017-05-29 17:29:27'),
(3134, 128, 'O Meu Guia de Finanças Pessoais - Como Gastar Sem Culpa e Investir Sem Erros', 'Mara Luquet', 'Letras e Lucros', '20151123165154mara-luquet-o-meu-guia-de-financas-pessoais-como-gastar-sem-culpa-e-investir-sem-erros-mara-luquet-8535238883-200x200-pu6e7a87b0-1.jpg', 'Este livro é um guia de finanças diferenciado e com particularidades relevantes, pois mostra como Mara Luquet cuida de seus recursos acumulados e como ela conduz seus próprios investimentos. A jornalista explica e compartilha suas experiências com gestão de orçamento e compra de ações e demonstra como esse assunto está entranhado nos projetos mais caros, nos sonhos e vivências mais importantes das pessoas. Através de anotações variadas, algumas muito íntimas, Mara Luquet fornece aos leitores seus próprios truques para driblar e enfrentar a armadilha da “generosidade” dos cartões de crédito e do sistema financeiro de uma forma geral. Trata-se de uma leitura essencial para quem precisa equilibrar o orçamento financeiro e se livrar das dívidas.', 2, '2017-05-29 17:29:27', '2017-05-29 17:29:27'),
(3135, 128, 'O Assunto É Dinheiro, uma Conversa com Carlos Alberto Sardenberg e Mara Luquet ', 'Mara Luquet e Carlos Alberto Sardenberg', 'Saraiva com Colaboração Letras e Lucros', '20151123165212o-assunto-e-dinheiro-uma-conversa-com-carlos-alberto-sardenberg-e-mara-luquet-col-letras-lucro-carlos-alberto-sardenberg-mara-luquet-8502055623-200x200-pu6e47e57a-1.jpg', 'Administrar as finanças domésticas de forma a viver bem hoje e no futuro está ao alcance apenas dos indicados em assuntos financeiros? Os autores mostram que não, diariamente, milhares de ouvintes da CBN. Num bate papo marcado pela irreverência e bom humor, os dois jornalistas passeiam pelo tema desmistificando-o e indicando caminhos para se lidar com dinheiro, com larga dose de com senso e um pouco de estratégia. Receita ao alcance de todos.', 3, '2017-05-29 17:29:27', '2017-05-29 17:29:27'),
(3136, 134, 'O Futuro É... Viajar, Malhar, Estudar, Namorar e Investir!', 'Mara Luquet', 'Benvirá', '2016092815080846332062.jpg', 'Neste livro, ela elabora com maestria três mensagens essenciais para quem quer bem usufruir da revolução da longevidade - Sem dúvida é melhor envelhecer com qualidade a morrer cedo. É indispensável adotar uma perspectiva de curso de vida- quanto mais cedo nos prepararmos para bem envelhecer, melhor – porém, nunca é tarde demais.', 0, '2017-05-29 17:29:44', '2017-05-29 17:29:44'),
(3137, 134, 'Muito Além do Voo', 'Mara Luquet e Ruy Marra', 'Leya Brasil', '2016011816271620151113203149download.jpg', 'Em “Muito Além do Voo”, os autores mostram como o estresse do dia a dia e o medo paralisante podem ser vencidos ao seguirmos uma alimentação saudável, praticarmos algum exercício, termos pensamentos positivos e respirarmos corretamente – e com várias referências e pesquisas científicas provando que isso é possível. Para eles, o esporte foi um ótimo momento para se vencer medos e se preparar para a adoção de um novo estilo de vida que só beneficia nosso corpo e nossa alma.', 1, '2017-05-29 17:29:44', '2017-05-29 17:29:44'),
(3138, 134, 'O Meu Guia de Finanças Pessoais - Como Gastar sem Culpa e Investir sem Erros', 'Mara Luquet', 'Letras e Lucros', '2016011816275920151123165154mara-luquet-o-meu-guia-de-financas-pessoais-como-gastar-sem-culpa-e-investir-sem-erros-mara-luquet-8535238883-200x200-pu6e7a87b0-1.jpg', 'Este livro é um guia de finanças diferenciado e com particularidades relevantes, pois mostra como Mara Luquet cuida de seus recursos acumulados e como ela conduz seus próprios investimentos. A jornalista explica e compartilha suas experiências com gestão de orçamento e compra de ações e demonstra como esse assunto está entranhado nos projetos mais caros, nos sonhos e vivências mais importantes das pessoas. Através de anotações variadas, algumas muito íntimas, Mara Luquet fornece aos leitores seus próprios truques para driblar e enfrentar a armadilha da “generosidade” dos cartões de crédito e do sistema financeiro de uma forma geral. Trata-se de uma leitura essencial para quem precisa equilibrar o orçamento financeiro e se livrar das dívidas.', 2, '2017-05-29 17:29:44', '2017-05-29 17:29:44'),
(3139, 134, 'Dinheiro', 'Mara Luquet e Carlos Alberto Sardenberg', 'Saraiva com Colaboração Letras e Lucros', '2016011816283220151123165212o-assunto-e-dinheiro-uma-conversa-com-carlos-alberto-sardenberg-e-mara-luquet-col-letras-lucro-carlos-alberto-sardenberg-mara-luquet-8502055623-200x200-pu6e47.jpg', 'Administrar as finanças domésticas de forma a viver bem hoje e no futuro está ao alcance apenas dos indicados em assuntos financeiros? Os autores mostram que não, diariamente, milhares de ouvintes da CBN. Num bate papo marcado pela irreverência e bom humor, os dois jornalistas passeiam pelo tema desmistificando-o e indicando caminhos para se lidar com dinheiro, com larga dose de com senso e um pouco de estratégia. Receita ao alcance de todos.', 3, '2017-05-29 17:29:44', '2017-05-29 17:29:44'),
(3175, 55, 'O Biquíni Made In Brazil', 'Lilian Pacce', 'Arte Ensaio', '20160928110631livro-lilian-pacce.jpg', 'O Biquíni Made In Brazil, que fala sobre a história e todas as curiosidades da peça. Com projeto gráfico assinado por Giovanni Bianco, além de mostrar fotos incríveis da evolução do traje de banho.', 0, '2017-07-07 13:10:58', '2017-07-07 13:10:58'),
(3176, 55, 'Pelo mundo da moda', 'Pelo mundo da moda', 'Senac', '201511231506344612-livros-pelo-mundo-da-moda1.jpg', 'O livro conta um pouco da história da moda,  revela não somente os bastidores das grandes criações no exterior como também o perfil de modelos de projeção internacional, além de apresentar uma análise do trabalho e da personalidade de importantes estilistas.', 1, '2017-07-07 13:10:58', '2017-07-07 13:10:58'),
(3177, 55, 'Ecobags', 'Lilian Pacce', 'Senac', '20151123104856ecobags-capa-livro.jpg', 'Um registro em fotos da exposição “Eu Não Sou de Plástico“, com sacolas não-descartáveis assinadas por grandes grifes e estilistas. ', 2, '2017-07-07 13:10:58', '2017-07-07 13:10:58'),
(3178, 92, 'O Biquíni Made In Brazil', 'Lilian Pacce ', 'Arte Ensaio', '20160928113358livro-lilian-pacce.jpg', 'O Biquíni Made In Brazil, que fala sobre a história e todas as curiosidades da peça.  Com projeto gráfico assinado por Giovanni Bianco, além de mostrar fotos incríveis da evolução do traje de banho.', 0, '2017-07-07 13:12:46', '2017-07-07 13:12:46'),
(3179, 92, 'Ecobags', '', 'Senac', '20151123150726ecobags-capa-livro.jpg', 'Um registro em fotos da exposição “Eu Não Sou de Plástico“, com sacolas não-descartáveis assinadas por grandes grifes e estilistas.', 1, '2017-07-07 13:12:46', '2017-07-07 13:12:46'),
(3180, 92, 'Pelo mundo da moda', 'Pelo mundo da moda', 'Senac', '201511231507084612-livros-pelo-mundo-da-moda1.jpg', 'O livro conta um pouco da história da moda,  revela não somente os bastidores das grandes criações no exterior como também o perfil de modelos de projeção internacional, além de apresentar uma análise do trabalho e da personalidade de importantes estilistas.', 2, '2017-07-07 13:12:46', '2017-07-07 13:12:46'),
(3181, 145, 'O Biquíni Made In Brazil', 'Lilian Pacce', ' Arte Ensaio', '20160928111150livro-lilian-pacce.jpg', 'O Biquíni Made In Brazil, que fala sobre a história e todas as curiosidades da peça. Com projeto gráfico assinado por Giovanni Bianco, além de mostrar fotos incríveis da evolução do traje de banho.', 0, '2017-07-07 13:14:24', '2017-07-07 13:14:24'),
(3182, 145, 'Pelo Mindo da Moda Criadores Grifes e Modelos ', 'Lilian Pacce ', 'Senac', '20160511093815201511231506344612-livros-pelo-mundo-da-moda1.jpg', 'O livro conta um pouco da história da moda, revela não somente os bastidores das grandes criações no exterior como também o perfil de modelos de projeção internacional , além de apresentar uma análise do trabalho e da personalidade de importante estilstas. ', 1, '2017-07-07 13:14:24', '2017-07-07 13:14:24'),
(3183, 145, 'Eco Bags', 'Lilian Pacce ', 'Senac ', '2016051109460520151123104856ecobags-capa-livro.jpg', 'Um registro em fotos da exposição ¨Eu não sou de Plástico¨, com sacolas não- descartavéis assinadas por grandes grifes e estilistas.', 2, '2017-07-07 13:14:24', '2017-07-07 13:14:24'),
(3216, 207, '50 Anos a Mil ', 'Lobão', 'Nova Fronteira', '20170705161511clobao.jpg', '50 Anos a Mil é um livro autobiográfico sobre a história de vida de Lobão, cantor e compositor de rock brasileiro. O livro foi publicado no final de 2010 e conta as memórias da vida e carreira de Lobão. ', 0, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(3217, 207, 'Manifesto do Nada na Terra do Nunca', 'Lobão', 'Nova Fronteira', '20170705160931clobao.jpg', 'Manifesto do Nada na Terra do Nunca, o autor aborda o "estado de paralisia" em que se encontra o Brasil. ', 1, '2017-07-20 21:46:50', '2017-07-20 21:46:50');
INSERT INTO `portfolio_publicacoes` (`id`, `portfolio_personalidades_id`, `titulo`, `autor`, `editora`, `imagem`, `texto`, `ordem`, `created_at`, `updated_at`) VALUES
(3218, 207, 'Em Busca do Rigor e da Misericórdia - Reflexões de um Ermitão Urbano', 'Lobão', 'Record', '20170705161345clobao.jpg', 'Em uma narrativa poético-político-musical, Lobão conta, passo a passo, em tempo real, toda a trajetória do processo criativo de seu novo disco, O rigor e a misericórdia. ', 2, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(3219, 207, 'Guia Politicamente Incorreto dos Anos 80 pelo Rock', 'Lobão', 'Leya', '20170705161819clobao.jpg', 'Lobão solta o verbo e conta tudo o que você sempre quis saber sobre o rock brasileiro dos anos 80. ', 3, '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(3222, 143, 'Índia: Da miséria à potência', 'Patricia Campos Mello', 'Editora Planeta ', '201707251109372423827.jpg', '', 0, '2017-07-25 14:18:09', '2017-07-25 14:18:09'),
(3223, 143, 'O mundo tem medo da China', 'Patricia Campos Mello', 'Editora Mostarda', '201707251110343185106.jpg', '', 1, '2017-07-25 14:18:09', '2017-07-25 14:18:09');

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_publicidade`
--

CREATE TABLE `portfolio_publicidade` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_personalidades_id` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portfolio_publicidade`
--

INSERT INTO `portfolio_publicidade` (`id`, `portfolio_personalidades_id`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(4294, 24, '20150817132608Campanha_coca cola_Astrid Fontenelle_1.PNG', 0, '2016-03-17 01:34:46', '2016-03-17 01:34:46'),
(4295, 24, '20150817132621Campanha_Astrid Fontenelle_GSK.jpg', 1, '2016-03-17 01:34:46', '2016-03-17 01:34:46'),
(4296, 24, '20150817132626Campanha_Astrid Fontenelle_Kapo.jpg', 2, '2016-03-17 01:34:46', '2016-03-17 01:34:46'),
(4297, 24, '20151027185036fullsizerender-14.jpg', 3, '2016-03-17 01:34:46', '2016-03-17 01:34:46'),
(4298, 24, '20151124111143astrid-fontenelle-2.jpg', 4, '2016-03-17 01:34:46', '2016-03-17 01:34:46'),
(4299, 24, '20151027185453astrid-knorr.jpg', 5, '2016-03-17 01:34:46', '2016-03-17 01:34:46'),
(4453, 71, '20151207025314800x937xgisele-pantene-campaign2pagespeedic-c3oh8v-sih.jpg', 0, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(4454, 71, '20151207030324melissa-capa.jpg', 1, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(4455, 71, '20151130133834rita.jpg', 2, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(4456, 71, '201511301337292013-ainda-melhor.jpg', 3, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(4457, 71, '20151210162017gisele-grandene-em-alta.jpg', 4, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(4458, 71, '20151130134829campanha.jpg', 5, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(4459, 71, '20151130133559sabrina-gasperin-ara-vartanian1.jpg', 6, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(4460, 71, '20151207025359pantene-aboutus-gisele.jpg', 7, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(4461, 71, '20151027184345vult-nails-1.jpg', 8, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(4462, 71, '20151207025340beautitas-pantene-gisele-bundchen-egerie.jpg', 9, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(4463, 71, '20151130133434campanha-amir-slama31.jpg', 10, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(4464, 71, '20151027184302campanha-92-1.png', 11, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(4465, 71, '20151130134228campanha-2.jpg', 12, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(4466, 71, '20151130135238santalollamo-640-2.jpg', 13, '2016-05-04 18:54:29', '2016-05-04 18:54:29'),
(4467, 71, '20151130134230campanha-1.jpg', 14, '2016-05-04 18:54:30', '2016-05-04 18:54:30'),
(4468, 71, '20151130134135campanha.jpg', 15, '2016-05-04 18:54:30', '2016-05-04 18:54:30'),
(4469, 71, '20151130134435campanha.jpg', 16, '2016-05-04 18:54:30', '2016-05-04 18:54:30'),
(4470, 71, '20151130135000campanha.jpg', 17, '2016-05-04 18:54:30', '2016-05-04 18:54:30'),
(4471, 71, '20150821102856Campanha_Duda molinos_1.jpg', 18, '2016-05-04 18:54:30', '2016-05-04 18:54:30'),
(4472, 71, '201511301351382.jpg', 19, '2016-05-04 18:54:30', '2016-05-04 18:54:30'),
(4815, 131, '20151217153524post-3206-1242326188.jpg', 0, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(4816, 131, '20151217152621sky-5.jpg', 1, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(4817, 131, '20151217152540sky-003.jpg', 2, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(4818, 131, '20151217152624sky-3.jpg', 3, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(4819, 131, '20151217152539cenouraebronze04.JPG', 4, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(4820, 131, '20151217152535agua-doce-quem-anuncio-dupla-450x310-outline3.jpg', 5, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(4821, 131, '20151217152536fff.jpg', 6, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(4822, 131, '20151217152548sem-titulo-2.jpg', 7, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(4823, 131, '20151217152553trousseau-02.JPG', 8, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(4824, 131, '20151217152545trousseau-03.JPG', 9, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(4825, 131, '20151217162333publicidade5.jpg', 10, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(4826, 131, '20151217162334publicidade4.jpg', 11, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(4827, 131, '20151217152549seda.JPG', 12, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(4828, 131, '20151217152558valisere-tododia.JPG', 13, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(4829, 131, '20151217162333publicidade2.jpg', 14, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(4830, 131, '20151217152548grendene-claudia-leitte.jpg', 15, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(4831, 131, '20151217152604zz.jpg', 16, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(4832, 131, '20151217162332publicidade3.jpg', 17, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(4833, 131, '20151217152608vivara-com-cristiana-arcangeli.JPG', 18, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(4834, 131, '20151217162332publicidade1.jpg', 19, '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(4899, 152, '20160729165006post-p-nextel.jpg', 0, '2016-12-23 03:25:59', '2016-12-23 03:25:59'),
(4900, 152, '20160729164959p-pedrigree.jpg', 1, '2016-12-23 03:25:59', '2016-12-23 03:25:59'),
(4960, 63, '20160920155609erika-palomino-shopping-pamplona.jpg', 0, '2017-01-27 19:29:19', '2017-01-27 19:29:19'),
(4962, 39, '2015112615334912142262-604428806362658-1995227864-n.jpg', 0, '2017-01-27 21:27:16', '2017-01-27 21:27:16'),
(5171, 42, '20150909135234SITE _Marilia Moreno _ Eletric Sommer 004.png', 0, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(5172, 42, '20150909135053image002 2.jpg', 1, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(5173, 42, '20151026105350marilia-moreno-blue-man.jpg', 2, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(5174, 42, '2015102122193610403473-10202437242851115-1029564437835243418-n-2.jpg', 3, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(5175, 42, '20151026105339marilia-moreno-blue-man-2.jpg', 4, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(5176, 42, '201605311147552.jpg', 5, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(5177, 42, '201605311147551.jpg', 6, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(5178, 54, '2016011216491312.png', 0, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(5179, 54, '2016011216491913.png', 1, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(5180, 54, '201601121649107.png', 2, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(5181, 54, '201601121649119.png', 3, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(5182, 54, '2016011216491716.png', 4, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(5183, 54, '2016011216491311.png', 5, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(5184, 54, '20160112164917electrolux-a.png', 6, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(5185, 54, '20160112164922showstock.png', 7, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(5186, 54, '20151130190645make-b-kit-gloria-coelho.jpg', 8, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(5187, 54, '20151130190618gloria-coelho.jpg', 9, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(5188, 54, '20160112164923verao-2010.png', 10, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(5189, 54, '20160112164921verao-2010-a.png', 11, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(5190, 54, '20160112165129chilli.png', 12, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(5191, 54, '20151029103100gloria-coelho-chilli-beans-linhas-assinadas.jpg', 13, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(5192, 54, '20160112164925electrolux.png', 14, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(5193, 54, '20160112164921inverno-2015.png', 15, '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(5205, 40, '20150909125203Unknown.jpeg', 0, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(5206, 40, '201510271908436ced27bc7ed6b7527b1fa946fbd329a6.jpg', 1, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(5207, 40, '20151027190844fotomulher.jpg', 2, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(5208, 40, '20151027190844fotomulher6.jpg', 3, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(5209, 40, '20151027190844fotomulher5.jpg', 4, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(5210, 40, '20150820145235Campanha_Marcelle Bittar_Arezzo_1.jpg', 5, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(5211, 40, '20161212115310whatsapp-image-2016-12-04-at-095346.jpeg', 6, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(5213, 112, '20160920160217erika-palomino-shopping-pamplona.jpg', 0, '2017-05-17 18:32:22', '2017-05-17 18:32:22'),
(5222, 34, '20150819153749Campanha_Costanza Pascolato_Havaianas_1.JPG', 0, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(5223, 34, '20150819153750Campanha_Costanza Pascolato_Havaianas_2.jpg', 1, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(5224, 34, '20150819153752Campanha_Costanza Pascolato_Chillibeans_2.jpg', 2, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(5225, 34, '20151123212858morena-rosa-campanha-02.jpg', 3, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(5226, 34, '20151123212858morena-rosa-campanha-04.jpg', 4, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(5227, 34, '20161212175521whatsapp-image-2016-12-04-at-095523.jpeg', 5, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(5228, 135, '20160119143222pub1.jpg', 0, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(5229, 135, '20160119143230pub2.jpg', 1, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(5230, 135, '20160119143237pub3.jpg', 2, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(5231, 135, '20160119143243pub4.jpg', 3, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(5232, 135, '20160119143250pub5.jpg', 4, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(5233, 135, '20161220164653whatsapp-image-2016-12-04-at-095523.jpeg', 5, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(5234, 148, '20160530164939alcacuz.jpg', 0, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(5235, 148, '20160530164939bremont.jpg', 1, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(5236, 148, '20160530171843maxior-joias-1.jpg', 2, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(5237, 148, '20160530170103granado.jpg', 3, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(5238, 148, '20160530170358galeria-lafayete.jpg', 4, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(5239, 148, '20160530165105the-rtiz.jpg', 5, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(5240, 148, '20160530165813farfatech.jpg', 6, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(5241, 148, '20160530165516arezzo1.jpg', 7, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(5251, 35, '20161212160326campanha-nextel-cicarelli.jpg', 0, '2017-05-22 19:43:30', '2017-05-22 19:43:30'),
(5264, 101, '2016020115011411953026-1142290889148134-7400579484307281724-n.jpg', 0, '2017-05-26 23:05:38', '2017-05-26 23:05:38'),
(5265, 101, '2016020115011411295699-1089713044405919-6344527965428035387-n.jpg', 1, '2017-05-26 23:05:38', '2017-05-26 23:05:38'),
(5266, 101, '2016020115011411050694-1121129394597617-8470617149845319375-n.jpg', 2, '2017-05-26 23:05:38', '2017-05-26 23:05:38'),
(5279, 30, '20151123205930998007-568828529825255-1283187294-n.jpg', 0, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(5280, 30, '20150819151806publicidade_Carol Scaff_2.jpg', 1, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(5281, 30, '20150819151803publicidade_Carol Scaff_1.jpg', 2, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(5291, 176, '20170321120145unknown.jpeg', 0, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(5292, 47, '20150820154749Campanha+Tiana-Dumenti_Wella-1.jpg', 0, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(5293, 47, '20160725225713image155.jpeg', 1, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(5294, 47, '20160725225713image148.jpeg', 2, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(5295, 47, '20160725225713image149.jpeg', 3, '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(5296, 170, '20161004183908olive-oyl-2.jpg', 0, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(5297, 170, '20161004183909jack-jazz-2007.jpg', 1, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(5298, 170, '20161004183909img-7302baixa.jpg', 2, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(5299, 170, '20161004183909bx-raf25561.jpg', 3, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(5300, 170, '20161004183912folheto20adulto20final-31.JPG', 4, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(5301, 170, '20161004183914leibasica-foto21-00013.jpg', 5, '2017-06-22 20:41:29', '2017-06-22 20:41:29'),
(5313, 205, '201706231229381.jpg', 0, '2017-06-23 15:48:55', '2017-06-23 15:48:55'),
(5341, 70, '20170523164256vanessa-rozan-bolsas.jpg', 0, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(5342, 70, '20170607113341vanessa-rozan-colgate.jpg', 1, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(5343, 70, '20150821102037Campanha_Vanessa Rozan_Mutus_4.jpg', 2, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(5344, 70, '20150821102037Campanha_Vanessa Rozan_Camila Klein_1.jpg', 3, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(5345, 70, '20150821102037Campanha_Vanessa Rozan_Huis Clos_3.jpg', 4, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(5346, 70, '20150821102037Campanha_Vanessa Rozan_Huis Clos_2.jpg', 5, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(5347, 70, '20170519111131vanessa-rozan-preferida-vertical.jpg', 6, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(5348, 70, '20150821102037Campanha_Vanessa Rozan_You_5.jpg', 7, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(5349, 26, '20151023000138macys-camila-daily-pick-sweepstakes.jpeg', 0, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(5350, 26, '20150819143443Campanha_Camila Alves_Datelli.jpg', 1, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(5351, 26, '20151027001532macy-s-promove-evento-com-a-participa-o-da-modelo-brasileira-camila-alves.jpg', 2, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(5352, 26, '20150819143508Campanha_Camila Alves_Macys_2.jpg', 3, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(5353, 26, '20150819143453Campanha_Camila Alves_Hering.jpg', 4, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(5354, 26, '20151023000151camila-alves-for-inc-campaign-image-6-1.jpg', 5, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(5355, 26, '20151023000138camila-alves-for-inc-only-at-macys-2.jpg', 6, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(5356, 26, '20150819143509Campanha_Camila Alves_Macys_4.jpg', 7, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(5357, 49, '20170127111304whatsapp-image-2017-01-27-at-094537.jpeg', 0, '2017-07-07 13:48:50', '2017-07-07 13:48:50'),
(5358, 58, '20151201171729andorinhachef.jpg', 0, '2017-07-20 14:20:45', '2017-07-20 14:20:45'),
(5359, 58, '20151201171717klm-mocotodest.jpg', 1, '2017-07-20 14:20:45', '2017-07-20 14:20:45'),
(5365, 29, '20150923213902Mel Lisboa_MATTEL_C&A (1).jpg', 0, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(5366, 53, '20151006185338images-5.jpeg', 0, '2017-07-25 18:08:40', '2017-07-25 18:08:40'),
(5367, 53, '20151006190016images-9.jpeg', 1, '2017-07-25 18:08:40', '2017-07-25 18:08:40'),
(5368, 53, '20151006190016images-12.jpeg', 2, '2017-07-25 18:08:40', '2017-07-25 18:08:40'),
(5369, 53, '20151006190119images-10.jpeg', 3, '2017-07-25 18:08:40', '2017-07-25 18:08:40'),
(5370, 53, '20170120133101whatsapp-image-2017-01-20-at-131657.jpeg', 4, '2017-07-25 18:08:40', '2017-07-25 18:08:40');

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_redes_sociais`
--

CREATE TABLE `portfolio_redes_sociais` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_personalidades_id` int(10) UNSIGNED NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vimeo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `flickr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pinterest` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gplus` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tumblr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portfolio_redes_sociais`
--

INSERT INTO `portfolio_redes_sociais` (`id`, `portfolio_personalidades_id`, `instagram`, `facebook`, `twitter`, `vimeo`, `youtube`, `linkedin`, `flickr`, `pinterest`, `gplus`, `tumblr`, `created_at`, `updated_at`) VALUES
(1776, 57, 'https://instagram.com/pborgesj', 'https://www.facebook.com/pborgesj', 'https://twitter.com/pauloborges', '', '', '', '', '', '', '', '2015-12-04 17:54:18', '2015-12-04 17:54:18'),
(2232, 51, 'http://instagram.com/pedroandradetv/', 'https://www.facebook.com/spapedro', 'http://twitter.com/pedroandradetv', 'https://vimeo.com/user2516333', '', 'https://www.linkedin.com/profile/view?id=AAkAAAhx_TcBN7H0-tSgEaZRBoBLWFCVDO5TRNo&authType=NAME_SEARCH&authToken=xOeH&locale=pt_BR&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A141688119%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1', '', 'https://br.pinterest.com/pedroandradetv/', 'https://plus.google.com/u/0/109454572071873888989/posts', 'https://www.tumblr.com/search/PEDRO+ANDRADE', '2016-03-07 19:48:12', '2016-03-07 19:48:12'),
(2256, 24, 'https://instagram.com/astridfontenelle', 'https://www.facebook.com/astridfontenelle', 'https://twitter.com/astridfontenell', '', '', '', '', 'https://www.pinterest.com/astridfontenell/', '', '', '2016-03-17 01:34:46', '2016-03-17 01:34:46'),
(2291, 97, 'https://instagram.com/silviapoppovic/', 'https://www.facebook.com/silvia.poppovic?fref=ts', '', '', '', '', '', '', '', '', '2016-03-17 19:59:33', '2016-03-17 19:59:33'),
(2369, 66, 'https://instagram.com/monadorf', 'https://www.facebook.com/mona.dorf?fref=ts', 'https://twitter.com/monadorf', '', 'https://www.youtube.com/user/monadorf10', 'https://www.linkedin.com/profile/view?id=AAkAAAG3L00BSIzTL75pctOgug8_RGxuyogJbNc&authType=NAME_SEARCH&authToken=0o3P&locale=pt_BR&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A28782413%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A14', '', '', 'https://plus.google.com/104155889073190688928/posts', '', '2016-04-18 18:35:56', '2016-04-18 18:35:56'),
(2379, 96, 'https://instagram.com/dudamolinos', 'https://www.facebook.com/duda.molinos.14', '', '', '', '', '', 'https://br.pinterest.com/dmolinos/', '', '', '2016-04-25 19:37:35', '2016-04-25 19:37:35'),
(2394, 61, 'https://www.instagram.com/josimarmelojornalista/?hl=pt-br', 'https://www.facebook.com/josimar.melo.14?fref=ts', 'https://twitter.com/josimarm', '', '', '', '', '', '', '', '2016-04-26 21:00:21', '2016-04-26 21:00:21'),
(2406, 142, 'https://www.instagram.com/chefandrefalcao/?hl=pt-br', 'https://www.facebook.com/chefandrefalcao/', '', '', '', '', '', '', '', '', '2016-05-04 16:19:50', '2016-05-04 16:19:50'),
(2407, 71, 'https://instagram.com/dudamolinos', 'https://www.facebook.com/duda.molinos.14', '', '', '', '', '', 'https://br.pinterest.com/dmolinos/', '', '', '2016-05-04 18:54:30', '2016-05-04 18:54:30'),
(2492, 139, 'https://www.instagram.com/vicmeirellesoficial/', '', '', '', '', '', '', '', '', '', '2016-05-24 18:09:57', '2016-05-24 18:09:57'),
(2595, 141, 'https://www.instagram.com/leaoserva/', 'https://www.facebook.com/malditosfios', 'https://twitter.com/leaoserva', '', '', '', '', '', '', '', '2016-06-23 14:55:25', '2016-06-23 14:55:25'),
(2665, 32, 'https://www.instagram.com/carlalamarca/		', 'https://www.facebook.com/carla.lamarca?fref=ts', 'https://twitter.com/carlalamarca', 'https://vimeo.com/carlalamarca', 'https://www.youtube.com/channel/UC_brncO5oLKSzPZMxBn5Y9Q', 'https://www.linkedin.com/profile/view?id=AAkAAAKEsU0BwMVtiPO2loOw53iYsbuRxzYy_8E&authType=NAME_SEARCH&authToken=DGQX&locale=pt_BR&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A42250573%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A14', '', '', 'https://plus.google.com/112344636727410598198/posts', '', '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(2684, 144, 'https://www.instagram.com/dlvieira82/', '', '', '', '', '', '', '', '', '', '2016-07-29 16:37:10', '2016-07-29 16:37:10'),
(2695, 73, 'https://instagram.com/marcelosommer/', 'https://www.facebook.com/estilistamarcelosommer/', 'https://twitter.com/sommermarcelo', '', '', 'https://www.linkedin.com/pub/marcelo-sommer/28/7aa/807', '', 'https://br.pinterest.com/marcelosommer/', 'https://plus.google.com/u/0/102949249591951450940/about', 'https://www.tumblr.com/search/MARCELO+SOMMER', '2016-07-29 23:01:36', '2016-07-29 23:01:36'),
(2753, 91, 'https://instagram.com/erikapalomino', 'https://www.facebook.com/erika.palomino.188', 'https://twitter.com/erikapalomino', '', '', '', '', '', '', '', '2016-09-20 19:38:47', '2016-09-20 19:38:47'),
(2842, 82, 'https://instagram.com/bgancia', 'https://www.facebook.com/barbaragancia', 'https://twitter.com/barbaragancia', '', '', '', '', '', '', '', '2016-10-13 13:29:50', '2016-10-13 13:29:50'),
(2905, 131, 'https://www.instagram.com/andreschiliro/', 'https://www.facebook.com/pages/Studio-André-Schiliró/219112674812069?fref=ts', 'https://twitter.com/andreschiliro', '', '', '', '', '', 'https://plus.google.com/107455511237314223333', 'https://www.tumblr.com/tagged/andre-schiliro', '2016-11-13 23:42:09', '2016-11-13 23:42:09'),
(2919, 124, '', 'https://www.facebook.com/luciana.l.moraes.9', 'https://twitter.com/LuLiviero', '', '', '', '', 'https://br.pinterest.com/lucianaliviero/', '', '', '2016-11-16 17:43:37', '2016-11-16 17:43:37'),
(2943, 175, 'https://www.instagram.com/sergiozobaran/', 'https://www.facebook.com/sergio.zobaran', '', '', '', '', '', '', '', '', '2016-12-07 19:28:50', '2016-12-07 19:28:50'),
(2994, 152, 'https://www.instagram.com/acredita_bonita/', 'https://www.facebook.com/blogacreditabonita', 'https://twitter.com/Acredita_Bonita', '', 'https://www.youtube.com/channel/UCgEq4wNeARqPELUPTMD1Rhw', '', '', '', '', '', '2016-12-23 03:26:00', '2016-12-23 03:26:00'),
(3001, 165, 'https://www.instagram.com/sergioaguiar07/', 'https://www.facebook.com/sergioaguiargn/', 'https://twitter.com/sergioaguiar07', '', '', '', '', '', 'https://plus.google.com/103283040921401139727', '', '2017-01-16 14:49:39', '2017-01-16 14:49:39'),
(3012, 138, 'https://www.instagram.com/zucatelli/?hl=pt', 'https://pt-br.facebook.com/celsozucatelli/', 'https://twitter.com/zucatelli', '', '', '', '', '', 'https://plus.google.com/108426432523504971950', 'https://www.tumblr.com/search/celso%20zucatelli', '2017-01-19 19:36:49', '2017-01-19 19:36:49'),
(3043, 146, 'https://www.instagram.com/zucatelli/?hl=pt', 'https://www.facebook.com/celsozucatelli/', 'https://twitter.com/zucatelli', '', '', '', '', '', 'https://plus.google.com/108426432523504971950/posts', 'https://www.tumblr.com/search/celso%20zucatelli', '2017-01-27 18:53:38', '2017-01-27 18:53:38'),
(3045, 90, 'https://instagram.com/costanzapascolatos2g/', 'https://pt-br.facebook.com/costanzapascolato', '', '', '', '', '', '', '', '', '2017-01-27 18:57:06', '2017-01-27 18:57:06'),
(3056, 63, 'https://instagram.com/erikapalomino', 'https://www.facebook.com/erika.palomino.188', 'https://twitter.com/erikapalomino', '', '', 'https://www.linkedin.com/profile/view?id=ADEAAAVy3tcBYO3VBLxmDSSfDir3XoIl_0xZnSk&authType=NAME_SEARCH&authToken=8Pl6&locale=en_US&srchid=988610261448979343067&srchindex=1&srchtotal=60&trk=vsrp_people_res_name&trkInfo=VSRPsearchId%3A988610261448979343067%2', '', 'https://br.pinterest.com/supermafca/', '', '', '2017-01-27 19:29:19', '2017-01-27 19:29:19'),
(3062, 173, '', 'https://www.facebook.com/jamilchade/', 'https://twitter.com/jamilchade', '', '', '', '', '', '', '', '2017-01-27 20:43:07', '2017-01-27 20:43:07'),
(3066, 99, 'https://www.instagram.com/josimarmelojornalista/?hl=pt-br', 'https://www.facebook.com/josimar.melo.14?fref=ts', 'https://twitter.com/josimarm', '', '', '', '', '', '', '', '2017-01-27 21:06:29', '2017-01-27 21:06:29'),
(3068, 140, 'https://www.instagram.com/leaoserva/', 'https://www.facebook.com/malditosfios', 'https://twitter.com/leaoserva', '', '', '', '', '', '', '', '2017-01-27 21:07:56', '2017-01-27 21:07:56'),
(3074, 100, 'https://instagram.com/luitha/?hl=pt-br', 'https://www.facebook.com/luitha.miraglia?fref=nf', 'https://twitter.com/luitha', '', 'https://www.youtube.com/channel/UCmB_YbygPlnz1R9wbR2LnHQ', '', '', 'https://br.pinterest.com/luitha/', 'https://plus.google.com/114117004186309469408/about', '', '2017-01-27 21:22:59', '2017-01-27 21:22:59'),
(3075, 39, 'https://instagram.com/luizealtenhofen', 'https://pt-br.facebook.com/luizealtenhofenoficial', 'https://twitter.com/altenhofenluize', '', '', '', '', '', 'https://plus.google.com/112707120139494848298/posts?pid=5832235944922207042&oid=112707120139494848298', 'https://www.tumblr.com/search/LUIZE+ALTENHOFEN', '2017-01-27 21:27:16', '2017-01-27 21:27:16'),
(3082, 137, 'https://www.instagram.com/marilubeer/', 'https://pt-br.facebook.com/marilu.beer', '', '', '', '', '', '', '', '', '2017-01-27 21:35:23', '2017-01-27 21:35:23'),
(3084, 98, 'https://instagram.com/monicawaldvogel', 'https://www.facebook.com/monica.waldvogel1?fref=ts', 'https://twitter.com/monicawaldvogel', '', '', '', '', '', '', '', '2017-01-27 21:45:48', '2017-01-27 21:45:48'),
(3087, 87, 'http://instagram.com/pedro_lourenco', 'http://pt-br.facebook.com/pedrolourenco.brand', 'http://twitter.com/pedro__lourenco', '', '', '', '', '', '', '', '2017-01-27 21:55:06', '2017-01-27 21:55:06'),
(3088, 93, 'https://www.instagram.com/reginaguerreiro1/', 'https://www.facebook.com/regina.guerreiro.568?fref=ts', 'http://twitter.com/reginaguerreiro', '', '', '', '', '', '', '', '2017-01-27 21:55:39', '2017-01-27 21:55:39'),
(3089, 78, 'https://www.instagram.com/reginaguerreiro1/', 'https://www.facebook.com/regina.guerreiro.568?fref=ts', 'https://twitter.com/reginaguerreiro', '', '', '', '', '', '', '', '2017-01-27 21:57:58', '2017-01-27 21:57:58'),
(3091, 68, 'https://instagram.com/rodrigobocardi/', 'https://www.facebook.com/rodrigo.bocardi?fref=ts', 'https://twitter.com/rodrigobocardi', '', '', '', '', '', '', '', '2017-01-27 22:14:14', '2017-01-27 22:14:14'),
(3097, 84, 'https://instagram.com/silviapoppovic/', 'https://www.facebook.com/silvia.poppovic?fref=ts', 'https://twitter.com/PoppovicSilvia', '', '', 'https://www.linkedin.com/profile/view?id=AAkAAATpwpkBBGqNdx-CJND6Cfg_RWHD0M_lVVc&authType=NAME_SEARCH&authToken=UcNe&locale=pt_BR&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A82428569%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A14', '', 'https://br.pinterest.com/fivoca/', '', '', '2017-01-27 22:34:16', '2017-01-27 22:34:16'),
(3098, 44, 'https://instagram.com/silviapoppovic/', 'https://www.facebook.com/silvia.poppovic?fref=ts', '', '', '', '', '', '', '', '', '2017-01-27 22:38:07', '2017-01-27 22:38:07'),
(3100, 31, 'https://instagram.com/bgancia', 'https://www.facebook.com/barbaragancia', 'https://twitter.com/barbaragancia', '', '', '', '', '', '', '', '2017-01-27 22:52:05', '2017-01-27 22:52:05'),
(3101, 136, 'https://www.instagram.com/marilubeer/', 'https://pt-br.facebook.com/marilu.beer', '', '', '', '', '', '', '', '', '2017-01-27 22:55:14', '2017-01-27 22:55:14'),
(3113, 65, 'https://instagram.com/gugachacra', 'www.facebook.com/gugachacra', 'http://twitter.com/gugachacra', '', '', '', '', '', 'https://plus.google.com/108194815524464906632/posts', '', '2017-01-27 23:11:20', '2017-01-27 23:11:20'),
(3115, 95, 'https://instagram.com/vanessarozan', 'https://www.facebook.com/vanessarozanoficial?fref=ts', 'https://twitter.com/vanessarozan', '', 'https://www.youtube.com/channel/UCqP4RwhiD_vHuHr8UtDrL5g', '', '', '', 'https://plus.google.com/u/0/114831496368132380796/posts', '', '2017-01-27 23:13:36', '2017-01-27 23:13:36'),
(3116, 64, 'https://instagram.com/felipesuhre', 'https://www.facebook.com/felipe.suhre', 'https://twitter.com/felipesuhre', '', '', '', '', '', '', '', '2017-01-27 23:21:56', '2017-01-27 23:21:56'),
(3117, 155, 'https://www.instagram.com/izabellacamargooficial/', '', 'https://twitter.com/IzabellaCamargo', '', '', '', '', '', '', '', '2017-01-30 13:12:34', '2017-01-30 13:12:34'),
(3141, 177, 'https://www.instagram.com/tognolliclaudio/', 'https://www.facebook.com/claudio.tognolli?ref=ts&fref=ts', 'https://twitter.com/claudiotognolli?lang=pt', '', 'https://www.youtube.com/user/tognolli', '', '', '', '', '', '2017-02-06 19:13:34', '2017-02-06 19:13:34'),
(3168, 154, 'https://www.instagram.com/ronaldrios/', 'https://www.facebook.com/ronaldrios/', 'https://twitter.com/ronaldrios?lang=pt', '', 'https://www.youtube.com/user/Apartamento58', 'https://www.linkedin.com/in/ronald-rios-6326172b', '', 'https://br.pinterest.com/ronaldriosgamer/', 'https://plus.google.com/+RonaldRios/posts', 'https://www.tumblr.com/tagged/ronald-rios', '2017-02-09 18:24:01', '2017-02-09 18:24:01'),
(3218, 181, 'https://www.instagram.com/thiagoamoral/', 'https://www.facebook.com/tcamaral?fref=ts', '', '', 'https://www.youtube.com/user/tcamaral25', '', '', '', '', '', '2017-02-13 21:13:22', '2017-02-13 21:13:22'),
(3239, 23, 'https://instagram.com/amandaramalhos', 'https://www.facebook.com/AmandaRamalhos', 'https://twitter.com/amandaramalhos', '', 'https://www.youtube.com/channel/UCdLJ65_p8R5psz49sQjEhKw', '', '', 'https://br.pinterest.com/amandaramal10/pins/', 'https://plus.google.com/+AmandaRamalhos/posts', '', '2017-02-16 19:43:48', '2017-02-16 19:43:48'),
(3240, 103, 'https://www.instagram.com/amandaramalho/', 'https://www.facebook.com/AmandaRamalhos', 'https://twitter.com/amandaramalhos', '', 'https://www.youtube.com/channel/UCdLJ65_p8R5psz49sQjEhKw', '', '', 'https://br.pinterest.com/amandaramal10/pins/', 'https://plus.google.com/+AmandaRamalhos/posts', '', '2017-02-16 19:44:16', '2017-02-16 19:44:16'),
(3260, 129, '', 'https://www.facebook.com/lucia.bronstein/photos?lst=772830578%3A1382173061%3A1486388873&source_ref=pb_friends_tl', '', '', '', '', '', '', '', '', '2017-02-20 03:13:01', '2017-02-20 03:13:01'),
(3264, 179, 'https://www.instagram.com/veronicavalois/', 'https://www.facebook.com/veronicavalois/?fref=ts', 'https://twitter.com/veronicavalois', '', 'https://www.youtube.com/user/veronicavalois', '', '', '', '', '', '2017-02-20 03:53:13', '2017-02-20 03:53:13'),
(3267, 38, 'https://instagram.com/ligiatv', 'https://www.facebook.com/L%C3%ADgia-Mendes-116639891751614/timeline/', 'https://twitter.com/ligiatv', '', '', '', '', '', '', '', '2017-02-21 13:41:12', '2017-02-21 13:41:12'),
(3268, 76, 'https://instagram.com/ligiatv', 'https://www.facebook.com/L%C3%ADgia-Mendes-116639891751614/timeline/', 'https://twitter.com/ligiatv', '', '', '', '', '', '', '', '2017-02-21 13:49:30', '2017-02-21 13:49:30'),
(3280, 178, 'https://www.instagram.com/alexandre.schumacher.mentoria/', 'https://www.facebook.com/alexandre.schumacher.7', 'https://twitter.com/aschumacher74?lang=pt', '', '', '', '', '', '', '', '2017-03-08 02:55:15', '2017-03-08 02:55:15'),
(3289, 156, 'https://www.instagram.com/korgut/?hl=pt-br', 'https://www.instagram.com/korgut/?hl=pt-br', 'https://twitter.com/sabrinakorgut', '', '', '', '', '', '', '', '2017-03-10 21:35:31', '2017-03-10 21:35:31'),
(3298, 19, 'https://www.instagram.com/cassioscapin/', 'https://www.facebook.com/cassio.scapin', 'https://twitter.com/cassio_scapin', '', '', '', '', '', '', '', '2017-03-14 20:49:42', '2017-03-14 20:49:42'),
(3326, 151, 'https://www.instagram.com/gustavohaddad/', 'https://www.facebook.com/gustavo.haddad?fref=ts', 'https://twitter.com/haddadg', '', 'https://www.youtube.com/user/ghaddad13', '', '', '', '', '', '2017-03-21 18:21:22', '2017-03-21 18:21:22'),
(3346, 171, 'https://www.instagram.com/janarosa/', 'https://www.facebook.com/janarosarealoficial/', 'https://twitter.com/janessacamargo?lang=pt', '', 'https://www.youtube.com/user/janessacamargo', '', '', 'https://br.pinterest.com/janarosa/', 'https://plus.google.com/+JanaRosa', 'http://janarosa.tumblr.com/page/357', '2017-03-30 14:43:38', '2017-03-30 14:43:38'),
(3379, 159, 'https://www.instagram.com/aramiro_oficial', 'https://www.facebook.com/aramiro.oficial', 'https://twitter.com/andreramiroo', '', 'https://www.youtube.com/channel/UC1q3cQxf8TsXxhumRTSw9Jg', '', '', '', '', '', '2017-04-13 20:45:55', '2017-04-13 20:45:55'),
(3387, 167, 'https://www.instagram.com/cesarmellooficial/', 'https://www.facebook.com/cesar.mello.54', '', '', '', '', '', '', '', '', '2017-04-18 21:39:14', '2017-04-18 21:39:14'),
(3402, 199, '', '', 'https://twitter.com/gcamarotti', '', '', '', '', '', '', '', '2017-04-24 21:50:28', '2017-04-24 21:50:28'),
(3403, 104, 'https://instagram.com/bgancia/', 'https://www.facebook.com/barbaragancia', 'https://twitter.com/barbaragancia', '', '', '', '', '', '', '', '2017-04-26 17:36:35', '2017-04-26 17:36:35'),
(3408, 107, 'https://instagram.com/dudubertholini/			', 'https://www.facebook.com/Dudu-Bertholini-130161110400866/timeline/						', 'https://twitter.com/bertholinidudu', '', '', '', '', 'https://br.pinterest.com/dbertholini/', 'https://plus.google.com/u/0/104742191701890602616/posts', '', '2017-04-27 19:20:24', '2017-04-27 19:20:24'),
(3411, 105, 'https://instagram.com/marcelosommer/', 'https://www.facebook.com/estilistamarcelosommer/', 'https://twitter.com/sommermarcelo', '', '', 'https://www.linkedin.com/pub/marcelo-sommer/28/7aa/807', '', 'https://plus.google.com/u/0/102949249591951450940/about', 'https://plus.google.com/u/0/102949249591951450940/about', 'https://www.tumblr.com/search/MARCELO+SOMMER', '2017-04-27 19:25:53', '2017-04-27 19:25:53'),
(3412, 108, 'https://www.instagram.com/josimarmelojornalista/?hl=pt-br', 'https://www.facebook.com/josimar.melo.14?fref=ts', 'https://twitter.com/josimarm', '', '', '', '', '', '', '', '2017-04-27 19:34:03', '2017-04-27 19:34:03'),
(3414, 132, 'https://www.instagram.com/sergiomenezes____/', '', '', '', '', '', '', '', '', '', '2017-05-02 20:46:33', '2017-05-02 20:46:33'),
(3428, 42, 'https://www.instagram.com/mariliamorenoreal/', 'https://www.facebook.com/Marilia-Moreno-93772959330/timeline/', 'https://twitter.com/mariliamoreno11', '', 'https://www.youtube.com/user/Mari11Lia', '', '', '', 'https://plus.google.com/105027274606142063918/posts', '', '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(3429, 54, 'https://instagram.com/gloriacoelho', 'https://www.facebook.com/pages/Gloria-Coelho/173582902677125', 'https://twitter.com/gloriacoelho', '', '', '', '', '', '', '', '2017-05-05 20:40:59', '2017-05-05 20:40:59'),
(3430, 86, 'https://instagram.com/gloriacoelho', 'https://www.facebook.com/pages/Gloria-Coelho/173582902677125', 'https://twitter.com/gloriacoelho', '', '', '', '', '', '', '', '2017-05-05 20:41:29', '2017-05-05 20:41:29'),
(3433, 188, 'https://www.instagram.com/andrelima_br/', 'https://www.facebook.com/andre.lima.102', '', '', '', '', '', '', '', '', '2017-05-11 13:50:28', '2017-05-11 13:50:28'),
(3439, 126, 'https://www.instagram.com/edgardpiccoli/', 'https://www.facebook.com/edgardpiccoli?fref=ts', 'https://twitter.com/edgard_piccoli', '', '', '', '', '', '', '', '2017-05-12 17:15:26', '2017-05-12 17:15:26'),
(3445, 149, 'https://www.instagram.com/saulomeneghettioficial/', 'https://www.facebook.com/saulomeneghettiartista/', 'https://twitter.com/saulomeneghetti', '', 'https://www.youtube.com/channel/UCbSxCXOcOFo7GUHrsvSqhZg', '', '', '', '', '', '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(3447, 40, 'https://www.instagram.com/marcellebittar/', 'https://www.facebook.com/marcelle.bittar', 'https://twitter.com/marcellebittar', '', '', '', '', '', '', '', '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(3451, 112, 'https://instagram.com/erikapalomino/', 'https://www.facebook.com/erika.palomino.188', 'https://twitter.com/erikapalomino', '', '', '', '', '', '', '', '2017-05-17 18:32:22', '2017-05-17 18:32:22'),
(3452, 153, 'https://imgwonders.com/user/fabioventuraoficial/2672436/', '', 'https://mobile.twitter.com/musicventura', '', '', '', '', '', '', '', '2017-05-17 18:39:39', '2017-05-17 18:39:39'),
(3454, 75, 'https://instagram.com/bgancia/', 'https://www.facebook.com/barbaragancia', 'https://twitter.com/barbaragancia', '', '', '', '', '', '', '', '2017-05-17 18:47:46', '2017-05-17 18:47:46'),
(3455, 79, 'https://instagram.com/marciobuzina/', 'https://www.facebook.com/marcio.silva.165685?fref=ts', 'https://mobile.twitter.com/MarciojaSilva/tweets', '', '', '', '', '', '', '', '2017-05-17 18:49:24', '2017-05-17 18:49:24'),
(3456, 60, 'https://instagram.com/marciobuzina/', 'https://www.facebook.com/marcio.silva.165685?fref=ts', 'https://mobile.twitter.com/MarciojaSilva/tweets', '', '', '', '', '', '', '', '2017-05-17 18:51:50', '2017-05-17 18:51:50'),
(3457, 111, 'https://instagram.com/marciobuzina/', 'https://www.facebook.com/marcio.silva.165685?fref=ts', 'https://mobile.twitter.com/MarciojaSilva/tweets', '', '', '', '', '', '', '', '2017-05-17 18:52:48', '2017-05-17 18:52:48'),
(3458, 125, '', 'https://www.facebook.com/silvia.poppovic?fref=ts', 'https://twitter.com/PoppovicSilvia', '', '', '', '', '', '', '', '2017-05-17 18:55:10', '2017-05-17 18:55:10'),
(3460, 34, 'https://instagram.com/costanzapascolatos2g/', 'https://pt-br.facebook.com/costanzapascolato', '', '', '', '', '', '', '', '', '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(3461, 135, 'https://instagram.com/costanzapascolatos2g/', 'https://pt-br.facebook.com/costanzapascolato', '', '', '', '', '', '', '', '', '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(3476, 148, 'https://www.instagram.com/consueloblocker/', 'https://www.facebook.com/Consueloblog/?pnref=lhc', 'https://twitter.com/consueloblocker?lang=pt', '', 'https://www.youtube.com/channel/UCpXRKGDDSw6m4u9oh-vPd2w', '', '', 'https://it.pinterest.com/consueloblocker/', 'https://plus.google.com/+Consueloblog_moda_estilo_afins/posts', '', '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(3479, 150, 'https://www.instagram.com/pedrobosnich/', 'https://www.facebook.com/pedrobosnich.ator/', 'https://twitter.com/pedrobosnich', '', '', '', '', '', '', '', '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(3483, 102, 'https://instagram.com/lilianpacce/', 'https://www.facebook.com/lilianpacce', 'https://twitter.com/LilianPacce', '', 'https://www.youtube.com/user/sitelilianpacce', '', '', 'https://br.pinterest.com/lilianpacce/', 'https://plus.google.com/+lilianpacce/posts', '', '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(3487, 186, 'https://www.instagram.com/bethylagardere/', '', 'https://twitter.com/bethylagardere', '', '', '', '', '', '', '', '2017-05-22 18:19:01', '2017-05-22 18:19:01'),
(3488, 191, 'https://www.instagram.com/criscarniato/?hl=pt-br', 'https://pt-br.facebook.com/cris.carniato', 'https://twitter.com/criscarniato', '', '', '', '', '', '', '', '2017-05-22 19:17:48', '2017-05-22 19:17:48'),
(3491, 35, 'https://instagram.com/daniellacicarelli/', '', '', '', '', '', '', '', '', '', '2017-05-22 19:43:30', '2017-05-22 19:43:30'),
(3501, 192, 'https://www.instagram.com/vplacca/', '', '', '', '', '', '', '', '', '', '2017-05-25 21:06:42', '2017-05-25 21:06:42'),
(3502, 72, 'https://instagram.com/mrosenbaum', 'https://pt-br.facebook.com/marcelorosenbaum', 'https://twitter.com/casarosenbaum', '', '', '', '', '', '', '', '2017-05-26 13:49:46', '2017-05-26 13:49:46'),
(3503, 88, 'https://instagram.com/mrosenbaum', 'https://pt-br.facebook.com/marcelorosenbaum', 'https://twitter.com/casarosenbaum', '', '', '', '', 'https://br.pinterest.com/marcelorosenbau/', '', '', '2017-05-26 13:50:03', '2017-05-26 13:50:03'),
(3512, 101, 'https://instagram.com/tuti_muller/', 'https://www.facebook.com/TutiMuller?fref=ts', 'https://twitter.com/tutimuller', 'https://vimeo.com/channels/tutimuller', 'https://www.youtube.com/user/tutimullersantos', '', '', '', '', '', '2017-05-26 23:05:38', '2017-05-26 23:05:38'),
(3518, 128, 'https://www.instagram.com/maraluquet/', 'https://www.facebook.com/letraselucros/?fref=ts', 'https://twitter.com/maraluquet', '', '', 'https://www.linkedin.com/in/mara-luquet-a479001', '', '', ' https://plus.google.com/114110811935052605444/posts', '', '2017-05-29 17:29:27', '2017-05-29 17:29:27'),
(3519, 134, 'https://www.instagram.com/maraluquet/', 'https://www.facebook.com/letraselucros/?fref=ts', 'https://twitter.com/maraluquet', '', '', 'https://www.linkedin.com/in/mara-luquet-a479001', '', '', ' https://plus.google.com/114110811935052605444/posts', '', '2017-05-29 17:29:44', '2017-05-29 17:29:44'),
(3529, 160, 'https://instagram.com/brunamiglioranza/', 'https://www.facebook.com/bruna.miglioranza', '', 'https://vimeo.com/user32991972', '', '', '', '', '', '', '2017-05-30 18:49:42', '2017-05-30 18:49:42'),
(3532, 30, 'https://www.instagram.com/carolscaff/', 'https://www.facebook.com/cascaff', '', '', '', '', '', '', '', '', '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(3533, 172, 'https://www.instagram.com/jeronimo_martins/', 'https://www.facebook.com/jeronimo.martins.7', '', '', '', '', '', '', '', '', '2017-05-31 19:26:16', '2017-05-31 19:26:16'),
(3539, 161, 'https://www.instagram.com/vandresilveira/', 'https://www.facebook.com/vandre.silveira', '', '', 'https://www.youtube.com/watch?v=TASAZ1bbNP8', '', '', '', '', '', '2017-06-12 13:11:20', '2017-06-12 13:11:20'),
(3542, 176, 'https://www.instagram.com/f.a.b.i.a.n.a.g.o.m.e.s/', 'https://www.facebook.com/fabiana.defatima.9', 'https://twitter.com/mac_fabiana_g?lang=pt', '', 'https://www.youtube.com/channel/UCqP4RwhiD_vHuHr8UtDrL5g', '', '', '', '', '', '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(3554, 182, 'https://www.instagram.com/marjoriegerardi/', 'https://www.facebook.com/marjoriegerardiatriz', 'https://twitter.com/marjoriegerardi', 'https://vimeo.com/user26743568', '', '', '', 'https://br.pinterest.com/marjoriegerardi/', '', '', '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(3560, 47, 'https://instagram.com/tatianadumenti', 'https://www.facebook.com/Tati-Du-452338578166122/timeline/', 'https://twitter.com/tatianadumenti', '', '', '', '', '', '', '', '2017-06-20 17:49:31', '2017-06-20 17:49:31'),
(3563, 197, 'http://www.imgrum.org/user/anacarolina.godoy_/1658108529', 'https://www.facebook.com/ana.c.godoy.7', '', '', 'https://www.youtube.com/watch?v=DH0psPH9miw', '', '', '', '', '', '2017-06-22 17:15:36', '2017-06-22 17:15:36'),
(3564, 170, 'https://www.instagram.com/omarbergeabeautyart/', '', '', '', '', '', '', '', '', '', '2017-06-22 20:41:30', '2017-06-22 20:41:30'),
(3567, 204, 'https://www.instagram.com/danisiwek/', '', '', '', '', '', '', '', '', '', '2017-06-23 14:37:06', '2017-06-23 14:37:06'),
(3568, 187, 'https://www.instagram.com/didicouto/?hl=pt-br', 'https://www.facebook.com/adriana.couto.7', 'https://twitter.com/coutodidi', '', '', '', '', '', '', '', '2017-06-23 15:01:13', '2017-06-23 15:01:13'),
(3569, 201, 'https://www.instagram.com/andrediasoficial/?hl=pt', 'https://www.facebook.com/AndreDiasOficial', 'https://twitter.com/andre_oficialfc', '', '', '', '', '', '', '', '2017-06-23 15:02:22', '2017-06-23 15:02:22'),
(3570, 56, 'http://instagram.com/pedro_lourenco', 'http://pt-br.facebook.com/pedrolourenco.brand', 'http://twitter.com/pedro__lourenco', '', '', '', '', '', '', '', '2017-06-23 15:03:27', '2017-06-23 15:03:27'),
(3572, 190, 'https://www.instagram.com/walcyrcarrasco/', 'https://www.facebook.com/walcyr.carrasco.5', 'https://twitter.com/walcyrcarrasco', '', '', '', '', '', '', '', '2017-06-23 15:05:26', '2017-06-23 15:05:26'),
(3575, 196, 'https://www.instagram.com/vidotticarol/', 'https://www.facebook.com/carol.vidotti', '', '', 'https://www.youtube.com/watch?v=qoBPszc8WPY', '', '', '', '', '', '2017-06-23 15:33:38', '2017-06-23 15:33:38'),
(3577, 205, '', 'https://www.facebook.com/ricardopepeperfil', '', '', '', '', '', '', '', '', '2017-06-23 15:48:55', '2017-06-23 15:48:55'),
(3587, 157, 'https://www.instagram.com/ekrominski/?hl=pt-br', 'https://www.facebook.com/erickkrominski', 'https://twitter.com/ekrominski?lang=pt', '', 'https://www.youtube.com/user/mtointeressante', 'https://www.linkedin.com/in/ekrominski', '', 'https://br.pinterest.com/mtointeressante/', 'https://plus.google.com/+ErickKrominski/posts', 'https://www.tumblr.com/search/erick%20krominski', '2017-06-26 13:48:53', '2017-06-26 13:48:53'),
(3589, 45, 'https://instagram.com/renatafan', 'https://www.facebook.com/renatafan', 'https://twitter.com/renatabfan', '', '', '', '', '', 'https://plus.google.com/106332353871004794876/about', '', '2017-06-26 17:06:04', '2017-06-26 17:06:04'),
(3592, 158, 'https://www.instagram.com/ekrominski/?hl=pt-br', 'https://www.facebook.com/erickkrominski', 'https://twitter.com/ekrominski?lang=pt', '', 'https://www.youtube.com/user/mtointeressante', 'https://www.linkedin.com/in/ekrominski', '', 'https://br.pinterest.com/mtointeressante/', 'https://plus.google.com/+ErickKrominski/posts', 'https://www.tumblr.com/search/erick%20krominski', '2017-06-28 18:48:51', '2017-06-28 18:48:51'),
(3596, 33, 'https://instagram.com/chiaragadaleta', 'https://www.facebook.com/pages/Eco-Era/364025713674736', 'https://twitter.com/chiaragadaleta', '', '', 'https://www.linkedin.com/profile/view?id=ADEAABUZtbIBOeAMkAYOlVSWYkzoe8d_WM9Q-sc&authType=NAME_SEARCH&authToken=UCG1&locale=en_US&srchid=988610261447164450421&srchindex=1&srchtotal=3&trk=vsrp_people_res_name&trkInfo=VSRPsearchId%3A988610261447164450421%2C', '', 'https://br.pinterest.com/specoera/', 'https://plus.google.com/118132999826522107660/posts', '', '2017-06-30 13:37:53', '2017-06-30 13:37:53'),
(3602, 185, 'https://www.instagram.com/renatavilelaatriz/', 'https://www.facebook.com/renata.vilela.7?fref=ts', '', '', '', '', '', '', '', '', '2017-06-30 20:50:49', '2017-06-30 20:50:49'),
(3625, 193, 'https://www.instagram.com/mariazildabethlem/', 'https://www.facebook.com/mzbethlem?fref=ts', '', '', '', '', '', '', '', '', '2017-07-05 23:11:29', '2017-07-05 23:11:29'),
(3632, 55, 'https://instagram.com/lilianpacce', 'https://pt-br.facebook.com/lilianpacce', 'https://twitter.com/lilianpacce', '', 'https://www.youtube.com/user/sitelilianpacce', '', '', 'https://br.pinterest.com/lilianpacce/', 'https://plus.google.com/+lilianpacce/posts', '', '2017-07-07 13:10:58', '2017-07-07 13:10:58'),
(3633, 92, 'https://instagram.com/lilianpacce/', 'https://pt-br.facebook.com/lilianpacce', 'https://twitter.com/lilianpacce', '', 'https://www.youtube.com/user/sitelilianpacce', '', '', 'https://br.pinterest.com/lilianpacce/', 'https://plus.google.com/+lilianpacce/posts', '', '2017-07-07 13:12:46', '2017-07-07 13:12:46'),
(3634, 145, 'https://www.instagram.com/lilianpacce/', 'https://www.facebook.com/lilianpacce', 'https://twitter.com/lilianpacce', '', 'https://www.youtube.com/user/sitelilianpacce', '', '', 'https://br.pinterest.com/lilianpacce/', 'https://plus.google.com/+lilianpacce/posts', '', '2017-07-07 13:14:24', '2017-07-07 13:14:24'),
(3635, 70, 'https://instagram.com/vanessarozan/', 'https://www.facebook.com/vanessarozanoficial?fref=ts', 'https://twitter.com/vanessarozan', '', 'https://www.youtube.com/channel/UCqP4RwhiD_vHuHr8UtDrL5g', '', '', 'https://br.pinterest.com/vanessarozan/', 'https://plus.google.com/u/0/114831496368132380796/posts', '', '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(3636, 26, 'https://instagram.com/iamcamilaalves', 'https://www.facebook.com/iamcamilaalves', 'https://twitter.com/iamcamilaalves', '', '', '', '', '', '', '', '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(3637, 49, 'https://instagram.com/andrevasco', 'https://www.facebook.com/andrevasco.av', 'https://twitter.com/andrevasco', '', 'https://www.youtube.com/channel/UCgy82jFAe1slrYwbjM3qG7Q', 'https://www.linkedin.com/profile/view?id=ADEAABAQ3VoBZKQ706lszZpIlVvm__PY06zU9ec&authType=NAME_SEARCH&authToken=BOYT&locale=pt_BR&srchid=988610261447173611818&srchindex=1&srchtotal=22&trk=vsrp_people_res_name&trkInfo=VSRPsearchId%3A988610261447173611818%2', '', 'https://br.pinterest.com/andrevasco/', 'https://plus.google.com/+Andr%C3%A9Vasco/posts', '', '2017-07-07 13:48:52', '2017-07-07 13:48:52'),
(3639, 183, 'https://www.instagram.com/anabarrosoatriz/', 'https://www.facebook.com/ana.barroso.16', '', '', '', '', '', '', '', '', '2017-07-07 18:50:09', '2017-07-07 18:50:09'),
(3643, 208, 'https://www.instagram.com/renataceribelli/?hl=pt-br', 'https://www.facebook.com/ReCeribelli/', 'https://twitter.com/receribelli', '', '', '', '', '', '', '', '2017-07-10 17:11:12', '2017-07-10 17:11:12'),
(3645, 194, 'https://www.instagram.com/josuegalinariator/', 'https://www.facebook.com/josue.galinari', '', 'http://www.imdb.com/name/nm8483321/', 'https://www.youtube.com/user/galinari4', 'https://www.linkedin.com/in/josu%25C3%25A9-galinari-59126a67/', '', '', 'https://plus.google.com/106838644483349363776', '', '2017-07-11 17:57:13', '2017-07-11 17:57:13'),
(3646, 77, 'https://instagram.com/chiaragadaleta', 'https://www.facebook.com/pages/Eco-Era/364025713674736', 'https://twitter.com/chiaragadaleta', '', '', 'https://www.linkedin.com/profile/view?id=ADEAABUZtbIBOeAMkAYOlVSWYkzoe8d_WM9Q-sc&authType=NAME_SEARCH&authToken=UCG1&locale=en_US&srchid=988610261447164450421&srchindex=1&srchtotal=3&trk=vsrp_people_res_name&trkInfo=VSRPsearchId%3A988610261447164450421%2C', '', 'https://br.pinterest.com/specoera/', 'https://plus.google.com/118132999826522107660/posts', '', '2017-07-12 20:16:07', '2017-07-12 20:16:07'),
(3647, 106, 'https://instagram.com/chiaragadaleta/', 'https://www.facebook.com/pages/Eco-Era/364025713674736', 'https://twitter.com/chiaragadaleta', '', '', 'https://www.linkedin.com/profile/view?id=ADEAABUZtbIBOeAMkAYOlVSWYkzoe8d_WM9Q-sc&authType=NAME_SEARCH&authToken=UCG1&locale=en_US&srchid=988610261447164450421&srchindex=1&srchtotal=3&trk=vsrp_people_res_name&trkInfo=VSRPsearchId%3A988610261447164450421%2C', '', 'https://br.pinterest.com/specoera/', 'https://plus.google.com/118132999826522107660/posts', '', '2017-07-12 20:18:31', '2017-07-12 20:18:31'),
(3657, 211, 'https://www.instagram.com/patrickamstalden/', 'www.facebook.com/patrick.amstalden', '', '', 'https://www.youtube.com/watch?v=DO_wy4DKdns', '', '', '', '', '', '2017-07-17 00:35:25', '2017-07-17 00:35:25'),
(3659, 195, 'https://www.instagram.com/lucianamello/', 'https://www.facebook.com/oficiallucianamello', 'https://twitter.com/lumellonega', '', 'https://www.youtube.com/user/oficialLucianaMello', 'https://www.linkedin.com/in/luciana-mello-0079b9104/', '', '', 'https://plus.google.com/+LucianaMello', '', '2017-07-17 02:22:52', '2017-07-17 02:22:52'),
(3662, 203, 'https://www.instagram.com/isaacmedeiross/', 'https://www.facebook.com/isaac.medeiros.3', '', '', '', '', '', '', '', '', '2017-07-17 13:26:36', '2017-07-17 13:26:36'),
(3672, 209, 'https://www.instagram.com/xoia/', 'https://www.facebook.com/txoia', '', '', '', '', '', '', '', '', '2017-07-18 18:33:06', '2017-07-18 18:33:06'),
(3677, 21, 'https://instagram.com/paulaprporto', 'https://www.facebook.com/paula.picarelli.3', '', '', '', '', '', '', '', '', '2017-07-18 19:58:11', '2017-07-18 19:58:11'),
(3680, 189, 'https://www.instagram.com/belizepombal/', 'https://pt-br.facebook.com/belize.pombal', 'https://twitter.com/belizepombal/with_replies', '', '', '', '', '', '', '', '2017-07-19 21:23:42', '2017-07-19 21:23:42'),
(3683, 89, 'https://instagram.com/gutorequena', 'https://www.facebook.com/estudiogutorequena', 'https://twitter.com/gutorequena', 'https://vimeo.com/estudiogutorequena', 'https://www.youtube.com/user/gutorequena', '', '', 'https://br.pinterest.com/gutorequena', 'https://plus.google.com/u/0/108348425714435304180/posts', '', '2017-07-20 13:35:13', '2017-07-20 13:35:13'),
(3685, 52, 'https://instagram.com/gutorequena', 'https://www.facebook.com/estudiogutorequena', 'https://twitter.com/gutorequena', 'https://vimeo.com/estudiogutorequena', 'https://www.youtube.com/user/gutorequena', '', '', 'https://br.pinterest.com/gutorequena', 'https://plus.google.com/u/0/108348425714435304180/posts', '', '2017-07-20 14:01:07', '2017-07-20 14:01:07'),
(3686, 58, 'http://instagram.com/rodrigomocoto', 'http://pt-br.facebook.com/rodrigomocoto', 'http://twitter.com/rodrigomocoto', '', 'https://www.youtube.com/watch?v=J3Y7o4zvL6U', '', '', '', '', '', '2017-07-20 14:20:45', '2017-07-20 14:20:45'),
(3687, 109, 'http://instagram.com/rodrigomocoto', 'http://pt-br.facebook.com/rodrigomocoto', 'http://twitter.com/rodrigomocoto', '', 'https://www.youtube.com/watch?v=J3Y7o4zvL6U', '', '', '', '', '', '2017-07-20 14:21:17', '2017-07-20 14:21:17'),
(3692, 207, 'https://www.instagram.com/lobaow/', 'https://www.facebook.com/Lobaooficial/', 'https://twitter.com/lobaoeletrico', '', 'https://www.youtube.com/channel/UCv-NWLLs-sKmgCoMwZuJPtw?gl=BR&hl=pt', '', '', '', '', 'https://www.tumblr.com/blog/yosoylobon', '2017-07-20 21:46:50', '2017-07-20 21:46:50'),
(3693, 198, 'https://www.instagram.com/adrianaaraujo_/?hl=pt-br', 'https://www.facebook.com/adriana.araujo.jornalista', 'https://twitter.com/adrianaaraujo_', '', '', '', '', '', '', '', '2017-07-21 17:57:03', '2017-07-21 17:57:03'),
(3694, 212, 'izabellabicalhooficial', 'https://www.facebook.com/izabella.bicalho.3', '', '', '', '', '', '', '', '', '2017-07-24 13:39:15', '2017-07-24 13:39:15'),
(3695, 67, 'https://www.instagram.com/monicawaldvogel_oficial/', 'https://www.facebook.com/monica.waldvogel1?fref=ts', 'https://twitter.com/monicawaldvogel', '', '', '', '', '', '', '', '2017-07-24 18:47:03', '2017-07-24 18:47:03'),
(3702, 206, 'https://www.instagram.com/alinemidlej/?hl=pt-br', 'https://www.facebook.com/aline.midlejapresentadora/', 'https://twitter.com/AlineMidlej', '', '', '', '', '', '', '', '2017-07-24 20:18:25', '2017-07-24 20:18:25'),
(3709, 29, 'https://instagram.com/mellisboa', '', 'https://twitter.com/mellisboaalves', '', '', '', '', '', '', '', '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(3712, 210, 'https://www.instagram.com/bettomarque/', 'https://www.facebook.com/Betto.marque/', 'https://twitter.com/bettomarque', '', 'https://www.youtube.com/user/RobertoMarks', '', '', '', '', '', '2017-07-25 01:54:37', '2017-07-25 01:54:37'),
(3713, 147, 'https://www.instagram.com/biapaesdebarros/?hl=pt-br', 'https://www.facebook.com/bia.paesdebarros.5', '', '', '', '', '', '', '', '', '2017-07-25 13:43:56', '2017-07-25 13:43:56'),
(3715, 143, 'https://www.instagram.com/patacamposmello/', 'https://www.facebook.com/patriciapata.mello', 'https://twitter.com/camposmello', '', '', '', '', '', '', '', '2017-07-25 14:18:09', '2017-07-25 14:18:09'),
(3716, 53, 'https://instagram.com/dudubertholini/', 'https://www.facebook.com/Dudu-Bertholini-130161110400866/timeline/', 'https://twitter.com/bertholinidudu', '', '', '', '', 'https://br.pinterest.com/dbertholini/', 'https://plus.google.com/u/0/104742191701890602616/posts', '', '2017-07-25 18:08:40', '2017-07-25 18:08:40'),
(3722, 213, 'http://instagram.com/janainaafhonso', 'http://fb.me/janaina.afhonso', '', '', 'https://www.youtube.com/channel/UC91KVeQYk5jfg9jTk7Q2nog', '', '', '', '', '', '2017-07-25 21:24:58', '2017-07-25 21:24:58');

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_red_carpet`
--

CREATE TABLE `portfolio_red_carpet` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_personalidades_id` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portfolio_red_carpet`
--

INSERT INTO `portfolio_red_carpet` (`id`, `portfolio_personalidades_id`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(1563, 51, '20151020184012image.jpeg', 0, '2016-03-07 19:48:12', '2016-03-07 19:48:12'),
(1564, 51, '20151020183944image.jpeg', 1, '2016-03-07 19:48:12', '2016-03-07 19:48:12'),
(1565, 51, '20151020172020pedroandradebrazilfoundationcelebrates-g3gdziunrml.jpg', 2, '2016-03-07 19:48:12', '2016-03-07 19:48:12'),
(1566, 51, '20151209220057ben1709.jpeg', 3, '2016-03-07 19:48:12', '2016-03-07 19:48:12'),
(1567, 51, '20151020183943image.jpeg', 4, '2016-03-07 19:48:12', '2016-03-07 19:48:12'),
(1717, 71, '201510271840296-1.jpg', 0, '2016-05-04 18:54:30', '2016-05-04 18:54:30'),
(1948, 32, '20151027004158carla-lamarca-1.jpg', 0, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(1949, 32, '20151027004205img-359914-carla-lamarca.jpg', 1, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(1950, 32, '20151027004153img-364850-estilo-lounge-caras.jpg', 2, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(1951, 32, '20151027004204carla-lamarca-8392-1200x800.jpg', 3, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(1952, 32, '20151027004200carla-lamarca-0305.jpg', 4, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(1953, 32, '20151027005154evento111.png', 5, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(1954, 32, '20151027005215p19f05104c36a4b61ac351711bf073d62.jpg', 6, '2016-07-27 19:26:54', '2016-07-27 19:26:54'),
(2321, 100, '20151112163945img-3713.JPG', 0, '2017-01-27 21:22:59', '2017-01-27 21:22:59'),
(2678, 159, '20161107181159andreramiro-g-20101119.jpg', 0, '2017-04-13 20:45:55', '2017-04-13 20:45:55'),
(2689, 132, '20151209150317images.jpeg', 0, '2017-05-02 20:46:33', '2017-05-02 20:46:33'),
(2690, 132, '20151209150317images-1.jpeg', 1, '2017-05-02 20:46:33', '2017-05-02 20:46:33'),
(2693, 42, '20151028162930image4.JPG', 0, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(2694, 42, '20151111160733img-3582.JPG', 1, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(2695, 42, '20151028172940image2.JPG', 2, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(2696, 42, '201601281728461536688-714668201891394-946287175-n.jpg', 3, '2017-05-05 20:04:34', '2017-05-05 20:04:34'),
(2702, 40, '20151021222556foto-49.jpg', 0, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(2703, 40, '20151021222135brazil-foundation-ana-beatriz-barros-marcelle-bittar.jpg', 1, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(2704, 40, '20151021222556marcelle-bittar-de-andre-lima.jpg', 2, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(2705, 40, '2015102122213424113-baile-da-vogue-0033.jpg', 3, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(2706, 40, '20151021222136cdc-2462.jpg', 4, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(2707, 40, '20151021222136300514-brazil-foundation-marcelle-bittar-400x600.jpg', 5, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(2708, 40, '20151021222556marcelle-bittar-attends-the-amfar-gala-in-sao-paulo-04.jpg', 6, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(2709, 40, '20151021222556post-3127-0-36368600-1348307809.jpg', 7, '2017-05-17 15:42:26', '2017-05-17 15:42:26'),
(2714, 34, '20151123221245costanza-pascolato-flickr-300x450.png', 0, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(2715, 34, '20151123221235costanza-pascolato-305.jpg', 1, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(2716, 34, '20151123221239costanza-pascolato-1.jpg', 2, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(2717, 34, '20151123215317costanza-skinny.jpg', 3, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(2718, 34, '201511232212347113constanza-pascolato-spfw-agnews.jpg', 4, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(2719, 34, '201511232212386257741644-922c9a2ef8-o.jpg', 5, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(2720, 34, '20151123214808pictures7.jpg', 6, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(2721, 34, '20151123221235constanza-festa-1.jpg', 7, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(2722, 34, '2015112322123717-prf-5903-costanza-pascolato.jpg', 8, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(2723, 34, '20151123221241images.jpeg', 9, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(2724, 34, '20151123221241unknown-2.jpeg', 10, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(2725, 34, '20151123221243unknown-1.jpeg', 11, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(2726, 34, '20151123221242dsc03460.jpeg', 12, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(2727, 34, '20151123221243images-1.jpeg', 13, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(2728, 34, '20151123221245costanza-pascolato.jpg', 14, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(2729, 34, '20151123221244capodarte-217-costanza-pascolato.jpg', 15, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(2730, 34, '20151123221246costanza.jpg', 16, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(2731, 34, '20151124172414sem-titulo-1.jpg', 17, '2017-05-17 21:11:58', '2017-05-17 21:11:58'),
(2732, 135, '20160119144445red-1.png', 0, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(2733, 135, '20160119144459red2.jpg', 1, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(2734, 135, '20160119144530red4.jpg', 2, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(2735, 135, '20160119144539red-5.jpg', 3, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(2736, 135, '20160119144547red-6.jpg', 4, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(2737, 135, '20160119144555red-7.jpg', 5, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(2738, 135, '20160119144601red-8.jpg', 6, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(2739, 135, '20160119144610red-9.jpg', 7, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(2740, 135, '20160119144621red-10.jpg', 8, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(2741, 135, '20160119144628red-11.jpg', 9, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(2742, 135, '20160119144635red-12.jpg', 10, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(2743, 135, '20160119144641red-13.jpg', 11, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(2744, 135, '20160119144649red-15.jpg', 12, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(2745, 135, '20160119144654red-16.jpg', 13, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(2746, 135, '20160119144700red-17.jpg', 14, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(2747, 135, '20160119144704red-18.jpg', 15, '2017-05-17 21:13:44', '2017-05-17 21:13:44'),
(2748, 148, '20160531120918sjj-9150-624x1024.jpg', 0, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(2749, 148, '2016053112091812139570-400311416760075-17359649-n.jpg', 1, '2017-05-19 00:11:29', '2017-05-19 00:11:29'),
(2764, 150, '20160531220819images-1.jpeg', 0, '2017-05-19 01:52:33', '2017-05-19 01:52:33'),
(2776, 102, '20151201123916image001.jpg', 0, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(2777, 102, '20151026174739img-20151026-wa0005.jpg', 1, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(2778, 102, '20151026174805img-20151026-wa0006.jpg', 2, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(2779, 102, '20151026174823img-20151026-wa0008.jpg', 3, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(2780, 102, '20151026174844img-20151026-wa0009.jpg', 4, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(2781, 102, '20151026174856img-20151026-wa0007.jpg', 5, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(2782, 102, '20161212172105whatsapp-image-2016-12-04-at-095500.jpeg', 6, '2017-05-19 14:33:43', '2017-05-19 14:33:43'),
(2785, 186, '20170213225429betty-4.jpg', 0, '2017-05-22 18:19:01', '2017-05-22 18:19:01'),
(2786, 186, '20170213225429bethy-lagardere-chamada-home.jpg', 1, '2017-05-22 18:19:01', '2017-05-22 18:19:01'),
(2797, 30, '2015102713125812030435-10153701881509487-7199237963503389541-o-1.jpg', 0, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(2798, 30, '2015112320534612239646-10153749534304487-7944152969722272340-n.jpg', 1, '2017-05-30 20:05:54', '2017-05-30 20:05:54'),
(2799, 172, '20161018223521image15.JPG', 0, '2017-05-31 19:26:16', '2017-05-31 19:26:16'),
(2806, 176, '20170321123839fabiana-gomes-1.jpg', 0, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(2807, 176, '20170321123844fabiana-gomes.jpg', 1, '2017-06-12 15:26:16', '2017-06-12 15:26:16'),
(2811, 182, '20170207193217fullsizerender.jpg', 0, '2017-06-19 15:29:31', '2017-06-19 15:29:31'),
(2816, 190, '2017041213072015099597-1630486140581471-4551865666684059648-n.jpg', 0, '2017-06-23 15:05:26', '2017-06-23 15:05:26'),
(2817, 190, '20170412130721o-autor-walcyr-carrasco.jpg', 1, '2017-06-23 15:05:26', '2017-06-23 15:05:26'),
(2833, 33, '20160118151121img-6427.jpg', 0, '2017-06-30 13:37:53', '2017-06-30 13:37:53'),
(2836, 55, '20161206194814whatsapp-image-2016-12-04-at-095500.jpeg', 0, '2017-07-07 13:10:58', '2017-07-07 13:10:58'),
(2837, 55, '20151201123843image001.jpg', 1, '2017-07-07 13:10:58', '2017-07-07 13:10:58'),
(2838, 145, '20161212175433whatsapp-image-2016-12-04-at-095500.jpeg', 0, '2017-07-07 13:14:24', '2017-07-07 13:14:24'),
(2839, 145, '201605121428362016042518272920151201123916image001.jpg', 1, '2017-07-07 13:14:24', '2017-07-07 13:14:24'),
(2840, 70, '20151118175015cine-1cinebemvestidas0210.jpg', 0, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(2841, 70, '20151216222315va.png', 1, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(2842, 70, '20151216222314vanessa-rosan.png', 2, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(2843, 70, '20161220142130whatsapp-image-2016-12-04-at-095249.jpeg', 3, '2017-07-07 13:40:33', '2017-07-07 13:40:33'),
(2844, 26, '20151014163542red-carpet-camila-alves-1.jpg', 0, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(2845, 26, '20151019191829874269576f05af45237b4f6ec39ba92a.jpg', 1, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(2846, 26, '20151027000955segredinho-camila-alves-e-sua-meia-modeladora.jpg', 2, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(2847, 26, '20151014163543red-carpet-camila-alves-5.jpg', 3, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(2848, 26, '20151027000956la-modella-mafia-camila-alves-at-the-vanity-fair-oscar-after-party-2011.jpg', 4, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(2849, 26, '20151014163539red-carpet-camila-alves-2.jpeg', 5, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(2850, 26, '20151014163543red-carpet-camila-alves-3.jpg', 6, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(2851, 26, '20151014163539red-carpet-camila-alves-4.jpg', 7, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(2852, 26, '201510270009571421025279-camila-alves-zoom.jpg', 8, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(2853, 26, '20151023000252299044-1853894565845-152367974-n.jpg', 9, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(2854, 26, '201510191918291eb454048d888300d6775e0c8c97a2cc.jpg', 10, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(2855, 26, '20151027000953pmgzyd9dg5xccxd.jpg', 11, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(2856, 26, '20151027000955matthew-mcconaughey-camila-alves-picture-perfect-pair-at-goldene-kamera-awards-01.jpg', 12, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(2857, 26, '20151027000956showbiz-matthew-mcconaughey.jpg', 13, '2017-07-07 13:42:10', '2017-07-07 13:42:10'),
(2858, 77, '20160118151157img-6427.jpg', 0, '2017-07-12 20:16:07', '2017-07-12 20:16:07'),
(2859, 106, '20160118151234img-6427.jpg', 0, '2017-07-12 20:18:31', '2017-07-12 20:18:31'),
(2866, 29, '20160328181631image.jpeg', 0, '2017-07-24 23:49:28', '2017-07-24 23:49:28'),
(2867, 147, '201606211507021461793711775-2.jpeg', 0, '2017-07-25 13:43:56', '2017-07-25 13:43:56');

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_subcategorias`
--

CREATE TABLE `portfolio_subcategorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_categorias_id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portfolio_subcategorias`
--

INSERT INTO `portfolio_subcategorias` (`id`, `portfolio_categorias_id`, `titulo`, `slug`, `ordem`, `created_at`, `updated_at`) VALUES
(12, 1, 'entretenimento', 'entretenimento', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 1, 'esportivo', 'esportivo', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 1, 'rádio', 'radio', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 4, 'estilistas', 'estilistas', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 4, 'stylists', 'stylists', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 4, 'editores de moda', 'editores-de-moda', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 10, 'chefs', 'chefs', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 10, 'crítico gastronômico', 'critico-gastronomico', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 10, 'food truckers', 'food-truckers', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 2, 'atores', 'atores', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 2, 'atrizes', 'atrizes', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_videos`
--

CREATE TABLE `portfolio_videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_personalidades_id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `servico` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `servico_video_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `embed` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `portfolio_videos`
--

INSERT INTO `portfolio_videos` (`id`, `portfolio_personalidades_id`, `titulo`, `url`, `servico`, `servico_video_id`, `thumbnail`, `embed`, `ordem`, `created_at`, `updated_at`) VALUES
(83, 90, 'Costanza - C&A', 'https://youtu.be/xaFcUjCPp1k?list=PLcrydkWS_1zF2vn70kosV1rJSPBcwOij4', 'youtube', 'xaFcUjCPp1k', 'xaFcUjCPp1k.jpg', '<iframe src=\'http://www.youtube.com/embed/xaFcUjCPp1k?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-10-15 20:47:58', '2016-08-18 13:29:28'),
(90, 44, 'Silvia Poppovic - Qual a idade para ser sexy?', 'https://youtu.be/jtkt7INg7Dk?list=PLcrydkWS_1zEFk6SvwEfTUXkzxc3WHk6m', 'youtube', 'jtkt7INg7Dk', 'jtkt7INg7Dk.jpg', '<iframe src=\'http://www.youtube.com/embed/jtkt7INg7Dk?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-10-16 14:30:40', '2015-10-27 18:10:37'),
(91, 84, 'Silvia Poppovic - Quem sou eu?', 'https://youtu.be/EnYpdPqUkZ0?list=PLcrydkWS_1zEFk6SvwEfTUXkzxc3WHk6m', 'youtube', 'EnYpdPqUkZ0', 'EnYpdPqUkZ0.jpg', '<iframe src=\'http://www.youtube.com/embed/EnYpdPqUkZ0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-10-16 14:39:03', '2015-10-27 18:10:37'),
(92, 42, 'Marília Moreno - New Reel', 'https://youtu.be/IJB6noiog4U?list=PLcrydkWS_1zHjq1d461YzPTG98cWSutRu', 'youtube', 'IJB6noiog4U', 'IJB6noiog4U.jpg', '<iframe src=\'http://www.youtube.com/embed/IJB6noiog4U?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-10-16 14:41:06', '2017-01-30 21:39:44'),
(93, 42, 'Marília Moreno - Comercial New', 'https://youtu.be/1I6y7BhaRY8?list=PLcrydkWS_1zHjq1d461YzPTG98cWSutRu', 'youtube', '1I6y7BhaRY8', '1I6y7BhaRY8.jpg', '<iframe src=\'http://www.youtube.com/embed/1I6y7BhaRY8?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-10-16 14:42:35', '2017-01-30 21:39:44'),
(94, 40, 'Marcelle Bittar - Semana de Moda São Paulo - Verão/2015', 'https://youtu.be/nyWXaNhC8js?list=PLcrydkWS_1zEGBT5cSDcdDlXltNZpVfR8', 'youtube', 'nyWXaNhC8js', 'nyWXaNhC8js.jpg', '<iframe src=\'http://www.youtube.com/embed/nyWXaNhC8js?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-10-16 14:46:16', '2015-11-13 17:32:26'),
(95, 40, 'Marcelle Bittar - Semana de Moda São Paulo - Verão/2015 - Pílula 3 - Sobrancelhas', 'https://youtu.be/uGHEl0LDPJg?list=PLcrydkWS_1zEGBT5cSDcdDlXltNZpVfR8', 'youtube', 'uGHEl0LDPJg', 'uGHEl0LDPJg.jpg', '<iframe src=\'http://www.youtube.com/embed/uGHEl0LDPJg?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-10-16 14:46:18', '2015-11-13 17:32:26'),
(96, 40, 'Marcelle Bittar - Semana de Moda São Paulo - Verão/2015 - Pílula 7 - Unhas do Verão 2015', 'https://youtu.be/a6GM0zzGNpo?list=PLcrydkWS_1zEGBT5cSDcdDlXltNZpVfR8', 'youtube', 'a6GM0zzGNpo', 'a6GM0zzGNpo.jpg', '<iframe src=\'http://www.youtube.com/embed/a6GM0zzGNpo?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-10-16 14:46:18', '2015-11-13 17:32:26'),
(98, 26, 'Camila Alves - Clipe Because Of You (Ne-Yo)', 'https://youtu.be/atz_aZA3rf0?list=PLcrydkWS_1zFKvOq9qA_vDDAo10dxOEnE', 'youtube', 'atz_aZA3rf0', 'atz_aZA3rf0.jpg', '<iframe src=\'http://www.youtube.com/embed/atz_aZA3rf0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-10-16 16:48:15', '2015-10-27 18:23:53'),
(99, 26, 'Camila Alves - Victória Secrets', 'https://youtu.be/Y_m3zTA4-5A?list=PLcrydkWS_1zFKvOq9qA_vDDAo10dxOEnE', 'youtube', 'Y_m3zTA4-5A', 'Y_m3zTA4-5A.jpg', '<iframe src=\'http://www.youtube.com/embed/Y_m3zTA4-5A?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 5, '2015-10-16 16:48:15', '2015-10-28 18:46:10'),
(102, 30, 'Carol Scaff - O Boticário Nativa Spa - Argiloterapia', 'https://youtu.be/yCVE10MB_gk?list=PLcrydkWS_1zG3U1pUh5Kge68xHKeWp--n', 'youtube', 'yCVE10MB_gk', 'yCVE10MB_gk.jpg', '<iframe src=\'http://www.youtube.com/embed/yCVE10MB_gk?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 5, '2015-10-16 17:00:48', '2015-11-12 17:41:58'),
(103, 30, 'Carol Scaff - Teaser', 'https://www.youtube.com/watch?v=fEL4T16xhDE&index=1&list=PLcrydkWS_1zG3U1pUh5Kge68xHKeWp--n', 'youtube', 'fEL4T16xhDE', 'fEL4T16xhDE.jpg', '<iframe src=\'http://www.youtube.com/embed/fEL4T16xhDE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-10-16 17:00:48', '2015-10-16 17:00:48'),
(105, 30, 'Carol Scaff', 'https://youtu.be/E5vdTJ4_AZI?list=PLcrydkWS_1zG3U1pUh5Kge68xHKeWp--n', 'youtube', 'E5vdTJ4_AZI', 'E5vdTJ4_AZI.jpg', '<iframe src=\'http://www.youtube.com/embed/E5vdTJ4_AZI?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-10-16 17:00:48', '2015-11-12 17:41:58'),
(106, 30, 'Carol Scaff - amaury Jr.', 'https://youtu.be/uC5xCXFknz4?list=PLcrydkWS_1zG3U1pUh5Kge68xHKeWp--n', 'youtube', 'uC5xCXFknz4', 'uC5xCXFknz4.jpg', '<iframe src=\'http://www.youtube.com/embed/uC5xCXFknz4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-10-16 17:02:23', '2015-11-12 17:41:58'),
(107, 24, 'Astrid Fontenelle - Kapo', 'https://youtu.be/AqP4CX3lA2A?list=PLcrydkWS_1zFpjudl49wasSy1gvB_GIeG', 'youtube', 'AqP4CX3lA2A', 'AqP4CX3lA2A.jpg', '<iframe src=\'http://www.youtube.com/embed/AqP4CX3lA2A?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-10-16 17:09:58', '2015-12-09 19:18:41'),
(108, 24, 'Astrid Fontenelle - Manifesto', 'https://youtu.be/RdENX3vtXZw?list=PLcrydkWS_1zFpjudl49wasSy1gvB_GIeG', 'youtube', 'RdENX3vtXZw', 'RdENX3vtXZw.jpg', '<iframe src=\'http://www.youtube.com/embed/RdENX3vtXZw?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2015-10-16 17:09:59', '2015-12-09 19:18:41'),
(113, 35, 'Daniella Cicarelli - Entrevista Programa do Jô', 'https://www.youtube.com/watch?v=jgr6Yr1LVCU', 'youtube', 'jgr6Yr1LVCU', 'jgr6Yr1LVCU.jpg', '<iframe src=\'http://www.youtube.com/embed/jgr6Yr1LVCU?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-10-19 17:24:24', '2016-06-15 21:11:08'),
(114, 26, 'Camila Alves - Programa do Jô parte I (12.05.11)', 'https://youtu.be/IYaqet2XNQE', 'youtube', 'IYaqet2XNQE', 'IYaqet2XNQE.jpg', '<iframe src=\'http://www.youtube.com/embed/IYaqet2XNQE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-10-19 17:27:01', '2015-10-28 18:46:10'),
(115, 26, 'Camila Alves - Programa do Jô parte II (12.05.11)', 'https://youtu.be/_u72s4MWH6Y', 'youtube', '_u72s4MWH6Y', '_u72s4MWH6Y.jpg', '<iframe src=\'http://www.youtube.com/embed/_u72s4MWH6Y?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-10-19 17:38:24', '2015-10-28 18:46:10'),
(117, 89, 'Guto Requena - Nos Trinques GNT', 'https://youtu.be/LhYFyDerbsI?list=PL4764F7A0712397EF', 'youtube', 'LhYFyDerbsI', 'LhYFyDerbsI.jpg', '<iframe src=\'http://www.youtube.com/embed/LhYFyDerbsI?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-10-19 19:03:04', '2015-10-27 18:10:49'),
(118, 52, 'Guto Requena - Nos Trinques (GNT) - Estante Árvore', 'https://youtu.be/LhYFyDerbsI?list=PL4764F7A0712397EF', 'youtube', 'LhYFyDerbsI', 'LhYFyDerbsI.jpg', '<iframe src=\'http://www.youtube.com/embed/LhYFyDerbsI?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-10-19 19:05:08', '2017-07-19 17:05:59'),
(121, 57, 'Paulo Borges - SPFW', 'https://youtu.be/a3-mAm1Bo7M?list=PLcrydkWS_1zF4GAZDhf3c1cHi79klJ5oL', 'youtube', 'a3-mAm1Bo7M', 'a3-mAm1Bo7M.jpg', '<iframe src=\'http://www.youtube.com/embed/a3-mAm1Bo7M?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-10-19 21:14:23', '2015-10-27 18:10:51'),
(122, 30, 'Carol Scaff - TV Host', 'https://youtu.be/2W-xXRZ4xRc', 'youtube', '2W-xXRZ4xRc', '2W-xXRZ4xRc.jpg', '<iframe src=\'http://www.youtube.com/embed/2W-xXRZ4xRc?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-10-21 13:32:39', '2015-11-12 17:41:58'),
(123, 47, 'Tatiana Dumenti - Paqueta Outono 2015', 'https://youtu.be/lcX7h5JWAE0', 'youtube', 'lcX7h5JWAE0', 'lcX7h5JWAE0.jpg', '<iframe src=\'http://www.youtube.com/embed/lcX7h5JWAE0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-10-21 14:16:59', '2015-10-21 14:16:59'),
(125, 19, 'Cássio Scapin - O LIBERTINO', 'https://www.youtube.com/watch?v=4PEHmNN9gl8', 'youtube', '4PEHmNN9gl8', '4PEHmNN9gl8.jpg', '<iframe src=\'http://www.youtube.com/embed/4PEHmNN9gl8?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-10-21 17:14:18', '2015-10-21 17:14:18'),
(126, 19, 'Cássio Scapin - Opera A viúva alegre', 'http://youtu.be/XIBNjk7GsAs', 'youtube', 'XIBNjk7GsAs', 'XIBNjk7GsAs.jpg', '<iframe src=\'http://www.youtube.com/embed/XIBNjk7GsAs?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-10-21 18:05:29', '2015-10-21 18:06:20'),
(129, 42, 'Marilia Moreno - Hosting Reel recent', 'https://youtu.be/KpugdZr-Fjg', 'youtube', 'KpugdZr-Fjg', 'KpugdZr-Fjg.jpg', '<iframe src=\'http://www.youtube.com/embed/KpugdZr-Fjg?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-10-22 13:07:59', '2017-01-30 21:39:44'),
(130, 61, 'Josimar Melo - Comer o que?', 'https://www.youtube.com/watch?v=ErHnHY4qQ2M&index=1&list=PLcrydkWS_1zHBZpauHHoqS_9k3DZZDo5h', 'youtube', 'ErHnHY4qQ2M', 'ErHnHY4qQ2M.jpg', '<iframe src=\'http://www.youtube.com/embed/ErHnHY4qQ2M?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-10-23 17:46:55', '2015-10-23 17:46:55'),
(138, 29, 'GQ BRASIL ENSAIO MEL LISBOA', 'https://youtu.be/9MJsgz_u0Us?list=PLcrydkWS_1zGKaBSL3etyMCNv27VohHNL', 'youtube', '9MJsgz_u0Us', '9MJsgz_u0Us.jpg', '<iframe src=\'http://www.youtube.com/embed/9MJsgz_u0Us?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-10-27 17:54:19', '2015-10-27 17:54:19'),
(143, 26, 'Camila Alves ', 'https://www.youtube.com/watch?v=I6KP5ermX4o', 'youtube', 'I6KP5ermX4o', 'I6KP5ermX4o.jpg', '<iframe src=\'http://www.youtube.com/embed/I6KP5ermX4o?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-10-27 18:23:52', '2015-10-28 18:46:10'),
(144, 26, 'Camila Alves - Cooks Up An In-N-Out-Style Burger ', 'http://videos.famousfix.com/videos?v=sRtT3fCbmgA', 'youtube', 'sRtT3fCbmgA', 'sRtT3fCbmgA.jpg', '<iframe src=\'http://www.youtube.com/embed/sRtT3fCbmgA?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2015-10-27 18:23:53', '2015-10-28 18:46:10'),
(145, 71, 'Duda Molinos - Comercial dos esmaltes Vult com Grazi Massafera (2015)', 'https://vimeo.com/130658133', 'vimeo', '130658133', '130658133.jpg', '<iframe src="//player.vimeo.com/video/130658133?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 1, '2015-10-27 18:28:34', '2015-12-07 14:39:47'),
(146, 52, 'Guto Requena - Galeria da Arquitetura | WZ Hotel', 'https://www.youtube.com/watch?v=8chwJCz6UNE', 'youtube', '8chwJCz6UNE', '8chwJCz6UNE.jpg', '<iframe src=\'http://www.youtube.com/embed/8chwJCz6UNE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2015-10-27 18:42:10', '2017-07-19 17:05:59'),
(147, 52, 'Guto Requena - Casa Cor', 'https://www.youtube.com/watch?v=NvELDwtqs3o', 'youtube', 'NvELDwtqs3o', 'NvELDwtqs3o.jpg', '<iframe src=\'http://www.youtube.com/embed/NvELDwtqs3o?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-10-27 18:46:42', '2017-07-19 17:05:59'),
(148, 30, 'Carol Scaff', 'https://vimeo.com/48057067 ', 'vimeo', '48057067', '48057067.jpg', '<iframe src="//player.vimeo.com/video/48057067?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 4, '2015-10-27 18:56:03', '2015-11-12 17:41:58'),
(154, 24, 'Astrid Fontenelle - He for she', 'https://vimeopro.com/banditsfilms/commercials/video/134878980', 'vimeo', '134878980', '134878980.jpg', '<iframe src="//player.vimeo.com/video/134878980?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 5, '2015-10-27 20:49:23', '2015-12-09 19:18:41'),
(155, 102, 'Canal You tube / Lilian Pacce ', 'https://youtu.be/9yXCiVkqRGg', 'youtube', '9yXCiVkqRGg', '9yXCiVkqRGg.jpg', '<iframe src=\'http://www.youtube.com/embed/9yXCiVkqRGg?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-10-28 00:24:21', '2017-04-11 20:13:52'),
(156, 102, 'Tendências polêmicas: jornalistas e blogueiras comentam', 'https://youtu.be/vdIytkh62NQ', 'youtube', 'vdIytkh62NQ', 'vdIytkh62NQ.jpg', '<iframe src=\'http://www.youtube.com/embed/vdIytkh62NQ?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-10-28 00:30:25', '2017-04-11 20:13:52'),
(157, 55, 'o lifestyle de lillian pacce - making of bamboo ', 'https://vimeo.com/48602617', 'vimeo', '48602617', '48602617.jpg', '<iframe src="//player.vimeo.com/video/48602617?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 1, '2015-10-28 00:40:19', '2017-04-11 20:11:11'),
(158, 102, 'o lifestyle de lillian pacce - making of bamboo ', 'https://vimeo.com/48602617', 'vimeo', '48602617', '48602617.jpg', '<iframe src="//player.vimeo.com/video/48602617?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 1, '2015-10-28 00:41:08', '2017-04-11 20:13:52'),
(159, 49, 'André Vasco - Cozinhando no Supermercado', 'https://youtu.be/Nmug7oruzXs', 'youtube', 'Nmug7oruzXs', 'Nmug7oruzXs.jpg', '<iframe src=\'http://www.youtube.com/embed/Nmug7oruzXs?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-10-28 13:51:44', '2016-12-08 15:03:29'),
(160, 49, 'André Vasco - Entrevista Agora é tarde com Rafa Bastos', 'https://youtu.be/oW1hr8mFoCU', 'youtube', 'oW1hr8mFoCU', 'oW1hr8mFoCU.jpg', '<iframe src=\'http://www.youtube.com/embed/oW1hr8mFoCU?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 9, '2015-10-28 13:56:47', '2016-12-08 15:03:29'),
(161, 49, 'André Vasco - Programa The noite com Danilo Gentili', 'https://youtu.be/Nh4DbQIlpz0', 'youtube', 'Nh4DbQIlpz0', 'Nh4DbQIlpz0.jpg', '<iframe src=\'http://www.youtube.com/embed/Nh4DbQIlpz0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 8, '2015-10-28 14:01:08', '2016-12-08 15:03:29'),
(162, 49, 'André Vasco - Programa Legendários', 'https://youtu.be/n69IpPhUlD0', 'youtube', 'n69IpPhUlD0', 'n69IpPhUlD0.jpg', '<iframe src=\'http://www.youtube.com/embed/n69IpPhUlD0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 10, '2015-10-28 14:01:09', '2016-12-08 15:03:29'),
(163, 24, 'Astrid Fontenelle - Coca cola', 'https://youtu.be/6eH-BN0gMos', 'youtube', '6eH-BN0gMos', '6eH-BN0gMos.jpg', '<iframe src=\'http://www.youtube.com/embed/6eH-BN0gMos?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-10-28 14:45:46', '2015-12-09 19:18:41'),
(165, 49, 'Andrè Vasco - Programa Qual é seu talento', 'https://youtu.be/z4D-PXChAT8', 'youtube', 'z4D-PXChAT8', 'z4D-PXChAT8.jpg', '<iframe src=\'http://www.youtube.com/embed/z4D-PXChAT8?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 5, '2015-10-28 16:59:20', '2016-12-08 15:03:29'),
(166, 49, 'André Vasco - Programa Astros', 'https://youtu.be/cr316j_sd7I', 'youtube', 'cr316j_sd7I', 'cr316j_sd7I.jpg', '<iframe src=\'http://www.youtube.com/embed/cr316j_sd7I?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 6, '2015-10-28 16:59:23', '2016-12-08 15:03:29'),
(168, 38, 'Lígia Mendes - Top 20 Band', 'https://youtu.be/d3a6Qed7s04', 'youtube', 'd3a6Qed7s04', 'd3a6Qed7s04.jpg', '<iframe src=\'http://www.youtube.com/embed/d3a6Qed7s04?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-10-28 17:03:35', '2015-10-28 17:03:35'),
(170, 103, 'Amanda Ramalho - 20 músicas', 'https://youtu.be/L4maagfXJAQ', 'youtube', 'L4maagfXJAQ', 'L4maagfXJAQ.jpg', '<iframe src=\'http://www.youtube.com/embed/L4maagfXJAQ?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-10-28 17:52:00', '2015-10-28 17:52:00'),
(171, 32, 'Carla Lamarca - Vídeo publicidade', 'https://vimeo.com/143909817', 'vimeo', '143909817', '143909817.jpg', '<iframe src="//player.vimeo.com/video/143909817?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 4, '2015-10-28 20:00:52', '2015-11-13 16:35:06'),
(172, 32, 'Carla Lamarca - Contém 1g', 'https://vimeo.com/143908935', 'vimeo', '143908935', '143908935.jpg', '<iframe src="//player.vimeo.com/video/143908935?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, '2015-10-28 20:00:55', '2015-11-13 16:35:06'),
(173, 32, 'Carla Lamarca - Guri Azul', 'https://vimeo.com/143910859', 'vimeo', '143910859', '143910859.jpg', '<iframe src="//player.vimeo.com/video/143910859?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 1, '2015-10-28 20:00:58', '2015-11-13 16:35:06'),
(174, 32, 'Carla Lamarca - entrevista com Oasis - Jornal MTV', 'https://vimeo.com/143913226', 'vimeo', '143913226', '143913226.jpg', '<iframe src="//player.vimeo.com/video/143913226?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 0, '2015-10-28 20:01:01', '2015-11-13 16:35:06'),
(175, 32, 'Carla Lamarca - Insensato Coração', 'https://vimeo.com/143912476', 'vimeo', '143912476', '143912476.jpg', '<iframe src="//player.vimeo.com/video/143912476?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 5, '2015-10-28 20:01:03', '2015-11-03 21:17:48'),
(177, 26, 'Camila Alves - Shoot', 'https://vimeo.com/143916172', 'vimeo', '143916172', '143916172.jpg', '<iframe src="//player.vimeo.com/video/143916172?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 6, '2015-10-28 20:12:20', '2015-10-28 20:12:20'),
(178, 23, 'Programa Pânico - Radio Jovem Pan FM', 'https://youtu.be/EYNoH264gBk', 'youtube', 'EYNoH264gBk', 'EYNoH264gBk.jpg', '<iframe src=\'http://www.youtube.com/embed/EYNoH264gBk?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-10-28 20:50:11', '2015-11-24 21:04:45'),
(179, 33, 'Chiara Gadaleta - Programa Eliana', 'https://youtu.be/yukk74Fn8KI', 'youtube', 'yukk74Fn8KI', 'yukk74Fn8KI.jpg', '<iframe src=\'http://www.youtube.com/embed/yukk74Fn8KI?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-10-29 12:49:55', '2017-05-05 15:04:47'),
(185, 78, 'Regina Guerreiro - Enjoy I', 'https://www.youtube.com/watch?v=CE503Bhsuro&index=2&list=PLcrydkWS_1zFcQ_ofUt4eXJvD72wZ4gQb', 'youtube', 'CE503Bhsuro', 'CE503Bhsuro.jpg', '<iframe src=\'http://www.youtube.com/embed/CE503Bhsuro?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-10-30 13:51:29', '2015-12-01 20:00:15'),
(186, 78, 'Regina Guerreiro - Enjoy II', 'https://www.youtube.com/watch?v=4MrKFqs8lxY&index=1&list=PLcrydkWS_1zFcQ_ofUt4eXJvD72wZ4gQb', 'youtube', '4MrKFqs8lxY', '4MrKFqs8lxY.jpg', '<iframe src=\'http://www.youtube.com/embed/4MrKFqs8lxY?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-10-30 13:51:29', '2015-12-01 20:00:15'),
(187, 34, 'Costanza Pascolato - Chilli Beans', 'https://youtu.be/HivXPNcoRaE?list=PLcrydkWS_1zF2vn70kosV1rJSPBcwOij4', 'youtube', 'HivXPNcoRaE', 'HivXPNcoRaE.jpg', '<iframe src=\'http://www.youtube.com/embed/HivXPNcoRaE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 6, '2015-11-03 19:08:22', '2016-08-18 13:32:28'),
(188, 34, 'Costanza e Marilu - A de Agora', 'https://youtu.be/ntUrBUQLiRQ?list=PLcrydkWS_1zF2vn70kosV1rJSPBcwOij4', 'youtube', 'ntUrBUQLiRQ', 'ntUrBUQLiRQ.jpg', '<iframe src=\'http://www.youtube.com/embed/ntUrBUQLiRQ?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-11-03 19:08:22', '2016-08-18 13:32:28'),
(189, 32, 'Carla Lamarca - Lux', 'https://vimeo.com/144546938', 'vimeo', '144546938', '144546938.jpg', '<iframe src="//player.vimeo.com/video/144546938?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 2, '2015-11-03 21:17:48', '2015-11-13 16:35:06'),
(190, 100, 'Luitha - Dia de los muertos', 'https://youtu.be/ziyCsh9PE0I?list=PLcrydkWS_1zHz-m5hLPCn9HvZHDzTHhFv', 'youtube', 'ziyCsh9PE0I', 'ziyCsh9PE0I.jpg', '<iframe src=\'http://www.youtube.com/embed/ziyCsh9PE0I?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-11-04 14:56:01', '2016-08-22 15:34:00'),
(191, 39, 'Luize - Rock in Rio', 'https://youtu.be/dl69EPGTSqI', 'youtube', 'dl69EPGTSqI', 'dl69EPGTSqI.jpg', '<iframe src=\'http://www.youtube.com/embed/dl69EPGTSqI?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-11-04 15:32:43', '2015-11-04 15:32:43'),
(192, 31, 'BARBARA GANCIA - TV Nube', 'https://youtu.be/mwkBUYho0jM', 'youtube', 'mwkBUYho0jM', 'mwkBUYho0jM.jpg', '<iframe src=\'http://www.youtube.com/embed/mwkBUYho0jM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-11-04 17:23:36', '2015-11-13 13:59:08'),
(193, 100, 'Luitha - Portifólio', 'https://youtu.be/aUgLXF60XFU', 'youtube', 'aUgLXF60XFU', 'aUgLXF60XFU.jpg', '<iframe src=\'http://www.youtube.com/embed/aUgLXF60XFU?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-11-04 18:16:04', '2016-08-22 15:34:00'),
(194, 49, 'André Vasco - Top 20', 'https://youtu.be/DeF4QxCeOS8', 'youtube', 'DeF4QxCeOS8', 'DeF4QxCeOS8.jpg', '<iframe src=\'http://www.youtube.com/embed/DeF4QxCeOS8?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-11-04 19:53:32', '2016-12-08 15:03:29'),
(195, 49, 'André vasco - troféu Imprensa', 'https://youtu.be/ctv6gk1k2MI', 'youtube', 'ctv6gk1k2MI', 'ctv6gk1k2MI.jpg', '<iframe src=\'http://www.youtube.com/embed/ctv6gk1k2MI?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 7, '2015-11-04 19:53:33', '2016-12-08 15:03:29'),
(196, 112, 'Érika Palomino - Entrevista MODA', 'https://youtu.be/hvLC6kbcl3k', 'youtube', 'hvLC6kbcl3k', 'hvLC6kbcl3k.jpg', '<iframe src=\'http://www.youtube.com/embed/hvLC6kbcl3k?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-11-05 15:53:23', '2015-11-16 12:31:30'),
(197, 64, 'Felipe Shure - Entrevista Ana Maria Braga', 'https://www.youtube.com/watch?v=jIpBwi3RcE4', 'youtube', 'jIpBwi3RcE4', 'jIpBwi3RcE4.jpg', '<iframe src=\'http://www.youtube.com/embed/jIpBwi3RcE4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-11-05 18:31:43', '2015-11-05 18:31:43'),
(198, 99, 'Josimar Melo - Entrevista com Renata', 'https://youtu.be/rEva1aD6mGg', 'youtube', 'rEva1aD6mGg', 'rEva1aD6mGg.jpg', '<iframe src=\'http://www.youtube.com/embed/rEva1aD6mGg?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-11-05 19:45:48', '2015-11-05 19:45:48'),
(199, 99, 'Josimar Melo - Entrevista com Eric', 'https://youtu.be/l6dXmBprM1Q', 'youtube', 'l6dXmBprM1Q', 'l6dXmBprM1Q.jpg', '<iframe src=\'http://www.youtube.com/embed/l6dXmBprM1Q?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-11-05 19:45:48', '2015-11-05 19:45:48'),
(200, 66, 'Mona Dorf - entrevista arquiteto', 'https://youtu.be/G-y2RNNJASw', 'youtube', 'G-y2RNNJASw', 'G-y2RNNJASw.jpg', '<iframe src=\'http://www.youtube.com/embed/G-y2RNNJASw?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-11-06 18:01:32', '2015-11-06 18:01:32'),
(201, 91, 'Érika palomino - Mulheres', 'https://vimeo.com/145128064', 'vimeo', '145128064', '145128064.jpg', '<iframe src="//player.vimeo.com/video/145128064?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 0, '2015-11-09 13:13:57', '2015-11-09 13:13:57'),
(202, 51, 'Pedro Andrade - Fusion', 'https://youtu.be/opR3_f-39P0', 'youtube', 'opR3_f-39P0', 'opR3_f-39P0.jpg', '<iframe src=\'http://www.youtube.com/embed/opR3_f-39P0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-11-09 13:38:54', '2016-02-04 13:14:26'),
(203, 112, 'Érika Palomino - Melissa', 'https://vimeo.com/145133590', 'vimeo', '145133590', '145133590.jpg', '<iframe src="//player.vimeo.com/video/145133590?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 0, '2015-11-09 14:17:25', '2015-11-16 12:31:30'),
(204, 53, 'Dudu Bertholini - aprendendo com o Vintage', 'https://vimeo.com/145172724', 'vimeo', '145172724', '145172724.jpg', '<iframe src="//player.vimeo.com/video/145172724?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 4, '2015-11-09 19:02:29', '2017-05-17 14:37:01'),
(205, 33, 'Chiara Gadaleta - Aprendendo com o Vintage com Dudu Bertholin', 'https://vimeo.com/145172724', 'vimeo', '145172724', '145172724.jpg', '<iframe src="//player.vimeo.com/video/145172724?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 4, '2015-11-09 19:06:56', '2017-05-05 15:04:47'),
(206, 53, 'Dudu Bertholini', 'https://youtu.be/ZSxpyC8UYZk', 'youtube', 'ZSxpyC8UYZk', 'ZSxpyC8UYZk.jpg', '<iframe src=\'http://www.youtube.com/embed/ZSxpyC8UYZk?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-11-09 19:26:01', '2017-05-17 14:37:01'),
(207, 70, 'Vanessa Rozan - Mãe também é gente', 'https://vimeo.com/145505013', 'vimeo', '145505013', '145505013.jpg', '<iframe src="//player.vimeo.com/video/145505013?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, '2015-11-12 12:53:31', '2016-12-09 18:04:11'),
(208, 77, 'Chiara gadaleta - A beleza a favor de todos.', 'https://vimeo.com/145506146', 'vimeo', '145506146', '145506146.jpg', '<iframe src="//player.vimeo.com/video/145506146?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, '2015-11-12 13:12:58', '2017-05-05 15:07:12'),
(209, 100, 'Luitha - Dia das mães Riachuelo', 'https://www.youtube.com/watch?v=CTigvjXycBo&list=PLcrydkWS_1zHz-m5hLPCn9HvZHDzTHhFv&index=6', 'youtube', 'CTigvjXycBo', 'CTigvjXycBo.jpg', '<iframe src=\'http://www.youtube.com/embed/CTigvjXycBo?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-11-13 17:21:08', '2016-08-22 15:34:00'),
(213, 70, 'Vanessa Rozan - Maria Garcia verão 2016', 'https://vimeo.com/146020081', 'vimeo', '146020081', '146020081.jpg', '<iframe src="//player.vimeo.com/video/146020081?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 1, '2015-11-17 18:07:24', '2016-12-09 18:04:11'),
(215, 70, 'Vanessa Rozan - Profissões de Moda', 'https://vimeo.com/146030919', 'vimeo', '146030919', '146030919.jpg', '<iframe src="//player.vimeo.com/video/146030919?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 2, '2015-11-18 17:46:27', '2016-12-09 18:04:11'),
(218, 95, 'Vanessa Rozan - No espelho dela', 'https://vimeo.com/146028396', 'vimeo', '146028396', '146028396.jpg', '<iframe src="//player.vimeo.com/video/146028396?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 2, '2015-11-18 19:49:29', '2015-12-07 18:13:06'),
(219, 70, 'Vanessa Rozan - Dia das bruxas', 'https://vimeo.com/146147521', 'vimeo', '146147521', '146147521.jpg', '<iframe src="//player.vimeo.com/video/146147521?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 4, '2015-11-18 20:45:26', '2016-12-09 18:04:11'),
(220, 34, 'Costanza Pascolato - Morena rosa Verão 2016', 'https://youtu.be/M4MdR54OOKQ', 'youtube', 'M4MdR54OOKQ', 'M4MdR54OOKQ.jpg', '<iframe src=\'http://www.youtube.com/embed/M4MdR54OOKQ?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2015-11-24 12:24:24', '2016-08-18 13:32:28'),
(222, 49, 'André Vasco - Miss Brasil 20014', 'https://youtu.be/uuOkOyX_Qos', 'youtube', 'uuOkOyX_Qos', 'uuOkOyX_Qos.jpg', '<iframe src=\'http://www.youtube.com/embed/uuOkOyX_Qos?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2015-11-24 19:23:21', '2016-12-08 15:03:29'),
(226, 23, 'Amanda Ramalo - 20 músicas com Carioca', 'https://youtu.be/oU1bjMJdtmk', 'youtube', 'oU1bjMJdtmk', 'oU1bjMJdtmk.jpg', '<iframe src=\'http://www.youtube.com/embed/oU1bjMJdtmk?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-11-24 21:04:45', '2015-11-24 21:04:45'),
(228, 103, 'Amanda Ramalho - 20 músicas com carioca', 'https://www.youtube.com/watch?v=oU1bjMJdtmk&index=3&list=PLcrydkWS_1zE-TQy4N97EygxP-qOk4TN1', 'youtube', 'oU1bjMJdtmk', 'oU1bjMJdtmk.jpg', '<iframe src=\'http://www.youtube.com/embed/oU1bjMJdtmk?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-11-24 21:09:05', '2015-11-24 21:09:05'),
(229, 103, 'Amanda Ramalho - Miss Brasil 2015 (Pânico na band)', 'https://www.youtube.com/watch?v=otVLQxDQECM&list=PLcrydkWS_1zE-TQy4N97EygxP-qOk4TN1&index=4', 'youtube', 'otVLQxDQECM', 'otVLQxDQECM.jpg', '<iframe src=\'http://www.youtube.com/embed/otVLQxDQECM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-11-24 21:09:05', '2015-11-24 21:09:05'),
(231, 34, 'Costanza Pascolato - Campanha verão 2016', 'https://youtu.be/YTpf7fRgxqY', 'youtube', 'YTpf7fRgxqY', 'YTpf7fRgxqY.jpg', '<iframe src=\'http://www.youtube.com/embed/YTpf7fRgxqY?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 5, '2015-11-25 16:49:50', '2016-08-18 13:32:28'),
(232, 90, 'Costanza Pascolato - Bastidores da campanha de verão - Morena Rosa', 'https://youtu.be/YTpf7fRgxqY', 'youtube', 'YTpf7fRgxqY', 'YTpf7fRgxqY.jpg', '<iframe src=\'http://www.youtube.com/embed/YTpf7fRgxqY?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-11-25 16:59:34', '2016-08-18 13:29:28'),
(238, 35, 'Daniella Cicarelli - Red Bull', 'https://www.youtube.com/watch?v=XnsC2GTxcU8&list=PLcrydkWS_1zFCFCH3X39sDuN2pWMaySXm&index=3', 'youtube', 'XnsC2GTxcU8', 'XnsC2GTxcU8.jpg', '<iframe src=\'http://www.youtube.com/embed/XnsC2GTxcU8?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 5, '2015-11-25 17:00:33', '2016-06-15 21:11:08'),
(239, 35, 'Daniella Cicarelli - Red Bull', 'https://www.youtube.com/watch?v=InpBMYDYa3c&list=PLcrydkWS_1zFCFCH3X39sDuN2pWMaySXm&index=4', 'youtube', 'InpBMYDYa3c', 'InpBMYDYa3c.jpg', '<iframe src=\'http://www.youtube.com/embed/InpBMYDYa3c?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 6, '2015-11-25 17:00:33', '2016-06-15 21:11:08'),
(240, 35, 'Daniella Cicarelli - Red Bull', 'https://www.youtube.com/watch?v=Q0Dq_XtPXeA&list=PLcrydkWS_1zFCFCH3X39sDuN2pWMaySXm&index=5', 'youtube', 'Q0Dq_XtPXeA', 'Q0Dq_XtPXeA.jpg', '<iframe src=\'http://www.youtube.com/embed/Q0Dq_XtPXeA?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 7, '2015-11-25 17:00:34', '2016-06-15 21:11:08'),
(241, 35, 'Daniella Cicarelli - Honda', 'https://www.youtube.com/watch?v=KRshKAPpYoU&list=PLcrydkWS_1zFCFCH3X39sDuN2pWMaySXm&index=7', 'youtube', 'KRshKAPpYoU', 'KRshKAPpYoU.jpg', '<iframe src=\'http://www.youtube.com/embed/KRshKAPpYoU?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2015-11-25 17:00:34', '2016-06-15 21:11:08'),
(242, 35, 'Daniella Cicarelli - Honda', 'https://www.youtube.com/watch?v=w4WvkZ4cY2s&list=PLcrydkWS_1zFCFCH3X39sDuN2pWMaySXm&index=8', 'youtube', 'w4WvkZ4cY2s', 'w4WvkZ4cY2s.jpg', '<iframe src=\'http://www.youtube.com/embed/w4WvkZ4cY2s?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-11-25 17:00:34', '2016-06-15 21:11:08'),
(243, 35, 'Daniella Cicarelli - Honda', 'https://www.youtube.com/watch?v=ML9eWr3bqf0&list=PLcrydkWS_1zFCFCH3X39sDuN2pWMaySXm&index=8', 'youtube', 'ML9eWr3bqf0', 'ML9eWr3bqf0.jpg', '<iframe src=\'http://www.youtube.com/embed/ML9eWr3bqf0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-11-25 17:22:24', '2016-06-15 21:11:08'),
(245, 38, 'Lígia Mendes - Programa QST', 'https://youtu.be/shOgElx83wE', 'youtube', 'shOgElx83wE', 'shOgElx83wE.jpg', '<iframe src=\'http://www.youtube.com/embed/shOgElx83wE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-11-26 18:19:00', '2015-11-26 18:19:00'),
(246, 42, 'Marilia Moreno - Mercedez Benz', 'https://youtu.be/7YNgiC0XSxo', 'youtube', '7YNgiC0XSxo', '7YNgiC0XSxo.jpg', '<iframe src=\'http://www.youtube.com/embed/7YNgiC0XSxo?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 5, '2015-11-26 18:24:24', '2017-01-30 21:39:44'),
(247, 42, 'Marília Moreno - Comercial Reel', 'https://youtu.be/zqANNjziwo8', 'youtube', 'zqANNjziwo8', 'zqANNjziwo8.jpg', '<iframe src=\'http://www.youtube.com/embed/zqANNjziwo8?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2015-11-26 18:31:25', '2017-01-30 21:39:44'),
(248, 47, 'Tatiana Dumenti - Programa Auto Esporte', 'https://youtu.be/sfuJeczwOWU?list=PLcrydkWS_1zHXZVOANz_rrGBZtWgxlvC_', 'youtube', 'sfuJeczwOWU', 'sfuJeczwOWU.jpg', '<iframe src=\'http://www.youtube.com/embed/sfuJeczwOWU?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-11-26 18:39:05', '2015-11-26 18:39:05'),
(249, 47, 'Tatiana Dumenti - Programa Auto Esporte', 'https://youtu.be/C7i1-1ILjEU?list=PLcrydkWS_1zHXZVOANz_rrGBZtWgxlvC_', 'youtube', 'C7i1-1ILjEU', 'C7i1-1ILjEU.jpg', '<iframe src=\'http://www.youtube.com/embed/C7i1-1ILjEU?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-11-26 18:47:28', '2015-11-26 18:47:28'),
(250, 47, 'Tatiana Dumenti - Play TV', 'https://youtu.be/zJNy4HUv1SY?list=PLcrydkWS_1zHXZVOANz_rrGBZtWgxlvC_', 'youtube', 'zJNy4HUv1SY', 'zJNy4HUv1SY.jpg', '<iframe src=\'http://www.youtube.com/embed/zJNy4HUv1SY?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-11-26 18:56:51', '2015-11-26 18:56:51'),
(251, 56, 'Pedro Lourenço - Making Off Nike', 'https://youtu.be/SQw3M9xE420', 'youtube', 'SQw3M9xE420', 'SQw3M9xE420.jpg', '<iframe src=\'http://www.youtube.com/embed/SQw3M9xE420?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-11-26 19:42:05', '2015-11-26 19:42:05'),
(253, 56, 'Pedro Lourenço - Mac Cosmetcs', 'https://youtu.be/DBDAGUn3gI4', 'youtube', 'DBDAGUn3gI4', 'DBDAGUn3gI4.jpg', '<iframe src=\'http://www.youtube.com/embed/DBDAGUn3gI4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-11-26 19:54:56', '2015-12-02 18:22:48'),
(254, 87, 'Pedro Lourenço - Making Off Nike', 'https://www.youtube.com/watch?v=SQw3M9xE420&index=1&list=PLcrydkWS_1zGWBv4RSU3ADEO777uHt3k5', 'youtube', 'SQw3M9xE420', 'SQw3M9xE420.jpg', '<iframe src=\'http://www.youtube.com/embed/SQw3M9xE420?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-11-26 19:58:23', '2015-11-26 19:58:23'),
(255, 87, 'Pedro Lourenço - Spring Summer', 'https://www.youtube.com/watch?v=VmnLRKMoXiM&index=2&list=PLcrydkWS_1zGWBv4RSU3ADEO777uHt3k5', 'youtube', 'VmnLRKMoXiM', 'VmnLRKMoXiM.jpg', '<iframe src=\'http://www.youtube.com/embed/VmnLRKMoXiM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-11-26 19:58:23', '2015-11-26 19:58:23'),
(256, 87, 'Pedro Lourenço - Mac Cosmetcs', 'https://www.youtube.com/watch?v=DBDAGUn3gI4&index=3&list=PLcrydkWS_1zGWBv4RSU3ADEO777uHt3k5', 'youtube', 'DBDAGUn3gI4', 'DBDAGUn3gI4.jpg', '<iframe src=\'http://www.youtube.com/embed/DBDAGUn3gI4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-11-26 19:58:24', '2015-11-26 19:58:24'),
(257, 42, 'Marilia Moreno - Campanha Carls Jr Burguer', 'https://www.youtube.com/watch?v=T8qkhJoH_7A', 'youtube', 'T8qkhJoH_7A', 'T8qkhJoH_7A.jpg', '<iframe src=\'http://www.youtube.com/embed/T8qkhJoH_7A?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 6, '2015-11-26 21:46:00', '2017-01-30 21:39:44'),
(258, 49, 'André Vasco - programa  8 minutos com Rafinha Bastos', 'https://www.youtube.com/watch?v=8Jj1OXfTDZQ&index=11&list=PLcrydkWS_1zEtKrpqmEIhqra-ILfJLLnO', 'youtube', '8Jj1OXfTDZQ', '8Jj1OXfTDZQ.jpg', '<iframe src=\'http://www.youtube.com/embed/8Jj1OXfTDZQ?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 11, '2015-11-27 17:22:57', '2016-12-08 15:03:29'),
(259, 125, 'Silvia Poppovic - Jornal da Manhã  - Rádio Jovem Pan', 'https://www.youtube.com/watch?v=YZMh4ylzJFA&list=PLcrydkWS_1zEFk6SvwEfTUXkzxc3WHk6m&index=6', 'youtube', 'YZMh4ylzJFA', 'YZMh4ylzJFA.jpg', '<iframe src=\'http://www.youtube.com/embed/YZMh4ylzJFA?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-11-27 20:49:37', '2015-11-27 20:49:37'),
(260, 125, 'Silvia Poppovic - Jornal da Manhã - Rádio Jovem Pan', 'https://www.youtube.com/watch?v=fotTeLRhJh8&list=PLcrydkWS_1zEFk6SvwEfTUXkzxc3WHk6m&index=5', 'youtube', 'fotTeLRhJh8', 'fotTeLRhJh8.jpg', '<iframe src=\'http://www.youtube.com/embed/fotTeLRhJh8?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-11-27 20:49:38', '2015-11-27 20:49:38'),
(261, 44, 'Silvia Poppovic - Já pensou Nisso', 'https://www.youtube.com/watch?v=WuJqBuzSHQk&list=PLcrydkWS_1zEFk6SvwEfTUXkzxc3WHk6m&index=4', 'youtube', 'WuJqBuzSHQk', 'WuJqBuzSHQk.jpg', '<iframe src=\'http://www.youtube.com/embed/WuJqBuzSHQk?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-11-27 20:51:03', '2015-11-27 20:51:03'),
(262, 84, 'Silvia Poppovic - Jornal da Manhã - Radio Jovem Pan', 'https://www.youtube.com/watch?v=YZMh4ylzJFA&list=PLcrydkWS_1zEFk6SvwEfTUXkzxc3WHk6m&index=6', 'youtube', 'YZMh4ylzJFA', 'YZMh4ylzJFA.jpg', '<iframe src=\'http://www.youtube.com/embed/YZMh4ylzJFA?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-11-27 20:52:37', '2015-11-27 20:52:37'),
(263, 71, 'Duda Molinos - Gisele Bundchen Vivara', 'https://youtu.be/B3v7-Lr-q9E', 'youtube', 'B3v7-Lr-q9E', 'B3v7-Lr-q9E.jpg', '<iframe src=\'http://www.youtube.com/embed/B3v7-Lr-q9E?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-11-30 18:01:08', '2015-12-07 14:39:47'),
(264, 71, 'Duda Molinos - Gisele Bundchen Vivara II', 'https://youtu.be/-eF4p3k7X9s', 'youtube', '-eF4p3k7X9s', '-eF4p3k7X9s.jpg', '<iframe src=\'http://www.youtube.com/embed/-eF4p3k7X9s?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 5, '2015-11-30 18:34:17', '2015-12-07 14:39:47'),
(265, 71, 'Duda Molinos - Camila Pitanga Avon', 'https://youtu.be/EVY8BD6LayQ', 'youtube', 'EVY8BD6LayQ', 'EVY8BD6LayQ.jpg', '<iframe src=\'http://www.youtube.com/embed/EVY8BD6LayQ?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2015-11-30 18:34:17', '2015-12-07 14:39:47'),
(266, 71, 'Duda Molinos - Fernanda Yong Nextel', 'https://www.youtube.com/watch?v=Cp72J7xPBNo&index=10&list=PLB2B650F795FCC553', 'youtube', 'Cp72J7xPBNo', 'Cp72J7xPBNo.jpg', '<iframe src=\'http://www.youtube.com/embed/Cp72J7xPBNo?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 6, '2015-11-30 18:34:17', '2015-12-07 14:39:47'),
(267, 71, 'Duda Molinos - Gisele Bundchen para Pantene', 'https://youtu.be/nxUkw3Ni8tQ', 'youtube', 'nxUkw3Ni8tQ', 'nxUkw3Ni8tQ.jpg', '<iframe src=\'http://www.youtube.com/embed/nxUkw3Ni8tQ?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 7, '2015-11-30 20:06:10', '2015-12-07 14:39:47'),
(268, 71, 'Duda Molinos - Campanha Santa Lolla', 'https://youtu.be/Ism-qZKrDKM', 'youtube', 'Ism-qZKrDKM', 'Ism-qZKrDKM.jpg', '<iframe src=\'http://www.youtube.com/embed/Ism-qZKrDKM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-11-30 20:06:11', '2015-12-07 14:39:47'),
(269, 71, 'Duda Molinos - Construcard Caixa, com Camila Pitanga', 'https://youtu.be/XQC6bJF8k8k', 'youtube', 'XQC6bJF8k8k', 'XQC6bJF8k8k.jpg', '<iframe src=\'http://www.youtube.com/embed/XQC6bJF8k8k?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 9, '2015-11-30 20:47:42', '2015-12-07 14:39:47'),
(270, 71, 'Duda Molinos - Imedia Loreal, com Camila Pitanga', 'https://youtu.be/HJTiJNY9ZPE', 'youtube', 'HJTiJNY9ZPE', 'HJTiJNY9ZPE.jpg', '<iframe src=\'http://www.youtube.com/embed/HJTiJNY9ZPE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 8, '2015-11-30 20:47:42', '2015-12-07 14:39:47'),
(271, 71, 'Duda Molinos - Campanha Filme Algar Telecom', 'https://youtu.be/HloMsfUy8cU', 'youtube', 'HloMsfUy8cU', 'HloMsfUy8cU.jpg', '<iframe src=\'http://www.youtube.com/embed/HloMsfUy8cU?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 10, '2015-11-30 20:47:44', '2015-12-07 14:39:47'),
(272, 71, 'Duda Molinos- ', 'https://youtu.be/ScpjjMKLq2A', 'youtube', 'ScpjjMKLq2A', 'ScpjjMKLq2A.jpg', '<iframe src=\'http://www.youtube.com/embed/ScpjjMKLq2A?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 11, '2015-11-30 21:03:16', '2015-12-07 14:39:47'),
(273, 63, 'Erika Palomino - Apresentação Oscar  Canal TNT', 'https://www.youtube.com/watch?v=bEmKedSgMvo&index=2&list=PLcrydkWS_1zF24nUCxV2xA7neTnfu1PFP', 'youtube', 'bEmKedSgMvo', 'bEmKedSgMvo.jpg', '<iframe src=\'http://www.youtube.com/embed/bEmKedSgMvo?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-12-01 14:30:37', '2015-12-01 14:30:37'),
(274, 112, 'Erika Palomino - Apresentação Oscar - TNT', 'https://youtu.be/bEmKedSgMvo?list=PLcrydkWS_1zF24nUCxV2xA7neTnfu1PFP', 'youtube', 'bEmKedSgMvo', 'bEmKedSgMvo.jpg', '<iframe src=\'http://www.youtube.com/embed/bEmKedSgMvo?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-12-01 14:32:27', '2015-12-01 14:32:27'),
(275, 97, 'Silvia Poppovic - Jornal da Manhã -Radio Jovem Pan', 'https://www.youtube.com/watch?v=YZMh4ylzJFA&index=6&list=PLcrydkWS_1zEFk6SvwEfTUXkzxc3WHk6m', 'youtube', 'YZMh4ylzJFA', 'YZMh4ylzJFA.jpg', '<iframe src=\'http://www.youtube.com/embed/YZMh4ylzJFA?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-12-01 15:10:47', '2015-12-01 15:10:47'),
(276, 97, 'Silvia Poppovic - Canal Já Pensou Nisso', 'https://youtu.be/EnYpdPqUkZ0?list=PLcrydkWS_1zEFk6SvwEfTUXkzxc3WHk6m', 'youtube', 'EnYpdPqUkZ0', 'EnYpdPqUkZ0.jpg', '<iframe src=\'http://www.youtube.com/embed/EnYpdPqUkZ0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-12-01 15:13:18', '2015-12-01 15:13:18'),
(277, 97, 'Silvia Poppovic - Canal Já Pensou Nisso', 'https://www.youtube.com/watch?v=jtkt7INg7Dk&index=1&list=PLcrydkWS_1zEFk6SvwEfTUXkzxc3WHk6m', 'youtube', 'jtkt7INg7Dk', 'jtkt7INg7Dk.jpg', '<iframe src=\'http://www.youtube.com/embed/jtkt7INg7Dk?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-12-01 15:13:18', '2015-12-01 15:13:18'),
(280, 54, 'Gloria Coelho - SPFW 2016', 'https://youtu.be/QSD5Abx4vBY?list=PLcrydkWS_1zG3DB9-UCusw0Qi4_risXfK', 'youtube', 'QSD5Abx4vBY', 'QSD5Abx4vBY.jpg', '<iframe src=\'http://www.youtube.com/embed/QSD5Abx4vBY?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-12-01 18:50:56', '2015-12-01 18:50:56'),
(281, 54, 'Gloria Coelho - 2016', 'https://youtu.be/KsUVnP8qyQ0?list=PLcrydkWS_1zG3DB9-UCusw0Qi4_risXfK', 'youtube', 'KsUVnP8qyQ0', 'KsUVnP8qyQ0.jpg', '<iframe src=\'http://www.youtube.com/embed/KsUVnP8qyQ0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-12-01 18:50:56', '2015-12-01 18:50:56'),
(282, 54, 'Gloria Coelho - Lookbook', 'https://youtu.be/HnLwr5twdA4?list=PLcrydkWS_1zG3DB9-UCusw0Qi4_risXfK', 'youtube', 'HnLwr5twdA4', 'HnLwr5twdA4.jpg', '<iframe src=\'http://www.youtube.com/embed/HnLwr5twdA4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-12-01 18:50:56', '2015-12-01 18:50:56'),
(283, 54, 'Gloria Coelho - Pílulas da Longevidade', 'https://youtu.be/1ZpNz9yHb54?list=PLcrydkWS_1zG3DB9-UCusw0Qi4_risXfK', 'youtube', '1ZpNz9yHb54', '1ZpNz9yHb54.jpg', '<iframe src=\'http://www.youtube.com/embed/1ZpNz9yHb54?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-12-01 18:50:57', '2015-12-01 18:50:57'),
(284, 54, 'Gloria Coelho - entrevista para Donata Meirelles', 'https://youtu.be/oEO3gU27bLw?list=PLcrydkWS_1zG3DB9-UCusw0Qi4_risXfK', 'youtube', 'oEO3gU27bLw', 'oEO3gU27bLw.jpg', '<iframe src=\'http://www.youtube.com/embed/oEO3gU27bLw?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2015-12-01 18:50:57', '2015-12-01 18:50:57'),
(287, 78, 'Regina Guerreiro - Um ano de Enjoy', 'https://youtu.be/MIefPRiBNPs', 'youtube', 'MIefPRiBNPs', 'MIefPRiBNPs.jpg', '<iframe src=\'http://www.youtube.com/embed/MIefPRiBNPs?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-12-01 20:00:15', '2015-12-01 20:00:15'),
(288, 78, 'Regina Guerreiro - Tá na mão ou não', 'https://youtu.be/Ly8bHL88x-U', 'youtube', 'Ly8bHL88x-U', 'Ly8bHL88x-U.jpg', '<iframe src=\'http://www.youtube.com/embed/Ly8bHL88x-U?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-12-01 20:00:16', '2015-12-01 20:00:16'),
(289, 78, 'Regina Guerreiro - Força na peruca', 'https://youtu.be/REapRCoRRfY', 'youtube', 'REapRCoRRfY', 'REapRCoRRfY.jpg', '<iframe src=\'http://www.youtube.com/embed/REapRCoRRfY?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2015-12-01 20:00:16', '2015-12-01 20:00:16'),
(290, 93, 'Regina Guerreiro - Palestra FAAP', 'https://youtu.be/ziP1Ltw-V-E', 'youtube', 'ziP1Ltw-V-E', 'ziP1Ltw-V-E.jpg', '<iframe src=\'http://www.youtube.com/embed/ziP1Ltw-V-E?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-12-01 20:40:37', '2015-12-01 20:40:37'),
(291, 58, 'Rodrigo Oliveira - Restaurante e cachaçaria', 'https://www.youtube.com/watch?v=AZ9EsURp7uY', 'youtube', 'AZ9EsURp7uY', 'AZ9EsURp7uY.jpg', '<iframe src=\'http://www.youtube.com/embed/AZ9EsURp7uY?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-12-01 20:54:22', '2015-12-01 20:54:22'),
(292, 109, 'Rodrigo Oliveira - Restaurante e Cachaçaria', 'https://www.youtube.com/watch?v=AZ9EsURp7uY', 'youtube', 'AZ9EsURp7uY', 'AZ9EsURp7uY.jpg', '<iframe src=\'http://www.youtube.com/embed/AZ9EsURp7uY?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-12-01 20:55:10', '2015-12-01 20:55:10'),
(293, 124, 'Luciana Liviero - Domingo Espetacular - TV Record', 'https://youtu.be/m_2WGXmntg8?list=PLcrydkWS_1zERYvm906CeQ3yWT-aetpmw', 'youtube', 'm_2WGXmntg8', 'm_2WGXmntg8.jpg', '<iframe src=\'http://www.youtube.com/embed/m_2WGXmntg8?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-12-01 21:03:18', '2015-12-01 21:03:18'),
(294, 124, 'Luciana Liviero - Domingo Espetacular - TV Record', 'https://youtu.be/Ql8jg_THqXo?list=PLcrydkWS_1zERYvm906CeQ3yWT-aetpmw', 'youtube', 'Ql8jg_THqXo', 'Ql8jg_THqXo.jpg', '<iframe src=\'http://www.youtube.com/embed/Ql8jg_THqXo?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-12-01 21:03:19', '2015-12-01 21:03:19'),
(295, 58, 'Rodrigo Oliveira - Mocotó no SBT Repórter', 'https://youtu.be/zrW1v38YrwM', 'youtube', 'zrW1v38YrwM', 'zrW1v38YrwM.jpg', '<iframe src=\'http://www.youtube.com/embed/zrW1v38YrwM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-12-02 12:41:30', '2015-12-02 12:41:30'),
(296, 109, 'Rodrigo Oliveira - Mocotó no SBT Repórter', 'https://youtu.be/zrW1v38YrwM', 'youtube', 'zrW1v38YrwM', 'zrW1v38YrwM.jpg', '<iframe src=\'http://www.youtube.com/embed/zrW1v38YrwM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-12-02 12:43:24', '2015-12-02 12:43:24'),
(297, 58, 'Rodrigo Oliveira - Programa Ronnie Von', 'https://youtu.be/Yed0hSkF8Bw', 'youtube', 'Yed0hSkF8Bw', 'Yed0hSkF8Bw.jpg', '<iframe src=\'http://www.youtube.com/embed/Yed0hSkF8Bw?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-12-02 12:58:28', '2015-12-02 12:58:28'),
(298, 109, 'Rodrigo Oliveira - Program Ronnie Von', 'https://youtu.be/Yed0hSkF8Bw', 'youtube', 'Yed0hSkF8Bw', 'Yed0hSkF8Bw.jpg', '<iframe src=\'http://www.youtube.com/embed/Yed0hSkF8Bw?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-12-02 12:59:10', '2015-12-02 12:59:10'),
(299, 58, 'Rodrigo Oliveira', 'https://youtu.be/zrl_aMj3k_I', 'youtube', 'zrl_aMj3k_I', 'zrl_aMj3k_I.jpg', '<iframe src=\'http://www.youtube.com/embed/zrl_aMj3k_I?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-12-02 13:13:07', '2015-12-02 13:13:07'),
(300, 109, 'Rodrigo Oliveira', 'https://youtu.be/zrl_aMj3k_I', 'youtube', 'zrl_aMj3k_I', 'zrl_aMj3k_I.jpg', '<iframe src=\'http://www.youtube.com/embed/zrl_aMj3k_I?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-12-02 13:13:40', '2015-12-02 13:13:40'),
(301, 58, 'Rodrigo Oliveira - KLM em parceria com o Mocotó', 'https://youtu.be/b4V28V4fnK4', 'youtube', 'b4V28V4fnK4', 'b4V28V4fnK4.jpg', '<iframe src=\'http://www.youtube.com/embed/b4V28V4fnK4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2015-12-02 13:29:19', '2015-12-02 13:29:19'),
(302, 109, 'Rodrigo Oliveira - KLM em parceria com o Mocotó', 'https://youtu.be/b4V28V4fnK4', 'youtube', 'b4V28V4fnK4', 'b4V28V4fnK4.jpg', '<iframe src=\'http://www.youtube.com/embed/b4V28V4fnK4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2015-12-02 13:30:12', '2015-12-02 13:30:12'),
(304, 71, 'Duda Molinos - Comercial sadia com Fernanda Montenegro e Fernanda Torres', 'https://youtu.be/DXE3eF4Feiw', 'youtube', 'DXE3eF4Feiw', 'DXE3eF4Feiw.jpg', '<iframe src=\'http://www.youtube.com/embed/DXE3eF4Feiw?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 12, '2015-12-02 17:03:58', '2015-12-07 14:39:47'),
(305, 71, 'Duda Molinos - Campanha Gisele Bundchen', 'https://youtu.be/N6LiTJOQmpw', 'youtube', 'N6LiTJOQmpw', 'N6LiTJOQmpw.jpg', '<iframe src=\'http://www.youtube.com/embed/N6LiTJOQmpw?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-12-02 17:14:08', '2015-12-07 14:39:47'),
(306, 56, 'Pedro Lourenço - Semana de moda em Paris', 'https://youtu.be/BMi_S4Y0kAE', 'youtube', 'BMi_S4Y0kAE', 'BMi_S4Y0kAE.jpg', '<iframe src=\'http://www.youtube.com/embed/BMi_S4Y0kAE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-12-02 18:10:08', '2015-12-02 18:22:48');
INSERT INTO `portfolio_videos` (`id`, `portfolio_personalidades_id`, `titulo`, `url`, `servico`, `servico_video_id`, `thumbnail`, `embed`, `ordem`, `created_at`, `updated_at`) VALUES
(307, 71, 'Duda Molinos - Semana de moda em Paris', 'https://youtu.be/BMi_S4Y0kAE', 'youtube', 'BMi_S4Y0kAE', 'BMi_S4Y0kAE.jpg', '<iframe src=\'http://www.youtube.com/embed/BMi_S4Y0kAE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 13, '2015-12-02 18:10:56', '2015-12-02 18:10:56'),
(308, 66, 'Mona Dorf - Entrevista Programa Todo Seu', 'https://youtu.be/F3hsNOrSv7Q?list=PLcrydkWS_1zHyaJzN2H2F8BtpjnSlUuTD', 'youtube', 'F3hsNOrSv7Q', 'F3hsNOrSv7Q.jpg', '<iframe src=\'http://www.youtube.com/embed/F3hsNOrSv7Q?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-12-02 20:31:12', '2015-12-02 20:31:12'),
(309, 53, 'Dudu Bertholini - Apresentação Oscar - TNT', 'https://youtu.be/bEmKedSgMvo?list=PLcrydkWS_1zF24nUCxV2xA7neTnfu1PFP', 'youtube', 'bEmKedSgMvo', 'bEmKedSgMvo.jpg', '<iframe src=\'http://www.youtube.com/embed/bEmKedSgMvo?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 5, '2015-12-02 21:01:33', '2017-05-17 14:37:01'),
(311, 101, 'Tutti Muller - Comercial da OI', 'https://youtu.be/4jqzDOT6LzE', 'youtube', '4jqzDOT6LzE', '4jqzDOT6LzE.jpg', '<iframe src=\'http://www.youtube.com/embed/4jqzDOT6LzE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-12-03 20:05:18', '2016-08-23 18:01:11'),
(312, 101, 'Tutti Muller - Longe do reino', 'https://youtu.be/DlZz7CnySXE', 'youtube', 'DlZz7CnySXE', 'DlZz7CnySXE.jpg', '<iframe src=\'http://www.youtube.com/embed/DlZz7CnySXE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-12-03 20:31:04', '2016-08-23 18:01:11'),
(313, 101, 'Tutti Muller - Vivo Valoriza', 'https://youtu.be/iD5lzKcmAbU', 'youtube', 'iD5lzKcmAbU', 'iD5lzKcmAbU.jpg', '<iframe src=\'http://www.youtube.com/embed/iD5lzKcmAbU?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2015-12-03 20:48:46', '2016-08-23 18:01:11'),
(314, 24, 'Astrid Fontenelle - Encerramento da MTV', 'https://youtu.be/n1TwoujJQiI', 'youtube', 'n1TwoujJQiI', 'n1TwoujJQiI.jpg', '<iframe src=\'http://www.youtube.com/embed/n1TwoujJQiI?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 6, '2015-12-04 14:19:05', '2015-12-09 19:18:41'),
(316, 24, 'Astrid Fontenelle - Chegadas e Partidas', 'https://youtu.be/rsP2K640Jms', 'youtube', 'rsP2K640Jms', 'rsP2K640Jms.jpg', '<iframe src=\'http://www.youtube.com/embed/rsP2K640Jms?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-12-04 14:45:35', '2015-12-09 19:17:18'),
(317, 24, 'Astrid e Bárbara Gancia - GNT', 'https://youtu.be/ofz9boEA4CM', 'youtube', 'ofz9boEA4CM', 'ofz9boEA4CM.jpg', '<iframe src=\'http://www.youtube.com/embed/ofz9boEA4CM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-12-04 16:58:43', '2015-12-09 19:17:18'),
(318, 31, 'Bárbara Gancia e Astrid - GNT', 'https://youtu.be/ofz9boEA4CM', 'youtube', 'ofz9boEA4CM', 'ofz9boEA4CM.jpg', '<iframe src=\'http://www.youtube.com/embed/ofz9boEA4CM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-12-04 16:59:34', '2015-12-04 16:59:34'),
(319, 82, 'Bárbara Gancia - Sem filtro', 'https://youtu.be/LBsQ6_VF7w0', 'youtube', 'LBsQ6_VF7w0', 'LBsQ6_VF7w0.jpg', '<iframe src=\'http://www.youtube.com/embed/LBsQ6_VF7w0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-12-04 17:23:02', '2015-12-04 17:23:02'),
(320, 82, 'Bárbara Gancia - Entrevista Toninho do diabo', 'https://youtu.be/4lilesJcmcY', 'youtube', '4lilesJcmcY', '4lilesJcmcY.jpg', '<iframe src=\'http://www.youtube.com/embed/4lilesJcmcY?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-12-04 17:41:53', '2015-12-04 18:03:54'),
(321, 82, 'Bárbara Gancia - Chefe da CIA', 'https://youtu.be/z8AYA6-WPVs', 'youtube', 'z8AYA6-WPVs', 'z8AYA6-WPVs.jpg', '<iframe src=\'http://www.youtube.com/embed/z8AYA6-WPVs?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-12-04 17:49:28', '2015-12-04 18:03:54'),
(322, 82, 'Barbara Gancia - entrevista com Caco, o sapo dos Muppets', 'https://youtu.be/psM9pFoJkQk', 'youtube', 'psM9pFoJkQk', 'psM9pFoJkQk.jpg', '<iframe src=\'http://www.youtube.com/embed/psM9pFoJkQk?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-12-04 18:03:54', '2015-12-04 18:03:54'),
(323, 65, 'Guga Chacra - fala sobre intervenção de Obama na Síria', 'https://youtu.be/ucdPHMgvdZM', 'youtube', 'ucdPHMgvdZM', 'ucdPHMgvdZM.jpg', '<iframe src=\'http://www.youtube.com/embed/ucdPHMgvdZM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-12-04 18:46:01', '2015-12-04 18:46:01'),
(324, 65, 'Guga Chacra - Vertentes do Islamismo', 'https://youtu.be/_5tjTowDuDk', 'youtube', '_5tjTowDuDk', '_5tjTowDuDk.jpg', '<iframe src=\'http://www.youtube.com/embed/_5tjTowDuDk?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-12-04 18:46:01', '2015-12-04 18:46:01'),
(325, 65, 'Guga Chacra - Globo News', 'https://youtu.be/u7Yed9x-KKs', 'youtube', 'u7Yed9x-KKs', 'u7Yed9x-KKs.jpg', '<iframe src=\'http://www.youtube.com/embed/u7Yed9x-KKs?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-12-04 18:46:03', '2015-12-04 18:46:03'),
(326, 65, 'Guga Chacra - jornal Globo News', 'https://youtu.be/eCALNuTk8Js', 'youtube', 'eCALNuTk8Js', 'eCALNuTk8Js.jpg', '<iframe src=\'http://www.youtube.com/embed/eCALNuTk8Js?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-12-04 18:50:52', '2015-12-04 18:50:52'),
(327, 58, 'Rodrigo Oliveira - Mousse de chocolate com geléia de Pimentaa', 'https://youtu.be/mnWcn__JXUM', 'youtube', 'mnWcn__JXUM', 'mnWcn__JXUM.jpg', '<iframe src=\'http://www.youtube.com/embed/mnWcn__JXUM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 5, '2015-12-04 21:02:56', '2015-12-04 21:02:56'),
(331, 96, 'Duda Molinos - Lab Duda Molinos 2', 'https://youtu.be/FV5u7QO54N8', 'youtube', 'FV5u7QO54N8', 'FV5u7QO54N8.jpg', '<iframe src=\'http://www.youtube.com/embed/FV5u7QO54N8?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-12-07 17:02:27', '2015-12-07 17:02:27'),
(332, 96, 'Duda Molinos - Passo a passo Lab. Duda Molinos', 'https://youtu.be/PH6SrJNle34', 'youtube', 'PH6SrJNle34', 'PH6SrJNle34.jpg', '<iframe src=\'http://www.youtube.com/embed/PH6SrJNle34?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-12-07 17:02:28', '2015-12-07 17:02:28'),
(333, 95, 'Vanessa Rozan - Passo a passo Maybelline', 'https://www.youtube.com/watch?v=7XrgmfWhAEk&index=4&list=PL5E026FC17BD96741', 'youtube', '7XrgmfWhAEk', '7XrgmfWhAEk.jpg', '<iframe src=\'http://www.youtube.com/embed/7XrgmfWhAEk?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-12-07 17:33:52', '2015-12-07 18:13:06'),
(334, 95, 'Vanessa Rozan - Programa Esquadrão da Moda', 'https://youtu.be/b95ubEDbCgA', 'youtube', 'b95ubEDbCgA', 'b95ubEDbCgA.jpg', '<iframe src=\'http://www.youtube.com/embed/b95ubEDbCgA?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-12-07 18:13:06', '2015-12-07 18:13:06'),
(335, 108, 'Josimar Mello - Entrevista a chef Renata Vanzetto', 'https://www.youtube.com/watch?v=rEva1aD6mGg', 'youtube', 'rEva1aD6mGg', 'rEva1aD6mGg.jpg', '<iframe src=\'http://www.youtube.com/embed/rEva1aD6mGg?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-12-07 18:46:35', '2015-12-07 18:46:35'),
(336, 108, 'Josimar Mello - Entrevista com Erick Jacquin', 'https://www.youtube.com/watch?v=l6dXmBprM1Q', 'youtube', 'l6dXmBprM1Q', 'l6dXmBprM1Q.jpg', '<iframe src=\'http://www.youtube.com/embed/l6dXmBprM1Q?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-12-07 18:46:35', '2015-12-07 18:46:35'),
(337, 99, 'Josimar Mello - Comer o que?', 'https://www.youtube.com/watch?v=ErHnHY4qQ2M', 'youtube', 'ErHnHY4qQ2M', 'ErHnHY4qQ2M.jpg', '<iframe src=\'http://www.youtube.com/embed/ErHnHY4qQ2M?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-12-07 18:53:03', '2015-12-07 18:53:03'),
(339, 51, 'Pedro Andrade - Reel de apresentação', 'https://vimeo.com/148132252', 'vimeo', '148132252', '148132252.jpg', '<iframe src="//player.vimeo.com/video/148132252?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 2, '2015-12-07 19:35:19', '2016-02-04 13:14:26'),
(341, 66, 'Mona Dorf - Mini TV entrevista Mona Dorf', 'https://youtu.be/m4nUjCdKYHs', 'youtube', 'm4nUjCdKYHs', 'm4nUjCdKYHs.jpg', '<iframe src=\'http://www.youtube.com/embed/m4nUjCdKYHs?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-12-08 13:22:46', '2015-12-08 13:22:46'),
(342, 66, 'Mona Dorf - chefe na cozinha', 'https://youtu.be/iHDP3ScN4a0', 'youtube', 'iHDP3ScN4a0', 'iHDP3ScN4a0.jpg', '<iframe src=\'http://www.youtube.com/embed/iHDP3ScN4a0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-12-08 13:22:48', '2015-12-08 13:22:48'),
(343, 66, 'Mona Dorf - Com Adrina Lessa', 'https://youtu.be/Hy1GdxDqAKU', 'youtube', 'Hy1GdxDqAKU', 'Hy1GdxDqAKU.jpg', '<iframe src=\'http://www.youtube.com/embed/Hy1GdxDqAKU?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2015-12-08 13:22:48', '2015-12-08 13:22:48'),
(344, 66, 'Mona Dorf - Sorriso do bem 2012', 'https://youtu.be/pzY-w73JYaw', 'youtube', 'pzY-w73JYaw', 'pzY-w73JYaw.jpg', '<iframe src=\'http://www.youtube.com/embed/pzY-w73JYaw?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 5, '2015-12-08 13:22:48', '2015-12-08 13:22:48'),
(345, 68, 'Rodrigo Bocardi - Estréia no Bom dia São Paulo', 'https://www.youtube.com/watch?v=CHDdEDr16IE&list=PLcrydkWS_1zGs39O8UyPOpovZFMvr_MnK&index=1', 'youtube', 'CHDdEDr16IE', 'CHDdEDr16IE.jpg', '<iframe src=\'http://www.youtube.com/embed/CHDdEDr16IE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-12-08 13:38:02', '2015-12-08 13:38:02'),
(346, 67, 'Monica Waldvogel - Programa "Entre aspas" (Globo News)', 'https://youtu.be/wDxlwYF2WeM', 'youtube', 'wDxlwYF2WeM', 'wDxlwYF2WeM.jpg', '<iframe src=\'http://www.youtube.com/embed/wDxlwYF2WeM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-12-08 13:44:48', '2017-07-24 18:47:03'),
(347, 21, 'Paula Picarelli - Entrelinhas', 'https://www.youtube.com/watch?v=kZpWYOI7tqM', 'youtube', 'kZpWYOI7tqM', 'kZpWYOI7tqM.jpg', '<iframe src=\'http://www.youtube.com/embed/kZpWYOI7tqM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-12-08 18:23:59', '2015-12-08 18:23:59'),
(348, 21, 'Paula Picarelli - O jardim', 'https://youtu.be/VkO_JR8emIY', 'youtube', 'VkO_JR8emIY', 'VkO_JR8emIY.jpg', '<iframe src=\'http://www.youtube.com/embed/VkO_JR8emIY?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-12-08 18:23:59', '2015-12-08 18:23:59'),
(349, 21, 'Paula Picarelli - O Escuro', 'https://youtu.be/J_8D22zmSDY', 'youtube', 'J_8D22zmSDY', 'J_8D22zmSDY.jpg', '<iframe src=\'http://www.youtube.com/embed/J_8D22zmSDY?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-12-08 18:24:00', '2015-12-08 18:24:00'),
(350, 129, 'Lúcia Bronstein - O Sistema', 'https://youtu.be/tHxP9ROWIi8', 'youtube', 'tHxP9ROWIi8', 'tHxP9ROWIi8.jpg', '<iframe src=\'http://www.youtube.com/embed/tHxP9ROWIi8?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-12-08 19:29:11', '2015-12-09 18:26:09'),
(351, 101, 'Tutti Muller - Celebre Antônio', 'https://vimeo.com/148255736?utm_source=email&utm_medium=vimeo-cliptranscode-201504&utm_campaign=29220', 'vimeo', '148255736', '148255736.jpg', '<iframe src="//player.vimeo.com/video/148255736?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 2, '2015-12-09 12:14:06', '2016-08-23 18:01:11'),
(352, 132, 'Sérgio Menezes - Clipe Sibéria', 'https://youtu.be/cvXPbTIo2xA', 'youtube', 'cvXPbTIo2xA', 'cvXPbTIo2xA.jpg', '<iframe src=\'http://www.youtube.com/embed/cvXPbTIo2xA?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2015-12-09 12:50:49', '2015-12-10 16:37:36'),
(353, 132, 'Sérgio Menezes - novela Amor e Intrigas', 'https://youtu.be/yT9BANE-w7M', 'youtube', 'yT9BANE-w7M', 'yT9BANE-w7M.jpg', '<iframe src=\'http://www.youtube.com/embed/yT9BANE-w7M?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2015-12-09 13:36:57', '2015-12-10 16:37:36'),
(354, 132, 'Sérgio Menezes - Sibéria Tonight Oficial', 'https://youtu.be/9xKhOyoBuRU', 'youtube', '9xKhOyoBuRU', '9xKhOyoBuRU.jpg', '<iframe src=\'http://www.youtube.com/embed/9xKhOyoBuRU?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 5, '2015-12-09 13:36:57', '2015-12-10 16:37:36'),
(356, 129, 'Lucia Bronstein - Trabalhos de amores quase perdidos', 'https://vimeo.com/148344349', 'vimeo', '148344349', '148344349.jpg', '<iframe src="//player.vimeo.com/video/148344349?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 0, '2015-12-09 17:25:05', '2015-12-09 17:25:05'),
(357, 132, 'Sérgio Menezes - Namíbia', 'https://youtu.be/94JtUB6Dmmw', 'youtube', '94JtUB6Dmmw', '94JtUB6Dmmw.jpg', '<iframe src=\'http://www.youtube.com/embed/94JtUB6Dmmw?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2015-12-09 17:37:09', '2015-12-10 15:40:11'),
(358, 129, 'Róza', 'https://vimeo.com/89511717', 'vimeo', '89511717', '89511717.jpg', '<iframe src="//player.vimeo.com/video/89511717?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, '2015-12-09 18:04:06', '2015-12-09 18:26:09'),
(359, 129, 'Nu - Quem ', 'https://www.youtube.com/watch?v=mwEube0bFZc', 'youtube', 'mwEube0bFZc', 'mwEube0bFZc.jpg', '<iframe src=\'http://www.youtube.com/embed/mwEube0bFZc?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-12-09 18:26:09', '2015-12-09 18:26:09'),
(360, 132, 'Sérgio Menezes - Politicamente Incorreto', 'https://youtu.be/dchvTXv67ck', 'youtube', 'dchvTXv67ck', 'dchvTXv67ck.jpg', '<iframe src=\'http://www.youtube.com/embed/dchvTXv67ck?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2015-12-09 18:46:39', '2015-12-10 15:40:11'),
(361, 51, 'Pedro Andrade - UnGlamorous the naked truth', 'https://www.youtube.com/watch?v=tpr7gU65pAU&index=4&list=PL93EC673E557AD390', 'youtube', 'tpr7gU65pAU', 'tpr7gU65pAU.jpg', '<iframe src=\'http://www.youtube.com/embed/tpr7gU65pAU?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2015-12-10 13:39:20', '2016-02-04 13:14:26'),
(362, 132, 'Sérgio Menezes - Ou tudo ou nada', 'https://www.youtube.com/watch?v=mB48C6hDMLM&index=4&list=PLcrydkWS_1zG3FVu3-RI6Mn9eT60C95wz', 'youtube', 'mB48C6hDMLM', 'mB48C6hDMLM.jpg', '<iframe src=\'http://www.youtube.com/embed/mB48C6hDMLM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2015-12-10 16:37:36', '2015-12-10 16:37:36'),
(363, 131, 'CAROLINA DIECKMANN / Revista Estilo', 'https://vimeo.com/33178706', 'vimeo', '33178706', '33178706.jpg', '<iframe src="//player.vimeo.com/video/33178706?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 0, '2015-12-17 14:40:05', '2015-12-17 14:40:05'),
(364, 131, 'LUCIANA GIMENEZ / Revista Joyce Pascowitch', 'https://vimeo.com/33176096', 'vimeo', '33176096', '33176096.jpg', '<iframe src="//player.vimeo.com/video/33176096?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 1, '2015-12-17 14:40:08', '2015-12-17 14:40:08'),
(365, 131, 'CLAUDIA LEITTE / Revista Boa Forma', 'https://vimeo.com/33179279', 'vimeo', '33179279', '33179279.jpg', '<iframe src="//player.vimeo.com/video/33179279?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 2, '2015-12-17 14:40:10', '2015-12-17 14:40:10'),
(366, 131, 'HARPERS BAZAAR / Alicia Kuczman', 'https://vimeo.com/39605887', 'vimeo', '39605887', '39605887.jpg', '<iframe src="//player.vimeo.com/video/39605887?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, '2015-12-17 14:40:12', '2015-12-17 14:40:12'),
(368, 131, 'CAROL FRANCISCHINI / Lifestyle One', 'https://vimeo.com/45256893', 'vimeo', '45256893', '45256893.jpg', '<iframe src="//player.vimeo.com/video/45256893?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 4, '2015-12-17 14:40:16', '2016-11-13 23:42:09'),
(369, 33, 'Chiara Gadaleta - ', 'https://vimeo.com/149296992', 'vimeo', '149296992', '149296992.jpg', '<iframe src="//player.vimeo.com/video/149296992?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 5, '2015-12-17 16:42:55', '2017-05-05 15:04:47'),
(370, 77, 'Chiara Gadaleta', 'https://vimeo.com/149296992', 'vimeo', '149296992', '149296992.jpg', '<iframe src="//player.vimeo.com/video/149296992?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 4, '2015-12-17 16:46:01', '2017-05-05 15:07:12'),
(371, 106, 'Chiara Gadaleta', 'https://vimeo.com/149296992', 'vimeo', '149296992', '149296992.jpg', '<iframe src="//player.vimeo.com/video/149296992?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 2, '2015-12-17 16:47:01', '2017-04-24 20:43:37'),
(372, 33, 'Chiara Gadalera - O Lek e a arte do Crochê', 'https://vimeo.com/149301712', 'vimeo', '149301712', '149301712.jpg', '<iframe src="//player.vimeo.com/video/149301712?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 6, '2015-12-17 18:35:06', '2017-05-05 15:04:47'),
(373, 77, 'Chiara Gadaleta - O Lek e a arte do crochê', 'https://vimeo.com/149301712', 'vimeo', '149301712', '149301712.jpg', '<iframe src="//player.vimeo.com/video/149301712?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 5, '2015-12-17 18:36:38', '2017-05-05 15:07:12'),
(374, 106, 'Chiara Gadaleta - O Lek e a arte do chochê', 'https://vimeo.com/149301712', 'vimeo', '149301712', '149301712.jpg', '<iframe src="//player.vimeo.com/video/149301712?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, '2015-12-17 18:37:20', '2017-04-24 20:43:37'),
(375, 33, 'Chiara Gadaleta - Teaser', 'https://vimeo.com/149299578', 'vimeo', '149299578', '149299578.jpg', '<iframe src="//player.vimeo.com/video/149299578?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 7, '2015-12-17 18:38:32', '2017-05-05 15:04:47'),
(376, 77, 'Chiara Gadaleta - Teaser', 'https://vimeo.com/149299578', 'vimeo', '149299578', '149299578.jpg', '<iframe src="//player.vimeo.com/video/149299578?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 6, '2015-12-17 18:39:04', '2017-05-05 15:07:12'),
(377, 106, 'Chiara Gadaletaa - Teaser', 'https://vimeo.com/149299578', 'vimeo', '149299578', '149299578.jpg', '<iframe src="//player.vimeo.com/video/149299578?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 1, '2015-12-17 18:39:47', '2017-04-24 20:43:37'),
(378, 135, 'Costanza e Marilu - A de Agora', 'https://youtu.be/ntUrBUQLiRQ?list=PLcrydkWS_1zF2vn70kosV1rJSPBcwOij4', 'youtube', 'ntUrBUQLiRQ', 'ntUrBUQLiRQ.jpg', '<iframe src=\'http://www.youtube.com/embed/ntUrBUQLiRQ?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2016-01-19 16:35:11', '2016-08-18 13:33:40'),
(379, 135, 'Costanza Pascolato - Morena rosa Verão 2016', 'https://youtu.be/M4MdR54OOKQ', 'youtube', 'M4MdR54OOKQ', 'M4MdR54OOKQ.jpg', '<iframe src=\'http://www.youtube.com/embed/M4MdR54OOKQ?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2016-01-19 16:37:12', '2016-08-18 13:33:40'),
(380, 135, 'Costanza Pascolato - Campanha verão 2016', 'https://youtu.be/YTpf7fRgxqY', 'youtube', 'YTpf7fRgxqY', 'YTpf7fRgxqY.jpg', '<iframe src=\'http://www.youtube.com/embed/YTpf7fRgxqY?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 5, '2016-01-19 16:37:13', '2016-08-18 13:33:40'),
(381, 135, 'Costanza Pascolato - Chilli Beans', 'https://youtu.be/HivXPNcoRaE?list=PLcrydkWS_1zF2vn70kosV1rJSPBcwOij4', 'youtube', 'HivXPNcoRaE', 'HivXPNcoRaE.jpg', '<iframe src=\'http://www.youtube.com/embed/HivXPNcoRaE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 6, '2016-01-19 16:37:13', '2016-08-18 13:33:40'),
(382, 136, 'A de Agora', 'https://www.youtube.com/watch?v=ntUrBUQLiRQ&list=PLcrydkWS_1zF2vn70kosV1rJSPBcwOij4&index=2', 'youtube', 'ntUrBUQLiRQ', 'ntUrBUQLiRQ.jpg', '<iframe src=\'http://www.youtube.com/embed/ntUrBUQLiRQ?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-01-28 19:10:20', '2016-01-28 19:10:20'),
(383, 136, 'Sofá Chat Show', 'https://www.youtube.com/watch?v=Rnl4pBa3bKo&index=5&list=PLcrydkWS_1zF2vn70kosV1rJSPBcwOij4', 'youtube', 'Rnl4pBa3bKo', 'Rnl4pBa3bKo.jpg', '<iframe src=\'http://www.youtube.com/embed/Rnl4pBa3bKo?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-01-28 19:10:20', '2016-01-28 19:10:20'),
(384, 136, 'Sofá dos Fãs', 'https://www.youtube.com/watch?v=PjC2Xqhc0rA&index=8&list=PLcrydkWS_1zF2vn70kosV1rJSPBcwOij4', 'youtube', 'PjC2Xqhc0rA', 'PjC2Xqhc0rA.jpg', '<iframe src=\'http://www.youtube.com/embed/PjC2Xqhc0rA?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-01-28 19:10:20', '2016-01-28 19:10:20'),
(385, 137, 'A de Agora', 'https://www.youtube.com/watch?v=ntUrBUQLiRQ&list=PLcrydkWS_1zF2vn70kosV1rJSPBcwOij4&index=2', 'youtube', 'ntUrBUQLiRQ', 'ntUrBUQLiRQ.jpg', '<iframe src=\'http://www.youtube.com/embed/ntUrBUQLiRQ?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-01-28 19:16:42', '2016-01-28 19:16:42'),
(386, 137, 'Sofá Chat Show', 'https://www.youtube.com/watch?v=Rnl4pBa3bKo&index=5&list=PLcrydkWS_1zF2vn70kosV1rJSPBcwOij4', 'youtube', 'Rnl4pBa3bKo', 'Rnl4pBa3bKo.jpg', '<iframe src=\'http://www.youtube.com/embed/Rnl4pBa3bKo?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-01-28 19:16:42', '2016-01-28 19:16:42'),
(387, 137, 'Sofá dos Fãs', 'https://www.youtube.com/watch?v=PjC2Xqhc0rA&index=8&list=PLcrydkWS_1zF2vn70kosV1rJSPBcwOij4', 'youtube', 'PjC2Xqhc0rA', 'PjC2Xqhc0rA.jpg', '<iframe src=\'http://www.youtube.com/embed/PjC2Xqhc0rA?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-01-28 19:16:42', '2016-01-28 19:16:42'),
(388, 51, 'Pedro Andrade - Chamada "Pedro Pelo Mundo"', 'https://youtu.be/OcZz-kBZm8o?list=PL93EC673E557AD390', 'youtube', 'OcZz-kBZm8o', 'OcZz-kBZm8o.jpg', '<iframe src=\'http://www.youtube.com/embed/OcZz-kBZm8o?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-02-04 13:14:26', '2016-02-04 13:14:26'),
(389, 51, 'Pedro Andrade Globonews', 'https://youtu.be/rWMavjZqaiQ?list=PL93EC673E557AD390', 'youtube', 'rWMavjZqaiQ', 'rWMavjZqaiQ.jpg', '<iframe src=\'http://www.youtube.com/embed/rWMavjZqaiQ?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-02-04 13:14:26', '2016-02-04 13:14:26'),
(392, 138, '"Melhor Pra Você" - Faça seu Vinho', 'https://youtu.be/IKv3EAg2Rlg?list=PLcrydkWS_1zHebdpQwR-2hopTxcAqPt1D', 'youtube', 'IKv3EAg2Rlg', 'IKv3EAg2Rlg.jpg', '<iframe src=\'http://www.youtube.com/embed/IKv3EAg2Rlg?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-02-17 18:21:48', '2016-02-24 17:27:31'),
(393, 138, 'Paçoca é contratada pela Rede TV!', 'https://youtu.be/bSB6hJ-Ar2U?list=PLcrydkWS_1zHebdpQwR-2hopTxcAqPt1D', 'youtube', 'bSB6hJ-Ar2U', 'bSB6hJ-Ar2U.jpg', '<iframe src=\'http://www.youtube.com/embed/bSB6hJ-Ar2U?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-02-17 18:21:49', '2016-02-17 18:21:49'),
(394, 138, 'Vinheta ', 'https://youtu.be/2ltzVgU77lE?list=PLcrydkWS_1zHebdpQwR-2hopTxcAqPt1D', 'youtube', '2ltzVgU77lE', '2ltzVgU77lE.jpg', '<iframe src=\'http://www.youtube.com/embed/2ltzVgU77lE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-02-17 18:21:49', '2016-02-24 17:27:31'),
(395, 138, 'Plantão Domingo Espetacular', 'https://youtu.be/OH1g8NnkF0E?list=PLcrydkWS_1zHebdpQwR-2hopTxcAqPt1D', 'youtube', 'OH1g8NnkF0E', 'OH1g8NnkF0E.jpg', '<iframe src=\'http://www.youtube.com/embed/OH1g8NnkF0E?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2016-02-17 18:21:50', '2016-02-17 18:21:50'),
(396, 100, 'Luitha entrevista Afrojack - Tommorrowland', 'https://youtu.be/QshKlQZal68?list=PLcrydkWS_1zHz-m5hLPCn9HvZHDzTHhFv', 'youtube', 'QshKlQZal68', 'QshKlQZal68.jpg', '<iframe src=\'http://www.youtube.com/embed/QshKlQZal68?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2016-03-28 20:01:38', '2016-08-22 15:34:00'),
(397, 100, 'Luitha entrevista Dimitri - Tomorrowland', 'https://youtu.be/E4znCHgL67M?list=PLcrydkWS_1zHz-m5hLPCn9HvZHDzTHhFv', 'youtube', 'E4znCHgL67M', 'E4znCHgL67M.jpg', '<iframe src=\'http://www.youtube.com/embed/E4znCHgL67M?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 5, '2016-03-28 20:01:39', '2016-08-22 15:34:00'),
(398, 129, 'Lúcia Bronstein - Cena De Histórias de Amor Duram Apenas 90 Minutos ', 'https://www.youtube.com/watch?v=ArqXlGuYt_Q&list=PLcrydkWS_1zEGbPEPhYpCbk-RVJ13JmN9&index=2', 'youtube', 'ArqXlGuYt_Q', 'ArqXlGuYt_Q.jpg', '<iframe src=\'http://www.youtube.com/embed/ArqXlGuYt_Q?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2016-03-29 21:00:25', '2016-03-29 21:00:25'),
(400, 142, 'Desafio do Azeite Gallo', 'https://www.youtube.com/watch?v=HIR3vpiaGAc&list=PLcrydkWS_1zGcqWYk4P1mLLhcnJfun88I&index=1', 'youtube', 'HIR3vpiaGAc', 'HIR3vpiaGAc.jpg', '<iframe src=\'http://www.youtube.com/embed/HIR3vpiaGAc?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-04-26 17:32:47', '2016-04-26 17:32:47'),
(401, 142, 'Picolés de Banana com Chocolate', 'https://www.youtube.com/watch?v=9W7vQe7qIps&list=PLcrydkWS_1zGcqWYk4P1mLLhcnJfun88I&index=2', 'youtube', '9W7vQe7qIps', '9W7vQe7qIps.jpg', '<iframe src=\'http://www.youtube.com/embed/9W7vQe7qIps?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-04-26 17:32:48', '2016-04-26 17:32:48'),
(402, 34, 'Dia das mães - Tiffany & Co', 'https://youtu.be/sjf7mGrYoMM?list=PLAA48A66A50E24A7A', 'youtube', 'sjf7mGrYoMM', 'sjf7mGrYoMM.jpg', '<iframe src=\'http://www.youtube.com/embed/sjf7mGrYoMM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-05-09 14:20:30', '2016-08-18 13:32:28'),
(403, 135, 'Dia das Mães - Tiffany & Co', 'https://youtu.be/sjf7mGrYoMM?list=PLAA48A66A50E24A7A', 'youtube', 'sjf7mGrYoMM', 'sjf7mGrYoMM.jpg', '<iframe src=\'http://www.youtube.com/embed/sjf7mGrYoMM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-05-09 14:21:36', '2016-08-18 13:33:40'),
(404, 146, 'Celso Zucatelli - Vinheta Melhor Pra Você RedeTV!', 'https://www.youtube.com/watch?v=2ltzVgU77lE', 'youtube', '2ltzVgU77lE', '2ltzVgU77lE.jpg', '<iframe src=\'http://www.youtube.com/embed/2ltzVgU77lE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-05-12 18:06:19', '2016-05-12 18:06:19'),
(405, 146, 'Celso Zucatelli - Paçoca é contatado pela RedeTV!', 'https://www.youtube.com/watch?v=bSB6hJ-Ar2U', 'youtube', 'bSB6hJ-Ar2U', 'bSB6hJ-Ar2U.jpg', '<iframe src=\'http://www.youtube.com/embed/bSB6hJ-Ar2U?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-05-12 18:06:19', '2016-05-12 18:06:19'),
(406, 146, 'Programa Melhor pra Você - Celso Zucatelli - Faça seu vinho', 'https://www.youtube.com/watch?v=IKv3EAg2Rlg', 'youtube', 'IKv3EAg2Rlg', 'IKv3EAg2Rlg.jpg', '<iframe src=\'http://www.youtube.com/embed/IKv3EAg2Rlg?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-05-12 18:06:19', '2016-05-12 18:06:19'),
(407, 146, 'Celso Zucatelli - PLANTÃO DOMINGO ESPETACULAR', 'https://www.youtube.com/watch?v=OH1g8NnkF0E', 'youtube', 'OH1g8NnkF0E', 'OH1g8NnkF0E.jpg', '<iframe src=\'http://www.youtube.com/embed/OH1g8NnkF0E?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2016-05-12 18:06:22', '2016-05-12 18:06:22'),
(408, 143, 'Entrevista Programa Roda Viva ', 'https://youtu.be/xbenL7V60Ac?list=PLcrydkWS_1zGdNGD6GfP6C6kZgWdwyfNx', 'youtube', 'xbenL7V60Ac', 'xbenL7V60Ac.jpg', '<iframe src=\'http://www.youtube.com/embed/xbenL7V60Ac?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-05-18 17:52:01', '2016-05-18 17:52:01'),
(409, 143, 'Entrevista Programa Imprensa na TV', 'https://youtu.be/c3yMCgbgUjU?list=PLcrydkWS_1zGdNGD6GfP6C6kZgWdwyfNx', 'youtube', 'c3yMCgbgUjU', 'c3yMCgbgUjU.jpg', '<iframe src=\'http://www.youtube.com/embed/c3yMCgbgUjU?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-05-18 17:52:01', '2016-05-18 17:52:01'),
(410, 143, 'Matéria - Estado Islâmico', 'https://youtu.be/AhC40l7TRlk?list=PLcrydkWS_1zGdNGD6GfP6C6kZgWdwyfNx', 'youtube', 'AhC40l7TRlk', 'AhC40l7TRlk.jpg', '<iframe src=\'http://www.youtube.com/embed/AhC40l7TRlk?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-05-18 17:52:02', '2016-05-18 17:52:02'),
(411, 143, 'Matéria - Ebola - Serra Leoa', 'https://youtu.be/yCNVvuDNMNU?list=PLcrydkWS_1zGdNGD6GfP6C6kZgWdwyfNx', 'youtube', 'yCNVvuDNMNU', 'yCNVvuDNMNU.jpg', '<iframe src=\'http://www.youtube.com/embed/yCNVvuDNMNU?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2016-05-18 17:52:03', '2016-05-18 17:52:03'),
(412, 140, 'Entrevista - TV Veja', 'https://youtu.be/vzldcHJ10Cs?list=PLcrydkWS_1zGytEll75BBaMTt0bNI9Zff', 'youtube', 'vzldcHJ10Cs', 'vzldcHJ10Cs.jpg', '<iframe src=\'http://www.youtube.com/embed/vzldcHJ10Cs?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-05-18 17:56:39', '2016-05-18 17:56:39'),
(413, 34, 'Campanha 50 anos Shopping Iguatemi SP', 'https://youtu.be/RqHlNyamegM', 'youtube', 'RqHlNyamegM', 'RqHlNyamegM.jpg', '<iframe src=\'http://www.youtube.com/embed/RqHlNyamegM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-05-23 14:57:15', '2016-08-18 13:32:28'),
(414, 90, 'Campanha 50 anos Shopping Iguatemi SP', 'https://youtu.be/RqHlNyamegM', 'youtube', 'RqHlNyamegM', 'RqHlNyamegM.jpg', '<iframe src=\'http://www.youtube.com/embed/RqHlNyamegM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-05-23 14:58:19', '2016-08-18 13:29:28'),
(415, 135, 'Campanha 50 anos Shopping Iguatemi SP', 'https://youtu.be/RqHlNyamegM', 'youtube', 'RqHlNyamegM', 'RqHlNyamegM.jpg', '<iframe src=\'http://www.youtube.com/embed/RqHlNyamegM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-05-23 14:59:32', '2016-08-18 13:33:40'),
(416, 147, 'Manual Do Jeans', 'https://youtu.be/ACjy4f0bclc?list=PLcrydkWS_1zFS0q1P7cZF9WgTxW67TS3O', 'youtube', 'ACjy4f0bclc', 'ACjy4f0bclc.jpg', '<iframe src=\'http://www.youtube.com/embed/ACjy4f0bclc?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-05-23 19:54:56', '2016-05-23 19:54:56'),
(417, 148, 'Consuelo Responde: Moda e Desejos', 'https://www.youtube.com/watch?v=YgxXfT8sbR8', 'youtube', 'YgxXfT8sbR8', 'YgxXfT8sbR8.jpg', '<iframe src=\'http://www.youtube.com/embed/YgxXfT8sbR8?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-05-30 19:19:06', '2016-05-30 19:19:06'),
(418, 148, 'Consuelo Responde: Pulseirismo, Minha vida na Itália e a Minha História', 'https://www.youtube.com/watch?v=TmuYhzBWQIU', 'youtube', 'TmuYhzBWQIU', 'TmuYhzBWQIU.jpg', '<iframe src=\'http://www.youtube.com/embed/TmuYhzBWQIU?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-05-30 19:19:07', '2016-05-30 19:19:07'),
(419, 148, 'TIFFANY: GAVEA ', 'https://www.youtube.com/watch?v=sjf7mGrYoMM', 'youtube', 'sjf7mGrYoMM', 'sjf7mGrYoMM.jpg', '<iframe src=\'http://www.youtube.com/embed/sjf7mGrYoMM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-05-30 19:19:07', '2016-05-30 19:19:07'),
(420, 35, 'Nextel - Daniella Cicarelli', 'https://www.youtube.com/watch?v=5lbNlZ7-8YU', 'youtube', '5lbNlZ7-8YU', '5lbNlZ7-8YU.jpg', '<iframe src=\'http://www.youtube.com/embed/5lbNlZ7-8YU?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-06-15 21:11:08', '2016-06-15 21:11:08'),
(421, 152, 'SEX AROUND THE WORLD com SETH KUGEL PPONTO1 ', 'https://www.youtube.com/watch?v=2my_MOn5uYI&list=PLcrydkWS_1zFC1OkLs56NTJBLsL4zFNWy&index=5', 'youtube', '2my_MOn5uYI', '2my_MOn5uYI.jpg', '<iframe src=\'http://www.youtube.com/embed/2my_MOn5uYI?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-06-27 11:10:36', '2016-06-27 11:10:36'),
(422, 152, 'UM VIVA PRA KELLY KEY! PARTE 11 ', 'https://www.youtube.com/watch?v=0M2n1hcvbFg&list=PLcrydkWS_1zFC1OkLs56NTJBLsL4zFNWy&index=4', 'youtube', '0M2n1hcvbFg', '0M2n1hcvbFg.jpg', '<iframe src=\'http://www.youtube.com/embed/0M2n1hcvbFg?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-06-27 11:10:37', '2016-06-27 11:10:37'),
(423, 152, 'ACEITA QUE DÓI MENOS PPONTO1 ', 'https://www.youtube.com/watch?v=XUwAe_gaPsM&list=PLcrydkWS_1zFC1OkLs56NTJBLsL4zFNWy&index=1', 'youtube', 'XUwAe_gaPsM', 'XUwAe_gaPsM.jpg', '<iframe src=\'http://www.youtube.com/embed/XUwAe_gaPsM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-06-27 11:10:37', '2016-06-27 11:10:37'),
(424, 152, 'CANSADO AOS 40 com RAFA CORTEZ PPONTO1 ', 'https://www.youtube.com/watch?v=M1jCybw-n9Q&list=PLcrydkWS_1zFC1OkLs56NTJBLsL4zFNWy&index=3', 'youtube', 'M1jCybw-n9Q', 'M1jCybw-n9Q.jpg', '<iframe src=\'http://www.youtube.com/embed/M1jCybw-n9Q?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2016-06-27 11:10:37', '2016-06-27 11:10:37'),
(425, 152, 'TESÃO X CORAÇÃO SOTAQUE X PRECONCEITO PPONTO1 ', 'https://www.youtube.com/watch?v=VYMVtcr4c48&index=2&list=PLcrydkWS_1zFC1OkLs56NTJBLsL4zFNWy', 'youtube', 'VYMVtcr4c48', 'VYMVtcr4c48.jpg', '<iframe src=\'http://www.youtube.com/embed/VYMVtcr4c48?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2016-06-27 11:10:38', '2016-06-27 11:10:38'),
(426, 49, 'André Vasco - Reel 2016 ', 'https://youtu.be/_e_v_ICO4AA?list=PLcrydkWS_1zEtKrpqmEIhqra-ILfJLLnO', 'youtube', '_e_v_ICO4AA', '_e_v_ICO4AA.jpg', '<iframe src=\'http://www.youtube.com/embed/_e_v_ICO4AA?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-06-27 19:41:11', '2016-12-08 15:42:58'),
(427, 49, 'Reel 2016', 'https://youtu.be/_e_v_ICO4AA?list=PLcrydkWS_1zEtKrpqmEIhqra-ILfJLLnO', 'youtube', '_e_v_ICO4AA', '_e_v_ICO4AA.jpg', '<iframe src=\'http://www.youtube.com/embed/_e_v_ICO4AA?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 12, '2016-06-28 17:54:24', '2016-12-08 15:03:29'),
(428, 154, 'A Semana -  TV Gazeta ', 'https://www.youtube.com/watch?v=H8rPWcve6c0', 'youtube', 'H8rPWcve6c0', 'H8rPWcve6c0.jpg', '<iframe src=\'http://www.youtube.com/embed/H8rPWcve6c0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-07-01 19:35:31', '2016-08-16 13:14:39'),
(429, 154, 'Ronald Rios entrevista Barbara Gancia ', 'https://www.youtube.com/watch?v=6m8TZi5n8gM', 'youtube', '6m8TZi5n8gM', '6m8TZi5n8gM.jpg', '<iframe src=\'http://www.youtube.com/embed/6m8TZi5n8gM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-07-01 19:38:51', '2016-08-16 13:14:39'),
(430, 154, 'Ronald Rios entrevistado pelo Ronnie Von ', 'https://www.youtube.com/watch?v=75LX71USFks', 'youtube', '75LX71USFks', '75LX71USFks.jpg', '<iframe src=\'http://www.youtube.com/embed/75LX71USFks?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2016-07-01 19:42:27', '2016-08-16 13:14:39'),
(431, 45, 'Revista TPM', 'https://youtu.be/qHzkRlmbxiY?list=PLcrydkWS_1zGuqJzXTEVXFRnhDtQo0406', 'youtube', 'qHzkRlmbxiY', 'qHzkRlmbxiY.jpg', '<iframe src=\'http://www.youtube.com/embed/qHzkRlmbxiY?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-07-04 15:15:06', '2017-06-26 17:06:04'),
(432, 35, 'Kaiser - Torneirinha', 'https://youtu.be/8KwQCImJ9ck?list=PLcrydkWS_1zFCFCH3X39sDuN2pWMaySXm', 'youtube', '8KwQCImJ9ck', '8KwQCImJ9ck.jpg', '<iframe src=\'http://www.youtube.com/embed/8KwQCImJ9ck?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 8, '2016-07-04 16:31:33', '2016-07-04 16:31:33'),
(433, 154, 'A Semana com Ronald Rios', 'https://youtu.be/EpeidEEHaKA', 'youtube', 'EpeidEEHaKA', 'EpeidEEHaKA.jpg', '<iframe src=\'http://www.youtube.com/embed/EpeidEEHaKA?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2016-07-05 17:23:26', '2016-08-16 13:14:39'),
(434, 31, 'entrevista com Ronald Rios', 'https://youtu.be/5dzzCZty8E4', 'youtube', '5dzzCZty8E4', '5dzzCZty8E4.jpg', '<iframe src=\'http://www.youtube.com/embed/5dzzCZty8E4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-07-05 17:24:14', '2016-07-05 17:24:14'),
(435, 75, 'Entrevista com Ronald Rios', 'https://youtu.be/5dzzCZty8E4', 'youtube', '5dzzCZty8E4', '5dzzCZty8E4.jpg', '<iframe src=\'http://www.youtube.com/embed/5dzzCZty8E4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-07-05 17:25:00', '2016-07-05 17:25:00'),
(436, 82, 'Entrevista  com Ronald Rios', 'https://youtu.be/5dzzCZty8E4', 'youtube', '5dzzCZty8E4', '5dzzCZty8E4.jpg', '<iframe src=\'http://www.youtube.com/embed/5dzzCZty8E4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2016-07-05 17:25:34', '2016-07-05 17:25:34'),
(437, 104, 'entrevista com Ronald Rios', 'https://youtu.be/5dzzCZty8E4', 'youtube', '5dzzCZty8E4', '5dzzCZty8E4.jpg', '<iframe src=\'http://www.youtube.com/embed/5dzzCZty8E4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-07-05 17:26:20', '2016-07-05 17:26:20'),
(438, 154, 'Edi Rock Anuncia filme dos Racionais MC', 'https://www.youtube.com/watch?v=BLroJnO3dVI&index=3&list=PLcrydkWS_1zHeaxs8ypBgCAkWhZD7BG20', 'youtube', 'BLroJnO3dVI', 'BLroJnO3dVI.jpg', '<iframe src=\'http://www.youtube.com/embed/BLroJnO3dVI?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 5, '2016-07-21 17:34:58', '2016-08-16 13:14:39'),
(439, 154, 'Skate para Iniciantes 1', 'https://www.youtube.com/watch?v=-tN5GLwCXMI&index=4&list=PLcrydkWS_1zHeaxs8ypBgCAkWhZD7BG20', 'youtube', '-tN5GLwCXMI', '-tN5GLwCXMI.jpg', '<iframe src=\'http://www.youtube.com/embed/-tN5GLwCXMI?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 6, '2016-07-21 17:34:58', '2016-08-16 13:14:39'),
(440, 154, '259 CQC Ronald Rios embarca no trem da morte do México 14 04 2014 Mirc', 'https://www.youtube.com/watch?v=GFSWOAxFCg0&index=5&list=PLcrydkWS_1zHeaxs8ypBgCAkWhZD7BG20', 'youtube', 'GFSWOAxFCg0', 'GFSWOAxFCg0.jpg', '<iframe src=\'http://www.youtube.com/embed/GFSWOAxFCg0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 7, '2016-07-21 17:34:58', '2016-08-16 13:14:39'),
(441, 154, 'CQC Documento da Semana Pedofilia 20 08 2012 ', 'https://www.youtube.com/watch?v=9HCAedAKdOo&index=6&list=PLcrydkWS_1zHeaxs8ypBgCAkWhZD7BG20', 'youtube', '9HCAedAKdOo', '9HCAedAKdOo.jpg', '<iframe src=\'http://www.youtube.com/embed/9HCAedAKdOo?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 8, '2016-07-21 17:34:58', '2016-08-16 13:14:39'),
(442, 154, 'Faixa de Gaza com Ronald Rios CQC 04 08 2014 ', 'https://www.youtube.com/watch?v=mBU_O83Yu5Q&index=7&list=PLcrydkWS_1zHeaxs8ypBgCAkWhZD7BG20', 'youtube', 'mBU_O83Yu5Q', 'mBU_O83Yu5Q.jpg', '<iframe src=\'http://www.youtube.com/embed/mBU_O83Yu5Q?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 9, '2016-07-21 17:34:59', '2016-08-16 13:14:39'),
(443, 154, 'Cotas Raciais 1 ', 'https://www.youtube.com/watch?v=_AYgRDWxr1g&index=8&list=PLcrydkWS_1zHeaxs8ypBgCAkWhZD7BG20', 'youtube', '_AYgRDWxr1g', '_AYgRDWxr1g.jpg', '<iframe src=\'http://www.youtube.com/embed/_AYgRDWxr1g?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 10, '2016-07-21 17:34:59', '2016-08-16 13:14:39'),
(444, 45, 'Terra - Vivo - vídeo semanal', 'https://youtu.be/0vvPWU_pf20?list=PLcrydkWS_1zGuqJzXTEVXFRnhDtQo0406', 'youtube', '0vvPWU_pf20', '0vvPWU_pf20.jpg', '<iframe src=\'http://www.youtube.com/embed/0vvPWU_pf20?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-07-22 16:33:17', '2017-06-26 17:06:04'),
(445, 155, 'SPTV', 'https://youtu.be/Z8D8G6QyXBY?list=PLcrydkWS_1zFgwEwgodFm8iCfmH4z5aFc', 'youtube', 'Z8D8G6QyXBY', 'Z8D8G6QyXBY.jpg', '<iframe src=\'http://www.youtube.com/embed/Z8D8G6QyXBY?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-07-27 19:25:15', '2016-07-27 19:25:15'),
(446, 144, 'Danilo Vieira - matéria', 'https://youtu.be/Jq3q17kJ0xo?list=PLcrydkWS_1zFebf4BfT7kwGk8ytbRupS2', 'youtube', 'Jq3q17kJ0xo', 'Jq3q17kJ0xo.jpg', '<iframe src=\'http://www.youtube.com/embed/Jq3q17kJ0xo?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-07-29 16:37:10', '2016-07-29 16:37:10'),
(447, 73, 'TokStok Collection Marcelo Sommer1 ', 'https://www.youtube.com/watch?v=OcG-jIP8hTQ', 'youtube', 'OcG-jIP8hTQ', 'OcG-jIP8hTQ.jpg', '<iframe src=\'http://www.youtube.com/embed/OcG-jIP8hTQ?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-07-29 22:15:12', '2016-07-29 22:15:12'),
(448, 73, 'Entrevista com o estilista Marcelo Sommer Designbook TokStok1 ', 'https://www.youtube.com/watch?v=bJFnQPlIGUA', 'youtube', 'bJFnQPlIGUA', 'bJFnQPlIGUA.jpg', '<iframe src=\'http://www.youtube.com/embed/bJFnQPlIGUA?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-07-29 22:15:12', '2016-07-29 22:15:12'),
(449, 73, 'DESFILE CAVALERA SPFW VERÃO 2013 2014 1 ', 'https://www.youtube.com/watch?v=9PlqQ5E6xbI', 'youtube', '9PlqQ5E6xbI', '9PlqQ5E6xbI.jpg', '<iframe src=\'http://www.youtube.com/embed/9PlqQ5E6xbI?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-07-29 22:15:13', '2016-07-29 22:15:13'),
(450, 73, 'Entrevista Marcelo Sommer Coleção Outono Inverno Extra 2012 ', 'https://www.youtube.com/watch?v=7Ga5eOqaw9w', 'youtube', '7Ga5eOqaw9w', '7Ga5eOqaw9w.jpg', '<iframe src=\'http://www.youtube.com/embed/7Ga5eOqaw9w?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2016-07-29 22:15:13', '2016-07-29 22:15:13'),
(451, 73, 'Marcelo Sommer Dicas Moda GNT ', 'https://www.youtube.com/watch?v=Zydi0T_VEbw', 'youtube', 'Zydi0T_VEbw', 'Zydi0T_VEbw.jpg', '<iframe src=\'http://www.youtube.com/embed/Zydi0T_VEbw?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2016-07-29 22:15:13', '2016-07-29 22:15:13'),
(452, 158, 'Erick Krominski _CQC _  Propõe aos políticos formas de Economizar 14 09 2015 ', 'https://www.youtube.com/watch?v=CylM7323Aj4&list=PLcrydkWS_1zHFYjQD2-20DVKK5u4RjCpU&index=1', 'youtube', 'CylM7323Aj4', 'CylM7323Aj4.jpg', '<iframe src=\'http://www.youtube.com/embed/CylM7323Aj4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2016-08-10 18:16:09', '2016-12-09 17:24:47'),
(453, 158, 'Erick Krominski _ CQC _ Bumbum de Justin Bieber é destaque do Semana em Fatos 13 07 2015 ', 'https://www.youtube.com/watch?v=Zum6P0FT6Eo&list=PLcrydkWS_1zHFYjQD2-20DVKK5u4RjCpU&index=2', 'youtube', 'Zum6P0FT6Eo', 'Zum6P0FT6Eo.jpg', '<iframe src=\'http://www.youtube.com/embed/Zum6P0FT6Eo?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 5, '2016-08-10 18:16:09', '2016-12-09 17:24:47'),
(454, 158, 'Erick Krominsk _ CQC_  Simuladores Ensina Truque da Nota Rasgada 18 05 2015 ', 'https://www.youtube.com/watch?v=10ehV16ierI&index=3&list=PLcrydkWS_1zHFYjQD2-20DVKK5u4RjCpU', 'youtube', '10ehV16ierI', '10ehV16ierI.jpg', '<iframe src=\'http://www.youtube.com/embed/10ehV16ierI?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 6, '2016-08-10 18:16:09', '2016-12-09 17:24:47'),
(455, 157, 'Erick Krominski _CQC _  Propõe aos políticos formas de Economizar 14 09 2015 ', 'https://www.youtube.com/watch?v=CylM7323Aj4&list=PLcrydkWS_1zHFYjQD2-20DVKK5u4RjCpU&index=1', 'youtube', 'CylM7323Aj4', 'CylM7323Aj4.jpg', '<iframe src=\'http://www.youtube.com/embed/CylM7323Aj4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2016-08-10 18:16:16', '2016-12-09 17:26:00'),
(456, 157, 'Erick Krominski _ CQC _ Bumbum de Justin Bieber é destaque do Semana em Fatos 13 07 2015 ', 'https://www.youtube.com/watch?v=Zum6P0FT6Eo&list=PLcrydkWS_1zHFYjQD2-20DVKK5u4RjCpU&index=2', 'youtube', 'Zum6P0FT6Eo', 'Zum6P0FT6Eo.jpg', '<iframe src=\'http://www.youtube.com/embed/Zum6P0FT6Eo?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 5, '2016-08-10 18:16:16', '2016-12-09 17:26:00'),
(457, 157, 'Erick Krominsk _ Trailer do Filme Antônia ', 'https://www.youtube.com/watch?v=-r6uJiKewyI&list=PLcrydkWS_1zHFYjQD2-20DVKK5u4RjCpU&index=4', 'youtube', '-r6uJiKewyI', '-r6uJiKewyI.jpg', '<iframe src=\'http://www.youtube.com/embed/-r6uJiKewyI?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 6, '2016-08-10 18:16:16', '2016-12-09 17:26:00'),
(458, 158, 'Erick Krominski _ Trailer do Filme Antônia ', 'https://www.youtube.com/watch?v=-r6uJiKewyI&index=4&list=PLcrydkWS_1zHFYjQD2-20DVKK5u4RjCpU', 'youtube', '-r6uJiKewyI', '-r6uJiKewyI.jpg', '<iframe src=\'http://www.youtube.com/embed/-r6uJiKewyI?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 7, '2016-08-10 18:51:56', '2016-12-09 17:24:47'),
(459, 154, 'Ronald Rios _ Hoje é dia de The Get Down _ Ogi e Jamés feat Tássia Reis1 ', 'https://www.youtube.com/watch?v=m0QJhpCQj_c&index=16&list=PLcrydkWS_1zHeaxs8ypBgCAkWhZD7BG20', 'youtube', 'm0QJhpCQj_c', 'm0QJhpCQj_c.jpg', '<iframe src=\'http://www.youtube.com/embed/m0QJhpCQj_c?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-08-16 13:14:39', '2016-08-16 13:14:39'),
(460, 90, 'Costanza Pascolato _ Natura  Chronos', 'https://www.youtube.com/watch?v=5FdAMqDLQTM', 'youtube', '5FdAMqDLQTM', '5FdAMqDLQTM.jpg', '<iframe src=\'http://www.youtube.com/embed/5FdAMqDLQTM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-08-18 13:29:28', '2016-08-18 13:29:28'),
(461, 34, 'Costanza Pascolato _ Natura Chronos ', 'https://www.youtube.com/watch?v=5FdAMqDLQTM&list=PLAA48A66A50E24A7A&index=4', 'youtube', '5FdAMqDLQTM', '5FdAMqDLQTM.jpg', '<iframe src=\'http://www.youtube.com/embed/5FdAMqDLQTM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-08-18 13:32:28', '2016-08-18 13:32:28'),
(462, 135, 'Costanza Pascolato _ Natura Chronos ', 'https://www.youtube.com/watch?v=5FdAMqDLQTM&list=PLAA48A66A50E24A7A&index=4', 'youtube', '5FdAMqDLQTM', '5FdAMqDLQTM.jpg', '<iframe src=\'http://www.youtube.com/embed/5FdAMqDLQTM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-08-18 13:33:40', '2016-08-18 13:33:40'),
(463, 157, 'Erick Krominski _ Bastidores da Coca Cola Brasil nos Jogos Olímpicos ', 'https://www.youtube.com/watch?v=9CC5TwQTVkA&list=PLcrydkWS_1zHFYjQD2-20DVKK5u4RjCpU&index=5', 'youtube', '9CC5TwQTVkA', '9CC5TwQTVkA.jpg', '<iframe src=\'http://www.youtube.com/embed/9CC5TwQTVkA?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2016-08-18 19:08:46', '2016-12-09 17:26:00'),
(464, 158, 'Erick Krominski _ Bastidores da Coca Cola Brasil nos Jogos Olímpicos ', 'https://www.youtube.com/watch?v=9CC5TwQTVkA&index=5&list=PLcrydkWS_1zHFYjQD2-20DVKK5u4RjCpU', 'youtube', '9CC5TwQTVkA', '9CC5TwQTVkA.jpg', '<iframe src=\'http://www.youtube.com/embed/9CC5TwQTVkA?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2016-08-18 19:16:17', '2016-12-09 17:24:47'),
(465, 157, 'Erick Krominski _ Andar dos Jogos Olímpicos na Coca Cola Brasil ', 'https://www.youtube.com/watch?v=FgVGfvXXkmI&list=PLcrydkWS_1zHFYjQD2-20DVKK5u4RjCpU&index=6', 'youtube', 'FgVGfvXXkmI', 'FgVGfvXXkmI.jpg', '<iframe src=\'http://www.youtube.com/embed/FgVGfvXXkmI?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-08-18 19:17:05', '2016-12-09 17:26:00'),
(466, 158, 'Erick Krominski _ Andar dos Jogos Olímpicos na Coca Cola Brasil ', 'https://www.youtube.com/watch?v=FgVGfvXXkmI&list=PLcrydkWS_1zHFYjQD2-20DVKK5u4RjCpU&index=6', 'youtube', 'FgVGfvXXkmI', 'FgVGfvXXkmI.jpg', '<iframe src=\'http://www.youtube.com/embed/FgVGfvXXkmI?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-08-18 19:17:35', '2016-12-09 17:24:47'),
(467, 157, 'Erick Krominski _ Aquecimento para os Jogos Olímpicos na sede da Coca Cola Brasil ', 'https://www.youtube.com/watch?v=VkhELh73AfE&list=PLcrydkWS_1zHFYjQD2-20DVKK5u4RjCpU&index=7', 'youtube', 'VkhELh73AfE', 'VkhELh73AfE.jpg', '<iframe src=\'http://www.youtube.com/embed/VkhELh73AfE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-08-18 19:18:22', '2016-12-09 17:26:00'),
(468, 158, 'Erick Krominski _ Aquecimento para os Jogos Olímpicos na sede da Coca Cola Brasil ', 'https://www.youtube.com/watch?v=VkhELh73AfE&list=PLcrydkWS_1zHFYjQD2-20DVKK5u4RjCpU&index=7', 'youtube', 'VkhELh73AfE', 'VkhELh73AfE.jpg', '<iframe src=\'http://www.youtube.com/embed/VkhELh73AfE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-08-18 19:18:53', '2016-12-09 17:24:47'),
(469, 100, 'Parada Coca Cola _ Cobertura Olimpiadas Rio 2016', 'https://www.youtube.com/watch?v=dkTfmPrYr54&index=9&list=PLcrydkWS_1zHz-m5hLPCn9HvZHDzTHhFv', 'youtube', 'dkTfmPrYr54', 'dkTfmPrYr54.jpg', '<iframe src=\'http://www.youtube.com/embed/dkTfmPrYr54?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-08-22 15:34:00', '2016-08-22 15:34:00'),
(470, 101, 'Tutti Muller _ Campanha Renner Primavera 2016', 'https://www.youtube.com/watch?v=l10FxeQ4YJc&index=4&list=PLcrydkWS_1zGAY1H06HvsoJKVWlSmQWMy', 'youtube', 'l10FxeQ4YJc', 'l10FxeQ4YJc.jpg', '<iframe src=\'http://www.youtube.com/embed/l10FxeQ4YJc?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-08-23 18:01:11', '2016-08-23 18:01:11');
INSERT INTO `portfolio_videos` (`id`, `portfolio_personalidades_id`, `titulo`, `url`, `servico`, `servico_video_id`, `thumbnail`, `embed`, `ordem`, `created_at`, `updated_at`) VALUES
(471, 165, 'Escalada do Globo News Em Pauta ', 'https://www.youtube.com/watch?v=CnXo8aPKvd8&index=1&list=PLcrydkWS_1zGkUZFjRK0XLpL1XKwJzadY', 'youtube', 'CnXo8aPKvd8', 'CnXo8aPKvd8.jpg', '<iframe src=\'http://www.youtube.com/embed/CnXo8aPKvd8?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-09-22 15:13:39', '2016-09-22 15:13:39'),
(472, 165, 'GloboNews Em Pauta Aplicativo mostra quanto os brasileiros pagam de impostos ', 'https://www.youtube.com/watch?v=7CrRznGzFC0&index=2&list=PLcrydkWS_1zGkUZFjRK0XLpL1XKwJzadY', 'youtube', '7CrRznGzFC0', '7CrRznGzFC0.jpg', '<iframe src=\'http://www.youtube.com/embed/7CrRznGzFC0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-09-22 15:13:41', '2016-09-22 15:13:41'),
(473, 165, 'Povos mais Infelizes do Mundo Vídeo Excluído do GloboNews em Pauta1 ', 'https://www.youtube.com/watch?v=ZDTZynVgFa8&index=3&list=PLcrydkWS_1zGkUZFjRK0XLpL1XKwJzadY', 'youtube', 'ZDTZynVgFa8', 'ZDTZynVgFa8.jpg', '<iframe src=\'http://www.youtube.com/embed/ZDTZynVgFa8?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-09-22 15:13:42', '2016-09-22 15:13:42'),
(474, 160, 'Videobook Bruna Miglioranza', 'https://vimeo.com/147886697', 'vimeo', '147886697', '147886697.jpg', '<iframe src="//player.vimeo.com/video/147886697?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 0, '2016-09-26 15:13:58', '2016-09-26 15:13:58'),
(475, 160, 'Desrotina (depois que você foi embora)', 'https://www.youtube.com/watch?v=Jq89BvLSmM4', 'youtube', 'Jq89BvLSmM4', 'Jq89BvLSmM4.jpg', '<iframe src=\'http://www.youtube.com/embed/Jq89BvLSmM4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-09-26 15:13:58', '2016-09-26 15:13:58'),
(497, 167, 'Musical Rei Leão no Palco do The Noite - César Mello como Mufasa', 'https://youtu.be/0tnTUpNImDw', 'youtube', '0tnTUpNImDw', '0tnTUpNImDw.jpg', '<iframe src=\'http://www.youtube.com/embed/0tnTUpNImDw?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-10-04 20:21:15', '2017-04-18 21:39:14'),
(498, 167, 'Mudança de Hábito - O Musical - Se Eu Achar Você (César Mello) ', 'https://www.youtube.com/watch?v=SOgEKpehgJo', 'youtube', 'SOgEKpehgJo', 'SOgEKpehgJo.jpg', '<iframe src=\'http://www.youtube.com/embed/SOgEKpehgJo?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-10-04 20:21:16', '2017-04-18 21:39:14'),
(499, 167, 'Hair - César Melo', 'https://www.youtube.com/watch?v=9FCSSIZU0NQ', 'youtube', '9FCSSIZU0NQ', '9FCSSIZU0NQ.jpg', '<iframe src=\'http://www.youtube.com/embed/9FCSSIZU0NQ?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2016-10-04 20:21:18', '2017-04-18 21:39:14'),
(500, 167, 'César Mello - Apresentador', 'https://www.youtube.com/watch?v=xHlzDaUGZuk', 'youtube', 'xHlzDaUGZuk', 'xHlzDaUGZuk.jpg', '<iframe src=\'http://www.youtube.com/embed/xHlzDaUGZuk?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2016-10-04 20:21:20', '2017-04-18 21:39:14'),
(515, 170, 'Campanha Natura Chronos', 'https://www.youtube.com/watch?v=veJKxIb17uM', 'youtube', 'veJKxIb17uM', 'veJKxIb17uM.jpg', '<iframe src=\'http://www.youtube.com/embed/veJKxIb17uM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-10-05 21:16:44', '2016-10-05 21:16:44'),
(516, 171, 'Terra Viva Beleza ', 'https://www.youtube.com/watch?v=XokJqSxRQ6k&list=PLE561ED5CD54C27D7&index=1', 'youtube', 'XokJqSxRQ6k', 'XokJqSxRQ6k.jpg', '<iframe src=\'http://www.youtube.com/embed/XokJqSxRQ6k?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-10-06 19:29:44', '2016-10-06 19:29:44'),
(517, 171, 'It MTV ', 'https://www.youtube.com/watch?v=794EUUlPJis&list=PLE561ED5CD54C27D7&index=2', 'youtube', '794EUUlPJis', '794EUUlPJis.jpg', '<iframe src=\'http://www.youtube.com/embed/794EUUlPJis?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 5, '2016-10-06 19:29:46', '2016-10-06 19:31:47'),
(518, 171, 'Perua ', 'https://www.youtube.com/watch?v=O6WRkEBUxkI&list=PLE561ED5CD54C27D7&index=3', 'youtube', 'O6WRkEBUxkI', 'O6WRkEBUxkI.jpg', '<iframe src=\'http://www.youtube.com/embed/O6WRkEBUxkI?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-10-06 19:29:47', '2016-10-06 19:29:47'),
(519, 171, 'Perua ', 'https://www.youtube.com/watch?v=emMY4D2Sa7I&list=PLE561ED5CD54C27D7&index=4', 'youtube', 'emMY4D2Sa7I', 'emMY4D2Sa7I.jpg', '<iframe src=\'http://www.youtube.com/embed/emMY4D2Sa7I?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2016-10-06 19:29:47', '2016-10-06 19:29:47'),
(520, 171, 'Preview Renner ', 'https://www.youtube.com/watch?v=etUhf2RLz_o&list=PLE561ED5CD54C27D7&index=5', 'youtube', 'etUhf2RLz_o', 'etUhf2RLz_o.jpg', '<iframe src=\'http://www.youtube.com/embed/etUhf2RLz_o?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2016-10-06 19:29:48', '2016-10-06 19:29:48'),
(521, 171, 'Enfim , 30', 'https://www.youtube.com/watch?v=WT9sdX9MF3M&list=PLE561ED5CD54C27D7&index=7', 'youtube', 'WT9sdX9MF3M', 'WT9sdX9MF3M.jpg', '<iframe src=\'http://www.youtube.com/embed/WT9sdX9MF3M?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-10-06 19:31:47', '2016-10-06 19:31:47'),
(522, 172, 'Acabou. - Jeronimo Martins (texto de Daniel Bovolento)', 'https://www.youtube.com/watch?v=PKcg1ulCPe0', 'youtube', 'PKcg1ulCPe0', 'PKcg1ulCPe0.jpg', '<iframe src=\'http://www.youtube.com/embed/PKcg1ulCPe0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-10-06 20:09:41', '2017-03-16 21:32:25'),
(525, 173, 'Roda Viva ', 'https://www.youtube.com/watch?v=g5AeguTj2J4', 'youtube', 'g5AeguTj2J4', 'g5AeguTj2J4.jpg', '<iframe src=\'http://www.youtube.com/embed/g5AeguTj2J4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-10-18 23:29:42', '2016-10-18 23:29:42'),
(526, 173, 'Morning Show ', 'https://www.youtube.com/watch?v=kXhkek9n-e0', 'youtube', 'kXhkek9n-e0', 'kXhkek9n-e0.jpg', '<iframe src=\'http://www.youtube.com/embed/kXhkek9n-e0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2016-10-18 23:29:42', '2016-10-19 10:35:58'),
(527, 126, 'Entrevista Lilian Pacce  ', 'https://www.youtube.com/watch?v=JcMoMWuzWnw', 'youtube', 'JcMoMWuzWnw', 'JcMoMWuzWnw.jpg', '<iframe src=\'http://www.youtube.com/embed/JcMoMWuzWnw?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-10-18 23:31:02', '2016-10-18 23:31:02'),
(528, 55, 'Morning Show - O Biquíni Mande In Brasil ', 'https://www.youtube.com/watch?v=JcMoMWuzWnw', 'youtube', 'JcMoMWuzWnw', 'JcMoMWuzWnw.jpg', '<iframe src=\'http://www.youtube.com/embed/JcMoMWuzWnw?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-10-18 23:35:17', '2017-04-11 20:11:11'),
(529, 173, 'Jamil Chade talks about the FIFA congress', 'https://www.youtube.com/watch?v=zBuSjE3cFkU', 'youtube', 'zBuSjE3cFkU', 'zBuSjE3cFkU.jpg', '<iframe src=\'http://www.youtube.com/embed/zBuSjE3cFkU?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 5, '2016-10-18 23:47:10', '2016-10-19 10:35:58'),
(530, 173, 'Salvados - "Un sistema creado dentro de la FIFA de pagos jamás declarados"', 'https://www.youtube.com/watch?v=mbpCVuSwSjE#t=9.385080075', 'youtube', 'mbpCVuSwSjE', 'mbpCVuSwSjE.jpg', '<iframe src=\'http://www.youtube.com/embed/mbpCVuSwSjE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-10-18 23:47:11', '2016-10-19 10:35:58'),
(531, 173, 'Jamil Chade mostra drama dos imigrantes nos Bálcãs', 'http://videos.bol.uol.com.br/video/jamil-chade-mostra-drama-dos-imigrantes-nos-balcas-04020D98316EC0B15326', 'youtube', '0', '0.jpg', '<iframe src=\'http://www.youtube.com/embed/0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-10-19 10:35:58', '2016-10-19 10:35:58'),
(532, 173, 'Jamil Chade conta o que o motivou a escrever o livro ', 'http://espn.uol.com.br/video/560593_jamil-chade-conta-o-que-o-motivou-a-escrever-o-livro-politica-propina-e-futebol', 'youtube', '0', '0.jpg', '<iframe src=\'http://www.youtube.com/embed/0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2016-10-19 10:35:58', '2016-10-19 10:35:58'),
(533, 173, 'Jamil Chade acompanha drama de imigrantes na Europa', 'http://videos.bol.uol.com.br/video/jamil-chade-acompanha-drama-de-imigrantes-na-europa-04024D9C3362C0B15326', 'youtube', '0', '0.jpg', '<iframe src=\'http://www.youtube.com/embed/0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 6, '2016-10-19 10:35:58', '2016-10-19 10:35:58'),
(535, 53, 'Campanha de Motorola', 'https://www.youtube.com/watch?v=AAkroVEjBvM', 'youtube', 'AAkroVEjBvM', 'AAkroVEjBvM.jpg', '<iframe src=\'http://www.youtube.com/embed/AAkroVEjBvM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-10-25 18:52:13', '2017-05-17 14:37:01'),
(536, 161, 'Vandré Silveira ', 'https://www.youtube.com/watch?v=TASAZ1bbNP8', 'youtube', 'TASAZ1bbNP8', 'TASAZ1bbNP8.jpg', '<iframe src=\'http://www.youtube.com/embed/TASAZ1bbNP8?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-10-25 23:03:10', '2016-10-25 23:03:10'),
(537, 161, 'Amor Veríssimo  - GNT ', 'https://vimeo.com/130053630', 'vimeo', '130053630', '130053630.jpg', '<iframe src="//player.vimeo.com/video/130053630?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 1, '2016-10-25 23:03:11', '2016-10-25 23:03:11'),
(538, 159, 'Tropa de Elite', 'https://vimeo.com/123451188', 'vimeo', '123451188', '123451188.jpg', '<iframe src="//player.vimeo.com/video/123451188?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 6, '2016-10-27 17:10:57', '2017-02-19 22:07:55'),
(539, 159, 'A Lei e o Crime - Record', 'https://vimeo.com/123451186', 'vimeo', '123451186', '123451186.jpg', '<iframe src="//player.vimeo.com/video/123451186?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, '2016-10-27 17:10:58', '2017-02-19 22:07:55'),
(544, 159, 'André Ramiro part Black Alien _ Não Foi à Toa ', 'https://www.youtube.com/watch?v=WrtwP93QR_g', 'youtube', 'WrtwP93QR_g', 'WrtwP93QR_g.jpg', '<iframe src=\'http://www.youtube.com/embed/WrtwP93QR_g?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-11-08 15:34:50', '2017-02-19 22:07:55'),
(545, 159, 'Levanta a Cabeça _ 2010', 'https://www.youtube.com/watch?v=EXj5L7QepMg', 'youtube', 'EXj5L7QepMg', 'EXj5L7QepMg.jpg', '<iframe src=\'http://www.youtube.com/embed/EXj5L7QepMg?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2016-11-08 15:34:51', '2017-02-19 22:07:55'),
(546, 159, 'Ilha da Fantasia ', 'https://www.youtube.com/watch?v=myvbCui7WBk', 'youtube', 'myvbCui7WBk', 'myvbCui7WBk.jpg', '<iframe src=\'http://www.youtube.com/embed/myvbCui7WBk?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 5, '2016-11-08 15:34:51', '2017-02-19 22:07:55'),
(548, 156, 'Sabrina Korgut _ Desesperada na balada Paulo Gustavo 220 Volts ', 'https://www.youtube.com/watch?v=Q4nex41SkgU&index=1&list=PLcrydkWS_1zEvL_VjVVmnjwQb11rOUuAJ', 'youtube', 'Q4nex41SkgU', 'Q4nex41SkgU.jpg', '<iframe src=\'http://www.youtube.com/embed/Q4nex41SkgU?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-11-14 20:51:00', '2016-11-14 20:51:00'),
(549, 156, 'Sabrina Korgut _ Envelhecimento Paulo Gustavo 220 Volts ', 'https://www.youtube.com/watch?v=JWPb3YLvBYo&index=2&list=PLcrydkWS_1zEvL_VjVVmnjwQb11rOUuAJ', 'youtube', 'JWPb3YLvBYo', 'JWPb3YLvBYo.jpg', '<iframe src=\'http://www.youtube.com/embed/JWPb3YLvBYo?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-11-14 20:51:02', '2016-11-14 20:51:02'),
(550, 156, 'Sabrina Korgut _ Pensando em Nada 220 Volts ', 'https://www.youtube.com/watch?v=EVAnytLisR8&index=3&list=PLcrydkWS_1zEvL_VjVVmnjwQb11rOUuAJ', 'youtube', 'EVAnytLisR8', 'EVAnytLisR8.jpg', '<iframe src=\'http://www.youtube.com/embed/EVAnytLisR8?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-11-14 20:51:04', '2016-11-14 20:51:04'),
(551, 171, 'Dicas de Beleza _ Terra', 'https://www.youtube.com/watch?v=YS676gr97eo', 'youtube', 'YS676gr97eo', 'YS676gr97eo.jpg', '<iframe src=\'http://www.youtube.com/embed/YS676gr97eo?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 6, '2016-11-14 21:20:39', '2016-11-14 21:20:39'),
(552, 156, 'Sabrina Korgut _ As Melhores Barbies _ Como Eliminar seu Chefe ', 'https://www.youtube.com/watch?v=vWOx7-dr_SM&list=PLcrydkWS_1zEvL_VjVVmnjwQb11rOUuAJ&index=4', 'youtube', 'vWOx7-dr_SM', 'vWOx7-dr_SM.jpg', '<iframe src=\'http://www.youtube.com/embed/vWOx7-dr_SM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2016-11-14 21:28:58', '2016-11-14 21:28:58'),
(553, 156, 'Sabrina Korgut_ Folhetim Ópera do Malandro 2003 Chico Buarque ', 'https://www.youtube.com/watch?v=CS-SH6mYxuc', 'youtube', 'CS-SH6mYxuc', 'CS-SH6mYxuc.jpg', '<iframe src=\'http://www.youtube.com/embed/CS-SH6mYxuc?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2016-11-14 21:28:59', '2016-11-14 21:28:59'),
(555, 49, 'André Vasco _ Casio ', 'https://www.youtube.com/watch?v=0L46f3oCC6c', 'youtube', '0L46f3oCC6c', '0L46f3oCC6c.jpg', '<iframe src=\'http://www.youtube.com/embed/0L46f3oCC6c?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-12-08 15:42:58', '2016-12-08 15:42:58'),
(556, 158, 'Erick Krominski _ Green Peace', 'https://www.youtube.com/watch?v=t7DGG-MRifA', 'youtube', 't7DGG-MRifA', 't7DGG-MRifA.jpg', '<iframe src=\'http://www.youtube.com/embed/t7DGG-MRifA?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-12-09 17:24:47', '2016-12-09 17:24:47'),
(557, 157, 'Erick Krominski _ Green Peace', 'https://www.youtube.com/watch?v=t7DGG-MRifA', 'youtube', 't7DGG-MRifA', 't7DGG-MRifA.jpg', '<iframe src=\'http://www.youtube.com/embed/t7DGG-MRifA?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-12-09 17:26:00', '2016-12-09 17:26:00'),
(559, 70, 'Vanessa Rozan _ Meu Gliter Minha Vida Noivas ', 'https://www.youtube.com/watch?v=8CEd1XWy4V0', 'youtube', '8CEd1XWy4V0', '8CEd1XWy4V0.jpg', '<iframe src=\'http://www.youtube.com/embed/8CEd1XWy4V0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2016-12-09 18:04:11', '2016-12-09 18:04:11'),
(560, 176, 'Fabiana Gomes _ Meu Gliter Minha Vida Noivas ', 'https://www.youtube.com/watch?v=8CEd1XWy4V0', 'youtube', '8CEd1XWy4V0', '8CEd1XWy4V0.jpg', '<iframe src=\'http://www.youtube.com/embed/8CEd1XWy4V0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2016-12-09 18:04:59', '2017-03-21 01:30:22'),
(561, 95, 'Vanessa Rozan_ Meu Gliter Minha Vida Noivas ', 'https://www.youtube.com/watch?v=8CEd1XWy4V0', 'youtube', '8CEd1XWy4V0', '8CEd1XWy4V0.jpg', '<iframe src=\'http://www.youtube.com/embed/8CEd1XWy4V0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2016-12-09 18:05:45', '2016-12-09 18:05:45'),
(562, 159, 'Andre Ramiro _ Luz Clara ', 'https://www.youtube.com/watch?v=g11NFcu1d5g&index=5&list=PLcrydkWS_1zESgE0pRilm9OCTstGaipUz', 'youtube', 'g11NFcu1d5g', 'g11NFcu1d5g.jpg', '<iframe src=\'http://www.youtube.com/embed/g11NFcu1d5g?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2016-12-20 19:30:04', '2017-02-19 22:07:55'),
(563, 42, 'Marília Moreno _ Vizzano ', 'https://www.youtube.com/watch?v=908l1HRK7KM&index=1&list=PLcrydkWS_1zHd2Hls3CllWIqeO0GCO6oF', 'youtube', '908l1HRK7KM', '908l1HRK7KM.jpg', '<iframe src=\'http://www.youtube.com/embed/908l1HRK7KM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-01-30 21:39:44', '2017-01-30 21:46:08'),
(565, 182, 'Mais Forte Que o Mundo _ A Historia de José Aldo', 'https://www.youtube.com/watch?v=Np6fLym7G4M&list=PLcrydkWS_1zFaO50ZfVU_0U7Xp4Po7HRb&index=2', 'youtube', 'Np6fLym7G4M', 'Np6fLym7G4M.jpg', '<iframe src=\'http://www.youtube.com/embed/Np6fLym7G4M?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2017-02-07 19:38:43', '2017-05-17 18:11:46'),
(568, 21, 'Paula Picarelli - KFS', 'https://www.youtube.com/watch?v=ccdtD1upbF4&list=PLcrydkWS_1zHW-gieSTkvyeXaDoaY5iHj&index=3', 'youtube', 'ccdtD1upbF4', 'ccdtD1upbF4.jpg', '<iframe src=\'http://www.youtube.com/embed/ccdtD1upbF4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2017-02-08 15:47:28', '2017-02-08 15:47:28'),
(570, 182, 'Clipe Banda Insonia ', 'https://vimeo.com/177895460', 'vimeo', '177895460', '177895460.jpg', '<iframe src="//player.vimeo.com/video/177895460?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 3, '2017-02-10 12:37:37', '2017-05-17 18:11:46'),
(571, 182, 'Cozinha ', 'https://vimeo.com/184886884', 'vimeo', '184886884', '184886884.jpg', '<iframe src="//player.vimeo.com/video/184886884?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 4, '2017-02-10 12:37:37', '2017-05-17 18:11:46'),
(572, 182, 'Liga Pontos ', 'https://vimeo.com/124168309', 'vimeo', '124168309', '124168309.jpg', '<iframe src="//player.vimeo.com/video/124168309?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 5, '2017-02-10 12:37:38', '2017-05-17 18:11:46'),
(573, 182, 'Bic Bond Bailarina', 'https://vimeo.com/97372580', 'vimeo', '97372580', '97372580.jpg', '<iframe src="//player.vimeo.com/video/97372580?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 6, '2017-02-10 12:37:39', '2017-05-17 18:11:46'),
(574, 182, 'Comercial Vivo HDTV', 'https://vimeo.com/94516313', 'vimeo', '94516313', '94516313.jpg', '<iframe src="//player.vimeo.com/video/94516313?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 7, '2017-02-10 12:37:40', '2017-05-17 18:11:46'),
(575, 182, 'Curta “SunSilk”', 'https://vimeo.com/91971592', 'vimeo', '91971592', '91971592.jpg', '<iframe src="//player.vimeo.com/video/91971592?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 8, '2017-02-10 12:37:41', '2017-05-17 18:11:46'),
(576, 182, 'Dança ', 'https://vimeo.com/130134300', 'vimeo', '130134300', '130134300.jpg', '<iframe src="//player.vimeo.com/video/130134300?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 9, '2017-02-10 12:37:41', '2017-05-17 18:11:46'),
(577, 182, 'Campanha Havaianas', 'https://vimeo.com/166342433', 'vimeo', '166342433', '166342433.jpg', '<iframe src="//player.vimeo.com/video/166342433?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 10, '2017-02-10 12:37:42', '2017-05-17 18:11:46'),
(580, 181, 'Thiago Amaral _ Uma Noite em Sampa ', 'https://www.youtube.com/watch?v=PKDXyde6dzs', 'youtube', 'PKDXyde6dzs', 'PKDXyde6dzs.jpg', '<iframe src=\'http://www.youtube.com/embed/PKDXyde6dzs?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2017-02-10 23:10:22', '2017-02-13 19:55:48'),
(581, 181, 'Thiago Amaral_ O Jardim ', 'https://vimeo.com/123008408', 'vimeo', '123008408', '123008408.jpg', '<iframe src="//player.vimeo.com/video/123008408?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 2, '2017-02-10 23:10:23', '2017-02-13 19:55:48'),
(582, 181, 'Thiago Amaral _ Ficção ', 'https://www.youtube.com/watch?v=e9PQA6AqKkE&list=PLcrydkWS_1zFXe4a-EcNhfIzcytCfcHQk&index=3', 'youtube', 'e9PQA6AqKkE', 'e9PQA6AqKkE.jpg', '<iframe src=\'http://www.youtube.com/embed/e9PQA6AqKkE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2017-02-10 23:10:23', '2017-02-13 19:55:49'),
(583, 181, 'Thiago Amaral _ Bollywwod Dance', 'https://www.youtube.com/watch?v=5SXQ8B0Nnuk&list=PLcrydkWS_1zFXe4a-EcNhfIzcytCfcHQk&index=4', 'youtube', '5SXQ8B0Nnuk', '5SXQ8B0Nnuk.jpg', '<iframe src=\'http://www.youtube.com/embed/5SXQ8B0Nnuk?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2017-02-10 23:10:24', '2017-02-13 20:03:11'),
(584, 181, 'Thiago Amaral _ A Garota da Moto', 'https://www.youtube.com/watch?v=i8T2MsU5B3A&index=1&list=PLcrydkWS_1zFXe4a-EcNhfIzcytCfcHQk', 'youtube', 'i8T2MsU5B3A', 'i8T2MsU5B3A.jpg', '<iframe src=\'http://www.youtube.com/embed/i8T2MsU5B3A?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-02-13 19:49:34', '2017-02-13 19:55:48'),
(585, 181, 'Thiago Amaral _ Cosmic Dance', 'https://www.youtube.com/watch?v=z9R8vZSdkis&index=5&list=PLcrydkWS_1zFXe4a-EcNhfIzcytCfcHQk', 'youtube', 'z9R8vZSdkis', 'z9R8vZSdkis.jpg', '<iframe src=\'http://www.youtube.com/embed/z9R8vZSdkis?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 5, '2017-02-13 20:03:13', '2017-02-13 20:03:13'),
(586, 181, 'Thiago Amaral _ Abertura da SPFH _ Fause Haten', 'https://www.youtube.com/watch?v=Q2_nz-G_8IQ', 'youtube', 'Q2_nz-G_8IQ', 'Q2_nz-G_8IQ.jpg', '<iframe src=\'http://www.youtube.com/embed/Q2_nz-G_8IQ?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 6, '2017-02-13 20:24:07', '2017-02-13 20:24:07'),
(587, 181, 'Thiago Amaral _ Te onde te Vejo Trailher ', 'https://www.youtube.com/watch?v=vP6CvBAduyc&index=7&list=PLcrydkWS_1zFXe4a-EcNhfIzcytCfcHQk', 'youtube', 'vP6CvBAduyc', 'vP6CvBAduyc.jpg', '<iframe src=\'http://www.youtube.com/embed/vP6CvBAduyc?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 7, '2017-02-13 20:24:08', '2017-02-13 20:24:08'),
(588, 181, 'Thiago Amaral _ Revista Bravo ', 'https://www.youtube.com/watch?v=BqGvntBzByU', 'youtube', 'BqGvntBzByU', 'BqGvntBzByU.jpg', '<iframe src=\'http://www.youtube.com/embed/BqGvntBzByU?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 8, '2017-02-13 20:24:09', '2017-02-13 20:24:09'),
(589, 178, 'Cena - 01 ', 'https://www.youtube.com/watch?v=1W8DbPH5FJ8', 'youtube', '1W8DbPH5FJ8', '1W8DbPH5FJ8.jpg', '<iframe src=\'http://www.youtube.com/embed/1W8DbPH5FJ8?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-02-15 14:19:36', '2017-02-15 14:19:36'),
(590, 178, 'Cena- 02 ', 'https://www.youtube.com/edit?video_id=z7TPY3xsdSM&video_referrer=watch', 'youtube', '0', '0.jpg', '<iframe src=\'http://www.youtube.com/embed/0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2017-02-15 14:19:37', '2017-02-15 14:19:37'),
(591, 159, 'Cena - 01 ', 'https://www.youtube.com/watch?v=JXhUgsysKRE&index=6&list=PLcrydkWS_1zESgE0pRilm9OCTstGaipUz', 'youtube', 'JXhUgsysKRE', 'JXhUgsysKRE.jpg', '<iframe src=\'http://www.youtube.com/embed/JXhUgsysKRE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-02-15 14:20:16', '2017-02-19 22:07:55'),
(592, 53, 'Campanha Motorola _ Festa Surpresa', 'https://www.youtube.com/watch?v=lDE9UF19yB4&list=PLcrydkWS_1zFRhO1Gv-khmwX1tyKVpQJd&index=4', 'youtube', 'lDE9UF19yB4', 'lDE9UF19yB4.jpg', '<iframe src=\'http://www.youtube.com/embed/lDE9UF19yB4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2017-02-20 14:34:54', '2017-05-17 14:37:01'),
(593, 185, 'Renata Vilela _ Separação', 'https://www.youtube.com/watch?v=JSYrr8BSb1k&index=1&list=PLcrydkWS_1zGooH8rbZVCnUWT59ycQrBg', 'youtube', 'JSYrr8BSb1k', 'JSYrr8BSb1k.jpg', '<iframe src=\'http://www.youtube.com/embed/JSYrr8BSb1k?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-03-06 20:04:14', '2017-03-06 20:04:14'),
(594, 185, 'Renata Vilela _ Triste Fim ', 'https://www.youtube.com/watch?v=3mxmcZjhXmY&index=2&list=PLcrydkWS_1zGooH8rbZVCnUWT59ycQrBg', 'youtube', '3mxmcZjhXmY', '3mxmcZjhXmY.jpg', '<iframe src=\'http://www.youtube.com/embed/3mxmcZjhXmY?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2017-03-06 20:04:15', '2017-03-06 20:04:15'),
(595, 185, 'Renata Vilela _ Tem Que Haver Algo _ Sweet Charity Brasil ', 'https://www.youtube.com/watch?v=4Hv9E6qF3wE&list=PLcrydkWS_1zGooH8rbZVCnUWT59ycQrBg&index=3', 'youtube', '4Hv9E6qF3wE', '4Hv9E6qF3wE.jpg', '<iframe src=\'http://www.youtube.com/embed/4Hv9E6qF3wE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2017-03-06 20:04:16', '2017-03-06 20:04:16'),
(597, 172, 'Cenas de Jeronimo Martins', 'https://www.youtube.com/watch?v=CfJjjxmvdU0&index=1&list=PLcrydkWS_1zFZJyvIomumrA9qag5BMfK8', 'youtube', 'CfJjjxmvdU0', 'CfJjjxmvdU0.jpg', '<iframe src=\'http://www.youtube.com/embed/CfJjjxmvdU0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2017-03-16 21:32:25', '2017-03-16 21:32:25'),
(599, 176, 'Fabiana Gomes _ DJ Boss in Drama Lista VIP Feat Karol Conka ', 'https://www.youtube.com/watch?v=tvbo8wKEDMA&index=1&list=PLcrydkWS_1zEfKUDj6uemwhdtMSQWCrce', 'youtube', 'tvbo8wKEDMA', 'tvbo8wKEDMA.jpg', '<iframe src=\'http://www.youtube.com/embed/tvbo8wKEDMA?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-03-21 14:04:26', '2017-03-21 14:04:26'),
(606, 191, 'Cris Carniato - Portfólio (Atriz) ', 'https://www.youtube.com/watch?v=wY5MY92VYAw&list=PLcrydkWS_1zFUn1Xs4z6F0U4sgO-EOB19&index=1', 'youtube', 'wY5MY92VYAw', 'wY5MY92VYAw.jpg', '<iframe src=\'http://www.youtube.com/embed/wY5MY92VYAw?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-03-29 15:15:43', '2017-03-29 15:15:43'),
(607, 191, 'Cris Carniato - Clipe (Publicidade) ', 'https://www.youtube.com/watch?v=Q0zlZ3iLJxk&index=2&list=PLcrydkWS_1zFUn1Xs4z6F0U4sgO-EOB19', 'youtube', 'Q0zlZ3iLJxk', 'Q0zlZ3iLJxk.jpg', '<iframe src=\'http://www.youtube.com/embed/Q0zlZ3iLJxk?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2017-03-29 15:15:43', '2017-03-29 15:15:43'),
(608, 191, 'Cris Carniato - Portfólio (Apresentadora) ', 'https://www.youtube.com/watch?v=OQvfIr8zccY&index=3&list=PLcrydkWS_1zFUn1Xs4z6F0U4sgO-EOB19', 'youtube', 'OQvfIr8zccY', 'OQvfIr8zccY.jpg', '<iframe src=\'http://www.youtube.com/embed/OQvfIr8zccY?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2017-03-29 15:15:43', '2017-03-29 15:15:43'),
(610, 188, 'Andre Lima _ Programa Caixa de Costura ', 'https://www.youtube.com/watch?v=dB1sbdTn7Pg', 'youtube', 'dB1sbdTn7Pg', 'dB1sbdTn7Pg.jpg', '<iframe src=\'http://www.youtube.com/embed/dB1sbdTn7Pg?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-04-11 19:47:51', '2017-04-11 19:47:51'),
(611, 55, 'Lilian Pacce _ Primeiro dia de MFW ', 'https://www.youtube.com/watch?v=yLktGBXfqLE&list=PLcrydkWS_1zGc3Z1Hbg5TK-jZezjzvMj7&index=3', 'youtube', 'yLktGBXfqLE', 'yLktGBXfqLE.jpg', '<iframe src=\'http://www.youtube.com/embed/yLktGBXfqLE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-04-11 20:11:11', '2017-04-11 20:11:11'),
(612, 102, 'Lilian Pacce _ Primeiro dia de MFW ', 'https://www.youtube.com/watch?v=yLktGBXfqLE&list=PLcrydkWS_1zGc3Z1Hbg5TK-jZezjzvMj7&index=3', 'youtube', 'yLktGBXfqLE', 'yLktGBXfqLE.jpg', '<iframe src=\'http://www.youtube.com/embed/yLktGBXfqLE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-04-11 20:13:52', '2017-04-11 20:13:52'),
(613, 145, 'Lilian Pacce_Primeiro dia de MFW', 'https://www.youtube.com/watch?v=yLktGBXfqLE&index=3&list=PLcrydkWS_1zGc3Z1Hbg5TK-jZezjzvMj7', 'youtube', 'yLktGBXfqLE', 'yLktGBXfqLE.jpg', '<iframe src=\'http://www.youtube.com/embed/yLktGBXfqLE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-04-11 20:22:28', '2017-04-11 20:22:28'),
(615, 190, 'Walcyr Carrasco _Êta Mundo Bom ', 'https://www.youtube.com/watch?v=JAcwA79lWmc&index=3&list=PLcrydkWS_1zHGHsh5FCKEsuJLYuGnQON7', 'youtube', 'JAcwA79lWmc', 'JAcwA79lWmc.jpg', '<iframe src=\'http://www.youtube.com/embed/JAcwA79lWmc?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-04-12 13:51:11', '2017-04-12 13:51:11'),
(616, 190, 'Walcyr Carrasco _ Verdades Secretas', 'https://www.youtube.com/watch?v=Lc9yeNWYQlE&index=1&list=PLcrydkWS_1zHGHsh5FCKEsuJLYuGnQON7&t=8s', 'youtube', 'Lc9yeNWYQlE', 'Lc9yeNWYQlE.jpg', '<iframe src=\'http://www.youtube.com/embed/Lc9yeNWYQlE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2017-04-12 13:51:11', '2017-04-12 13:51:11'),
(617, 190, 'Walcyr Carrasco _ Chocolate com Pimenta', 'https://www.youtube.com/watch?v=LjtrjqJFXFg&index=4&list=PLcrydkWS_1zHGHsh5FCKEsuJLYuGnQON7', 'youtube', 'LjtrjqJFXFg', 'LjtrjqJFXFg.jpg', '<iframe src=\'http://www.youtube.com/embed/LjtrjqJFXFg?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2017-04-12 13:51:12', '2017-04-12 13:51:12'),
(618, 190, 'Walcyr Carrasco _ Gabriela', 'https://www.youtube.com/watch?v=OYnf2RC1RhQ&index=2&list=PLcrydkWS_1zHGHsh5FCKEsuJLYuGnQON7', 'youtube', 'OYnf2RC1RhQ', 'OYnf2RC1RhQ.jpg', '<iframe src=\'http://www.youtube.com/embed/OYnf2RC1RhQ?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2017-04-12 13:51:13', '2017-04-12 13:51:13'),
(619, 159, 'Andre Ramiro _ Ação Social _ Baleia', 'https://www.youtube.com/watch?v=mhUoU3ZVbwE&list=PLcrydkWS_1zESgE0pRilm9OCTstGaipUz&index=7', 'youtube', 'mhUoU3ZVbwE', 'mhUoU3ZVbwE.jpg', '<iframe src=\'http://www.youtube.com/embed/mhUoU3ZVbwE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 7, '2017-04-13 20:45:55', '2017-04-13 20:45:55'),
(620, 194, 'Entranhas ', 'https://www.youtube.com/watch?v=zQm289aG4KU&list=PLcrydkWS_1zEjP6lhveaV_m7ATZNdNvG2&index=16', 'youtube', 'zQm289aG4KU', 'zQm289aG4KU.jpg', '<iframe src=\'http://www.youtube.com/embed/zQm289aG4KU?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2017-04-17 00:11:11', '2017-06-13 19:41:47'),
(621, 194, 'Sem Nome Definido ', 'https://www.youtube.com/watch?v=17hCwHWk7qw&t=2s', 'youtube', '17hCwHWk7qw', '17hCwHWk7qw.jpg', '<iframe src=\'http://www.youtube.com/embed/17hCwHWk7qw?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2017-04-17 00:11:11', '2017-06-13 19:41:47'),
(622, 194, 'COPPERTONE', 'https://www.youtube.com/watch?v=qvXepD2X4cE&list=PLcrydkWS_1zEjP6lhveaV_m7ATZNdNvG2&index=7', 'youtube', 'qvXepD2X4cE', 'qvXepD2X4cE.jpg', '<iframe src=\'http://www.youtube.com/embed/qvXepD2X4cE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2017-04-17 00:11:12', '2017-06-13 19:41:47'),
(623, 167, 'César Mello - Sinais de Fogo Cover', 'https://www.youtube.com/watch?v=3B_Yk6EA9EA&list=PLcrydkWS_1zEc1xd9fhmwcSI1gmHA8K90&index=1&t=1s', 'youtube', '3B_Yk6EA9EA', '3B_Yk6EA9EA.jpg', '<iframe src=\'http://www.youtube.com/embed/3B_Yk6EA9EA?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-04-18 21:39:14', '2017-04-18 21:39:14'),
(624, 195, 'Luciana Mello - Joia Rara', 'https://www.youtube.com/watch?v=oWfLEtpRaKI&list=PLcrydkWS_1zENBTeZkYLXx8fNzGtirXu1', 'youtube', 'oWfLEtpRaKI', 'oWfLEtpRaKI.jpg', '<iframe src=\'http://www.youtube.com/embed/oWfLEtpRaKI?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-04-18 21:41:10', '2017-04-18 21:41:10'),
(625, 198, 'Adriana Araújo _ Donald Trump toma posse em Washington EUA _ 2017 ', 'https://www.youtube.com/watch?v=2G2GLAEXtcQ&index=1&t=74s&list=PLcrydkWS_1zH2rTX-a5BJ1KgLuAUjVc2P', 'youtube', '2G2GLAEXtcQ', '2G2GLAEXtcQ.jpg', '<iframe src=\'http://www.youtube.com/embed/2G2GLAEXtcQ?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-04-24 16:56:36', '2017-04-24 16:56:36'),
(626, 198, 'Adriana Araújo _ Preparação de Cerimonia de Posse de Donald Trump _ 2017 ', 'https://www.youtube.com/watch?v=UPSnlDX6aRg&index=3&t=23s&list=PLcrydkWS_1zH2rTX-a5BJ1KgLuAUjVc2P', 'youtube', 'UPSnlDX6aRg', 'UPSnlDX6aRg.jpg', '<iframe src=\'http://www.youtube.com/embed/UPSnlDX6aRg?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2017-04-24 16:56:37', '2017-04-24 16:56:37'),
(627, 198, 'Adriana Araújo _ Impeachment de Dilma Rousseff _ 2016 ', 'https://www.youtube.com/watch?v=QQk5jOdUkGg&index=4&t=18s&list=PLcrydkWS_1zH2rTX-a5BJ1KgLuAUjVc2P', 'youtube', 'QQk5jOdUkGg', 'QQk5jOdUkGg.jpg', '<iframe src=\'http://www.youtube.com/embed/QQk5jOdUkGg?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2017-04-24 16:56:38', '2017-04-24 16:56:38'),
(628, 33, 'H&H Menos é Demais', 'https://www.youtube.com/watch?v=ymlZO_gEss4', 'youtube', 'ymlZO_gEss4', 'ymlZO_gEss4.jpg', '<iframe src=\'http://www.youtube.com/embed/ymlZO_gEss4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2017-04-24 20:41:44', '2017-05-05 15:04:47'),
(629, 77, 'H&H Menos é Demais', 'https://www.youtube.com/watch?v=ymlZO_gEss4', 'youtube', 'ymlZO_gEss4', 'ymlZO_gEss4.jpg', '<iframe src=\'http://www.youtube.com/embed/ymlZO_gEss4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2017-04-24 20:42:43', '2017-05-05 15:07:12'),
(630, 106, 'H&H Menos é Demais', 'https://www.youtube.com/watch?v=ymlZO_gEss4', 'youtube', 'ymlZO_gEss4', 'ymlZO_gEss4.jpg', '<iframe src=\'http://www.youtube.com/embed/ymlZO_gEss4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-04-24 20:43:37', '2017-04-24 20:43:37'),
(631, 187, 'Adriana Couto_ As novas mídias', 'https://www.youtube.com/watch?v=FFUpR-BcWcs&index=1&list=PLcrydkWS_1zFjJP7FWoFaLdmDboCa1drK', 'youtube', 'FFUpR-BcWcs', 'FFUpR-BcWcs.jpg', '<iframe src=\'http://www.youtube.com/embed/FFUpR-BcWcs?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-05-04 21:08:20', '2017-05-04 21:29:08'),
(632, 187, 'Adriana Couto_ Metrópolis_ Mano Brown', 'https://www.youtube.com/watch?v=YvHJIbGj2ks&index=2&list=PLcrydkWS_1zFjJP7FWoFaLdmDboCa1drK', 'youtube', 'YvHJIbGj2ks', 'YvHJIbGj2ks.jpg', '<iframe src=\'http://www.youtube.com/embed/YvHJIbGj2ks?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2017-05-04 21:08:20', '2017-05-04 21:29:08'),
(633, 187, 'Adriana Couto_ TV Cocoricó_ Participação', 'https://www.youtube.com/watch?v=yD8a7U6nj3g&list=PLcrydkWS_1zFjJP7FWoFaLdmDboCa1drK&index=3', 'youtube', 'yD8a7U6nj3g', 'yD8a7U6nj3g.jpg', '<iframe src=\'http://www.youtube.com/embed/yD8a7U6nj3g?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2017-05-04 21:08:22', '2017-05-04 21:18:29'),
(635, 187, 'Adriana Couto_ Metrópolis_ Os Trapalhões ', 'https://www.youtube.com/watch?v=7ADBO1MST_4&index=4&list=PLcrydkWS_1zFjJP7FWoFaLdmDboCa1drK', 'youtube', '7ADBO1MST_4', '7ADBO1MST_4.jpg', '<iframe src=\'http://www.youtube.com/embed/7ADBO1MST_4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2017-05-04 21:25:10', '2017-05-04 21:29:08'),
(636, 33, 'Dicas da Chiara_ novos negócios_ Menos é demais', 'https://www.youtube.com/watch?v=mlw4YVhAwik&index=6&list=PLcrydkWS_1zFbnKVt6m2ZFP02a5DLivrK', 'youtube', 'mlw4YVhAwik', 'mlw4YVhAwik.jpg', '<iframe src=\'http://www.youtube.com/embed/mlw4YVhAwik?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-05-05 15:04:45', '2017-05-05 15:04:45'),
(637, 33, 'Dicas da Chiara_ consumidor consciente_ Menos é demais', 'https://www.youtube.com/watch?v=AM33T0pAC60&list=PLcrydkWS_1zFbnKVt6m2ZFP02a5DLivrK&index=8', 'youtube', 'AM33T0pAC60', 'AM33T0pAC60.jpg', '<iframe src=\'http://www.youtube.com/embed/AM33T0pAC60?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2017-05-05 15:04:47', '2017-05-05 15:04:47'),
(638, 77, 'Dicas da Chiara_ guarda-roupa_ Menos é demais', 'https://www.youtube.com/watch?v=TRjFXSWHFYQ&index=5&list=PLcrydkWS_1zFbnKVt6m2ZFP02a5DLivrK', 'youtube', 'TRjFXSWHFYQ', 'TRjFXSWHFYQ.jpg', '<iframe src=\'http://www.youtube.com/embed/TRjFXSWHFYQ?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-05-05 15:07:11', '2017-05-05 15:07:11'),
(639, 77, 'Dicas da Chiara_ nova moda_ Menos é demais', 'https://www.youtube.com/watch?v=BQkfH3mNimI&list=PLcrydkWS_1zFbnKVt6m2ZFP02a5DLivrK&index=7', 'youtube', 'BQkfH3mNimI', 'BQkfH3mNimI.jpg', '<iframe src=\'http://www.youtube.com/embed/BQkfH3mNimI?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2017-05-05 15:07:12', '2017-05-05 15:07:12'),
(640, 149, 'O Rico e Lázaro', 'https://www.youtube.com/watch?v=wI-5i_vvb8A&index=2&list=PLcrydkWS_1zH8esXL3wy9r1nrOZONrkzp', 'youtube', 'wI-5i_vvb8A', 'wI-5i_vvb8A.jpg', '<iframe src=\'http://www.youtube.com/embed/wI-5i_vvb8A?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-05-17 13:58:55', '2017-05-17 13:58:55'),
(643, 182, 'Personagem Tainá_ Rock Story', 'https://www.youtube.com/watch?v=MUWOEMjNt9Q&list=PLcrydkWS_1zFaO50ZfVU_0U7Xp4Po7HRb&index=6', 'youtube', 'MUWOEMjNt9Q', 'MUWOEMjNt9Q.jpg', '<iframe src=\'http://www.youtube.com/embed/MUWOEMjNt9Q?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-05-17 18:11:45', '2017-05-17 18:11:45'),
(644, 182, 'Cena Personagem Tainá_Rock Story', 'https://www.youtube.com/watch?v=so5O8RDx5dM&index=5&list=PLcrydkWS_1zFaO50ZfVU_0U7Xp4Po7HRb', 'youtube', 'so5O8RDx5dM', 'so5O8RDx5dM.jpg', '<iframe src=\'http://www.youtube.com/embed/so5O8RDx5dM?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2017-05-17 18:11:46', '2017-05-17 18:11:46'),
(648, 201, 'André Dias_Avenida Q Racistas ', 'https://www.youtube.com/watch?v=2nsTFjvgBMQ&index=3&list=PLcrydkWS_1zGJtqAo1jU179wkjZhhVYyQ', 'youtube', '2nsTFjvgBMQ', '2nsTFjvgBMQ.jpg', '<iframe src=\'http://www.youtube.com/embed/2nsTFjvgBMQ?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-05-24 18:56:47', '2017-05-24 18:56:47'),
(649, 201, 'André Dias_Programa Todo Seu', 'https://www.youtube.com/watch?v=Izc4NHZJw3s&index=2&list=PLcrydkWS_1zGJtqAo1jU179wkjZhhVYyQ&t=20s', 'youtube', 'Izc4NHZJw3s', 'Izc4NHZJw3s.jpg', '<iframe src=\'http://www.youtube.com/embed/Izc4NHZJw3s?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2017-05-24 18:56:47', '2017-05-24 18:56:47'),
(650, 201, '10 minutos com André Dias', 'https://www.youtube.com/watch?v=dDu5gFIRT9I&list=PLcrydkWS_1zGJtqAo1jU179wkjZhhVYyQ&index=4', 'youtube', 'dDu5gFIRT9I', 'dDu5gFIRT9I.jpg', '<iframe src=\'http://www.youtube.com/embed/dDu5gFIRT9I?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2017-05-24 18:56:47', '2017-05-24 18:56:47'),
(651, 53, 'Dudu Bertholini_ Nós os Fashionistas ', 'https://www.youtube.com/watch?v=PNgRKc-vFV4&list=PLcrydkWS_1zFRhO1Gv-khmwX1tyKVpQJd&index=5', 'youtube', 'PNgRKc-vFV4', 'PNgRKc-vFV4.jpg', '<iframe src=\'http://www.youtube.com/embed/PNgRKc-vFV4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-05-24 19:07:38', '2017-05-24 19:07:38'),
(655, 194, 'Cena PM', 'https://www.youtube.com/watch?v=8iSBafZeIqg&index=17&list=PLcrydkWS_1zEjP6lhveaV_m7ATZNdNvG2', 'youtube', '8iSBafZeIqg', '8iSBafZeIqg.jpg', '<iframe src=\'http://www.youtube.com/embed/8iSBafZeIqg?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-06-13 19:41:40', '2017-06-13 19:41:40'),
(656, 194, 'Cena NM', 'https://www.youtube.com/watch?v=yhohrPQ6BBI&index=18&list=PLcrydkWS_1zEjP6lhveaV_m7ATZNdNvG2', 'youtube', 'yhohrPQ6BBI', 'yhohrPQ6BBI.jpg', '<iframe src=\'http://www.youtube.com/embed/yhohrPQ6BBI?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2017-06-13 19:41:47', '2017-06-13 19:41:47'),
(657, 197, 'Comercial Nestlé Grego Light', 'https://www.youtube.com/watch?v=02RpHJhFWZo&list=PLcrydkWS_1zFf8HWYi1HZneOBPif8le3R&index=2', 'youtube', '02RpHJhFWZo', '02RpHJhFWZo.jpg', '<iframe src=\'http://www.youtube.com/embed/02RpHJhFWZo?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-06-16 18:31:39', '2017-06-16 18:31:39'),
(658, 45, 'Renata Fan Terra ', 'https://www.youtube.com/watch?v=y4I53T6pNwQ&index=4&list=PLcrydkWS_1zGuqJzXTEVXFRnhDtQo0406', 'youtube', 'y4I53T6pNwQ', 'y4I53T6pNwQ.jpg', '<iframe src=\'http://www.youtube.com/embed/y4I53T6pNwQ?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-06-26 15:21:23', '2017-06-26 17:06:04'),
(659, 207, 'Uma Ilha na Lua Acústico', 'https://www.youtube.com/watch?v=V1R2cD-HbV8&list=PLcrydkWS_1zEQujv88sOelwHr_qnfy3cr', 'youtube', 'V1R2cD-HbV8', 'V1R2cD-HbV8.jpg', '<iframe src=\'http://www.youtube.com/embed/V1R2cD-HbV8?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-07-05 13:46:59', '2017-07-05 13:46:59'),
(660, 49, 'André Vasco _ Hum Minutinho _ Parada LGBT', 'https://www.youtube.com/watch?v=qS06kkF5I7s', 'youtube', 'qS06kkF5I7s', 'qS06kkF5I7s.jpg', '<iframe src=\'http://www.youtube.com/embed/qS06kkF5I7s?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 13, '2017-07-07 13:48:52', '2017-07-07 13:48:52'),
(661, 183, 'VS', 'https://www.youtube.com/watch?v=rJtWwvPHMDw&index=1&list=PLcrydkWS_1zEKr09JbVGu9ENRHqoeXaYK', 'youtube', 'rJtWwvPHMDw', 'rJtWwvPHMDw.jpg', '<iframe src=\'http://www.youtube.com/embed/rJtWwvPHMDw?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-07-07 18:47:44', '2017-07-07 18:47:44'),
(662, 183, 'JR', 'https://www.youtube.com/watch?v=zWBJaXRbBs4&list=PLcrydkWS_1zEKr09JbVGu9ENRHqoeXaYK&index=2', 'youtube', 'zWBJaXRbBs4', 'zWBJaXRbBs4.jpg', '<iframe src=\'http://www.youtube.com/embed/zWBJaXRbBs4?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2017-07-07 18:50:07', '2017-07-07 18:50:07'),
(663, 183, 'EMB', 'https://www.youtube.com/watch?v=dk-L4j-op80&index=3&list=PLcrydkWS_1zEKr09JbVGu9ENRHqoeXaYK', 'youtube', 'dk-L4j-op80', 'dk-L4j-op80.jpg', '<iframe src=\'http://www.youtube.com/embed/dk-L4j-op80?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 2, '2017-07-07 18:50:07', '2017-07-07 18:50:07'),
(664, 183, 'DI', 'https://www.youtube.com/watch?v=4MQZ8JhMxRA&list=PLcrydkWS_1zEKr09JbVGu9ENRHqoeXaYK&index=4', 'youtube', '4MQZ8JhMxRA', '4MQZ8JhMxRA.jpg', '<iframe src=\'http://www.youtube.com/embed/4MQZ8JhMxRA?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 3, '2017-07-07 18:50:08', '2017-07-07 18:50:08'),
(665, 183, 'AAV', 'https://www.youtube.com/watch?v=StZRiNIEqPg&list=PLcrydkWS_1zEKr09JbVGu9ENRHqoeXaYK&index=5', 'youtube', 'StZRiNIEqPg', 'StZRiNIEqPg.jpg', '<iframe src=\'http://www.youtube.com/embed/StZRiNIEqPg?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 4, '2017-07-07 18:50:08', '2017-07-07 18:50:08'),
(666, 183, 'ADH', 'https://www.youtube.com/watch?v=uUFtr8blyp0&index=6&list=PLcrydkWS_1zEKr09JbVGu9ENRHqoeXaYK', 'youtube', 'uUFtr8blyp0', 'uUFtr8blyp0.jpg', '<iframe src=\'http://www.youtube.com/embed/uUFtr8blyp0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 5, '2017-07-07 18:50:09', '2017-07-07 18:50:09'),
(667, 52, 'O Pavilhão Dançando pelo Estudio Guto Requena - Projeto VÍDEO SKOL feito para as Olimpíadas 2016', 'https://www.youtube.com/watch?v=7lAC4lg7iVQ', 'youtube', '7lAC4lg7iVQ', '7lAC4lg7iVQ.jpg', '<iframe src=\'http://www.youtube.com/embed/7lAC4lg7iVQ?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-07-19 17:05:59', '2017-07-19 17:05:59'),
(668, 52, 'NÓS SOMOS: Estudio Guto Requena', 'https://www.youtube.com/watch?v=rZz1Y55-GXk&t=7s', 'youtube', 'rZz1Y55-GXk', 'rZz1Y55-GXk.jpg', '<iframe src=\'http://www.youtube.com/embed/rZz1Y55-GXk?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2017-07-19 17:05:59', '2017-07-19 17:05:59'),
(669, 189, 'Apresentação', 'https://www.youtube.com/watch?v=hxIZAqdc2mw&list=PLcrydkWS_1zHYmHbMAXqD1VWjssNFV-6H&index=2', 'youtube', 'hxIZAqdc2mw', 'hxIZAqdc2mw.jpg', '<iframe src=\'http://www.youtube.com/embed/hxIZAqdc2mw?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-07-19 21:23:42', '2017-07-19 21:23:42'),
(670, 212, 'Vingança', 'https://www.youtube.com/watch?v=Ugq8xtm1n9A&list=PLcrydkWS_1zEFLHkvVf0PL-vzvxn05i3T&index=1', 'youtube', 'Ugq8xtm1n9A', 'Ugq8xtm1n9A.jpg', '<iframe src=\'http://www.youtube.com/embed/Ugq8xtm1n9A?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-07-24 13:39:14', '2017-07-24 13:39:14'),
(671, 212, 'Gota D´Água ', 'https://www.youtube.com/watch?v=88EsT_eV9kc&list=PLcrydkWS_1zEFLHkvVf0PL-vzvxn05i3T&index=2', 'youtube', '88EsT_eV9kc', '88EsT_eV9kc.jpg', '<iframe src=\'http://www.youtube.com/embed/88EsT_eV9kc?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2017-07-24 13:39:14', '2017-07-24 13:39:14'),
(672, 67, 'Entre Aspas  Chamada Globonews   ', 'https://www.youtube.com/watch?v=817enKiurJA&list=PLcrydkWS_1zHdzW4lS0G4bDHX-eWAwy81', 'youtube', '817enKiurJA', '817enKiurJA.jpg', '<iframe src=\'http://www.youtube.com/embed/817enKiurJA?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-07-24 18:47:03', '2017-07-24 18:47:03'),
(673, 206, 'Estreia GN', 'https://www.youtube.com/watch?v=ONYpij7JEcY&index=1&list=PLcrydkWS_1zHc8cUK2xUmMlJ0RAPTwKJj', 'youtube', 'ONYpij7JEcY', 'ONYpij7JEcY.jpg', '<iframe src=\'http://www.youtube.com/embed/ONYpij7JEcY?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-07-24 20:18:23', '2017-07-24 20:18:23'),
(674, 206, 'Jornal da Band_Desafio Master Chef', 'https://www.youtube.com/watch?v=Vtcj8KxCtdY&index=2&list=PLcrydkWS_1zHc8cUK2xUmMlJ0RAPTwKJj', 'youtube', 'Vtcj8KxCtdY', 'Vtcj8KxCtdY.jpg', '<iframe src=\'http://www.youtube.com/embed/Vtcj8KxCtdY?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2017-07-24 20:18:24', '2017-07-24 20:18:24'),
(675, 206, 'Portfólio Aline Midlej', 'https://vimeo.com/220887254', 'vimeo', '220887254', '220887254.jpg', '<iframe src="//player.vimeo.com/video/220887254?color=f05a8e&portrait=0&autoplay=1&badge=0&title=0&byline=0"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', 2, '2017-07-24 20:18:25', '2017-07-24 20:18:25'),
(676, 210, 'Olavo e Bebel ', 'https://www.youtube.com/watch?v=iK1MJL6I22g&t=82s', 'youtube', 'iK1MJL6I22g', 'iK1MJL6I22g.jpg', '<iframe src=\'http://www.youtube.com/embed/iK1MJL6I22g?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-07-25 01:54:37', '2017-07-25 01:54:37'),
(677, 210, 'Beto ', 'https://www.youtube.com/watch?v=RRevUZKF6FE', 'youtube', 'RRevUZKF6FE', 'RRevUZKF6FE.jpg', '<iframe src=\'http://www.youtube.com/embed/RRevUZKF6FE?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2017-07-25 01:54:37', '2017-07-25 01:54:37'),
(678, 213, 'Janaina Afhonso _ Café Pelé_Partida de Xadrez para Cannes', 'https://www.youtube.com/watch?v=E2oP0Ii1Gsg&list=PLcrydkWS_1zE89zrzNjHb0C1OkLn7swjK&index=2', 'youtube', 'E2oP0Ii1Gsg', 'E2oP0Ii1Gsg.jpg', '<iframe src=\'http://www.youtube.com/embed/E2oP0Ii1Gsg?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 0, '2017-07-25 21:24:56', '2017-07-25 21:24:56'),
(679, 213, 'Janaina Afhonso _ Comercial da SKY com Giba', 'https://www.youtube.com/watch?v=vKRgKchM_c0&list=PLcrydkWS_1zE89zrzNjHb0C1OkLn7swjK', 'youtube', 'vKRgKchM_c0', 'vKRgKchM_c0.jpg', '<iframe src=\'http://www.youtube.com/embed/vKRgKchM_c0?autoplay=1\'  frameborder=\'0\' allowfullscreen></iframe>', 1, '2017-07-25 21:24:58', '2017-07-25 21:24:58');

-- --------------------------------------------------------

--
-- Estrutura da tabela `portfolio_workshops`
--

CREATE TABLE `portfolio_workshops` (
  `id` int(10) UNSIGNED NOT NULL,
  `portfolio_personalidades_id` int(10) UNSIGNED NOT NULL,
  `titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `autor` text COLLATE utf8_unicode_ci NOT NULL,
  `editora` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `publicacoes`
--

CREATE TABLE `publicacoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `publicacoes`
--

INSERT INTO `publicacoes` (`id`, `texto`, `created_at`, `updated_at`) VALUES
(1, '<p>O nosso Departamento de Publica&ccedil;&otilde;es surgiu com a vontade e necessidade de oferecer um trabalho + plural para nossos clientes. Buscamos inserir no mercado editorial livros, tanto de autores consagrados quanto de novos talentos, sempre alinhados com a vis&atilde;o que temos em todo nosso escrit&oacute;rio.</p>\r\n\r\n<p>Muito mais do que publicar livros, buscamos direcionar a carreira do autor e dar suporte a todas as etapas que envolvem esta produ&ccedil;&atilde;o, como a escolha da editora correta, gerenciar deadlines entre autor e editora, cuidar da diagrama&ccedil;&atilde;o e escolha das fotos, alinhar reuni&otilde;es entre os envolvidos no projeto, escolher os melhores eventos e livrarias para lan&ccedil;amento do livro, entre outros assuntos. Tudo isso para garantir que o projeto seja sucesso de cr&iacute;tica e vendas.</p>\r\n\r\n<p>A ideia &eacute; que o autor continue sua hist&oacute;ria e ideais por meio das publica&ccedil;&otilde;es.</p>\r\n\r\n<p>Entre os projetos j&aacute; lan&ccedil;ados, temos dois sucessos: &ldquo;Maquiagem&rdquo;, escrito por Duda Molinos e lan&ccedil;ado pela Editora Senac que j&aacute; est&aacute; na 11&ordf; Edi&ccedil;&atilde;o. O livro foi um marco na carreira de Duda e no mundo da maquiagem, que teve, nesta obra, uma verdadeira enciclop&eacute;dia para quem quer ingressar no aprendizado da maquiagem.</p>\r\n\r\n<p>Outro sucesso foi &ldquo;O Melhor Guia de Nova York&rdquo;, escrito pelo jornalista Pedro Andrade e lan&ccedil;ado pela editora Rocco. Em pouco tempo o livro se tornou cl&aacute;ssico e fundamental para todos que querem conhecer uma das cidades mais din&acirc;micas e encantadoras do mundo. O livro ficou por semanas seguidas na lista dos mais vendidos da Revista Veja.</p>\r\n\r\n<p>No prelo j&aacute; temos o livro de Costanza Pascolato, Paulo Borges, Barbara Gancia, Lilian Pacce, Jamil Chade, Izabella Camargo, Ronald Rios, Claudio Tognolli, Paula Picarelli, e Astrid Fontenelle. Agora &eacute; aguardar as novidades!&nbsp;</p>\r\n\r\n<p>Aqui voc&ecirc; tamb&eacute;m poder&aacute; conhecer todas as outras publica&ccedil;&otilde;es de nossos agenciados. Agora &eacute; s&oacute; correr para a livraria!</p>\r\n\r\n<p>Devido ao nosso relacionamento com as editoras, estamos buscando novos autores que tenham uma boa hist&oacute;ria pra contar!</p>\r\n\r\n<p>Entre em contato com nossa equipe e conhe&ccedil;a mais do departamento de Publica&ccedil;&otilde;es.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '0000-00-00 00:00:00', '2017-01-23 15:04:33');

-- --------------------------------------------------------

--
-- Estrutura da tabela `publicacoes_imagens`
--

CREATE TABLE `publicacoes_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `publicacoes_id` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `publicacoes_imagens`
--

INSERT INTO `publicacoes_imagens` (`id`, `publicacoes_id`, `imagem`, `ordem`, `created_at`, `updated_at`) VALUES
(1776, 1, '20160928112940livro-lilian-pacce.jpg', 0, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1777, 1, '2015120115255311.jpg', 1, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1778, 1, '2015120917215920151203114935astrid-livro.jpg', 2, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1779, 1, '20151203114943costanza.jpg', 3, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1780, 1, '2015121010481520151209205550image.png', 4, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1781, 1, '20170725152003lobao-1.jpg', 5, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1782, 1, '20170424184431livro-memorial-escandalo.jpg', 6, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1783, 1, '20161019101840livro-jamil-chade.jpg', 7, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1784, 1, '201512011525462.jpg', 8, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1785, 1, '201512011525468.jpg', 9, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1786, 1, '201512011525483.jpg', 10, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1787, 1, '20161019102330livro-jamil-politica.jpg', 11, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1788, 1, '20151201152611mara-luquet.jpg', 12, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1789, 1, '20151201152557costanza-estampa.jpg', 13, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1790, 1, '20151201152609josimar-melo10.jpg', 14, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1791, 1, '20151201152610lilian-pacce.jpg', 15, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1792, 1, '201512011525475.jpg', 16, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1793, 1, '20151201152600josimar-15.jpg', 17, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1794, 1, '20170424184432livro-segredos-do-conclave.jpg', 18, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1795, 1, '20151201152611sem-titulo-1.jpg', 19, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1796, 1, '20151201152553erika-palomino-2.jpg', 20, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1797, 1, '20151209173037sem-titulo-1.jpg', 21, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1798, 1, '20151201152604josimar-melo-6.jpg', 22, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1799, 1, '20151201152553costanza.jpg', 23, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1800, 1, '20151209174146j.jpg', 24, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1801, 1, '20151201183618silvia.jpg', 25, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1802, 1, '20151201152602josimar-07.jpg', 26, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1803, 1, '201512011525487.jpg', 27, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1804, 1, '20151201152552erika-palomino-3.jpg', 28, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1805, 1, '20170725152013lobao-2.jpg', 29, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1806, 1, '20151201152611josimar-flavour.jpg', 30, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1807, 1, '20151201152605josimar-melo-7.jpg', 31, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1808, 1, '20151201152607livro-costanza-mascara.jpg', 32, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1809, 1, '20151201152600josimar-13.jpg', 33, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1810, 1, '20151201152603josimar-melo-5.jpg', 34, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1811, 1, '20170725152020lobao-3.jpg', 35, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1812, 1, '20151201152603josimar-14.jpg', 36, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1813, 1, '20170725151500leao-serva-2-certo.jpg', 37, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1814, 1, '20151201152610mara-luquet-2.jpg', 38, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1815, 1, '20151201152606josimar-melo-9.jpg', 39, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1816, 1, '20170725152027lobao-4.jpg', 40, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1817, 1, '20170725151438patricia-campos-mello-1-certo.jpg', 41, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1818, 1, '20151209173651josimar-melo-acerveja.jpg', 42, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1819, 1, '20170725151517leao-serva-certo.jpg', 43, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1820, 1, '20151201152549costanza-3.jpg', 44, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1821, 1, '20170725151508leao-serva-3-certo.jpg', 45, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1822, 1, '20151215152417chris.jpg', 46, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1823, 1, '20151201152600josimar-11.jpg', 47, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1824, 1, '20151207172702josimar-16.jpg', 48, '2017-07-25 18:21:50', '2017-07-25 18:21:50'),
(1825, 1, '20170725151450patricia-campos-mello-2-certo.jpg', 49, '2017-07-25 18:21:50', '2017-07-25 18:21:50');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$9SBl0jwcm5/nYejJeCRob.AA0ALzUhrxXBG1b2xrkazBB7FPqP.B6', 'aDXMRCglTJfBJ6HOPANtr24xKzSrbJqSnqqM6x3Tjr5v0fB6sF8V7K62XQHE', '0000-00-00 00:00:00', '2016-01-15 13:34:27'),
(2, 'caico', 'caico@caicodequeiroz.com.br', '$2y$10$HR/nN1/EbtTtI0/mSO74fujcjM6bTa0ghT5bYmg6GBeyctJiAXNeq', 'KYjal3BkmswOthItgtaSPvjEwAY7Zo7SpcINyQmpdpBNxPniNDELSuQJxVlK', '2015-08-24 17:42:20', '2017-07-25 21:25:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `acao_social`
--
ALTER TABLE `acao_social`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `acao_social_imagens`
--
ALTER TABLE `acao_social_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `acao_social_imagens_acao_social_id_foreign` (`acao_social_id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imprensa`
--
ALTER TABLE `imprensa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `juridico`
--
ALTER TABLE `juridico`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `licenciamento`
--
ALTER TABLE `licenciamento`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `licenciamento_imagens`
--
ALTER TABLE `licenciamento_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `licenciamento_imagens_licenciamento_id_foreign` (`licenciamento_id`);

--
-- Indexes for table `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parcerias`
--
ALTER TABLE `parcerias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `portfolio_acao_social`
--
ALTER TABLE `portfolio_acao_social`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_acao_social_portfolio_personalidades_id_foreign` (`portfolio_personalidades_id`);

--
-- Indexes for table `portfolio_capas`
--
ALTER TABLE `portfolio_capas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_capas_portfolio_personalidades_id_foreign` (`portfolio_personalidades_id`);

--
-- Indexes for table `portfolio_categorias`
--
ALTER TABLE `portfolio_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portfolio_consultoria_moda`
--
ALTER TABLE `portfolio_consultoria_moda`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_consultoria_moda_portfolio_personalidades_id_foreign` (`portfolio_personalidades_id`);

--
-- Indexes for table `portfolio_creditos`
--
ALTER TABLE `portfolio_creditos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_creditos_portfolio_personalidades_id_foreign` (`portfolio_personalidades_id`);

--
-- Indexes for table `portfolio_desfiles`
--
ALTER TABLE `portfolio_desfiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_desfiles_portfolio_personalidades_id_foreign` (`portfolio_personalidades_id`);

--
-- Indexes for table `portfolio_espetaculos`
--
ALTER TABLE `portfolio_espetaculos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_espetaculos_portfolio_personalidades_id_foreign` (`portfolio_personalidades_id`);

--
-- Indexes for table `portfolio_extras`
--
ALTER TABLE `portfolio_extras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_extras_portfolio_personalidades_id_foreign` (`portfolio_personalidades_id`);

--
-- Indexes for table `portfolio_fotos`
--
ALTER TABLE `portfolio_fotos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_fotos_portfolio_personalidades_id_foreign` (`portfolio_personalidades_id`);

--
-- Indexes for table `portfolio_idiomas`
--
ALTER TABLE `portfolio_idiomas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `portfolio_idiomas_titulo_unique` (`titulo`);

--
-- Indexes for table `portfolio_idiomas_personalidades`
--
ALTER TABLE `portfolio_idiomas_personalidades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_idiomas_personalidades_personalidade_foreign` (`personalidade`),
  ADD KEY `portfolio_idiomas_personalidades_idioma_foreign` (`idioma`);

--
-- Indexes for table `portfolio_palestras`
--
ALTER TABLE `portfolio_palestras`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_palestras_portfolio_personalidades_id_foreign` (`portfolio_personalidades_id`);

--
-- Indexes for table `portfolio_paralaxe`
--
ALTER TABLE `portfolio_paralaxe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_paralaxe_portfolio_personalidades_id_foreign` (`portfolio_personalidades_id`);

--
-- Indexes for table `portfolio_personalidades`
--
ALTER TABLE `portfolio_personalidades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_personalidades_portfolio_categorias_id_foreign` (`portfolio_categorias_id`),
  ADD KEY `portfolio_personalidades_portfolio_subcategorias_id_foreign` (`portfolio_subcategorias_id`);

--
-- Indexes for table `portfolio_premios`
--
ALTER TABLE `portfolio_premios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_premios_portfolio_personalidades_id_foreign` (`portfolio_personalidades_id`);

--
-- Indexes for table `portfolio_publicacoes`
--
ALTER TABLE `portfolio_publicacoes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_publicacoes_portfolio_personalidades_id_foreign` (`portfolio_personalidades_id`);

--
-- Indexes for table `portfolio_publicidade`
--
ALTER TABLE `portfolio_publicidade`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_publicidade_portfolio_personalidades_id_foreign` (`portfolio_personalidades_id`);

--
-- Indexes for table `portfolio_redes_sociais`
--
ALTER TABLE `portfolio_redes_sociais`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_redes_sociais_portfolio_personalidades_id_foreign` (`portfolio_personalidades_id`);

--
-- Indexes for table `portfolio_red_carpet`
--
ALTER TABLE `portfolio_red_carpet`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_red_carpet_portfolio_personalidades_id_foreign` (`portfolio_personalidades_id`);

--
-- Indexes for table `portfolio_subcategorias`
--
ALTER TABLE `portfolio_subcategorias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_subcategorias_portfolio_categorias_id_foreign` (`portfolio_categorias_id`);

--
-- Indexes for table `portfolio_videos`
--
ALTER TABLE `portfolio_videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_videos_portfolio_personalidades_id_foreign` (`portfolio_personalidades_id`);

--
-- Indexes for table `portfolio_workshops`
--
ALTER TABLE `portfolio_workshops`
  ADD PRIMARY KEY (`id`),
  ADD KEY `portfolio_workshops_portfolio_personalidades_id_foreign` (`portfolio_personalidades_id`);

--
-- Indexes for table `publicacoes`
--
ALTER TABLE `publicacoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `publicacoes_imagens`
--
ALTER TABLE `publicacoes_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `publicacoes_imagens_publicacoes_id_foreign` (`publicacoes_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `acao_social`
--
ALTER TABLE `acao_social`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `acao_social_imagens`
--
ALTER TABLE `acao_social_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=956;
--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `imprensa`
--
ALTER TABLE `imprensa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `juridico`
--
ALTER TABLE `juridico`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `licenciamento`
--
ALTER TABLE `licenciamento`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `licenciamento_imagens`
--
ALTER TABLE `licenciamento_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=589;
--
-- AUTO_INCREMENT for table `marca`
--
ALTER TABLE `marca`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `parcerias`
--
ALTER TABLE `parcerias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `portfolio_acao_social`
--
ALTER TABLE `portfolio_acao_social`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1516;
--
-- AUTO_INCREMENT for table `portfolio_capas`
--
ALTER TABLE `portfolio_capas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4267;
--
-- AUTO_INCREMENT for table `portfolio_categorias`
--
ALTER TABLE `portfolio_categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `portfolio_consultoria_moda`
--
ALTER TABLE `portfolio_consultoria_moda`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `portfolio_creditos`
--
ALTER TABLE `portfolio_creditos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1693;
--
-- AUTO_INCREMENT for table `portfolio_desfiles`
--
ALTER TABLE `portfolio_desfiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=992;
--
-- AUTO_INCREMENT for table `portfolio_espetaculos`
--
ALTER TABLE `portfolio_espetaculos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2592;
--
-- AUTO_INCREMENT for table `portfolio_extras`
--
ALTER TABLE `portfolio_extras`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1153;
--
-- AUTO_INCREMENT for table `portfolio_fotos`
--
ALTER TABLE `portfolio_fotos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16446;
--
-- AUTO_INCREMENT for table `portfolio_idiomas`
--
ALTER TABLE `portfolio_idiomas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `portfolio_idiomas_personalidades`
--
ALTER TABLE `portfolio_idiomas_personalidades`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1728;
--
-- AUTO_INCREMENT for table `portfolio_palestras`
--
ALTER TABLE `portfolio_palestras`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=509;
--
-- AUTO_INCREMENT for table `portfolio_paralaxe`
--
ALTER TABLE `portfolio_paralaxe`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2687;
--
-- AUTO_INCREMENT for table `portfolio_personalidades`
--
ALTER TABLE `portfolio_personalidades`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=214;
--
-- AUTO_INCREMENT for table `portfolio_premios`
--
ALTER TABLE `portfolio_premios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=694;
--
-- AUTO_INCREMENT for table `portfolio_publicacoes`
--
ALTER TABLE `portfolio_publicacoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3224;
--
-- AUTO_INCREMENT for table `portfolio_publicidade`
--
ALTER TABLE `portfolio_publicidade`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5371;
--
-- AUTO_INCREMENT for table `portfolio_redes_sociais`
--
ALTER TABLE `portfolio_redes_sociais`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3723;
--
-- AUTO_INCREMENT for table `portfolio_red_carpet`
--
ALTER TABLE `portfolio_red_carpet`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2868;
--
-- AUTO_INCREMENT for table `portfolio_subcategorias`
--
ALTER TABLE `portfolio_subcategorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `portfolio_videos`
--
ALTER TABLE `portfolio_videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=680;
--
-- AUTO_INCREMENT for table `portfolio_workshops`
--
ALTER TABLE `portfolio_workshops`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `publicacoes`
--
ALTER TABLE `publicacoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `publicacoes_imagens`
--
ALTER TABLE `publicacoes_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1826;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `acao_social_imagens`
--
ALTER TABLE `acao_social_imagens`
  ADD CONSTRAINT `acao_social_imagens_acao_social_id_foreign` FOREIGN KEY (`acao_social_id`) REFERENCES `acao_social` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `licenciamento_imagens`
--
ALTER TABLE `licenciamento_imagens`
  ADD CONSTRAINT `licenciamento_imagens_licenciamento_id_foreign` FOREIGN KEY (`licenciamento_id`) REFERENCES `licenciamento` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `portfolio_acao_social`
--
ALTER TABLE `portfolio_acao_social`
  ADD CONSTRAINT `portfolio_acao_social_portfolio_personalidades_id_foreign` FOREIGN KEY (`portfolio_personalidades_id`) REFERENCES `portfolio_personalidades` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `portfolio_capas`
--
ALTER TABLE `portfolio_capas`
  ADD CONSTRAINT `portfolio_capas_portfolio_personalidades_id_foreign` FOREIGN KEY (`portfolio_personalidades_id`) REFERENCES `portfolio_personalidades` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `portfolio_consultoria_moda`
--
ALTER TABLE `portfolio_consultoria_moda`
  ADD CONSTRAINT `portfolio_consultoria_moda_portfolio_personalidades_id_foreign` FOREIGN KEY (`portfolio_personalidades_id`) REFERENCES `portfolio_personalidades` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `portfolio_creditos`
--
ALTER TABLE `portfolio_creditos`
  ADD CONSTRAINT `portfolio_creditos_portfolio_personalidades_id_foreign` FOREIGN KEY (`portfolio_personalidades_id`) REFERENCES `portfolio_personalidades` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `portfolio_desfiles`
--
ALTER TABLE `portfolio_desfiles`
  ADD CONSTRAINT `portfolio_desfiles_portfolio_personalidades_id_foreign` FOREIGN KEY (`portfolio_personalidades_id`) REFERENCES `portfolio_personalidades` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `portfolio_espetaculos`
--
ALTER TABLE `portfolio_espetaculos`
  ADD CONSTRAINT `portfolio_espetaculos_portfolio_personalidades_id_foreign` FOREIGN KEY (`portfolio_personalidades_id`) REFERENCES `portfolio_personalidades` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `portfolio_extras`
--
ALTER TABLE `portfolio_extras`
  ADD CONSTRAINT `portfolio_extras_portfolio_personalidades_id_foreign` FOREIGN KEY (`portfolio_personalidades_id`) REFERENCES `portfolio_personalidades` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `portfolio_fotos`
--
ALTER TABLE `portfolio_fotos`
  ADD CONSTRAINT `portfolio_fotos_portfolio_personalidades_id_foreign` FOREIGN KEY (`portfolio_personalidades_id`) REFERENCES `portfolio_personalidades` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `portfolio_idiomas_personalidades`
--
ALTER TABLE `portfolio_idiomas_personalidades`
  ADD CONSTRAINT `portfolio_idiomas_personalidades_idioma_foreign` FOREIGN KEY (`idioma`) REFERENCES `portfolio_idiomas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `portfolio_idiomas_personalidades_personalidade_foreign` FOREIGN KEY (`personalidade`) REFERENCES `portfolio_personalidades` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `portfolio_palestras`
--
ALTER TABLE `portfolio_palestras`
  ADD CONSTRAINT `portfolio_palestras_portfolio_personalidades_id_foreign` FOREIGN KEY (`portfolio_personalidades_id`) REFERENCES `portfolio_personalidades` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `portfolio_paralaxe`
--
ALTER TABLE `portfolio_paralaxe`
  ADD CONSTRAINT `portfolio_paralaxe_portfolio_personalidades_id_foreign` FOREIGN KEY (`portfolio_personalidades_id`) REFERENCES `portfolio_personalidades` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `portfolio_personalidades`
--
ALTER TABLE `portfolio_personalidades`
  ADD CONSTRAINT `portfolio_personalidades_portfolio_categorias_id_foreign` FOREIGN KEY (`portfolio_categorias_id`) REFERENCES `portfolio_categorias` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `portfolio_personalidades_portfolio_subcategorias_id_foreign` FOREIGN KEY (`portfolio_subcategorias_id`) REFERENCES `portfolio_subcategorias` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `portfolio_premios`
--
ALTER TABLE `portfolio_premios`
  ADD CONSTRAINT `portfolio_premios_portfolio_personalidades_id_foreign` FOREIGN KEY (`portfolio_personalidades_id`) REFERENCES `portfolio_personalidades` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `portfolio_publicacoes`
--
ALTER TABLE `portfolio_publicacoes`
  ADD CONSTRAINT `portfolio_publicacoes_portfolio_personalidades_id_foreign` FOREIGN KEY (`portfolio_personalidades_id`) REFERENCES `portfolio_personalidades` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `portfolio_publicidade`
--
ALTER TABLE `portfolio_publicidade`
  ADD CONSTRAINT `portfolio_publicidade_portfolio_personalidades_id_foreign` FOREIGN KEY (`portfolio_personalidades_id`) REFERENCES `portfolio_personalidades` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `portfolio_redes_sociais`
--
ALTER TABLE `portfolio_redes_sociais`
  ADD CONSTRAINT `portfolio_redes_sociais_portfolio_personalidades_id_foreign` FOREIGN KEY (`portfolio_personalidades_id`) REFERENCES `portfolio_personalidades` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `portfolio_red_carpet`
--
ALTER TABLE `portfolio_red_carpet`
  ADD CONSTRAINT `portfolio_red_carpet_portfolio_personalidades_id_foreign` FOREIGN KEY (`portfolio_personalidades_id`) REFERENCES `portfolio_personalidades` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `portfolio_subcategorias`
--
ALTER TABLE `portfolio_subcategorias`
  ADD CONSTRAINT `portfolio_subcategorias_portfolio_categorias_id_foreign` FOREIGN KEY (`portfolio_categorias_id`) REFERENCES `portfolio_categorias` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `portfolio_videos`
--
ALTER TABLE `portfolio_videos`
  ADD CONSTRAINT `portfolio_videos_portfolio_personalidades_id_foreign` FOREIGN KEY (`portfolio_personalidades_id`) REFERENCES `portfolio_personalidades` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `portfolio_workshops`
--
ALTER TABLE `portfolio_workshops`
  ADD CONSTRAINT `portfolio_workshops_portfolio_personalidades_id_foreign` FOREIGN KEY (`portfolio_personalidades_id`) REFERENCES `portfolio_personalidades` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `publicacoes_imagens`
--
ALTER TABLE `publicacoes_imagens`
  ADD CONSTRAINT `publicacoes_imagens_publicacoes_id_foreign` FOREIGN KEY (`publicacoes_id`) REFERENCES `publicacoes` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
