<?php

namespace App\Providers;

use Input;
use Illuminate\Support\ServiceProvider;

use App\Models\PortfolioCategorias;
use App\Models\PortfolioPersonalidades;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->share('listaCategorias', PortfolioCategorias::ordenado()->get());


        $todomundo = [];

        $todomundo_qry = PortfolioPersonalidades::publicados()->orderBy('nome', 'asc')->get();

        foreach ($todomundo_qry as $key => $value) {
          if(!isset($todomundo['link_principal'][$value->nome])){
            $todomundo['link_principal'][$value->nome] = [
              'nome' => $value->nome,
              'slug' => $value->slug,
              'categoria_titulo' => $value->categoria->titulo,
            ];
            $todomundo['links'][$value->nome][] = [
              'nome' => $value->nome,
              'slug' => $value->slug,
              'categoria_titulo' => $value->categoria->titulo,
            ];
          }else{
            $todomundo['links'][$value->nome][] = [
              'nome' => $value->nome,
              'slug' => $value->slug,
              'categoria_titulo' => $value->categoria->titulo,
            ];
          }
        }

        view()->share('todomundo', $todomundo);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('path.public', function() {
            return base_path().'/../public_html';
        });
    }
}
