<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LicenciamentoImagens extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'licenciamento_imagens';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['id'];

    public function texto()
    {
        return $this->belongsTo('App\Models\Licenciamento', 'licenciamento_id')->orderBy('ordem', 'asc');
    }
}
