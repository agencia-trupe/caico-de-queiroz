<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PortfolioSubcategorias extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'portfolio_subcategorias';
    protected $appends = ['classe_menu', 'titulo_categoria'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $visible = ['id', 'titulo', 'slug', 'portfolio_categorias_id', 'classe_menu', 'titulo_categoria'];

    public function getClasseMenuAttribute(){
      return (isset($this->categoria)) ? $this->categoria->classe_menu : '';
    }

    public function getTituloCategoriaAttribute(){
      return (isset($this->categoria)) ? $this->categoria->titulo : '';
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\PortfolioCategorias', 'portfolio_categorias_id');
    }

    public function personalidades()
    {
        return $this->hasMany('App\Models\PortfolioPersonalidades', 'portfolio_subcategorias_id')->orderBy('nome', 'asc');
    }

    public function scopeOrdenado($query)
    {
        return $query->orderBy('ordem', 'asc');
    }
}
