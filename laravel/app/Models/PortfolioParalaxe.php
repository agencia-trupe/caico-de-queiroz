<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PortfolioParalaxe extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'portfolio_paralaxe';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['id'];

    public function personalidade()
    {
        return $this->belongsTo('App\Models\PortfolioPersonalidades', 'portfolio_personalidades_id');
    }
}
