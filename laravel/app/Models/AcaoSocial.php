<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AcaoSocial extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'acao_social';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['id'];

    public function imagens()
    {
        return $this->hasMany('App\Models\AcaoSocialImagens', 'acao_social_id')->orderBy('ordem', 'asc');
    }
}
