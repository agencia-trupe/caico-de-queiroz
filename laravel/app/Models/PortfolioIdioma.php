<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PortfolioIdioma extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'portfolio_idiomas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['id'];

    public function getIdiomasArrayAttribute(){
      $idiomas = $this->idiomas->pluck('id');
    }

    public function scopeOrdenado($query)
    {
      return $query->orderBy('titulo', 'asc');
    }

    public function personalidade()
    {
      return $this->belongsToMany('App\Models\PortfolioPersonalidades', 'portfolio_idiomas_personalidades', 'personalidade', 'idioma');      
    }
}
