<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PortfolioPersonalidades extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'portfolio_personalidades';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function scopeSlug($query, $slug = '')
    {
        return ($slug) ? $query->where('slug', '=', $slug) : $query;
    }

    public function scopeOrdemAlfabetica($query)
    {
        return $query->orderBy('nome', 'asc');
    }

    public function scopeAleatorio($query)
    {
        return $query->orderByRaw("RAND()");
    }

    public function scopePublicados($query)
    {
      return $query->where('publicar', '=', 1);
    }

    public function scopeComThumb($query)
    {
      return $query->where('thumb_home', '!=', '');
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\PortfolioCategorias', 'portfolio_categorias_id');
    }

    public function subcategoria()
    {
        return $this->belongsTo('App\Models\PortfolioSubcategorias', 'portfolio_subcategorias_id');
    }

    // MÓDULOS

    public function paralaxe()
    {
        return $this->hasOne('App\Models\PortfolioParalaxe', 'portfolio_personalidades_id');
    }

    public function fotos()
    {
        return $this->hasMany('App\Models\PortfolioFotos', 'portfolio_personalidades_id')->orderBy('ordem', 'asc');
    }

    public function personagens()
    {
        return $this->hasMany('App\Models\PortfolioPersonagens', 'portfolio_personalidades_id')->orderBy('ordem', 'asc');
    }

    public function capas()
    {
        return $this->hasMany('App\Models\PortfolioCapas', 'portfolio_personalidades_id')->orderBy('ordem', 'asc');
    }

    public function publicidade()
    {
        return $this->hasMany('App\Models\PortfolioPublicidade', 'portfolio_personalidades_id')->orderBy('ordem', 'asc');
    }

    public function desfiles()
    {
        return $this->hasMany('App\Models\PortfolioDesfiles', 'portfolio_personalidades_id')->orderBy('ordem', 'asc');
    }

    public function videos()
    {
        return $this->hasMany('App\Models\PortfolioVideos', 'portfolio_personalidades_id')->orderBy('ordem', 'asc');
    }

    public function acaosocial()
    {
        return $this->hasMany('App\Models\PortfolioAcaoSocial', 'portfolio_personalidades_id')->orderBy('ordem', 'asc');
    }

    public function editorialdemoda()
    {
        return $this->hasMany('App\Models\PortfolioEditorialDeModa', 'portfolio_personalidades_id')->orderBy('ordem', 'asc');
    }

    public function personalstylist()
    {
        return $this->hasMany('App\Models\PortfolioPersonalStylist', 'portfolio_personalidades_id')->orderBy('ordem', 'asc');
    }

    public function redessociais()
    {
        return $this->hasOne('App\Models\PortfolioRedesSociais', 'portfolio_personalidades_id');
    }

    public function palestras()
    {
        return $this->hasOne('App\Models\PortfolioPalestras', 'portfolio_personalidades_id');
    }

    public function consultoriamoda()
    {
        return $this->hasOne('App\Models\PortfolioConsultoriaModa', 'portfolio_personalidades_id');
    }

    public function publicacoes()
    {
        return $this->hasMany('App\Models\PortfolioPublicacoes', 'portfolio_personalidades_id')->orderBy('ordem', 'asc');
    }

    public function extras()
    {
        return $this->hasOne('App\Models\PortfolioExtras', 'portfolio_personalidades_id');
    }

    public function redcarpet()
    {
        return $this->hasMany('App\Models\PortfolioRedCarpet', 'portfolio_personalidades_id')->orderBy('ordem', 'asc');
    }

    public function espetaculos()
    {
        return $this->hasMany('App\Models\PortfolioEspetaculos', 'portfolio_personalidades_id')->orderBy('ordem', 'asc');
    }

    public function workshops()
    {
        return $this->hasMany('App\Models\PortfolioWorkshops', 'portfolio_personalidades_id')->orderBy('ordem', 'asc');
    }

    public function creditos()
    {
        return $this->hasOne('App\Models\PortfolioCreditos', 'portfolio_personalidades_id');
    }

    public function idiomas()
    {
      return $this->belongsToMany('App\Models\PortfolioIdioma', 'portfolio_idiomas_personalidades', 'personalidade', 'idioma');
    }

    public function premios()
    {
      return $this->hasMany('App\Models\PortfolioPremios', 'portfolio_personalidades_id')->orderBy('ordem', 'asc');
    }
}
