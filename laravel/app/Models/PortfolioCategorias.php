<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PortfolioCategorias extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'portfolio_categorias';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function subcategorias()
    {
        return $this->hasMany('App\Models\PortfolioSubcategorias', 'portfolio_categorias_id')->orderBy('ordem', 'asc');
    }

    public function personalidades()
    {
        return $this->hasMany('App\Models\PortfolioPersonalidades', 'portfolio_categorias_id')->orderBy('nome', 'asc');
    }

    public function scopeOrdenado($query)
    {
        return $query->orderBy('ordem', 'asc');
    }
}
