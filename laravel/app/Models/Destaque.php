<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Destaque extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'destaques';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['id'];

    public function imagens()
    {
        return $this->hasMany('App\Models\DestaquesBanners', 'destaque_id')->orderBy('ordem', 'asc');
    }
}
