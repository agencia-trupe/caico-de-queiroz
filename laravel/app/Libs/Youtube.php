<?php

namespace App\Libs;

use \Image, \Input;

/*
*	Classe para manipulação de url do YOUTUBE
*	@author Bruno Monteiro
*	@since 17/08/2015
*/

class Youtube{

	private $url;
	private $id;

	private $oembed_endpoint = 'http://www.youtube.com/oembed';
	private $thumb_path = 'assets/img/portfolio/videos/';
	private $servico = 'youtube';

	public function __construct($url)
	{
		$this->url = $url;
		$this->id = $this->setId();
	}

	public function getUrl(){ return $this->url; }
	public function getId(){ return $this->id; }
	public function getServico(){ return $this->servico; }

	public function getEmbed($largura = null, $altura = null)
	{
    if (!$this->url) { return ''; }
		/*
		*  default_ratio = 16/9;
		*/
		$dimensoes_str = "";

		if($largura != null || $altura != null){

			if($largura != null && $altura === null)
				$altura = (9 * $largura)/16;

			if($largura === null && $altura != null)
				$largura = (16 * $altura)/9;

			$dimensoes_str = " width='".$largura."' height='".$altura."'";
		}

		return "<iframe src='http://www.youtube.com/embed/{$this->id}?autoplay=1' {$dimensoes_str} frameborder='0' allowfullscreen></iframe>";
	}

	public function getThumbnail($tamanho = 'large', $salvar = true) {
		// @tamanho: 'small', 'medium', 'large'

		$tamanhos = ['small' => 'default.jpg', 'medium' => 'mqdefault.jpg', 'large' => 'sddefault.jpg', 'default' => '0.jpg'];

		if(!in_array($tamanho, $tamanhos))
			$tamanho = 'large';

		$url_thumb = "http://img.youtube.com/vi/{$this->id}/{$tamanhos[$tamanho]}";
		$url_thumb_alternativa = "http://img.youtube.com/vi/{$this->id}/{$tamanhos['default']}";

		if($url_thumb === false)
			return '';

		$filename = $this->id.'.jpg';

		if($salvar){

			if(!file_exists(public_path($this->thumb_path)))
				mkdir(public_path($this->thumb_path, 0777));

			if(!@copy($url_thumb, $this->thumb_path.'/'.$filename)){
				@copy($url_thumb_alternativa, $this->thumb_path.'/'.$filename);
			}

		}

		return $filename;
	}

	public function setId()
	{
		if(strpos($this->url, 'youtu.be') !== FALSE){
			preg_match('@youtu.be/(.*)$@i', $this->url, $url_parts);

			$video_id = isset($url_parts[1]) && $url_parts[1] ? $url_parts[1] : 0;

			if(strpos($video_id, '?') !== false){
				$video_id_explode = explode('?', $video_id);

				$video_id = isset($video_id_explode[0]) && $video_id_explode[0] ? $video_id_explode[0] : 0;
			}

	        return $video_id;
		}else{
			parse_str( parse_url( $this->url, PHP_URL_QUERY ), $url_parts );
	    	return isset($url_parts['v']) && $url_parts['v'] ? $url_parts['v'] : 0;
		}

	}
}