<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Publicacoes;

class PublicacoesController extends Controller
{
    /**
     * Página /publicacoes
     *
     * @return Response
     */
    public function index()
    {
    	$publicacoes = Publicacoes::first();

        return view('site.publicacoes.index')->with(compact('publicacoes'));
    }

}
