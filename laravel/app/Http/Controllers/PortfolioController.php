<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\PortfolioPersonalidades;

class PortfolioController extends Controller
{
  /**
   * Página /portfolio/entrada/
   *
   * @return Response
   */
  public function entrada($slug = '')
  {
    if(!$slug) return redirect('/home');

  	$personalidade  = PortfolioPersonalidades::with('paralaxe')->slug($slug)->first();

    if(!$personalidade) return redirect('/home');
    if($personalidade->publicar != 1) return redirect('/home');

    view()->share('marcarSlugCategoria', $personalidade->categoria->slug);
    view()->share('marcarSlugSubcategoria', isset($personalidade->subcategoria->slug) ? $personalidade->subcategoria->slug : '');
    view()->share('marcarSlugPersonalidade', $slug);

    return view('site.portfolio.index')->with(compact('personalidade'));
  }

  /**
   * Página /portfolio/detalhe/
   *
   * @return Response
   */
  public function detalhe($slug = '')
  {
    if(!$slug) return redirect('/home');

    $personalidade  = PortfolioPersonalidades::with(
      'categoria',
      'subcategoria',
      'fotos',
      'capas',
      'publicidade',
      'desfiles',
      'videos',
      'acaosocial',
      'redessociais',
      'palestras',
      'consultoriamoda',
      'publicacoes',
      'extras'
    )->slug($slug)
     ->first();

    if(!$personalidade) return redirect('/home');
    if($personalidade->publicar != 1) return redirect('/home');
    
    view()->share('marcarSlugCategoria', $personalidade->categoria->slug);
    view()->share('marcarSlugSubcategoria', isset($personalidade->subcategoria->slug) ? $personalidade->subcategoria->slug : '');
    view()->share('marcarSlugPersonalidade', $slug);

    return view('site.portfolio.detalhe')->with(compact('personalidade'));
  }

  public function release($slug = '')
  {
    if(!$slug) return redirect('/home');

    $personalidade  = PortfolioPersonalidades::with('paralaxe')->slug($slug)->first();

    if(!$personalidade) return redirect('/home');

    return response()->download('assets/releases/'.$personalidade->arquivo_release);
  }
}
