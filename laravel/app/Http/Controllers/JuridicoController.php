<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Juridico;

class JuridicoController extends Controller
{
    /**
     * Página /juridico
     *
     * @return Response
     */
    public function index()
    {
    	$juridico = Juridico::first();

        return view('site.juridico.index')->with(compact('juridico'));
    }

}
