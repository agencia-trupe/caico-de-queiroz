<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\About;
use App\Models\Marca;
use App\Models\Banner;
use App\Models\PortfolioPersonalidades;
use App\Models\PortfolioCategorias;
use App\Models\PortfolioSubcategorias;

class HomeController extends Controller
{
    /**
     * Página /home
     *
     * @return Response
     */
    public function index(Request $request)
    {
    	$about = About::first();

      $marca = Marca::first();
      $banners = Banner::ordenado()->get();
      $mosaico = $this->gerarMosaico();

      return view('site.home.novo')->with(compact('about', 'marca', 'banners', 'mosaico'))->with('classe', 'fundo-preto');

    }

    public function gerarMosaico(){

      // Categorias e subcategorias foram removidas do mosaico
      // a pedido do Cliente (deixando só personalidades)

      //$listaSecoes = PortfolioCategorias::ordenado()->whereNotIn('id', [9, 11])->get()->toArray();
      $listaSecoes = [];

      //$listaSubSecoes = PortfolioSubcategorias::with('categoria')->ordenado()->whereNotIn('id', [13, 18, 22])->get()->toArray();
      $listaSubSecoes = [];

      //$secoes_merge = array_merge($listaSecoes, $listaSubSecoes);

      // Pegar um número aleatório (entre 2 e 5) de seções
      // $secoes_keys = array_rand($secoes_merge, rand(2,4));
      // foreach($secoes_keys as $chave){
      //   $secoes[] = $secoes_merge[$chave];
      // }

      // pegar (16 - nro_secoes) registros de personalidades ordenados
      // aleatoriamente que tenham a thumb quadrada
      $personalidades = PortfolioPersonalidades::comThumb()
                                                ->where('publicar', 1)
                                                ->aleatorio()
                                                //->limit(16 - count($secoes))
                                                ->limit(16)
                                                ->whereHas('categoria', function($query){
                                                  $query->where('publicar', 1);
                                                })
                                                ->get()
                                                ->toArray();



      // Inserir o array de seções no de personalidades em pontos aleatórios
      //$retorno = array_merge($personalidades, $secoes);
      $retorno = $personalidades;
      shuffle($retorno);

      return $retorno;
    }

}
