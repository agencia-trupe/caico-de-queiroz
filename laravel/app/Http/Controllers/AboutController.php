<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\About;

class AboutController extends Controller
{
    /**
     * Página /about
     *
     * @return Response
     */
    public function index()
    {
    	$about = About::first();

        return view('site.about.index')->with(compact('about'));
    }

}
