<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use View;
use Validator;

use App\Libs\Thumbs;
use App\Libs\Youtube;
use App\Libs\Vimeo;

use App\Models\PortfolioPersonalidades;
use App\Models\PortfolioCategorias;
use App\Models\PortfolioSubcategorias;
use App\Models\PortfolioParalaxe;
use App\Models\PortfolioFotos;
use App\Models\PortfolioPersonagens;
use App\Models\PortfolioCapas;
use App\Models\PortfolioPublicidade;
use App\Models\PortfolioDesfiles;
use App\Models\PortfolioVideos;
use App\Models\PortfolioAcaoSocial;
use App\Models\PortfolioEditorialDeModa;
use App\Models\PortfolioPersonalStylist;
use App\Models\PortfolioRedesSociais;
use App\Models\PortfolioPalestras;
use App\Models\PortfolioConsultoriaModa;
use App\Models\PortfolioPublicacoes;
use App\Models\PortfolioExtras;
use App\Models\PortfolioRedCarpet;
use App\Models\PortfolioEspetaculos;
use App\Models\PortfolioWorkshops;
use App\Models\PortfolioCreditos;
use App\Models\PortfolioIdioma;
use App\Models\PortfolioPremios;

class PortfolioController extends Controller
{

    var $listaCategorias;
    var $listaSubcategorias;
    var $listaIdiomas;

    public function __construct()
    {
        $this->listaCategorias = PortfolioCategorias::ordenado()->get();
        $this->listaSubcategorias = PortfolioSubcategorias::ordenado()->get();
        $this->listaIdiomas = PortfolioIdioma::ordenado()->get();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
      $registros = PortfolioPersonalidades::ordemAlfabetica()->get();

      return view('painel.portfolio.index')->with(compact('registros'))
                                           ->with(['listaCategorias' => $this->listaCategorias])
                                           ->with(['listaSubcategorias' => $this->listaSubcategorias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
      return view('painel.portfolio.create')->with(['listaCategorias' => $this->listaCategorias])
                                            ->with(['listaSubcategorias' => $this->listaSubcategorias])
                                            ->with(['listaIdiomas' => $this->listaIdiomas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
          'portfolio_categorias_id' => 'required|exists:portfolio_categorias,id',
          'nome' => 'required|min:3',
          'cor_nome' => 'required|in:000,FFF',
          'imagem_entrada' => 'required|image',
          'thumb_home' => 'required|image',
        ]);

        $object = new PortfolioPersonalidades;

        $object->portfolio_categorias_id = $request->input('portfolio_categorias_id');
        $object->portfolio_subcategorias_id = $request->has('portfolio_subcategorias_id') ? $request->input('portfolio_subcategorias_id') : null;
        $object->nome = $request->input('nome');

        $slug = str_slug($object->nome);
        $slug_append = 1;
        $check = PortfolioPersonalidades::where('slug', $slug)->first();
        if(sizeof($check) > 0){
          while(sizeof($check) > 0){
            $slug = str_slug($object->nome) . '-' . $slug_append;
            $slug_append++;
            $check = PortfolioPersonalidades::where('slug', $slug)->first();
          }
        }
        $object->slug = $slug;

        $object->cor_nome = $request->input('cor_nome');
        $object->link_site = prep_url($request->input('link_site'));

        $object->publicar = ($request->input('publicar') == 'publicar') ? 1 : 0;

        $imagem_entrada = Thumbs::make('imagem_entrada', null, null, 'portfolio/entrada/');
        if($imagem_entrada) $object->imagem_entrada = $imagem_entrada;

        $thumb_home = Thumbs::make('thumb_home', 400, 400, 'portfolio/thumbs-home/');
        if($thumb_home) $object->thumb_home = $thumb_home;

        if($request->file('arquivo_release')){
            $arquivo_release = $request->file('arquivo_release');
            $filename = 'release_'.$object->slug.'.'.$arquivo_release->getClientOriginalExtension();
            if($arquivo_release->move('assets/releases/', $filename))
                $object->arquivo_release = $filename;
        }

        try {

            $object->save();

            $this->salvarModulos($object, $request, $object->id);

            $request->session()->flash('sucesso', 'Cadastro inserido com sucesso.');

            return redirect()->route('painel.portfolio.index');

        } catch (\Exception $e) {

            $request->flash();

            return back()->withErrors(array('Erro ao inserir Cadastro! ('.$e->getMessage().')'));

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $registro = PortfolioPersonalidades::find($id);
        return view('painel.portfolio.edit')->with(compact('registro'))
                                              ->with(['listaCategorias' => $this->listaCategorias])
                                              ->with(['listaSubcategorias' => $this->listaSubcategorias])
                                              ->with(['listaIdiomas' => $this->listaIdiomas]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'portfolio_categorias_id' => 'required|exists:portfolio_categorias,id',
            'nome' => 'required|min:3',
            'cor_nome' => 'required|in:000,FFF',
            'imagem_entrada' => 'sometimes|image',
            'thumb_home' => 'sometimes|image',
        ]);

        $object = PortfolioPersonalidades::find($id);

        $object->portfolio_categorias_id = $request->input('portfolio_categorias_id');
        $object->portfolio_subcategorias_id = $request->has('portfolio_subcategorias_id') ? $request->input('portfolio_subcategorias_id') : null;
        $object->nome = $request->input('nome');

        $slug = str_slug($object->nome);
        $slug_append = 1;
        $check = PortfolioPersonalidades::where('slug', $slug)->where('id', '!=', $id)->first();
        if(sizeof($check) > 0){
            while(sizeof($check) > 0){
                $slug = $slug . '-' . $slug_append;
                $slug_append++;
                $check = PortfolioPersonalidades::where('slug', $slug)->where('id', '!=', $id)->first();
            }
            $object->slug = $slug;
        }else{
            $object->slug = $slug;
        }

        $object->cor_nome = $request->input('cor_nome');
        $object->link_site = prep_url($request->input('link_site'));

        $object->publicar = ($request->input('publicar') == 'publicar') ? 1 : 0;

        $imagem_entrada = Thumbs::make('imagem_entrada', null, null, 'portfolio/entrada/');
        if($imagem_entrada) $object->imagem_entrada = $imagem_entrada;

        $thumb_home = Thumbs::make('thumb_home', 400, 400, 'portfolio/thumbs-home/');
        if($thumb_home) $object->thumb_home = $thumb_home;

        $remover_thumb_home = $request->remover_thumb_home;
        if($remover_thumb_home && $remover_thumb_home == 1) $object->thumb_home = '';

        if($request->file('arquivo_release')){
            $arquivo_release = $request->file('arquivo_release');
            $filename = 'release_'.$object->slug.'.'.$arquivo_release->getClientOriginalExtension();
            if($arquivo_release->move('assets/releases/', $filename))
                $object->arquivo_release = $filename;
        }


        try {

            $object->save();

            $this->salvarModulos($object, $request, $object->id, true);

            $request->session()->flash('sucesso', 'Cadastro alterado com sucesso.');

            return redirect()->route('painel.portfolio.index');

        } catch (\Exception $e) {

            $request->flash();

            return back()->withErrors(array('Erro ao alterar Cadastro! ('.$e->getMessage().')'));

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        $object = PortfolioPersonalidades::find($id);
        $object->delete();

        $request->session()->flash('sucesso', 'Cadastro removido com sucesso.');

        return redirect()->route('painel.portfolio.index');
    }

    /**
     * Gerencia os módulos do cadastro
     *
     * @param  Request  $request
     * @param  int  $id
     * @param  bool $edicao
     * @return Response
     */
    private function salvarModulos($object, $request, $id, $edicao = false){

        $has_paralaxe = $request->has('has_paralaxe') ? $request->input('has_paralaxe') : 0;
        $has_fotos = $request->has('has_fotos') ? $request->input('has_fotos') : 0;
        $has_personagens = $request->has('has_personagens') ? $request->input('has_personagens') : 0;
        $has_capas = $request->has('has_capas') ? $request->input('has_capas') : 0;
        $has_publicidade = $request->has('has_publicidade') ? $request->input('has_publicidade') : 0;
        $has_desfiles = $request->has('has_desfiles') ? $request->input('has_desfiles') : 0;
        $has_videos = $request->has('has_videos') ? $request->input('has_videos') : 0;
        $has_acaosocial = $request->has('has_acaosocial') ? $request->input('has_acaosocial') : 0;
        $has_editorial_de_moda = $request->has('has_editorial_de_moda') ? $request->input('has_editorial_de_moda') : 0;
        $has_personal_stylist = $request->has('has_personal_stylist') ? $request->input('has_personal_stylist') : 0;
        $has_midiassociais = $request->has('has_midiassociais') ? $request->input('has_midiassociais') : 0;
        $has_palestras = $request->has('has_palestras') ? $request->input('has_palestras') : 0;
        $has_consultoria = $request->has('has_consultoria') ? $request->input('has_consultoria') : 0;
        $has_publicacoes = $request->has('has_publicacoes') ? $request->input('has_publicacoes') : 0;
        $has_extras = $request->has('has_extras') ? $request->input('has_extras') : 0;
        $has_redcarpet = $request->has('has_redcarpet') ? $request->input('has_redcarpet') : 0;
        $has_espetaculos = $request->has('has_espetaculos') ? $request->input('has_espetaculos') : 0;
        $has_workshops = $request->has('has_workshops') ? $request->input('has_workshops') : 0;
        $has_creditos = $request->has('has_creditos') ? $request->input('has_creditos') : 0;
        $has_idiomas = $request->has('has_idiomas') ? $request->input('has_idiomas') : 0;
        $has_premios = $request->has('has_premios') ? $request->input('has_premios') : 0;

        if($has_paralaxe  == 1)
            $this->imagensParalaxe($request, $id);
        elseif($edicao)
            $object->paralaxe()->delete();

        if($has_fotos  == 1)
            $this->imagensFotos($request, $id);
        elseif($edicao){
            foreach($object->fotos as $foto)
                $foto->delete();
        }

        if($has_personagens  == 1)
            $this->imagensPersonagens($request, $id);
        elseif($edicao){
            foreach($object->personagens as $personagem)
                $personagem->delete();
        }

        if($has_capas  == 1)
            $this->imagensCapas($request, $id);
        elseif($edicao){
            foreach($object->capas as $capa)
                $capa->delete();
        }

        if($has_publicidade  == 1)
            $this->imagensPublicidade($request, $id);
        elseif($edicao){
            foreach($object->publicidade as $publi)
                $publi->delete();
        }

        if($has_desfiles  == 1)
            $this->imagensDesfiles($request, $id);
        elseif($edicao){
            foreach($object->desfiles as $desfile)
                $desfile->delete();
        }

        if($has_videos  == 1)
            $this->armazenaVideos($request, $id);
        elseif($edicao){
            foreach($object->videos as $video)
                $video->delete();
        }

        if($has_acaosocial  == 1)
            $this->armazenaAcaoSocial($request, $id);
        elseif($edicao){
            foreach($object->acaosocial as $acao)
                $acao->delete();
        }

        if($has_editorial_de_moda  == 1)
            $this->imagensEditorialDeModa($request, $id);
        elseif($edicao){
            foreach($object->editorialdemoda as $editorial)
                $editorial->delete();
        }

        if($has_personal_stylist  == 1)
            $this->imagensPersonalStylist($request, $id);
        elseif($edicao){
            foreach($object->personalstylist as $personal)
                $personal->delete();
        }

        if($has_midiassociais  == 1)
            $this->armazenaMidiasSociais($request, $id);
        elseif($edicao)
            $object->redessociais()->delete();

        if($has_palestras  == 1)
            $this->armazenaPalestras($request, $id);
        elseif($edicao)
            $object->palestras()->delete();

        if($has_consultoria  == 1)
            $this->armazenaConsultoria($request, $id);
        elseif($edicao)
            $object->consultoriamoda()->delete();

        if($has_publicacoes  == 1)
            $this->armazenaPublicacoes($request, $id);
        elseif($edicao){
            foreach($object->publicacoes as $publi)
                $publi->delete();
        }

        if($has_extras  == 1)
            $this->armazenaExtras($request, $id);
        elseif($edicao)
            $object->extras()->delete();

        if($has_redcarpet  == 1)
            $this->imagensRedCarpet($request, $id);
        elseif($edicao){
            foreach($object->redcarpet as $redcarpet)
                $redcarpet->delete();
        }

        if($has_espetaculos  == 1)
            $this->armazenaEspetaculos($request, $id);
        elseif($edicao){
            foreach($object->espetaculos as $espet)
                $espet->delete();
        }

        if($has_workshops  == 1)
            $this->armazenaWorkshops($request, $id);
        elseif($edicao){
            foreach($object->workshops as $work)
                $work->delete();
        }

        if($has_creditos  == 1)
            $this->armazenaCreditos($request, $id);
        elseif($edicao)
            $object->creditos()->delete();

        if($has_idiomas  == 1)
          $this->armazenaIdiomas($request, $object);
        elseif($edicao)
          $object->idiomas()->detach();

        if($has_premios  == 1)
            $this->armazenaPremios($request, $id);
        elseif($edicao){
            foreach($object->premios as $premio)
                $premio->delete();
        }
    }

    private function imagensParalaxe(Request $request, $id)
    {
        $imagens = $request->get('imagem_paralaxe');

        if($imagens && is_array($imagens)){

            PortfolioParalaxe::where('portfolio_personalidades_id', '=', $id)->delete();

            $obj = new PortfolioParalaxe;
            $obj->portfolio_personalidades_id = $id;

            foreach ($imagens as $key => $value)
                $obj->{ 'imagem_' . ($key + 1) } = $value;

            $obj->save();
        }
    }

    private function imagensFotos(Request $request, $id)
    {
        $imagens = $request->get('imagem_fotos');

        PortfolioFotos::where('portfolio_personalidades_id', '=', $id)->delete();

        if($imagens && is_array($imagens)){

            foreach ($imagens as $key => $value){
                $obj = new PortfolioFotos;
                $obj->portfolio_personalidades_id = $id;
                $obj->imagem = $value;
                $obj->ordem = $key;
                $obj->save();
            }
        }
    }

    private function imagensPersonagens(Request $request, $id)
    {
        $imagens = $request->get('imagem_personagens');

        PortfolioPersonagens::where('portfolio_personalidades_id', '=', $id)->delete();

        if($imagens && is_array($imagens)){

            foreach ($imagens as $key => $value){
                $obj = new PortfolioPersonagens;
                $obj->portfolio_personalidades_id = $id;
                $obj->imagem = $value;
                $obj->ordem = $key;
                $obj->save();
            }
        }
    }

    private function imagensCapas(Request $request, $id)
    {
        $imagens = $request->get('imagem_capas');

        PortfolioCapas::where('portfolio_personalidades_id', '=', $id)->delete();

        if($imagens && is_array($imagens)){

            foreach ($imagens as $key => $value){
                $obj = new PortfolioCapas;
                $obj->portfolio_personalidades_id = $id;
                $obj->imagem = $value;
                $obj->ordem = $key;
                $obj->save();
            }
        }
    }

    private function imagensPublicidade(Request $request, $id)
    {
        $imagens = $request->get('imagem_publicidade');

        PortfolioPublicidade::where('portfolio_personalidades_id', '=', $id)->delete();

        if($imagens && is_array($imagens)){

            foreach ($imagens as $key => $value){
                $obj = new PortfolioPublicidade;
                $obj->portfolio_personalidades_id = $id;
                $obj->imagem = $value;
                $obj->ordem = $key;
                $obj->save();
            }
        }
    }

    private function imagensDesfiles(Request $request, $id)
    {
        $imagens = $request->get('imagem_desfiles');

        PortfolioDesfiles::where('portfolio_personalidades_id', '=', $id)->delete();

        if($imagens && is_array($imagens)){

            foreach ($imagens as $key => $value){
                $obj = new PortfolioDesfiles;
                $obj->portfolio_personalidades_id = $id;
                $obj->imagem = $value;
                $obj->ordem = $key;
                $obj->save();
            }
        }
    }

    private function armazenaVideos(Request $request, $id)
    {
        $videos = $request->get('videos_titulo');

        if($videos && is_array($videos)){

            // remover ids que não foram enviados
            PortfolioVideos::where('portfolio_personalidades_id', $id)
                           ->whereNotIn('id', $request->input('videos_id'))
                           ->delete();

            foreach ($videos as $key => $value){

                $url = ($request->has('videos_url.'.$key)) ? $request->input('videos_url.'.$key) : '';

                if($request->has('videos_id.'.$key))
                    $obj = PortfolioVideos::find($request->input('videos_id.'.$key));
                else
                    $obj = new PortfolioVideos;

                $obj->titulo = $value;
                $obj->ordem = $key;
                $obj->portfolio_personalidades_id = $id;


                if($obj->url != $url){
                    $video = $this->_getVideoInfo($url);
                    $obj->servico = $video['servico'];
                    $obj->servico_video_id = $video['servico_video_id'];
                    $obj->thumbnail = $video['thumbnail'];
                    $obj->embed = $video['embed'];
                }
                $obj->url = $url;
                $obj->save();
            }
        }else{
            PortfolioVideos::where('portfolio_personalidades_id', $id)
                           ->delete();
        }
    }

    private function _getVideoInfo($url)
    {
        // Verificar se link é VIMEO | YOUTUBE
        if(str_is('*vimeo*', $url))
            $video = new Vimeo($url);
        else
            $video = new Youtube($url);


        $info['servico'] = $video->getServico();
        $info['servico_video_id'] = $video->getId();
        $info['thumbnail'] = $video->getThumbnail();
        $info['embed'] = $video->getEmbed();

        return $info;
    }

    private function armazenaAcaoSocial(Request $request, $id)
    {
        $acoes = $request->get('acaosocial_titulo');

        PortfolioAcaoSocial::where('portfolio_personalidades_id', '=', $id)->delete();

        if($acoes && is_array($acoes)){

            foreach ($acoes as $key => $value){
                $obj = new PortfolioAcaoSocial;
                $obj->portfolio_personalidades_id = $id;
                $obj->titulo = $value;
                $obj->text = ($request->has('acaosocial_texto.'.$key)) ? $request->input('acaosocial_texto.'.$key) : '';
                $obj->imagem = ($request->has('acaosocial_imagem.'.$key)) ? $request->input('acaosocial_imagem.'.$key) : '';
                $obj->ordem = $key;
                $obj->save();
            }
        }
    }

    private function imagensEditorialDeModa(Request $request, $id)
    {
        $imagens = $request->get('imagem_editorialdemoda');

        PortfolioEditorialDeModa::where('portfolio_personalidades_id', '=', $id)->delete();

        if($imagens && is_array($imagens)){

            foreach ($imagens as $key => $value){
                $obj = new PortfolioEditorialDeModa;
                $obj->portfolio_personalidades_id = $id;
                $obj->imagem = $value;
                $obj->ordem = $key;
                $obj->save();
            }
        }
    }

    private function imagensPersonalStylist(Request $request, $id)
    {
        $imagens = $request->get('imagem_personalstylist');

        PortfolioPersonalStylist::where('portfolio_personalidades_id', '=', $id)->delete();

        if($imagens && is_array($imagens)){

            foreach ($imagens as $key => $value){
                $obj = new PortfolioPersonalStylist;
                $obj->portfolio_personalidades_id = $id;
                $obj->imagem = $value;
                $obj->ordem = $key;
                $obj->save();
            }
        }
    }

    private function armazenaMidiasSociais(Request $request, $id)
    {
        PortfolioRedesSociais::where('portfolio_personalidades_id', '=', $id)->delete();

        $obj = new PortfolioRedesSociais;
        $obj->portfolio_personalidades_id = $id;
        $obj->instagram = $request->input('midiassociais.instagram');
        $obj->facebook = $request->input('midiassociais.facebook');
        $obj->twitter = $request->input('midiassociais.twitter');
        $obj->vimeo = $request->input('midiassociais.vimeo');
        $obj->youtube = $request->input('midiassociais.youtube');
        $obj->linkedin = $request->input('midiassociais.linkedin');
        $obj->flickr = $request->input('midiassociais.flickr');
        $obj->pinterest = $request->input('midiassociais.pinterest');
        $obj->gplus = $request->input('midiassociais.gplus');
        $obj->tumblr = $request->input('midiassociais.tumblr');
        $obj->imdb = $request->input('midiassociais.imdb');
        $obj->save();
    }

    private function armazenaPalestras(Request $request, $id)
    {
        PortfolioPalestras::where('portfolio_personalidades_id', '=', $id)->delete();

        $obj = new PortfolioPalestras;
        $obj->portfolio_personalidades_id = $id;
        $obj->texto = $request->input('palestras.texto');
        $obj->save();
    }

    private function armazenaConsultoria(Request $request, $id)
    {
        PortfolioConsultoriaModa::where('portfolio_personalidades_id', '=', $id)->delete();

        $obj = new PortfolioConsultoriaModa;
        $obj->portfolio_personalidades_id = $id;
        $obj->texto = $request->input('consultoria.texto');
        $obj->save();
    }

    private function armazenaPublicacoes(Request $request, $id)
    {
        $publi = $request->get('publicacoes_titulo');

        PortfolioPublicacoes::where('portfolio_personalidades_id', '=', $id)->delete();

        if($publi && is_array($publi)){

            foreach ($publi as $key => $value){
                $obj = new PortfolioPublicacoes;
                $obj->portfolio_personalidades_id = $id;
                $obj->titulo = $value;
                $obj->autor = ($request->has('publicacoes_autor.'.$key)) ? $request->input('publicacoes_autor.'.$key) : '';
                $obj->editora = ($request->has('publicacoes_editora.'.$key)) ? $request->input('publicacoes_editora.'.$key) : '';
                $obj->imagem = ($request->has('publicacoes_imagem.'.$key)) ? $request->input('publicacoes_imagem.'.$key) : '';
                $obj->texto = ($request->has('publicacoes_texto.'.$key)) ? $request->input('publicacoes_texto.'.$key) : '';
                $obj->ordem = $key;
                $obj->save();
            }
        }
    }

    public function armazenaExtras(Request $request, $id)
    {
        PortfolioExtras::where('portfolio_personalidades_id', '=', $id)->delete();

        $obj = new PortfolioExtras;
        $obj->portfolio_personalidades_id = $id;
        $obj->texto = $request->input('extras.texto');
        $obj->save();
    }

    private function imagensRedCarpet(Request $request, $id)
    {
        $imagens = $request->get('imagem_redcarpet');

        PortfolioRedCarpet::where('portfolio_personalidades_id', '=', $id)->delete();

        if($imagens && is_array($imagens)){

            foreach ($imagens as $key => $value){
                $obj = new PortfolioRedCarpet;
                $obj->portfolio_personalidades_id = $id;
                $obj->imagem = $value;
                $obj->ordem = $key;
                $obj->save();
            }
        }
    }

    private function armazenaEspetaculos(Request $request, $id)
    {
        $espet = $request->get('espetaculos_titulo');

        PortfolioEspetaculos::where('portfolio_personalidades_id', '=', $id)->delete();

        if($espet && is_array($espet)){

            foreach ($espet as $key => $value){
                $obj = new PortfolioEspetaculos;
                $obj->portfolio_personalidades_id = $id;
                $obj->titulo = $value;
                $obj->autor = ($request->has('espetaculos_autor.'.$key)) ? $request->input('espetaculos_autor.'.$key) : '';
                $obj->editora = ($request->has('espetaculos_editora.'.$key)) ? $request->input('espetaculos_editora.'.$key) : '';
                $obj->imagem = ($request->has('espetaculos_imagem.'.$key)) ? $request->input('espetaculos_imagem.'.$key) : '';
                $obj->texto = ($request->has('espetaculos_texto.'.$key)) ? $request->input('espetaculos_texto.'.$key) : '';
                $obj->ordem = $key;
                $obj->save();
            }
        }
    }

    private function armazenaWorkshops(Request $request, $id)
    {
      $work = $request->get('workshops_titulo');

      PortfolioWorkshops::where('portfolio_personalidades_id', '=', $id)->delete();

      if($work && is_array($work)){

          foreach ($work as $key => $value){
              $obj = new PortfolioWorkshops;
              $obj->portfolio_personalidades_id = $id;
              $obj->titulo = $value;
              $obj->autor = ($request->has('workshops_autor.'.$key)) ? $request->input('workshops_autor.'.$key) : '';
              $obj->editora = ($request->has('workshops_editora.'.$key)) ? $request->input('workshops_editora.'.$key) : '';
              $obj->imagem = ($request->has('workshops_imagem.'.$key)) ? $request->input('workshops_imagem.'.$key) : '';
              $obj->texto = ($request->has('workshops_texto.'.$key)) ? $request->input('workshops_texto.'.$key) : '';
              $obj->ordem = $key;
              $obj->save();
          }
      }
    }

    private function armazenaPremios(Request $request, $id)
    {
      $premios = $request->get('premios_titulo');

      PortfolioPremios::where('portfolio_personalidades_id', '=', $id)->delete();

      if($premios && is_array($premios)){

          foreach ($premios as $key => $value){
              $obj = new PortfolioPremios;
              $obj->portfolio_personalidades_id = $id;
              $obj->titulo = $value;
              $obj->imagem = ($request->has('premios_imagem.'.$key)) ? $request->input('premios_imagem.'.$key) : '';
              $obj->texto = ($request->has('premios_texto.'.$key)) ? $request->input('premios_texto.'.$key) : '';
              $obj->ordem = $key;
              $obj->save();
          }
      }
    }

    public function armazenaCreditos(Request $request, $id)
    {
        PortfolioCreditos::where('portfolio_personalidades_id', '=', $id)->delete();

        $obj = new PortfolioCreditos;
        $obj->portfolio_personalidades_id = $id;
        $obj->texto = $request->input('creditos.texto');
        $obj->save();
    }

    private function armazenaIdiomas(Request $request, $object)
    {
      $idiomas = $request->idiomas;

      $object->idiomas()->detach();
      $object->idiomas()->attach($idiomas);
    }

    public function postAdicionarIdioma(Request $request){

      $validator = Validator::make($request->all(), [
        'titulo' => 'required|unique:portfolio_idiomas',
        'imagem' => 'required'
      ]);

      if ($validator->fails()) {

        return ['status' => 'erro'];

      }else{

        $idioma = new PortfolioIdioma;
        $idioma->titulo = $request->titulo;
        $idioma->imagem = $request->imagem;
        $idioma->save();

        return [
          'status' => 'ok',
          'id'     => $idioma->id,
          'titulo' => $request->titulo,
          'imagem' => $request->imagem,
        ];
      }

    }
}
