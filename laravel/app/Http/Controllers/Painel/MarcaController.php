<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Libs\Thumbs;
use App\Models\Marca;

class MarcaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $registros = Marca::all();

        return view('painel.marca.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return view('painel.marca.edit')->with('registro', Marca::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $object = Marca::find($id);

        $imagem = Thumbs::make('imagem', 200, 220, 'marca/');
        if($imagem) $object->imagem = $imagem;

        try {

            $object->save();

            $request->session()->flash('sucesso', 'Marca alterada com sucesso.');

            return redirect()->route('painel.marca.index');

        } catch (\Exception $e) {

            $request->flash();

            return back()->withErrors(array('Erro ao alterar texto! ('.$e->getMessage().')'));

        }
    }

}
