<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Libs\Thumbs;

class BannersController extends Controller
{

  protected $pathImagens = 'banners/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = Banner::ordenado()->get();

      return view('painel.banners.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      return view('painel.banners.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'imagem' => 'required|image'
    	]);

      $object = new Banner;

      $object->titulo = $request->titulo;
      $object->frase = $request->frase;
      $object->link = $request->link;

      $imagem = Thumbs::make('imagem', null, 220, $this->pathImagens);
      if($imagem) $object->imagem = $imagem;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.banners.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('painel.banners.edit')->with('registro', Banner::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'imagem' => 'sometimes|image'
    	]);

      $object = Banner::find($id);

      $object->titulo = $request->titulo;
      $object->frase = $request->frase;
      $object->link = $request->link;

      $imagem = Thumbs::make('imagem', null, 220, $this->pathImagens);
      if($imagem) $object->imagem = $imagem;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.banners.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = Banner::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.banners.index');
    }

}
