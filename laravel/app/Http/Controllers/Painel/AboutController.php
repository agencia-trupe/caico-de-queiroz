<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use View;

use App\Libs\Thumbs;
use App\Models\About;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $registros = About::all();

        return view('painel.about.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return view('painel.about.edit')->with('registro', About::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $object = About::find($id);

        $object->texto = $request->input('texto');

        $imagem = Thumbs::make('imagem', 900, 900, 'about/');
        if($imagem) $object->imagem = $imagem;

        $object->instagram = $request->input('instagram');
        $object->twitter   = $request->input('twitter');
        $object->facebook  = $request->input('facebook');
        $object->tumblr    = $request->input('tumblr');
        $object->linkedin  = $request->input('linkedin');
        $object->pinterest = $request->input('pinterest');
        $object->youtube   = $request->input('youtube');
        $object->periscope = $request->input('periscope');
        $object->vimeo     = $request->input('vimeo');
        $object->googleplus= $request->input('googleplus');
        $object->wordpress = $request->input('wordpress');
        $object->snapchat  = $request->input('snapchat');

        try {

            $object->save();

            $request->session()->flash('sucesso', 'Texto alterado com sucesso.');

            return redirect()->route('painel.about.index');

        } catch (\Exception $e) {

            $request->flash();

            return back()->withErrors(array('Erro ao alterar texto! ('.$e->getMessage().')'));

        }
    }

}
