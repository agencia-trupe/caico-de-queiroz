<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use View;

use App\Libs\Thumbs;
use App\Models\AcaoSocial;
use App\Models\AcaoSocialImagens;

class AcaoSocialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $registros = AcaoSocial::all();

        return view('painel.acaosocial.index')->with(compact('registros'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return view('painel.acaosocial.edit')->with('registro', AcaoSocial::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $object = AcaoSocial::find($id);

        $object->texto = $request->input('texto');

        try {

            $object->save();

            $this->updateImagens($request, $id);

            $request->session()->flash('sucesso', 'Texto alterado com sucesso.');

            return redirect()->route('painel.acaosocial.index');

        } catch (\Exception $e) {

            $request->flash();

            return back()->withErrors(array('Erro ao alterar texto! ('.$e->getMessage().')'));

        }
    }

    /**
     * Update the images od the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return void
     */

    private function updateImagens(Request $request, $id)
    {
        AcaoSocialImagens::where('acao_social_id', '=', $id)->delete();

        $imagens = $request->get('imagem');

        if($imagens && is_array($imagens)){
            foreach ($imagens as $key => $value) {
                $obj = new AcaoSocialImagens;
                $obj->acao_social_id = $id;
                $obj->imagem = $value;
                $obj->ordem = $key;
                $obj->save();
            }
        }
    }
}
