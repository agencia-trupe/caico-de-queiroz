<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use View;

use App\Libs\Thumbs;
use App\Models\Destaque;
use App\Models\DestaquesBanners;
use App\Libs\Youtube;

class DestaquesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $registros = Destaque::all();

      return view('painel.destaques.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $registro = Destaque::find($id);

      $video1 = new Youtube($registro->embed_1);
      $video2 = new Youtube($registro->embed_2);

      return view('painel.destaques.edit')->with([
        'registro' => $registro,
        'video1' => $video1,
        'video2' => $video2,
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $object = Destaque::findOrFail($id);

      $object->fill($request->except('_method', '_token', 'imagem'));

      $this->uploadImages($object);
      $this->uploadBanners($object, $request);

      try {

          $object->save();

          $request->session()->flash('sucesso', 'Destaques alterados com sucesso.');

          return redirect()->route('painel.destaques.index');

      } catch (\Exception $e) {

          $request->flash();

          return back()->withErrors(array('Erro ao alterar destaques! ('.$e->getMessage().')'));

      }
    }

    private function uploadImages($object)
    {
      $image_index_with_large_thumbs = [1, 4, 5, 8, 11];

      for ($i=1; $i <= 11; $i++) {
        if(in_array($i, $image_index_with_large_thumbs)) {
          $imagem = Thumbs::make("imagem_{$i}", 800, 400, 'destaques/');
        } else {
          $imagem = Thumbs::make("imagem_{$i}", 400, 400, 'destaques/');
        }
        
        if($imagem) $object->{"imagem_{$i}"} = $imagem;  
      }
    }

    private function uploadBanners($object, $request)
    {
      DestaquesBanners::where('destaque_id', '=', $object->id)->delete();
      
      $imagens = $request->get('imagem');

      if($imagens && is_array($imagens)){
        foreach ($imagens as $key => $value) {
          $obj = new DestaquesBanners;
          $obj->destaque_id = $object->id;
          $obj->imagem = $value;
          $obj->ordem = $key;
          $obj->save();
        }
      }
    }
}
