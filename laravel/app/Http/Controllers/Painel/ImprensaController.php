<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use View;

use App\Libs\Thumbs;
use App\Models\Imprensa;

class ImprensaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $registros = Imprensa::all();

        return view('painel.imprensa.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return view('painel.imprensa.edit')->with('registro', Imprensa::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $object = Imprensa::find($id);

        $object->texto = $request->input('texto');

        $imagem = Thumbs::make('imagem', 900, 900, 'imprensa/');
        if($imagem) $object->imagem = $imagem;

        try {

            $object->save();

            $request->session()->flash('sucesso', 'Texto alterado com sucesso.');

            return redirect()->route('painel.imprensa.index');

        } catch (\Exception $e) {

            $request->flash();

            return back()->withErrors(array('Erro ao alterar texto! ('.$e->getMessage().')'));

        }
    }

}
