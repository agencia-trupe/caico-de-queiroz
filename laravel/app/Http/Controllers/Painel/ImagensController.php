<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use View;

use App\Libs\Thumbs;

class ImagensController extends Controller
{

	/*
		Armazena as imagens enviadas via Ajax
	*/
	public function upload(Request $request)
	{
    $arquivo = $request->file('files');
    $path    = $request->input('path');

	  $path_original = "assets/img/{$path}/originais/";
    $path_asset    = "assets/img/{$path}/redimensionadas/";
    $path_thumb    = "assets/img/{$path}/thumbs/";
	  $path_upload   = public_path($path_original);

    // $this->criarDiretoriosPadrao($path);

    $nome_arquivo = $arquivo->getClientOriginalName();
    $extensao_arquivo = $arquivo->getClientOriginalExtension();
    $nome_arquivo_sem_extensao = str_replace($extensao_arquivo, '', $nome_arquivo);

	  $filename = date('YmdHis').str_slug($nome_arquivo_sem_extensao).'.'.$extensao_arquivo;

    // Armazenar Original
    $arquivo->move($path_upload, $filename);

    $name = $path_original.$filename;
    $thumb = $path_thumb.$filename;

    // Armazenar Redimensionada
    if(str_is('*paralaxe*', $path)){

      // $this->criarDiretoriosParalaxe($path);

			// redimensionadas/horizontais
			// redimensionadas/verticais
      Thumbs::makeFromFile($path_upload, $filename, 800, 500, public_path($path_asset.'horizontais/'), 'rgba(0,0,0,0)', true, 'top');
      Thumbs::makeFromFile($path_upload, $filename, 400, 700, public_path($path_asset.'verticais/'), 'rgba(0,0,0,0)', true, 'top');

    }elseif(str_is('*idiomas*', $path)){

			// redimensionadas/icones
			Thumbs::makeFromFile($path_upload, $filename, 60, null, public_path($path_asset.'icones/'), 'rgba(0,0,0,0)', true, 'top');

		}else{

			// redimensionadas
      Thumbs::makeFromFile($path_upload, $filename, 900, null, public_path($path_asset), 'rgba(0,0,0,0)', false);
    }

    // Armazenar Thumb
  	Thumbs::makeFromFile($path_upload, $filename, 400, 400, public_path($path_thumb), 'rgba(0,0,0,0)', true);

  	return [
    	'thumb' => $thumb,
    	'filename' => $filename
    ];
	}


    private function criarDiretoriosPadrao($path)
    {
      if(!file_exists(public_path("assets/img/{$path}")))
        mkdir(public_path("assets/img/{$path}"), 0777);

      if(!file_exists(public_path("assets/img/{$path}/originais/")))
        mkdir(public_path("assets/img/{$path}/originais/"), 0777);

      if(!file_exists(public_path("assets/img/{$path}/redimensionadas/")))
        mkdir(public_path("assets/img/{$path}/redimensionadas/"), 0777);

      if(!file_exists(public_path("assets/img/{$path}/thumbs/")))
        mkdir(public_path("assets/img/{$path}/thumbs/"), 0777);
    }

    private function criarDiretoriosParalaxe($path)
    {
      if(!file_exists(public_path("assets/img/{$path}/redimensionadas/horizontais")))
        mkdir(public_path("assets/img/{$path}/redimensionadas/horizontais"), 0777);

      if(!file_exists(public_path("assets/img/{$path}/redimensionadas/verticais")))
        mkdir(public_path("assets/img/{$path}/redimensionadas/verticais"), 0777);
    }
}
