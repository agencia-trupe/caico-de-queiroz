<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use View;

use App\Libs\Thumbs;
use App\Models\Contato;

class ContatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $registros = Contato::all();

        return view('painel.contato.index')->with(compact('registros'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return view('painel.contato.edit')->with('registro', Contato::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $object = Contato::find($id);

        $object->fill($request->except('_method', '_token'));

        try {

            $object->save();

            $request->session()->flash('sucesso', 'Texto alterado com sucesso.');

            return redirect()->route('painel.contato.index');

        } catch (\Exception $e) {

            $request->flash();

            return back()->withErrors(array('Erro ao alterar texto! ('.$e->getMessage().')'));

        }
    }

}