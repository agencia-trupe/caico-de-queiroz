<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use View;

use App\Libs\Thumbs;
use App\Models\Parcerias;
use App\Models\ParceriasImagens;

class ParceriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $registros = Parcerias::all();

        return view('painel.parcerias.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return view('painel.parcerias.edit')->with('registro', Parcerias::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $object = Parcerias::find($id);

        $object->texto = $request->input('texto');

        $imagem = Thumbs::make('imagem', 900, 900, 'parcerias/');
        if($imagem) $object->imagem = $imagem;

        try {

            $object->save();

            $request->session()->flash('sucesso', 'Texto alterado com sucesso.');

            return redirect()->route('painel.parcerias.index');

        } catch (\Exception $e) {

            $request->flash();

            return back()->withErrors(array('Erro ao alterar texto! ('.$e->getMessage().')'));

        }
    }

}
