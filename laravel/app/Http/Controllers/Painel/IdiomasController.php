<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;

use App\Libs\Thumbs;
use App\Models\PortfolioIdioma;

class IdiomasController extends Controller{

	public function index()
	{
		$registros = PortfolioIdioma::ordenado()->get();

		return view('painel.idiomas.index')->with(compact('registros'));
	}

	public function create()
	{
		return view('painel.idiomas.create');
	}

	public function store(Request $request)
	{
		$this->validate($request, [
    	'titulo' => 'required|unique:portfolio_idiomas',
    	'imagem' => 'required|image'
    ]);

		$object = new PortfolioIdioma;

		$object->titulo = $request->titulo;

		$imagem = Thumbs::make('imagem', 60, null, 'portfolio/idiomas/redimensionadas/icones/');
			if($imagem) $object->imagem = $imagem;

		try {

			$object->save();

			$request->session()->flash('sucesso', 'Idioma criado com sucesso.');

			return redirect()->route('painel.idiomas.index');

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao criar idioma! ('.$e->getMessage().')'));

		}
	}

	public function edit($id)
	{
		$registro = PortfolioIdioma::find($id);

		return view('painel.idiomas.edit')->with(compact('registro'));
	}

	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'titulo' => 'required|unique:portfolio_idiomas,titulo,'.$id,
    	'imagem' => 'sometimes|image'
  	]);

		$object = PortfolioIdioma::find($id);

		$object->titulo = $request->titulo;

		$imagem = Thumbs::make('imagem', 60, null, 'portfolio/idiomas/redimensionadas/icones/');
			if($imagem) $object->imagem = $imagem;

		try {

			$object->save();

			$request->session()->flash('sucesso', 'Idioma alterado com sucesso.');

			return redirect()->route('painel.idiomas.index');

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao alterar idioma! ('.$e->getMessage().')'));

		}
	}

	public function destroy(Request $request, $id)
	{
		$object = PortfolioIdioma::find($id);
		$object->delete();

		$request->session()->flash('sucesso', 'Idioma removido com sucesso.');

		return redirect()->route('painel.idiomas.index');
	}
}
