<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use View;

class HomeController extends Controller {

    public function index()
	{
		return View::make('painel.home.index');
	}

	public function login()
	{
		return View::make('painel.login.index');
	}

}