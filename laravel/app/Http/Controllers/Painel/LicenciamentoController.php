<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use View;

use App\Libs\Thumbs;
use App\Models\Licenciamento;
use App\Models\LicenciamentoImagens;

class LicenciamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $registros = Licenciamento::all();

        return view('painel.licenciamento.index')->with(compact('registros'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return view('painel.licenciamento.edit')->with('registro', Licenciamento::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $object = Licenciamento::find($id);

        $object->texto = $request->input('texto');

        try {

            $object->save();

            $this->updateImagens($request, $id);

            $request->session()->flash('sucesso', 'Texto alterado com sucesso.');

            return redirect()->route('painel.licenciamento.index');

        } catch (\Exception $e) {

            $request->flash();

            return back()->withErrors(array('Erro ao alterar texto! ('.$e->getMessage().')'));

        }
    }

    /**
     * Update the images od the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return void
     */

    private function updateImagens(Request $request, $id)
    {
        LicenciamentoImagens::where('licenciamento_id', '=', $id)->delete();

        $imagens = $request->get('imagem');

        if($imagens && is_array($imagens)){
            foreach ($imagens as $key => $value) {
                $obj = new LicenciamentoImagens;
                $obj->licenciamento_id = $id;
                $obj->imagem = $value;
                $obj->ordem = $key;
                $obj->save();
            }
        }
    }
}
