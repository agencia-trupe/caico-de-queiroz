<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Licenciamento;

class LicenciamentoController extends Controller
{
    /**
     * Página /licenciamento
     *
     * @return Response
     */
    public function index()
    {
    	$licenciamento = Licenciamento::first();

        return view('site.licenciamento.index')->with(compact('licenciamento'));
    }

}
