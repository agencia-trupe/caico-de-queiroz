<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Destaque;
use App\Models\DestaquesBanners;
use App\Libs\Youtube;

class DestaquesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $destaques = Destaque::first();
      $embed1 = new Youtube($destaques->embed_1);
      $embed2 = new Youtube($destaques->embed_2);
      $banners = DestaquesBanners::orderByRaw("RAND()")->get();

      return view('site.destaques.index')->with([
        'destaques' => $destaques,
        'embed1' => $embed1->getEmbed(500),
        'embed2' => $embed2->getEmbed(500),
        'banners' => $banners
      ]);
    }
}
