<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\AcaoSocial;

class AcaoSocialController extends Controller
{
    /**
     * Página /acao-social
     *
     * @return Response
     */
    public function index()
    {
    	$acaosocial = AcaoSocial::first();

        return view('site.acaosocial.index')->with(compact('acaosocial'));
    }

}
