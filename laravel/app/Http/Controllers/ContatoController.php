<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Contato;

use Mail;
use Log;

class ContatoController extends Controller
{
    /**
     * Página /contato
     *
     * @return Response
     */
    public function index()
    {
    	$contato = Contato::first();

        return view('site.contato.index')->with(compact('contato'));
    }

    /**
     * Enviar Formulário de Contato
     *
     * @return Response
     */
    public function enviar(Request $request)
    {
        $data['nome'] = $request->input('contato_nome');
        $data['email'] = $request->input('contato_email');
        $data['telefone'] = $request->input('contato_telefone');
        $data['mensagem'] = $request->input('contato_mensagem');

        Mail::send('emails.contato', $data, function($message) use ($data)
        {
            $message->to(config('site.contato.email'), config('site.contato.nome'))
                    ->subject(config('site.contato.assunto'))
                    ->bcc('bruno@trupe.net')
                    ->replyTo($data['email'], $data['nome']);

            if(config('mail.pretend')) Log::info(view('emails.contato', $data)->render());
        });

        return back()->with('contatoEnviado', true);
    }

}
