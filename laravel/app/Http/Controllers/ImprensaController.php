<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Imprensa;

class ImprensaController extends Controller
{
    /**
     * Página /imprensa
     *
     * @return Response
     */
    public function index()
    {
    	$imprensa = Imprensa::first();

        return view('site.imprensa.index')->with(compact('imprensa'));
    }

}
