<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Parcerias;

class ParceriasController extends Controller
{
    /**
     * Página /parcerias
     *
     * @return Response
     */
    public function index()
    {
    	$parcerias = Parcerias::first();

        return view('site.parcerias.index')->with(compact('parcerias'));
    }

}
