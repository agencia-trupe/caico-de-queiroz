<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'site.home', 'uses' => 'HomeController@index']);
Route::get('about', ['as' => 'site.about', 'uses' => 'AboutController@index']);
Route::get('imprensa', ['as' => 'site.imprensa', 'uses' => 'ImprensaController@index']);
Route::get('publicacoes', ['as' => 'site.publicacoes', 'uses' => 'PublicacoesController@index']);
Route::get('acao-social', ['as' => 'site.acaosocial', 'uses' => 'AcaoSocialController@index']);
Route::get('juridico', ['as' => 'site.juridico', 'uses' => 'JuridicoController@index']);
Route::get('parcerias', ['as' => 'site.parcerias', 'uses' => 'ParceriasController@index']);
Route::get('licenciamento', ['as' => 'site.licenciamento', 'uses' => 'LicenciamentoController@index']);
Route::get('contato', ['as' => 'site.contato', 'uses' => 'ContatoController@index']);
Route::post('contato', ['as' => 'site.contato.enviar', 'uses' => 'ContatoController@enviar']);
Route::get('destaques', ['as' => 'site.destaques', 'uses' => 'DestaquesController@index']);

Route::get('portfolio/entrada/{slug?}', ['as' => 'site.portfolio.entrada', 'uses' => 'PortfolioController@entrada']);
Route::get('portfolio/detalhe/{slug?}', ['as' => 'site.portfolio.detalhe', 'uses' => 'PortfolioController@detalhe']);
Route::get('portfolio/release/{slug?}', ['as' => 'site.portfolio.release', 'uses' => 'PortfolioController@release']);

Route::post('mosaico/novo-item', function(Illuminate\Http\Request $request){

	$tipo = $request->tipo;
	$ids_atuais = $request->ids_atuais;

	if($tipo == 'personalidade'){

		$item = App\Models\PortfolioPersonalidades::comThumb()
																							->where('publicar', 1)
																							->aleatorio()
																							->whereNotIn('id', $ids_atuais)
																							->whereHas('categoria', function($query){
																								$query->where('publicar', 1);
																							})
																							->first()
																							->toArray();

	}elseif($tipo == 'categoria'){

		$omitir = [9, 11];

		$item = App\Models\PortfolioCategorias::ordenado()->whereNotIn('id', array_merge($ids_atuais, $omitir))->first()->toArray();

	}elseif($tipo == 'subcategoria'){

		$omitir = [13, 18, 22];

		$item = App\Models\PortfolioSubcategorias::with('categoria')->ordenado()->whereNotIn('id', array_merge($ids_atuais, $omitir))->first()->toArray();

	}

	return $item;
});

// Route::get('cut', function(){
// 	$r = App\Models\PortfolioPersonalidades::all();
// 	foreach($r as $port){
//
// 		$img = $port->imagem_entrada;
// 		$port->thumb_home = $img;
// 		$port->save();
//
// 		echo $img.' redimensionado<br>';
//
// 	}
// });
//*******************************************************************************************//
//*******************************************************************************************//
//*******************************************************************************************//


// Tela de Login do painel
Route::get('painel/login', ['as' => 'painel.login', 'uses' => 'Painel\HomeController@login']);

// Efetuar Login via POST
Route::post('painel/login', ['as' => 'painel.auth', 'uses' => 'Auth\AuthController@postLogin']);

// Logout do Painel
Route::get('painel/logout', ['as' => 'painel.off', 'uses' => 'Auth\AuthController@getLogout']);

Route::post('getSubcategorias', ['as' => 'get.subcategorias', function(Illuminate\Http\Request $request){

	$categoria = App\Models\PortfolioCategorias::find($request->input('cat_id'));

	if($categoria)
		return $categoria->subcategorias;
	else
		return [];
}]);


// Página principal do Painel
Route::group([
		'middleware' => 'auth',
		'namespace' => 'Painel',
		'prefix' => 'painel'
	], function () {

		Route::get('/', ['as' => 'painel.home', 'uses' => 'HomeController@index']);
		Route::post('imagens/upload', ['as' => 'imagens.upload', 'uses' => 'ImagensController@upload']);
		Route::post('imagens/uploadParalaxe', ['as' => 'imagens.upload-paralaxe', 'uses' => 'ImagensController@uploadParalaxe']);
		Route::post('idiomas/adicionar', 'PortfolioController@postAdicionarIdioma');

		Route::post('ajax/gravaOrdem', function(Illuminate\Http\Request $request){
      $itens = $request->input('data');
      $tabela = $request->input('tabela');
      for ($i = 0; $i < count($itens); $i++)
       DB::table($tabela)->where('id', $itens[$i])->update(array('ordem' => $i));
    });

		Route::resource('usuarios', 'UsuariosController', ['except' => ['show']]);

		Route::resource('about', 'AboutController', ['only' => ['index', 'edit', 'update']]);
		Route::resource('imprensa', 'ImprensaController', ['only' => ['index', 'edit', 'update']]);
		Route::resource('juridico', 'JuridicoController', ['only' => ['index', 'edit', 'update']]);

		Route::resource('publicacoes', 'PublicacoesController', ['only' => ['index', 'edit', 'update']]);
		Route::resource('acaosocial', 'AcaoSocialController', ['only' => ['index', 'edit', 'update']]);
		Route::resource('parcerias', 'ParceriasController', ['only' => ['index', 'edit', 'update']]);
		Route::resource('licenciamento', 'LicenciamentoController', ['only' => ['index', 'edit', 'update']]);

		Route::resource('contato', 'ContatoController', ['only' => ['index', 'edit', 'update']]);

		Route::resource('portfolio', 'PortfolioController', ['except' => ['show']]);

		Route::resource('idiomas', 'IdiomasController', ['except' => ['show']]);

		Route::resource('marca', 'MarcaController', ['except' => ['show']]);
    Route::resource('banners', 'BannersController', ['except' => ['show']]);
    
    Route::resource('destaques', 'DestaquesController', ['only' => ['index', 'edit', 'update']]);
	}
);


/*
Route::get('fix-83jf3453165f413q1f', function(){
	$registros = \App\Models\PortfolioParalaxe::all();
	foreach($registros as $registro){
		for ($i=1; $i <= 5; $i++) {

			$titulo = "imagem_{$i}";
			$img = $registro->{$titulo};

			$diretorios = [
				'assets/img/portfolio/paralaxe/originais/',
				'assets/img/portfolio/paralaxe/redimensionadas/horizontais/',
				'assets/img/portfolio/paralaxe/redimensionadas/verticais/',
				'assets/img/portfolio/paralaxe/thumbs/'
			];

			foreach($diretorios as $dir){
				if($img != '' && $img != '.' && $img != '..' && $img != '/' && !file_exists(public_path($dir.$img))){
					echo "-- NAO encontrado: {$dir}{$img} <br>";

					$outros_tipos = ['jpg', 'jpeg', 'bmp', 'tif', 'gif'];
					foreach($outros_tipos as $tipo){
						$novo_tipo = str_replace('png', $tipo, $img);
						if(file_exists(public_path($dir.$novo_tipo))){
							echo "mas existe {$tipo}<br>";
							//rename(public_path($dir.$novo_tipo), public_path($dir.$img));
						}
					}
					echo '<br>';
				}
			}

		}
	}
});
*/
