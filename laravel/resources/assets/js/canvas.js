var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
var raf;

function Cruz(){

	this.quadrante = false;
	this.iniciado = false;
	this.finalizado = false;
	this.x = null;
	this.y = null;
	this.color = '182,184,187';
	this.opacidade = 0;
	this.vopacidade = null;
	this.barrasIniciadas = false;
	this.barrasFinalizadas = false;
	this.barraHorizontal = null;
	this.barraVertical = null;

	this.baseH = 0;
	this.baseW = 0;

	this.delay = 0;
	this.delay_contador = 0;

	this.mostrando = true;
	this.apagando = false;

	this.draw = function(quadrante){

		if(!this.iniciado){
			this.baseH = window.innerHeight;
			this.baseW = window.innerWidth;
			this.quadrante = quadrante;
			this.delay = CaicoCanvas._numeroAleatorioEntre(5,60);
			switch(this.quadrante){
				case 1:
					var deltax = [0,(0.45 * this.baseW)];
					var deltay = [0,(0.5 * this.baseH)];
					break;
				case 2:
					var deltax = [(0.55 * this.baseW),this.baseW];
					var deltay = [0,(0.5 * this.baseH)];
					break;
				case 3:
					var deltax = [0,(0.45 * this.baseW)];
					var deltay = [(0.5 * this.baseH),this.baseH];
					break;
				case 4:
					var deltax = [(0.55 * this.baseW),this.baseW];
					var deltay = [(0.5 * this.baseH),this.baseH];
					break;
			}

			this.x = CaicoCanvas._numeroAleatorioEntre(deltax[0] + 40, deltax[1] - 40);
			this.y = CaicoCanvas._numeroAleatorioEntre(deltay[0] + 40, deltay[1] - 40);
			this.vopacidade = CaicoCanvas._numeroAleatorioEntre(0.04, 0.09);
			this.iniciado = true;
		}

		if(this.delay_contador > this.delay){

			if(this.mostrando){

				this.opacidade += this.vopacidade;

				if(this.opacidade >= 1){
					if(!this.barrasIniciadas){
						this.barraHorizontal = new Barra();
						this.barraVertical = new Barra();
						this.barrasIniciadas = true;
					}

					this.barraHorizontal.mover();
					this.barraVertical.mover();

					this.barraHorizontal.draw('horizontal', this.x, this.y);
					this.barraVertical.draw('vertical', this.x, this.y);

					if(this.barraHorizontal.x >= canvas.width && this.barraVertical.y >= canvas.height ){
						this.barrasFinalizadas = true;
					}

					if(this.barrasFinalizadas){
						this.mostrando = false;
						this.apagando = true;
					}
				}

			}else{

				this.opacidade -= this.vopacidade * 1.5;

				if(this.opacidade <= 0){
					this.finalizado = true;
				}

			}

			context.beginPath();

			context.fillStyle = 'rgba(' + this.color + ', ' + this.opacidade + ')';

			// retângulo horizontal
			var x_inicial = this.x - 15;
			var y_inicial = this.y - 5;

			var path = new Path2D();
			path.moveTo(x_inicial, y_inicial);
			path.lineTo(x_inicial + 10, y_inicial);
			path.lineTo(x_inicial + 10, y_inicial - 10);
			path.lineTo(x_inicial + 20, y_inicial - 10);
			path.lineTo(x_inicial + 20, y_inicial);
			path.lineTo(x_inicial + 30, y_inicial);
			path.lineTo(x_inicial + 30, y_inicial + 10);
			path.lineTo(x_inicial + 20, y_inicial + 10);
			path.lineTo(x_inicial + 20, y_inicial + 20);
			path.lineTo(x_inicial + 10, y_inicial + 20);
			path.lineTo(x_inicial + 10, y_inicial + 10);
			path.lineTo(x_inicial, y_inicial + 10);
			path.closePath;

			context.fill(path);

		}
		this.delay_contador++;
	};
}

function Barra(){

	this.iniciado = false;
	this.x = 0;
	this.y = 0;
	this.vx = 0;
	this.vy = 0;
	this.color = '230,230,230';
	this.opacidade = 0;
	this.vopacidade = null;
	this.width = 0;
	this.height = 0;
	this.x_inicial = 0;
	this.y_inicial = 0;

	this.draw = function(orientacao, x_parent, y_parent){
		if(!this.iniciado){
			//this.vopacidade = CaicoCanvas._numeroAleatorioEntre(0.007, 0.015);
			this.vopacidade = 0.01;
			if(orientacao == 'horizontal'){
				this.width = CaicoCanvas._numeroAleatorioEntre(300, 600);
				this.height = 10;
				this.y = y_parent - 5;
				this.x = x_parent - canvas.width;
				this.vx = CaicoCanvas._numeroAleatorioEntre(5, 12);
				this.vy = 0;
			}else{
				this.height = CaicoCanvas._numeroAleatorioEntre(200, 400);
				this.width = 10;
				this.x = x_parent - 5;
				this.y = y_parent - canvas.height;
				this.vx = 0;
				this.vy = CaicoCanvas._numeroAleatorioEntre(5, 12);
			}
			this.iniciado = true;
		}

		context.fillStyle = 'rgba(' + this.color + ', ' + this.opacidade + ')';
		context.fillRect(this.x, this.y, this.width, this.height);

		if(orientacao == 'horizontal'){
			if(this.x <= x_parent)
				this.opacidade += this.vopacidade;
			else
				this.opacidade -= this.vopacidade;
		}else{
			if(this.y <= y_parent)
				this.opacidade += this.vopacidade;
			else
				this.opacidade -= this.vopacidade;
		}
	};

	this.mover = function(){
		this.x += this.vx;
		this.y += this.vy;
	}
}

var CaicoCanvas = {

	init : function(){
		var cruzes = [
			new Cruz(),
			new Cruz(),
			new Cruz(),
			new Cruz(),
			new Cruz(),
			new Cruz(),
		];

		CaicoCanvas.draw(cruzes);
	},

	_numeroAleatorioEntre : function(min, max){
		return Math.random()  * (max - min) + min;
	},

	_clear : function(){
		context.clearRect(0,0,canvas.width,canvas.height);
	},

	_posicionaImagens : function(){
		var marca = new Image();
		var selo = new Image();

		marca.src = 'assets/img/marca-caico.png';
		selo.src = 'assets/img/selo-10anos-v2.png';

		x_marca = (canvas.width - marca.width) / 2;
		x_selo = (canvas.width - selo.width) / 2;

		y_marca = 50;
		y_selo = 330;
		context.drawImage(marca, x_marca, y_marca);
		context.drawImage(selo, x_selo, y_selo);
	},

	_ajustarTamanhoCanvas : function(){
		$('#canvas').attr('height', window.innerHeight)
					.attr('width', window.innerWidth);
	},

	draw : function(cruzes){
		CaicoCanvas._clear();
		quadrante = 1;
		for (var i = 0; i < cruzes.length; i++) {
			if(cruzes[i].finalizado == true){
				cruzes[i] = new Cruz()
			}
			cruzes[i].draw(quadrante);
			quadrante++;
			if(quadrante > 4){
				quadrante = 1;
			}
		};

		CaicoCanvas._posicionaImagens();

		if(window.requestAnimationFrame !== undefined){
			raf = window.requestAnimationFrame(function(){
				CaicoCanvas.draw(cruzes)
			});
		}
	}
};


// Request Animation Frame Polyfill
(function() {
    var lastTime = 0;
    var vendors = ['webkit', 'moz'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame =
          window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

$('document').ready( function(){

	if(!is_medium()){

		CaicoCanvas._ajustarTamanhoCanvas();

		CaicoCanvas.init();

		$(window).on('resize', function(){
			CaicoCanvas._ajustarTamanhoCanvas();
		});

	}

});