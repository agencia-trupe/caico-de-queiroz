var Menu = {

	toggle : function(botao){
		var currentMenu = botao.hasClass('link-submenu') ? '.contemMenuPrincipal' : '.contemMenuAgenciados';
		var otherMenu = botao.hasClass('link-submenu') ? '.contemMenuAgenciados' : '.contemMenuPrincipal';

		// Se o botão clicado está ativo, desativar e esconder menu
		// Se o botão clicado está inativo, ativar e mostrar menu
		// Se o botão clicado está inativo e outro ativo, desativar outro, esconder menu aberto e mostrar menu
		if(botao.hasClass('ativo')){
			Menu.fechar(currentMenu);
		}else{

			if($('.menu .link.ativo').length)
				Menu.fechar(otherMenu);

			botao.addClass('ativo');
			Menu.abrir(currentMenu);
		}
	},

	abrir : function(classeMenu){
		$('html, body').animate({
			scrollTop : 0
		}, 30);
		$(classeMenu).removeClass('escondido');
	},

	fechar : function(classeMenu){

		if(undefined === classeMenu)
			classeMenu = '.contemMenuPrincipal, .contemMenuAgenciados';

		$('.menu .link.ativo').removeClass('ativo');
		$(classeMenu).addClass('escondido');
		setTimeout( function(){
			Menu.fecharSubSub();
			Menu.fecharSubAberto();
			Menu.centralizarSub();
			Menu._resetarAlinhamento();
		}, 300);
	},

	abrirSub : function(botao){

		var parent = botao.parent();

		// Fechar sub aberto
		Menu.fecharSubAberto();

		botao.addClass('ativo');
		Menu._animarAlinhamento();
		parent.addClass('subcategorias-aberto');
		parent.parent().addClass('subcategorias-aberto');
		setTimeout( function(){
			parent.parent().addClass('alinhamento-direita');
			Menu.ajustarPosicaoSub(parent.find('> ul'));
		}, 300);
	},

	fecharSubAberto : function(){
		Menu.fecharSubSub();
		$('.contemSubMenu .submenu > li > a.ativo').removeClass('ativo');
		$('.contemSubMenu .submenu > li.subcategorias-aberto').removeClass('subcategorias-aberto');
		$('.contemSubMenu .submenu.subcategorias-aberto').removeClass('subcategorias-aberto');
	},

	centralizarSub : function (){
		$('.contemSubMenu .submenu.alinhamento-direita').removeClass('alinhamento-direita');
	},

	_animarAlinhamento : function(){
		var itens = $('.menuAgenciados > li > a');
		itens.each( function(index){
			$(this).css('display', 'inline');
			var wParent = $(this).parent().width();
			var wItem = $(this).width();
			var wDiff = (wParent - wItem) / 2;
			$(this).css({
				'display' : 'block',
				'margin-right': wDiff + 'px'
			});
		});
	},

	_resetarAlinhamento : function(){
		var itens = $('.menuAgenciados > li > a');
		itens.each( function(index){
			$(this).css({
				'display' : 'block',
				'margin-right': '0px'
			});
		});
	},

	abrirSubSub : function(botao){

		var parent = botao.parent();

		// Fechar subsub aberto
		Menu.fecharSubSub();

		botao.addClass('ativo');
		parent.parent().addClass('overflow-visivel');
		parent.addClass('subpersonalidades-aberto');
		$('.ativo-inicial').removeClass('ativo-inicial');
	},

	fecharSubSub : function(){
		$('.submenu .subcategorias > li > a.ativo').removeClass('ativo');
		$('.submenu .subcategorias.overflow-visivel').removeClass('overflow-visivel');
		$('.submenu .subcategorias > li.subpersonalidades-aberto').removeClass('subpersonalidades-aberto');
	},

	ajustarPosicaoSub : function(sub){
		if(sub.offset() !== undefined){
			var altura_viewport = parseInt($('body').css('height')) - 40;
			var posicao = sub.offset().top;
			var children = sub.hasClass('subtodomundo') ? sub.find('> ul > li') : sub.find('> li');
			var altura_individual = parseInt(children.first().outerHeight(true));
			var margens = parseInt(sub.css('margin-top')) + parseInt(sub.css('margin-bottom'));
			var padding = parseInt(sub.css('padding-top')) + parseInt(sub.css('padding-bottom'));
			var altura_sub = padding + margens + (children.length * altura_individual);
			var posicao_final = posicao + altura_sub;
			var delta = (posicao_final - altura_viewport) * 1;

			if(delta > 0){

				if(sub.hasClass('subtodomundo')){
					if(!sub.hasClass('posisicionado')){

						sub.animate({
							top : ((posicao + 20) * -1)
						}, 300);

						sub.css('max-height', altura_viewport - 80)
							 .css('overflow', 'hidden');

								myScroll = new IScroll('.subtodomundo', {
									mouseWheel : true,
									scrollbars : true
								});

						sub.addClass('posisicionado');
					}

				}else{
					sub.animate({
						top : '-='+delta
					}, 300);
				}

			}

		}
	},

};

var myScroll;

function ajustarColunas(){

	if(is_medium('portrait') || is_small('any'))
		nro_colunas = 2;
	else
		nro_colunas = 4;

	var nro_modulos = $('.contemModulos .personalidades-modulo').length;
	var largura_modulo = (parseInt($('body').css('width')) / nro_colunas);
	var largura_scroller = largura_modulo * nro_modulos;

	$('.contemModulos .personalidades-modulo').css('width', largura_modulo + 'px');
	$('.contemModulos').css('width', largura_scroller + 'px');

	myScroll = new IScroll('.escondeOverflow', {
		mouseWheel : true,
		scrollX    : true,
		scrollY    : false,
		scrollbars : false,
		click      : true
	});
}

function is_small(orientation){

	var BODY_largura = parseInt($('body').css('width'));
	var BODY_altura = parseInt($('body').css('height'));

	var is_small_landscape = (BODY_largura <= 667) && (BODY_largura >= BODY_altura);
	var is_small_portrait = (BODY_largura <= 375) && (BODY_largura < BODY_altura);

	if('any' == orientation)
		return is_small_landscape || is_small_portrait;
	if('landscape' == orientation)
		return is_small_landscape;
	if('portrait' == orientation)
		return is_small_portrait;
	else
		return is_small_landscape && is_small_portrait;
}

function is_medium(orientation){

	var BODY_largura = parseInt($('body').css('width'));
	var BODY_altura = parseInt($('body').css('height'));

	var is_medium_landscape = (BODY_largura >= 668) && (BODY_largura <= 1024) && (BODY_largura >= BODY_altura);
	var is_medium_portrait = (BODY_largura >= 376) && (BODY_largura <= 769) && (BODY_largura < BODY_altura);

	if('any' == orientation)
		return is_medium_landscape || is_medium_portrait;
	if('landscape' == orientation)
		return is_medium_landscape;
	if('portrait' == orientation)
		return is_medium_portrait;
	else
		return is_medium_landscape && is_medium_portrait;
}

function redimensionarTitulo(){
	if(is_small('landscape'))
		$('#modulo-base h1').fitText(1);
	else if(is_small('portrait'))
		$('#modulo-base h1').fitText(0.7);
	else
		$('#modulo-base h1').fitText(0.6);
}

$('document').ready( function(){

	$('.menu .link').on('click', function(e){
		e.preventDefault();
		Menu.toggle($(this));
	});

	$('.contemSubMenu').on('mousemove', function(e){
		$('.contemSubMenu').css('background-position', (e.clientX - 50) + 'px ' + (e.clientY - 50) + 'px');
	});

	$('.contemSubMenu').on('click', function(e){
		Menu.fechar();
	});

	$('.contemMenuAgenciados .submenu > li > a').on('click', function(e){
		e.stopPropagation();
		e.preventDefault();
		Menu.abrirSub($(this));
	});

	$('.contemMenuAgenciados .submenu .subcategorias:not(.subpersonalidades) > li > a').on('click', function(e){
		e.stopPropagation();
		e.preventDefault();
		Menu.abrirSubSub($(this));
	});

	$('.contemSubMenu .submenu').on('click', function(e){
		e.stopPropagation();
	});

	$('.contemSubMenu .submenu').on('mousemove', function(e){
		e.stopPropagation();
		$('.contemSubMenu').css('background-position', '-100px -100px');
	});

	$('.contemSubMenu').on('mouseout', function(e){
		$('.contemSubMenu').css('background-position', '-100px -100px');
	});

	if($('#modulo-paralaxe').length){

		var scroll_paralaxe_Y = $('#modulo-paralaxe').offset().top;
		var velocidade_opacidade = 800;
		var scroll_paralaxe = new IScroll('#camada-paralaxe', {
	 		mouseWheel : true,
	 		scrollX    : false,
	 		scrollY    : true,
	 		scrollbars : true,
	 		probeType  : 3,
	 		click      : true
	 	});

	 	$('#camada-paralaxe .paralaxe-move').stellar({
	 		scrollProperty: 'transform',
			horizontalScrolling: false,
			responsive: true
		});

		$(document).on('click', '#paralaxe-scroll', function(e){
			e.preventDefault();

			scroll_paralaxe.scrollTo(0, -scroll_paralaxe_Y, 1000, IScroll.utils.ease.quadratic);

			$.each($('.imagem-paralaxe img'), function(){
				$(this).animate({
					opacity : 1
				}, velocidade_opacidade, 'swing');
				velocidade_opacidade = Math.floor(Math.random() * 400) + 1020;
			});

			$('#camada-nome .nome .ver-detalhes-img').animate({
				opacity : 1
			}, Math.floor(Math.random() * 400) + 1020, 'swing');
		});

		scroll_paralaxe.on('scroll', function(){
			var incremento = parseFloat(1/scroll_paralaxe_Y) * parseFloat(-scroll_paralaxe.y);
			$('.imagem-paralaxe img').css('opacity', incremento);
			$('#camada-nome .nome .ver-detalhes-img').css('opacity', incremento);
		});
	}

	// Scroll dos Textos
	if($('.contemTextoImagem section').length){
		var altura_texto = parseInt($('.contemTextoImagem section .padding-interno').css('height'));
		var altura_container = parseInt($('.contemTextoImagem section').css('height'));

		if(altura_texto > altura_container){
			var scrollTexto = new IScroll('.contemTextoImagem section', {
		 		mouseWheel     : true,
		 		scrollX        : false,
		 		scrollY        : true,
		 		scrollbars     : true,
		 		fadeScrollbars : true
		 	});
		}
	}

	// Scroll dos Contato
	if($('.contemContato .formContato').length){
		var altura_texto = parseInt($('.contemContato .formContato .padding-interno').css('height'));
		var altura_container = parseInt($('.contemContato .formContato').css('height'));

		if(altura_texto > altura_container){
			var scrollContato = new IScroll('.contemContato .formContato', {
		 		mouseWheel     : true,
		 		scrollX        : false,
		 		scrollY        : true,
		 		scrollbars     : true,
		 		fadeScrollbars : true
		 	});
		}
	}

	// Scroll das imagens
	if($('.asideImagens').length){
		var altura_aside = parseInt($('aside.asideImagens').css('height'));
		var altura_imgs = parseInt($('aside.asideImagens .listaImagens').css('height'));

		if(!is_small('any')){
			if(altura_imgs > (altura_aside + 40)){
				var scrollTexto = new IScroll('aside.asideImagens', {
			 		mouseWheel : true,
			 		scrollX    : false,
			 		scrollY    : true,
			 		scrollbars : false
			 	});
			}
		}
	}

	if($('.contemModulos').length){

		var scrollModulo = new Array();

		ajustarColunas();

		redimensionarTitulo();

		$(window).on('orientationchange', function(){
			ajustarColunas();
			redimensionarTitulo();
		});

		$.each($('.personalidades-modulo'), function() {
			var id = $(this).attr('id');
			var scrollable = parseInt($(this).css('height')) < parseInt($(this).find(">:first-child").css('height')) + 40;
			if(scrollable){
				imagesLoaded($('#'+id), function(){
				 	scrollModulo.push(new IScroll('#'+id, {
				 		mouseWheel : true,
				 		scrollX    : false,
				 		scrollY    : true,
				 		scrollbars : true,
				 		fadeScrollbars : true
				 	}));
				});
			}
		});

		if($('.personalidades-modulo').length > 4){

			$('#seta-navegacao').fadeIn({
				duration : 'normal',
				easing : 'swing',
			    done : function(){
			    	$('#seta-navegacao').click( function(e){
						e.preventDefault();
						var largura_modulo = parseInt($('.personalidades-modulo:first').css('width'));
						myScroll.scrollBy(-largura_modulo * 2, 0, 900, IScroll.utils.ease.back);
					});
			    }
			});
		}

		if($('.listaVideos')){
			$('.video').on('click', function(){
				var embed = $(this).find('img').attr('data-embed');
				$(this).addClass('videoAtivo').html(embed);
			});
		}

		if($('.icones-redes-sociais').length){
			$('.icones-redes-sociais .icone').on('click', function(e){
				e.preventDefault();

				var tab = $(this).attr('href');

				$('.icones-redes-sociais .icone.ativo').removeClass('ativo');
				$(this).addClass('ativo');

				$('.tab-social.tab-ativa, .tab-social.ativo').fadeOut('fast', function(){
					setTimeout( function(){
						$('.lista-redes-sociais '+tab).addClass('tab-ativa').fadeIn('normal');
					}, 200);
				});

			});
		}

		if($('.fancybox-imagem').length){
			$('.fancybox-imagem').fancybox({
				titleShow: false,
				helpers:  {
			        title:  null
			    }
			});
		}
	}

});
