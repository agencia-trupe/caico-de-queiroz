var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

jQuery(function($){
    $.datepicker.regional['pt-BR'] = {
        closeText: 'Fechar',
        prevText: '&#x3c;Anterior',
        nextText: 'Pr&oacute;ximo&#x3e;',
        currentText: 'Hoje',
        monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
        'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
        'Jul','Ago','Set','Out','Nov','Dez'],
        dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
        dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 0,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
});

function iconeUploadStart(scope){
    $('.multiUpload .icone .glyphicon-open', scope).css('opacity', 0);
    setTimeout( function(){
        $('.multiUpload .icone .glyphicon-refresh', scope).css({'display' : 'block', 'opacity': 1});
        $('.multiUpload .icone .glyphicon-open', scope).css('display', 'none');
    }, 350);
}

function iconeUploadStop(scope){
    $('.multiUpload .icone .glyphicon-refresh', scope).css('opacity', 0);
    setTimeout( function(){
        $('.multiUpload .icone .glyphicon-open', scope).css({'display' : 'block', 'opacity': 1});
        $('.multiUpload .icone .glyphicon-refresh', scope).css('display', 'none');
    }, 350);
}

function controlaUploadImagens(limite, scope){
    if(maximoImagensAtingido(limite, scope))
        esconderUploadImagens(0, scope);
    else
        mostrarUploadImagens(0, scope);
}

function maximoImagensAtingido(limite, scope){
    // se limite == 0, não limitar a quantidade de imagens
    return limite == 0 ? false : $('.projetoImagem', scope).length >= limite;
}

function esconderUploadImagens(speed, scope){
    $('.multiUpload', scope).hide(speed);
    $('.limiteImagensAtingido', scope).show(speed);
}

function mostrarUploadImagens(speed, scope){
    $('.multiUpload', scope).show(speed);
    $('.limiteImagensAtingido', scope).hide(speed);
}

function esconderSelectSubcategorias(speed){
    $('#contemSelectSubcategorias select').attr('required', false);
    $('#contemSelectSubcategorias').hide(speed);
}

function mostrarSelectSubcategorias(speed){
    $('#contemSelectSubcategorias select').attr('required', true);
    $('#contemSelectSubcategorias').show(speed);
}

function popularSelectSubcategorias(retorno){
    var options_str = "<option value=''>Selecione uma Categoria</option>";
    $.each(retorno, function(){
        options_str += '<option value='+$(this)[0].id+'>'+$(this)[0].titulo+'</option>';
    });
    $('#inputSubcategoria').html(options_str);
    marcarSelectSubcategorias();
}

function marcarSelectSubcategorias(){
    var campo = $('#portfolio_subcategorias_id_selecionado');
    if(campo.length && campo.val())
        $('#inputSubcategoria').val(campo.val())
}

function getSubcategorias(cat_id){
    $.post('getSubcategorias',
    {
        _token: CSRF_TOKEN,
        cat_id : cat_id
    }, function(retorno){
        popularSelectSubcategorias(retorno);
        if(retorno.length > 0)
            mostrarSelectSubcategorias('fast');
        else
            esconderSelectSubcategorias('fast');
    });
}

function busca(termo){
    $("table").find("tr").each(function(index) {
        if (!index) return;

        var campo_nome = $(this).find("td.celula-nome").first().text();
        var campo_cat = $(this).find("td.celula-cat").first().text();
        var campo_subcat = $(this).find("td.celula-subcat").first().text();
        var str_compare = campo_nome + ' ' + campo_cat + ' ' + campo_subcat;
        str_compare = str_compare.toLowerCase();
        $(this).toggle(str_compare.indexOf( termo.toLowerCase() ) !== -1);
    });
}

$('document').ready( function(){

    // Controle de input de Categorias
    if($('#inputCategoria').length){

        $('#inputCategoria').change( function(){
            getSubcategorias($(this).val());
        });

        var categoria_selecionada = $('#inputCategoria').val();
        getSubcategorias(categoria_selecionada);
    }

    // Controle de botões de módulos
    $('.area-modulo').on('hide.bs.collapse', function (e) {
        $(this).prev('input[type=hidden]').val('0');
        //$(this).prev().prev('h4').find('a.btn-ativar-modulo').addClass('btn-default').removeClass('btn-info');
        $(this).prev().prev('h4').find('a.btn-ativar-modulo').html("adicionar <span class='glyphicon glyphicon-triangle-bottom'></span>");
    });
    $('.area-modulo').on('show.bs.collapse', function (e) {
        $(this).prev('input[type=hidden]').val('1');
        $(this).prev().prev('h4').find('a.btn-ativar-modulo').html("remover <span class='glyphicon glyphicon-triangle-top'></span>");
    });

    // Gerenciamento do plugin de videos
    $('#submitVideo').click( function(e){
        e.preventDefault();
        var video_titulo = $('#inputVideoTitulo').val();
        var video_url = $('#inputVideoUrl').val();
        var video_id = $('#inputVideoId').val();

        if(video_titulo == ''){
            var formgroup = $('#inputVideoTitulo').parent().parent()
            formgroup.addClass('has-error has-feedback');
            formgroup.find('input').after("<span class='glyphicon glyphicon-remove form-control-feedback'></span>");
            setTimeout( function(){
                formgroup.removeClass('has-error').removeClass('has-feedback').find('.form-control-feedback').remove();
            }, 3000);
            return false;
        }


        if(video_url == ''){
            var formgroup = $('#inputVideoUrl').parent().parent()
            formgroup.addClass('has-error has-feedback');
            formgroup.find('input').after("<span class='glyphicon glyphicon-remove form-control-feedback'></span>");
            setTimeout( function(){
                formgroup.removeClass('has-error').removeClass('has-feedback').find('.form-control-feedback').remove();
            }, 3000);
            return false;
        }

        $('#inputVideoTitulo').val('');
        $('#inputVideoUrl').val('');
        $('#inputVideoId').val('');

        var li_html = "";
        li_html += "<li class='list-group-item'>";
            li_html += "<a href='"+video_url+"' class='btn btn-link btn-sm' target='_blank'>"+video_titulo+"</a>";
            li_html += "<div class='btn-group pull-right'><a href='#' class='btn btn-sm btn-info btn-alterar-video'>alterar</a>";
            li_html += "<a href='#' class='btn btn-sm btn-danger btn-remover-video'>remover</a></div>";
            li_html += "<input type='hidden' name='videos_id[]' value='"+video_id+"'>";
            li_html += "<input type='hidden' name='videos_titulo[]' value='"+video_titulo+"'>";
            li_html += "<input type='hidden' name='videos_url[]' value='"+video_url+"'>";
        li_html += "</li>";

        if($('.toRemove').length){
            $('.toRemove').replaceWith(li_html);
            $('.toRemove').removeClass('toRemove');
        }else{
            $('.listaVideos').append(li_html);
        }
        $('#submitVideo').html('Adicionar <span class=\'glyphicon glyphicon-plus\'></span>');
        $('#cancelarEdicaoVideo').hide('fast');
    });

    $(document).on('click', '.btn-alterar-video', function(e){
        e.preventDefault();

        var parent = $(this).parent().parent();
        var titulo = parent.find("input[name='videos_titulo[]']").val();
        var url = parent.find("input[name='videos_url[]']").val();
        var id = parent.find("input[name='videos_id[]']").val();

        $('#inputVideoTitulo').val(titulo);
        $('#inputVideoUrl').val(url);
        $('#inputVideoId').val(id);
        $('#cancelarEdicaoVideo').show('fast');
        $('#submitVideo').html('Salvar <span class=\'glyphicon glyphicon-plus\'></span>');

        parent.addClass('toRemove');
    });

    $(document).on('click', '#cancelarEdicaoVideo', function(e){
        e.preventDefault();
        $('#inputVideoTitulo').val('');
        $('#inputVideoUrl').val('');
        $('.toRemove').removeClass('toRemove');
        $('#submitVideo').html('Adicionar <span class=\'glyphicon glyphicon-plus\'></span>');
        $('#cancelarEdicaoVideo').hide('fast');
    });

    $(document).on('click', '.btn-remover-video', function(e){
        e.preventDefault();
        $(this).parent().parent().hide('normal', function(){
            $(this).remove();
        });
    });

    $('.listaVideos').sortable().disableSelection();

    // Gerenciamento do plugin de Ações sociais
    $('#submitAcaoSocial').click( function(e){
        e.preventDefault();
        var acaosocial_titulo = $('#inputAcaoSocialTitulo').val();
        var acaosocial_texto = $('#inputAcaoSocialTexto').val();
        //var acaosocial_imagem = $('#inputAcaoSocialImagem').val();

        if(acaosocial_titulo == ''){
            var formgroup = $('#inputAcaoSocialTitulo').parent().parent()
            formgroup.addClass('has-error has-feedback');
            formgroup.find('input').after("<span class='glyphicon glyphicon-remove form-control-feedback'></span>");
            setTimeout( function(){
                formgroup.removeClass('has-error').removeClass('has-feedback').find('.form-control-feedback').remove();
            }, 3000);
            return false;
        }

        if(acaosocial_texto == ''){
            var formgroup = $('#inputAcaoSocialTexto').parent().parent()
            formgroup.addClass('has-error has-feedback');
            formgroup.find('input').after("<span class='glyphicon glyphicon-remove form-control-feedback'></span>");
            setTimeout( function(){
                formgroup.removeClass('has-error').removeClass('has-feedback').find('.form-control-feedback').remove();
            }, 3000);
            return false;
        }

        if(imagemUrlRetornoAcaoSocial == ''){
            var formgroup = $('#inputAcaoSocialImagem').parent().parent()
            formgroup.addClass('has-error has-feedback');
            formgroup.find('input').after("<span class='glyphicon glyphicon-remove form-control-feedback'></span>");
            setTimeout( function(){
                formgroup.removeClass('has-error').removeClass('has-feedback').find('.form-control-feedback').remove();
            }, 3000);
            return false;
        }

        $('#inputAcaoSocialTitulo').val('');
        $('#inputAcaoSocialTexto').val('');

        // subir imagem, retornar o caminho e o titulo
        // acaosocial_imagem = ?
        var li_html = "";
        li_html += "<li class='list-group-item'>";
            li_html += "<div class='row'>";
                li_html += "<div class='col-sm-2'>";
                    li_html += "<img src='assets/img/portfolio/acaosocial/thumbs/"+imagemUrlRetornoAcaoSocial+"' style='max-width:100%;'>";
                li_html += "</div>";
                li_html += "<div class='col-sm-7'>";
                    li_html += "<h5>"+acaosocial_titulo+"</h5>";
                    li_html += "<p>"+acaosocial_texto+"</p>";
                li_html += "</div>";
                li_html += "<div class='col-sm-3'>";
                    li_html += "<div class='btn-group'>";
                        li_html += "<a href='#' class='btn btn-xs btn-info btn-alterar-acaosocial'>alterar</a>";
                        li_html += "<a href='#' class='btn btn-xs btn-danger btn-remover-acaosocial'>remover</a>";
                    li_html += "</div>";
                li_html += "</div>";
            li_html += "</div>";
            li_html += "<input type='hidden' name='acaosocial_titulo[]' value='"+acaosocial_titulo+"'>";
            li_html += "<input type='hidden' name='acaosocial_texto[]' value='"+acaosocial_texto+"'>";
            li_html += "<input type='hidden' name='acaosocial_imagem[]' value='"+imagemUrlRetornoAcaoSocial+"'>";
        li_html += "</li>";

        if($('.toRemove').length){
            $('.toRemove').replaceWith(li_html);
            $('.toRemove').removeClass('toRemove');
        }else{
            $('.listaAcoesSociais').append(li_html);
        }
        $('#submitAcaoSocial').html('Adicionar <span class=\'glyphicon glyphicon-plus\'></span>');
        $('#imagem-placeholder').html('');
        imagemUrlRetornoAcaoSocial = '';
        $('#cancelarEdicaoAcaoSocial').hide('fast');
    });

    var imagemUrlRetornoAcaoSocial = '';

    $('#inputAcaoSocialImagem').fileupload({
        dataType : 'json',
        type     : 'post',
        formData: [{
            name  : 'path',
            value : 'portfolio/acaosocial/'
        }],
        start: function (){
            $('#imagem-placeholder').html("<img src='assets/img/ajax-loader.gif' style='margin:20px 0;'>");
            $('#submitAcaoSocial').attr('disabled', 'disabled');
        },
        done: function (e, data) {
            imagemUrlRetornoAcaoSocial = data.result.filename;
            $('#imagem-placeholder').html("<img style='max-width:30%;' src='assets/img/portfolio/acaosocial/thumbs/"+imagemUrlRetornoAcaoSocial+"'>");
            $('#submitAcaoSocial').attr('disabled', false);
        }
    });

    $(document).on('click', '.btn-alterar-acaosocial', function(e){
        e.preventDefault();

        var parent = $(this).parent().parent().parent().parent();
        var titulo = parent.find("input[name='acaosocial_titulo[]']").val();
        var texto = parent.find("input[name='acaosocial_texto[]']").val();
        var imagem = parent.find("input[name='acaosocial_imagem[]']").val();

        $('#inputAcaoSocialTitulo').val(titulo);
        $('#inputAcaoSocialTexto').val(texto);
        $('#imagem-placeholder').html("<img style='max-width:30%;' src='assets/img/portfolio/acaosocial/thumbs/"+imagem+"'>");
        imagemUrlRetornoAcaoSocial = imagem;
        $('#cancelarEdicaoAcaoSocial').show('fast');
        $('#submitAcaoSocial').html('Salvar <span class=\'glyphicon glyphicon-plus\'></span>');

        parent.addClass('toRemove');
    });

    $(document).on('click', '#cancelarEdicaoAcaoSocial', function(e){
        e.preventDefault();
        $('#inputAcaoSocialTitulo').val('');
        $('#inputAcaoSocialTexto').val('');
        $('#imagem-placeholder').html('');
        $('.toRemove').removeClass('toRemove');
        imagemUrlRetornoAcaoSocial = '';
        $('#submitAcaoSocial').html('Adicionar <span class=\'glyphicon glyphicon-plus\'></span>');
        $('#cancelarEdicaoAcaoSocial').hide('fast');
    });

    $(document).on('click', '.btn-remover-acaosocial', function(e){
        e.preventDefault();
        $(this).parent().parent().parent().parent().hide('normal', function(){
            $(this).remove();
        });
    });

    $('.listaAcoesSociais').sortable().disableSelection();

    // Gerenciamento do plugin de Publicações
    $('#submitPublicacoes').click( function(e){
        e.preventDefault();
        var publicacoes_titulo = $('#inputPublicacoesTitulo').val();
        var publicacoes_autor = $('#inputPublicacoesAutor').val();
        var publicacoes_editora = $('#inputPublicacoesEditora').val();
        var publicacoes_texto = $('#inputPublicacoesTexto').val();

        if(publicacoes_titulo == ''){
            var formgroup = $('#inputPublicacoesTitulo').parent().parent()
            formgroup.addClass('has-error has-feedback');
            formgroup.find('input').after("<span class='glyphicon glyphicon-remove form-control-feedback'></span>");
            setTimeout( function(){
                formgroup.removeClass('has-error').removeClass('has-feedback').find('.form-control-feedback').remove();
            }, 3000);
            return false;
        }

        if(imagemUrlRetornoPublicacoes == ''){
            var formgroup = $('#inputPublicacoesImagem').parent().parent()
            formgroup.addClass('has-error has-feedback');
            formgroup.find('input').after("<span class='glyphicon glyphicon-remove form-control-feedback'></span>");
            setTimeout( function(){
                formgroup.removeClass('has-error').removeClass('has-feedback').find('.form-control-feedback').remove();
            }, 3000);
            return false;
        }

        $('#inputPublicacoesTitulo').val('');
        $('#inputPublicacoesAutor').val('');
        $('#inputPublicacoesEditora').val('');
        $('#inputPublicacoesTexto').val('');

        // subir imagem, retornar o caminho e o titulo
        var li_html = "";
        li_html += "<li class='list-group-item'>";
            li_html += "<div class='row'>";
                li_html += "<div class='col-sm-2'>";
                    li_html += "<img src='assets/img/portfolio/publicacoes/thumbs/"+imagemUrlRetornoPublicacoes+"' style='max-width:100%;'>";
                li_html += "</div>";
                li_html += "<div class='col-sm-7'>";
                    li_html += "<h5><strong>"+publicacoes_titulo+"</strong></h5>";
                    li_html += "<p><strong>Autor</strong> : "+publicacoes_autor+"<br><strong>Editora</strong> : "+publicacoes_editora+"</p>";
                    li_html += "<p>"+publicacoes_texto+"</p>";
                li_html += "</div>";
                li_html += "<div class='col-sm-3'>";
                    li_html += "<div class='btn-group'>";
                        li_html += "<a href='#' class='btn btn-xs btn-info btn-alterar-publicacoes'>alterar</a>";
                        li_html += "<a href='#' class='btn btn-xs btn-danger btn-remover-publicacoes'>remover</a>";
                    li_html += "</div>";
                li_html += "</div>";
            li_html += "</div>";
            li_html += "<input type='hidden' name='publicacoes_titulo[]' value='"+publicacoes_titulo+"'>";
            li_html += "<input type='hidden' name='publicacoes_editora[]' value='"+publicacoes_editora+"'>";
            li_html += "<input type='hidden' name='publicacoes_autor[]' value='"+publicacoes_autor+"'>";
            li_html += "<input type='hidden' name='publicacoes_texto[]' value='"+publicacoes_texto+"'>";
            li_html += "<input type='hidden' name='publicacoes_imagem[]' value='"+imagemUrlRetornoPublicacoes+"'>";
        li_html += "</li>";

        if($('.toRemove-publi').length){
            $('.toRemove-publi').replaceWith(li_html);
            $('.toRemove-publi').removeClass('toRemove-publi');
        }else{
            $('.listaPublicacoes').append(li_html);
        }
        $('#submitPublicacoes').html('Adicionar <span class=\'glyphicon glyphicon-plus\'></span>');
        $('#imagem-placeholder-publicacoes').html('');
        imagemUrlRetornoPublicacoes = '';
        $('#cancelarEdicaoPublicacoes').hide('fast');
    });

    var imagemUrlRetornoPublicacoes = '';

    $('#inputPublicacoesImagem').fileupload({
        dataType : 'json',
        type     : 'post',
        formData: [{
            name  : 'path',
            value : 'portfolio/publicacoes/'
        }],
        start: function (){
            $('#imagem-placeholder-publicacoes').html("<img src='assets/img/ajax-loader.gif' style='margin:20px 0;'>");
            $('#submitPublicacoes').attr('disabled', 'disabled');
        },
        done: function (e, data) {
            imagemUrlRetornoPublicacoes = data.result.filename;
            $('#imagem-placeholder-publicacoes').html("<img style='max-width:30%;' src='assets/img/portfolio/publicacoes/thumbs/"+imagemUrlRetornoPublicacoes+"'>");
            $('#submitPublicacoes').attr('disabled', false);
        }
    });

    $(document).on('click', '.btn-alterar-publicacoes', function(e){
        e.preventDefault();

        var parent = $(this).parent().parent().parent().parent();
        var titulo = parent.find("input[name='publicacoes_titulo[]']").val();
        var autor = parent.find("input[name='publicacoes_autor[]']").val();
        var editora = parent.find("input[name='publicacoes_editora[]']").val();
        var texto = parent.find("input[name='publicacoes_texto[]']").val();
        var imagem = parent.find("input[name='publicacoes_imagem[]']").val();

        $('#inputPublicacoesTitulo').val(titulo);
        $('#inputPublicacoesAutor').val(autor);
        $('#inputPublicacoesEditora').val(editora);
        $('#inputPublicacoesTexto').val(texto);

        $('#imagem-placeholder-publicacoes').html("<img style='max-width:30%;' src='assets/img/portfolio/publicacoes/thumbs/"+imagem+"'>");
        imagemUrlRetornoPublicacoes = imagem;
        $('#cancelarEdicaoPublicacoes').show('fast');
        $('#submitPublicacoes').html('Salvar <span class=\'glyphicon glyphicon-plus\'></span>');
        $('.toRemove-publi').removeClass('toRemove-publi');
        parent.addClass('toRemove-publi');
    });

    $(document).on('click', '#cancelarEdicaoPublicacoes', function(e){
        e.preventDefault();
        $('#inputPublicacoesTitulo').val('');
        $('#inputPublicacoesAutor').val('');
        $('#inputPublicacoesEditora').val('');
        $('#inputPublicacoesTexto').val('');
        $('#imagem-placeholder-publicacoes').html('');
        $('.toRemove-publi').removeClass('toRemove-publi');
        imagemUrlRetornoPublicacoes = '';
        $('#submitPublicacoes').html('Adicionar <span class=\'glyphicon glyphicon-plus\'></span>');
        $('#cancelarEdicaoPublicacoes').hide('fast');
    });

    $(document).on('click', '.btn-remover-publicacoes', function(e){
        e.preventDefault();
        $(this).parent().parent().parent().parent().hide('normal', function(){
            $(this).remove();
        });
    });

    $('.listaPublicacoes').sortable().disableSelection();

    // Gerenciamento do plugin de Espetáculos
    $('#submitEspetaculos').click( function(e){
        e.preventDefault();
        var espetaculos_titulo = $('#inputEspetaculosTitulo').val();
        var espetaculos_autor = $('#inputEspetaculosAutor').val();
        var espetaculos_editora = $('#inputEspetaculosEditora').val();
        var espetaculos_texto = $('#inputEspetaculosTexto').val();

        if(espetaculos_titulo == ''){
            var formgroup = $('#inputEspetaculosTitulo').parent().parent()
            formgroup.addClass('has-error has-feedback');
            formgroup.find('input').after("<span class='glyphicon glyphicon-remove form-control-feedback'></span>");
            setTimeout( function(){
                formgroup.removeClass('has-error').removeClass('has-feedback').find('.form-control-feedback').remove();
            }, 3000);
            return false;
        }

        if(imagemUrlRetornoEspetaculos == ''){
            var formgroup = $('#inputEspetaculosImagem').parent().parent()
            formgroup.addClass('has-error has-feedback');
            formgroup.find('input').after("<span class='glyphicon glyphicon-remove form-control-feedback'></span>");
            setTimeout( function(){
                formgroup.removeClass('has-error').removeClass('has-feedback').find('.form-control-feedback').remove();
            }, 3000);
            return false;
        }

        $('#inputEspetaculosTitulo').val('');
        $('#inputEspetaculosAutor').val('');
        $('#inputEspetaculosEditora').val('');
        $('#inputEspetaculosTexto').val('');

        // subir imagem, retornar o caminho e o titulo
        var li_html = "";
        li_html += "<li class='list-group-item'>";
            li_html += "<div class='row'>";
                li_html += "<div class='col-sm-2'>";
                    li_html += "<img src='assets/img/portfolio/espetaculos/thumbs/"+imagemUrlRetornoEspetaculos+"' style='max-width:100%;'>";
                li_html += "</div>";
                li_html += "<div class='col-sm-7'>";
                    li_html += "<h5><strong>"+espetaculos_titulo+"</strong></h5>";
                    li_html += "<p><strong>Autor</strong> : "+espetaculos_autor+"<br><strong>Editora</strong> : "+espetaculos_editora+"</p>";
                    li_html += "<p>"+espetaculos_texto+"</p>";
                li_html += "</div>";
                li_html += "<div class='col-sm-3'>";
                    li_html += "<div class='btn-group'>";
                        li_html += "<a href='#' class='btn btn-xs btn-info btn-alterar-espetaculos'>alterar</a>";
                        li_html += "<a href='#' class='btn btn-xs btn-danger btn-remover-espetaculos'>remover</a>";
                    li_html += "</div>";
                li_html += "</div>";
            li_html += "</div>";
            li_html += "<input type='hidden' name='espetaculos_titulo[]' value='"+espetaculos_titulo+"'>";
            li_html += "<input type='hidden' name='espetaculos_editora[]' value='"+espetaculos_editora+"'>";
            li_html += "<input type='hidden' name='espetaculos_autor[]' value='"+espetaculos_autor+"'>";
            li_html += "<input type='hidden' name='espetaculos_texto[]' value='"+espetaculos_texto+"'>";
            li_html += "<input type='hidden' name='espetaculos_imagem[]' value='"+imagemUrlRetornoEspetaculos+"'>";
        li_html += "</li>";

        if($('.toRemove-espet').length){
            $('.toRemove-espet').replaceWith(li_html);
            $('.toRemove-espet').removeClass('toRemove-espet');
        }else{
            $('.listaEspetaculos').append(li_html);
        }
        $('#submitEspetaculos').html('Adicionar <span class=\'glyphicon glyphicon-plus\'></span>');
        $('#imagem-placeholder-espetaculos').html('');
        imagemUrlRetornoEspetaculos = '';
        $('#cancelarEdicaoEspetaculos').hide('fast');
    });

    var imagemUrlRetornoEspetaculos = '';

    $('#inputEspetaculosImagem').fileupload({
        dataType : 'json',
        type     : 'post',
        formData: [{
            name  : 'path',
            value : 'portfolio/espetaculos/'
        }],
        start: function (){
            $('#imagem-placeholder-espetaculos').html("<img src='assets/img/ajax-loader.gif' style='margin:20px 0;'>");
            $('#submitEspetaculos').attr('disabled', 'disabled');
        },
        done: function (e, data) {
            imagemUrlRetornoEspetaculos = data.result.filename;
            $('#imagem-placeholder-espetaculos').html("<img style='max-width:30%;' src='assets/img/portfolio/espetaculos/thumbs/"+imagemUrlRetornoEspetaculos+"'>");
            $('#submitEspetaculos').attr('disabled', false);
        }
    });

    $(document).on('click', '.btn-alterar-espetaculos', function(e){
        e.preventDefault();

        var parent = $(this).parent().parent().parent().parent();
        var titulo = parent.find("input[name='espetaculos_titulo[]']").val();
        var autor = parent.find("input[name='espetaculos_autor[]']").val();
        var editora = parent.find("input[name='espetaculos_editora[]']").val();
        var texto = parent.find("input[name='espetaculos_texto[]']").val();
        var imagem = parent.find("input[name='espetaculos_imagem[]']").val();

        $('#inputEspetaculosTitulo').val(titulo);
        $('#inputEspetaculosAutor').val(autor);
        $('#inputEspetaculosEditora').val(editora);
        $('#inputEspetaculosTexto').val(texto);

        $('#imagem-placeholder-espetaculos').html("<img style='max-width:30%;' src='assets/img/portfolio/espetaculos/thumbs/"+imagem+"'>");
        imagemUrlRetornoEspetaculos = imagem;
        $('#cancelarEdicaoEspetaculos').show('fast');
        $('#submitEspetaculos').html('Salvar <span class=\'glyphicon glyphicon-plus\'></span>');
        $('.toRemove-espet').removeClass('toRemove-espet');
        parent.addClass('toRemove-espet');
    });

    $(document).on('click', '#cancelarEdicaoEspetaculos', function(e){
        e.preventDefault();
        $('#inputEspetaculosTitulo').val('');
        $('#inputEspetaculosAutor').val('');
        $('#inputEspetaculosEditora').val('');
        $('#inputEspetaculosTexto').val('');
        $('#imagem-placeholder-espetaculos').html('');
        $('.toRemove-espet').removeClass('toRemove-espet');
        imagemUrlRetornoEspetaculos = '';
        $('#submitEspetaculos').html('Adicionar <span class=\'glyphicon glyphicon-plus\'></span>');
        $('#cancelarEdicaoEspetaculos').hide('fast');
    });

    $(document).on('click', '.btn-remover-espetaculos', function(e){
        e.preventDefault();
        $(this).parent().parent().parent().parent().hide('normal', function(){
            $(this).remove();
        });
    });

    $('.listaEspetaculos').sortable().disableSelection();

    // Gerenciamento do plugin de Workshops
    $('#submitWorkshops').click( function(e){
        e.preventDefault();
        var workshops_titulo = $('#inputWorkshopsTitulo').val();
        var workshops_autor = $('#inputWorkshopsAutor').val();
        var workshops_editora = $('#inputWorkshopsEditora').val();
        var workshops_texto = $('#inputWorkshopsTexto').val();

        if(workshops_titulo == ''){
            var formgroup = $('#inputWorkshopsTitulo').parent().parent()
            formgroup.addClass('has-error has-feedback');
            formgroup.find('input').after("<span class='glyphicon glyphicon-remove form-control-feedback'></span>");
            setTimeout( function(){
                formgroup.removeClass('has-error').removeClass('has-feedback').find('.form-control-feedback').remove();
            }, 3000);
            return false;
        }

        /*****************************
        * Imagem Não obrigatória
        ******************************/

        // if(imagemUrlRetornoWorkshops == ''){
        //     var formgroup = $('#inputWorkshopsImagem').parent().parent()
        //     formgroup.addClass('has-error has-feedback');
        //     formgroup.find('input').after("<span class='glyphicon glyphicon-remove form-control-feedback'></span>");
        //     setTimeout( function(){
        //         formgroup.removeClass('has-error').removeClass('has-feedback').find('.form-control-feedback').remove();
        //     }, 3000);
        //     return false;
        // }

        $('#inputWorkshopsTitulo').val('');
        $('#inputWorkshopsAutor').val('');
        $('#inputWorkshopsEditora').val('');
        $('#inputWorkshopsTexto').val('');

        // subir imagem, retornar o caminho e o titulo
        var li_html = "";
        li_html += "<li class='list-group-item'>";
            li_html += "<div class='row'>";
                li_html += "<div class='col-sm-2'>";

                    if(imagemUrlRetornoWorkshops)
                        li_html += "<img src='assets/img/portfolio/workshops/thumbs/"+imagemUrlRetornoWorkshops+"' style='max-width:100%;'>";

                li_html += "</div>";
                li_html += "<div class='col-sm-7'>";
                    li_html += "<h5><strong>"+workshops_titulo+"</strong></h5>";
                    li_html += "<p><strong>Autor</strong> : "+workshops_autor+"<br><strong>Editora</strong> : "+workshops_editora+"</p>";
                    li_html += "<p>"+workshops_texto+"</p>";
                li_html += "</div>";
                li_html += "<div class='col-sm-3'>";
                    li_html += "<div class='btn-group'>";
                        li_html += "<a href='#' class='btn btn-xs btn-info btn-alterar-workshops'>alterar</a>";
                        li_html += "<a href='#' class='btn btn-xs btn-danger btn-remover-workshops'>remover</a>";
                    li_html += "</div>";
                li_html += "</div>";
            li_html += "</div>";
            li_html += "<input type='hidden' name='workshops_titulo[]' value='"+workshops_titulo+"'>";
            li_html += "<input type='hidden' name='workshops_editora[]' value='"+workshops_editora+"'>";
            li_html += "<input type='hidden' name='workshops_autor[]' value='"+workshops_autor+"'>";
            li_html += "<input type='hidden' name='workshops_texto[]' value='"+workshops_texto+"'>";
            li_html += "<input type='hidden' name='workshops_imagem[]' value='"+imagemUrlRetornoWorkshops+"'>";
        li_html += "</li>";

        if($('.toRemove-work').length){
            $('.toRemove-work').replaceWith(li_html);
            $('.toRemove-work').removeClass('toRemove-work');
        }else{
            $('.listaWorkshops').append(li_html);
        }
        $('#submitWorkshops').html('Adicionar <span class=\'glyphicon glyphicon-plus\'></span>');
        $('#imagem-placeholder-workshops').html('');
        imagemUrlRetornoWorkshops = '';
        $('#cancelarEdicaoWorkshops').hide('fast');
    });

    var imagemUrlRetornoWorkshops = '';

    $('#inputWorkshopsImagem').fileupload({
        dataType : 'json',
        type     : 'post',
        formData: [{
            name  : 'path',
            value : 'portfolio/workshops/'
        }],
        start: function (){
            $('#imagem-placeholder-workshops').html("<img src='assets/img/ajax-loader.gif' style='margin:20px 0;'>");
            $('#submitWorkshops').attr('disabled', 'disabled');
        },
        done: function (e, data) {
            imagemUrlRetornoWorkshops = data.result.filename;
            $('#imagem-placeholder-workshops').html("<img style='max-width:30%;' src='assets/img/portfolio/workshops/thumbs/"+imagemUrlRetornoWorkshops+"'><br><a href='#' class='workshops_imagem_remover'>remover imagem</a><br>");
            $('#submitWorkshops').attr('disabled', false);
        }
    });

    $(document).on('click', '.btn-alterar-workshops', function(e){
        e.preventDefault();

        var parent = $(this).parent().parent().parent().parent();
        var titulo = parent.find("input[name='workshops_titulo[]']").val();
        var autor = parent.find("input[name='workshops_autor[]']").val();
        var editora = parent.find("input[name='workshops_editora[]']").val();
        var texto = parent.find("input[name='workshops_texto[]']").val();
        var imagem = parent.find("input[name='workshops_imagem[]']").val();

        $('#inputWorkshopsTitulo').val(titulo);
        $('#inputWorkshopsAutor').val(autor);
        $('#inputWorkshopsEditora').val(editora);
        $('#inputWorkshopsTexto').val(texto);

        if(imagem){
            $('#imagem-placeholder-workshops').html("<img style='max-width:30%;' src='assets/img/portfolio/workshops/thumbs/"+imagem+"'><br><a href='#' class='workshops_imagem_remover'>remover imagem</a><br>");
        }

        imagemUrlRetornoWorkshops = imagem;
        $('#cancelarEdicaoWorkshops').show('fast');
        $('#submitWorkshops').html('Salvar <span class=\'glyphicon glyphicon-plus\'></span>');
        $('.toRemove-work').removeClass('toRemove-work');
        parent.addClass('toRemove-work');
    });

    $(document).on('click', '.workshops_imagem_remover', function(e){
        e.preventDefault();
        imagemUrlRetornoWorkshops = '';
        $(this).parent().find('img').remove();
        $(this).parent().find('a').remove();
        $(this).parent().parent().find("input[name='workshops_imagem[]']").val('');
        return false;
    });

    $(document).on('click', '#cancelarEdicaoWorkshops', function(e){
        e.preventDefault();
        $('#inputWorkshopsTitulo').val('');
        $('#inputWorkshopsAutor').val('');
        $('#inputWorkshopsEditora').val('');
        $('#inputWorkshopsTexto').val('');
        $('#imagem-placeholder-workshops').html('');
        $('.toRemove-work').removeClass('toRemove-work');
        imagemUrlRetornoWorkshops = '';
        $('#submitWorkshops').html('Adicionar <span class=\'glyphicon glyphicon-plus\'></span>');
        $('#cancelarEdicaoWorkshops').hide('fast');
    });

    $(document).on('click', '.btn-remover-workshops', function(e){
        e.preventDefault();
        $(this).parent().parent().parent().parent().hide('normal', function(){
            $(this).remove();
        });
    });

    $('.listaWorkshops').sortable().disableSelection();


    // Botão de excluir registro
    $('.btn-delete').click( function(e){
    	e.preventDefault();
    	var form = $(this).closest('form');
    	bootbox.confirm('Deseja Excluir o Registro?', function(result){
	      	if(result)
	        	form.submit();
	      	else
	        	$(this).modal('hide');
    	});
  	});

    // Ordenação na tabela
    $('table.table-sortable tbody').sortable({
        update : function () {
            serial = [];
            tabela = $('table.table-sortable').attr('data-tabela');
            $('table.table-sortable tbody').children('tr').each(function(idx, elm) {
                serial.push(elm.id.split('_')[1])
            });
            $.post('ajax/gravaOrdem', { data : serial , tabela : tabela });
        },
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        handle : $('.btn-move')
    }).disableSelection();

	$('.btn-move').click( function(e){e.preventDefault();});

	$('.datepicker').datepicker();

    if($('textarea').length)
        $('textarea').not('.textarea_simples').ckeditor({
            customConfig: '/assets/js/ckeditor_config.js'
        });

    $('.listaImagens').sortable({
        placeholder: "imagem-placeholder"
    }).disableSelection();

    var timer;

    $('#buscaPersonalidades').on('keyup', function(){
        var termo = $(this).val();

        clearTimeout(timer);

        timer = setTimeout( function(){
            busca(termo);
        }, 300);
    });


    if($('.fileupload').length){

        $.each($('.fileupload'), function(){
            var fieldname = $(this).attr('data-fieldname');
            var limite = $(this).attr('data-limite');
            var scope  = $(this).parent().parent();
            var path   = $(this).attr('data-path');

            controlaUploadImagens(limite, scope);

            $(this).fileupload({
                dataType : 'json',
                type     : 'post',
                formData: [{
                    name  : 'path',
                    value : path
                }],
                start: function(e, data){
                    iconeUploadStart(scope);
                },
                done: function (e, data) {
                    var imagem = "<div class='projetoImagem'>";
                    imagem += "<img src='"+data.result.thumb+"'>";
                    imagem += "<input type='hidden' name='"+fieldname+"[]' value='"+data.result.filename+"'>";
                    imagem += "<a href='#' class='btn btn-sm btn-danger btn-remover' title='remover a imagem'><span class='glyphicon glyphicon-remove-sign'></span> <strong>remover imagem</strong></a>";
                    imagem += "</div>";
                    if(!maximoImagensAtingido(limite, scope)){
                        $('.listaImagens', scope).append(imagem);
                        $('.listaImagens', scope).sortable("refresh");
                        controlaUploadImagens(limite, scope);
                        iconeUploadStop(scope);
                    }
                }
            });
        });

        $(document).on('click', '.projetoImagem a.btn-remover', function(e){
            e.preventDefault();
            var parent = $(this).parent();
            var scope  = $(this).parent().parent().parent();
            var limite = $('.fileupload', scope).attr('data-limite');
            parent.css('opacity', .35);
            setTimeout( function(){
                parent.remove();
                controlaUploadImagens(limite, scope);
            }, 350);
        });

    }

    // Gerenciamento do plugin de Idiomas
    $('#submitIdiomas').click( function(e){
        e.preventDefault();
        var idiomas_titulo = $('#inputIdiomasTitulo').val();
        var idiomas_imagem = imagemUrlRetornoIdiomas;

        if(idiomas_titulo == ''){
            var formgroup = $('#inputIdiomasTitulo').parent().parent()
            formgroup.addClass('has-error has-feedback');
            formgroup.find('input').after("<span class='glyphicon glyphicon-remove form-control-feedback'></span>");
            setTimeout( function(){
                formgroup.removeClass('has-error').removeClass('has-feedback').find('.form-control-feedback').remove();
            }, 3000);
            return false;
        }

        if(imagemUrlRetornoIdiomas == ''){
            var formgroup = $('#inputIdiomasImagem').parent().parent()
            formgroup.addClass('has-error has-feedback');
            formgroup.find('input').after("<span class='glyphicon glyphicon-remove form-control-feedback'></span>");
            setTimeout( function(){
                formgroup.removeClass('has-error').removeClass('has-feedback').find('.form-control-feedback').remove();
            }, 3000);
            return false;
        }

        $.post('painel/idiomas/adicionar', {
          titulo : idiomas_titulo,
          imagem : idiomas_imagem
        }, function(resposta){

          if(resposta.status == 'ok'){

            $('#idiomas-msg').html("");

            var checkbox_html = "";
            checkbox_html += "<div class='col-sm-2'>";
            checkbox_html += "<label style='text-align:center;'>";
            checkbox_html += "<input type='checkbox' name='idiomas[]' value='"+ resposta.id +"'> <img style='display: block; max-width: 100%' src='assets/img/portfolio/idiomas/redimensionadas/icones/"+imagemUrlRetornoIdiomas+"'> " + idiomas_titulo;
            checkbox_html += "</label>";
            checkbox_html += "</div>";

            $('.listaIdiomas').append(checkbox_html);

          }else{

            var msg_html = "";
            msg_html += "<div class='panel panel-default'>";
            msg_html += "<div class='panel-body bg-info'>";
            msg_html += "O idioma já está cadastrado!.";
            msg_html += "</div></div>";

            $('#idiomas-msg').html(msg_html);

          }

          $('#inputIdiomasTitulo').val('');
          $('#imagem-placeholder-idioma').html('');
          imagemUrlRetornoIdiomas = '';

        });

    });

    var imagemUrlRetornoIdiomas = '';

    $('#inputIdiomasImagem').fileupload({
        dataType : 'json',
        type     : 'post',
        formData: [{
            name  : 'path',
            value : 'portfolio/idiomas/'
        }],
        start: function (){
            $('#imagem-placeholder-idioma').html("<img src='assets/img/ajax-loader.gif' style='margin:20px 0;'>");
            $('#submitIdiomas').attr('disabled', 'disabled');
        },
        done: function (e, data) {
            imagemUrlRetornoIdiomas = data.result.filename;
            $('#imagem-placeholder-idioma').html("<img style='max-width:30%;' src='assets/img/portfolio/idiomas/thumbs/"+imagemUrlRetornoIdiomas+"'>");
            $('#submitIdiomas').attr('disabled', false);
        }
    });

    // Gerenciamento do plugin de Premios
    $('#submitPremios').click( function(e){
        e.preventDefault();
        var premios_titulo = $('#inputPremiosTitulo').val();
        var premios_texto = $('#inputPremiosTexto').val();
        var premios_imagem = $('#inputPremiosImagem').val();

        if(premios_titulo == ''){
            var formgroup = $('#inputPremiosTitulo').parent().parent()
            formgroup.addClass('has-error has-feedback');
            formgroup.find('input').after("<span class='glyphicon glyphicon-remove form-control-feedback'></span>");
            setTimeout( function(){
                formgroup.removeClass('has-error').removeClass('has-feedback').find('.form-control-feedback').remove();
            }, 3000);
            return false;
        }

        if(premios_texto == ''){
            var formgroup = $('#inputPremiosTexto').parent().parent()
            formgroup.addClass('has-error has-feedback');
            formgroup.find('input').after("<span class='glyphicon glyphicon-remove form-control-feedback'></span>");
            setTimeout( function(){
                formgroup.removeClass('has-error').removeClass('has-feedback').find('.form-control-feedback').remove();
            }, 3000);
            return false;
        }

        $('#inputPremiosTitulo').val('');
        $('#inputPremiosTexto').val('');

        // subir imagem, retornar o caminho e o titulo
        // premios_imagem = ?
        var li_html = "";
        li_html += "<li class='list-group-item'>";
            li_html += "<div class='row'>";
                li_html += "<div class='col-sm-2'>";
                    if(imagemUrlRetornoPremios != ''){
                      li_html += "<img src='assets/img/portfolio/premios/thumbs/"+imagemUrlRetornoPremios+"' style='max-width:100%;'>";
                    }
                li_html += "</div>";
                li_html += "<div class='col-sm-7'>";
                    li_html += "<h5>"+premios_titulo+"</h5>";
                    li_html += "<p>"+premios_texto+"</p>";
                li_html += "</div>";
                li_html += "<div class='col-sm-3'>";
                    li_html += "<div class='btn-group'>";
                        li_html += "<a href='#' class='btn btn-xs btn-info btn-alterar-premios'>alterar</a>";
                        li_html += "<a href='#' class='btn btn-xs btn-danger btn-remover-premios'>remover</a>";
                    li_html += "</div>";
                li_html += "</div>";
            li_html += "</div>";
            li_html += "<input type='hidden' name='premios_titulo[]' value='"+premios_titulo+"'>";
            li_html += "<input type='hidden' name='premios_texto[]' value='"+premios_texto+"'>";
            li_html += "<input type='hidden' name='premios_imagem[]' value='"+imagemUrlRetornoPremios+"'>";
        li_html += "</li>";

        if($('.toRemove').length){
            $('.toRemove').replaceWith(li_html);
            $('.toRemove').removeClass('toRemove');
        }else{
            $('.listaPremios').append(li_html);
        }
        $('#submitPremios').html('Adicionar <span class=\'glyphicon glyphicon-plus\'></span>');
        $('#imagem-placeholder-premios').html('');
        imagemUrlRetornoPremios = '';
        $('#cancelarEdicaoPremios').hide('fast');
    });

    var imagemUrlRetornoPremios = '';

    $('#inputPremiosImagem').fileupload({
        dataType : 'json',
        type     : 'post',
        formData: [{
            name  : 'path',
            value : 'portfolio/premios/'
        }],
        start: function (){
            $('#imagem-placeholder-premios').html("<img src='assets/img/ajax-loader.gif' style='margin:20px 0;'>");
            $('#submitPremios').attr('disabled', 'disabled');
        },
        done: function (e, data) {
            imagemUrlRetornoPremios = data.result.filename;
            $('#imagem-placeholder-premios').html("<img style='max-width:30%;' src='assets/img/portfolio/premios/thumbs/"+imagemUrlRetornoPremios+"'>");
            $('#submitPremios').attr('disabled', false);
        }
    });

    $(document).on('click', '.btn-alterar-premios', function(e){
        e.preventDefault();

        var parent = $(this).parent().parent().parent().parent();
        var titulo = parent.find("input[name='premios_titulo[]']").val();
        var texto = parent.find("input[name='premios_texto[]']").val();
        var imagem = parent.find("input[name='premios_imagem[]']").val();

        $('#inputPremiosTitulo').val(titulo);
        $('#inputPremiosTexto').val(texto);
        if(imagem != ''){
          $('#imagem-placeholder-premios').html("<img style='max-width:30%;' src='assets/img/portfolio/premios/thumbs/"+imagem+"'>");
        }
        imagemUrlRetornoPremios = imagem;
        $('#cancelarEdicaoPremios').show('fast');
        $('#submitPremios').html('Salvar <span class=\'glyphicon glyphicon-plus\'></span>');

        parent.addClass('toRemove');
    });

    $(document).on('click', '#cancelarEdicaoPremios', function(e){
        e.preventDefault();
        $('#inputPremiosTitulo').val('');
        $('#inputPremiosTexto').val('');
        $('#imagem-placeholder-premios').html('');
        $('.toRemove').removeClass('toRemove');
        imagemUrlRetornoPremios = '';
        $('#submitPremios').html('Adicionar <span class=\'glyphicon glyphicon-plus\'></span>');
        $('#cancelarEdicaoPremios').hide('fast');
    });

    $(document).on('click', '.btn-remover-premios', function(e){
        e.preventDefault();
        $(this).parent().parent().parent().parent().hide('normal', function(){
            $(this).remove();
        });
    });

    $('.listaPremios').sortable().disableSelection();

});
