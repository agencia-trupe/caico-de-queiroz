@extends('painel.template.index')

@section('conteudo')

    <div class="container-fluid padded-bottom">
    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		      	<h2>
		        	Editar Texto - About
		        </h2>

		        <hr>

		        @include('painel.template.mensagens')

		        <form action="{{ URL::route('painel.about.update', $registro->id) }}" method="post" enctype="multipart/form-data">

					<input type="hidden" name="_method" value="PUT">

					{!! csrf_field() !!}

			    	<div class="form-group">
						<label for="inputTexto">Texto</label>
						<textarea name="texto" class="form-control" id="inputTexto">{{$registro->texto}}</textarea>
					</div>

					<div class="form-group">
						@if($registro->imagem)
							Imagem Atual<br>
							<img src="assets/img/about/{{$registro->imagem}}"><br>
						@endif
						<label for="inputImagem">Trocar Imagem</label>
						<input type="file" class="form-control" id="inputImagem" name="imagem">
					</div>

					<hr>

					<div class="form-group">
						<label for="input-instagram">Instagram</label>
						<input type="text" class="form-control" name="instagram" id="input-instagram" value="{{$registro->instagram}}">
					</div>

					<div class="form-group">
						<label for="input-twitter">Twitter</label>
						<input type="text" class="form-control" name="twitter" id="input-twitter" value="{{$registro->twitter}}">
					</div>

					<div class="form-group">
						<label for="input-facebook">Facebook</label>
						<input type="text" class="form-control" name="facebook" id="input-facebook" value="{{$registro->facebook}}">
					</div>

					<div class="form-group">
						<label for="input-tumblr">Tumblr</label>
						<input type="text" class="form-control" name="tumblr" id="input-tumblr" value="{{$registro->tumblr}}">
					</div>

					<div class="form-group">
						<label for="input-linkedin">LinkedIn</label>
						<input type="text" class="form-control" name="linkedin" id="input-linkedin" value="{{$registro->linkedin}}">
					</div>

					<div class="form-group">
						<label for="input-pinterest">Pinterest</label>
						<input type="text" class="form-control" name="pinterest" id="input-pinterest" value="{{$registro->pinterest}}">
					</div>

					<div class="form-group">
						<label for="input-youtube">YouTube</label>
						<input type="text" class="form-control" name="youtube" id="input-youtube" value="{{$registro->youtube}}">
					</div>

					<div class="form-group">
						<label for="input-periscope">Periscope</label>
						<input type="text" class="form-control" name="periscope" id="input-periscope" value="{{$registro->periscope}}">
					</div>

					<div class="form-group">
						<label for="input-vimeo">Vimeo</label>
						<input type="text" class="form-control" name="vimeo" id="input-vimeo" value="{{$registro->vimeo}}">
					</div>

					<div class="form-group">
						<label for="input-googleplus">Google +</label>
						<input type="text" class="form-control" name="googleplus" id="input-googleplus" value="{{$registro->googleplus}}">
					</div>

					<div class="form-group">
						<label for="input-wordpress">Wordpress</label>
						<input type="text" class="form-control" name="wordpress" id="input-wordpress" value="{{$registro->wordpress}}">
					</div>

					<div class="form-group">
						<label for="input-snapchat">Snapchat</label>
						<input type="text" class="form-control" name="snapchat" id="input-snapchat" value="{{$registro->snapchat}}">
					</div>

					<hr>

					<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

					<a href="{{ URL::route('painel.about.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

				</form>
			</div>
		</div>
    </div>

@endsection