@extends('painel.template.index')

@section('conteudo')

    <div class="container-fluid padded-bottom">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h2>
                    Marca
                </h2>

                <hr>

            	@include('painel.template.mensagens')

                <table class="table table-striped table-bordered table-hover">

              		<thead>
                		<tr>
                  			<th>Imagem</th>
                  			<th><span class="glyphicon glyphicon-cog"></span></th>
                		</tr>
              		</thead>

              		<tbody>
                    	@foreach ($registros as $registro)

                        	<tr class="tr-row">
                          		<td>
                                <img src="assets/img/marca/{{$registro->imagem}}" class="img-thumbnail" />
                              </td>
                          		<td class="crud-actions">
                            		<a href="{{ URL::route('painel.marca.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>
                          		</td>
                        	</tr>

                    	@endforeach
              		</tbody>

            	</table>

            </div>
        </div>
    </div>

@endsection
