@extends('painel.template.index')

@section('conteudo')

    <div class="container-fluid padded-bottom">
    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		      	<h2>
		        	Editar Marca
		        </h2>

		        <hr>

		        @include('painel.template.mensagens')

		        <form action="{{ URL::route('painel.marca.update', $registro->id) }}" method="post" enctype="multipart/form-data">

					<input type="hidden" name="_method" value="PUT">

					{!! csrf_field() !!}

					<div class="form-group">
						@if($registro->imagem)
							Imagem Atual<br>
							<img src="assets/img/marca/{{$registro->imagem}}"><br>
						@endif
						<label for="inputImagem">Trocar Imagem</label>
						<input type="file" class="form-control" id="inputImagem" name="imagem">
					</div>

					<hr>

					<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

					<a href="{{ URL::route('painel.marca.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

				</form>
			</div>
		</div>
    </div>

@endsection
