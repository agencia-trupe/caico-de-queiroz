<nav class="navbar navbar-default">
	<div class="container-fluid">

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-painel" aria-expanded="false">
			    <span class="sr-only">Navegação</span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
		    </button>
		    <a class="navbar-brand" title="Página Inicial" href="{{ URL::route('painel.home') }}">{{ config('site.titulo') }}</a>
		</div>

		<div class="collapse navbar-collapse" id="menu-painel">
			<ul class="nav navbar-nav">

				<li @if(str_is('painel.home*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.home')}}" title="Início">Início</a>
				</li>

				<li class="dropdown @if(preg_match('~painel.(marca|banners*)~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(preg_match('~painel.(marca*)~', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.marca.index')}}" title="Marca">Marca</a>
						</li>
						<li @if(preg_match('~painel.(banners*)~', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.banners.index')}}" title="Banners">Banners</a>
						</li>
					</ul>
				</li>

				<li @if(str_is('painel.about*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.about.index')}}" title="About">About</a>
				</li>

        <li @if(str_is('painel.destaques*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.destaques.index')}}" title="Destaques">Destaques</a>
				</li>

				<li @if(str_is('painel.imprensa*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.imprensa.index')}}" title="Imprensa">Imprensa</a>
				</li>

				<li @if(str_is('painel.publicacoes*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.publicacoes.index')}}" title="Publicações">Publicações</a>
				</li>

				<li @if(str_is('painel.acaosocial*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.acaosocial.index')}}" title="Ação Social">Ação Social</a>
				</li>

				<li @if(str_is('painel.juridico*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.juridico.index')}}" title="Jurídico">Jurídico</a>
				</li>

				<li @if(str_is('painel.parcerias*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.parcerias.index')}}" title="Parcerias">Parcerias</a>
				</li>

				<li @if(str_is('painel.licenciamento*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.licenciamento.index')}}" title="Licenciamento">Licenciamento</a>
				</li>

				<li @if(str_is('painel.contato*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.contato.index')}}" title="Contato">Contato</a>
				</li>

				<li @if(str_is('painel.portfolio*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('painel.portfolio.index')}}" title="Portfolio">Portfolio</a>
				</li>

				<li class="dropdown @if(preg_match('~painel.(usuarios|idiomas*)~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sistema <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(preg_match('~painel.(usuarios*)~', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.usuarios.index')}}" title="Usuários do Painel">Usuários</a>
						</li>
						<li @if(preg_match('~painel.(idiomas*)~', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.idiomas.index')}}" title="Gerenciar Idiomas">Gerenciar Idiomas</a>
						</li>
						<li role="separator" class="divider"></li>
						<li>
							<a href="{{URL::route('painel.off')}}" title="Logout">Logout</a>
						</li>
					</ul>
				</li>

			</ul>
		</div>

	</div>
</nav>
