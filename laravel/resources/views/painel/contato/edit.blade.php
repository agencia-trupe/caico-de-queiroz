@extends('painel.template.index')

@section('conteudo')

    <div class="container-fluid padded-bottom">
    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		      	<h2>
		        	Editar Texto - Contato
		        </h2>

		        <hr>

		        @include('painel.template.mensagens')

		        <form action="{{ URL::route('painel.contato.update', $registro->id) }}" method="post" enctype="multipart/form-data">

					<input type="hidden" name="_method" value="PUT">

					{!! csrf_field() !!}

			    	<div class="form-group">
						<label for="inputTelefone">Telefone</label>
						<textarea name="telefone" class="form-control" id="inputTelefone">{{$registro->telefone}}</textarea>
					</div>

					<div class="form-group">
						<label for="inputendereco">Endereço</label>
						<textarea name="endereco" class="form-control" id="inputendereco">{{$registro->endereco}}</textarea>
					</div>

					<div class="form-group">
						<label for="input-google_maps">Código de Incorporação do Google Maps</label>
						<input type="text" class="form-control" name="google_maps" id="input-google_maps" value="{{$registro->google_maps}}">
					</div>

					<hr>

					<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

					<a href="{{ URL::route('painel.contato.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

				</form>
			</div>
		</div>
    </div>

@endsection