<h4>Publicações <a data-toggle="collapse" href="#modulo-publicacoes" class="btn btn-xs btn-default btn-ativar-modulo">@if(isset($registro) && sizeof($registro->publicacoes)) remover <span class='glyphicon glyphicon-triangle-top'></span> @else adicionar <span class='glyphicon glyphicon-triangle-bottom'></span> @endif</a></h4>

<input type="hidden" id="inputHasPublicacoes" name="has_publicacoes" @if(isset($registro) && sizeof($registro->publicacoes)) value="1" @endif>

<div id="modulo-publicacoes" class="area-modulo collapse @if(isset($registro) && sizeof($registro->publicacoes)) in @endif " style="margin:20px 0;">

	<div class="well">
		<div class="form-horizontal">

			<div class="form-group">
				<label for="inputPublicacoesTitulo" class="col-sm-2 control-label">Título</label>
				<div class="col-sm-10">
					<input type="text" id="inputPublicacoesTitulo" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="inputPublicacoesAutor" class="col-sm-2 control-label">Autor</label>
				<div class="col-sm-10">
					<input type="text" id="inputPublicacoesAutor" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="inputPublicacoesEditora" class="col-sm-2 control-label">Editora</label>
				<div class="col-sm-10">
					<input type="text" id="inputPublicacoesEditora" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="inputPublicacoesTexto" class="col-sm-2 control-label">Texto</label>
				<div class="col-sm-10">
					<input type="text" id="inputPublicacoesTexto" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="inputPublicacoesImagem" class="col-sm-2 control-label">Imagem</label>
				<div class="col-sm-10">
					<div id="imagem-placeholder-publicacoes"></div>
					<input type="file" id="inputPublicacoesImagem" class="form-control" name="files" data-url="painel/imagens/upload" data-path='portfolio/publicacoes'>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="button" id="submitPublicacoes" class="btn btn-xs btn-success"> Adicionar <span class='glyphicon glyphicon-plus'></span></button>
					<button type="button" id="cancelarEdicaoPublicacoes" style="display:none;" class="btn btn-xs btn-default"> Cancelar</button>
				</div>
			</div>

			<hr>

			<ul class="listaPublicacoes list-group">
				@if(isset($registro) && sizeof($registro->publicacoes))
					@foreach($registro->publicacoes as $key => $publi)
						<li class='list-group-item'>
							<div class="row">
								<div class="col-sm-2">
									<img src="assets/img/portfolio/publicacoes/thumbs/{{ $publi->imagem }}" style='max-width:100%;'>
								</div>
								<div class="col-sm-7">
									<h5><strong>{{ $publi->titulo }}</strong></h5>
									<p>
										<strong>Autor</strong>: {{ $publi->autor }}<br>
										<strong>Editora</strong>: {{ $publi->editora }}
									</p>
									<p>
										{{ $publi->texto }}
									</p>
								</div>
								<div class="col-sm-3">
									<div class="btn-group">
										<a href="#" class='btn btn-xs btn-info btn-alterar-publicacoes'>alterar</a>
										<a href="#" class='btn btn-xs btn-danger btn-remover-publicacoes'>remover</a>
									</div>
								</div>
							</div>
							<input type="hidden" name='publicacoes_titulo[]' value='{{ $publi->titulo }}'>
							<input type="hidden" name='publicacoes_editora[]' value='{{ $publi->editora }}'>
							<input type="hidden" name='publicacoes_autor[]' value='{{ $publi->autor }}'>
							<input type="hidden" name='publicacoes_texto[]' value='{{ $publi->texto }}'>
							<input type="hidden" name='publicacoes_imagem[]' value='{{ $publi->imagem }}'>
						</li>
					@endforeach
				@endif
			</ul>

			<div class="panel panel-default">
				<div class="panel-body">
			    	Você pode clicar e arrastar cadastros para ordená-los.
			  	</div>
			</div>

		</div>
	</div>

</div>

<hr>