@extends('painel.template.index')

@section('conteudo')

    <div class="container-fluid padded-bottom">
    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		      	<h2>
		        	Portfolio <small>Editar Personalidade</small>
		        </h2>

		        <hr>

		        @include('painel.template.mensagens')

				<form action="{{ URL::route('painel.portfolio.update', $registro->id) }}" method="post" enctype="multipart/form-data">

					<input type="hidden" name="_method" value="PUT">

					{!! csrf_field() !!}

					<div class="form-group">
						<label for="inputCategoria">Categoria</label>
						<select name="portfolio_categorias_id" id="inputCategoria" class="form-control" required>
							<option value="">Selecione uma Categoria</option>
							@foreach($listaCategorias as $categoria)
								<option value="{{ $categoria->id }}" @if($registro->portfolio_categorias_id == $categoria->id) selected @endif >{{ $categoria->titulo }}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group" id='contemSelectSubcategorias'>
						<label for="inputSubcategoria">Subcategoria</label>
						<select name="portfolio_subcategorias_id" id="inputSubcategoria" class="form-control">
							<option value="">Selecione uma Subcategoria</option>
						</select>
						<input type="hidden" id="portfolio_subcategorias_id_selecionado" value="{{ $registro->portfolio_subcategorias_id }}">
					</div>

			    	<div class="form-group">
						<label for="inputNome">Nome</label>
						<input type="text" class="form-control" id="inputNome" name="nome"  value="{{ $registro->nome }}" required>
					</div>

					<div class="form-group">
						<label for="inputCorNome">Cor do Nome na página do Portfolio</label>
						<select name="cor_nome" id="inputCorNome" required class='form-control'>
							<option value="FFF" @if($registro->cor_nome == 'FFF') selected @endif >BRANCO</option>
							<option value="000" @if($registro->cor_nome == '000') selected @endif >PRETO</option>
						</select>
					</div>

					<div class="well">
						<div class="form-group">
							@if($registro->imagem_entrada)
								Imagem Atual<br>
								<img src="assets/img/portfolio/entrada/{{$registro->imagem_entrada}}" style='max-width:100%;'><hr>
							@endif
							<label for="inputImagem">Imagem de Entrada</label>
							<input type="file" class="form-control" id="inputImagem" name="imagem_entrada">
						</div>
					</div>

          <div class="well">
						<div class="form-group">
							@if($registro->thumb_home)
								Thumbnail Atual<br>
								<img src="assets/img/portfolio/thumbs-home/{{$registro->thumb_home}}" style='max-width: 200px;'><hr>
                <hr>
                <label><input type="checkbox" name="remover_thumb_home" value="1"> Remover imagem atual</label>
							@endif
              <hr>
							<label for="inputThumbHome">Nova Thumbnail para a Home</label>
							<input type="file" class="form-control" id="inputThumbHome" name="thumb_home">
						</div>
					</div>

					<div class="well">
						<div class="form-group">
							@if($registro->arquivo_release)
								Release Atual :
								<a href="assets/releases/{{ $registro->arquivo_release }}" class='btn btn-sm btn-default' target='_blank'>{{ $registro->arquivo_release }}</a><hr>
							@endif
							<label for="inputRelease">Release</label>
							<input type="file" class="form-control" id="inputRelease" name="arquivo_release">
						</div>
					</div>

					<div class="form-group">
						<label for="inputLinkSite">Link para o site</label>
						<input type="text" class="form-control" id="inputLinkSite" name="link_site"  value="{{ $registro->link_site }}">
					</div>

          <div class="well">
            <div class="row">
              <label class="col-sm-6">
                <input type="radio" name="publicar" value="publicar" required @if($registro->publicar == 1) checked @endif > Publicar
              </label>
              <label class="col-sm-6">
                <input type="radio" name="publicar" value="rascunho" @if($registro->publicar == 0) checked @endif > Salvar como rascunho
              </label>
            </div>
          </div>

					<hr>

					<h3>Módulos</h3>

					<div class="panel panel-default">
						<div class="panel-body">
					    	Clique nos módulos para inserí-los no cadastro.
					  	</div>
					</div>

					@include('painel.portfolio.paralaxe', compact('registro'))

					@include('painel.portfolio.fotos', compact('registro'))

					@include('painel.portfolio.personagens', compact('registro'))

					@include('painel.portfolio.capas', compact('registro'))

					@include('painel.portfolio.publicidade', compact('registro'))

					@include('painel.portfolio.desfiles', compact('registro'))

					@include('painel.portfolio.videos', compact('registro'))

					@include('painel.portfolio.acaosocial', compact('registro'))

					@include('painel.portfolio.editorialdemoda', compact('registro'))

					@include('painel.portfolio.personalstylist', compact('registro'))

					@include('painel.portfolio.redessociais', compact('registro'))

					@include('painel.portfolio.palestras', compact('registro'))

					@include('painel.portfolio.consultoriamoda', compact('registro'))

					@include('painel.portfolio.publicacoes', compact('registro'))

					@include('painel.portfolio.extras', compact('registro'))

					@include('painel.portfolio.redcarpet', compact('registro'))

					@include('painel.portfolio.espetaculos', compact('registro'))

					@include('painel.portfolio.workshops', compact('registro'))

					@include('painel.portfolio.creditos', compact('registro'))

          @include('painel.portfolio.idiomas', compact('registro'))

          @include('painel.portfolio.premios', compact('registro'))

					<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

					<a href="{{ URL::route('painel.portfolio.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

				</form>
			</div>
		</div>
    </div>

@endsection
