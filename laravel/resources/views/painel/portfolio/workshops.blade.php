<h4>Workshops <a data-toggle="collapse" href="#modulo-workshops" class="btn btn-xs btn-default btn-ativar-modulo">@if(isset($registro) && sizeof($registro->workshops)) remover <span class='glyphicon glyphicon-triangle-top'></span> @else adicionar <span class='glyphicon glyphicon-triangle-bottom'></span> @endif</a></h4>

<input type="hidden" id="inputHasWorkshops" name="has_workshops" @if(isset($registro) && sizeof($registro->workshops)) value="1" @endif>

<div id="modulo-workshops" class="area-modulo collapse @if(isset($registro) && sizeof($registro->workshops)) in @endif " style="margin:20px 0;">

	<div class="well">
		<div class="form-horizontal">

			<div class="form-group">
				<label for="inputWorkshopsTitulo" class="col-sm-2 control-label">Título</label>
				<div class="col-sm-10">
					<input type="text" id="inputWorkshopsTitulo" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="inputWorkshopsAutor" class="col-sm-2 control-label">Autor</label>
				<div class="col-sm-10">
					<input type="text" id="inputWorkshopsAutor" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="inputWorkshopsEditora" class="col-sm-2 control-label">Editora</label>
				<div class="col-sm-10">
					<input type="text" id="inputWorkshopsEditora" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="inputWorkshopsTexto" class="col-sm-2 control-label">Texto</label>
				<div class="col-sm-10">
					<input type="text" id="inputWorkshopsTexto" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="inputWorkshopsImagem" class="col-sm-2 control-label">Imagem</label>
				<div class="col-sm-10">
					<div id="imagem-placeholder-workshops"></div>
					<input type="file" id="inputWorkshopsImagem" class="form-control" name="files" data-url="painel/imagens/upload" data-path='portfolio/workshops'>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="button" id="submitWorkshops" class="btn btn-xs btn-success"> Adicionar <span class='glyphicon glyphicon-plus'></span></button>
					<button type="button" id="cancelarEdicaoWorkshops" style="display:none;" class="btn btn-xs btn-default"> Cancelar</button>
				</div>
			</div>

			<hr>

			<ul class="listaWorkshops list-group">
				@if(isset($registro) && sizeof($registro->workshops))
					@foreach($registro->workshops as $key => $publi)
						<li class='list-group-item'>
							<div class="row">
								<div class="col-sm-2">
									@if($publi->imagem)
										<img src="assets/img/portfolio/workshops/thumbs/{{ $publi->imagem }}" style='max-width:100%;'>
									@endif
								</div>
								<div class="col-sm-7">
									<h5><strong>{{ $publi->titulo }}</strong></h5>
									<p>
										<strong>Autor</strong>: {{ $publi->autor }}<br>
										<strong>Editora</strong>: {{ $publi->editora }}
									</p>
									<p>
										{{ $publi->texto }}
									</p>
								</div>
								<div class="col-sm-3">
									<div class="btn-group">
										<a href="#" class='btn btn-xs btn-info btn-alterar-workshops'>alterar</a>
										<a href="#" class='btn btn-xs btn-danger btn-remover-workshops'>remover</a>
									</div>
								</div>
							</div>
							<input type="hidden" name='workshops_titulo[]' value='{{ $publi->titulo }}'>
							<input type="hidden" name='workshops_editora[]' value='{{ $publi->editora }}'>
							<input type="hidden" name='workshops_autor[]' value='{{ $publi->autor }}'>
							<input type="hidden" name='workshops_texto[]' value='{{ $publi->texto }}'>
							<input type="hidden" name='workshops_imagem[]' value='{{ $publi->imagem }}'>
						</li>
					@endforeach
				@endif
			</ul>

			<div class="panel panel-default">
				<div class="panel-body">
			    	Você pode clicar e arrastar cadastros para ordená-los.
			  	</div>
			</div>

		</div>
	</div>

</div>

<hr>