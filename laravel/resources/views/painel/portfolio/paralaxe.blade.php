<h4>Imagens em Paralaxe <a data-toggle="collapse" href="#modulo-paralaxe" class="btn btn-xs btn-default btn-ativar-modulo">@if(isset($registro) && $registro->paralaxe) remover <span class='glyphicon glyphicon-triangle-top'></span> @else adicionar <span class='glyphicon glyphicon-triangle-bottom'></span> @endif</a></h4>

<input type="hidden" id="inputHasParalaxe" name="has_paralaxe" @if(isset($registro) && $registro->paralaxe) value="1" @endif>

<div id="modulo-paralaxe" class="area-modulo collapse @if(isset($registro) && $registro->paralaxe) in @endif " style="margin:20px 0;">

	<div class="well">

		<div class="multiUpload">
			<div class="icone">
				<span class="glyphicon glyphicon-open"></span>
				<span class="glyphicon glyphicon-refresh"></span>
			</div>
			<p>
				Escolha as imagens para criar o efeito de Paralaxe. Você pode selecionar até 5 imagens.
			</p>
			<input id="fileupload-portfolio" class="fileupload" type="file" name="files" data-url="painel/imagens/upload" data-limite='5' data-path='portfolio/paralaxe' data-fieldname='imagem_paralaxe' multiple>

		</div>

		<div class="limiteImagensAtingido" style='display:none;'>
			<div class="panel panel-default">
				<div class="panel-body bg-info">
			    	Número máximo de imagens (5) atingido.
			  	</div>
			</div>
		</div>

		<div class="listaImagens">
			@if(isset($registro) && $registro->paralaxe)
				@for($i = 1; $i <= 5; $i++)
					@if(isset($registro->paralaxe->{'imagem_'.$i}) && $registro->paralaxe->{'imagem_'.$i} != '')
						<div class='projetoImagem'>
				        	<img src="assets/img/portfolio/paralaxe/thumbs/{{ $registro->paralaxe->{'imagem_'.$i} }}">
				        	<input type='hidden' name='imagem_paralaxe[]' value="{{ $registro->paralaxe->{'imagem_'.$i} }}">
				        	<a href='#' class='btn btn-sm btn-danger btn-remover' title='remover a imagem'><span class='glyphicon glyphicon-remove-sign'></span> <strong>remover imagem</strong></a>
			        	</div>
		        	@endif
				@endfor
			@endif
		</div>

		<div class="panel panel-default">
			<div class="panel-body">
		    	Você pode clicar e arrastar as imagens para ordená-las.
		  	</div>
		</div>
	</div>

</div>

<hr>