<h4>Palestras <a data-toggle="collapse" href="#modulo-palestras" class="btn btn-xs btn-default btn-ativar-modulo">@if(isset($registro) && sizeof($registro->palestras)) remover <span class='glyphicon glyphicon-triangle-top'></span> @else adicionar <span class='glyphicon glyphicon-triangle-bottom'></span> @endif</a></h4>

<input type="hidden" id="inputHasPalestras" name="has_palestras" @if(isset($registro) && sizeof($registro->palestras)) value="1" @endif>

<div id="modulo-palestras" class="area-modulo collapse @if(isset($registro) && sizeof($registro->palestras)) in @endif " style="margin:20px 0;">

	<div class="well">
		<div class="form-horizontal">

			<div class="form-group">
				<label for="inputMidiasPalestras" class="col-sm-2 control-label"> Texto</label>
				<div class="col-sm-10">
					<textarea id="inputMidiasPalestras" name="palestras[texto]">@if(isset($registro) && sizeof($registro->palestras)) {{ $registro->palestras->texto }} @endif</textarea>
				</div>
			</div>

		</div>
	</div>

</div>

<hr>