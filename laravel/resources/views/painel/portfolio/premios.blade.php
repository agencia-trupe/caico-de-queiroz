<h4>Prêmios <a data-toggle="collapse" href="#modulo-premios" class="btn btn-xs btn-default btn-ativar-modulo">@if(isset($registro) && sizeof($registro->premios)) remover <span class='glyphicon glyphicon-triangle-top'></span> @else adicionar <span class='glyphicon glyphicon-triangle-bottom'></span> @endif</a></h4>

<input type="hidden" id="inputHasPremios" name="has_premios" @if(isset($registro) && sizeof($registro->premios)) value="1" @endif>

<div id="modulo-premios" class="area-modulo collapse @if(isset($registro) && sizeof($registro->premios)) in @endif " style="margin:20px 0;">

	<div class="well">
		<div class="form-horizontal">

			<div class="form-group">
				<label for="inputPremiosTitulo" class="col-sm-2 control-label">Título</label>
				<div class="col-sm-10">
					<input type="text" id="inputPremiosTitulo" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="inputPremiosTexto" class="col-sm-2 control-label">Texto</label>
				<div class="col-sm-10">
					<input type="text" id="inputPremiosTexto" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="inputPremiosImagem" class="col-sm-2 control-label">Imagem</label>
				<div class="col-sm-10">
					<div id="imagem-placeholder-premios"></div>
					<input type="file" id="inputPremiosImagem" class="form-control" name="files" data-url="painel/imagens/upload" data-path='portfolio/premios'>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="button" id="submitPremios" class="btn btn-xs btn-success"> Adicionar <span class='glyphicon glyphicon-plus'></span></button>
					<button type="button" id="cancelarEdicaoPremios" style="display:none;" class="btn btn-xs btn-default"> Cancelar</button>
				</div>
			</div>

			<hr>

			<ul class="listaPremios list-group">
				@if(isset($registro) && sizeof($registro->premios))
					@foreach($registro->premios as $key => $premio)
						<li class='list-group-item'>
							<div class="row">
								<div class="col-sm-2">
                  @if($premio->imagem != '')
									  <img src="assets/img/portfolio/premios/thumbs/{{ $premio->imagem }}" style='max-width:100%;'>
                  @endif
								</div>
								<div class="col-sm-7">
									<h5><strong>{{ $premio->titulo }}</strong></h5>
									<p>
										{{ $premio->texto }}
									</p>
								</div>
								<div class="col-sm-3">
									<div class="btn-group">
										<a href="#" class='btn btn-xs btn-info btn-alterar-premios'>alterar</a>
										<a href="#" class='btn btn-xs btn-danger btn-remover-premios'>remover</a>
									</div>
								</div>
							</div>
							<input type="hidden" name='premios_titulo[]' value='{{ $premio->titulo }}'>
							<input type="hidden" name='premios_texto[]' value='{{ $premio->texto }}'>
							<input type="hidden" name='premios_imagem[]' value='{{ $premio->imagem }}'>
						</li>
					@endforeach
				@endif
			</ul>

			<div class="panel panel-default">
				<div class="panel-body">
			    	Você pode clicar e arrastar cadastros para ordená-los.
			  	</div>
			</div>

		</div>
	</div>

</div>

<hr>
