<h4>Idiomas <a data-toggle="collapse" href="#modulo-idiomas" class="btn btn-xs btn-default btn-ativar-modulo">@if(isset($registro) && sizeof($registro->idiomas)) remover <span class='glyphicon glyphicon-triangle-top'></span> @else adicionar <span class='glyphicon glyphicon-triangle-bottom'></span> @endif</a></h4>

<input type="hidden" id="inputHasIdiomas" name="has_idiomas" @if(isset($registro) && sizeof($registro->idiomas)) value="1" @endif>

<div id="modulo-idiomas" class="area-modulo collapse @if(isset($registro) && sizeof($registro->idiomas)) in @endif " style="margin:20px 0;">

	<div class="well">

    <h5>Selecione um ou mais:</h5>

    <div id="idiomas-msg"></div>

    <div class="listaIdiomas row">
      @foreach($listaIdiomas as $idioma)
        <div class="col-sm-2">
          <label style='text-align:center;'>
            <input type="checkbox"
                   name="idiomas[]"
                   value="{{$idioma->id}}"
                   @if(isset($registro) && sizeof($registro->idiomas) && in_array($idioma->id, $registro->idiomas->pluck('id')->toArray())) checked @endif
                   >

            <img src="assets/img/portfolio/idiomas/redimensionadas/icones/{{$idioma->imagem}}"
                 style="display: block; max-width: 100%">

            {{$idioma->titulo}}
          </label>
        </div>
      @endforeach
    </div>

    <hr>

		<div class="form-horizontal">

      <div class="form-group">
				<label class="col-sm-2 control-label">Novo Idioma</label>
			</div>

			<div class="form-group">
				<label for="inputIdiomasTitulo" class="col-sm-2 control-label">Título</label>
				<div class="col-sm-10">
					<input type="text" id="inputIdiomasTitulo" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="inputIdiomasImagem" class="col-sm-2 control-label">Imagem</label>
				<div class="col-sm-10">
					<div id="imagem-placeholder-idioma"></div>
					<input type="file" id="inputIdiomasImagem" class="form-control" name="files" data-url="painel/imagens/upload" data-path='portfolio/idiomas'>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="button" id="submitIdiomas" class="btn btn-xs btn-success"> Adicionar <span class='glyphicon glyphicon-plus'></span></button>
				</div>
			</div>

		</div>
	</div>

</div>

<hr>
