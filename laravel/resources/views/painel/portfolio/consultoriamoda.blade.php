<h4>Consultoria em Moda <a data-toggle="collapse" href="#modulo-consultoria" class="btn btn-xs btn-default btn-ativar-modulo">@if(isset($registro) && sizeof($registro->consultoriamoda)) remover <span class='glyphicon glyphicon-triangle-top'></span> @else adicionar <span class='glyphicon glyphicon-triangle-bottom'></span> @endif</a></h4>

<input type="hidden" id="inputHasConsultoria" name="has_consultoria" @if(isset($registro) && sizeof($registro->consultoriamoda)) value="1" @endif>

<div id="modulo-consultoria" class="area-modulo collapse @if(isset($registro) && sizeof($registro->consultoriamoda)) in @endif " style="margin:20px 0;">

	<div class="well">
		<div class="form-horizontal">

			<div class="form-group">
				<label for="inputMidiasConsultoria" class="col-sm-2 control-label"> Texto</label>
				<div class="col-sm-10">
					<textarea id="inputMidiasConsultoria" name="consultoria[texto]">@if(isset($registro) && sizeof($registro->consultoriamoda)) {{ $registro->consultoriamoda->texto }} @endif</textarea>
				</div>
			</div>

		</div>
	</div>

</div>

<hr>