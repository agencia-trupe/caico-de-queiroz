<h4>Ações Sociais <a data-toggle="collapse" href="#modulo-acaosocial" class="btn btn-xs btn-default btn-ativar-modulo">@if(isset($registro) && sizeof($registro->acaosocial)) remover <span class='glyphicon glyphicon-triangle-top'></span> @else adicionar <span class='glyphicon glyphicon-triangle-bottom'></span> @endif</a></h4>

<input type="hidden" id="inputHasAcaoSocial" name="has_acaosocial" @if(isset($registro) && sizeof($registro->acaosocial)) value="1" @endif>

<div id="modulo-acaosocial" class="area-modulo collapse @if(isset($registro) && sizeof($registro->acaosocial)) in @endif " style="margin:20px 0;">

	<div class="well">
		<div class="form-horizontal">

			<div class="form-group">
				<label for="inputAcaoSocialTitulo" class="col-sm-2 control-label">Título</label>
				<div class="col-sm-10">
					<input type="text" id="inputAcaoSocialTitulo" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="inputAcaoSocialTexto" class="col-sm-2 control-label">Texto</label>
				<div class="col-sm-10">
					<input type="text" id="inputAcaoSocialTexto" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="inputAcaoSocialImagem" class="col-sm-2 control-label">Imagem</label>
				<div class="col-sm-10">
					<div id="imagem-placeholder"></div>
					<input type="file" id="inputAcaoSocialImagem" class="form-control" name="files" data-url="painel/imagens/upload" data-path='portfolio/acaosocial'>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="button" id="submitAcaoSocial" class="btn btn-xs btn-success"> Adicionar <span class='glyphicon glyphicon-plus'></span></button>
					<button type="button" id="cancelarEdicaoAcaoSocial" style="display:none;" class="btn btn-xs btn-default"> Cancelar</button>
				</div>
			</div>

			<hr>

			<ul class="listaAcoesSociais list-group">
				@if(isset($registro) && sizeof($registro->acaosocial))
					@foreach($registro->acaosocial as $key => $acao)
						<li class='list-group-item'>
							<div class="row">
								<div class="col-sm-2">
									<img src="assets/img/portfolio/acaosocial/thumbs/{{ $acao->imagem }}" style='max-width:100%;'>
								</div>
								<div class="col-sm-7">
									<h5>{{ $acao->titulo }}</h5>
									<p>
										{{ $acao->text }}
									</p>
								</div>
								<div class="col-sm-3">
									<div class="btn-group">
										<a href="#" class='btn btn-xs btn-info btn-alterar-acaosocial'>alterar</a>
										<a href="#" class='btn btn-xs btn-danger btn-remover-acaosocial'>remover</a>
									</div>
								</div>
							</div>
							<input type="hidden" name='acaosocial_titulo[]' value='{{ $acao->titulo }}'>
							<input type="hidden" name='acaosocial_texto[]' value='{{ $acao->text }}'>
							<input type="hidden" name='acaosocial_imagem[]' value='{{ $acao->imagem }}'>
						</li>
					@endforeach
				@endif
			</ul>

			<div class="panel panel-default">
				<div class="panel-body">
			    	Você pode clicar e arrastar cadastros para ordená-los.
			  	</div>
			</div>

		</div>
	</div>

</div>

<hr>