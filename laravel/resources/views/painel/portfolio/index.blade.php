@extends('painel.template.index')

@section('conteudo')

<div class="container-fluid padded-bottom">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      <h2>Portfolio <small>Cadastro de Personalidades</small></h2>

      <hr>

      @include('painel.template.mensagens')

      <a href="{{ URL::route('painel.portfolio.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Personalidade</a>

    </div>

    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">

      <hr>

      <input type="search" id="buscaPersonalidades" placeholder="Busca" class="form-control">

    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      <table class="table table-striped table-bordered table-hover ">

    		<thead>
      		<tr>
            <th style="width:10%">Publicado</th>
            <th style="width:15%">Categoria</th>
            <th style="width:15%">Subcategoria</th>
      			<th>Nome</th>
      			<th><span class="glyphicon glyphicon-cog"></span></th>
      		</tr>
    		</thead>

    		<tbody>
        	@foreach ($registros as $registro)
          	<tr class="tr-row">
              <td>{!! $registro->publicar == 1 ? "<span class='glyphicon glyphicon-ok text-success'></span>" : "<span class='glyphicon glyphicon-remove text-danger'></span>" !!}</td>
              <td class="celula-cat">{{ $registro->categoria->titulo }}</td>
              <td class="celula-subcat">{{ $registro->subcategoria->titulo or '--' }}</td>
          		<td class="celula-nome">{{ $registro->nome }}</td>
              <td class="crud-actions">
              	<a href="{{ URL::route('painel.portfolio.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

                <form action="{{ URL::route('painel.portfolio.destroy', $registro->id) }}" method="post">
                  {!! csrf_field() !!}
                  <input type="hidden" name="_method" value="DELETE">
                  <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                </form>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>

    </div>
  </div>
</div>

@endsection
