<h4>Cinema | Música | Teatro | TV <a data-toggle="collapse" href="#modulo-espetaculos" class="btn btn-xs btn-default btn-ativar-modulo">@if(isset($registro) && sizeof($registro->espetaculos)) remover <span class='glyphicon glyphicon-triangle-top'></span> @else adicionar <span class='glyphicon glyphicon-triangle-bottom'></span> @endif</a></h4>

<input type="hidden" id="inputHasEspetaculos" name="has_espetaculos" @if(isset($registro) && sizeof($registro->espetaculos)) value="1" @endif>

<div id="modulo-espetaculos" class="area-modulo collapse @if(isset($registro) && sizeof($registro->espetaculos)) in @endif " style="margin:20px 0;">

	<div class="well">
		<div class="form-horizontal">

			<div class="form-group">
				<label for="inputEspetaculosTitulo" class="col-sm-2 control-label">Nome do espetáculo</label>
				<div class="col-sm-10">
					<input type="text" id="inputEspetaculosTitulo" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="inputEspetaculosAutor" class="col-sm-2 control-label">Diretor</label>
				<div class="col-sm-10">
					<input type="text" id="inputEspetaculosAutor" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="inputEspetaculosEditora" class="col-sm-2 control-label">Ano</label>
				<div class="col-sm-10">
					<input type="text" id="inputEspetaculosEditora" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="inputEspetaculosTexto" class="col-sm-2 control-label">Texto</label>
				<div class="col-sm-10">
					<input type="text" id="inputEspetaculosTexto" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="inputEspetaculosImagem" class="col-sm-2 control-label">Imagem</label>
				<div class="col-sm-10">
					<div id="imagem-placeholder-espetaculos"></div>
					<input type="file" id="inputEspetaculosImagem" class="form-control" name="files" data-url="painel/imagens/upload" data-path='portfolio/espetaculos'>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="button" id="submitEspetaculos" class="btn btn-xs btn-success"> Adicionar <span class='glyphicon glyphicon-plus'></span></button>
					<button type="button" id="cancelarEdicaoEspetaculos" style="display:none;" class="btn btn-xs btn-default"> Cancelar</button>
				</div>
			</div>

			<hr>

			<ul class="listaEspetaculos list-group">
				@if(isset($registro) && sizeof($registro->espetaculos))
					@foreach($registro->espetaculos as $key => $espet)
						<li class='list-group-item'>
							<div class="row">
								<div class="col-sm-2">
									<img src="assets/img/portfolio/espetaculos/thumbs/{{ $espet->imagem }}" style='max-width:100%;'>
								</div>
								<div class="col-sm-7">
									<h5><strong>{{ $espet->titulo }}</strong></h5>
									<p>
										<strong>Autor</strong>: {{ $espet->autor }}<br>
										<strong>Editora</strong>: {{ $espet->editora }}
									</p>
									<p>
										{{ $espet->texto }}
									</p>
								</div>
								<div class="col-sm-3">
									<div class="btn-group">
										<a href="#" class='btn btn-xs btn-info btn-alterar-espetaculos'>alterar</a>
										<a href="#" class='btn btn-xs btn-danger btn-remover-espetaculos'>remover</a>
									</div>
								</div>
							</div>
							<input type="hidden" name='espetaculos_titulo[]' value='{{ $espet->titulo }}'>
							<input type="hidden" name='espetaculos_editora[]' value='{{ $espet->editora }}'>
							<input type="hidden" name='espetaculos_autor[]' value='{{ $espet->autor }}'>
							<input type="hidden" name='espetaculos_texto[]' value='{{ $espet->texto }}'>
							<input type="hidden" name='espetaculos_imagem[]' value='{{ $espet->imagem }}'>
						</li>
					@endforeach
				@endif
			</ul>

			<div class="panel panel-default">
				<div class="panel-body">
			    	Você pode clicar e arrastar cadastros para ordená-los.
			  	</div>
			</div>

		</div>
	</div>

</div>

<hr>
