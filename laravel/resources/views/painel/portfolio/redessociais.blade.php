<h4>Redes Sociais <a data-toggle="collapse" href="#modulo-redessociais" class="btn btn-xs btn-default btn-ativar-modulo">@if(isset($registro) && sizeof($registro->redessociais)) remover <span class='glyphicon glyphicon-triangle-top'></span> @else adicionar <span class='glyphicon glyphicon-triangle-bottom'></span> @endif</a></h4>

<input type="hidden" id="inputHasRedesSociais" name="has_midiassociais" @if(isset($registro) && sizeof($registro->redessociais)) value="1" @endif>

<div id="modulo-redessociais" class="area-modulo collapse @if(isset($registro) && sizeof($registro->redessociais)) in @endif " style="margin:20px 0;">

	<div class="well">
		<div class="form-horizontal">

			<div class="form-group">
				<label for="inputMidiasSociais-instagram" class="col-sm-2 control-label"> Instagram</label>
				<div class="col-sm-10">
					<input type="text" id="inputMidiasSociais-instagram" name="midiassociais[instagram]" class="form-control" @if(isset($registro->redessociais->instagram)) value="{{ $registro->redessociais->instagram }}" @endif placeholder="formato: https://instagram.com/nome_de_usuario/">
				</div>
			</div>

			<div class="form-group">
				<label for="inputMidiasSociais-facebook" class="col-sm-2 control-label"> Facebook</label>
				<div class="col-sm-10">
					<input type="text" id="inputMidiasSociais-facebook" name="midiassociais[facebook]" class="form-control" @if(isset($registro->redessociais->facebook)) value="{{ $registro->redessociais->facebook }}" @endif placeholder="formato: https://www.facebook.com/nome_de_usuario">
				</div>
			</div>

			<div class="form-group">
				<label for="inputMidiasSociais-twitter" class="col-sm-2 control-label"> Twitter</label>
				<div class="col-sm-10">
					<input type="text" id="inputMidiasSociais-twitter" name="midiassociais[twitter]" class="form-control" @if(isset($registro->redessociais->twitter)) value="{{ $registro->redessociais->twitter }}" @endif placeholder="formato: https://www.twitter.com/nome_de_usuario">
				</div>
			</div>

			<div class="form-group">
				<label for="inputMidiasSociais-vimeo" class="col-sm-2 control-label"> Vimeo</label>
				<div class="col-sm-10">
					<input type="text" id="inputMidiasSociais-vimeo" name="midiassociais[vimeo]" class="form-control" @if(isset($registro->redessociais->vimeo)) value="{{ $registro->redessociais->vimeo }}" @endif placeholder="formato: https://vimeo.com/nome_de_usuario">
				</div>
			</div>

			<div class="form-group">
				<label for="inputMidiasSociais-youtube" class="col-sm-2 control-label"> Youtube</label>
				<div class="col-sm-10">
					<input type="text" id="inputMidiasSociais-youtube" name="midiassociais[youtube]" class="form-control" @if(isset($registro->redessociais->youtube)) value="{{ $registro->redessociais->youtube }}" @endif placeholder="formato: https://www.youtube.com/user/nome_de_usuario">
				</div>
			</div>

			<div class="form-group">
				<label for="inputMidiasSociais-linkedin" class="col-sm-2 control-label"> Linkedin</label>
				<div class="col-sm-10">
					<input type="text" id="inputMidiasSociais-linkedin" name="midiassociais[linkedin]" class="form-control" @if(isset($registro->redessociais->linkedin)) value="{{ $registro->redessociais->linkedin }}" @endif placeholder="formato: https://br.linkedin.com/in/nome_de_usuario">
				</div>
			</div>

			<div class="form-group">
				<label for="inputMidiasSociais-flickr" class="col-sm-2 control-label"> Flickr</label>
				<div class="col-sm-10">
					<input type="text" id="inputMidiasSociais-flickr" name="midiassociais[flickr]" class="form-control" @if(isset($registro->redessociais->flickr)) value="{{ $registro->redessociais->flickr }}" @endif placeholder="formato: https://www.flickr.com/photos/nome_de_usuario/">
				</div>
			</div>

			<div class="form-group">
				<label for="inputMidiasSociais-pinterest" class="col-sm-2 control-label"> Pinterest</label>
				<div class="col-sm-10">
					<input type="text" id="inputMidiasSociais-pinterest" name="midiassociais[pinterest]" class="form-control" @if(isset($registro->redessociais->pinterest)) value="{{ $registro->redessociais->pinterest }}" @endif placeholder="formato: https://www.pinterest.com/nome_de_usuario/">
				</div>
			</div>

			<div class="form-group">
				<label for="inputMidiasSociais-gplus" class="col-sm-2 control-label"> Google Plus</label>
				<div class="col-sm-10">
					<input type="text" id="inputMidiasSociais-gplus" name="midiassociais[gplus]" class="form-control" @if(isset($registro->redessociais->gplus)) value="{{ $registro->redessociais->gplus }}" @endif placeholder="formato: https://plus.google.com/id_de_usuario">
				</div>
			</div>

			<div class="form-group">
				<label for="inputMidiasSociais-tumblr" class="col-sm-2 control-label"> Tumblr</label>
				<div class="col-sm-10">
					<input type="text" id="inputMidiasSociais-tumblr" name="midiassociais[tumblr]" class="form-control" @if(isset($registro->redessociais->tumblr)) value="{{ $registro->redessociais->tumblr }}" @endif placeholder="formato: https://nome_de_usuario.tumblr.com">
				</div>
			</div>

			<div class="form-group">
				<label for="inputMidiasSociais-imdb" class="col-sm-2 control-label"> IMDb</label>
				<div class="col-sm-10">
					<input type="text" id="inputMidiasSociais-imdb" name="midiassociais[imdb]" class="form-control" @if(isset($registro->redessociais->imdb)) value="{{ $registro->redessociais->imdb }}" @endif placeholder="formato: http://imdb.com/name/id_de_usuario">
				</div>
			</div>
		</div>
	</div>

</div>

<hr>
