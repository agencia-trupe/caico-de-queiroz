<h4>Vídeos <a data-toggle="collapse" href="#modulo-videos" class="btn btn-xs btn-default btn-ativar-modulo">@if(isset($registro) && sizeof($registro->videos)) remover <span class='glyphicon glyphicon-triangle-top'></span> @else adicionar <span class='glyphicon glyphicon-triangle-bottom'></span> @endif</a></h4>

<input type="hidden" id="inputHasVideos" name="has_videos" @if(isset($registro) && sizeof($registro->videos)) value="1" @endif>

<div id="modulo-videos" class="area-modulo collapse @if(isset($registro) && sizeof($registro->videos)) in @endif " style="margin:20px 0;">

	<div class="well">
		<div class="form-horizontal">

			<div class="form-group">
				<label for="inputVideoTitulo" class="col-sm-2 control-label">Título</label>
				<div class="col-sm-10">
					<input type="text" id="inputVideoTitulo" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label for="inputVideoUrl" class="col-sm-2 control-label">Endereço (Vimeo ou Youtube)</label>
				<div class="col-sm-10">
					<input type="text" id="inputVideoUrl" class="form-control">
				</div>
			</div>

			<input type="hidden" id="inputVideoId">

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="button" id="submitVideo" class="btn btn-xs btn-success"> Adicionar <span class='glyphicon glyphicon-plus'></span></button>
					<button type="button" id="cancelarEdicaoVideo" style="display:none;" class="btn btn-xs btn-default"> Cancelar</button>
				</div>
			</div>

			<hr>

			<ul class="listaVideos list-group">
				@if(isset($registro) && sizeof($registro->videos))
					@foreach($registro->videos as $key => $video)
						<li class='list-group-item'>

							<a href="{{ prep_url($video->url) }}" class='btn btn-link btn-sm' target="_blank">{{ $video->titulo }}</a>

							<div class="btn-group pull-right">
								<a href="#" class='btn btn-sm btn-info btn-alterar-video'>alterar</a>
								<a href="#" class='btn btn-sm btn-danger btn-remover-video'>remover</a>
							</div>

							<input type="hidden" name='videos_id[]' value='{{ $video->id }}'>
							<input type="hidden" name='videos_titulo[]' value='{{ $video->titulo }}'>
							<input type="hidden" name='videos_url[]' value='{{ $video->url }}'>

						</li>
					@endforeach
				@endif
			</ul>

			<div class="panel panel-default">
				<div class="panel-body">
			    	Você pode clicar e arrastar os vídeos para ordená-los.
			  	</div>
			</div>

		</div>
	</div>

</div>

<hr>