<h4>Créditos <a data-toggle="collapse" href="#modulo-creditos" class="btn btn-xs btn-default btn-ativar-modulo">@if(isset($registro) && sizeof($registro->creditos)) remover <span class='glyphicon glyphicon-triangle-top'></span> @else adicionar <span class='glyphicon glyphicon-triangle-bottom'></span> @endif</a></h4>

<input type="hidden" id="inputHasCreditos" name="has_creditos" @if(isset($registro) && sizeof($registro->creditos)) value="1" @endif>

<div id="modulo-creditos" class="area-modulo collapse @if(isset($registro) && sizeof($registro->creditos)) in @endif " style="margin:20px 0;">

	<div class="well">
		<div class="form-horizontal">

			<div class="form-group">
				<label for="inputMidiasCreditos" class="col-sm-2 control-label"> Texto</label>
				<div class="col-sm-10">
					<textarea id="inputMidiasCreditos" name="creditos[texto]">@if(isset($registro) && sizeof($registro->creditos)) {{ $registro->creditos->texto }} @endif</textarea>
				</div>
			</div>

		</div>
	</div>

</div>

<hr>