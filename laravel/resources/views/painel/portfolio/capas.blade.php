<h4>Capas <a data-toggle="collapse" href="#modulo-capas" class="btn btn-xs btn-default btn-ativar-modulo">@if(isset($registro) && sizeof($registro->capas)) remover <span class='glyphicon glyphicon-triangle-top'></span> @else adicionar <span class='glyphicon glyphicon-triangle-bottom'></span> @endif</a></h4>

<input type="hidden" id="inputHasCapas" name="has_capas" @if(isset($registro) && sizeof($registro->capas)) value="1" @endif>

<div id="modulo-capas" class="area-modulo collapse @if(isset($registro) && sizeof($registro->capas)) in @endif " style="margin:20px 0;">

	<div class="well">

		<div class="multiUpload">
			<div class="icone">
				<span class="glyphicon glyphicon-open"></span>
				<span class="glyphicon glyphicon-refresh"></span>
			</div>
			<p>
				Escolha as imagens para o cadastro de Capas. Você pode selecionar mais de um arquivo ao mesmo tempo. Você também pode arrastar e soltar arquivos nesta área para começar a enviar.<br>
				Se preferir também pode utilizar o botão abaixo para selecioná-las.
			</p>
			<input id="fileupload-capas" class="fileupload" type="file" name="files" data-url="painel/imagens/upload" data-limite='0' data-path='portfolio/capas' data-fieldname='imagem_capas' multiple>

		</div>

		<div class="limiteImagensAtingido" style='display:none;'>
			<div class="panel panel-default">
				<div class="panel-body bg-info">
			    	Número máximo de imagens (20) atingido.
			  	</div>
			</div>
		</div>

		<div class="listaImagens">
			@if(isset($registro) && sizeof($registro->capas))
				@foreach($registro->capas as $k => $foto)
					<div class='projetoImagem'>
			        	<img src="assets/img/portfolio/capas/thumbs/{{ $foto->imagem }}">
			        	<input type='hidden' name='imagem_capas[]' value="{{ $foto->imagem }}">
			        	<a href='#' class='btn btn-sm btn-danger btn-remover' title='remover a imagem'><span class='glyphicon glyphicon-remove-sign'></span> <strong>remover imagem</strong></a>
		        	</div>
				@endforeach
			@endif
		</div>

		<div class="panel panel-default">
			<div class="panel-body">
		    	Você pode clicar e arrastar as imagens para ordená-las.
		  	</div>
		</div>
	</div>

</div>

<hr>