<h4>Extras <a data-toggle="collapse" href="#modulo-extras" class="btn btn-xs btn-default btn-ativar-modulo">@if(isset($registro) && sizeof($registro->extras)) remover <span class='glyphicon glyphicon-triangle-top'></span> @else adicionar <span class='glyphicon glyphicon-triangle-bottom'></span> @endif</a></h4>

<input type="hidden" id="inputHasExtras" name="has_extras" @if(isset($registro) && sizeof($registro->extras)) value="1" @endif>

<div id="modulo-extras" class="area-modulo collapse @if(isset($registro) && sizeof($registro->extras)) in @endif " style="margin:20px 0;">

	<div class="well">
		<div class="form-horizontal">

			<div class="form-group">
				<label for="inputMidiasExtras" class="col-sm-2 control-label"> Texto</label>
				<div class="col-sm-10">
					<textarea id="inputMidiasExtras" name="extras[texto]">@if(isset($registro) && sizeof($registro->extras)) {{ $registro->extras->texto }} @endif</textarea>
				</div>
			</div>

		</div>
	</div>

</div>

<hr>