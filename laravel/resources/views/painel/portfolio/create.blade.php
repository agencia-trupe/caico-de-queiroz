@extends('painel.template.index')

@section('conteudo')

    <div class="container-fluid padded-bottom">
    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		      	<h2>
		        	Portfolio <small>Adicionar Personalidade</small>
		        </h2>

		        <hr>

		        @include('painel.template.mensagens')

				<form action="{{ URL::route('painel.portfolio.store') }}" method="post" enctype="multipart/form-data">

					{!! csrf_field() !!}

					<div class="form-group">
						<label for="inputCategoria">Categoria</label>
						<select name="portfolio_categorias_id" id="inputCategoria" class="form-control" required>
							<option value="">Selecione uma Categoria</option>
							@foreach($listaCategorias as $categoria)
								<option value="{{ $categoria->id }}" @if(old('portfolio_categorias_id') == $categoria->id) selected @endif >{{ $categoria->titulo }}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group" id='contemSelectSubcategorias'>
						<label for="inputSubcategoria">Subcategoria</label>
						<select name="portfolio_subcategorias_id" id="inputSubcategoria" class="form-control">
							<option value="">Selecione uma Subcategoria</option>
						</select>
						<input type="hidden" id="portfolio_subcategorias_id_selecionado" value="{{ old('portfolio_subcategorias_id') }}">
					</div>

			    	<div class="form-group">
						<label for="inputNome">Nome</label>
						<input type="text" class="form-control" id="inputNome" name="nome"  value="{{ old('nome') }}" required>
					</div>

					<div class="form-group">
						<label for="inputCorNome">Cor do Nome na página do Portfolio</label>
						<select name="cor_nome" id="inputCorNome" required class='form-control'>
							<option value="FFF" @if(old('cor_nome') == 'FFF') selected @endif >BRANCO</option>
							<option value="000" @if(old('cor_nome') == '000') selected @endif >PRETO</option>
						</select>
					</div>

					<div class="well">
						<div class="form-group">
							<label for="inputImagem">Imagem de Entrada</label>
							<input type="file" class="form-control" id="inputImagem" name="imagem_entrada" required>
						</div>
					</div>

          <div class="well">
						<div class="form-group">
							<label for="inputThumbHome">Thumbnail para a Home</label>
							<input type="file" class="form-control" id="inputThumbHome" name="thumb_home" required>
						</div>
					</div>

					<div class="well">
						<div class="form-group">
							<label for="inputRelease">Release</label>
							<input type="file" class="form-control" id="inputRelease" name="arquivo_release">
						</div>
					</div>

					<div class="form-group">
						<label for="inputLinkSite">Link para o site</label>
						<input type="text" class="form-control" id="inputLinkSite" name="link_site"  value="{{ old('link_site') }}">
					</div>

          <div class="well">
            <div class="row">
              <label class="col-sm-6">
                <input type="radio" name="publicar" value="publicar" required @if(old('publicar') == 'publicar') checked @endif > Publicar
              </label>
              <label class="col-sm-6">
                <input type="radio" name="publicar" value="rascunho" @if(old('publicar') == 'rascunho') checked @endif > Salvar como rascunho
              </label>
            </div>
          </div>

					<hr>

					<h3>Módulos</h3>

					<div class="panel panel-default">
						<div class="panel-body">
					    	Clique nos módulos para inserí-los no cadastro.
					  	</div>
					</div>

					@include('painel.portfolio.paralaxe')

					@include('painel.portfolio.fotos')

					@include('painel.portfolio.personagens', compact('registro'))

					@include('painel.portfolio.capas')

					@include('painel.portfolio.publicidade')

					@include('painel.portfolio.desfiles')

					@include('painel.portfolio.videos')

					@include('painel.portfolio.acaosocial')

					@include('painel.portfolio.editorialdemoda')

					@include('painel.portfolio.personalstylist')

					@include('painel.portfolio.redessociais')

					@include('painel.portfolio.palestras')

					@include('painel.portfolio.consultoriamoda')

					@include('painel.portfolio.publicacoes')

					@include('painel.portfolio.extras')

					@include('painel.portfolio.redcarpet')

					@include('painel.portfolio.espetaculos')

					@include('painel.portfolio.workshops')

					@include('painel.portfolio.creditos')

          @include('painel.portfolio.idiomas')

          @include('painel.portfolio.premios')

					<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

					<a href="{{ URL::route('painel.portfolio.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

				</form>
			</div>
		</div>
    </div>

@endsection
