@extends('painel.template.index')

@section('conteudo')

<div class="container-fluid padded-bottom">
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">

		  <h2>Adicionar Idioma</h2>

		  <hr>

		  @include('painel.template.mensagens')

			<form action="{{ URL::route('painel.idiomas.store') }}" method="post" enctype="multipart/form-data">

				{!! csrf_field() !!}

			  <div class="form-group">
					<label for="inputTitulo">Título</label>
					<input type="text" class="form-control" id="inputTitulo" name="titulo"  value="{{ old('titulo') }}" required>
				</div>

        <div class="form-group">
					<label for="inputImagem">Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem" required>
				</div>

				<hr>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{ URL::route('painel.idiomas.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</form>
		</div>
	</div>
</div>

@endsection
