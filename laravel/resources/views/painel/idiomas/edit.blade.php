@extends('painel.template.index')

@section('conteudo')

<div class="container-fluid padded-bottom">
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">

		  <h2>Alterar Idioma</h2>

		  <hr>

		  @include('painel.template.mensagens')

			<form action="{{ URL::route('painel.idiomas.update', $registro->id) }}" method="post" enctype="multipart/form-data">

				{!! csrf_field() !!}

        <input type="hidden" name="_method" value="PUT">

			  <div class="form-group">
					<label for="inputTitulo">Título</label>
					<input type="text" class="form-control" id="inputTitulo" name="titulo"  value="{{ $registro->titulo }}" required>
				</div>

        <div class="form-group">
          @if($registro->imagem)
            Imagem Atual<br>
            <img src="assets/img/portfolio/idiomas/redimensionadas/icones/{{$registro->imagem}}"><br>
          @endif
          <label for="inputImagem">Trocar Imagem</label>
          <input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>

				<hr>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{ URL::route('painel.idiomas.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</form>
		</div>
	</div>
</div>

@endsection
