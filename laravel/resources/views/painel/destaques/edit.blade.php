@extends('painel.template.index')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
        
        <h2>Editar Destaques</h2>

        <hr>

        @include('painel.template.mensagens')

        <form action="{{ URL::route('painel.destaques.update', $registro->id) }}" method="post" enctype="multipart/form-data">
          <input type="hidden" name="_method" value="PUT">
          {!! csrf_field() !!}

          <label for="inputImagem">Destaque 1</label>

          <div class="form-group">
          <label for="inputImagem">Imagem</label>
						@if($registro->imagem_1)
              <div>
							  <img src="assets/img/destaques/{{$registro->imagem_1}}" class="img-thumbnail">
              </div>
						@endif
						<input type="file" class="form-control" id="inputImagem" name="imagem_1">
					</div>

          <div class="form-group">
						<label for="inputLink1">Link</label>
						<input type="text" class="form-control" id="inputLink1" name="link_1" value="{{ $registro->link_1 }}">
					</div>

          <hr>

          <label for="inputImagem">Destaque 2</label>

          <div class="form-group">
          <label for="inputImagem">Imagem</label>
						@if($registro->imagem_2)
              <div>
							  <img class="img-thumbnail" src="assets/img/destaques/{{$registro->imagem_2}}">
              </div>
						@endif
						<input type="file" class="form-control" id="inputImagem" name="imagem_2">
					</div>

          <div class="form-group">
						<label for="inputLink2">Link</label>
						<input type="text" class="form-control" id="inputLink2" name="link_2" value="{{ $registro->link_2 }}">
					</div>

          <hr>
          
          <label for="inputImagem">Destaque 3</label>

          <div class="form-group">
          <label for="inputImagem">Imagem</label>
						@if($registro->imagem_3)
              <div>
							  <img class="img-thumbnail" src="assets/img/destaques/{{$registro->imagem_3}}">
              </div>
						@endif
						<input type="file" class="form-control" id="inputImagem" name="imagem_3">
					</div>

          <div class="form-group">
						<label for="inputLink3">Link</label>
						<input type="text" class="form-control" id="inputLink3" name="link_3" value="{{ $registro->link_3 }}">
					</div>

          <hr>
          
          <label for="inputImagem">Destaque 4</label>

          <div class="form-group">
          <label for="inputImagem">Imagem</label>
						@if($registro->imagem_4)
              <div>
							  <img class="img-thumbnail" src="assets/img/destaques/{{$registro->imagem_4}}">
              </div>
						@endif
						<input type="file" class="form-control" id="inputImagem" name="imagem_4">
					</div>

          <div class="form-group">
						<label for="inputLink4">Link</label>
						<input type="text" class="form-control" id="inputLink4" name="link_4" value="{{ $registro->link_4 }}">
					</div>

          <hr>
          
          <label for="inputImagem">Destaque 5</label>

          <div class="form-group">
          <label for="inputImagem">Imagem</label>
						@if($registro->imagem_5)
              <div>
							  <img class="img-thumbnail" src="assets/img/destaques/{{$registro->imagem_5}}">
              </div>
						@endif
						<input type="file" class="form-control" id="inputImagem" name="imagem_5">
					</div>

          <div class="form-group">
						<label for="inputLink5">Link</label>
						<input type="text" class="form-control" id="inputLink5" name="link_5" value="{{ $registro->link_5 }}">
					</div>

          <hr>
          
          <label for="inputImagem">Destaque 6</label>

          <div class="form-group">
          <label for="inputImagem">Imagem</label>
						@if($registro->imagem_6)
              <div>
							  <img class="img-thumbnail" src="assets/img/destaques/{{$registro->imagem_6}}">
              </div>
						@endif
						<input type="file" class="form-control" id="inputImagem" name="imagem_6">
					</div>

          <div class="form-group">
						<label for="inputLink6">Link</label>
						<input type="text" class="form-control" id="inputLink6" name="link_6" value="{{ $registro->link_6 }}">
					</div>

          <hr>
          
          <label for="inputImagem">Destaque 7</label>

          <div class="form-group">
          <label for="inputImagem">Imagem</label>
						@if($registro->imagem_7)
              <div>
							  <img class="img-thumbnail" src="assets/img/destaques/{{$registro->imagem_7}}">
              </div>
						@endif
						<input type="file" class="form-control" id="inputImagem" name="imagem_7">
					</div>

          <div class="form-group">
						<label for="inputLink7">Link</label>
						<input type="text" class="form-control" id="inputLink7" name="link_7" value="{{ $registro->link_7 }}">
					</div>

          <hr>
          
          <label for="inputImagem">Destaque 8</label>

          <div class="form-group">
          <label for="inputImagem">Imagem</label>
						@if($registro->imagem_8)
              <div>
							  <img class="img-thumbnail" src="assets/img/destaques/{{$registro->imagem_8}}">
              </div>
						@endif
						<input type="file" class="form-control" id="inputImagem" name="imagem_8">
					</div>

          <div class="form-group">
						<label for="inputLink8">Link</label>
						<input type="text" class="form-control" id="inputLink8" name="link_8" value="{{ $registro->link_8 }}">
					</div>

          <hr>
          
          <label for="inputImagem">Destaque 9</label>

          <div class="form-group">
          <label for="inputImagem">Imagem</label>
						@if($registro->imagem_9)
              <div>
							  <img class="img-thumbnail" src="assets/img/destaques/{{$registro->imagem_9}}">
              </div>
						@endif
						<input type="file" class="form-control" id="inputImagem" name="imagem_9">
					</div>

          <div class="form-group">
						<label for="inputLink9">Link</label>
						<input type="text" class="form-control" id="inputLink9" name="link_9" value="{{ $registro->link_9 }}">
					</div>

          <hr>
          
          <label for="inputImagem">Destaque 10</label>

          <div class="form-group">
          <label for="inputImagem">Imagem</label>
						@if($registro->imagem_10)
              <div>
							  <img class="img-thumbnail" src="assets/img/destaques/{{$registro->imagem_10}}">
              </div>
						@endif
						<input type="file" class="form-control" id="inputImagem" name="imagem_10">
					</div>

          <div class="form-group">
						<label for="inputLink10">Link</label>
						<input type="text" class="form-control" id="inputLink10" name="link_10" value="{{ $registro->link_10 }}">
          </div>
          
          <hr>
          
          <label for="inputImagem">Destaque 11</label>

          <div class="form-group">
          <label for="inputImagem">Imagem</label>
						@if($registro->imagem_11)
              <div>
							  <img class="img-thumbnail" src="assets/img/destaques/{{$registro->imagem_11}}">
              </div>
						@endif
						<input type="file" class="form-control" id="inputImagem" name="imagem_11">
					</div>

          <div class="form-group">
						<label for="inputLink10">Link</label>
						<input type="text" class="form-control" id="inputLink10" name="link_11" value="{{ $registro->link_11 }}">
					</div>

          <hr>

          <h2>Destaques em Vídeo</h2>

          <div class="form-group">
						<label for="inputVideo1">Vídeo 1 (URL do youtube)</label><br>
            {!! $video1->getEmbed(600) !!}
						<input type="text" class="form-control" id="inputVideo1" name="embed_1" value="{{ $registro->embed_1 }}">
					</div>
          
          <div class="form-group">
						<label for="inputVideo2">Vídeo 2 (URL do youtube)</label><br>
            {!! $video2->getEmbed(600) !!}
						<input type="text" class="form-control" id="inputVideo2" name="embed_2" value="{{ $registro->embed_2 }}">
          </div>
          
          <hr>

          <h2>Banners</h2>

          <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
              @include('painel.template.imagens-upload', ['registro' => $registro, 'path' => 'banners_destaques', 'hide_ordering' => true])
            </div>
          </div>

          <hr>

          <button type="submit" title="Alterar" class="btn btn-success">Alterar</button>
          <a href="{{ URL::route('painel.destaques.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>
        </form>
      </div>
    </div>
  </div>

@endsection