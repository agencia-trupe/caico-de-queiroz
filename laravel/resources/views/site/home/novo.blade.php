@extends('site.template.default')

@section('conteudo')

	<div class="conteudo links-home-remodelada {{$classe}}">

    <nav>
			<div class="marca-medium">
        <img src="assets/img/marca/{{$marca->imagem}}" alt="Caíco de Queiroz" />
      </div>
			<div class="superior">
        <ul>
          <li>
            <a href="#" id="link-artistas-esclusivos">ARTISTAS EXCLUSIVOS</a>
          </li>
          <li>
            <a href="about">ABOUT</a>
          </li>
					<li class="mobile-item">
						<div class="marca-small">
			        <img src="assets/img/marca/{{$marca->imagem}}" alt="Caíco de Queiroz" />
			      </div>
					</li>
          <li class="small-inline">
            <div id="lista-redes-sociais-home">

        			@if($about->instagram)
        				<a href="{{ prep_url($about->instagram) }}" title="Instagram" target="_blank"><img src="assets/img/icones/about/icone-instagram.png" alt="instagram"></a>
        			@endif

        			@if($about->twitter)
        				<a href="{{ prep_url($about->twitter) }}" title="Twitter" target="_blank"><img src="assets/img/icones/about/icone-twitter.png" alt="twitter"></a>
        			@endif

        			@if($about->facebook)
        				<a href="{{ prep_url($about->facebook) }}" title="Facebook" target="_blank"><img src="assets/img/icones/about/icone-facebook.png" alt="facebook"></a>
        			@endif

        			@if($about->tumblr)
        				<a href="{{ prep_url($about->tumblr) }}" title="Tumblr" target="_blank"><img src="assets/img/icones/about/icone-tumblr-v2.png" alt="tumblr"></a>
        			@endif

        			@if($about->linkedin)
        				<a href="{{ prep_url($about->linkedin) }}" title="LinkedIn" target="_blank"><img src="assets/img/icones/about/icone-linkedin.png" alt="linkedin"></a>
        			@endif

        			@if($about->pinterest)
        				<a href="{{ prep_url($about->pinterest) }}" title="Pinterest" target="_blank"><img src="assets/img/icones/about/icone-pinterest.png" alt="pinterest"></a>
        			@endif

        			@if($about->youtube)
        				<a href="{{ prep_url($about->youtube) }}" title="Youtube" target="_blank"><img src="assets/img/icones/about/icone-youtube.png" alt="youtube"></a>
        			@endif

        			@if($about->periscope)
        				<a href="{{ prep_url($about->periscope) }}" title="Periscope" target="_blank"><img src="assets/img/icones/about/icone-periscope.png" alt="periscope"></a>
        			@endif

        			@if($about->vimeo)
        				<a href="{{ prep_url($about->vimeo) }}" title="Vimeo" target="_blank"><img src="assets/img/icones/about/icone-vimeo.png" alt="vimeo"></a>
        			@endif

        			@if($about->googleplus)
        				<a href="{{ prep_url($about->googleplus) }}" title="Google+" target="_blank"><img src="assets/img/icones/about/icone-googleplus.png" alt="googleplus"></a>
        			@endif

        			@if($about->wordpress)
        				<a href="{{ prep_url($about->wordpress) }}" title="Wordpress" target="_blank"><img src="assets/img/icones/about/icone-wordpress.png" alt="wordpress"></a>
        			@endif

        			@if($about->snapchat)
        				<a href="https://www.snapchat.com/" title="snap: {{ $about->snapchat }}" id="link-snapchat" target="_blank"><img src="assets/img/icones/about/icone-snapchat-v2.png" alt="snapchat"><span>snap: {{ $about->snapchat }}</span></a>
        			@endif

              <a href="http://www.imdb.com/user/ur27699259/?ref_=nb_usr_prof_0" target="_blank"><img src="assets/img/icones/about/icone-imdb.png" alt="imdb"></a>

							<a href="#" id="toggle-menu">[ MENU ]</a>
          </li>
        </ul>
      </div>
      <div class="inferior">
        <ul>
          <li><a href="https://caicodequeiroz.wordpress.com/" target="_blank" title="BLOG">BLOG</a></li>
          <li><a href="destaques" title="DESTAQUES">DESTAQUES</a></li>
          <li><a href="imprensa" title="IMPRENSA">IMPRENSA</a></li>
          <li><a href="publicacoes" title="PUBLICAÇÕES">PUBLICAÇÕES</a></li>
          <li><a href="acao-social" title="AÇÃO SOCIAL">AÇÃO SOCIAL</a></li>
          <li><a href="juridico" title="JURÍDICO">JURÍDICO</a></li>
          <li><a href="parcerias" title="PARCERIAS">PARCERIAS</a></li>
          <li><a href="licenciamento" title="LICENCIAMENTO">LICENCIAMENTO</a></li>
          <li><a href="contato" title="CONTATO">CONTATO</a></li>
        </ul>
      </div>
    </nav>

    <header>

      <div class="marca">
        <img src="assets/img/marca/{{$marca->imagem}}" alt="Caíco de Queiroz" />
      </div>

      <div class="banners">
        <div id="novo-banner-home">

						<div id="pager"></div>

          @foreach($banners as $banner)
            <div class="banner">

              @if($banner->link)
                <a href="{{$banner->link}}">
              @endif

              @if($banner->imagem)
                <div class="imagem">
                  <img src="assets/img/banners/{{$banner->imagem}}" alt="{{$banner->titulo}}" />
                </div>
              @endif

              @if($banner->titulo)
                <div class="texto">
                  <h1>{{$banner->titulo}}</h1>
                  @if($banner->frase)
                    <h2>{{$banner->frase}}</h2>
                  @endif
                </div>
              @endif

              @if($banner->link)
                </a>
              @endif

            </div>
          @endforeach

        </div>
      </div>

    </header>

    <div id="mosaico">
      @foreach($mosaico as $link)
				<div class="item-mosaico">

					@if(isset($link['nome']))

						<a href="portfolio/entrada/{{ $link['slug'] }}" class="link-personalidade" data-linkid="{{$link['id']}}">
							<img src="assets/img/portfolio/thumbs-home/{{$link['thumb_home']}}" alt="{{$link['nome']}}" />
							<span>{{$link['nome']}}</span>
		        </a>

					@elseif(isset($link['portfolio_categorias_id']))

						<a href="#" title="{{$link['titulo']}}" class="link-secao subcategoria {{$link['classe_menu']}}" data-cat="{{$link['titulo_categoria']}}" data-subcat="{{$link['titulo']}}" data-linkid="{{$link['id']}}">
							<div class="texto">
								<h1>{{$link['titulo_categoria']}}</h1>
								<h2>{{$link['titulo']}}</h2>
							</div>
		        </a>

					@else

						<a href="#" title="{{$link['titulo']}}" class="link-secao categoria {{$link['classe_menu']}}" data-cat="{{$link['titulo']}}" data-linkid="{{$link['id']}}">
							<h1>{{$link['titulo']}}</h1>
						</a>

					@endif

				</div>
      @endforeach
    </div>

		@include('site.template.menu')

	</div>

@stop
