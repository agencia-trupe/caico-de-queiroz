@extends('site.template.default')

@section('conteudo')

	<div class="conteudo">

		<div class="contemTextoImagem">
			<section>
				<div class="padding-interno">
					<h1>+ jurídico</h1>

					{!! $juridico->texto !!}
				</div>
			</section>
			<aside style="background-image:url(assets/img/juridico/{{ $juridico->imagem }});"></aside>
		</div>


		@include('site.template.menu')
	</div>

@stop