<div class="menu">
	<a href="#contentMenuPrincipal" title="Institucional" class="link link-submenu"><span>INSTITUCIONAL</span></a>
	<a href="#contentMenuAgenciados" title="Artistas Exclusivos" class="link link-agenciados"><span>ARTISTAS EXCLUSIVOS</span></a>
</div>

<div class="contemSubMenu escondido contemMenuPrincipal" id="contentMenuPrincipal">
	<ul class="submenu">
		<li><a href="about" @if(str_is('site.about*', Route::currentRouteName())) class="ativo" @endif title="About">ABOUT</a></li>
    <li><a href="destaques" @if(str_is('site.destaques*', Route::currentRouteName())) class="ativo" @endif title="Destaques">DESTAQUES</a></li>
		<li><a href="https://caicodequeiroz.wordpress.com/" title="Blog" target="_blank">BLOG</a></li>
		<li><a href="imprensa" @if(str_is('site.imprensa*', Route::currentRouteName())) class="ativo" @endif title="Imprensa">IMPRENSA</a></li>
		<li><a href="publicacoes" @if(str_is('site.publicacoes*', Route::currentRouteName())) class="ativo" @endif title="Publicações">PUBLICAÇÕES</a></li>
		<li><a href="acao-social" @if(str_is('site.acaosocial*', Route::currentRouteName())) class="ativo" @endif title="Ação Social">AÇÃO SOCIAL</a></li>
		<li><a href="juridico" @if(str_is('site.juridico*', Route::currentRouteName())) class="ativo" @endif title="Jurídico">JURÍDICO</a></li>
		<li><a href="parcerias" @if(str_is('site.parcerias*', Route::currentRouteName())) class="ativo" @endif title="Parcerias">PARCERIAS</a></li>
		<li><a href="licenciamento" @if(str_is('site.licenciamento*', Route::currentRouteName())) class="ativo" @endif title="Licenciamento">LICENCIAMENTO</a></li>
		<li><a href="contato" @if(str_is('site.contato*', Route::currentRouteName())) class="ativo" @endif title="Contato">CONTATO</a></li>
	</ul>
</div>

<div class="contemSubMenu escondido contemMenuAgenciados" id="contentMenuAgenciados">
	<ul class="submenu menuAgenciados">
		@if(sizeof($listaCategorias))
			@foreach($listaCategorias as $k => $cat)

				@if(!in_array($cat->id, [9, 11]))

					<li>
						<a href="#" title="{{ $cat->titulo }}" class="{{ $cat->classe_menu }} @if(isset($marcarSlugCategoria) && $cat->slug == $marcarSlugCategoria) ativo @endif ">{{ $cat->titulo }}</a>
						@if(sizeof($cat->subcategorias))
							<ul class="subcategorias">
								@foreach($cat->subcategorias as $j => $sub)
									@if(!in_array($sub->id, [13, 22]))
										<li>
											<a href="#" @if(isset($marcarSlugCategoria) && $cat->slug == $marcarSlugCategoria && isset($marcarSlugSubcategoria) && $sub->slug == $marcarSlugSubcategoria) class="ativo-inicial" @endif title="{{ $sub->titulo }}">{{ $sub->titulo }}</a>
											@if(sizeof($sub->personalidades))
												<ul class="subpersonalidades com-wrapper">
													<ul class="wrapper">
														@foreach($sub->personalidades as $i => $perso)
															@if($perso->publicar == 1)
																<li><a href="portfolio/entrada/{{ $perso->slug }}" @if(isset($marcarSlugPersonalidade) && $perso->slug == $marcarSlugPersonalidade) class="ativo" @endif title="{{ $perso->nome }}">{{ $perso->nome }}</a></li>
															@endif
														@endforeach
													</ul>
												</ul>
											@endif
										</li>
									@endif
								@endforeach
							</ul>
						@else
							@if(sizeof($cat->personalidades))
								<ul class="subcategorias subpersonalidades">
									@foreach($cat->personalidades as $i => $perso)
										@if($perso->publicar == 1)
											<li><a href="portfolio/entrada/{{ $perso->slug }}" @if(isset($marcarSlugPersonalidade) && $perso->slug == $marcarSlugPersonalidade) class="ativo-inicial" @endif title="{{ $perso->nome }}">{{ $perso->nome }}</a></li>
										@endif
									@endforeach
								</ul>
							@endif
						@endif
					</li>

				@endif

			@endforeach
			<li>
				<a href="#" title="TODOMUNDO" class="link-branco link-todomundo @if(isset($marcarSlugCategoria) && $cat->slug == 'todomundo') ativo @endif ">TODOMUNDO</a>
				<ul class="subcategorias subpersonalidades subtodomundo">
					<ul class="wrapper">

						@foreach($todomundo['link_principal'] as $i => $perso)

							<li>
								<a href="portfolio/entrada/{{ $perso['slug'] }}"
									@if(isset($marcarSlugPersonalidade) && $perso['slug'] == $marcarSlugPersonalidade)
										class="ativo-inicial"
									@endif
									title="{{ $perso['nome'] }}">
									{{ $perso['nome'] }}
								</a>
							</li>

							@if(isset($todomundo['links'][$perso['nome']]) && sizeof($todomundo['links'][$perso['nome']]) > 0)
								<li class='segunda-linha'>
								@foreach($todomundo['links'][$perso['nome']] as $c => $p)
									@if($c > 0)
										<small>&bull;</small>
									@endif
									<a href="portfolio/entrada/{{ $p['slug'] }}" title="{{ $p['nome'] }}">
										<small>{{$p['categoria_titulo']}}</small>
									</a>
								@endforeach
							</li>
							@endif

						@endforeach
					</ul>
				</ul>
			</li>
		@endif
	</ul>
</div>
