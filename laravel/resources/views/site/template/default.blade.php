<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2015 Trupe Design" />
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <meta name="keywords" content="" />

	<title>Caíco de Queiroz</title>
	<meta name="description" content="">
	<meta property="og:title" content="Teste"/>
	<meta property="og:description" content=""/>

  <meta property="og:site_name" content="Teste"/>
  <meta property="og:type" content="website"/>
  <meta property="og:image" content=""/>
  <meta property="og:url" content="{{ Request::url() }}"/>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<link rel="stylesheet" href="assets/css/vendor.css">

	<link rel="stylesheet" href="assets/css/site.css?cachebuster={{ time() }}">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>

	<!-- Google Analytics -->
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-32643324-2', 'auto');
	ga('send', 'pageview');
	</script>
	<!-- End Google Analytics -->
</head>
<body @if(str_is('site.portfolio*', Route::currentRouteName())) class="body-portfolio" @endif>

	@yield('conteudo')

	<script src="assets/js/site.js?cachebuster={{ time() }}"></script>
	
</body>
</html>
