@extends('site.template.default')

@section('conteudo')

	<div class="conteudo">

		<div class="contemContato">

			<div class="ladoContato ladoBranco">
				<div class="padding-interno">

					<h1>+ contato</h1>

					<div class="mapa_mobile">
						<div class="endereco">
							{!! $contato->endereco !!}
						</div>
						<div class="mapa">
							{!! embed_maps($contato->google_maps, null, 200, 'mapaContato', true) !!}
						</div>
						<div class="formulario">
							@if(Session::has('contatoEnviado'))
								<div class="contato-enviado">
									Obrigado por entrar em contato.<br>Responderemos assim que possível.
								</div>
							@else
								<form action="{{ URL::route('site.contato.enviar') }}" method="post">
									{!! csrf_field() !!}
									<input type="text" name="contato_nome" placeholder="nome" required>
									<input type="email" name="contato_email" placeholder="e-mail" required>
									<input type="text" name="contato_telefone" placeholder="telefone">
									<textarea name="contato_mensagem" required placeholder="mensagem"></textarea>
									<input type="submit" value="ENVIAR">
								</form>
							@endif
						</div>
					</div>

					<div class="telefone">
						{!! $contato->telefone !!}
					</div>

				</div>
			</div>

			<div class="ladoContato ladoPreto">
				<div class="padding-interno">

					<div class="mapa-endereco">
						<div class="endereco">
							{!! $contato->endereco !!}
						</div>
						<div class="mapa">
							{!! embed_maps($contato->google_maps, null, 200, 'mapaContato', true) !!}
						</div>
					</div>

					<div class="formulario">
						@if(Session::has('contatoEnviado'))
							<div class="contato-enviado">
								Obrigado por entrar em contato.<br>Responderemos assim que possível.
							</div>
						@else
							<form action="{{ URL::route('site.contato.enviar') }}" method="post">
								{!! csrf_field() !!}
								<input type="text" name="contato_nome" placeholder="nome" required>
								<input type="email" name="contato_email" placeholder="e-mail" required>
								<input type="text" name="contato_telefone" placeholder="telefone">
								<textarea name="contato_mensagem" required placeholder="mensagem"></textarea>
								<input type="submit" value="ENVIAR">
							</form>
						@endif
					</div>

				</div>
			</div>

		</div>

		@include('site.template.menu')
	</div>

@stop