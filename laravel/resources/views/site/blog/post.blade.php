@extends('site.template.default')

@section('conteudo')

	<div class="conteudo">

		<div class="contemPost">

			<aside>

				<h1>+ blog</h1>

				<div class="contemFiltros">

					<form action="" method="post">

						<select name="filtro_assunto">
							<option value="">assunto/tag</option>
						</select>

						<select name="filtro_personalidade">
							<option value="">personalidade</option>
						</select>

						<input type="submit" value="BUSCAR">

					</form>

				</div>

				<div class="contemDatas">
					<ul class="anos">
						<li class='aberto'>
							<a href="" title="">2014</a>
							<ul class="meses">
								<li>
									<li><a href="" title="">[agosto]</a></li>
									<li><a href="" title="">[julho]</a></li>
									<li><a href="" title="">[junho]</a></li>
									<li><a href="" title="">[maio]</a></li>
									<li><a href="" title="">[abril]</a></li>
									<li><a href="" title="">[fevereiro]</a></li>
									<li><a href="" title="">[janeiro]</a></li>
								</li>
							</ul>
						</li>

						<li>
							<a href="" title="">2013</a>
							<ul class="meses">
								<li>
									<li><a href="" title="">[agosto]</a></li>
									<li><a href="" title="">[julho]</a></li>
									<li><a href="" title="">[junho]</a></li>
									<li><a href="" title="">[maio]</a></li>
									<li><a href="" title="">[abril]</a></li>
									<li><a href="" title="">[fevereiro]</a></li>
									<li><a href="" title="">[janeiro]</a></li>
								</li>
							</ul>
						</li>

						<li>
							<a href="" title="">2012</a>
							<ul class="meses">
								<li>
									<li><a href="" title="">[agosto]</a></li>
									<li><a href="" title="">[julho]</a></li>
									<li><a href="" title="">[junho]</a></li>
									<li><a href="" title="">[maio]</a></li>
									<li><a href="" title="">[abril]</a></li>
									<li><a href="" title="">[fevereiro]</a></li>
									<li><a href="" title="">[janeiro]</a></li>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</aside>

			<section>
				<figure>
					<img src="/assets/img/placeholder-capa-1.jpg" alt="">
				</figure>
				<div class="titulo">
					<h2>
						Astrid Fontenele apresenta o Saia Justa com jóias H.Stern exvlusivas
					</h2>
					<div class="data">
						13 jan 2015
					</div>
				</div>
			</section>

			<article>
				<div class="post">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, quia. Iure cumque minima vitae beatae, esse, temporibus ullam, doloribus fugit soluta excepturi ex quaerat pariatur officia repellat. Aspernatur ad, voluptatem.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi numquam quam sunt debitis nihil, alias omnis et earum cupiditate. Esse officiis praesentium ducimus ratione veniam dignissimos odit amet. Nulla, impedit.</p>
					<img src="/assets/img/placeholder-post-1.jpg" alt="">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, quia. Iure cumque minima vitae beatae, esse, temporibus ullam, doloribus fugit soluta excepturi ex quaerat pariatur officia repellat. Aspernatur ad, voluptatem.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi numquam quam sunt debitis nihil, alias omnis et earum cupiditate. Esse officiis praesentium ducimus ratione veniam dignissimos odit amet. Nulla, impedit.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, quia. Iure cumque minima vitae beatae, esse, temporibus ullam, doloribus fugit soluta excepturi ex quaerat pariatur officia repellat. Aspernatur ad, voluptatem.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi numquam quam sunt debitis nihil, alias omnis et earum cupiditate. Esse officiis praesentium ducimus ratione veniam dignissimos odit amet. Nulla, impedit.</p>
					<img src="/assets/img/placeholder-post-1.jpg" alt="">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, quia. Iure cumque minima vitae beatae, esse, temporibus ullam, doloribus fugit soluta excepturi ex quaerat pariatur officia repellat. Aspernatur ad, voluptatem.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi numquam quam sunt debitis nihil, alias omnis et earum cupiditate. Esse officiis praesentium ducimus ratione veniam dignissimos odit amet. Nulla, impedit.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, quia. Iure cumque minima vitae beatae, esse, temporibus ullam, doloribus fugit soluta excepturi ex quaerat pariatur officia repellat. Aspernatur ad, voluptatem.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi numquam quam sunt debitis nihil, alias omnis et earum cupiditate. Esse officiis praesentium ducimus ratione veniam dignissimos odit amet. Nulla, impedit.</p>
				</div>
			</article>
		</div>


		@include('site.template.menu')
	</div>

@stop