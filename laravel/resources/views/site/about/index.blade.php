@extends('site.template.default')

@section('conteudo')

	<div class="conteudo">

		<div class="contemTextoImagem">
			<section>
				<div class="padding-interno">
					<h1>+ about</h1>

					<div class="lista-redes-sociais">
						@if($about->instagram)
							<a href="{{ prep_url($about->instagram) }}" title="Instagram" target="_blank"><img src="assets/img/icones/about/icone-instagram.png" alt="instagram"></a>
						@endif

						@if($about->twitter)
							<a href="{{ prep_url($about->twitter) }}" title="Twitter" target="_blank"><img src="assets/img/icones/about/icone-twitter.png" alt="twitter"></a>
						@endif

						@if($about->facebook)
							<a href="{{ prep_url($about->facebook) }}" title="Facebook" target="_blank"><img src="assets/img/icones/about/icone-facebook.png" alt="facebook"></a>
						@endif

						@if($about->tumblr)
							<a href="{{ prep_url($about->tumblr) }}" title="Tumblr" target="_blank"><img src="assets/img/icones/about/icone-tumblr-v2.png" alt="tumblr"></a>
						@endif

						@if($about->linkedin)
							<a href="{{ prep_url($about->linkedin) }}" title="LinkedIn" target="_blank"><img src="assets/img/icones/about/icone-linkedin.png" alt="linkedin"></a>
						@endif

						@if($about->pinterest)
							<a href="{{ prep_url($about->pinterest) }}" title="Pinterest" target="_blank"><img src="assets/img/icones/about/icone-pinterest.png" alt="pinterest"></a>
						@endif

						@if($about->youtube)
							<a href="{{ prep_url($about->youtube) }}" title="Youtube" target="_blank"><img src="assets/img/icones/about/icone-youtube.png" alt="youtube"></a>
						@endif

						@if($about->periscope)
							<a href="{{ prep_url($about->periscope) }}" title="Periscope" target="_blank"><img src="assets/img/icones/about/icone-periscope.png" alt="periscope"></a>
						@endif

						@if($about->vimeo)
							<a href="{{ prep_url($about->vimeo) }}" title="Vimeo" target="_blank"><img src="assets/img/icones/about/icone-vimeo.png" alt="vimeo"></a>
						@endif

						@if($about->googleplus)
							<a href="{{ prep_url($about->googleplus) }}" title="Google+" target="_blank"><img src="assets/img/icones/about/icone-googleplus.png" alt="googleplus"></a>
						@endif

						@if($about->wordpress)
							<a href="{{ prep_url($about->wordpress) }}" title="Wordpress" target="_blank"><img src="assets/img/icones/about/icone-wordpress.png" alt="wordpress"></a>
						@endif

						@if($about->snapchat)
							<a href="https://www.snapchat.com/" id="link-snapchat" title="snap: {{ $about->snapchat }}" target="_blank"><img src="assets/img/icones/about/icone-snapchat-v2.png" alt="snapchat"><span>snap: {{ $about->snapchat }}</span></a>
						@endif
					</div>

					{!! $about->texto !!}
				</div>
			</section>
			<aside style="background-image:url(assets/img/about/{{ $about->imagem }});"></aside>
		</div>


		@include('site.template.menu')
	</div>

@stop