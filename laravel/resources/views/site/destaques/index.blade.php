@extends('site.template.default')

@section('conteudo')

  <div class="conteudo destaques">

    <div class="titulo">
      <h2>destaques</h2>
    </div>

    <div class="videos">
      <div class="video">
        <div class="embed-container">
          {!! $embed1 !!}
        </div>
      </div>

      <div class="video">
        <div class="embed-container">
          {!! $embed2 !!}
        </div>
      </div>
    </div>

  </div>
  
  @include('site.template.menu')
  
  <div class="destaques-grid">

    @if (count($banners) > 0)
      <div class="destaques-banners cycle-slideshow"
         data-cycle-fx="scrollHorz"
				 data-cycle-pager=".destaques-grid .destaques-banners .pager"
				 data-cycle-log="false"
				 data-cycle-slides="> .destaques-banner">
        @foreach ($banners as $banner)
          <div class="destaques-banner">
            <img src="assets/img/banners_destaques/redimensionadas/{{$banner->imagem}}">
          </div>
        @endforeach
        <div class="pager"></div>
      </div>
    @endif

    <div class="image image-large">
      @if($destaques->link_1)
        <a href="{{$destaques->link_1}}" title="Visitar destaque">
      @endif
        <img src="assets/img/destaques/{{$destaques->imagem_1}}" alt="Caíco de Queiroz - Destaques">
      @if($destaques->link_1)
        </a>
      @endif
    </div>

    <div class="image image-small">
      @if($destaques->link_2)
        <a href="{{$destaques->link_2}}" title="Visitar destaque">
      @endif
        <img src="assets/img/destaques/{{$destaques->imagem_2}}" alt="Caíco de Queiroz - Destaques">
      @if($destaques->link_2)
        </a>
      @endif
    </div>

    <div class="image image-small">
      @if($destaques->link_3)
        <a href="{{$destaques->link_3}}" title="Visitar destaque">
      @endif
        <img src="assets/img/destaques/{{$destaques->imagem_3}}" alt="Caíco de Queiroz - Destaques">
      @if($destaques->link_3)
        </a>
      @endif
    </div>

    <div class="image image-large">
      @if($destaques->link_4)
        <a href="{{$destaques->link_4}}" title="Visitar destaque">
      @endif
        <img src="assets/img/destaques/{{$destaques->imagem_4}}" alt="Caíco de Queiroz - Destaques">
      @if($destaques->link_4)
        </a>
      @endif
    </div>

    <div class="image image-large">
      @if($destaques->link_5)
        <a href="{{$destaques->link_5}}" title="Visitar destaque">
      @endif
        <img src="assets/img/destaques/{{$destaques->imagem_5}}" alt="Caíco de Queiroz - Destaques">
      @if($destaques->link_5)
        </a>
      @endif
    </div>

    <div class="image image-small">
      @if($destaques->link_6)
        <a href="{{$destaques->link_6}}" title="Visitar destaque">
      @endif
        <img src="assets/img/destaques/{{$destaques->imagem_6}}" alt="Caíco de Queiroz - Destaques">
      @if($destaques->link_6)
        </a>
      @endif
    </div>

    <div class="image image-small">
      @if($destaques->link_7)
        <a href="{{$destaques->link_7}}" title="Visitar destaque">
      @endif
        <img src="assets/img/destaques/{{$destaques->imagem_7}}" alt="Caíco de Queiroz - Destaques">
      @if($destaques->link_7)
        </a>
      @endif
    </div>

    <div class="image image-large">
      @if($destaques->link_8)
        <a href="{{$destaques->link_8}}" title="Visitar destaque">
      @endif
        <img src="assets/img/destaques/{{$destaques->imagem_8}}" alt="Caíco de Queiroz - Destaques">
      @if($destaques->link_8)
        </a>
      @endif
    </div>

    <div class="image image-small">
      @if($destaques->link_9)
        <a href="{{$destaques->link_9}}" title="Visitar destaque">
      @endif
        <img src="assets/img/destaques/{{$destaques->imagem_9}}" alt="Caíco de Queiroz - Destaques">
      @if($destaques->link_9)
        </a>
      @endif
    </div>

    <div class="image image-small">
      @if($destaques->link_10)
        <a href="{{$destaques->link_10}}" title="Visitar destaque">
      @endif
        <img src="assets/img/destaques/{{$destaques->imagem_10}}" alt="Caíco de Queiroz - Destaques">
      @if($destaques->link_10)
        </a>
      @endif
    </div>

    <div class="image image-large">
      @if($destaques->link_11)
        <a href="{{$destaques->link_11}}" title="Visitar destaque">
      @endif
        <img src="assets/img/destaques/{{$destaques->imagem_11}}" alt="Caíco de Queiroz - Destaques">
      @if($destaques->link_11)
        </a>
      @endif
    </div>
  </div>

@stop