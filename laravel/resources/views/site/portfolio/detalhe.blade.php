@extends('site.template.default')

@section('conteudo')

	<div class="conteudo">

		<div class="escondeOverflow">

			<div class="contemModulos">

				@include('site.portfolio.modulos.base', compact('personalidade'))

				@include('site.portfolio.modulos.fotos', compact('personalidade'))

				@include('site.portfolio.modulos.personagens', compact('personalidade'))

				@include('site.portfolio.modulos.publicidade', compact('personalidade'))

				@include('site.portfolio.modulos.capas', compact('personalidade'))

				@include('site.portfolio.modulos.desfiles', compact('personalidade'))

				@include('site.portfolio.modulos.videos', compact('personalidade'))

				@include('site.portfolio.modulos.editorialdemoda', compact('personalidade'))

				@include('site.portfolio.modulos.personalstylist', compact('personalidade'))

				@include('site.portfolio.modulos.redes-sociais', compact('personalidade'))

				@include('site.portfolio.modulos.acoes-sociais', compact('personalidade'))

				@include('site.portfolio.modulos.palestras', compact('personalidade'))

				@include('site.portfolio.modulos.consultoria-moda', compact('personalidade'))

				@include('site.portfolio.modulos.publicacoes', compact('personalidade'))

				@include('site.portfolio.modulos.extras', compact('personalidade'))

				@include('site.portfolio.modulos.red-carpet', compact('personalidade'))

				@include('site.portfolio.modulos.espetaculos', compact('personalidade'))

				@include('site.portfolio.modulos.workshops', compact('personalidade'))

				@include('site.portfolio.modulos.creditos', compact('personalidade'))

				@include('site.portfolio.modulos.premios', compact('personalidade'))

			</div>

			<a href="#" id="seta-navegacao-esquerda" class="fundoBranco" title="ver mais"><img src="assets/img/seta-scroll-lateral.png" alt="ver mais"></a>
			<a href="#" id="seta-navegacao" class="fundoBranco" title="ver mais"><img src="assets/img/seta-scroll-lateral.png" alt="ver mais"></a>

		</div>

		@include('site.template.menu')
	</div>

@stop
