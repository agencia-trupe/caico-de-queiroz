@if(sizeof($personalidade->personagens))

	<div id="modulo-personagens" class="personalidades-modulo ">
		<div class="titulo">
			+ personagens <span class="pager"></span>
		</div>
		<div class="listaImagens cycle-slideshow"
			 data-cycle-fx="scrollHorz"
			 data-cycle-pager="#modulo-personagens .pager"
			 data-cycle-timeout="0"
			 data-cycle-log="false"
			 data-cycle-slides="> .imagem">
			@foreach($personalidade->personagens as $k => $foto)
				<div class="imagem" style="background-image:url('assets/img/portfolio/personagens/redimensionadas/{{ $foto->imagem }}')">
					<a href="assets/img/portfolio/personagens/redimensionadas/{{ $foto->imagem }}" title="Ampliar" class="fancybox-imagem" rel="cycle-modulo-personagens">
						<img src="assets/img/portfolio/personagens/redimensionadas/{{ $foto->imagem }}" alt="Caíco de Queiroz - {{ $personalidade->nome }}">
					</a>
				</div>
			@endforeach
		</div>
	</div>

@endif
