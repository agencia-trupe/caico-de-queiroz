@if(sizeof($personalidade->palestras))

	<div id="modulo-palestras" class="personalidades-modulo modulo-texto-padrao">
		<div class="padding-modulos">
			<div class="titulo">
				+ palestras
			</div>
			<div class="texto">
				{!! $personalidade->palestras->texto !!}
			</div>
		</div>
	</div>

@endif