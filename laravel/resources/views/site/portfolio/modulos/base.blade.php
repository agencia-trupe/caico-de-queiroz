<div id="modulo-base" class="personalidades-modulo">
	<div class="padding-modulos">
		<h2>{{ $personalidade->categoria->titulo }}</h2>
		@if(sizeof($personalidade->subcategoria))
			<h3>{{ $personalidade->subcategoria->titulo }}</h3>
		@endif

		<h1>{{ $personalidade->nome }}</h1>

		@if($personalidade->arquivo_release)
			<a href="portfolio/release/{{ $personalidade->slug }}" title="Download Release" class="btn-download">
				<img src="assets/img/botao-download-release.png" alt="Download Release">
			</a> <br>
		@endif

		@if($personalidade->link_site)
			<a href="{{ prep_url($personalidade->link_site) }}" title="Visitar site" target="_blank" class="btn-visitar-site">
				<img src="assets/img/botao-visit-web.png" alt="Visitar site">
			</a>
		@endif

		@if(sizeof($personalidade->idiomas) > 0)
			<div class="lista-idiomas">
				<span>IDIOMAS:</span>
				@foreach($personalidade->idiomas as $idioma)
					<div class="idioma">
						<img src="assets/img/portfolio/idiomas/redimensionadas/icones/{{$idioma->imagem}}"
								 title="{{$idioma->titulo}}"
								 alt="{{$idioma->titulo}}">
						<span>{{$idioma->titulo}}</span>
					</div>
				@endforeach
			</div>
		@endif
	</div>
</div>
