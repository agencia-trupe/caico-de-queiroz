@if(sizeof($personalidade->workshops))

	<div id="modulo-workshops" class="personalidades-modulo">
		<div class="padding-modulos">
			<div class="titulo">
				+ workshops
			</div>
			<div class="listaWorkshops">
				@foreach($personalidade->workshops as $k => $workshops)
					<div class="workshops">
						@if($workshops->imagem)
							<div class="imagem">
								<a href="assets/img/portfolio/workshops/redimensionadas/{{ $workshops->imagem }}" title="Ampliar" class="fancybox-imagem">
									<img src="assets/img/portfolio/workshops/thumbs/{{ $workshops->imagem }}" alt="{{ $workshops->titulo }}">
								</a>
							</div>
						@endif
						<div class="titulos">
							<h3>{{ $workshops->titulo }}</h3>
							<h3>{{ $workshops->autor }}</h3>
							<h3>{{ $workshops->editora }}</h3>
						</div>
						<div class="texto">
							{{ $workshops->texto }}
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>

@endif