@if(sizeof($personalidade->espetaculos))

	<div id="modulo-espetaculos" class="personalidades-modulo">
		<div class="padding-modulos">
			<div class="titulo">
				+ cinema | música | teatro | tv
			</div>
			<div class="listaEspetaculos">
				@foreach($personalidade->espetaculos as $k => $espetaculo)
					<div class="espetaculos">
						<div class="imagem">
							<a href="assets/img/portfolio/espetaculos/redimensionadas/{{ $espetaculo->imagem }}" title="Ampliar" class="fancybox-imagem">
								<img src="assets/img/portfolio/espetaculos/thumbs/{{ $espetaculo->imagem }}" alt="{{ $espetaculo->titulo }}">
							</a>
						</div>
						<div class="titulos">
							<h3>{{ $espetaculo->titulo }}</h3>
							<h3>{{ $espetaculo->autor }}</h3>
							<h3>{{ $espetaculo->editora }}</h3>
						</div>
						<div class="texto">
							{{ $espetaculo->texto }}
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>

@endif
