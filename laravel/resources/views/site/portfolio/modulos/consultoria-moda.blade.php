@if(sizeof($personalidade->consultoriamoda))

	<div id="modulo-consultoriamoda" class="personalidades-modulo modulo-texto-padrao">
		<div class="padding-modulos">
			<div class="titulo">
				+ consultoria de moda
			</div>
			<div class="texto">
				{!! $personalidade->consultoriamoda->texto !!}
			</div>
		</div>
	</div>

@endif