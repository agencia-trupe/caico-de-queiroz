@if(sizeof($personalidade->fotos))

	<div id="modulo-fotos" class="personalidades-modulo ">
		<div class="titulo">
			+ fotos <span class="pager"></span>
		</div>
		<div class="listaImagens cycle-slideshow"
			 data-cycle-fx="scrollHorz"
			 data-cycle-pager="#modulo-fotos .pager"
			 data-cycle-timeout="0"
			 data-cycle-log="false"
			 data-cycle-slides="> .imagem">
			@foreach($personalidade->fotos as $k => $foto)
				<div class="imagem" style="background-image:url('assets/img/portfolio/fotos/redimensionadas/{{ $foto->imagem }}')">
					<a href="assets/img/portfolio/fotos/redimensionadas/{{ $foto->imagem }}" title="Ampliar" class="fancybox-imagem" rel="cycle-modulo-fotos">
						<img src="assets/img/portfolio/fotos/redimensionadas/{{ $foto->imagem }}" alt="Caíco de Queiroz - {{ $personalidade->nome }}">
					</a>
				</div>
			@endforeach
		</div>
	</div>

@endif