@if(sizeof($personalidade->premios))

	<div id="modulo-premios" class="personalidades-modulo">
		<div class="padding-modulos">
			<div class="titulo">
				+ prêmios
			</div>
			<div class="listaPremios">
				@foreach($personalidade->premios as $k => $premio)
					<div class="premio">
            @if($premio->imagem != '')
  						<div class="imagem">
  							<a href="assets/img/portfolio/premios/redimensionadas/{{ $premio->imagem }}" title="Ampliar" class="fancybox-imagem">
  								<img src="assets/img/portfolio/premios/thumbs/{{ $premio->imagem }}" alt="{{ $premio->titulo }}">
  							</a>
  						</div>
            @endif
						<div class="texto">
							<h3>{{ $premio->titulo }}</h3>
              <p>
                {{ $premio->texto }}
              </p>              
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>

@endif
