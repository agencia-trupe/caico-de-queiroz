@if(sizeof($personalidade->videos))

	<div id="modulo-videos" class="personalidades-modulo">
		<div class="padding-modulos">
			<div class="titulo">
				+ vídeos
			</div>

			<div class="listaVideos">
				@foreach($personalidade->videos as $k => $video)
					<div class="video">
						<img src="assets/img/portfolio/videos/{{ $video->thumbnail }}" alt="{{ $video->titulo }}" data-embed="{{ $video->embed }}">
					</div>
				@endforeach
			</div>
		</div>
	</div>

@endif