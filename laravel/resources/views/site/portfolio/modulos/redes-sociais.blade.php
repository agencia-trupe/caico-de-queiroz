@if(sizeof($personalidade->redessociais))

	<div id="modulo-redes-sociais" class="personalidades-modulo">
		<div class="padding-modulos">
			<div class="titulo">
				+ redes sociais
			</div>

			<?php $ativo = true ?>

			<div class="icones-redes-sociais">
				@if($personalidade->redessociais->twitter)
					<a href="#social-twitter" class="icone icone-twitter <?php if($ativo): echo ' ativo'; $ativo = false; endif ?>">Twitter</a>
				@endif

				@if($personalidade->redessociais->instagram)
					<a href="#social-instagram" class="icone icone-instagram <?php if($ativo): echo ' ativo'; $ativo = false; endif ?>">Instagram</a>
				@endif

				@if($personalidade->redessociais->facebook)
					<a href="#social-facebook" class="icone icone-facebook <?php if($ativo): echo ' ativo'; $ativo = false; endif ?>">Facebook</a>
				@endif

				@if($personalidade->redessociais->vimeo)
					<a href="#social-vimeo" class="icone icone-vimeo <?php if($ativo): echo ' ativo'; $ativo = false; endif ?>">Vimeo</a>
				@endif

				@if($personalidade->redessociais->youtube)
					<a href="#social-youtube" class="icone icone-youtube <?php if($ativo): echo ' ativo'; $ativo = false; endif ?>">Youtube</a>
				@endif

				@if($personalidade->redessociais->linkedin)
					<a href="#social-linkedin" class="icone icone-linkedin <?php if($ativo): echo ' ativo'; $ativo = false; endif ?>">Linkedin</a>
				@endif

				@if($personalidade->redessociais->flickr)
					<a href="#social-flickr" class="icone icone-flickr <?php if($ativo): echo ' ativo'; $ativo = false; endif ?>">Flickr</a>
				@endif

				@if($personalidade->redessociais->pinterest)
					<a href="#social-pinterest" class="icone icone-pinterest <?php if($ativo): echo ' ativo'; $ativo = false; endif ?>">Pinterest</a>
				@endif

				@if($personalidade->redessociais->gplus)
					<a href="#social-gplus" class="icone icone-gplus <?php if($ativo): echo ' ativo'; $ativo = false; endif ?>">Google Plus</a>
				@endif

				@if($personalidade->redessociais->tumblr)
					<a href="#social-tumblr" class="icone icone-tumblr <?php if($ativo): echo ' ativo'; $ativo = false; endif ?>">Tumblr</a>
				@endif

				@if($personalidade->redessociais->imdb)
					<a href="#social-imdb" class="icone icone-imdb <?php if($ativo): echo ' ativo'; $ativo = false; endif ?>">IMDb</a>
				@endif
			</div>

			<div class="lista-redes-sociais">
				<?php $tab_ativo = true ?>

				@if($personalidade->redessociais->twitter)
			    	<div class="tab-social <?php if($tab_ativo): echo ' ativo'; $tab_ativo = false; endif ?>" id="social-twitter">
			    		<div class="seguir">
							<a href="{{ $personalidade->redessociais->twitter }}" title="Twitter" target="_blank">twitter: {{ get_social_user($personalidade->redessociais->twitter) }}</a>
						</div>
						<div class="widget">
				    		<a class="twitter-timeline" href="https://twitter.com/{{ get_social_user($personalidade->redessociais->twitter, '') }}" data-screen-name="{{ get_social_user($personalidade->redessociais->twitter, '') }}" data-widget-id="631543269239951360">Tweets de {{ get_social_user($personalidade->redessociais->twitter) }}</a>
							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
						</div>
				    </div>
			    @endif

				@if($personalidade->redessociais->instagram)
					<div class="tab-social <?php if($tab_ativo): echo ' ativo'; $tab_ativo = false; endif ?>" id="social-instagram">
						<div class="seguir">
							<a href="{{ $personalidade->redessociais->instagram }}" title="Instagram" target="_blank">instagram: {{ get_social_user($personalidade->redessociais->instagram) }}</a>
						</div>
						<!-- embed de post -->
					</div>
				@endif

				@if($personalidade->redessociais->facebook)
					<div class="tab-social <?php if($tab_ativo): echo ' ativo'; $tab_ativo = false; endif ?>" id="social-facebook">
						<div class="seguir">
							<a href="{{ $personalidade->redessociais->facebook }}" title="Facebook" target="_blank">facebook: {{ get_social_user($personalidade->redessociais->facebook, '/') }}</a>
						</div>
						<div class="widget">
							<iframe src="//www.facebook.com/plugins/follow?href={{ urlencode($personalidade->redessociais->facebook) }}&amp;layout=standard&amp;show_faces=true&amp;colorscheme=light&amp;width=450&amp;height=80&amp;locale=pt_BR" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:80px;" allowTransparency="true"></iframe>
						</div>
					</div>
				@endif

			    @if($personalidade->redessociais->vimeo)
				    <div class="tab-social <?php if($tab_ativo): echo ' ativo'; $tab_ativo = false; endif ?>" id="social-vimeo">
						<div class="seguir">
							<a href="{{ $personalidade->redessociais->vimeo }}" title="Vimeo" target="_blank">vimeo: {{ get_social_user($personalidade->redessociais->vimeo, '') }}</a>
						</div>
						<div class="widget">

						</div>
				    </div>
			    @endif

			    @if($personalidade->redessociais->youtube)
				    <div class="tab-social <?php if($tab_ativo): echo ' ativo'; $tab_ativo = false; endif ?>" id="social-youtube">
						<div class="seguir">
							<a href="{{ $personalidade->redessociais->youtube }}" title="YouTube" target="_blank">youtube: {{ get_social_user($personalidade->redessociais->youtube, '', 1) }}</a>
						</div>
						<div class="widget">

						</div>
				    </div>
			    @endif

			    @if($personalidade->redessociais->linkedin)
				    <div class="tab-social <?php if($tab_ativo): echo ' ativo'; $tab_ativo = false; endif ?>" id="social-linkedin">
						<div class="seguir">
							<a href="{{ $personalidade->redessociais->linkedin }}" title="LinkedIn" target="_blank">linkedin: {{ get_social_user($personalidade->redessociais->linkedin, '', 1) }}</a>
						</div>
						<div class="widget">
							<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
							<script type="IN/MemberProfile" data-id="{{ $personalidade->redessociais->linkedin }}" data-format="inline"></script>
						</div>
				    </div>
			    @endif

			    @if($personalidade->redessociais->flickr)
				    <div class="tab-social <?php if($tab_ativo): echo ' ativo'; $tab_ativo = false; endif ?>" id="social-flickr">
						<div class="seguir">
							<a href="{{ $personalidade->redessociais->flickr }}" title="Flickr" target="_blank">flickr: {{ get_social_user($personalidade->redessociais->flickr, '', 1) }}</a>
						</div>
						<div class="widget">

						</div>
				    </div>
			    @endif

			    @if($personalidade->redessociais->pinterest)
				    <div class="tab-social <?php if($tab_ativo): echo ' ativo'; $tab_ativo = false; endif ?>" id="social-pinterest">
						<div class="seguir">
							<a href="{{ $personalidade->redessociais->pinterest }}" title="Pinterest" target="_blank">pinterest: {{ get_social_user($personalidade->redessociais->pinterest, '') }}</a>
						</div>
						<div class="widget">
							<a data-pin-do="embedPin" href="http://pt.pinterest.com/pin/99360735500167749/"></a>
							<script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
						</div>
				    </div>
			    @endif

			    @if($personalidade->redessociais->gplus)
				    <div class="tab-social <?php if($tab_ativo): echo ' ativo'; $tab_ativo = false; endif ?>" id="social-gplus">
						<div class="seguir">
							<a href="{{ $personalidade->redessociais->gplus }}" title="Google Plus" target="_blank">google+: {{ get_social_user($personalidade->redessociais->gplus, '') }}</a>
						</div>
						<div class="widget">

						</div>
				    </div>
			    @endif

			    @if($personalidade->redessociais->tumblr)
				    <div class="tab-social <?php if($tab_ativo): echo ' ativo'; $tab_ativo = false; endif ?>" id="social-tumblr">
						<div class="seguir">
							<!-- <a href="{{ $personalidade->redessociais->tumblr }}" title="Tumblr" target="_blank">tumblr: {{ get_social_user($personalidade->redessociais->tumblr, '') }}</a> -->
							<iframe class="btn" frameborder="0" border="0" scrolling="no" allowtransparency="true" height="20" width="200" src="https://platform.tumblr.com/v2/follow_button.html?type=follow-blog&amp;tumblelog={{ get_social_user($personalidade->redessociais->tumblr, '') }}&amp;color=blue"></iframe>
						</div>
						<div class="widget">
						</div>
				    </div>
			    @endif

			        @if($personalidade->redessociais->imdb)
			    	    <div class="tab-social <?php if($tab_ativo): echo ' ativo'; $tab_ativo = false; endif ?>" id="social-imdb">
			    			<div class="seguir">
			    				<a href="{{ $personalidade->redessociais->imdb }}" title="Google Plus" target="_blank">IMDb</a>
			    			</div>
			    			<div class="widget">
			    			</div>
			    	    </div>
			        @endif
		  	</div>
		</div>
	</div>

@endif
