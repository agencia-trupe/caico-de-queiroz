@if(sizeof($personalidade->extras))

	<div id="modulo-extras" class="personalidades-modulo modulo-texto-padrao">
		<div class="padding-modulos">
			<div class="titulo">
				+ extras
			</div>
			<div class="texto">
				{!! $personalidade->extras->texto !!}
			</div>
		</div>
	</div>

@endif