@if(sizeof($personalidade->creditos))

	<div id="modulo-creditos" class="personalidades-modulo modulo-texto-padrao">
		<div class="padding-modulos">
			<div class="titulo">
				+ créditos
			</div>
			<div class="texto">
				{!! $personalidade->creditos->texto !!}
			</div>
		</div>
	</div>

@endif