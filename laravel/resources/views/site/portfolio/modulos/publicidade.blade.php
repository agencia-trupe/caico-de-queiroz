@if(sizeof($personalidade->publicidade))

	<div id="modulo-publicidade" class="personalidades-modulo modulo-imagens-padrao">
		<div class="padding-modulos">
			<div class="titulo">
				+ publicidade
			</div>
			<div class="listaImagens cycle-slideshow"
				 data-cycle-fx="scrollHorz"
				 data-cycle-pager="#modulo-publicidade .pager"
				 data-cycle-timeout="0"
				 data-cycle-log="false"
				 data-cycle-slides="> .imagem" >
				@foreach($personalidade->publicidade as $k => $foto)
					<div class="imagem">
						<a href="assets/img/portfolio/publicidade/redimensionadas/{{ $foto->imagem }}" title="Ampliar" class="fancybox-imagem" rel="cycle-modulo-publicidade">
							<img src="assets/img/portfolio/publicidade/redimensionadas/{{ $foto->imagem }}" alt="Caíco de Queiroz - {{ $personalidade->nome }}">
						</a>
					</div>
				@endforeach
			</div>
			<div class="pager"></div>
		</div>
	</div>

@endif