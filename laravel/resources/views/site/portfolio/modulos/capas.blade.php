@if(sizeof($personalidade->capas))

	<div id="modulo-capas" class="personalidades-modulo modulo-imagens-padrao">
		<div class="padding-modulos">
			<div class="titulo">
				+ capas
			</div>
			<div class="listaImagens cycle-slideshow"
				 data-cycle-fx="scrollHorz"
				 data-cycle-pager="#modulo-capas .pager"
				 data-cycle-timeout="0"
				 data-cycle-log="false"
				 data-cycle-slides="> .imagem"
				 data-cycle-auto-height="calc" >
				@foreach($personalidade->capas as $k => $foto)
					<div class="imagem">
						<a href="assets/img/portfolio/capas/redimensionadas/{{ $foto->imagem }}" title="Ampliar" class="fancybox-imagem" rel="cycle-modulo-capas">
							<img src="assets/img/portfolio/capas/redimensionadas/{{ $foto->imagem }}" alt="Caíco de Queiroz - {{ $personalidade->nome }}">
						</a>
					</div>
				@endforeach
			</div>
			<div class="pager"></div>
		</div>
	</div>

@endif