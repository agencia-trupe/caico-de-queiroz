@if(sizeof($personalidade->editorialdemoda))

	<div id="modulo-editorialdemoda" class="personalidades-modulo ">
		<div class="titulo">
			+ editorial de moda <span class="pager"></span>
		</div>
		<div class="listaImagens cycle-slideshow"
			 data-cycle-fx="scrollHorz"
			 data-cycle-pager="#modulo-editorialdemoda .pager"
			 data-cycle-timeout="0"
			 data-cycle-log="false"
			 data-cycle-slides="> .imagem">
			@foreach($personalidade->editorialdemoda as $k => $foto)
				<div class="imagem" style="background-image:url('assets/img/portfolio/editorialdemoda/redimensionadas/{{ $foto->imagem }}')">
					<a href="assets/img/portfolio/editorialdemoda/redimensionadas/{{ $foto->imagem }}" title="Ampliar" class="fancybox-imagem" rel="cycle-modulo-editorialdemoda">
						<img src="assets/img/portfolio/editorialdemoda/redimensionadas/{{ $foto->imagem }}" alt="Caíco de Queiroz - {{ $personalidade->nome }}">
					</a>
				</div>
			@endforeach
		</div>
	</div>

@endif
