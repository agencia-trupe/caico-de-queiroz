@if(sizeof($personalidade->acaosocial))

	<div id="modulo-acoes-sociais" class="personalidades-modulo">
		<div class="padding-modulos">
			<div class="titulo">
				+ ação social
			</div>
			<div class="listaAcaoSocial">
				@foreach($personalidade->acaosocial as $k => $acaosocial)
					<div class="acaoSocial">
						<div class="imagem">
							<a href="assets/img/portfolio/acaosocial/redimensionadas/{{ $acaosocial->imagem }}" title="Ampliar" class="fancybox-imagem">
								<img src="assets/img/portfolio/acaosocial/thumbs/{{ $acaosocial->imagem }}" alt="{{ $acaosocial->titulo }}">
							</a>
						</div>
						<div class="texto">
							<h2 style='font-weight:bold;'>{{ $acaosocial->titulo }}</h2>
							{{ $acaosocial->text }}
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>

@endif