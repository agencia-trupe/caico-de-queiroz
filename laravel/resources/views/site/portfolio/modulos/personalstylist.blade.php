@if(sizeof($personalidade->personalstylist))

	<div id="modulo-personalstylist" class="personalidades-modulo ">
		<div class="titulo">
			+ personal stylist <span class="pager"></span>
		</div>
		<div class="listaImagens cycle-slideshow"
			 data-cycle-fx="scrollHorz"
			 data-cycle-pager="#modulo-personalstylist .pager"
			 data-cycle-timeout="0"
			 data-cycle-log="false"
			 data-cycle-slides="> .imagem">
			@foreach($personalidade->personalstylist as $k => $foto)
				<div class="imagem" style="background-image:url('assets/img/portfolio/personalstylist/redimensionadas/{{ $foto->imagem }}')">
					<a href="assets/img/portfolio/personalstylist/redimensionadas/{{ $foto->imagem }}" title="Ampliar" class="fancybox-imagem" rel="cycle-modulo-personalstylist">
						<img src="assets/img/portfolio/personalstylist/redimensionadas/{{ $foto->imagem }}" alt="Caíco de Queiroz - {{ $personalidade->nome }}">
					</a>
				</div>
			@endforeach
		</div>
	</div>

@endif
