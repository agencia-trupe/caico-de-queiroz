@if(sizeof($personalidade->redcarpet))

	<div id="modulo-redcarpet" class="personalidades-modulo modulo-imagens-padrao">
		<div class="padding-modulos">
			<div class="titulo">
				+ red carpet
			</div>
			<div class="listaImagens cycle-slideshow"
				 data-cycle-fx="scrollHorz"
				 data-cycle-pager="#modulo-redcarpet .pager"
				 data-cycle-timeout="0"
				 data-cycle-log="false"
				 data-cycle-slides="> .imagem" >
				@foreach($personalidade->redcarpet as $k => $foto)
					<div class="imagem">
						<a href="assets/img/portfolio/redcarpet/redimensionadas/{{ $foto->imagem }}" title="Ampliar" class="fancybox-imagem" rel="cycle-modulo-red-carpet">
							<img src="assets/img/portfolio/redcarpet/redimensionadas/{{ $foto->imagem }}" alt="Caíco de Queiroz - {{ $personalidade->nome }}">
						</a>
					</div>
				@endforeach
			</div>
			<div class="pager"></div>
		</div>
	</div>

@endif