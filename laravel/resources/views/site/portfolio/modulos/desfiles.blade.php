@if(sizeof($personalidade->desfiles))

	<div id="modulo-desfiles" class="personalidades-modulo modulo-imagens-padrao">
		<div class="padding-modulos">
			<div class="titulo">
				+ desfiles
			</div>
			<div class="listaImagens cycle-slideshow"
				 data-cycle-fx="scrollHorz"
				 data-cycle-pager="#modulo-desfiles .pager"
				 data-cycle-timeout="0"
				 data-cycle-log="false"
				 data-cycle-slides="> .imagem" >
				@foreach($personalidade->desfiles as $k => $foto)
					<div class="imagem">
						<a href="assets/img/portfolio/desfiles/redimensionadas/{{ $foto->imagem }}" title="Ampliar" class="fancybox-imagem" rel="cycle-modulo-desfiles">
							<img src="assets/img/portfolio/desfiles/redimensionadas/{{ $foto->imagem }}" alt="Caíco de Queiroz - {{ $personalidade->nome }}">
						</a>
					</div>
				@endforeach
			</div>
			<div class="pager"></div>
		</div>
	</div>

@endif