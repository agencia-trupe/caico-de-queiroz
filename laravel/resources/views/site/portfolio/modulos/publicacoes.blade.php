@if(sizeof($personalidade->publicacoes))

	<div id="modulo-publicacoes" class="personalidades-modulo">
		<div class="padding-modulos">
			<div class="titulo">
				+ publicações
			</div>
			<div class="listaPublicacoes">
				@foreach($personalidade->publicacoes as $k => $publicacao)
					<div class="publicacao">
						<div class="imagem">
							<a href="assets/img/portfolio/publicacoes/redimensionadas/{{ $publicacao->imagem }}" title="Ampliar" class="fancybox-imagem">
								<img src="assets/img/portfolio/publicacoes/thumbs/{{ $publicacao->imagem }}" alt="{{ $publicacao->titulo }}">
							</a>
						</div>
						<div class="titulos">
							<h3>{{ $publicacao->titulo }}</h3>
							<h3>{{ $publicacao->autor }}</h3>
							<h3>{{ $publicacao->editora }}</h3>
						</div>
						<div class="texto">
							{{ $publicacao->texto }}
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>

@endif