@extends('site.template.default')

@section('conteudo')

	<div class="conteudo">

		<div id="modulo-entrada">

			@if($personalidade->paralaxe)

				<a id="camada-nome" href="portfolio/detalhe/{{ $personalidade->slug }}" title="Ver detalhes">

					<div class="nome" style="color:#{{ $personalidade->cor_nome }}">
						<span @if($personalidade->cor_nome == '000') style="text-shadow: 0 0 2px #FFFFFF" @else style="text-shadow: 0 0 2px #000000" @endif >{{ $personalidade->nome }}</span>
						<img src="assets/img/plus-perfil.png" alt="ver detalhes" class="ver-detalhes-img">
					</div>

				</a>

				<div id="camada-paralaxe" class="paralaxe-wrapper">

					<div class="paralaxe-move">

						<a id="paralaxe-scroll" href="{{ url('portfolio/entrada/'.$personalidade->slug) }}#modulo-paralaxe" title="{{ $personalidade->nome }}">
							<div class="scroll-down">
								<span class="animated bounce" style="color:#{{ $personalidade->cor_nome }}">
									<span class="seta">&lsaquo;</span>
									SCROLL DOWN
								</span>
							</div>
						</a>

						<?php
							$contador = 0;
							if($personalidade->paralaxe->imagem_1 != '')
								$contador++;
							if($personalidade->paralaxe->imagem_3 != '')
								$contador++;
							if($personalidade->paralaxe->imagem_2 != '')
								$contador++;
							if($personalidade->paralaxe->imagem_4 != '')
								$contador++;
							if($personalidade->paralaxe->imagem_5 != '')
								$contador++;
						?>

						<div id="modulo-paralaxe" class="imagens-{{$contador}}">
							<a href="portfolio/detalhe/{{ $personalidade->slug }}" title="Ver detalhes">
								@if($personalidade->paralaxe->imagem_1 != '' && file_exists('assets/img/portfolio/paralaxe/redimensionadas/horizontais/'.$personalidade->paralaxe->imagem_1))
									<div class="prlx-img-wrapper" data-stellar-ratio="0.4"><div id='prlx-1' class="imagem-paralaxe imagem1"><img src='assets/img/portfolio/paralaxe/redimensionadas/horizontais/{{ $personalidade->paralaxe->imagem_1 }}'></div></div>
								@endif
								@if($personalidade->paralaxe->imagem_2 != '' && file_exists('assets/img/portfolio/paralaxe/redimensionadas/horizontais/'.$personalidade->paralaxe->imagem_2))
									<div class="prlx-img-wrapper" data-stellar-ratio="1.8"><div id='prlx-2' class="imagem-paralaxe imagem2"><img src='assets/img/portfolio/paralaxe/redimensionadas/horizontais/{{ $personalidade->paralaxe->imagem_2 }}'></div></div>
								@endif
								@if($personalidade->paralaxe->imagem_3 != '' && file_exists('assets/img/portfolio/paralaxe/redimensionadas/horizontais/'.$personalidade->paralaxe->imagem_3))
									<div class="prlx-img-wrapper" data-stellar-ratio="1.6"><div id='prlx-3' class="imagem-paralaxe imagem3"><img src='assets/img/portfolio/paralaxe/redimensionadas/horizontais/{{ $personalidade->paralaxe->imagem_3 }}'></div></div>
								@endif
								@if($personalidade->paralaxe->imagem_4 != '' && file_exists('assets/img/portfolio/paralaxe/redimensionadas/horizontais/'.$personalidade->paralaxe->imagem_4))
									<div class="prlx-img-wrapper" data-stellar-ratio="0.7"><div id='prlx-4' class="imagem-paralaxe imagem4"><img src='assets/img/portfolio/paralaxe/redimensionadas/verticais/{{ $personalidade->paralaxe->imagem_4 }}'></div></div>
								@endif
								@if($personalidade->paralaxe->imagem_5 != '' && file_exists('assets/img/portfolio/paralaxe/redimensionadas/horizontais/'.$personalidade->paralaxe->imagem_5))
									<div class="prlx-img-wrapper" data-stellar-ratio="2"><div id='prlx-5' class="imagem-paralaxe imagem5"><img src='assets/img/portfolio/paralaxe/redimensionadas/verticais/{{ $personalidade->paralaxe->imagem_5 }}'></div></div>
								@endif
							</a>
						</div>

					</div>
				</div>
			@else

				<a id="camada-nome" class="full" href="portfolio/detalhe/{{ $personalidade->slug }}" title="Ver detalhes">

					<div class="nome" style="color:#{{ $personalidade->cor_nome }}">
						<span @if($personalidade->cor_nome == '000') style="text-shadow: 0 0 2px #FFFFFF" @else style="text-shadow: 0 0 2px #000000" @endif >{{ $personalidade->nome }}</span>
						<img src="assets/img/plus-perfil.png" alt="ver detalhes" class="ver-detalhes-img">
					</div>

				</a>

			@endif

			<div id="camada-imagem-entrada" style="background-image:url('assets/img/portfolio/entrada/{{ $personalidade->imagem_entrada }}')"></div>

		</div>

		@include('site.template.menu')
	</div>

@stop
