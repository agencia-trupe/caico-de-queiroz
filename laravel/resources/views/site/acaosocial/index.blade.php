@extends('site.template.default')

@section('conteudo')

	<div class="conteudo">

		<div class="contemTextoImagem">
			<section>
				<div class="padding-interno">
					<h1>+ ação social</h1>

					{!! $acaosocial->texto !!}
				</div>
			</section>
			<aside class='asideImagensCycle'>
				@if(sizeof($acaosocial->imagens))

					<a href="#" class="cycle-nav" id="cycle-prev"><img src="assets/img/seta-branca-retrocede.png" alt="Retroceder"></a>
					<a href="#" class="cycle-nav" id="cycle-next"><img src="assets/img/seta-branca-avanca.png" alt="Avançar"></a>

					<ul class="listaImagensCycle cycle-slideshow"
						data-cycle-fx="fadeout"
			 			data-cycle-timeout="5000"
			 			data-cycle-log="false"
			 			data-cycle-slides="> .imagemBoxCycle"
			 			data-cycle-prev="#cycle-prev"
			 			data-cycle-next="#cycle-next"
					>
						@foreach($acaosocial->imagens as $k => $img)
							<li class='imagemBoxCycle'>
								<div class="paddingCycle">
									<img src="assets/img/acaosocial/redimensionadas/{{ $img->imagem }}" alt="Caíco de Queiroz">
								</div>
							</li>
						@endforeach
					</ul>
				@endif
			</aside>
		</div>


		@include('site.template.menu')
	</div>

@stop