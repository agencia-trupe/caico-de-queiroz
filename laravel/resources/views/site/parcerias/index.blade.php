@extends('site.template.default')

@section('conteudo')

	<div class="conteudo">

		<div class="contemTextoImagem">
			<section>
				<div class="padding-interno">
					<h1>+ parcerias</h1>

					{!! $parcerias->texto !!}
				</div>
			</section>
			<aside style="background-image:url(assets/img/parcerias/{{ $parcerias->imagem }});"></aside>
		</div>


		@include('site.template.menu')
	</div>

@stop