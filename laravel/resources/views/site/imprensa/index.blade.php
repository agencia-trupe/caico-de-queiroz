@extends('site.template.default')

@section('conteudo')

	<div class="conteudo">

		<div class="contemTextoImagem">
			<section>
				<div class="padding-interno">
					<h1>+ imprensa</h1>

					{!! $imprensa->texto !!}
				</div>
			</section>
			<aside style="background-image:url(assets/img/imprensa/{{ $imprensa->imagem }});"></aside>
		</div>


		@include('site.template.menu')
	</div>

@stop