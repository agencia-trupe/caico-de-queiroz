<?php

use Illuminate\Database\Seeder;

class LicenciamentoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('licenciamento')->delete();

        DB::table('licenciamento')->insert([
            'texto' => '<p>Texto Licenciamento</p>'
        ]);
    }
}
