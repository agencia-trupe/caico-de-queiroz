<?php

use Illuminate\Database\Seeder;

class SubcategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('portfolio_subcategorias')->delete();

      DB::table('portfolio_subcategorias')->insert([
			  [
          'portfolio_categorias_id' => 1,
          'titulo' => 'novos talentos',
          'slug' => 'novos-talentos',
          'ordem' => 0,
			  ],
        [
            'portfolio_categorias_id' => 1,
            'titulo' => 'entretenimento',
            'slug' => 'entretenimento',
            'ordem' => 1,
        ],
        [
            'portfolio_categorias_id' => 1,
            'titulo' => 'esportivo',
            'slug' => 'esportivo',
            'ordem' => 2,
        ],
        [
            'portfolio_categorias_id' => 1,
            'titulo' => 'rádio',
            'slug' => 'radio',
            'ordem' => 3,
        ],
        [
            'portfolio_categorias_id' => 2,
            'titulo' => 'novos talentos',
            'slug' => 'novos-talentos',
            'ordem' => 0,
        ],
        [
            'portfolio_categorias_id' => 2,
            'titulo' => 'atores',
            'slug' => 'atores',
            'ordem' => 1,
        ],
        [
            'portfolio_categorias_id' => 4,
            'titulo' => 'estilistas',
            'slug' => 'estilistas',
            'ordem' => 0,
        ],
        [
            'portfolio_categorias_id' => 4,
            'titulo' => 'stylists',
            'slug' => 'stylists',
            'ordem' => 1,
        ],
        [
            'portfolio_categorias_id' => 4,
            'titulo' => 'editores de moda',
            'slug' => 'editores-de-moda',
            'ordem' => 2,
        ],
        [
            'portfolio_categorias_id' => 10,
            'titulo' => 'chefs',
            'slug' => 'chefs',
            'ordem' => 0,
        ],
        [
            'portfolio_categorias_id' => 10,
            'titulo' => 'crítico gastronômico',
            'slug' => 'critico-gastronomico',
            'ordem' => 1,
        ],
        [
            'portfolio_categorias_id' => 10,
            'titulo' => 'food truckers',
            'slug' => 'food-truckers',
            'ordem' => 2,
        ]
      ]);
    }
}
