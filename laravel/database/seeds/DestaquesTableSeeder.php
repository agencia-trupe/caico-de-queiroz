<?php

use Illuminate\Database\Seeder;

class DestaquesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('destaques')->delete();

      DB::table('destaques')->insert([
        'link_1' => 'to change'
      ]);
    }
}
