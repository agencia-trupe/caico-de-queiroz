<?php

use Illuminate\Database\Seeder;

class ParceriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('parcerias')->delete();

        DB::table('parcerias')->insert([
            'texto' => '<p>Texto Parcerias</p>'
        ]);
    }
}
