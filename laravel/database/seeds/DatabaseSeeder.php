<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('UserTableSeeder');

        // Batch 1
        $this->call('AboutSeeder');
        $this->call('CreditosTableSeeder');
        $this->call('ImprensaTableSeeder');
        $this->call('JuridicoTableSeeder');

        // Batch 2
        $this->call('PublicacoesTableSeeder');
        $this->call('AcaoSocialTableSeeder');
        $this->call('ParceriasTableSeeder');
        $this->call('LicenciamentoTableSeeder');

        // Batch 3
        $this->call('ContatoTableSeeder');
        $this->call('CategoriasTableSeeder');
        $this->call('SubcategoriasTableSeeder');

        $this->call('DestaquesTableSeeder');

        Model::reguard();
    }
}
