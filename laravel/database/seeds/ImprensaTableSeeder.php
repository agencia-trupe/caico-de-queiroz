<?php

use Illuminate\Database\Seeder;

class ImprensaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('imprensa')->delete();

        DB::table('imprensa')->insert([
            'texto' => '<p>Texto Imprensa</p>',
            'imagem' => ''
        ]);
    }
}
