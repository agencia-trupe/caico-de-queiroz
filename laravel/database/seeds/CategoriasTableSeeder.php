<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('portfolio_categorias')->delete();

        DB::table('portfolio_categorias')->insert([
			[
				'id' => 1,
				'titulo' => 'Apresentadores',
				'slug' => 'apresentadores',
				'classe_menu' => 'link-amarelo',
				'ordem' => 0,
			],
			[
				'id' => 2,
				'titulo' => 'Atores',
				'slug' => 'atores',
				'classe_menu' => 'link-vermelho',
				'ordem' => 1,
			],
			[
				'id' => 3,
				'titulo' => 'Design | Arquitetura',
				'slug' => 'design-e-arquitetura',
				'classe_menu' => 'link-azulclaro',
				'ordem' => 2,
			],
			[
				'id' => 4,
				'titulo' => 'Moda',
				'slug' => 'moda',
				'classe_menu' => 'link-rosa',
				'ordem' => 3,
			],
			[
				'id' => 5,
				'titulo' => 'Jornalistas',
				'slug' => 'jornalistas',
				'classe_menu' => 'link-verde',
				'ordem' => 5,
			],
			[
				'id' => 6,
				'titulo' => 'Blogueiros',
				'slug' => 'blogueiros',
				'classe_menu' => 'link-amareloclaro',
				'ordem' => 6,
			],
			[
				'id' => 7,
				'titulo' => 'Make Up | Hair',
				'slug' => 'make-up-e-hair',
				'classe_menu' => 'link-verdeclaro',
				'ordem' => 7,
			],
			[
				'id' => 8,
				'titulo' => 'Palestrantes | Repensadores',
				'slug' => 'palestrantes-e-repensadores',
				'classe_menu' => 'link-laranja',
				'ordem' => 8,
			],
			[
				'id' => 9,
				'titulo' => 'Fotógrafos',
				'slug' => 'fotografos',
				'classe_menu' => 'link-roxo',
				'ordem' => 9,
			],
			[
				'id' => 10,
				'titulo' => 'Gastronomia',
				'slug' => 'gastronomia',
				'classe_menu' => 'link-azul',
				'ordem' => 10,
			],
			[
				'id' => 11,
				'titulo' => 'Produção Executiva',
				'slug' => 'producao-executiva',
				'classe_menu' => 'link-cinza',
				'ordem' => 11,
			]
        ]);
    }
}