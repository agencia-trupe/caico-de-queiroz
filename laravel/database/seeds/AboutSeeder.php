<?php

use Illuminate\Database\Seeder;

class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('about')->delete();

        DB::table('about')->insert([
            'texto' => '<p>Texto About</p>',
            'imagem' => ''
        ]);
    }
}
