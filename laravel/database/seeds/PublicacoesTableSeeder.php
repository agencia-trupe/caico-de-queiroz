<?php

use Illuminate\Database\Seeder;

class PublicacoesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('publicacoes')->delete();

        DB::table('publicacoes')->insert([
            'texto' => '<p>Texto Publicações</p>'
        ]);
    }
}
