<?php

use Illuminate\Database\Seeder;

class CreditosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('creditos')->delete();

        DB::table('creditos')->insert([
            'texto' => '<p>Texto Créditos</p>',
            'imagem' => ''
        ]);
    }
}
