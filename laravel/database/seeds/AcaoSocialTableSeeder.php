<?php

use Illuminate\Database\Seeder;

class AcaoSocialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('acao_social')->delete();

        DB::table('acao_social')->insert([
            'texto' => '<p>Texto Ação Social</p>'
        ]);
    }
}
