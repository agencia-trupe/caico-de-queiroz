<?php

use Illuminate\Database\Seeder;

class JuridicoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('juridico')->delete();

        DB::table('juridico')->insert([
            'texto' => '<p>Texto Jurídico</p>',
            'imagem' => ''
        ]);
    }
}
