<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPortfolioRedesSociaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portfolio_redes_sociais', function (Blueprint $table) {
            $table->string('tumblr')->after('gplus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('portfolio_redes_sociais', function (Blueprint $table) {
            $table->dropColumn('tumblr');
        });
    }
}
