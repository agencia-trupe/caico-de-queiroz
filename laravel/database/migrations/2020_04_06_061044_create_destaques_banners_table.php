<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDestaquesBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('destaques_banners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('destaque_id')->unsigned();
            $table->foreign('destaque_id')->references('id')->on('destaques')->onDelete('cascade');
            $table->string('imagem');
            $table->integer('ordem');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('destaques_banners');
    }
}
