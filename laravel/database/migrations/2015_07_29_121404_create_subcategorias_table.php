<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubcategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio_subcategorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('portfolio_categorias_id')->unsigned();
            $table->foreign('portfolio_categorias_id')->references('id')->on('portfolio_categorias')->onDelete('cascade');
            $table->string('titulo');
            $table->string('slug');
            $table->integer('ordem');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('portfolio_subcategorias');
    }
}
