<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPortfolioVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portfolio_videos', function (Blueprint $table) {
            $table->string('servico')->after('url');
            $table->string('servico_video_id')->after('servico');
            $table->string('thumbnail')->after('servico_video_id');
            $table->string('embed')->after('thumbnail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('portfolio_videos', function (Blueprint $table) {
            $table->dropColumn('servico');
            $table->dropColumn('servico_video_id');
            $table->dropColumn('thumbnail');
            $table->dropColumn('embed');
        });
    }
}
