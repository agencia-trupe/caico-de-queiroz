<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAboutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('about', function (Blueprint $table) {
            $table->string('instagram')->after('imagem');
            $table->string('twitter')->after('instagram');
            $table->string('facebook')->after('twitter');
            $table->string('tumblr')->after('facebook');
            $table->string('linkedin')->after('tumblr');
            $table->string('snapchat')->after('linkedin');
            $table->string('pinterest')->after('snapchat');
            $table->string('youtube')->after('pinterest');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('about', function (Blueprint $table) {
            $table->dropColumn('instagram');
            $table->dropColumn('twitter');
            $table->dropColumn('facebook');
            $table->dropColumn('tumblr');
            $table->dropColumn('linkedin');
            $table->dropColumn('snapchat');
            $table->dropColumn('pinterest');
            $table->dropColumn('youtube');
        });
    }
}
