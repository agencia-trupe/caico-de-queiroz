<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParalaxeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio_paralaxe', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('portfolio_personalidades_id')->unsigned();
            $table->foreign('portfolio_personalidades_id')->references('id')->on('portfolio_personalidades')->onDelete('cascade');

            $table->string('imagem_1');
            $table->string('imagem_2');
            $table->string('imagem_3');
            $table->string('imagem_4');
            $table->string('imagem_5');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('portfolio_paralaxe');
    }
}
