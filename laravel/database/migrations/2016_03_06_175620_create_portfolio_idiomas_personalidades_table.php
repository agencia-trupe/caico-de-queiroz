<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioIdiomasPersonalidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio_idiomas_personalidades', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('personalidade')
                  ->unsigned();

            $table->foreign('personalidade')
                  ->references('id')
                  ->on('portfolio_personalidades')
                  ->onDelete('cascade');

            $table->integer('idioma')
                  ->unsigned();

            $table->foreign('idioma')
                  ->references('id')
                  ->on('portfolio_idiomas')
                  ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('portfolio_idiomas_personalidades');
    }
}
