<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio_personalidades', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('portfolio_categorias_id')->unsigned();
            $table->foreign('portfolio_categorias_id')->references('id')->on('portfolio_categorias')->onDelete('cascade');

            $table->integer('portfolio_subcategorias_id')->unsigned()->nullable();
            $table->foreign('portfolio_subcategorias_id')->references('id')->on('portfolio_subcategorias')->onDelete('cascade');

            $table->string('nome');
            $table->string('slug');
            $table->string('cor_nome');
            $table->string('imagem_entrada');
            $table->string('link_site');
            $table->string('arquivo_release');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('portfolio_personalidades');
    }
}
