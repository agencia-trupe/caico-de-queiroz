<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDestaquesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('destaques', function (Blueprint $table) {
            $table->increments('id');
            $table->string('embed_1');
            $table->string('embed_2');
            $table->string('imagem_1');
            $table->string('link_1');
            $table->string('imagem_2');
            $table->string('link_2');
            $table->string('imagem_3');
            $table->string('link_3');
            $table->string('imagem_4');
            $table->string('link_4');
            $table->string('imagem_5');
            $table->string('link_5');
            $table->string('imagem_6');
            $table->string('link_6');
            $table->string('imagem_7');
            $table->string('link_7');
            $table->string('imagem_8');
            $table->string('link_8');
            $table->string('imagem_9');
            $table->string('link_9');
            $table->string('imagem_10');
            $table->string('link_10');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('destaques');
    }
}
