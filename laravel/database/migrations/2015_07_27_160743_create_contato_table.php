<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contato', function (Blueprint $table) {
            $table->increments('id');

            $table->text('telefone');
            $table->text('endereco');
            $table->text('google_maps');

            $table->string('facebook');
            $table->string('twitter');
            $table->string('instagram');
            $table->string('vimeo');
            $table->string('youtube');
            $table->string('linkedin');
            $table->string('flickr');
            $table->string('pinterest');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contato');
    }
}
