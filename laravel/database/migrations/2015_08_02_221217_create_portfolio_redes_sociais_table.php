<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioRedesSociaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio_redes_sociais', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('portfolio_personalidades_id')->unsigned();
            $table->foreign('portfolio_personalidades_id')->references('id')->on('portfolio_personalidades')->onDelete('cascade');

            $table->string('instagram');
            $table->string('facebook');
            $table->string('twitter');
            $table->string('vimeo');
            $table->string('youtube');
            $table->string('linkedin');
            $table->string('flickr');
            $table->string('pinterest');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('portfolio_redes_sociais');
    }
}
