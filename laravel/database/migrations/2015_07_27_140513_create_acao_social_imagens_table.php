<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcaoSocialImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acao_social_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('acao_social_id')->unsigned();
            $table->foreign('acao_social_id')->references('id')->on('acao_social')->onDelete('cascade');
            $table->string('imagem');
            $table->integer('ordem');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('acao_social_imagens');
    }
}
