<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioPersonalStylistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio_personal_stylist', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('portfolio_personalidades_id')->unsigned();
            $table->foreign('portfolio_personalidades_id')->references('id')->on('portfolio_personalidades')->onDelete('cascade');

            $table->string('imagem');
            $table->integer('ordem');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('portfolio_personal_stylist');
    }
}
