<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParceriasImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parcerias_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parcerias_id')->unsigned();
            $table->foreign('parcerias_id')->references('id')->on('parcerias')->onDelete('cascade');
            $table->string('imagem');
            $table->integer('ordem');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('parcerias_imagens');
    }
}
