<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPortfolioPersonalidadesPublicar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('portfolio_personalidades', function (Blueprint $table) {
        $table->integer('publicar')
              ->default(0)
              ->after('arquivo_release');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('portfolio_personalidades', function (Blueprint $table) {
        $table->dropColumn('publicar');
      });
    }
}
