<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDestaquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('destaques', function (Blueprint $table) {
            $table->string('imagem_11')->after('link_10');
            $table->string('link_11')->after('imagem_11');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('destaques', function (Blueprint $table) {
            $table->dropColumn('imagem_11');
            $table->dropColumn('link_11');
        });
    }
}
