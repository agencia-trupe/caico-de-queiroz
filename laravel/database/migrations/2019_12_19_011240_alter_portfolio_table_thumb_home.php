<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPortfolioTableThumbHome extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portfolio_personalidades', function (Blueprint $table) {
          $table->string('thumb_home')->after('imagem_entrada');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('portfolio_personalidades', function (Blueprint $table) {
          $table->dropColumn('thumb_home');
        });
    }
}
