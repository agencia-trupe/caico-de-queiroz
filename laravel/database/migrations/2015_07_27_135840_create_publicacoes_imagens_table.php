<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicacoesImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publicacoes_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('publicacoes_id')->unsigned();
            $table->foreign('publicacoes_id')->references('id')->on('publicacoes')->onDelete('cascade');
            $table->string('imagem');
            $table->integer('ordem');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('publicacoes_imagens');
    }
}
