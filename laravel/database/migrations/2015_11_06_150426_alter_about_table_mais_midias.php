<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAboutTableMaisMidias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('about', function (Blueprint $table) {
            $table->string('periscope')->after('youtube');
            $table->string('vimeo')->after('periscope');
            $table->string('googleplus')->after('vimeo');
            $table->string('wordpress')->after('googleplus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('about', function (Blueprint $table) {
            $table->dropColumn('periscope');
            $table->dropColumn('vimeo');
            $table->dropColumn('googleplus');
            $table->dropColumn('wordpress');
        });
    }
}
