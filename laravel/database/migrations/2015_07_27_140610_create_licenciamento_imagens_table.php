<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLicenciamentoImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licenciamento_imagens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('licenciamento_id')->unsigned();
            $table->foreign('licenciamento_id')->references('id')->on('licenciamento')->onDelete('cascade');
            $table->string('imagem');
            $table->integer('ordem');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('licenciamento_imagens');
    }
}
