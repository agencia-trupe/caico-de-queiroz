create table `portfolio_red_carpet` (`id` int unsigned not null auto_increment primary key, `portfolio_personalidades_id` int unsigned not null, `imagem` varchar(255) not null, `ordem` int not null, `created_at` timestamp default 0 not null, `updated_at` timestamp default 0 not null) default character set utf8 collate utf8_unicode_ci
alter table `portfolio_red_carpet` add constraint portfolio_red_carpet_portfolio_personalidades_id_foreign foreign key (`portfolio_personalidades_id`) references `portfolio_personalidades` (`id`) on delete cascade
create table `portfolio_espetaculos` (`id` int unsigned not null auto_increment primary key, `portfolio_personalidades_id` int unsigned not null, `titulo` text not null, `autor` text not null, `editora` text not null, `imagem` varchar(255) not null, `texto` text not null, `ordem` int not null, `created_at` timestamp default 0 not null, `updated_at` timestamp default 0 not null) default character set utf8 collate utf8_unicode_ci
alter table `portfolio_espetaculos` add constraint portfolio_espetaculos_portfolio_personalidades_id_foreign foreign key (`portfolio_personalidades_id`) references `portfolio_personalidades` (`id`) on delete cascade
create table `portfolio_workshops` (`id` int unsigned not null auto_increment primary key, `portfolio_personalidades_id` int unsigned not null, `titulo` text not null, `autor` text not null, `editora` text not null, `imagem` varchar(255) not null, `texto` text not null, `ordem` int not null, `created_at` timestamp default 0 not null, `updated_at` timestamp default 0 not null) default character set utf8 collate utf8_unicode_ci
alter table `portfolio_workshops` add constraint portfolio_workshops_portfolio_personalidades_id_foreign foreign key (`portfolio_personalidades_id`) references `portfolio_personalidades` (`id`) on delete cascade


alter table `about` add `instagram` varchar(255) not null after `imagem`, add `twitter` varchar(255) not null after `instagram`, add `facebook` varchar(255) not null after `twitter`, add `tumblr` varchar(255) not null after `facebook`, add `linkedin` varchar(255) not null after `tumblr`, add `snapchat` varchar(255) not null after `linkedin`, add `pinterest` varchar(255) not null after `snapchat`, add `youtube` varchar(255) not null after `pinterest`

CREATE TABLE `portfolio_creditos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `portfolio_personalidades_id` int(10) unsigned NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `portfolio_creditos_portfolio_personalidades_id_foreign` (`portfolio_personalidades_id`),
  CONSTRAINT `portfolio_creditos_portfolio_personalidades_id_foreign` FOREIGN KEY (`portfolio_personalidades_id`) REFERENCES `portfolio_personalidades` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
